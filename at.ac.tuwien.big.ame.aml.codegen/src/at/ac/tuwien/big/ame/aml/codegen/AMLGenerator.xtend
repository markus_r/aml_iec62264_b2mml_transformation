package at.ac.tuwien.big.ame.aml.codegen

import at.ac.tuwien.big.ame.aml.CAEXFile
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.ac.tuwien.big.ame.aml.InstanceHierarchy
import at.ac.tuwien.big.ame.aml.InterfaceClassLib
import at.ac.tuwien.big.ame.aml.RoleClassLib
import at.ac.tuwien.big.ame.aml.SystemUnitClassLib
import at.ac.tuwien.big.ame.aml.InternalElement
import at.ac.tuwien.big.ame.aml.InterfaceClass
import at.ac.tuwien.big.ame.aml.RoleClass
import at.ac.tuwien.big.ame.aml.SystemUnitClass
import at.ac.tuwien.big.ame.aml.ExternalInterface
import at.ac.tuwien.big.ame.aml.Attribute
import at.ac.tuwien.big.ame.aml.InternalLink
import at.ac.tuwien.big.ame.aml.SupportedRoleClass
import at.ac.tuwien.big.ame.aml.RoleRequirements

class AMLGenerator implements IGenerator {

	override doGenerate(Resource input, IFileSystemAccess fsa) {
		val EObject root = input.contents.get(0)
		generateAmlFile(root, fsa)
	}
	
	def dispatch generateAmlFile(CAEXFile caexFile, IFileSystemAccess fsa) {
		println("Generating AML file...")
		fsa.generateFile("output.aml", generateCaexFile(caexFile))
		println("AML file completed!")
	}
	
	def dispatch generateAmlFile(EObject object, IFileSystemAccess access) {
		println("AMLCodeGenerator: Ignoring model element: " + object)
	}
	
		
	private def addAutomationMLEditorInformation(){
		'''
		<SuperiorStandardVersion>AutomationML 2.1</SuperiorStandardVersion>
		<SourceDocumentInformation OriginName="AutomationML Editor" OriginID="916578CA-FE0D-474E-A4FC-9E1719892369" OriginVersion="4.4.3.0" OriginVendor="AutomationML e.V." OriginVendorURL="www.AutomationML.org" OriginRelease="4.4.3.0" OriginProjectTitle="unspecified" OriginProjectID="unspecified" />
		'''
	}
	
	private def generateCaexFile(CAEXFile caexFile){
		'''
		<?xml version="1.0" encoding="utf-8"?>
		<CAEXFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.dke.de/CAEX" SchemaVersion="3.0" FileName="output.aml" xsi:schemaLocation="http://www.dke.de/CAEX CAEX_ClassModel_V.3.0.xsd">
			�addAutomationMLEditorInformation�
			�FOR instHier : caexFile.instanceHierarchy�
				�generateInstanceHierarchy(instHier)�
			�ENDFOR�
			�FOR icl : caexFile.interfaceClassLib�
				�generateInterfaceClassLib(icl)�
			�ENDFOR�
			�FOR rcl : caexFile.roleClassLib�
				�generateRoleClassLib(rcl)�
			�ENDFOR�
			�FOR sucl : caexFile.systemUnitClassLib�
				�generateSystemUnitClassLib(sucl)�
			�ENDFOR�
		</CAEXFile>
		'''	
	}
	
	private def generateInstanceHierarchy(InstanceHierarchy instanceHierarchy) {
		'''
		<InstanceHierarchy Name="�instanceHierarchy.name�">
		    �IF instanceHierarchy.description !== null && instanceHierarchy.description.value !== ""�
		    <Description>�instanceHierarchy.description.value�</Description>
		    �ENDIF�
		    �IF instanceHierarchy.version !== null�
		    <Version>�instanceHierarchy.version.toString�</Version>
		    �ENDIF�
		    �FOR internalElement : instanceHierarchy.internalElement�
		    	�generateInternalElement(internalElement)�
		    �ENDFOR�
		</InstanceHierarchy>
		'''	
	}
	
	private def generateInternalElement(InternalElement internalElement){
		'''
		<InternalElement Name="�internalElement.name�" ID="�internalElement.ID�">
			�FOR externalInterface : internalElement.externalInterface�
				�generateExternalInterface(externalInterface)�
			�ENDFOR�
			�FOR subInternalElement : internalElement.internalElement�
				�generateInternalElement(subInternalElement)�
			�ENDFOR�
			�FOR supportedRoleClass : internalElement.supportedRoleClass�
				�generateSupportedRoleClass(supportedRoleClass)�
			�ENDFOR�
			�FOR internalLink : internalElement.internalLink�
				�generateInternalLink(internalLink)�
			�ENDFOR�
			�IF internalElement.roleRequirements !== null�
				�generateRoleRequirements(internalElement.roleRequirements)�
			�ENDIF�
		</InternalElement>
		'''
	}
	
	private def generateRoleRequirements(RoleRequirements roleRequirements){
		'''
		<RoleRequirements RefBaseRoleClassPath="�roleRequirements.refBaseRoleClassPath�" />
		'''
	}
	
	private def generateInternalLink(InternalLink internalLink){
		'''
		<InternalLink Name="�internalLink.name�" RefPartnerSideA="�internalLink.partnerSideA?.ID�" RefPartnerSideB="�internalLink.partnerSideB?.ID�" />
		'''
	}
	
	private def generateSupportedRoleClass(SupportedRoleClass supportedRoleClass){
		'''
		<SupportedRoleClass RefRoleClassPath="�supportedRoleClass.refRoleClassPath�" />
		'''
	}	
	
	private def generateExternalInterface(InterfaceClass interfaceClass){
		'''
		<ExternalInterface Name="�interfaceClass.name�" RefBaseClassPath="�interfaceClass.refBaseClassPath�" ID="�interfaceClass.ID�">
			�FOR attribute : interfaceClass.attribute�
				�generateAttribute(attribute)�
			�ENDFOR�
		</ExternalInterface>
		'''
	}
	
	private def generateAttribute(Attribute attribute){
		'''
		<Attribute Name="�attribute.name�" AttributeDataType="xs:string">
			<Value>�attribute.value�</Value>
		</Attribute>
		'''
	}
	
	private def generateInterfaceClassLib(InterfaceClassLib interfaceClassLib) {
		'''
		<InterfaceClassLib Name="�interfaceClassLib.name�">
		    �IF interfaceClassLib.description !== null && interfaceClassLib.description.value !== ""�
		    <Description>�interfaceClassLib.description.value�</Description>
		  	�ENDIF�
		  	�IF interfaceClassLib.version !== null�
		  	<Version>�interfaceClassLib.version?.value�</Version>
		  	�ENDIF�
		    �FOR interfaceClass : interfaceClassLib.interfaceClass�
		    	�generateInterfaceClass(interfaceClass)�
		    �ENDFOR�
		</InterfaceClassLib>
		'''	
	}
	
	private def generateInterfaceClass(InterfaceClass interfaceClass){
		'''
		<InterfaceClass Name="�interfaceClass.name�" RefBaseClassPath="�interfaceClass.refBaseClassPath�">
				�IF interfaceClass.description !== null && interfaceClass.description.value !== ""�
				<Description>�interfaceClass.description?.value�</Description>
		        �ENDIF�
		        �FOR subInterfaceClass : interfaceClass.interfaceClass�
		        	�generateInterfaceClass(subInterfaceClass)�
		        �ENDFOR�
		</InterfaceClass>
		'''
	}
	
	private def generateRoleClassLib(RoleClassLib roleClassLib) {
		'''
		<RoleClassLib Name="�roleClassLib.name�">
		�IF roleClassLib.description !== null && roleClassLib.description.value !== ""�
		<Description>�roleClassLib.description.value�</Description>
		�ENDIF�
		�IF roleClassLib.version !== null�
		<Version>�roleClassLib.version.value�</Version>
		�ENDIF�
		   �FOR roleClass : roleClassLib.roleClass�
		    	�generateRoleClass(roleClass)�
		    �ENDFOR�
		</RoleClassLib>
		'''	
	}
	
	private def generateRoleClass(RoleClass roleClass){
		'''
		<RoleClass Name="�roleClass.name�" RefBaseClassPath="�roleClass.refBaseClassPath�">
				�IF roleClass.description !== null && roleClass.description.value !== ""�
				<Description>�roleClass.description?.value�</Description>
		        �ENDIF�
		        �FOR subRoleClass : roleClass.roleClass�
		        	�generateRoleClass(subRoleClass)�
		        �ENDFOR�
		</RoleClass>
		'''
	}
	
	private def generateSystemUnitClassLib(SystemUnitClassLib systemUnitClassLib) {
		'''
		<SystemUnitClassLib Name="�systemUnitClassLib.name�">
			�IF systemUnitClassLib.description !== null && systemUnitClassLib.description.value !== ""�
			<Description>�systemUnitClassLib.description?.value�</Description>
		    �ENDIF�
		    �IF systemUnitClassLib.version !== null�
		    <Version>�systemUnitClassLib.version?.value�</Version>
		    �ENDIF�
		    �FOR systemUnitClass : systemUnitClassLib.systemUnitClass�
		    	�generateSystemUnitClass(systemUnitClass)�
		    �ENDFOR�
		</SystemUnitClassLib>
		'''	
	}
	
	private def generateSystemUnitClass(SystemUnitClass systemUnitClass){
		'''
		<SystemUnitClass Name="�systemUnitClass.name�" ID="�systemUnitClass.ID�">
				�IF systemUnitClass.description !== null && systemUnitClass.description.value !== ""�
				<Description>�systemUnitClass.description?.value�</Description>
		        �ENDIF�
		        �FOR attribute : systemUnitClass.attribute�
		        	�generateAttribute(attribute)�
		        �ENDFOR�
		        �FOR internalElement : systemUnitClass.internalElement�
		        	�generateInternalElement(internalElement)�
		        �ENDFOR�
		        <SupportedRoleClass RefRoleClassPath="�systemUnitClass.supportedRoleClass�" />
		      </SystemUnitClass>
		'''
	}

	/**
	 * Formats an Integer by introducing thousands separator.
	 * 
	 * @param i Integer that should be formatted
	 * @return formatted Integer value as String
	 */
	private def formatInteger(int i) {
		var ic = i
		var ret = ""
		while (ic >= 1000) {
			var part = "" + (ic % 1000)
			while (part.length < 3) {
				part = "0" + part
			}
			ret = "." + part + ret
			ic = ic / 1000
		}
		ret = ic + ret
		ret
	}

	/**
	 * Formats a double value by introducing floating point.
	 * 
	 * @param d double value that should be formatted
	 * @return formatted double value as String
	 */
	private def formatDouble(double d) {
		if (d == Math.round(d)) {
			return String.valueOf(d as long)
		}
		val ret = Math.round(d * 10);
		ret / 10 + "," + (ret % 10)
	}

	/**
	 * Returns a String which is stripped of all non alpha-numerical values. 
	 * This is needed for computing the identifiers (id) of HTML elements as well as 
	 * the file names for details pages.
	 * 
	 * @param string String for which non alpha-numerical values should be removed
	 * @return String with removed non alpha-numerical values
	 */
	private def toAlphaNumerical(String string) {
		string.replaceAll("[^A-Za-z0-9]", "")
	}
	
	/**
	 * Computes the output directory in which the generated HTML files should be places.
	 * 
	 * @param system STL system for which HTML code is generated
	 * @return name of the output directory in which the generated HTML files should be placed
	 */
	private def getOutputDir(CAEXFile caexFile) {
		val systemName = caexFile?.fileName
		val systemNameInLowerCase = systemName.toLowerCase
		val systemNameWithoutSpaces = systemNameInLowerCase.replace(" ", "")
		systemNameWithoutSpaces
	}
}
