package at.ac.tuwien.big.ame.aml.codegen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ResourceLoader {
	
	public static String loadResources(String path) throws FileNotFoundException, IOException{
		File file = new File(path);
		StringBuilder result = new StringBuilder();
		if(!file.exists() || file.isDirectory()) {
			throw new FileNotFoundException("File is either a directory or does not exist!");
		}
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String inputLine = "";
		String lineSeparator = System.getProperty("line.separator");
		try {
			while ((inputLine = reader.readLine()) != null) {
				result.append(inputLine+lineSeparator);			
			}
		} catch (IOException e) {
			throw new IOException("Error while reading file!");
		} finally {
			reader.close();
		}
		return result.toString().trim();
	}
}
