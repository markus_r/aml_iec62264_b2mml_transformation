package at.ac.tuwien.big.ame.aml.codegen;

import org.eclipse.emf.mwe2.runtime.workflow.IWorkflowComponent;
import org.eclipse.emf.mwe2.runtime.workflow.IWorkflowContext;

public class Mwe2DebugMessage implements IWorkflowComponent {
	private String message;

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void invoke(IWorkflowContext ctx) {
		System.out.println(getMessage());
	}

	public void postInvoke() {
	}

	public void preInvoke() {
	}
}