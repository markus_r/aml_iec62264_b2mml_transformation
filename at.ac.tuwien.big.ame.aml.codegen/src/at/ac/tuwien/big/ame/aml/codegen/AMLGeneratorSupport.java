package at.ac.tuwien.big.ame.aml.codegen;

import org.eclipse.xtext.resource.generic.AbstractGenericResourceSupport;

import com.google.inject.Module;

public class AMLGeneratorSupport extends AbstractGenericResourceSupport {

	@Override
	protected Module createGuiceModule() {
		return new AMLGeneratorModule();
	}

}
