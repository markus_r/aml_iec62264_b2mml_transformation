package at.ac.tuwien.big.ame.aml.codegen;

import at.ac.tuwien.big.ame.aml.Attribute;
import at.ac.tuwien.big.ame.aml.CAEXFile;
import at.ac.tuwien.big.ame.aml.Description;
import at.ac.tuwien.big.ame.aml.InstanceHierarchy;
import at.ac.tuwien.big.ame.aml.InterfaceClass;
import at.ac.tuwien.big.ame.aml.InterfaceClassLib;
import at.ac.tuwien.big.ame.aml.InternalElement;
import at.ac.tuwien.big.ame.aml.InternalLink;
import at.ac.tuwien.big.ame.aml.RoleClass;
import at.ac.tuwien.big.ame.aml.RoleClassLib;
import at.ac.tuwien.big.ame.aml.RoleRequirements;
import at.ac.tuwien.big.ame.aml.SupportedRoleClass;
import at.ac.tuwien.big.ame.aml.SystemUnitClass;
import at.ac.tuwien.big.ame.aml.SystemUnitClassLib;
import at.ac.tuwien.big.ame.aml.Version;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.xbase.lib.InputOutput;

@SuppressWarnings("all")
public class AMLGenerator implements IGenerator {
  @Override
  public void doGenerate(final Resource input, final IFileSystemAccess fsa) {
    final EObject root = input.getContents().get(0);
    this.generateAmlFile(root, fsa);
  }
  
  protected String _generateAmlFile(final CAEXFile caexFile, final IFileSystemAccess fsa) {
    String _xblockexpression = null;
    {
      InputOutput.<String>println("Generating AML file...");
      fsa.generateFile("output.aml", this.generateCaexFile(caexFile));
      _xblockexpression = InputOutput.<String>println("AML file completed!");
    }
    return _xblockexpression;
  }
  
  protected String _generateAmlFile(final EObject object, final IFileSystemAccess access) {
    return InputOutput.<String>println(("AMLCodeGenerator: Ignoring model element: " + object));
  }
  
  private CharSequence addAutomationMLEditorInformation() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<SuperiorStandardVersion>AutomationML 2.1</SuperiorStandardVersion>");
    _builder.newLine();
    _builder.append("<SourceDocumentInformation OriginName=\"AutomationML Editor\" OriginID=\"916578CA-FE0D-474E-A4FC-9E1719892369\" OriginVersion=\"4.4.3.0\" OriginVendor=\"AutomationML e.V.\" OriginVendorURL=\"www.AutomationML.org\" OriginRelease=\"4.4.3.0\" OriginProjectTitle=\"unspecified\" OriginProjectID=\"unspecified\" />");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateCaexFile(final CAEXFile caexFile) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
    _builder.newLine();
    _builder.append("<CAEXFile xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.dke.de/CAEX\" SchemaVersion=\"3.0\" FileName=\"output.aml\" xsi:schemaLocation=\"http://www.dke.de/CAEX CAEX_ClassModel_V.3.0.xsd\">");
    _builder.newLine();
    _builder.append("\t");
    CharSequence _addAutomationMLEditorInformation = this.addAutomationMLEditorInformation();
    _builder.append(_addAutomationMLEditorInformation, "\t");
    _builder.newLineIfNotEmpty();
    {
      EList<InstanceHierarchy> _instanceHierarchy = caexFile.getInstanceHierarchy();
      for(final InstanceHierarchy instHier : _instanceHierarchy) {
        _builder.append("\t");
        CharSequence _generateInstanceHierarchy = this.generateInstanceHierarchy(instHier);
        _builder.append(_generateInstanceHierarchy, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<InterfaceClassLib> _interfaceClassLib = caexFile.getInterfaceClassLib();
      for(final InterfaceClassLib icl : _interfaceClassLib) {
        _builder.append("\t");
        CharSequence _generateInterfaceClassLib = this.generateInterfaceClassLib(icl);
        _builder.append(_generateInterfaceClassLib, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<RoleClassLib> _roleClassLib = caexFile.getRoleClassLib();
      for(final RoleClassLib rcl : _roleClassLib) {
        _builder.append("\t");
        CharSequence _generateRoleClassLib = this.generateRoleClassLib(rcl);
        _builder.append(_generateRoleClassLib, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<SystemUnitClassLib> _systemUnitClassLib = caexFile.getSystemUnitClassLib();
      for(final SystemUnitClassLib sucl : _systemUnitClassLib) {
        _builder.append("\t");
        CharSequence _generateSystemUnitClassLib = this.generateSystemUnitClassLib(sucl);
        _builder.append(_generateSystemUnitClassLib, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</CAEXFile>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateInstanceHierarchy(final InstanceHierarchy instanceHierarchy) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<InstanceHierarchy Name=\"");
    String _name = instanceHierarchy.getName();
    _builder.append(_name);
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    {
      if (((instanceHierarchy.getDescription() != null) && (instanceHierarchy.getDescription().getValue() != ""))) {
        _builder.append("    ");
        _builder.append("<Description>");
        String _value = instanceHierarchy.getDescription().getValue();
        _builder.append(_value, "    ");
        _builder.append("</Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      Version _version = instanceHierarchy.getVersion();
      boolean _tripleNotEquals = (_version != null);
      if (_tripleNotEquals) {
        _builder.append("    ");
        _builder.append("<Version>");
        String _string = instanceHierarchy.getVersion().toString();
        _builder.append(_string, "    ");
        _builder.append("</Version>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<InternalElement> _internalElement = instanceHierarchy.getInternalElement();
      for(final InternalElement internalElement : _internalElement) {
        _builder.append("    ");
        CharSequence _generateInternalElement = this.generateInternalElement(internalElement);
        _builder.append(_generateInternalElement, "    ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</InstanceHierarchy>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateInternalElement(final InternalElement internalElement) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<InternalElement Name=\"");
    String _name = internalElement.getName();
    _builder.append(_name);
    _builder.append("\" ID=\"");
    String _iD = internalElement.getID();
    _builder.append(_iD);
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    {
      EList<InterfaceClass> _externalInterface = internalElement.getExternalInterface();
      for(final InterfaceClass externalInterface : _externalInterface) {
        _builder.append("\t");
        CharSequence _generateExternalInterface = this.generateExternalInterface(externalInterface);
        _builder.append(_generateExternalInterface, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<InternalElement> _internalElement = internalElement.getInternalElement();
      for(final InternalElement subInternalElement : _internalElement) {
        _builder.append("\t");
        Object _generateInternalElement = this.generateInternalElement(subInternalElement);
        _builder.append(_generateInternalElement, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<SupportedRoleClass> _supportedRoleClass = internalElement.getSupportedRoleClass();
      for(final SupportedRoleClass supportedRoleClass : _supportedRoleClass) {
        _builder.append("\t");
        CharSequence _generateSupportedRoleClass = this.generateSupportedRoleClass(supportedRoleClass);
        _builder.append(_generateSupportedRoleClass, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<InternalLink> _internalLink = internalElement.getInternalLink();
      for(final InternalLink internalLink : _internalLink) {
        _builder.append("\t");
        CharSequence _generateInternalLink = this.generateInternalLink(internalLink);
        _builder.append(_generateInternalLink, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      RoleRequirements _roleRequirements = internalElement.getRoleRequirements();
      boolean _tripleNotEquals = (_roleRequirements != null);
      if (_tripleNotEquals) {
        _builder.append("\t");
        CharSequence _generateRoleRequirements = this.generateRoleRequirements(internalElement.getRoleRequirements());
        _builder.append(_generateRoleRequirements, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</InternalElement>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateRoleRequirements(final RoleRequirements roleRequirements) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<RoleRequirements RefBaseRoleClassPath=\"");
    String _refBaseRoleClassPath = roleRequirements.getRefBaseRoleClassPath();
    _builder.append(_refBaseRoleClassPath);
    _builder.append("\" />");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  private CharSequence generateInternalLink(final InternalLink internalLink) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<InternalLink Name=\"");
    String _name = internalLink.getName();
    _builder.append(_name);
    _builder.append("\" RefPartnerSideA=\"");
    InterfaceClass _partnerSideA = internalLink.getPartnerSideA();
    String _iD = null;
    if (_partnerSideA!=null) {
      _iD=_partnerSideA.getID();
    }
    _builder.append(_iD);
    _builder.append("\" RefPartnerSideB=\"");
    InterfaceClass _partnerSideB = internalLink.getPartnerSideB();
    String _iD_1 = null;
    if (_partnerSideB!=null) {
      _iD_1=_partnerSideB.getID();
    }
    _builder.append(_iD_1);
    _builder.append("\" />");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  private CharSequence generateSupportedRoleClass(final SupportedRoleClass supportedRoleClass) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<SupportedRoleClass RefRoleClassPath=\"");
    String _refRoleClassPath = supportedRoleClass.getRefRoleClassPath();
    _builder.append(_refRoleClassPath);
    _builder.append("\" />");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  private CharSequence generateExternalInterface(final InterfaceClass interfaceClass) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<ExternalInterface Name=\"");
    String _name = interfaceClass.getName();
    _builder.append(_name);
    _builder.append("\" RefBaseClassPath=\"");
    String _refBaseClassPath = interfaceClass.getRefBaseClassPath();
    _builder.append(_refBaseClassPath);
    _builder.append("\" ID=\"");
    String _iD = interfaceClass.getID();
    _builder.append(_iD);
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    {
      EList<Attribute> _attribute = interfaceClass.getAttribute();
      for(final Attribute attribute : _attribute) {
        _builder.append("\t");
        CharSequence _generateAttribute = this.generateAttribute(attribute);
        _builder.append(_generateAttribute, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</ExternalInterface>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateAttribute(final Attribute attribute) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<Attribute Name=\"");
    String _name = attribute.getName();
    _builder.append(_name);
    _builder.append("\" AttributeDataType=\"xs:string\">");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<Value>");
    String _value = attribute.getValue();
    _builder.append(_value, "\t");
    _builder.append("</Value>");
    _builder.newLineIfNotEmpty();
    _builder.append("</Attribute>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateInterfaceClassLib(final InterfaceClassLib interfaceClassLib) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<InterfaceClassLib Name=\"");
    String _name = interfaceClassLib.getName();
    _builder.append(_name);
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    {
      if (((interfaceClassLib.getDescription() != null) && (interfaceClassLib.getDescription().getValue() != ""))) {
        _builder.append("    ");
        _builder.append("<Description>");
        String _value = interfaceClassLib.getDescription().getValue();
        _builder.append(_value, "    ");
        _builder.append("</Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      Version _version = interfaceClassLib.getVersion();
      boolean _tripleNotEquals = (_version != null);
      if (_tripleNotEquals) {
        _builder.append("  \t");
        _builder.append("<Version>");
        Version _version_1 = interfaceClassLib.getVersion();
        String _value_1 = null;
        if (_version_1!=null) {
          _value_1=_version_1.getValue();
        }
        _builder.append(_value_1, "  \t");
        _builder.append("</Version>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<InterfaceClass> _interfaceClass = interfaceClassLib.getInterfaceClass();
      for(final InterfaceClass interfaceClass : _interfaceClass) {
        _builder.append("    ");
        CharSequence _generateInterfaceClass = this.generateInterfaceClass(interfaceClass);
        _builder.append(_generateInterfaceClass, "    ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</InterfaceClassLib>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateInterfaceClass(final InterfaceClass interfaceClass) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<InterfaceClass Name=\"");
    String _name = interfaceClass.getName();
    _builder.append(_name);
    _builder.append("\" RefBaseClassPath=\"");
    String _refBaseClassPath = interfaceClass.getRefBaseClassPath();
    _builder.append(_refBaseClassPath);
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    {
      if (((interfaceClass.getDescription() != null) && (interfaceClass.getDescription().getValue() != ""))) {
        _builder.append("\t\t");
        _builder.append("<Description>");
        Description _description = interfaceClass.getDescription();
        String _value = null;
        if (_description!=null) {
          _value=_description.getValue();
        }
        _builder.append(_value, "\t\t");
        _builder.append("</Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<InterfaceClass> _interfaceClass = interfaceClass.getInterfaceClass();
      for(final InterfaceClass subInterfaceClass : _interfaceClass) {
        _builder.append("        ");
        Object _generateInterfaceClass = this.generateInterfaceClass(subInterfaceClass);
        _builder.append(_generateInterfaceClass, "        ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</InterfaceClass>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateRoleClassLib(final RoleClassLib roleClassLib) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<RoleClassLib Name=\"");
    String _name = roleClassLib.getName();
    _builder.append(_name);
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    {
      if (((roleClassLib.getDescription() != null) && (roleClassLib.getDescription().getValue() != ""))) {
        _builder.append("<Description>");
        String _value = roleClassLib.getDescription().getValue();
        _builder.append(_value);
        _builder.append("</Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      Version _version = roleClassLib.getVersion();
      boolean _tripleNotEquals = (_version != null);
      if (_tripleNotEquals) {
        _builder.append("<Version>");
        String _value_1 = roleClassLib.getVersion().getValue();
        _builder.append(_value_1);
        _builder.append("</Version>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<RoleClass> _roleClass = roleClassLib.getRoleClass();
      for(final RoleClass roleClass : _roleClass) {
        _builder.append("   ");
        CharSequence _generateRoleClass = this.generateRoleClass(roleClass);
        _builder.append(_generateRoleClass, "   ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</RoleClassLib>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateRoleClass(final RoleClass roleClass) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<RoleClass Name=\"");
    String _name = roleClass.getName();
    _builder.append(_name);
    _builder.append("\" RefBaseClassPath=\"");
    String _refBaseClassPath = roleClass.getRefBaseClassPath();
    _builder.append(_refBaseClassPath);
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    {
      if (((roleClass.getDescription() != null) && (roleClass.getDescription().getValue() != ""))) {
        _builder.append("\t\t");
        _builder.append("<Description>");
        Description _description = roleClass.getDescription();
        String _value = null;
        if (_description!=null) {
          _value=_description.getValue();
        }
        _builder.append(_value, "\t\t");
        _builder.append("</Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<RoleClass> _roleClass = roleClass.getRoleClass();
      for(final RoleClass subRoleClass : _roleClass) {
        _builder.append("        ");
        Object _generateRoleClass = this.generateRoleClass(subRoleClass);
        _builder.append(_generateRoleClass, "        ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</RoleClass>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateSystemUnitClassLib(final SystemUnitClassLib systemUnitClassLib) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<SystemUnitClassLib Name=\"");
    String _name = systemUnitClassLib.getName();
    _builder.append(_name);
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    {
      if (((systemUnitClassLib.getDescription() != null) && (systemUnitClassLib.getDescription().getValue() != ""))) {
        _builder.append("\t");
        _builder.append("<Description>");
        Description _description = systemUnitClassLib.getDescription();
        String _value = null;
        if (_description!=null) {
          _value=_description.getValue();
        }
        _builder.append(_value, "\t");
        _builder.append("</Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      Version _version = systemUnitClassLib.getVersion();
      boolean _tripleNotEquals = (_version != null);
      if (_tripleNotEquals) {
        _builder.append("    ");
        _builder.append("<Version>");
        Version _version_1 = systemUnitClassLib.getVersion();
        String _value_1 = null;
        if (_version_1!=null) {
          _value_1=_version_1.getValue();
        }
        _builder.append(_value_1, "    ");
        _builder.append("</Version>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<SystemUnitClass> _systemUnitClass = systemUnitClassLib.getSystemUnitClass();
      for(final SystemUnitClass systemUnitClass : _systemUnitClass) {
        _builder.append("    ");
        CharSequence _generateSystemUnitClass = this.generateSystemUnitClass(systemUnitClass);
        _builder.append(_generateSystemUnitClass, "    ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</SystemUnitClassLib>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateSystemUnitClass(final SystemUnitClass systemUnitClass) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<SystemUnitClass Name=\"");
    String _name = systemUnitClass.getName();
    _builder.append(_name);
    _builder.append("\" ID=\"");
    String _iD = systemUnitClass.getID();
    _builder.append(_iD);
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    {
      if (((systemUnitClass.getDescription() != null) && (systemUnitClass.getDescription().getValue() != ""))) {
        _builder.append("\t\t");
        _builder.append("<Description>");
        Description _description = systemUnitClass.getDescription();
        String _value = null;
        if (_description!=null) {
          _value=_description.getValue();
        }
        _builder.append(_value, "\t\t");
        _builder.append("</Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<Attribute> _attribute = systemUnitClass.getAttribute();
      for(final Attribute attribute : _attribute) {
        _builder.append("        ");
        CharSequence _generateAttribute = this.generateAttribute(attribute);
        _builder.append(_generateAttribute, "        ");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<InternalElement> _internalElement = systemUnitClass.getInternalElement();
      for(final InternalElement internalElement : _internalElement) {
        _builder.append("        ");
        CharSequence _generateInternalElement = this.generateInternalElement(internalElement);
        _builder.append(_generateInternalElement, "        ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("        ");
    _builder.append("<SupportedRoleClass RefRoleClassPath=\"");
    EList<SupportedRoleClass> _supportedRoleClass = systemUnitClass.getSupportedRoleClass();
    _builder.append(_supportedRoleClass, "        ");
    _builder.append("\" />");
    _builder.newLineIfNotEmpty();
    _builder.append("      ");
    _builder.append("</SystemUnitClass>");
    _builder.newLine();
    return _builder;
  }
  
  /**
   * Formats an Integer by introducing thousands separator.
   * 
   * @param i Integer that should be formatted
   * @return formatted Integer value as String
   */
  private String formatInteger(final int i) {
    String _xblockexpression = null;
    {
      int ic = i;
      String ret = "";
      while ((ic >= 1000)) {
        {
          String part = ("" + Integer.valueOf((ic % 1000)));
          while ((part.length() < 3)) {
            part = ("0" + part);
          }
          ret = (("." + part) + ret);
          ic = (ic / 1000);
        }
      }
      String _plus = (Integer.valueOf(ic) + ret);
      ret = _plus;
      _xblockexpression = ret;
    }
    return _xblockexpression;
  }
  
  /**
   * Formats a double value by introducing floating point.
   * 
   * @param d double value that should be formatted
   * @return formatted double value as String
   */
  private String formatDouble(final double d) {
    String _xblockexpression = null;
    {
      long _round = Math.round(d);
      boolean _equals = (d == _round);
      if (_equals) {
        return String.valueOf(((long) d));
      }
      final long ret = Math.round((d * 10));
      String _plus = (Long.valueOf((ret / 10)) + ",");
      _xblockexpression = (_plus + Long.valueOf((ret % 10)));
    }
    return _xblockexpression;
  }
  
  /**
   * Returns a String which is stripped of all non alpha-numerical values.
   * This is needed for computing the identifiers (id) of HTML elements as well as
   * the file names for details pages.
   * 
   * @param string String for which non alpha-numerical values should be removed
   * @return String with removed non alpha-numerical values
   */
  private String toAlphaNumerical(final String string) {
    return string.replaceAll("[^A-Za-z0-9]", "");
  }
  
  /**
   * Computes the output directory in which the generated HTML files should be places.
   * 
   * @param system STL system for which HTML code is generated
   * @return name of the output directory in which the generated HTML files should be placed
   */
  private String getOutputDir(final CAEXFile caexFile) {
    String _xblockexpression = null;
    {
      String _fileName = null;
      if (caexFile!=null) {
        _fileName=caexFile.getFileName();
      }
      final String systemName = _fileName;
      final String systemNameInLowerCase = systemName.toLowerCase();
      final String systemNameWithoutSpaces = systemNameInLowerCase.replace(" ", "");
      _xblockexpression = systemNameWithoutSpaces;
    }
    return _xblockexpression;
  }
  
  public String generateAmlFile(final EObject caexFile, final IFileSystemAccess fsa) {
    if (caexFile instanceof CAEXFile) {
      return _generateAmlFile((CAEXFile)caexFile, fsa);
    } else if (caexFile != null) {
      return _generateAmlFile(caexFile, fsa);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(caexFile, fsa).toString());
    }
  }
}
