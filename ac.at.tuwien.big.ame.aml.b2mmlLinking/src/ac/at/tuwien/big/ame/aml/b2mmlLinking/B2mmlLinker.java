package ac.at.tuwien.big.ame.aml.b2mmlLinking;

import java.util.ArrayList;
import java.util.List;

import at.ac.tuwien.big.ame.aml.AmlFactory;
import at.ac.tuwien.big.ame.aml.Attribute;
import at.ac.tuwien.big.ame.aml.CAEXFile;
import at.ac.tuwien.big.ame.aml.ExternalInterface;
import at.ac.tuwien.big.ame.aml.InstanceHierarchy;
import at.ac.tuwien.big.ame.aml.InternalElement;
import at.ac.tuwien.big.ame.aml.SupportedRoleClass;

public class B2mmlLinker {
	
	// model that will be linked to B2MML files
	private CAEXFile model = null;
	
	public static int genericID = 1000;
	
	public B2mmlLinker(CAEXFile model) {
		this.model = model;
	}
	
	public List<InternalElement> getAllInternalElements(){
		if(model == null) {
			System.err.println("no model selected");
			return null;
		}
		List<InternalElement> answerList = new ArrayList<>();
		for (InstanceHierarchy instanceHierarchy : model.getInstanceHierarchy()) {
			for (InternalElement internalElement : instanceHierarchy.getInternalElement()) {
				answerList.add(internalElement);
			}
		}
		return answerList;
	}
	
	public InternalElement getInternalElement(String name){
		if(model == null || name == null || name.equals("")) {
			System.err.println("no model selected or name given");
			return null;
		}
		for (InstanceHierarchy instanceHierarchy : model.getInstanceHierarchy()) {
			for (InternalElement internalElement : instanceHierarchy.getInternalElement()) {
				if(internalElement.getName().equals(name)) {
					return internalElement;
				}
			}
		}
		return null;
	}
	
	/*
	 * <InternalElement Name="Robot-B2MML-Document" ID="44ddfad7-1936-42f0-9819-e6a469cf32b5">
          <RoleRequirements RefBaseRoleClassPath="AutomationMLB2MMLRoleClassLib/B2mmlData" />
        </InternalElement>
	 */
	public void linkB2mmlToInternalElement(InternalElement internalElement, String b2mmlFilePath) {
		InternalElement b2mmlDocument = AmlFactory.eINSTANCE.createInternalElement();
		ExternalInterface externalInterface = AmlFactory.eINSTANCE.createExternalInterface();
		Attribute mimeType = AmlFactory.eINSTANCE.createAttribute();		
		Attribute file = AmlFactory.eINSTANCE.createAttribute();
		SupportedRoleClass supportedRoleClass = AmlFactory.eINSTANCE.createSupportedRoleClass();
		
		b2mmlDocument.setName("B2MML-Document");
		b2mmlDocument.setID(""+(genericID++));
		
		externalInterface.setName("B2MML");
		externalInterface.setRefBaseClassPath("AutomationMLB2MMLInterfaceClassLib/B2mmlReference");
		externalInterface.setID(""+(genericID++));
		
		mimeType.setName("MIMEType");
		mimeType.setValue("application/x.b2mml+xml");
		
		file.setName("refURI");
		file.setAttributeDataType("xs:anyURI");
		file.setValue(b2mmlFilePath);
		
		supportedRoleClass.setRefRoleClassPath("AutomationMLB2MMLRoleClassLib/B2mmlData");
		
		externalInterface.getAttribute().add(mimeType);
		externalInterface.getAttribute().add(file);
		b2mmlDocument.getSupportedRoleClass().add(supportedRoleClass);
		b2mmlDocument.getExternalInterface().add(externalInterface);
	}
	
	public static void main(String[] args) {
		
	}

}
