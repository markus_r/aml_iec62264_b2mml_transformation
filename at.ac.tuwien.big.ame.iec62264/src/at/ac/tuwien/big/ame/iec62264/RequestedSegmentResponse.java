/**
 */
package at.ac.tuwien.big.ame.iec62264;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requested Segment Response</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getRequestedSegmentResponse()
 * @model
 * @generated
 */
public interface RequestedSegmentResponse extends SegmentResponse {
} // RequestedSegmentResponse
