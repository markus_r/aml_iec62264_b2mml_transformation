/**
 */
package at.ac.tuwien.big.ame.iec62264.util;

import at.ac.tuwien.big.ame.iec62264.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package
 * @generated
 */
public class Iec62264AdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Iec62264Package modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iec62264AdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Iec62264Package.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Iec62264Switch<Adapter> modelSwitch = new Iec62264Switch<Adapter>() {
		@Override
		public Adapter casePerson(Person object) {
			return createPersonAdapter();
		}

		@Override
		public Adapter casePersonnelClass(PersonnelClass object) {
			return createPersonnelClassAdapter();
		}

		@Override
		public Adapter caseQualificationTestSpecification(QualificationTestSpecification object) {
			return createQualificationTestSpecificationAdapter();
		}

		@Override
		public Adapter casePersonnelClassProperty(PersonnelClassProperty object) {
			return createPersonnelClassPropertyAdapter();
		}

		@Override
		public Adapter casePersonProperty(PersonProperty object) {
			return createPersonPropertyAdapter();
		}

		@Override
		public Adapter caseQualificationTestResult(QualificationTestResult object) {
			return createQualificationTestResultAdapter();
		}

		@Override
		public Adapter casePersonnelModel(PersonnelModel object) {
			return createPersonnelModelAdapter();
		}

		@Override
		public Adapter caseEquipmentModel(EquipmentModel object) {
			return createEquipmentModelAdapter();
		}

		@Override
		public Adapter caseEquipmentClass(EquipmentClass object) {
			return createEquipmentClassAdapter();
		}

		@Override
		public Adapter caseEquipmentCapabilityTestResult(EquipmentCapabilityTestResult object) {
			return createEquipmentCapabilityTestResultAdapter();
		}

		@Override
		public Adapter caseEquipmentCapabilityTestSpecification(EquipmentCapabilityTestSpecification object) {
			return createEquipmentCapabilityTestSpecificationAdapter();
		}

		@Override
		public Adapter caseEquipmentClassProperty(EquipmentClassProperty object) {
			return createEquipmentClassPropertyAdapter();
		}

		@Override
		public Adapter caseEquipmentProperty(EquipmentProperty object) {
			return createEquipmentPropertyAdapter();
		}

		@Override
		public Adapter caseEquipment(Equipment object) {
			return createEquipmentAdapter();
		}

		@Override
		public Adapter casePhysicalAssetModel(PhysicalAssetModel object) {
			return createPhysicalAssetModelAdapter();
		}

		@Override
		public Adapter casePhysicalAssetClass(PhysicalAssetClass object) {
			return createPhysicalAssetClassAdapter();
		}

		@Override
		public Adapter casePhysicalAssetCapabilityTestResult(PhysicalAssetCapabilityTestResult object) {
			return createPhysicalAssetCapabilityTestResultAdapter();
		}

		@Override
		public Adapter casePhysicalAssetProperty(PhysicalAssetProperty object) {
			return createPhysicalAssetPropertyAdapter();
		}

		@Override
		public Adapter casePhysicalAsset(PhysicalAsset object) {
			return createPhysicalAssetAdapter();
		}

		@Override
		public Adapter casePhysicalAssetCapabilityTestSpecification(PhysicalAssetCapabilityTestSpecification object) {
			return createPhysicalAssetCapabilityTestSpecificationAdapter();
		}

		@Override
		public Adapter casePhysicalAssetClassProperty(PhysicalAssetClassProperty object) {
			return createPhysicalAssetClassPropertyAdapter();
		}

		@Override
		public Adapter caseEquipmentAssetMapping(EquipmentAssetMapping object) {
			return createEquipmentAssetMappingAdapter();
		}

		@Override
		public Adapter caseMaterialModel(MaterialModel object) {
			return createMaterialModelAdapter();
		}

		@Override
		public Adapter caseMaterialClass(MaterialClass object) {
			return createMaterialClassAdapter();
		}

		@Override
		public Adapter caseMaterialTestResult(MaterialTestResult object) {
			return createMaterialTestResultAdapter();
		}

		@Override
		public Adapter caseMaterialTestSpecification(MaterialTestSpecification object) {
			return createMaterialTestSpecificationAdapter();
		}

		@Override
		public Adapter caseMaterialSublot(MaterialSublot object) {
			return createMaterialSublotAdapter();
		}

		@Override
		public Adapter caseMaterialLot(MaterialLot object) {
			return createMaterialLotAdapter();
		}

		@Override
		public Adapter caseMaterialLotProperty(MaterialLotProperty object) {
			return createMaterialLotPropertyAdapter();
		}

		@Override
		public Adapter caseMaterialDefinition(MaterialDefinition object) {
			return createMaterialDefinitionAdapter();
		}

		@Override
		public Adapter caseMaterialDefinitionProperty(MaterialDefinitionProperty object) {
			return createMaterialDefinitionPropertyAdapter();
		}

		@Override
		public Adapter caseMaterialClassProperty(MaterialClassProperty object) {
			return createMaterialClassPropertyAdapter();
		}

		@Override
		public Adapter caseProcessSegmentModel(ProcessSegmentModel object) {
			return createProcessSegmentModelAdapter();
		}

		@Override
		public Adapter caseProcessSegment(ProcessSegment object) {
			return createProcessSegmentAdapter();
		}

		@Override
		public Adapter casePersonnelSegmentSpecificationProperty(PersonnelSegmentSpecificationProperty object) {
			return createPersonnelSegmentSpecificationPropertyAdapter();
		}

		@Override
		public Adapter caseEquipmentSegmentSpecificationProperty(EquipmentSegmentSpecificationProperty object) {
			return createEquipmentSegmentSpecificationPropertyAdapter();
		}

		@Override
		public Adapter casePhysicalAssetSegmentSpecificationProperty(PhysicalAssetSegmentSpecificationProperty object) {
			return createPhysicalAssetSegmentSpecificationPropertyAdapter();
		}

		@Override
		public Adapter caseMaterialSegmentSpecificationProperty(MaterialSegmentSpecificationProperty object) {
			return createMaterialSegmentSpecificationPropertyAdapter();
		}

		@Override
		public Adapter caseMaterialSegmentSpecification(MaterialSegmentSpecification object) {
			return createMaterialSegmentSpecificationAdapter();
		}

		@Override
		public Adapter casePhysicalAssetSegmentSpecification(PhysicalAssetSegmentSpecification object) {
			return createPhysicalAssetSegmentSpecificationAdapter();
		}

		@Override
		public Adapter caseEquipmentSegmentSpecification(EquipmentSegmentSpecification object) {
			return createEquipmentSegmentSpecificationAdapter();
		}

		@Override
		public Adapter casePersonnelSegmentSpecification(PersonnelSegmentSpecification object) {
			return createPersonnelSegmentSpecificationAdapter();
		}

		@Override
		public Adapter caseProcessSegmentParameter(ProcessSegmentParameter object) {
			return createProcessSegmentParameterAdapter();
		}

		@Override
		public Adapter caseProcessSegmentDependency(ProcessSegmentDependency object) {
			return createProcessSegmentDependencyAdapter();
		}

		@Override
		public Adapter caseIec62264Model(Iec62264Model object) {
			return createIec62264ModelAdapter();
		}

		@Override
		public Adapter caseOperationsDefinitionModel(OperationsDefinitionModel object) {
			return createOperationsDefinitionModelAdapter();
		}

		@Override
		public Adapter caseOperationsScheduleModel(OperationsScheduleModel object) {
			return createOperationsScheduleModelAdapter();
		}

		@Override
		public Adapter caseOperationsPerformanceModel(OperationsPerformanceModel object) {
			return createOperationsPerformanceModelAdapter();
		}

		@Override
		public Adapter caseOperationsCapabilityModel(OperationsCapabilityModel object) {
			return createOperationsCapabilityModelAdapter();
		}

		@Override
		public Adapter caseProcessSegmentCapabilityModel(ProcessSegmentCapabilityModel object) {
			return createProcessSegmentCapabilityModelAdapter();
		}

		@Override
		public Adapter caseHierarchyScope(HierarchyScope object) {
			return createHierarchyScopeAdapter();
		}

		@Override
		public Adapter caseMaterialSpecificationProperty(MaterialSpecificationProperty object) {
			return createMaterialSpecificationPropertyAdapter();
		}

		@Override
		public Adapter casePhysicalAssetSpecificationProperty(PhysicalAssetSpecificationProperty object) {
			return createPhysicalAssetSpecificationPropertyAdapter();
		}

		@Override
		public Adapter caseEquipmentSpecificationProperty(EquipmentSpecificationProperty object) {
			return createEquipmentSpecificationPropertyAdapter();
		}

		@Override
		public Adapter casePersonnelSpecificationProperty(PersonnelSpecificationProperty object) {
			return createPersonnelSpecificationPropertyAdapter();
		}

		@Override
		public Adapter caseMaterialSpecification(MaterialSpecification object) {
			return createMaterialSpecificationAdapter();
		}

		@Override
		public Adapter casePhysicalAssetSpecification(PhysicalAssetSpecification object) {
			return createPhysicalAssetSpecificationAdapter();
		}

		@Override
		public Adapter caseEquipmentSpecification(EquipmentSpecification object) {
			return createEquipmentSpecificationAdapter();
		}

		@Override
		public Adapter casePersonnelSpecification(PersonnelSpecification object) {
			return createPersonnelSpecificationAdapter();
		}

		@Override
		public Adapter caseParameterSpecification(ParameterSpecification object) {
			return createParameterSpecificationAdapter();
		}

		@Override
		public Adapter caseOperationsSegmentDependency(OperationsSegmentDependency object) {
			return createOperationsSegmentDependencyAdapter();
		}

		@Override
		public Adapter caseOperationsMaterialBillItem(OperationsMaterialBillItem object) {
			return createOperationsMaterialBillItemAdapter();
		}

		@Override
		public Adapter caseOperationsMaterialBill(OperationsMaterialBill object) {
			return createOperationsMaterialBillAdapter();
		}

		@Override
		public Adapter caseOperationsSegment(OperationsSegment object) {
			return createOperationsSegmentAdapter();
		}

		@Override
		public Adapter caseOperationsDefinition(OperationsDefinition object) {
			return createOperationsDefinitionAdapter();
		}

		@Override
		public Adapter caseMaterialRequirement(MaterialRequirement object) {
			return createMaterialRequirementAdapter();
		}

		@Override
		public Adapter caseMaterialRequirementProperty(MaterialRequirementProperty object) {
			return createMaterialRequirementPropertyAdapter();
		}

		@Override
		public Adapter casePhysicalAssetRequirementProperty(PhysicalAssetRequirementProperty object) {
			return createPhysicalAssetRequirementPropertyAdapter();
		}

		@Override
		public Adapter caseEquipmentRequirementProperty(EquipmentRequirementProperty object) {
			return createEquipmentRequirementPropertyAdapter();
		}

		@Override
		public Adapter casePersonnelRequirementProperty(PersonnelRequirementProperty object) {
			return createPersonnelRequirementPropertyAdapter();
		}

		@Override
		public Adapter caseOperationsSchedule(OperationsSchedule object) {
			return createOperationsScheduleAdapter();
		}

		@Override
		public Adapter caseOperationsRequest(OperationsRequest object) {
			return createOperationsRequestAdapter();
		}

		@Override
		public Adapter caseRequestedSegmentResponse(RequestedSegmentResponse object) {
			return createRequestedSegmentResponseAdapter();
		}

		@Override
		public Adapter caseSegmentRequirement(SegmentRequirement object) {
			return createSegmentRequirementAdapter();
		}

		@Override
		public Adapter caseEquipmentRequirement(EquipmentRequirement object) {
			return createEquipmentRequirementAdapter();
		}

		@Override
		public Adapter casePhysicalAssetRequirement(PhysicalAssetRequirement object) {
			return createPhysicalAssetRequirementAdapter();
		}

		@Override
		public Adapter casePersonnelRequirement(PersonnelRequirement object) {
			return createPersonnelRequirementAdapter();
		}

		@Override
		public Adapter caseSegmentParameter(SegmentParameter object) {
			return createSegmentParameterAdapter();
		}

		@Override
		public Adapter caseMaterialActualProperty(MaterialActualProperty object) {
			return createMaterialActualPropertyAdapter();
		}

		@Override
		public Adapter casePhysicalAssetActualProperty(PhysicalAssetActualProperty object) {
			return createPhysicalAssetActualPropertyAdapter();
		}

		@Override
		public Adapter caseEquipmentActualProperty(EquipmentActualProperty object) {
			return createEquipmentActualPropertyAdapter();
		}

		@Override
		public Adapter casePersonnelActualProperty(PersonnelActualProperty object) {
			return createPersonnelActualPropertyAdapter();
		}

		@Override
		public Adapter caseMaterialActual(MaterialActual object) {
			return createMaterialActualAdapter();
		}

		@Override
		public Adapter casePhysicalAssetActual(PhysicalAssetActual object) {
			return createPhysicalAssetActualAdapter();
		}

		@Override
		public Adapter caseEquipmentActual(EquipmentActual object) {
			return createEquipmentActualAdapter();
		}

		@Override
		public Adapter casePersonnelActual(PersonnelActual object) {
			return createPersonnelActualAdapter();
		}

		@Override
		public Adapter caseSegmentData(SegmentData object) {
			return createSegmentDataAdapter();
		}

		@Override
		public Adapter caseSegmentResponse(SegmentResponse object) {
			return createSegmentResponseAdapter();
		}

		@Override
		public Adapter caseOperationsResponse(OperationsResponse object) {
			return createOperationsResponseAdapter();
		}

		@Override
		public Adapter caseOperationsPerformance(OperationsPerformance object) {
			return createOperationsPerformanceAdapter();
		}

		@Override
		public Adapter caseOperationsCapability(OperationsCapability object) {
			return createOperationsCapabilityAdapter();
		}

		@Override
		public Adapter caseMaterialCapabilityProperty(MaterialCapabilityProperty object) {
			return createMaterialCapabilityPropertyAdapter();
		}

		@Override
		public Adapter casePhysicalAssetCapabilityProperty(PhysicalAssetCapabilityProperty object) {
			return createPhysicalAssetCapabilityPropertyAdapter();
		}

		@Override
		public Adapter caseEquipmentCapabilityProperty(EquipmentCapabilityProperty object) {
			return createEquipmentCapabilityPropertyAdapter();
		}

		@Override
		public Adapter casePersonnelCapabilityProperty(PersonnelCapabilityProperty object) {
			return createPersonnelCapabilityPropertyAdapter();
		}

		@Override
		public Adapter caseMaterialCapability(MaterialCapability object) {
			return createMaterialCapabilityAdapter();
		}

		@Override
		public Adapter casePhysicalAssetCapability(PhysicalAssetCapability object) {
			return createPhysicalAssetCapabilityAdapter();
		}

		@Override
		public Adapter caseEquipmentCapability(EquipmentCapability object) {
			return createEquipmentCapabilityAdapter();
		}

		@Override
		public Adapter casePersonnelCapability(PersonnelCapability object) {
			return createPersonnelCapabilityAdapter();
		}

		@Override
		public Adapter caseProcessSegmentCapability(ProcessSegmentCapability object) {
			return createProcessSegmentCapabilityAdapter();
		}

		@Override
		public Adapter caseReferencialProperty(ReferencialProperty object) {
			return createReferencialPropertyAdapter();
		}

		@Override
		public Adapter caseReferentialPersonnelProperty(ReferentialPersonnelProperty object) {
			return createReferentialPersonnelPropertyAdapter();
		}

		@Override
		public Adapter caseReferentialEquipmentProperty(ReferentialEquipmentProperty object) {
			return createReferentialEquipmentPropertyAdapter();
		}

		@Override
		public Adapter caseReferentialPhysicalAssetProperty(ReferentialPhysicalAssetProperty object) {
			return createReferentialPhysicalAssetPropertyAdapter();
		}

		@Override
		public Adapter caseReferentialMaterialTypeProperty(ReferentialMaterialTypeProperty object) {
			return createReferentialMaterialTypePropertyAdapter();
		}

		@Override
		public Adapter caseReferentialMaterialProperty(ReferentialMaterialProperty object) {
			return createReferentialMaterialPropertyAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.Person
	 * @generated
	 */
	public Adapter createPersonAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClass <em>Personnel Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClass
	 * @generated
	 */
	public Adapter createPersonnelClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification <em>Qualification Test Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification
	 * @generated
	 */
	public Adapter createQualificationTestSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty <em>Personnel Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty
	 * @generated
	 */
	public Adapter createPersonnelClassPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonProperty <em>Person Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonProperty
	 * @generated
	 */
	public Adapter createPersonPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestResult <em>Qualification Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestResult
	 * @generated
	 */
	public Adapter createQualificationTestResultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelModel <em>Personnel Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelModel
	 * @generated
	 */
	public Adapter createPersonnelModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentModel <em>Equipment Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentModel
	 * @generated
	 */
	public Adapter createEquipmentModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClass <em>Equipment Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClass
	 * @generated
	 */
	public Adapter createEquipmentClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult <em>Equipment Capability Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult
	 * @generated
	 */
	public Adapter createEquipmentCapabilityTestResultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification <em>Equipment Capability Test Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification
	 * @generated
	 */
	public Adapter createEquipmentCapabilityTestSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty <em>Equipment Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty
	 * @generated
	 */
	public Adapter createEquipmentClassPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentProperty <em>Equipment Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentProperty
	 * @generated
	 */
	public Adapter createEquipmentPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.Equipment <em>Equipment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.Equipment
	 * @generated
	 */
	public Adapter createEquipmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel <em>Physical Asset Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel
	 * @generated
	 */
	public Adapter createPhysicalAssetModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass <em>Physical Asset Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass
	 * @generated
	 */
	public Adapter createPhysicalAssetClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult <em>Physical Asset Capability Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult
	 * @generated
	 */
	public Adapter createPhysicalAssetCapabilityTestResultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty <em>Physical Asset Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty
	 * @generated
	 */
	public Adapter createPhysicalAssetPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset <em>Physical Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAsset
	 * @generated
	 */
	public Adapter createPhysicalAssetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification <em>Physical Asset Capability Test Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification
	 * @generated
	 */
	public Adapter createPhysicalAssetCapabilityTestSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty <em>Physical Asset Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty
	 * @generated
	 */
	public Adapter createPhysicalAssetClassPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping <em>Equipment Asset Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping
	 * @generated
	 */
	public Adapter createEquipmentAssetMappingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialModel <em>Material Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialModel
	 * @generated
	 */
	public Adapter createMaterialModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialClass <em>Material Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClass
	 * @generated
	 */
	public Adapter createMaterialClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestResult <em>Material Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestResult
	 * @generated
	 */
	public Adapter createMaterialTestResultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestSpecification <em>Material Test Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestSpecification
	 * @generated
	 */
	public Adapter createMaterialTestSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot <em>Material Sublot</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot
	 * @generated
	 */
	public Adapter createMaterialSublotAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot <em>Material Lot</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot
	 * @generated
	 */
	public Adapter createMaterialLotAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialLotProperty <em>Material Lot Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLotProperty
	 * @generated
	 */
	public Adapter createMaterialLotPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition <em>Material Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinition
	 * @generated
	 */
	public Adapter createMaterialDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty <em>Material Definition Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty
	 * @generated
	 */
	public Adapter createMaterialDefinitionPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialClassProperty <em>Material Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClassProperty
	 * @generated
	 */
	public Adapter createMaterialClassPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel <em>Process Segment Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel
	 * @generated
	 */
	public Adapter createProcessSegmentModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment <em>Process Segment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment
	 * @generated
	 */
	public Adapter createProcessSegmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecificationProperty <em>Personnel Segment Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecificationProperty
	 * @generated
	 */
	public Adapter createPersonnelSegmentSpecificationPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecificationProperty <em>Equipment Segment Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecificationProperty
	 * @generated
	 */
	public Adapter createEquipmentSegmentSpecificationPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecificationProperty <em>Physical Asset Segment Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecificationProperty
	 * @generated
	 */
	public Adapter createPhysicalAssetSegmentSpecificationPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecificationProperty <em>Material Segment Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecificationProperty
	 * @generated
	 */
	public Adapter createMaterialSegmentSpecificationPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification <em>Material Segment Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification
	 * @generated
	 */
	public Adapter createMaterialSegmentSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification <em>Physical Asset Segment Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification
	 * @generated
	 */
	public Adapter createPhysicalAssetSegmentSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification <em>Equipment Segment Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification
	 * @generated
	 */
	public Adapter createEquipmentSegmentSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification <em>Personnel Segment Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification
	 * @generated
	 */
	public Adapter createPersonnelSegmentSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter <em>Process Segment Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter
	 * @generated
	 */
	public Adapter createProcessSegmentParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency <em>Process Segment Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency
	 * @generated
	 */
	public Adapter createProcessSegmentDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model
	 * @generated
	 */
	public Adapter createIec62264ModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel <em>Operations Definition Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel
	 * @generated
	 */
	public Adapter createOperationsDefinitionModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsScheduleModel <em>Operations Schedule Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsScheduleModel
	 * @generated
	 */
	public Adapter createOperationsScheduleModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformanceModel <em>Operations Performance Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformanceModel
	 * @generated
	 */
	public Adapter createOperationsPerformanceModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapabilityModel <em>Operations Capability Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapabilityModel
	 * @generated
	 */
	public Adapter createOperationsCapabilityModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapabilityModel <em>Process Segment Capability Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapabilityModel
	 * @generated
	 */
	public Adapter createProcessSegmentCapabilityModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.HierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.HierarchyScope
	 * @generated
	 */
	public Adapter createHierarchyScopeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecificationProperty <em>Material Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecificationProperty
	 * @generated
	 */
	public Adapter createMaterialSpecificationPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecificationProperty <em>Physical Asset Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecificationProperty
	 * @generated
	 */
	public Adapter createPhysicalAssetSpecificationPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecificationProperty <em>Equipment Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSpecificationProperty
	 * @generated
	 */
	public Adapter createEquipmentSpecificationPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecificationProperty <em>Personnel Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSpecificationProperty
	 * @generated
	 */
	public Adapter createPersonnelSpecificationPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification <em>Material Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecification
	 * @generated
	 */
	public Adapter createMaterialSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification <em>Physical Asset Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification
	 * @generated
	 */
	public Adapter createPhysicalAssetSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecification <em>Equipment Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSpecification
	 * @generated
	 */
	public Adapter createEquipmentSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecification <em>Personnel Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSpecification
	 * @generated
	 */
	public Adapter createPersonnelSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ParameterSpecification <em>Parameter Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ParameterSpecification
	 * @generated
	 */
	public Adapter createParameterSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency <em>Operations Segment Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency
	 * @generated
	 */
	public Adapter createOperationsSegmentDependencyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem <em>Operations Material Bill Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem
	 * @generated
	 */
	public Adapter createOperationsMaterialBillItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill <em>Operations Material Bill</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill
	 * @generated
	 */
	public Adapter createOperationsMaterialBillAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment <em>Operations Segment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment
	 * @generated
	 */
	public Adapter createOperationsSegmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition <em>Operations Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinition
	 * @generated
	 */
	public Adapter createOperationsDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement <em>Material Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement
	 * @generated
	 */
	public Adapter createMaterialRequirementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirementProperty <em>Material Requirement Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirementProperty
	 * @generated
	 */
	public Adapter createMaterialRequirementPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirementProperty <em>Physical Asset Requirement Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirementProperty
	 * @generated
	 */
	public Adapter createPhysicalAssetRequirementPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirementProperty <em>Equipment Requirement Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirementProperty
	 * @generated
	 */
	public Adapter createEquipmentRequirementPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirementProperty <em>Personnel Requirement Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelRequirementProperty
	 * @generated
	 */
	public Adapter createPersonnelRequirementPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule <em>Operations Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSchedule
	 * @generated
	 */
	public Adapter createOperationsScheduleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest <em>Operations Request</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest
	 * @generated
	 */
	public Adapter createOperationsRequestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.RequestedSegmentResponse <em>Requested Segment Response</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.RequestedSegmentResponse
	 * @generated
	 */
	public Adapter createRequestedSegmentResponseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement <em>Segment Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement
	 * @generated
	 */
	public Adapter createSegmentRequirementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirement <em>Equipment Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirement
	 * @generated
	 */
	public Adapter createEquipmentRequirementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement <em>Physical Asset Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement
	 * @generated
	 */
	public Adapter createPhysicalAssetRequirementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement <em>Personnel Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelRequirement
	 * @generated
	 */
	public Adapter createPersonnelRequirementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.SegmentParameter <em>Segment Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentParameter
	 * @generated
	 */
	public Adapter createSegmentParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialActualProperty <em>Material Actual Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActualProperty
	 * @generated
	 */
	public Adapter createMaterialActualPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActualProperty <em>Physical Asset Actual Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetActualProperty
	 * @generated
	 */
	public Adapter createPhysicalAssetActualPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentActualProperty <em>Equipment Actual Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentActualProperty
	 * @generated
	 */
	public Adapter createEquipmentActualPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelActualProperty <em>Personnel Actual Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelActualProperty
	 * @generated
	 */
	public Adapter createPersonnelActualPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual <em>Material Actual</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual
	 * @generated
	 */
	public Adapter createMaterialActualAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual <em>Physical Asset Actual</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual
	 * @generated
	 */
	public Adapter createPhysicalAssetActualAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentActual <em>Equipment Actual</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentActual
	 * @generated
	 */
	public Adapter createEquipmentActualAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelActual <em>Personnel Actual</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelActual
	 * @generated
	 */
	public Adapter createPersonnelActualAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.SegmentData <em>Segment Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentData
	 * @generated
	 */
	public Adapter createSegmentDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse <em>Segment Response</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse
	 * @generated
	 */
	public Adapter createSegmentResponseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsResponse <em>Operations Response</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsResponse
	 * @generated
	 */
	public Adapter createOperationsResponseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance <em>Operations Performance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformance
	 * @generated
	 */
	public Adapter createOperationsPerformanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability <em>Operations Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability
	 * @generated
	 */
	public Adapter createOperationsCapabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapabilityProperty <em>Material Capability Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapabilityProperty
	 * @generated
	 */
	public Adapter createMaterialCapabilityPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityProperty <em>Physical Asset Capability Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityProperty
	 * @generated
	 */
	public Adapter createPhysicalAssetCapabilityPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityProperty <em>Equipment Capability Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityProperty
	 * @generated
	 */
	public Adapter createEquipmentCapabilityPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapabilityProperty <em>Personnel Capability Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapabilityProperty
	 * @generated
	 */
	public Adapter createPersonnelCapabilityPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability <em>Material Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability
	 * @generated
	 */
	public Adapter createMaterialCapabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability <em>Physical Asset Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability
	 * @generated
	 */
	public Adapter createPhysicalAssetCapabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability <em>Equipment Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability
	 * @generated
	 */
	public Adapter createEquipmentCapabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability <em>Personnel Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability
	 * @generated
	 */
	public Adapter createPersonnelCapabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability <em>Process Segment Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability
	 * @generated
	 */
	public Adapter createProcessSegmentCapabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty <em>Referencial Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferencialProperty
	 * @generated
	 */
	public Adapter createReferencialPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty <em>Referential Personnel Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty
	 * @generated
	 */
	public Adapter createReferentialPersonnelPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty <em>Referential Equipment Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty
	 * @generated
	 */
	public Adapter createReferentialEquipmentPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty <em>Referential Physical Asset Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty
	 * @generated
	 */
	public Adapter createReferentialPhysicalAssetPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty <em>Referential Material Type Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty
	 * @generated
	 */
	public Adapter createReferentialMaterialTypePropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialProperty <em>Referential Material Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialMaterialProperty
	 * @generated
	 */
	public Adapter createReferentialMaterialPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Iec62264AdapterFactory
