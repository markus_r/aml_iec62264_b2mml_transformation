/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.AssemblyRelationship;
import at.ac.tuwien.big.ame.iec62264.AssemblyType;
import at.ac.tuwien.big.ame.iec62264.CapabilityType;
import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialCapability;
import at.ac.tuwien.big.ame.iec62264.MaterialCapabilityProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialClass;
import at.ac.tuwien.big.ame.iec62264.MaterialDefinition;
import at.ac.tuwien.big.ame.iec62264.MaterialLot;
import at.ac.tuwien.big.ame.iec62264.MaterialSublot;

import java.math.BigDecimal;

import java.time.ZonedDateTime;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Capability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getAssembledFromCapabilities <em>Assembled From Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getMaterialCapabilityProperty <em>Material Capability Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getMaterialClass <em>Material Class</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getMaterialDefinition <em>Material Definition</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getMaterialLot <em>Material Lot</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getMaterialSublot <em>Material Sublot</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getReason <em>Reason</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getHierarchyscope <em>Hierarchyscope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MaterialCapabilityImpl extends MinimalEObjectImpl.Container implements MaterialCapability {
	/**
	 * The cached value of the '{@link #getAssembledFromCapabilities() <em>Assembled From Capabilities</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssembledFromCapabilities()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialCapability> assembledFromCapabilities;

	/**
	 * The cached value of the '{@link #getMaterialCapabilityProperty() <em>Material Capability Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialCapabilityProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialCapabilityProperty> materialCapabilityProperty;

	/**
	 * The cached value of the '{@link #getMaterialClass() <em>Material Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClass()
	 * @generated
	 * @ordered
	 */
	protected MaterialClass materialClass;

	/**
	 * The cached value of the '{@link #getMaterialDefinition() <em>Material Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinition()
	 * @generated
	 * @ordered
	 */
	protected MaterialDefinition materialDefinition;

	/**
	 * The cached value of the '{@link #getMaterialLot() <em>Material Lot</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialLot()
	 * @generated
	 * @ordered
	 */
	protected MaterialLot materialLot;

	/**
	 * The cached value of the '{@link #getMaterialSublot() <em>Material Sublot</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSublot()
	 * @generated
	 * @ordered
	 */
	protected MaterialSublot materialSublot;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCapabilityType() <em>Capability Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilityType()
	 * @generated
	 * @ordered
	 */
	protected static final CapabilityType CAPABILITY_TYPE_EDEFAULT = CapabilityType.USED;

	/**
	 * The cached value of the '{@link #getCapabilityType() <em>Capability Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilityType()
	 * @generated
	 * @ordered
	 */
	protected CapabilityType capabilityType = CAPABILITY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getReason() <em>Reason</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected static final String REASON_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReason() <em>Reason</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected String reason = REASON_EDEFAULT;

	/**
	 * The default value of the '{@link #getConfidenceFactor() <em>Confidence Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceFactor()
	 * @generated
	 * @ordered
	 */
	protected static final String CONFIDENCE_FACTOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConfidenceFactor() <em>Confidence Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceFactor()
	 * @generated
	 * @ordered
	 */
	protected String confidenceFactor = CONFIDENCE_FACTOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaterialUse() <em>Material Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialUse()
	 * @generated
	 * @ordered
	 */
	protected static final String MATERIAL_USE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMaterialUse() <em>Material Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialUse()
	 * @generated
	 * @ordered
	 */
	protected String materialUse = MATERIAL_USE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime startTime = START_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime END_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime endTime = END_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal QUANTITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal quantity = QUANTITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String QUANTITY_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String quantityUnitOfMeasure = QUANTITY_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHierarchyscope() <em>Hierarchyscope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyscope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScope hierarchyscope;

	/**
	 * The default value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected static final AssemblyType ASSEMBLY_TYPE_EDEFAULT = AssemblyType.PHYSICAL;

	/**
	 * The cached value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected AssemblyType assemblyType = ASSEMBLY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected static final AssemblyRelationship ASSEMBLY_RELATIONSHIP_EDEFAULT = AssemblyRelationship.PERMANENT;

	/**
	 * The cached value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected AssemblyRelationship assemblyRelationship = ASSEMBLY_RELATIONSHIP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialCapabilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.MATERIAL_CAPABILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialCapability> getAssembledFromCapabilities() {
		if (assembledFromCapabilities == null) {
			assembledFromCapabilities = new EObjectResolvingEList<MaterialCapability>(MaterialCapability.class, this,
					Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLED_FROM_CAPABILITIES);
		}
		return assembledFromCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialCapabilityProperty> getMaterialCapabilityProperty() {
		if (materialCapabilityProperty == null) {
			materialCapabilityProperty = new EObjectContainmentEList<MaterialCapabilityProperty>(
					MaterialCapabilityProperty.class, this,
					Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_CAPABILITY_PROPERTY);
		}
		return materialCapabilityProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialClass getMaterialClass() {
		if (materialClass != null && materialClass.eIsProxy()) {
			InternalEObject oldMaterialClass = (InternalEObject) materialClass;
			materialClass = (MaterialClass) eResolveProxy(oldMaterialClass);
			if (materialClass != oldMaterialClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_CLASS, oldMaterialClass, materialClass));
			}
		}
		return materialClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialClass basicGetMaterialClass() {
		return materialClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialClass(MaterialClass newMaterialClass) {
		MaterialClass oldMaterialClass = materialClass;
		materialClass = newMaterialClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_CLASS,
					oldMaterialClass, materialClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialDefinition getMaterialDefinition() {
		if (materialDefinition != null && materialDefinition.eIsProxy()) {
			InternalEObject oldMaterialDefinition = (InternalEObject) materialDefinition;
			materialDefinition = (MaterialDefinition) eResolveProxy(oldMaterialDefinition);
			if (materialDefinition != oldMaterialDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_DEFINITION, oldMaterialDefinition,
							materialDefinition));
			}
		}
		return materialDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialDefinition basicGetMaterialDefinition() {
		return materialDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialDefinition(MaterialDefinition newMaterialDefinition) {
		MaterialDefinition oldMaterialDefinition = materialDefinition;
		materialDefinition = newMaterialDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_DEFINITION, oldMaterialDefinition,
					materialDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialLot getMaterialLot() {
		if (materialLot != null && materialLot.eIsProxy()) {
			InternalEObject oldMaterialLot = (InternalEObject) materialLot;
			materialLot = (MaterialLot) eResolveProxy(oldMaterialLot);
			if (materialLot != oldMaterialLot) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_LOT, oldMaterialLot, materialLot));
			}
		}
		return materialLot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialLot basicGetMaterialLot() {
		return materialLot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialLot(MaterialLot newMaterialLot) {
		MaterialLot oldMaterialLot = materialLot;
		materialLot = newMaterialLot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_LOT,
					oldMaterialLot, materialLot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSublot getMaterialSublot() {
		if (materialSublot != null && materialSublot.eIsProxy()) {
			InternalEObject oldMaterialSublot = (InternalEObject) materialSublot;
			materialSublot = (MaterialSublot) eResolveProxy(oldMaterialSublot);
			if (materialSublot != oldMaterialSublot) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_SUBLOT, oldMaterialSublot, materialSublot));
			}
		}
		return materialSublot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSublot basicGetMaterialSublot() {
		return materialSublot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialSublot(MaterialSublot newMaterialSublot) {
		MaterialSublot oldMaterialSublot = materialSublot;
		materialSublot = newMaterialSublot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_SUBLOT,
					oldMaterialSublot, materialSublot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_CAPABILITY__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityType getCapabilityType() {
		return capabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCapabilityType(CapabilityType newCapabilityType) {
		CapabilityType oldCapabilityType = capabilityType;
		capabilityType = newCapabilityType == null ? CAPABILITY_TYPE_EDEFAULT : newCapabilityType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_CAPABILITY__CAPABILITY_TYPE,
					oldCapabilityType, capabilityType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReason(String newReason) {
		String oldReason = reason;
		reason = newReason;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_CAPABILITY__REASON,
					oldReason, reason));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConfidenceFactor() {
		return confidenceFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfidenceFactor(String newConfidenceFactor) {
		String oldConfidenceFactor = confidenceFactor;
		confidenceFactor = newConfidenceFactor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.MATERIAL_CAPABILITY__CONFIDENCE_FACTOR, oldConfidenceFactor, confidenceFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMaterialUse() {
		return materialUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialUse(String newMaterialUse) {
		String oldMaterialUse = materialUse;
		materialUse = newMaterialUse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_USE,
					oldMaterialUse, materialUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(ZonedDateTime newStartTime) {
		ZonedDateTime oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_CAPABILITY__START_TIME,
					oldStartTime, startTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(ZonedDateTime newEndTime) {
		ZonedDateTime oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_CAPABILITY__END_TIME,
					oldEndTime, endTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(BigDecimal newQuantity) {
		BigDecimal oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_CAPABILITY__QUANTITY,
					oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQuantityUnitOfMeasure() {
		return quantityUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantityUnitOfMeasure(String newQuantityUnitOfMeasure) {
		String oldQuantityUnitOfMeasure = quantityUnitOfMeasure;
		quantityUnitOfMeasure = newQuantityUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.MATERIAL_CAPABILITY__QUANTITY_UNIT_OF_MEASURE, oldQuantityUnitOfMeasure,
					quantityUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope getHierarchyscope() {
		if (hierarchyscope != null && hierarchyscope.eIsProxy()) {
			InternalEObject oldHierarchyscope = (InternalEObject) hierarchyscope;
			hierarchyscope = (HierarchyScope) eResolveProxy(oldHierarchyscope);
			if (hierarchyscope != oldHierarchyscope) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.MATERIAL_CAPABILITY__HIERARCHYSCOPE, oldHierarchyscope, hierarchyscope));
			}
		}
		return hierarchyscope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope basicGetHierarchyscope() {
		return hierarchyscope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyscope(HierarchyScope newHierarchyscope) {
		HierarchyScope oldHierarchyscope = hierarchyscope;
		hierarchyscope = newHierarchyscope;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_CAPABILITY__HIERARCHYSCOPE,
					oldHierarchyscope, hierarchyscope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyType getAssemblyType() {
		return assemblyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyType(AssemblyType newAssemblyType) {
		AssemblyType oldAssemblyType = assemblyType;
		assemblyType = newAssemblyType == null ? ASSEMBLY_TYPE_EDEFAULT : newAssemblyType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLY_TYPE,
					oldAssemblyType, assemblyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationship getAssemblyRelationship() {
		return assemblyRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyRelationship(AssemblyRelationship newAssemblyRelationship) {
		AssemblyRelationship oldAssemblyRelationship = assemblyRelationship;
		assemblyRelationship = newAssemblyRelationship == null ? ASSEMBLY_RELATIONSHIP_EDEFAULT
				: newAssemblyRelationship;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLY_RELATIONSHIP, oldAssemblyRelationship,
					assemblyRelationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_CAPABILITY_PROPERTY:
			return ((InternalEList<?>) getMaterialCapabilityProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLED_FROM_CAPABILITIES:
			return getAssembledFromCapabilities();
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_CAPABILITY_PROPERTY:
			return getMaterialCapabilityProperty();
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_CLASS:
			if (resolve)
				return getMaterialClass();
			return basicGetMaterialClass();
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_DEFINITION:
			if (resolve)
				return getMaterialDefinition();
			return basicGetMaterialDefinition();
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_LOT:
			if (resolve)
				return getMaterialLot();
			return basicGetMaterialLot();
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_SUBLOT:
			if (resolve)
				return getMaterialSublot();
			return basicGetMaterialSublot();
		case Iec62264Package.MATERIAL_CAPABILITY__DESCRIPTION:
			return getDescription();
		case Iec62264Package.MATERIAL_CAPABILITY__CAPABILITY_TYPE:
			return getCapabilityType();
		case Iec62264Package.MATERIAL_CAPABILITY__REASON:
			return getReason();
		case Iec62264Package.MATERIAL_CAPABILITY__CONFIDENCE_FACTOR:
			return getConfidenceFactor();
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_USE:
			return getMaterialUse();
		case Iec62264Package.MATERIAL_CAPABILITY__START_TIME:
			return getStartTime();
		case Iec62264Package.MATERIAL_CAPABILITY__END_TIME:
			return getEndTime();
		case Iec62264Package.MATERIAL_CAPABILITY__QUANTITY:
			return getQuantity();
		case Iec62264Package.MATERIAL_CAPABILITY__QUANTITY_UNIT_OF_MEASURE:
			return getQuantityUnitOfMeasure();
		case Iec62264Package.MATERIAL_CAPABILITY__HIERARCHYSCOPE:
			if (resolve)
				return getHierarchyscope();
			return basicGetHierarchyscope();
		case Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLY_TYPE:
			return getAssemblyType();
		case Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLY_RELATIONSHIP:
			return getAssemblyRelationship();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLED_FROM_CAPABILITIES:
			getAssembledFromCapabilities().clear();
			getAssembledFromCapabilities().addAll((Collection<? extends MaterialCapability>) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_CAPABILITY_PROPERTY:
			getMaterialCapabilityProperty().clear();
			getMaterialCapabilityProperty().addAll((Collection<? extends MaterialCapabilityProperty>) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_CLASS:
			setMaterialClass((MaterialClass) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_DEFINITION:
			setMaterialDefinition((MaterialDefinition) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_LOT:
			setMaterialLot((MaterialLot) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_SUBLOT:
			setMaterialSublot((MaterialSublot) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__CAPABILITY_TYPE:
			setCapabilityType((CapabilityType) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__REASON:
			setReason((String) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__CONFIDENCE_FACTOR:
			setConfidenceFactor((String) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_USE:
			setMaterialUse((String) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__START_TIME:
			setStartTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__END_TIME:
			setEndTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__QUANTITY:
			setQuantity((BigDecimal) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure((String) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__HIERARCHYSCOPE:
			setHierarchyscope((HierarchyScope) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLY_TYPE:
			setAssemblyType((AssemblyType) newValue);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLY_RELATIONSHIP:
			setAssemblyRelationship((AssemblyRelationship) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLED_FROM_CAPABILITIES:
			getAssembledFromCapabilities().clear();
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_CAPABILITY_PROPERTY:
			getMaterialCapabilityProperty().clear();
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_CLASS:
			setMaterialClass((MaterialClass) null);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_DEFINITION:
			setMaterialDefinition((MaterialDefinition) null);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_LOT:
			setMaterialLot((MaterialLot) null);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_SUBLOT:
			setMaterialSublot((MaterialSublot) null);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__CAPABILITY_TYPE:
			setCapabilityType(CAPABILITY_TYPE_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__REASON:
			setReason(REASON_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__CONFIDENCE_FACTOR:
			setConfidenceFactor(CONFIDENCE_FACTOR_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_USE:
			setMaterialUse(MATERIAL_USE_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__START_TIME:
			setStartTime(START_TIME_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__END_TIME:
			setEndTime(END_TIME_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure(QUANTITY_UNIT_OF_MEASURE_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__HIERARCHYSCOPE:
			setHierarchyscope((HierarchyScope) null);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLY_TYPE:
			setAssemblyType(ASSEMBLY_TYPE_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLY_RELATIONSHIP:
			setAssemblyRelationship(ASSEMBLY_RELATIONSHIP_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLED_FROM_CAPABILITIES:
			return assembledFromCapabilities != null && !assembledFromCapabilities.isEmpty();
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_CAPABILITY_PROPERTY:
			return materialCapabilityProperty != null && !materialCapabilityProperty.isEmpty();
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_CLASS:
			return materialClass != null;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_DEFINITION:
			return materialDefinition != null;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_LOT:
			return materialLot != null;
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_SUBLOT:
			return materialSublot != null;
		case Iec62264Package.MATERIAL_CAPABILITY__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.MATERIAL_CAPABILITY__CAPABILITY_TYPE:
			return capabilityType != CAPABILITY_TYPE_EDEFAULT;
		case Iec62264Package.MATERIAL_CAPABILITY__REASON:
			return REASON_EDEFAULT == null ? reason != null : !REASON_EDEFAULT.equals(reason);
		case Iec62264Package.MATERIAL_CAPABILITY__CONFIDENCE_FACTOR:
			return CONFIDENCE_FACTOR_EDEFAULT == null ? confidenceFactor != null
					: !CONFIDENCE_FACTOR_EDEFAULT.equals(confidenceFactor);
		case Iec62264Package.MATERIAL_CAPABILITY__MATERIAL_USE:
			return MATERIAL_USE_EDEFAULT == null ? materialUse != null : !MATERIAL_USE_EDEFAULT.equals(materialUse);
		case Iec62264Package.MATERIAL_CAPABILITY__START_TIME:
			return START_TIME_EDEFAULT == null ? startTime != null : !START_TIME_EDEFAULT.equals(startTime);
		case Iec62264Package.MATERIAL_CAPABILITY__END_TIME:
			return END_TIME_EDEFAULT == null ? endTime != null : !END_TIME_EDEFAULT.equals(endTime);
		case Iec62264Package.MATERIAL_CAPABILITY__QUANTITY:
			return QUANTITY_EDEFAULT == null ? quantity != null : !QUANTITY_EDEFAULT.equals(quantity);
		case Iec62264Package.MATERIAL_CAPABILITY__QUANTITY_UNIT_OF_MEASURE:
			return QUANTITY_UNIT_OF_MEASURE_EDEFAULT == null ? quantityUnitOfMeasure != null
					: !QUANTITY_UNIT_OF_MEASURE_EDEFAULT.equals(quantityUnitOfMeasure);
		case Iec62264Package.MATERIAL_CAPABILITY__HIERARCHYSCOPE:
			return hierarchyscope != null;
		case Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLY_TYPE:
			return assemblyType != ASSEMBLY_TYPE_EDEFAULT;
		case Iec62264Package.MATERIAL_CAPABILITY__ASSEMBLY_RELATIONSHIP:
			return assemblyRelationship != ASSEMBLY_RELATIONSHIP_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", capabilityType: ");
		result.append(capabilityType);
		result.append(", reason: ");
		result.append(reason);
		result.append(", confidenceFactor: ");
		result.append(confidenceFactor);
		result.append(", materialUse: ");
		result.append(materialUse);
		result.append(", startTime: ");
		result.append(startTime);
		result.append(", endTime: ");
		result.append(endTime);
		result.append(", quantity: ");
		result.append(quantity);
		result.append(", quantityUnitOfMeasure: ");
		result.append(quantityUnitOfMeasure);
		result.append(", assemblyType: ");
		result.append(assemblyType);
		result.append(", assemblyRelationship: ");
		result.append(assemblyRelationship);
		result.append(')');
		return result.toString();
	}

} //MaterialCapabilityImpl
