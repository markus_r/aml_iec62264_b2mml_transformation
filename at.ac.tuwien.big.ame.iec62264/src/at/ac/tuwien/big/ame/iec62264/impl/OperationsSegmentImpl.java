/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.EquipmentSpecification;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialSpecification;
import at.ac.tuwien.big.ame.iec62264.OperationsSegment;
import at.ac.tuwien.big.ame.iec62264.OperationsType;
import at.ac.tuwien.big.ame.iec62264.ParameterSpecification;
import at.ac.tuwien.big.ame.iec62264.PersonnelSpecification;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification;
import at.ac.tuwien.big.ame.iec62264.ProcessSegment;

import java.math.BigDecimal;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Segment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getProcessSegments <em>Process Segments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getSubSegments <em>Sub Segments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getParameterSpecifications <em>Parameter Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getPersonnelSpecifications <em>Personnel Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getEquipmentSpecifications <em>Equipment Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getPhysicalAssetSpecifications <em>Physical Asset Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getMaterialSpecifications <em>Material Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getDuration <em>Duration</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getDurationUnitOfMeasure <em>Duration Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl#getWorkDefinitionId <em>Work Definition Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsSegmentImpl extends MinimalEObjectImpl.Container implements OperationsSegment {
	/**
	 * The cached value of the '{@link #getProcessSegments() <em>Process Segments</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegments()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessSegment> processSegments;

	/**
	 * The cached value of the '{@link #getSubSegments() <em>Sub Segments</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubSegments()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsSegment> subSegments;

	/**
	 * The cached value of the '{@link #getParameterSpecifications() <em>Parameter Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterSpecification> parameterSpecifications;

	/**
	 * The cached value of the '{@link #getPersonnelSpecifications() <em>Personnel Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelSpecification> personnelSpecifications;

	/**
	 * The cached value of the '{@link #getEquipmentSpecifications() <em>Equipment Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentSpecification> equipmentSpecifications;

	/**
	 * The cached value of the '{@link #getPhysicalAssetSpecifications() <em>Physical Asset Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetSpecification> physicalAssetSpecifications;

	/**
	 * The cached value of the '{@link #getMaterialSpecifications() <em>Material Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSpecification> materialSpecifications;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal DURATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal duration = DURATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDurationUnitOfMeasure() <em>Duration Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDurationUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String DURATION_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDurationUnitOfMeasure() <em>Duration Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDurationUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String durationUnitOfMeasure = DURATION_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected static final OperationsType OPERATIONS_TYPE_EDEFAULT = OperationsType.PRODUCTION;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsType operationsType = OPERATIONS_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorkDefinitionId() <em>Work Definition Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkDefinitionId()
	 * @generated
	 * @ordered
	 */
	protected static final String WORK_DEFINITION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWorkDefinitionId() <em>Work Definition Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkDefinitionId()
	 * @generated
	 * @ordered
	 */
	protected String workDefinitionId = WORK_DEFINITION_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsSegmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.OPERATIONS_SEGMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessSegment> getProcessSegments() {
		if (processSegments == null) {
			processSegments = new EObjectResolvingEList<ProcessSegment>(ProcessSegment.class, this,
					Iec62264Package.OPERATIONS_SEGMENT__PROCESS_SEGMENTS);
		}
		return processSegments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsSegment> getSubSegments() {
		if (subSegments == null) {
			subSegments = new EObjectResolvingEList<OperationsSegment>(OperationsSegment.class, this,
					Iec62264Package.OPERATIONS_SEGMENT__SUB_SEGMENTS);
		}
		return subSegments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterSpecification> getParameterSpecifications() {
		if (parameterSpecifications == null) {
			parameterSpecifications = new EObjectContainmentEList<ParameterSpecification>(ParameterSpecification.class,
					this, Iec62264Package.OPERATIONS_SEGMENT__PARAMETER_SPECIFICATIONS);
		}
		return parameterSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelSpecification> getPersonnelSpecifications() {
		if (personnelSpecifications == null) {
			personnelSpecifications = new EObjectContainmentEList<PersonnelSpecification>(PersonnelSpecification.class,
					this, Iec62264Package.OPERATIONS_SEGMENT__PERSONNEL_SPECIFICATIONS);
		}
		return personnelSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentSpecification> getEquipmentSpecifications() {
		if (equipmentSpecifications == null) {
			equipmentSpecifications = new EObjectContainmentEList<EquipmentSpecification>(EquipmentSpecification.class,
					this, Iec62264Package.OPERATIONS_SEGMENT__EQUIPMENT_SPECIFICATIONS);
		}
		return equipmentSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetSpecification> getPhysicalAssetSpecifications() {
		if (physicalAssetSpecifications == null) {
			physicalAssetSpecifications = new EObjectContainmentEList<PhysicalAssetSpecification>(
					PhysicalAssetSpecification.class, this,
					Iec62264Package.OPERATIONS_SEGMENT__PHYSICAL_ASSET_SPECIFICATIONS);
		}
		return physicalAssetSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSpecification> getMaterialSpecifications() {
		if (materialSpecifications == null) {
			materialSpecifications = new EObjectContainmentEList<MaterialSpecification>(MaterialSpecification.class,
					this, Iec62264Package.OPERATIONS_SEGMENT__MATERIAL_SPECIFICATIONS);
		}
		return materialSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_SEGMENT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_SEGMENT__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getDuration() {
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDuration(BigDecimal newDuration) {
		BigDecimal oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_SEGMENT__DURATION,
					oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDurationUnitOfMeasure() {
		return durationUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDurationUnitOfMeasure(String newDurationUnitOfMeasure) {
		String oldDurationUnitOfMeasure = durationUnitOfMeasure;
		durationUnitOfMeasure = newDurationUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_SEGMENT__DURATION_UNIT_OF_MEASURE, oldDurationUnitOfMeasure,
					durationUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsType newOperationsType) {
		OperationsType oldOperationsType = operationsType;
		operationsType = newOperationsType == null ? OPERATIONS_TYPE_EDEFAULT : newOperationsType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_SEGMENT__OPERATIONS_TYPE,
					oldOperationsType, operationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getWorkDefinitionId() {
		return workDefinitionId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorkDefinitionId(String newWorkDefinitionId) {
		String oldWorkDefinitionId = workDefinitionId;
		workDefinitionId = newWorkDefinitionId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_SEGMENT__WORK_DEFINITION_ID, oldWorkDefinitionId, workDefinitionId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SEGMENT__PARAMETER_SPECIFICATIONS:
			return ((InternalEList<?>) getParameterSpecifications()).basicRemove(otherEnd, msgs);
		case Iec62264Package.OPERATIONS_SEGMENT__PERSONNEL_SPECIFICATIONS:
			return ((InternalEList<?>) getPersonnelSpecifications()).basicRemove(otherEnd, msgs);
		case Iec62264Package.OPERATIONS_SEGMENT__EQUIPMENT_SPECIFICATIONS:
			return ((InternalEList<?>) getEquipmentSpecifications()).basicRemove(otherEnd, msgs);
		case Iec62264Package.OPERATIONS_SEGMENT__PHYSICAL_ASSET_SPECIFICATIONS:
			return ((InternalEList<?>) getPhysicalAssetSpecifications()).basicRemove(otherEnd, msgs);
		case Iec62264Package.OPERATIONS_SEGMENT__MATERIAL_SPECIFICATIONS:
			return ((InternalEList<?>) getMaterialSpecifications()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SEGMENT__PROCESS_SEGMENTS:
			return getProcessSegments();
		case Iec62264Package.OPERATIONS_SEGMENT__SUB_SEGMENTS:
			return getSubSegments();
		case Iec62264Package.OPERATIONS_SEGMENT__PARAMETER_SPECIFICATIONS:
			return getParameterSpecifications();
		case Iec62264Package.OPERATIONS_SEGMENT__PERSONNEL_SPECIFICATIONS:
			return getPersonnelSpecifications();
		case Iec62264Package.OPERATIONS_SEGMENT__EQUIPMENT_SPECIFICATIONS:
			return getEquipmentSpecifications();
		case Iec62264Package.OPERATIONS_SEGMENT__PHYSICAL_ASSET_SPECIFICATIONS:
			return getPhysicalAssetSpecifications();
		case Iec62264Package.OPERATIONS_SEGMENT__MATERIAL_SPECIFICATIONS:
			return getMaterialSpecifications();
		case Iec62264Package.OPERATIONS_SEGMENT__ID:
			return getId();
		case Iec62264Package.OPERATIONS_SEGMENT__DESCRIPTION:
			return getDescription();
		case Iec62264Package.OPERATIONS_SEGMENT__DURATION:
			return getDuration();
		case Iec62264Package.OPERATIONS_SEGMENT__DURATION_UNIT_OF_MEASURE:
			return getDurationUnitOfMeasure();
		case Iec62264Package.OPERATIONS_SEGMENT__OPERATIONS_TYPE:
			return getOperationsType();
		case Iec62264Package.OPERATIONS_SEGMENT__WORK_DEFINITION_ID:
			return getWorkDefinitionId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SEGMENT__PROCESS_SEGMENTS:
			getProcessSegments().clear();
			getProcessSegments().addAll((Collection<? extends ProcessSegment>) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__SUB_SEGMENTS:
			getSubSegments().clear();
			getSubSegments().addAll((Collection<? extends OperationsSegment>) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__PARAMETER_SPECIFICATIONS:
			getParameterSpecifications().clear();
			getParameterSpecifications().addAll((Collection<? extends ParameterSpecification>) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__PERSONNEL_SPECIFICATIONS:
			getPersonnelSpecifications().clear();
			getPersonnelSpecifications().addAll((Collection<? extends PersonnelSpecification>) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__EQUIPMENT_SPECIFICATIONS:
			getEquipmentSpecifications().clear();
			getEquipmentSpecifications().addAll((Collection<? extends EquipmentSpecification>) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__PHYSICAL_ASSET_SPECIFICATIONS:
			getPhysicalAssetSpecifications().clear();
			getPhysicalAssetSpecifications().addAll((Collection<? extends PhysicalAssetSpecification>) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__MATERIAL_SPECIFICATIONS:
			getMaterialSpecifications().clear();
			getMaterialSpecifications().addAll((Collection<? extends MaterialSpecification>) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__DURATION:
			setDuration((BigDecimal) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__DURATION_UNIT_OF_MEASURE:
			setDurationUnitOfMeasure((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__OPERATIONS_TYPE:
			setOperationsType((OperationsType) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__WORK_DEFINITION_ID:
			setWorkDefinitionId((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SEGMENT__PROCESS_SEGMENTS:
			getProcessSegments().clear();
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__SUB_SEGMENTS:
			getSubSegments().clear();
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__PARAMETER_SPECIFICATIONS:
			getParameterSpecifications().clear();
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__PERSONNEL_SPECIFICATIONS:
			getPersonnelSpecifications().clear();
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__EQUIPMENT_SPECIFICATIONS:
			getEquipmentSpecifications().clear();
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__PHYSICAL_ASSET_SPECIFICATIONS:
			getPhysicalAssetSpecifications().clear();
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__MATERIAL_SPECIFICATIONS:
			getMaterialSpecifications().clear();
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__DURATION:
			setDuration(DURATION_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__DURATION_UNIT_OF_MEASURE:
			setDurationUnitOfMeasure(DURATION_UNIT_OF_MEASURE_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__OPERATIONS_TYPE:
			setOperationsType(OPERATIONS_TYPE_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT__WORK_DEFINITION_ID:
			setWorkDefinitionId(WORK_DEFINITION_ID_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SEGMENT__PROCESS_SEGMENTS:
			return processSegments != null && !processSegments.isEmpty();
		case Iec62264Package.OPERATIONS_SEGMENT__SUB_SEGMENTS:
			return subSegments != null && !subSegments.isEmpty();
		case Iec62264Package.OPERATIONS_SEGMENT__PARAMETER_SPECIFICATIONS:
			return parameterSpecifications != null && !parameterSpecifications.isEmpty();
		case Iec62264Package.OPERATIONS_SEGMENT__PERSONNEL_SPECIFICATIONS:
			return personnelSpecifications != null && !personnelSpecifications.isEmpty();
		case Iec62264Package.OPERATIONS_SEGMENT__EQUIPMENT_SPECIFICATIONS:
			return equipmentSpecifications != null && !equipmentSpecifications.isEmpty();
		case Iec62264Package.OPERATIONS_SEGMENT__PHYSICAL_ASSET_SPECIFICATIONS:
			return physicalAssetSpecifications != null && !physicalAssetSpecifications.isEmpty();
		case Iec62264Package.OPERATIONS_SEGMENT__MATERIAL_SPECIFICATIONS:
			return materialSpecifications != null && !materialSpecifications.isEmpty();
		case Iec62264Package.OPERATIONS_SEGMENT__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.OPERATIONS_SEGMENT__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.OPERATIONS_SEGMENT__DURATION:
			return DURATION_EDEFAULT == null ? duration != null : !DURATION_EDEFAULT.equals(duration);
		case Iec62264Package.OPERATIONS_SEGMENT__DURATION_UNIT_OF_MEASURE:
			return DURATION_UNIT_OF_MEASURE_EDEFAULT == null ? durationUnitOfMeasure != null
					: !DURATION_UNIT_OF_MEASURE_EDEFAULT.equals(durationUnitOfMeasure);
		case Iec62264Package.OPERATIONS_SEGMENT__OPERATIONS_TYPE:
			return operationsType != OPERATIONS_TYPE_EDEFAULT;
		case Iec62264Package.OPERATIONS_SEGMENT__WORK_DEFINITION_ID:
			return WORK_DEFINITION_ID_EDEFAULT == null ? workDefinitionId != null
					: !WORK_DEFINITION_ID_EDEFAULT.equals(workDefinitionId);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", duration: ");
		result.append(duration);
		result.append(", durationUnitOfMeasure: ");
		result.append(durationUnitOfMeasure);
		result.append(", operationsType: ");
		result.append(operationsType);
		result.append(", workDefinitionId: ");
		result.append(workDefinitionId);
		result.append(')');
		return result.toString();
	}

} //OperationsSegmentImpl
