/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialRequirementProperties <em>Material Requirement Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getAssembledFromRequirements <em>Assembled From Requirements</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialClasses <em>Material Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialDefinitions <em>Material Definitions</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialLots <em>Material Lots</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialSublot <em>Material Sublot</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getStorageLocation <em>Storage Location</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement()
 * @model
 * @generated
 */
public interface MaterialRequirement extends EObject {
	/**
	 * Returns the value of the '<em><b>Material Requirement Properties</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialRequirementProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Requirement Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Requirement Properties</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_MaterialRequirementProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<MaterialRequirementProperty> getMaterialRequirementProperties();

	/**
	 * Returns the value of the '<em><b>Assembled From Requirements</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembled From Requirements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembled From Requirements</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_AssembledFromRequirements()
	 * @model
	 * @generated
	 */
	EList<MaterialRequirement> getAssembledFromRequirements();

	/**
	 * Returns the value of the '<em><b>Material Classes</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Classes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Classes</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_MaterialClasses()
	 * @model
	 * @generated
	 */
	EList<MaterialClass> getMaterialClasses();

	/**
	 * Returns the value of the '<em><b>Material Definitions</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Definitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Definitions</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_MaterialDefinitions()
	 * @model
	 * @generated
	 */
	EList<MaterialDefinition> getMaterialDefinitions();

	/**
	 * Returns the value of the '<em><b>Material Lots</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialLot}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Lots</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Lots</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_MaterialLots()
	 * @model
	 * @generated
	 */
	EList<MaterialLot> getMaterialLots();

	/**
	 * Returns the value of the '<em><b>Material Sublot</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialSublot}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Sublot</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Sublot</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_MaterialSublot()
	 * @model
	 * @generated
	 */
	EList<MaterialSublot> getMaterialSublot();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Material Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Use</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Use</em>' attribute.
	 * @see #setMaterialUse(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_MaterialUse()
	 * @model
	 * @generated
	 */
	String getMaterialUse();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialUse <em>Material Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Use</em>' attribute.
	 * @see #getMaterialUse()
	 * @generated
	 */
	void setMaterialUse(String value);

	/**
	 * Returns the value of the '<em><b>Storage Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Location</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Location</em>' attribute.
	 * @see #setStorageLocation(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_StorageLocation()
	 * @model
	 * @generated
	 */
	String getStorageLocation();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getStorageLocation <em>Storage Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Location</em>' attribute.
	 * @see #getStorageLocation()
	 * @generated
	 */
	void setStorageLocation(String value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(BigDecimal)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_Quantity()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 * @generated
	 */
	BigDecimal getQuantity();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getQuantity <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity Unit Of Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity Unit Of Measure</em>' attribute.
	 * @see #setQuantityUnitOfMeasure(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_QuantityUnitOfMeasure()
	 * @model
	 * @generated
	 */
	String getQuantityUnitOfMeasure();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity Unit Of Measure</em>' attribute.
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 */
	void setQuantityUnitOfMeasure(String value);

	/**
	 * Returns the value of the '<em><b>Assembly Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.AssemblyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyType
	 * @see #setAssemblyType(AssemblyType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_AssemblyType()
	 * @model
	 * @generated
	 */
	AssemblyType getAssemblyType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getAssemblyType <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyType
	 * @see #getAssemblyType()
	 * @generated
	 */
	void setAssemblyType(AssemblyType value);

	/**
	 * Returns the value of the '<em><b>Assembly Relationship</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.AssemblyRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Relationship</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Relationship</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyRelationship
	 * @see #setAssemblyRelationship(AssemblyRelationship)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialRequirement_AssemblyRelationship()
	 * @model
	 * @generated
	 */
	AssemblyRelationship getAssemblyRelationship();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getAssemblyRelationship <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Relationship</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyRelationship
	 * @see #getAssemblyRelationship()
	 * @generated
	 */
	void setAssemblyRelationship(AssemblyRelationship value);

} // MaterialRequirement
