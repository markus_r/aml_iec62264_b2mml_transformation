/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Personnel Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersonnelRequirementProperties <em>Personnel Requirement Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersonnelClasses <em>Personnel Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersons <em>Persons</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersonnelUse <em>Personnel Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPersonnelRequirement()
 * @model
 * @generated
 */
public interface PersonnelRequirement extends EObject {
	/**
	 * Returns the value of the '<em><b>Personnel Requirement Properties</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirementProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Requirement Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Requirement Properties</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPersonnelRequirement_PersonnelRequirementProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<PersonnelRequirementProperty> getPersonnelRequirementProperties();

	/**
	 * Returns the value of the '<em><b>Personnel Classes</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PersonnelClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Classes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Classes</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPersonnelRequirement_PersonnelClasses()
	 * @model
	 * @generated
	 */
	EList<PersonnelClass> getPersonnelClasses();

	/**
	 * Returns the value of the '<em><b>Persons</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.Person}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Persons</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Persons</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPersonnelRequirement_Persons()
	 * @model
	 * @generated
	 */
	EList<Person> getPersons();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPersonnelRequirement_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Personnel Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Use</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Use</em>' attribute.
	 * @see #setPersonnelUse(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPersonnelRequirement_PersonnelUse()
	 * @model
	 * @generated
	 */
	String getPersonnelUse();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersonnelUse <em>Personnel Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Personnel Use</em>' attribute.
	 * @see #getPersonnelUse()
	 * @generated
	 */
	void setPersonnelUse(String value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(BigDecimal)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPersonnelRequirement_Quantity()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 * @generated
	 */
	BigDecimal getQuantity();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getQuantity <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity Unit Of Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity Unit Of Measure</em>' attribute.
	 * @see #setQuantityUnitOfMeasure(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPersonnelRequirement_QuantityUnitOfMeasure()
	 * @model
	 * @generated
	 */
	String getQuantityUnitOfMeasure();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity Unit Of Measure</em>' attribute.
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 */
	void setQuantityUnitOfMeasure(String value);

} // PersonnelRequirement
