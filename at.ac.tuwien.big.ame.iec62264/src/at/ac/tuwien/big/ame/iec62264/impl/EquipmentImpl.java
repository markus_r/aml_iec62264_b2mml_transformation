/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Equipment;
import at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification;
import at.ac.tuwien.big.ame.iec62264.EquipmentClass;
import at.ac.tuwien.big.ame.iec62264.EquipmentProperty;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentImpl#getChildren <em>Children</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentImpl#getTestSpecifications <em>Test Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentImpl#getEquipmentClass <em>Equipment Class</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentImpl#getEquipmentLevel <em>Equipment Level</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EquipmentImpl extends MinimalEObjectImpl.Container implements Equipment {
	/**
	 * The cached value of the '{@link #getChildren() <em>Children</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildren()
	 * @generated
	 * @ordered
	 */
	protected EList<Equipment> children;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentProperty> properties;

	/**
	 * The cached value of the '{@link #getTestSpecifications() <em>Test Specifications</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentCapabilityTestSpecification> testSpecifications;

	/**
	 * The cached value of the '{@link #getEquipmentClass() <em>Equipment Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentClass()
	 * @generated
	 * @ordered
	 */
	protected EquipmentClass equipmentClass;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getEquipmentLevel() <em>Equipment Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentLevel()
	 * @generated
	 * @ordered
	 */
	protected static final String EQUIPMENT_LEVEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEquipmentLevel() <em>Equipment Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentLevel()
	 * @generated
	 * @ordered
	 */
	protected String equipmentLevel = EQUIPMENT_LEVEL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.EQUIPMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Equipment> getChildren() {
		if (children == null) {
			children = new EObjectResolvingEList<Equipment>(Equipment.class, this, Iec62264Package.EQUIPMENT__CHILDREN);
		}
		return children;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentProperty> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<EquipmentProperty>(EquipmentProperty.class, this,
					Iec62264Package.EQUIPMENT__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentCapabilityTestSpecification> getTestSpecifications() {
		if (testSpecifications == null) {
			testSpecifications = new EObjectResolvingEList<EquipmentCapabilityTestSpecification>(
					EquipmentCapabilityTestSpecification.class, this, Iec62264Package.EQUIPMENT__TEST_SPECIFICATIONS);
		}
		return testSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClass getEquipmentClass() {
		if (equipmentClass != null && equipmentClass.eIsProxy()) {
			InternalEObject oldEquipmentClass = (InternalEObject) equipmentClass;
			equipmentClass = (EquipmentClass) eResolveProxy(oldEquipmentClass);
			if (equipmentClass != oldEquipmentClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.EQUIPMENT__EQUIPMENT_CLASS, oldEquipmentClass, equipmentClass));
			}
		}
		return equipmentClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClass basicGetEquipmentClass() {
		return equipmentClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentClass(EquipmentClass newEquipmentClass) {
		EquipmentClass oldEquipmentClass = equipmentClass;
		equipmentClass = newEquipmentClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT__EQUIPMENT_CLASS,
					oldEquipmentClass, equipmentClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEquipmentLevel() {
		return equipmentLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentLevel(String newEquipmentLevel) {
		String oldEquipmentLevel = equipmentLevel;
		equipmentLevel = newEquipmentLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT__EQUIPMENT_LEVEL,
					oldEquipmentLevel, equipmentLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT__PROPERTIES:
			return ((InternalEList<?>) getProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT__CHILDREN:
			return getChildren();
		case Iec62264Package.EQUIPMENT__PROPERTIES:
			return getProperties();
		case Iec62264Package.EQUIPMENT__TEST_SPECIFICATIONS:
			return getTestSpecifications();
		case Iec62264Package.EQUIPMENT__EQUIPMENT_CLASS:
			if (resolve)
				return getEquipmentClass();
			return basicGetEquipmentClass();
		case Iec62264Package.EQUIPMENT__ID:
			return getId();
		case Iec62264Package.EQUIPMENT__DESCRIPTION:
			return getDescription();
		case Iec62264Package.EQUIPMENT__EQUIPMENT_LEVEL:
			return getEquipmentLevel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT__CHILDREN:
			getChildren().clear();
			getChildren().addAll((Collection<? extends Equipment>) newValue);
			return;
		case Iec62264Package.EQUIPMENT__PROPERTIES:
			getProperties().clear();
			getProperties().addAll((Collection<? extends EquipmentProperty>) newValue);
			return;
		case Iec62264Package.EQUIPMENT__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			getTestSpecifications().addAll((Collection<? extends EquipmentCapabilityTestSpecification>) newValue);
			return;
		case Iec62264Package.EQUIPMENT__EQUIPMENT_CLASS:
			setEquipmentClass((EquipmentClass) newValue);
			return;
		case Iec62264Package.EQUIPMENT__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.EQUIPMENT__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.EQUIPMENT__EQUIPMENT_LEVEL:
			setEquipmentLevel((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT__CHILDREN:
			getChildren().clear();
			return;
		case Iec62264Package.EQUIPMENT__PROPERTIES:
			getProperties().clear();
			return;
		case Iec62264Package.EQUIPMENT__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			return;
		case Iec62264Package.EQUIPMENT__EQUIPMENT_CLASS:
			setEquipmentClass((EquipmentClass) null);
			return;
		case Iec62264Package.EQUIPMENT__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT__EQUIPMENT_LEVEL:
			setEquipmentLevel(EQUIPMENT_LEVEL_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT__CHILDREN:
			return children != null && !children.isEmpty();
		case Iec62264Package.EQUIPMENT__PROPERTIES:
			return properties != null && !properties.isEmpty();
		case Iec62264Package.EQUIPMENT__TEST_SPECIFICATIONS:
			return testSpecifications != null && !testSpecifications.isEmpty();
		case Iec62264Package.EQUIPMENT__EQUIPMENT_CLASS:
			return equipmentClass != null;
		case Iec62264Package.EQUIPMENT__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.EQUIPMENT__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.EQUIPMENT__EQUIPMENT_LEVEL:
			return EQUIPMENT_LEVEL_EDEFAULT == null ? equipmentLevel != null
					: !EQUIPMENT_LEVEL_EDEFAULT.equals(equipmentLevel);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", equipmentLevel: ");
		result.append(equipmentLevel);
		result.append(')');
		return result.toString();
	}

} //EquipmentImpl
