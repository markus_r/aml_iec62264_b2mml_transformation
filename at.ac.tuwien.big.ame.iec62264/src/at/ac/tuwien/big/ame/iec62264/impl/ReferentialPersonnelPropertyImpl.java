/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.PersonProperty;
import at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty;
import at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Referential Personnel Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialPersonnelPropertyImpl#getPersonnelClassProperty <em>Personnel Class Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialPersonnelPropertyImpl#getPersonProperty <em>Person Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ReferentialPersonnelPropertyImpl extends ReferencialPropertyImpl
		implements ReferentialPersonnelProperty {
	/**
	 * The cached value of the '{@link #getPersonnelClassProperty() <em>Personnel Class Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelClassProperty()
	 * @generated
	 * @ordered
	 */
	protected PersonnelClassProperty personnelClassProperty;

	/**
	 * The cached value of the '{@link #getPersonProperty() <em>Person Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonProperty()
	 * @generated
	 * @ordered
	 */
	protected PersonProperty personProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferentialPersonnelPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.REFERENTIAL_PERSONNEL_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelClassProperty getPersonnelClassProperty() {
		if (personnelClassProperty != null && personnelClassProperty.eIsProxy()) {
			InternalEObject oldPersonnelClassProperty = (InternalEObject) personnelClassProperty;
			personnelClassProperty = (PersonnelClassProperty) eResolveProxy(oldPersonnelClassProperty);
			if (personnelClassProperty != oldPersonnelClassProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY,
							oldPersonnelClassProperty, personnelClassProperty));
			}
		}
		return personnelClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelClassProperty basicGetPersonnelClassProperty() {
		return personnelClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonnelClassProperty(PersonnelClassProperty newPersonnelClassProperty) {
		PersonnelClassProperty oldPersonnelClassProperty = personnelClassProperty;
		personnelClassProperty = newPersonnelClassProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY, oldPersonnelClassProperty,
					personnelClassProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonProperty getPersonProperty() {
		if (personProperty != null && personProperty.eIsProxy()) {
			InternalEObject oldPersonProperty = (InternalEObject) personProperty;
			personProperty = (PersonProperty) eResolveProxy(oldPersonProperty);
			if (personProperty != oldPersonProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY, oldPersonProperty,
							personProperty));
			}
		}
		return personProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonProperty basicGetPersonProperty() {
		return personProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonProperty(PersonProperty newPersonProperty) {
		PersonProperty oldPersonProperty = personProperty;
		personProperty = newPersonProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY, oldPersonProperty,
					personProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY:
			if (resolve)
				return getPersonnelClassProperty();
			return basicGetPersonnelClassProperty();
		case Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY:
			if (resolve)
				return getPersonProperty();
			return basicGetPersonProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY:
			setPersonnelClassProperty((PersonnelClassProperty) newValue);
			return;
		case Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY:
			setPersonProperty((PersonProperty) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY:
			setPersonnelClassProperty((PersonnelClassProperty) null);
			return;
		case Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY:
			setPersonProperty((PersonProperty) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY:
			return personnelClassProperty != null;
		case Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY:
			return personProperty != null;
		}
		return super.eIsSet(featureID);
	}

} //ReferentialPersonnelPropertyImpl
