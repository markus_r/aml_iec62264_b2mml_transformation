/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process Segment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getChildren <em>Children</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getProcessSegmentParameters <em>Process Segment Parameters</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getPersonnelSegmentSpecifications <em>Personnel Segment Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getEquipmentSegmentSpecifications <em>Equipment Segment Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getPhysicalAssetSegmentSpecifications <em>Physical Asset Segment Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getMaterialSegmentSpecifications <em>Material Segment Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getDuration <em>Duration</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getDurationUnitOfMeasure <em>Duration Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment()
 * @model
 * @generated
 */
public interface ProcessSegment extends EObject {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.ProcessSegment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment_Children()
	 * @model
	 * @generated
	 */
	EList<ProcessSegment> getChildren();

	/**
	 * Returns the value of the '<em><b>Process Segment Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment Parameters</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment_ProcessSegmentParameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProcessSegmentParameter> getProcessSegmentParameters();

	/**
	 * Returns the value of the '<em><b>Personnel Segment Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Segment Specifications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Segment Specifications</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment_PersonnelSegmentSpecifications()
	 * @model containment="true"
	 * @generated
	 */
	EList<PersonnelSegmentSpecification> getPersonnelSegmentSpecifications();

	/**
	 * Returns the value of the '<em><b>Equipment Segment Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Segment Specifications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Segment Specifications</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment_EquipmentSegmentSpecifications()
	 * @model containment="true"
	 * @generated
	 */
	EList<EquipmentSegmentSpecification> getEquipmentSegmentSpecifications();

	/**
	 * Returns the value of the '<em><b>Physical Asset Segment Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Segment Specifications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Segment Specifications</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment_PhysicalAssetSegmentSpecifications()
	 * @model containment="true"
	 * @generated
	 */
	EList<PhysicalAssetSegmentSpecification> getPhysicalAssetSegmentSpecifications();

	/**
	 * Returns the value of the '<em><b>Material Segment Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Segment Specifications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Segment Specifications</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment_MaterialSegmentSpecifications()
	 * @model containment="true"
	 * @generated
	 */
	EList<MaterialSegmentSpecification> getMaterialSegmentSpecifications();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.OperationsType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #setOperationsType(OperationsType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment_OperationsType()
	 * @model
	 * @generated
	 */
	OperationsType getOperationsType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getOperationsType <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsType value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(BigDecimal)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment_Duration()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 * @generated
	 */
	BigDecimal getDuration();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Duration Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration Unit Of Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration Unit Of Measure</em>' attribute.
	 * @see #setDurationUnitOfMeasure(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment_DurationUnitOfMeasure()
	 * @model
	 * @generated
	 */
	String getDurationUnitOfMeasure();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getDurationUnitOfMeasure <em>Duration Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration Unit Of Measure</em>' attribute.
	 * @see #getDurationUnitOfMeasure()
	 * @generated
	 */
	void setDurationUnitOfMeasure(String value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #setHierarchyScope(HierarchyScope)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegment_HierarchyScope()
	 * @model
	 * @generated
	 */
	HierarchyScope getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getHierarchyScope <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScope value);

} // ProcessSegment
