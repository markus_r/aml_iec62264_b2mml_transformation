/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.OperationsCapabilityModel;
import at.ac.tuwien.big.ame.iec62264.Person;
import at.ac.tuwien.big.ame.iec62264.PersonnelClass;
import at.ac.tuwien.big.ame.iec62264.PersonnelModel;
import at.ac.tuwien.big.ame.iec62264.QualificationTestResult;
import at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Personnel Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelModelImpl#getPersonnelClasses <em>Personnel Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelModelImpl#getPersons <em>Persons</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelModelImpl#getTestSpecifications <em>Test Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelModelImpl#getTestResults <em>Test Results</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelModelImpl#getOperationsCapabilityModel <em>Operations Capability Model</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonnelModelImpl extends MinimalEObjectImpl.Container implements PersonnelModel {
	/**
	 * The cached value of the '{@link #getPersonnelClasses() <em>Personnel Classes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelClass> personnelClasses;

	/**
	 * The cached value of the '{@link #getPersons() <em>Persons</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersons()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> persons;

	/**
	 * The cached value of the '{@link #getTestSpecifications() <em>Test Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<QualificationTestSpecification> testSpecifications;

	/**
	 * The cached value of the '{@link #getTestResults() <em>Test Results</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestResults()
	 * @generated
	 * @ordered
	 */
	protected EList<QualificationTestResult> testResults;

	/**
	 * The cached value of the '{@link #getOperationsCapabilityModel() <em>Operations Capability Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsCapabilityModel()
	 * @generated
	 * @ordered
	 */
	protected OperationsCapabilityModel operationsCapabilityModel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonnelModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PERSONNEL_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelClass> getPersonnelClasses() {
		if (personnelClasses == null) {
			personnelClasses = new EObjectContainmentEList<PersonnelClass>(PersonnelClass.class, this,
					Iec62264Package.PERSONNEL_MODEL__PERSONNEL_CLASSES);
		}
		return personnelClasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Person> getPersons() {
		if (persons == null) {
			persons = new EObjectContainmentEList<Person>(Person.class, this, Iec62264Package.PERSONNEL_MODEL__PERSONS);
		}
		return persons;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QualificationTestSpecification> getTestSpecifications() {
		if (testSpecifications == null) {
			testSpecifications = new EObjectContainmentEList<QualificationTestSpecification>(
					QualificationTestSpecification.class, this, Iec62264Package.PERSONNEL_MODEL__TEST_SPECIFICATIONS);
		}
		return testSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QualificationTestResult> getTestResults() {
		if (testResults == null) {
			testResults = new EObjectContainmentEList<QualificationTestResult>(QualificationTestResult.class, this,
					Iec62264Package.PERSONNEL_MODEL__TEST_RESULTS);
		}
		return testResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsCapabilityModel getOperationsCapabilityModel() {
		return operationsCapabilityModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsCapabilityModel(OperationsCapabilityModel newOperationsCapabilityModel,
			NotificationChain msgs) {
		OperationsCapabilityModel oldOperationsCapabilityModel = operationsCapabilityModel;
		operationsCapabilityModel = newOperationsCapabilityModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PERSONNEL_MODEL__OPERATIONS_CAPABILITY_MODEL, oldOperationsCapabilityModel,
					newOperationsCapabilityModel);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsCapabilityModel(OperationsCapabilityModel newOperationsCapabilityModel) {
		if (newOperationsCapabilityModel != operationsCapabilityModel) {
			NotificationChain msgs = null;
			if (operationsCapabilityModel != null)
				msgs = ((InternalEObject) operationsCapabilityModel).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.PERSONNEL_MODEL__OPERATIONS_CAPABILITY_MODEL, null,
						msgs);
			if (newOperationsCapabilityModel != null)
				msgs = ((InternalEObject) newOperationsCapabilityModel).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.PERSONNEL_MODEL__OPERATIONS_CAPABILITY_MODEL, null,
						msgs);
			msgs = basicSetOperationsCapabilityModel(newOperationsCapabilityModel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PERSONNEL_MODEL__OPERATIONS_CAPABILITY_MODEL, newOperationsCapabilityModel,
					newOperationsCapabilityModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_MODEL__PERSONNEL_CLASSES:
			return ((InternalEList<?>) getPersonnelClasses()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PERSONNEL_MODEL__PERSONS:
			return ((InternalEList<?>) getPersons()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PERSONNEL_MODEL__TEST_SPECIFICATIONS:
			return ((InternalEList<?>) getTestSpecifications()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PERSONNEL_MODEL__TEST_RESULTS:
			return ((InternalEList<?>) getTestResults()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PERSONNEL_MODEL__OPERATIONS_CAPABILITY_MODEL:
			return basicSetOperationsCapabilityModel(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_MODEL__PERSONNEL_CLASSES:
			return getPersonnelClasses();
		case Iec62264Package.PERSONNEL_MODEL__PERSONS:
			return getPersons();
		case Iec62264Package.PERSONNEL_MODEL__TEST_SPECIFICATIONS:
			return getTestSpecifications();
		case Iec62264Package.PERSONNEL_MODEL__TEST_RESULTS:
			return getTestResults();
		case Iec62264Package.PERSONNEL_MODEL__OPERATIONS_CAPABILITY_MODEL:
			return getOperationsCapabilityModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_MODEL__PERSONNEL_CLASSES:
			getPersonnelClasses().clear();
			getPersonnelClasses().addAll((Collection<? extends PersonnelClass>) newValue);
			return;
		case Iec62264Package.PERSONNEL_MODEL__PERSONS:
			getPersons().clear();
			getPersons().addAll((Collection<? extends Person>) newValue);
			return;
		case Iec62264Package.PERSONNEL_MODEL__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			getTestSpecifications().addAll((Collection<? extends QualificationTestSpecification>) newValue);
			return;
		case Iec62264Package.PERSONNEL_MODEL__TEST_RESULTS:
			getTestResults().clear();
			getTestResults().addAll((Collection<? extends QualificationTestResult>) newValue);
			return;
		case Iec62264Package.PERSONNEL_MODEL__OPERATIONS_CAPABILITY_MODEL:
			setOperationsCapabilityModel((OperationsCapabilityModel) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_MODEL__PERSONNEL_CLASSES:
			getPersonnelClasses().clear();
			return;
		case Iec62264Package.PERSONNEL_MODEL__PERSONS:
			getPersons().clear();
			return;
		case Iec62264Package.PERSONNEL_MODEL__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			return;
		case Iec62264Package.PERSONNEL_MODEL__TEST_RESULTS:
			getTestResults().clear();
			return;
		case Iec62264Package.PERSONNEL_MODEL__OPERATIONS_CAPABILITY_MODEL:
			setOperationsCapabilityModel((OperationsCapabilityModel) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_MODEL__PERSONNEL_CLASSES:
			return personnelClasses != null && !personnelClasses.isEmpty();
		case Iec62264Package.PERSONNEL_MODEL__PERSONS:
			return persons != null && !persons.isEmpty();
		case Iec62264Package.PERSONNEL_MODEL__TEST_SPECIFICATIONS:
			return testSpecifications != null && !testSpecifications.isEmpty();
		case Iec62264Package.PERSONNEL_MODEL__TEST_RESULTS:
			return testResults != null && !testResults.isEmpty();
		case Iec62264Package.PERSONNEL_MODEL__OPERATIONS_CAPABILITY_MODEL:
			return operationsCapabilityModel != null;
		}
		return super.eIsSet(featureID);
	}

} //PersonnelModelImpl
