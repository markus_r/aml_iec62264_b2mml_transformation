/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package
 * @generated
 */
public interface Iec62264Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Iec62264Factory eINSTANCE = at.ac.tuwien.big.ame.iec62264.impl.Iec62264FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Person</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Person</em>'.
	 * @generated
	 */
	Person createPerson();

	/**
	 * Returns a new object of class '<em>Personnel Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Class</em>'.
	 * @generated
	 */
	PersonnelClass createPersonnelClass();

	/**
	 * Returns a new object of class '<em>Qualification Test Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Qualification Test Specification</em>'.
	 * @generated
	 */
	QualificationTestSpecification createQualificationTestSpecification();

	/**
	 * Returns a new object of class '<em>Personnel Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Class Property</em>'.
	 * @generated
	 */
	PersonnelClassProperty createPersonnelClassProperty();

	/**
	 * Returns a new object of class '<em>Person Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Person Property</em>'.
	 * @generated
	 */
	PersonProperty createPersonProperty();

	/**
	 * Returns a new object of class '<em>Qualification Test Result</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Qualification Test Result</em>'.
	 * @generated
	 */
	QualificationTestResult createQualificationTestResult();

	/**
	 * Returns a new object of class '<em>Personnel Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Model</em>'.
	 * @generated
	 */
	PersonnelModel createPersonnelModel();

	/**
	 * Returns a new object of class '<em>Equipment Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Model</em>'.
	 * @generated
	 */
	EquipmentModel createEquipmentModel();

	/**
	 * Returns a new object of class '<em>Equipment Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Class</em>'.
	 * @generated
	 */
	EquipmentClass createEquipmentClass();

	/**
	 * Returns a new object of class '<em>Equipment Capability Test Result</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Capability Test Result</em>'.
	 * @generated
	 */
	EquipmentCapabilityTestResult createEquipmentCapabilityTestResult();

	/**
	 * Returns a new object of class '<em>Equipment Capability Test Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Capability Test Specification</em>'.
	 * @generated
	 */
	EquipmentCapabilityTestSpecification createEquipmentCapabilityTestSpecification();

	/**
	 * Returns a new object of class '<em>Equipment Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Class Property</em>'.
	 * @generated
	 */
	EquipmentClassProperty createEquipmentClassProperty();

	/**
	 * Returns a new object of class '<em>Equipment Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Property</em>'.
	 * @generated
	 */
	EquipmentProperty createEquipmentProperty();

	/**
	 * Returns a new object of class '<em>Equipment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment</em>'.
	 * @generated
	 */
	Equipment createEquipment();

	/**
	 * Returns a new object of class '<em>Physical Asset Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Model</em>'.
	 * @generated
	 */
	PhysicalAssetModel createPhysicalAssetModel();

	/**
	 * Returns a new object of class '<em>Physical Asset Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Class</em>'.
	 * @generated
	 */
	PhysicalAssetClass createPhysicalAssetClass();

	/**
	 * Returns a new object of class '<em>Physical Asset Capability Test Result</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Capability Test Result</em>'.
	 * @generated
	 */
	PhysicalAssetCapabilityTestResult createPhysicalAssetCapabilityTestResult();

	/**
	 * Returns a new object of class '<em>Physical Asset Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Property</em>'.
	 * @generated
	 */
	PhysicalAssetProperty createPhysicalAssetProperty();

	/**
	 * Returns a new object of class '<em>Physical Asset</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset</em>'.
	 * @generated
	 */
	PhysicalAsset createPhysicalAsset();

	/**
	 * Returns a new object of class '<em>Physical Asset Capability Test Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Capability Test Specification</em>'.
	 * @generated
	 */
	PhysicalAssetCapabilityTestSpecification createPhysicalAssetCapabilityTestSpecification();

	/**
	 * Returns a new object of class '<em>Physical Asset Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Class Property</em>'.
	 * @generated
	 */
	PhysicalAssetClassProperty createPhysicalAssetClassProperty();

	/**
	 * Returns a new object of class '<em>Equipment Asset Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Asset Mapping</em>'.
	 * @generated
	 */
	EquipmentAssetMapping createEquipmentAssetMapping();

	/**
	 * Returns a new object of class '<em>Material Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Model</em>'.
	 * @generated
	 */
	MaterialModel createMaterialModel();

	/**
	 * Returns a new object of class '<em>Material Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Class</em>'.
	 * @generated
	 */
	MaterialClass createMaterialClass();

	/**
	 * Returns a new object of class '<em>Material Test Result</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Test Result</em>'.
	 * @generated
	 */
	MaterialTestResult createMaterialTestResult();

	/**
	 * Returns a new object of class '<em>Material Test Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Test Specification</em>'.
	 * @generated
	 */
	MaterialTestSpecification createMaterialTestSpecification();

	/**
	 * Returns a new object of class '<em>Material Sublot</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Sublot</em>'.
	 * @generated
	 */
	MaterialSublot createMaterialSublot();

	/**
	 * Returns a new object of class '<em>Material Lot</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Lot</em>'.
	 * @generated
	 */
	MaterialLot createMaterialLot();

	/**
	 * Returns a new object of class '<em>Material Lot Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Lot Property</em>'.
	 * @generated
	 */
	MaterialLotProperty createMaterialLotProperty();

	/**
	 * Returns a new object of class '<em>Material Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Definition</em>'.
	 * @generated
	 */
	MaterialDefinition createMaterialDefinition();

	/**
	 * Returns a new object of class '<em>Material Definition Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Definition Property</em>'.
	 * @generated
	 */
	MaterialDefinitionProperty createMaterialDefinitionProperty();

	/**
	 * Returns a new object of class '<em>Material Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Class Property</em>'.
	 * @generated
	 */
	MaterialClassProperty createMaterialClassProperty();

	/**
	 * Returns a new object of class '<em>Process Segment Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Segment Model</em>'.
	 * @generated
	 */
	ProcessSegmentModel createProcessSegmentModel();

	/**
	 * Returns a new object of class '<em>Process Segment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Segment</em>'.
	 * @generated
	 */
	ProcessSegment createProcessSegment();

	/**
	 * Returns a new object of class '<em>Personnel Segment Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Segment Specification Property</em>'.
	 * @generated
	 */
	PersonnelSegmentSpecificationProperty createPersonnelSegmentSpecificationProperty();

	/**
	 * Returns a new object of class '<em>Equipment Segment Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Segment Specification Property</em>'.
	 * @generated
	 */
	EquipmentSegmentSpecificationProperty createEquipmentSegmentSpecificationProperty();

	/**
	 * Returns a new object of class '<em>Physical Asset Segment Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Segment Specification Property</em>'.
	 * @generated
	 */
	PhysicalAssetSegmentSpecificationProperty createPhysicalAssetSegmentSpecificationProperty();

	/**
	 * Returns a new object of class '<em>Material Segment Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Segment Specification Property</em>'.
	 * @generated
	 */
	MaterialSegmentSpecificationProperty createMaterialSegmentSpecificationProperty();

	/**
	 * Returns a new object of class '<em>Material Segment Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Segment Specification</em>'.
	 * @generated
	 */
	MaterialSegmentSpecification createMaterialSegmentSpecification();

	/**
	 * Returns a new object of class '<em>Physical Asset Segment Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Segment Specification</em>'.
	 * @generated
	 */
	PhysicalAssetSegmentSpecification createPhysicalAssetSegmentSpecification();

	/**
	 * Returns a new object of class '<em>Equipment Segment Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Segment Specification</em>'.
	 * @generated
	 */
	EquipmentSegmentSpecification createEquipmentSegmentSpecification();

	/**
	 * Returns a new object of class '<em>Personnel Segment Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Segment Specification</em>'.
	 * @generated
	 */
	PersonnelSegmentSpecification createPersonnelSegmentSpecification();

	/**
	 * Returns a new object of class '<em>Process Segment Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Segment Parameter</em>'.
	 * @generated
	 */
	ProcessSegmentParameter createProcessSegmentParameter();

	/**
	 * Returns a new object of class '<em>Process Segment Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Segment Dependency</em>'.
	 * @generated
	 */
	ProcessSegmentDependency createProcessSegmentDependency();

	/**
	 * Returns a new object of class '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model</em>'.
	 * @generated
	 */
	Iec62264Model createIec62264Model();

	/**
	 * Returns a new object of class '<em>Operations Definition Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Definition Model</em>'.
	 * @generated
	 */
	OperationsDefinitionModel createOperationsDefinitionModel();

	/**
	 * Returns a new object of class '<em>Operations Schedule Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Schedule Model</em>'.
	 * @generated
	 */
	OperationsScheduleModel createOperationsScheduleModel();

	/**
	 * Returns a new object of class '<em>Operations Performance Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Performance Model</em>'.
	 * @generated
	 */
	OperationsPerformanceModel createOperationsPerformanceModel();

	/**
	 * Returns a new object of class '<em>Operations Capability Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Capability Model</em>'.
	 * @generated
	 */
	OperationsCapabilityModel createOperationsCapabilityModel();

	/**
	 * Returns a new object of class '<em>Process Segment Capability Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Segment Capability Model</em>'.
	 * @generated
	 */
	ProcessSegmentCapabilityModel createProcessSegmentCapabilityModel();

	/**
	 * Returns a new object of class '<em>Hierarchy Scope</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hierarchy Scope</em>'.
	 * @generated
	 */
	HierarchyScope createHierarchyScope();

	/**
	 * Returns a new object of class '<em>Material Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Specification Property</em>'.
	 * @generated
	 */
	MaterialSpecificationProperty createMaterialSpecificationProperty();

	/**
	 * Returns a new object of class '<em>Physical Asset Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Specification Property</em>'.
	 * @generated
	 */
	PhysicalAssetSpecificationProperty createPhysicalAssetSpecificationProperty();

	/**
	 * Returns a new object of class '<em>Equipment Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Specification Property</em>'.
	 * @generated
	 */
	EquipmentSpecificationProperty createEquipmentSpecificationProperty();

	/**
	 * Returns a new object of class '<em>Personnel Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Specification Property</em>'.
	 * @generated
	 */
	PersonnelSpecificationProperty createPersonnelSpecificationProperty();

	/**
	 * Returns a new object of class '<em>Material Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Specification</em>'.
	 * @generated
	 */
	MaterialSpecification createMaterialSpecification();

	/**
	 * Returns a new object of class '<em>Physical Asset Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Specification</em>'.
	 * @generated
	 */
	PhysicalAssetSpecification createPhysicalAssetSpecification();

	/**
	 * Returns a new object of class '<em>Equipment Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Specification</em>'.
	 * @generated
	 */
	EquipmentSpecification createEquipmentSpecification();

	/**
	 * Returns a new object of class '<em>Personnel Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Specification</em>'.
	 * @generated
	 */
	PersonnelSpecification createPersonnelSpecification();

	/**
	 * Returns a new object of class '<em>Parameter Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Specification</em>'.
	 * @generated
	 */
	ParameterSpecification createParameterSpecification();

	/**
	 * Returns a new object of class '<em>Operations Segment Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Segment Dependency</em>'.
	 * @generated
	 */
	OperationsSegmentDependency createOperationsSegmentDependency();

	/**
	 * Returns a new object of class '<em>Operations Material Bill Item</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Material Bill Item</em>'.
	 * @generated
	 */
	OperationsMaterialBillItem createOperationsMaterialBillItem();

	/**
	 * Returns a new object of class '<em>Operations Material Bill</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Material Bill</em>'.
	 * @generated
	 */
	OperationsMaterialBill createOperationsMaterialBill();

	/**
	 * Returns a new object of class '<em>Operations Segment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Segment</em>'.
	 * @generated
	 */
	OperationsSegment createOperationsSegment();

	/**
	 * Returns a new object of class '<em>Operations Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Definition</em>'.
	 * @generated
	 */
	OperationsDefinition createOperationsDefinition();

	/**
	 * Returns a new object of class '<em>Material Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Requirement</em>'.
	 * @generated
	 */
	MaterialRequirement createMaterialRequirement();

	/**
	 * Returns a new object of class '<em>Material Requirement Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Requirement Property</em>'.
	 * @generated
	 */
	MaterialRequirementProperty createMaterialRequirementProperty();

	/**
	 * Returns a new object of class '<em>Physical Asset Requirement Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Requirement Property</em>'.
	 * @generated
	 */
	PhysicalAssetRequirementProperty createPhysicalAssetRequirementProperty();

	/**
	 * Returns a new object of class '<em>Equipment Requirement Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Requirement Property</em>'.
	 * @generated
	 */
	EquipmentRequirementProperty createEquipmentRequirementProperty();

	/**
	 * Returns a new object of class '<em>Personnel Requirement Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Requirement Property</em>'.
	 * @generated
	 */
	PersonnelRequirementProperty createPersonnelRequirementProperty();

	/**
	 * Returns a new object of class '<em>Operations Schedule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Schedule</em>'.
	 * @generated
	 */
	OperationsSchedule createOperationsSchedule();

	/**
	 * Returns a new object of class '<em>Operations Request</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Request</em>'.
	 * @generated
	 */
	OperationsRequest createOperationsRequest();

	/**
	 * Returns a new object of class '<em>Requested Segment Response</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Requested Segment Response</em>'.
	 * @generated
	 */
	RequestedSegmentResponse createRequestedSegmentResponse();

	/**
	 * Returns a new object of class '<em>Segment Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Segment Requirement</em>'.
	 * @generated
	 */
	SegmentRequirement createSegmentRequirement();

	/**
	 * Returns a new object of class '<em>Equipment Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Requirement</em>'.
	 * @generated
	 */
	EquipmentRequirement createEquipmentRequirement();

	/**
	 * Returns a new object of class '<em>Physical Asset Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Requirement</em>'.
	 * @generated
	 */
	PhysicalAssetRequirement createPhysicalAssetRequirement();

	/**
	 * Returns a new object of class '<em>Personnel Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Requirement</em>'.
	 * @generated
	 */
	PersonnelRequirement createPersonnelRequirement();

	/**
	 * Returns a new object of class '<em>Segment Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Segment Parameter</em>'.
	 * @generated
	 */
	SegmentParameter createSegmentParameter();

	/**
	 * Returns a new object of class '<em>Material Actual Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Actual Property</em>'.
	 * @generated
	 */
	MaterialActualProperty createMaterialActualProperty();

	/**
	 * Returns a new object of class '<em>Physical Asset Actual Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Actual Property</em>'.
	 * @generated
	 */
	PhysicalAssetActualProperty createPhysicalAssetActualProperty();

	/**
	 * Returns a new object of class '<em>Equipment Actual Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Actual Property</em>'.
	 * @generated
	 */
	EquipmentActualProperty createEquipmentActualProperty();

	/**
	 * Returns a new object of class '<em>Personnel Actual Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Actual Property</em>'.
	 * @generated
	 */
	PersonnelActualProperty createPersonnelActualProperty();

	/**
	 * Returns a new object of class '<em>Material Actual</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Actual</em>'.
	 * @generated
	 */
	MaterialActual createMaterialActual();

	/**
	 * Returns a new object of class '<em>Physical Asset Actual</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Actual</em>'.
	 * @generated
	 */
	PhysicalAssetActual createPhysicalAssetActual();

	/**
	 * Returns a new object of class '<em>Equipment Actual</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Actual</em>'.
	 * @generated
	 */
	EquipmentActual createEquipmentActual();

	/**
	 * Returns a new object of class '<em>Personnel Actual</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Actual</em>'.
	 * @generated
	 */
	PersonnelActual createPersonnelActual();

	/**
	 * Returns a new object of class '<em>Segment Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Segment Data</em>'.
	 * @generated
	 */
	SegmentData createSegmentData();

	/**
	 * Returns a new object of class '<em>Segment Response</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Segment Response</em>'.
	 * @generated
	 */
	SegmentResponse createSegmentResponse();

	/**
	 * Returns a new object of class '<em>Operations Response</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Response</em>'.
	 * @generated
	 */
	OperationsResponse createOperationsResponse();

	/**
	 * Returns a new object of class '<em>Operations Performance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Performance</em>'.
	 * @generated
	 */
	OperationsPerformance createOperationsPerformance();

	/**
	 * Returns a new object of class '<em>Operations Capability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Capability</em>'.
	 * @generated
	 */
	OperationsCapability createOperationsCapability();

	/**
	 * Returns a new object of class '<em>Material Capability Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Capability Property</em>'.
	 * @generated
	 */
	MaterialCapabilityProperty createMaterialCapabilityProperty();

	/**
	 * Returns a new object of class '<em>Physical Asset Capability Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Capability Property</em>'.
	 * @generated
	 */
	PhysicalAssetCapabilityProperty createPhysicalAssetCapabilityProperty();

	/**
	 * Returns a new object of class '<em>Equipment Capability Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Capability Property</em>'.
	 * @generated
	 */
	EquipmentCapabilityProperty createEquipmentCapabilityProperty();

	/**
	 * Returns a new object of class '<em>Personnel Capability Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Capability Property</em>'.
	 * @generated
	 */
	PersonnelCapabilityProperty createPersonnelCapabilityProperty();

	/**
	 * Returns a new object of class '<em>Material Capability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Capability</em>'.
	 * @generated
	 */
	MaterialCapability createMaterialCapability();

	/**
	 * Returns a new object of class '<em>Physical Asset Capability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Capability</em>'.
	 * @generated
	 */
	PhysicalAssetCapability createPhysicalAssetCapability();

	/**
	 * Returns a new object of class '<em>Equipment Capability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Capability</em>'.
	 * @generated
	 */
	EquipmentCapability createEquipmentCapability();

	/**
	 * Returns a new object of class '<em>Personnel Capability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Capability</em>'.
	 * @generated
	 */
	PersonnelCapability createPersonnelCapability();

	/**
	 * Returns a new object of class '<em>Process Segment Capability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Segment Capability</em>'.
	 * @generated
	 */
	ProcessSegmentCapability createProcessSegmentCapability();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Iec62264Package getIec62264Package();

} //Iec62264Factory
