/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.EquipmentActualProperty;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Actual Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualPropertyImpl#getSubProperties <em>Sub Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EquipmentActualPropertyImpl extends ReferentialEquipmentPropertyImpl implements EquipmentActualProperty {
	/**
	 * The cached value of the '{@link #getSubProperties() <em>Sub Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentActualProperty> subProperties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentActualPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.EQUIPMENT_ACTUAL_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentActualProperty> getSubProperties() {
		if (subProperties == null) {
			subProperties = new EObjectContainmentEList<EquipmentActualProperty>(EquipmentActualProperty.class, this,
					Iec62264Package.EQUIPMENT_ACTUAL_PROPERTY__SUB_PROPERTIES);
		}
		return subProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ACTUAL_PROPERTY__SUB_PROPERTIES:
			return ((InternalEList<?>) getSubProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ACTUAL_PROPERTY__SUB_PROPERTIES:
			return getSubProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ACTUAL_PROPERTY__SUB_PROPERTIES:
			getSubProperties().clear();
			getSubProperties().addAll((Collection<? extends EquipmentActualProperty>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ACTUAL_PROPERTY__SUB_PROPERTIES:
			getSubProperties().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ACTUAL_PROPERTY__SUB_PROPERTIES:
			return subProperties != null && !subProperties.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EquipmentActualPropertyImpl
