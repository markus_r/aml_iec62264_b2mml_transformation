/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hierarchy Scope</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.HierarchyScope#getSubScope <em>Sub Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.HierarchyScope#getEquipment <em>Equipment</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.HierarchyScope#getEquipmentElementLevel <em>Equipment Element Level</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getHierarchyScope()
 * @model
 * @generated
 */
public interface HierarchyScope extends EObject {
	/**
	 * Returns the value of the '<em><b>Sub Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Scope</em>' containment reference.
	 * @see #setSubScope(HierarchyScope)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getHierarchyScope_SubScope()
	 * @model containment="true"
	 * @generated
	 */
	HierarchyScope getSubScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.HierarchyScope#getSubScope <em>Sub Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub Scope</em>' containment reference.
	 * @see #getSubScope()
	 * @generated
	 */
	void setSubScope(HierarchyScope value);

	/**
	 * Returns the value of the '<em><b>Equipment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment</em>' reference.
	 * @see #setEquipment(Equipment)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getHierarchyScope_Equipment()
	 * @model
	 * @generated
	 */
	Equipment getEquipment();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.HierarchyScope#getEquipment <em>Equipment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment</em>' reference.
	 * @see #getEquipment()
	 * @generated
	 */
	void setEquipment(Equipment value);

	/**
	 * Returns the value of the '<em><b>Equipment Element Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Element Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Element Level</em>' attribute.
	 * @see #setEquipmentElementLevel(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getHierarchyScope_EquipmentElementLevel()
	 * @model
	 * @generated
	 */
	String getEquipmentElementLevel();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.HierarchyScope#getEquipmentElementLevel <em>Equipment Element Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Element Level</em>' attribute.
	 * @see #getEquipmentElementLevel()
	 * @generated
	 */
	void setEquipmentElementLevel(String value);

} // HierarchyScope
