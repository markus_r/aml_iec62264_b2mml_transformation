/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.OperationsDefinition;
import at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill;
import at.ac.tuwien.big.ame.iec62264.OperationsSegment;
import at.ac.tuwien.big.ame.iec62264.OperationsType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl#getOperationsSegments <em>Operations Segments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl#getOperationsMaterialBills <em>Operations Material Bills</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl#getBillOfMaterialId <em>Bill Of Material Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl#getWorkDefinitionId <em>Work Definition Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl#getBillOfResourceId <em>Bill Of Resource Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl#getHierarchysScope <em>Hierarchys Scope</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsDefinitionImpl extends MinimalEObjectImpl.Container implements OperationsDefinition {
	/**
	 * The cached value of the '{@link #getOperationsSegments() <em>Operations Segments</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsSegments()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsSegment> operationsSegments;

	/**
	 * The cached value of the '{@link #getOperationsMaterialBills() <em>Operations Material Bills</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsMaterialBills()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsMaterialBill> operationsMaterialBills;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected static final OperationsType OPERATIONS_TYPE_EDEFAULT = OperationsType.PRODUCTION;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsType operationsType = OPERATIONS_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getBillOfMaterialId() <em>Bill Of Material Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBillOfMaterialId()
	 * @generated
	 * @ordered
	 */
	protected static final String BILL_OF_MATERIAL_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBillOfMaterialId() <em>Bill Of Material Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBillOfMaterialId()
	 * @generated
	 * @ordered
	 */
	protected String billOfMaterialId = BILL_OF_MATERIAL_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorkDefinitionId() <em>Work Definition Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkDefinitionId()
	 * @generated
	 * @ordered
	 */
	protected static final String WORK_DEFINITION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWorkDefinitionId() <em>Work Definition Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkDefinitionId()
	 * @generated
	 * @ordered
	 */
	protected String workDefinitionId = WORK_DEFINITION_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getBillOfResourceId() <em>Bill Of Resource Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBillOfResourceId()
	 * @generated
	 * @ordered
	 */
	protected static final String BILL_OF_RESOURCE_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBillOfResourceId() <em>Bill Of Resource Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBillOfResourceId()
	 * @generated
	 * @ordered
	 */
	protected String billOfResourceId = BILL_OF_RESOURCE_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHierarchysScope() <em>Hierarchys Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchysScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScope hierarchysScope;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.OPERATIONS_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsSegment> getOperationsSegments() {
		if (operationsSegments == null) {
			operationsSegments = new EObjectResolvingEList<OperationsSegment>(OperationsSegment.class, this,
					Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_SEGMENTS);
		}
		return operationsSegments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsMaterialBill> getOperationsMaterialBills() {
		if (operationsMaterialBills == null) {
			operationsMaterialBills = new EObjectContainmentEList<OperationsMaterialBill>(OperationsMaterialBill.class,
					this, Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_MATERIAL_BILLS);
		}
		return operationsMaterialBills;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_DEFINITION__ID, oldId,
					id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_DEFINITION__VERSION,
					oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_DEFINITION__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsType newOperationsType) {
		OperationsType oldOperationsType = operationsType;
		operationsType = newOperationsType == null ? OPERATIONS_TYPE_EDEFAULT : newOperationsType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_TYPE, oldOperationsType, operationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBillOfMaterialId() {
		return billOfMaterialId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBillOfMaterialId(String newBillOfMaterialId) {
		String oldBillOfMaterialId = billOfMaterialId;
		billOfMaterialId = newBillOfMaterialId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_DEFINITION__BILL_OF_MATERIAL_ID, oldBillOfMaterialId, billOfMaterialId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getWorkDefinitionId() {
		return workDefinitionId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorkDefinitionId(String newWorkDefinitionId) {
		String oldWorkDefinitionId = workDefinitionId;
		workDefinitionId = newWorkDefinitionId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_DEFINITION__WORK_DEFINITION_ID, oldWorkDefinitionId, workDefinitionId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBillOfResourceId() {
		return billOfResourceId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBillOfResourceId(String newBillOfResourceId) {
		String oldBillOfResourceId = billOfResourceId;
		billOfResourceId = newBillOfResourceId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_DEFINITION__BILL_OF_RESOURCE_ID, oldBillOfResourceId, billOfResourceId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope getHierarchysScope() {
		if (hierarchysScope != null && hierarchysScope.eIsProxy()) {
			InternalEObject oldHierarchysScope = (InternalEObject) hierarchysScope;
			hierarchysScope = (HierarchyScope) eResolveProxy(oldHierarchysScope);
			if (hierarchysScope != oldHierarchysScope) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.OPERATIONS_DEFINITION__HIERARCHYS_SCOPE, oldHierarchysScope,
							hierarchysScope));
			}
		}
		return hierarchysScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope basicGetHierarchysScope() {
		return hierarchysScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchysScope(HierarchyScope newHierarchysScope) {
		HierarchyScope oldHierarchysScope = hierarchysScope;
		hierarchysScope = newHierarchysScope;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_DEFINITION__HIERARCHYS_SCOPE, oldHierarchysScope, hierarchysScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_MATERIAL_BILLS:
			return ((InternalEList<?>) getOperationsMaterialBills()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_SEGMENTS:
			return getOperationsSegments();
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_MATERIAL_BILLS:
			return getOperationsMaterialBills();
		case Iec62264Package.OPERATIONS_DEFINITION__ID:
			return getId();
		case Iec62264Package.OPERATIONS_DEFINITION__VERSION:
			return getVersion();
		case Iec62264Package.OPERATIONS_DEFINITION__DESCRIPTION:
			return getDescription();
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_TYPE:
			return getOperationsType();
		case Iec62264Package.OPERATIONS_DEFINITION__BILL_OF_MATERIAL_ID:
			return getBillOfMaterialId();
		case Iec62264Package.OPERATIONS_DEFINITION__WORK_DEFINITION_ID:
			return getWorkDefinitionId();
		case Iec62264Package.OPERATIONS_DEFINITION__BILL_OF_RESOURCE_ID:
			return getBillOfResourceId();
		case Iec62264Package.OPERATIONS_DEFINITION__HIERARCHYS_SCOPE:
			if (resolve)
				return getHierarchysScope();
			return basicGetHierarchysScope();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_SEGMENTS:
			getOperationsSegments().clear();
			getOperationsSegments().addAll((Collection<? extends OperationsSegment>) newValue);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_MATERIAL_BILLS:
			getOperationsMaterialBills().clear();
			getOperationsMaterialBills().addAll((Collection<? extends OperationsMaterialBill>) newValue);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__VERSION:
			setVersion((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_TYPE:
			setOperationsType((OperationsType) newValue);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__BILL_OF_MATERIAL_ID:
			setBillOfMaterialId((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__WORK_DEFINITION_ID:
			setWorkDefinitionId((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__BILL_OF_RESOURCE_ID:
			setBillOfResourceId((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__HIERARCHYS_SCOPE:
			setHierarchysScope((HierarchyScope) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_SEGMENTS:
			getOperationsSegments().clear();
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_MATERIAL_BILLS:
			getOperationsMaterialBills().clear();
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__VERSION:
			setVersion(VERSION_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_TYPE:
			setOperationsType(OPERATIONS_TYPE_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__BILL_OF_MATERIAL_ID:
			setBillOfMaterialId(BILL_OF_MATERIAL_ID_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__WORK_DEFINITION_ID:
			setWorkDefinitionId(WORK_DEFINITION_ID_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__BILL_OF_RESOURCE_ID:
			setBillOfResourceId(BILL_OF_RESOURCE_ID_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION__HIERARCHYS_SCOPE:
			setHierarchysScope((HierarchyScope) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_SEGMENTS:
			return operationsSegments != null && !operationsSegments.isEmpty();
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_MATERIAL_BILLS:
			return operationsMaterialBills != null && !operationsMaterialBills.isEmpty();
		case Iec62264Package.OPERATIONS_DEFINITION__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.OPERATIONS_DEFINITION__VERSION:
			return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
		case Iec62264Package.OPERATIONS_DEFINITION__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.OPERATIONS_DEFINITION__OPERATIONS_TYPE:
			return operationsType != OPERATIONS_TYPE_EDEFAULT;
		case Iec62264Package.OPERATIONS_DEFINITION__BILL_OF_MATERIAL_ID:
			return BILL_OF_MATERIAL_ID_EDEFAULT == null ? billOfMaterialId != null
					: !BILL_OF_MATERIAL_ID_EDEFAULT.equals(billOfMaterialId);
		case Iec62264Package.OPERATIONS_DEFINITION__WORK_DEFINITION_ID:
			return WORK_DEFINITION_ID_EDEFAULT == null ? workDefinitionId != null
					: !WORK_DEFINITION_ID_EDEFAULT.equals(workDefinitionId);
		case Iec62264Package.OPERATIONS_DEFINITION__BILL_OF_RESOURCE_ID:
			return BILL_OF_RESOURCE_ID_EDEFAULT == null ? billOfResourceId != null
					: !BILL_OF_RESOURCE_ID_EDEFAULT.equals(billOfResourceId);
		case Iec62264Package.OPERATIONS_DEFINITION__HIERARCHYS_SCOPE:
			return hierarchysScope != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", version: ");
		result.append(version);
		result.append(", description: ");
		result.append(description);
		result.append(", operationsType: ");
		result.append(operationsType);
		result.append(", billOfMaterialId: ");
		result.append(billOfMaterialId);
		result.append(", workDefinitionId: ");
		result.append(workDefinitionId);
		result.append(", billOfResourceId: ");
		result.append(billOfResourceId);
		result.append(')');
		return result.toString();
	}

} //OperationsDefinitionImpl
