/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.time.ZonedDateTime;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process Segment Capability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getSubCapabilities <em>Sub Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getPersonnelCapabilities <em>Personnel Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getEquipmentCapabilities <em>Equipment Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getPhysicalAssetCapabilities <em>Physical Asset Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getMaterialCapabilities <em>Material Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getProcessSegment <em>Process Segment</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getCapacityType <em>Capacity Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getReason <em>Reason</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability()
 * @model
 * @generated
 */
public interface ProcessSegmentCapability extends EObject {
	/**
	 * Returns the value of the '<em><b>Sub Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Capabilities</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_SubCapabilities()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProcessSegmentCapability> getSubCapabilities();

	/**
	 * Returns the value of the '<em><b>Personnel Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Capabilities</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_PersonnelCapabilities()
	 * @model containment="true"
	 * @generated
	 */
	EList<PersonnelCapability> getPersonnelCapabilities();

	/**
	 * Returns the value of the '<em><b>Equipment Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Capabilities</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_EquipmentCapabilities()
	 * @model containment="true"
	 * @generated
	 */
	EList<EquipmentCapability> getEquipmentCapabilities();

	/**
	 * Returns the value of the '<em><b>Physical Asset Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Capabilities</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_PhysicalAssetCapabilities()
	 * @model containment="true"
	 * @generated
	 */
	EList<PhysicalAssetCapability> getPhysicalAssetCapabilities();

	/**
	 * Returns the value of the '<em><b>Material Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialCapability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Capabilities</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_MaterialCapabilities()
	 * @model containment="true"
	 * @generated
	 */
	EList<MaterialCapability> getMaterialCapabilities();

	/**
	 * Returns the value of the '<em><b>Process Segment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment</em>' reference.
	 * @see #setProcessSegment(ProcessSegment)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_ProcessSegment()
	 * @model required="true"
	 * @generated
	 */
	ProcessSegment getProcessSegment();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getProcessSegment <em>Process Segment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Segment</em>' reference.
	 * @see #getProcessSegment()
	 * @generated
	 */
	void setProcessSegment(ProcessSegment value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Capacity Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.CapacityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capacity Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capacity Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.CapacityType
	 * @see #setCapacityType(CapacityType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_CapacityType()
	 * @model
	 * @generated
	 */
	CapacityType getCapacityType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getCapacityType <em>Capacity Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Capacity Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.CapacityType
	 * @see #getCapacityType()
	 * @generated
	 */
	void setCapacityType(CapacityType value);

	/**
	 * Returns the value of the '<em><b>Reason</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reason</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reason</em>' attribute.
	 * @see #setReason(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_Reason()
	 * @model
	 * @generated
	 */
	String getReason();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getReason <em>Reason</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reason</em>' attribute.
	 * @see #getReason()
	 * @generated
	 */
	void setReason(String value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_StartTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_EndTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #setHierarchyScope(HierarchyScope)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapability_HierarchyScope()
	 * @model
	 * @generated
	 */
	HierarchyScope getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getHierarchyScope <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScope value);

} // ProcessSegmentCapability
