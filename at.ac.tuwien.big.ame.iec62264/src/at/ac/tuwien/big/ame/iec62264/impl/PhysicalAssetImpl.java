/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.PhysicalAsset;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl#getChildren <em>Children</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl#getPhysicalAssetClass <em>Physical Asset Class</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl#getTestSpecifications <em>Test Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl#getPhysicalLocation <em>Physical Location</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl#getFixedAssetId <em>Fixed Asset Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl#getVendorId <em>Vendor Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalAssetImpl extends MinimalEObjectImpl.Container implements PhysicalAsset {
	/**
	 * The cached value of the '{@link #getChildren() <em>Children</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildren()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAsset> children;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetProperty> properties;

	/**
	 * The cached value of the '{@link #getPhysicalAssetClass() <em>Physical Asset Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClass()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetClass physicalAssetClass;

	/**
	 * The cached value of the '{@link #getTestSpecifications() <em>Test Specifications</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetCapabilityTestSpecification> testSpecifications;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPhysicalLocation() <em>Physical Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String PHYSICAL_LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPhysicalLocation() <em>Physical Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalLocation()
	 * @generated
	 * @ordered
	 */
	protected String physicalLocation = PHYSICAL_LOCATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getFixedAssetId() <em>Fixed Asset Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFixedAssetId()
	 * @generated
	 * @ordered
	 */
	protected static final String FIXED_ASSET_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFixedAssetId() <em>Fixed Asset Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFixedAssetId()
	 * @generated
	 * @ordered
	 */
	protected String fixedAssetId = FIXED_ASSET_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getVendorId() <em>Vendor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVendorId()
	 * @generated
	 * @ordered
	 */
	protected static final String VENDOR_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVendorId() <em>Vendor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVendorId()
	 * @generated
	 * @ordered
	 */
	protected String vendorId = VENDOR_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PHYSICAL_ASSET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAsset> getChildren() {
		if (children == null) {
			children = new EObjectResolvingEList<PhysicalAsset>(PhysicalAsset.class, this,
					Iec62264Package.PHYSICAL_ASSET__CHILDREN);
		}
		return children;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetProperty> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<PhysicalAssetProperty>(PhysicalAssetProperty.class, this,
					Iec62264Package.PHYSICAL_ASSET__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClass getPhysicalAssetClass() {
		if (physicalAssetClass != null && physicalAssetClass.eIsProxy()) {
			InternalEObject oldPhysicalAssetClass = (InternalEObject) physicalAssetClass;
			physicalAssetClass = (PhysicalAssetClass) eResolveProxy(oldPhysicalAssetClass);
			if (physicalAssetClass != oldPhysicalAssetClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.PHYSICAL_ASSET__PHYSICAL_ASSET_CLASS, oldPhysicalAssetClass,
							physicalAssetClass));
			}
		}
		return physicalAssetClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClass basicGetPhysicalAssetClass() {
		return physicalAssetClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetClass(PhysicalAssetClass newPhysicalAssetClass) {
		PhysicalAssetClass oldPhysicalAssetClass = physicalAssetClass;
		physicalAssetClass = newPhysicalAssetClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PHYSICAL_ASSET__PHYSICAL_ASSET_CLASS,
					oldPhysicalAssetClass, physicalAssetClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetCapabilityTestSpecification> getTestSpecifications() {
		if (testSpecifications == null) {
			testSpecifications = new EObjectResolvingEList<PhysicalAssetCapabilityTestSpecification>(
					PhysicalAssetCapabilityTestSpecification.class, this,
					Iec62264Package.PHYSICAL_ASSET__TEST_SPECIFICATIONS);
		}
		return testSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PHYSICAL_ASSET__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PHYSICAL_ASSET__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPhysicalLocation() {
		return physicalLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalLocation(String newPhysicalLocation) {
		String oldPhysicalLocation = physicalLocation;
		physicalLocation = newPhysicalLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PHYSICAL_ASSET__PHYSICAL_LOCATION,
					oldPhysicalLocation, physicalLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFixedAssetId() {
		return fixedAssetId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFixedAssetId(String newFixedAssetId) {
		String oldFixedAssetId = fixedAssetId;
		fixedAssetId = newFixedAssetId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PHYSICAL_ASSET__FIXED_ASSET_ID,
					oldFixedAssetId, fixedAssetId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVendorId() {
		return vendorId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVendorId(String newVendorId) {
		String oldVendorId = vendorId;
		vendorId = newVendorId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PHYSICAL_ASSET__VENDOR_ID,
					oldVendorId, vendorId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET__PROPERTIES:
			return ((InternalEList<?>) getProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET__CHILDREN:
			return getChildren();
		case Iec62264Package.PHYSICAL_ASSET__PROPERTIES:
			return getProperties();
		case Iec62264Package.PHYSICAL_ASSET__PHYSICAL_ASSET_CLASS:
			if (resolve)
				return getPhysicalAssetClass();
			return basicGetPhysicalAssetClass();
		case Iec62264Package.PHYSICAL_ASSET__TEST_SPECIFICATIONS:
			return getTestSpecifications();
		case Iec62264Package.PHYSICAL_ASSET__ID:
			return getId();
		case Iec62264Package.PHYSICAL_ASSET__DESCRIPTION:
			return getDescription();
		case Iec62264Package.PHYSICAL_ASSET__PHYSICAL_LOCATION:
			return getPhysicalLocation();
		case Iec62264Package.PHYSICAL_ASSET__FIXED_ASSET_ID:
			return getFixedAssetId();
		case Iec62264Package.PHYSICAL_ASSET__VENDOR_ID:
			return getVendorId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET__CHILDREN:
			getChildren().clear();
			getChildren().addAll((Collection<? extends PhysicalAsset>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET__PROPERTIES:
			getProperties().clear();
			getProperties().addAll((Collection<? extends PhysicalAssetProperty>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET__PHYSICAL_ASSET_CLASS:
			setPhysicalAssetClass((PhysicalAssetClass) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			getTestSpecifications().addAll((Collection<? extends PhysicalAssetCapabilityTestSpecification>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET__PHYSICAL_LOCATION:
			setPhysicalLocation((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET__FIXED_ASSET_ID:
			setFixedAssetId((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET__VENDOR_ID:
			setVendorId((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET__CHILDREN:
			getChildren().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET__PROPERTIES:
			getProperties().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET__PHYSICAL_ASSET_CLASS:
			setPhysicalAssetClass((PhysicalAssetClass) null);
			return;
		case Iec62264Package.PHYSICAL_ASSET__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET__PHYSICAL_LOCATION:
			setPhysicalLocation(PHYSICAL_LOCATION_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET__FIXED_ASSET_ID:
			setFixedAssetId(FIXED_ASSET_ID_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET__VENDOR_ID:
			setVendorId(VENDOR_ID_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET__CHILDREN:
			return children != null && !children.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET__PROPERTIES:
			return properties != null && !properties.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET__PHYSICAL_ASSET_CLASS:
			return physicalAssetClass != null;
		case Iec62264Package.PHYSICAL_ASSET__TEST_SPECIFICATIONS:
			return testSpecifications != null && !testSpecifications.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.PHYSICAL_ASSET__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.PHYSICAL_ASSET__PHYSICAL_LOCATION:
			return PHYSICAL_LOCATION_EDEFAULT == null ? physicalLocation != null
					: !PHYSICAL_LOCATION_EDEFAULT.equals(physicalLocation);
		case Iec62264Package.PHYSICAL_ASSET__FIXED_ASSET_ID:
			return FIXED_ASSET_ID_EDEFAULT == null ? fixedAssetId != null
					: !FIXED_ASSET_ID_EDEFAULT.equals(fixedAssetId);
		case Iec62264Package.PHYSICAL_ASSET__VENDOR_ID:
			return VENDOR_ID_EDEFAULT == null ? vendorId != null : !VENDOR_ID_EDEFAULT.equals(vendorId);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", physicalLocation: ");
		result.append(physicalLocation);
		result.append(", fixedAssetId: ");
		result.append(fixedAssetId);
		result.append(", vendorId: ");
		result.append(vendorId);
		result.append(')');
		return result.toString();
	}

} //PhysicalAssetImpl
