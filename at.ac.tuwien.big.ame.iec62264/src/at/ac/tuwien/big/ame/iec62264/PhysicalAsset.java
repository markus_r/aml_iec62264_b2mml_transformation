/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getChildren <em>Children</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getProperties <em>Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getPhysicalAssetClass <em>Physical Asset Class</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getTestSpecifications <em>Test Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getPhysicalLocation <em>Physical Location</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getFixedAssetId <em>Fixed Asset Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getVendorId <em>Vendor Id</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAsset()
 * @model
 * @generated
 */
public interface PhysicalAsset extends EObject {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAsset_Children()
	 * @model
	 * @generated
	 */
	EList<PhysicalAsset> getChildren();

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAsset_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<PhysicalAssetProperty> getProperties();

	/**
	 * Returns the value of the '<em><b>Physical Asset Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Class</em>' reference.
	 * @see #setPhysicalAssetClass(PhysicalAssetClass)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAsset_PhysicalAssetClass()
	 * @model required="true"
	 * @generated
	 */
	PhysicalAssetClass getPhysicalAssetClass();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getPhysicalAssetClass <em>Physical Asset Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Class</em>' reference.
	 * @see #getPhysicalAssetClass()
	 * @generated
	 */
	void setPhysicalAssetClass(PhysicalAssetClass value);

	/**
	 * Returns the value of the '<em><b>Test Specifications</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Specifications</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Specifications</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAsset_TestSpecifications()
	 * @model
	 * @generated
	 */
	EList<PhysicalAssetCapabilityTestSpecification> getTestSpecifications();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAsset_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAsset_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Physical Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Location</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Location</em>' attribute.
	 * @see #setPhysicalLocation(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAsset_PhysicalLocation()
	 * @model
	 * @generated
	 */
	String getPhysicalLocation();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getPhysicalLocation <em>Physical Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Location</em>' attribute.
	 * @see #getPhysicalLocation()
	 * @generated
	 */
	void setPhysicalLocation(String value);

	/**
	 * Returns the value of the '<em><b>Fixed Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fixed Asset Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fixed Asset Id</em>' attribute.
	 * @see #setFixedAssetId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAsset_FixedAssetId()
	 * @model
	 * @generated
	 */
	String getFixedAssetId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getFixedAssetId <em>Fixed Asset Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fixed Asset Id</em>' attribute.
	 * @see #getFixedAssetId()
	 * @generated
	 */
	void setFixedAssetId(String value);

	/**
	 * Returns the value of the '<em><b>Vendor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vendor Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vendor Id</em>' attribute.
	 * @see #setVendorId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAsset_VendorId()
	 * @model
	 * @generated
	 */
	String getVendorId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getVendorId <em>Vendor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vendor Id</em>' attribute.
	 * @see #getVendorId()
	 * @generated
	 */
	void setVendorId(String value);

} // PhysicalAsset
