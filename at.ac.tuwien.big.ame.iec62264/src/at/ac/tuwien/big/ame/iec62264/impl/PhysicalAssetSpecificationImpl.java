/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.PhysicalAsset;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecificationProperty;

import java.math.BigDecimal;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationImpl#getPhysicalAssetSpecificationProperties <em>Physical Asset Specification Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationImpl#getPhysicAlassetClasses <em>Physic Alasset Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationImpl#getPhysicalAssets <em>Physical Assets</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationImpl#getPhysicalAssetUse <em>Physical Asset Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationImpl#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalAssetSpecificationImpl extends MinimalEObjectImpl.Container implements PhysicalAssetSpecification {
	/**
	 * The cached value of the '{@link #getPhysicalAssetSpecificationProperties() <em>Physical Asset Specification Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetSpecificationProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetSpecificationProperty> physicalAssetSpecificationProperties;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPhysicAlassetClasses() <em>Physic Alasset Classes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicAlassetClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetClass> physicAlassetClasses;

	/**
	 * The cached value of the '{@link #getPhysicalAssets() <em>Physical Assets</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssets()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAsset physicalAssets;

	/**
	 * The default value of the '{@link #getPhysicalAssetUse() <em>Physical Asset Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetUse()
	 * @generated
	 * @ordered
	 */
	protected static final String PHYSICAL_ASSET_USE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPhysicalAssetUse() <em>Physical Asset Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetUse()
	 * @generated
	 * @ordered
	 */
	protected String physicalAssetUse = PHYSICAL_ASSET_USE_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal QUANTITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal quantity = QUANTITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String QUANTITY_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String quantityUnitOfMeasure = QUANTITY_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PHYSICAL_ASSET_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetSpecificationProperty> getPhysicalAssetSpecificationProperties() {
		if (physicalAssetSpecificationProperties == null) {
			physicalAssetSpecificationProperties = new EObjectContainmentEList<PhysicalAssetSpecificationProperty>(
					PhysicalAssetSpecificationProperty.class, this,
					Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_SPECIFICATION_PROPERTIES);
		}
		return physicalAssetSpecificationProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetClass> getPhysicAlassetClasses() {
		if (physicAlassetClasses == null) {
			physicAlassetClasses = new EObjectResolvingEList<PhysicalAssetClass>(PhysicalAssetClass.class, this,
					Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSIC_ALASSET_CLASSES);
		}
		return physicAlassetClasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAsset getPhysicalAssets() {
		if (physicalAssets != null && physicalAssets.eIsProxy()) {
			InternalEObject oldPhysicalAssets = (InternalEObject) physicalAssets;
			physicalAssets = (PhysicalAsset) eResolveProxy(oldPhysicalAssets);
			if (physicalAssets != oldPhysicalAssets) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSETS, oldPhysicalAssets,
							physicalAssets));
			}
		}
		return physicalAssets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAsset basicGetPhysicalAssets() {
		return physicalAssets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssets(PhysicalAsset newPhysicalAssets) {
		PhysicalAsset oldPhysicalAssets = physicalAssets;
		physicalAssets = newPhysicalAssets;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSETS, oldPhysicalAssets, physicalAssets));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPhysicalAssetUse() {
		return physicalAssetUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetUse(String newPhysicalAssetUse) {
		String oldPhysicalAssetUse = physicalAssetUse;
		physicalAssetUse = newPhysicalAssetUse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_USE, oldPhysicalAssetUse,
					physicalAssetUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(BigDecimal newQuantity) {
		BigDecimal oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__QUANTITY, oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQuantityUnitOfMeasure() {
		return quantityUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantityUnitOfMeasure(String newQuantityUnitOfMeasure) {
		String oldQuantityUnitOfMeasure = quantityUnitOfMeasure;
		quantityUnitOfMeasure = newQuantityUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE, oldQuantityUnitOfMeasure,
					quantityUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_SPECIFICATION_PROPERTIES:
			return ((InternalEList<?>) getPhysicalAssetSpecificationProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_SPECIFICATION_PROPERTIES:
			return getPhysicalAssetSpecificationProperties();
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__DESCRIPTION:
			return getDescription();
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSIC_ALASSET_CLASSES:
			return getPhysicAlassetClasses();
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSETS:
			if (resolve)
				return getPhysicalAssets();
			return basicGetPhysicalAssets();
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_USE:
			return getPhysicalAssetUse();
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__QUANTITY:
			return getQuantity();
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			return getQuantityUnitOfMeasure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_SPECIFICATION_PROPERTIES:
			getPhysicalAssetSpecificationProperties().clear();
			getPhysicalAssetSpecificationProperties()
					.addAll((Collection<? extends PhysicalAssetSpecificationProperty>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSIC_ALASSET_CLASSES:
			getPhysicAlassetClasses().clear();
			getPhysicAlassetClasses().addAll((Collection<? extends PhysicalAssetClass>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSETS:
			setPhysicalAssets((PhysicalAsset) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_USE:
			setPhysicalAssetUse((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__QUANTITY:
			setQuantity((BigDecimal) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_SPECIFICATION_PROPERTIES:
			getPhysicalAssetSpecificationProperties().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSIC_ALASSET_CLASSES:
			getPhysicAlassetClasses().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSETS:
			setPhysicalAssets((PhysicalAsset) null);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_USE:
			setPhysicalAssetUse(PHYSICAL_ASSET_USE_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure(QUANTITY_UNIT_OF_MEASURE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_SPECIFICATION_PROPERTIES:
			return physicalAssetSpecificationProperties != null && !physicalAssetSpecificationProperties.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSIC_ALASSET_CLASSES:
			return physicAlassetClasses != null && !physicAlassetClasses.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSETS:
			return physicalAssets != null;
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_USE:
			return PHYSICAL_ASSET_USE_EDEFAULT == null ? physicalAssetUse != null
					: !PHYSICAL_ASSET_USE_EDEFAULT.equals(physicalAssetUse);
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__QUANTITY:
			return QUANTITY_EDEFAULT == null ? quantity != null : !QUANTITY_EDEFAULT.equals(quantity);
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			return QUANTITY_UNIT_OF_MEASURE_EDEFAULT == null ? quantityUnitOfMeasure != null
					: !QUANTITY_UNIT_OF_MEASURE_EDEFAULT.equals(quantityUnitOfMeasure);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", physicalAssetUse: ");
		result.append(physicalAssetUse);
		result.append(", quantity: ");
		result.append(quantity);
		result.append(", quantityUnitOfMeasure: ");
		result.append(quantityUnitOfMeasure);
		result.append(')');
		return result.toString();
	}

} //PhysicalAssetSpecificationImpl
