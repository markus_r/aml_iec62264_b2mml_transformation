/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty;
import at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Personnel Class Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassPropertyImpl#getTestSpecifications <em>Test Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassPropertyImpl#getSubProperties <em>Sub Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassPropertyImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassPropertyImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassPropertyImpl#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassPropertyImpl#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonnelClassPropertyImpl extends MinimalEObjectImpl.Container implements PersonnelClassProperty {
	/**
	 * The cached value of the '{@link #getTestSpecifications() <em>Test Specifications</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<QualificationTestSpecification> testSpecifications;

	/**
	 * The cached value of the '{@link #getSubProperties() <em>Sub Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelClassProperty> subProperties;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getValueUnitOfMeasure() <em>Value Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValueUnitOfMeasure() <em>Value Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String valueUnitOfMeasure = VALUE_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonnelClassPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PERSONNEL_CLASS_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QualificationTestSpecification> getTestSpecifications() {
		if (testSpecifications == null) {
			testSpecifications = new EObjectResolvingEList<QualificationTestSpecification>(
					QualificationTestSpecification.class, this,
					Iec62264Package.PERSONNEL_CLASS_PROPERTY__TEST_SPECIFICATIONS);
		}
		return testSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelClassProperty> getSubProperties() {
		if (subProperties == null) {
			subProperties = new EObjectContainmentEList<PersonnelClassProperty>(PersonnelClassProperty.class, this,
					Iec62264Package.PERSONNEL_CLASS_PROPERTY__SUB_PROPERTIES);
		}
		return subProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PERSONNEL_CLASS_PROPERTY__ID, oldId,
					id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PERSONNEL_CLASS_PROPERTY__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PERSONNEL_CLASS_PROPERTY__VALUE,
					oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValueUnitOfMeasure() {
		return valueUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueUnitOfMeasure(String newValueUnitOfMeasure) {
		String oldValueUnitOfMeasure = valueUnitOfMeasure;
		valueUnitOfMeasure = newValueUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PERSONNEL_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE, oldValueUnitOfMeasure,
					valueUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__SUB_PROPERTIES:
			return ((InternalEList<?>) getSubProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__TEST_SPECIFICATIONS:
			return getTestSpecifications();
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__SUB_PROPERTIES:
			return getSubProperties();
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__ID:
			return getId();
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__DESCRIPTION:
			return getDescription();
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__VALUE:
			return getValue();
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE:
			return getValueUnitOfMeasure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			getTestSpecifications().addAll((Collection<? extends QualificationTestSpecification>) newValue);
			return;
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__SUB_PROPERTIES:
			getSubProperties().clear();
			getSubProperties().addAll((Collection<? extends PersonnelClassProperty>) newValue);
			return;
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__VALUE:
			setValue((String) newValue);
			return;
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE:
			setValueUnitOfMeasure((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			return;
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__SUB_PROPERTIES:
			getSubProperties().clear();
			return;
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__VALUE:
			setValue(VALUE_EDEFAULT);
			return;
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE:
			setValueUnitOfMeasure(VALUE_UNIT_OF_MEASURE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__TEST_SPECIFICATIONS:
			return testSpecifications != null && !testSpecifications.isEmpty();
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__SUB_PROPERTIES:
			return subProperties != null && !subProperties.isEmpty();
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__VALUE:
			return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE:
			return VALUE_UNIT_OF_MEASURE_EDEFAULT == null ? valueUnitOfMeasure != null
					: !VALUE_UNIT_OF_MEASURE_EDEFAULT.equals(valueUnitOfMeasure);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", value: ");
		result.append(value);
		result.append(", valueUnitOfMeasure: ");
		result.append(valueUnitOfMeasure);
		result.append(')');
		return result.toString();
	}

} //PersonnelClassPropertyImpl
