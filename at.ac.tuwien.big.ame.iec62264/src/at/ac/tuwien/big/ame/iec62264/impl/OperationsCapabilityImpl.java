/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.CapabilityType;
import at.ac.tuwien.big.ame.iec62264.EquipmentCapability;
import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialCapability;
import at.ac.tuwien.big.ame.iec62264.OperationsCapability;
import at.ac.tuwien.big.ame.iec62264.PersonnelCapability;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability;

import java.time.ZonedDateTime;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Capability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getProcessSegmentCapabilities <em>Process Segment Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getPersonnelCapabilities <em>Personnel Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getEquipmentCapabilities <em>Equipment Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getPhysicalAssetCapabilities <em>Physical Asset Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getMaterialCapabilities <em>Material Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getCapacityType <em>Capacity Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getReason <em>Reason</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsCapabilityImpl extends MinimalEObjectImpl.Container implements OperationsCapability {
	/**
	 * The cached value of the '{@link #getProcessSegmentCapabilities() <em>Process Segment Capabilities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegmentCapabilities()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessSegmentCapability> processSegmentCapabilities;

	/**
	 * The cached value of the '{@link #getPersonnelCapabilities() <em>Personnel Capabilities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelCapabilities()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelCapability> personnelCapabilities;

	/**
	 * The cached value of the '{@link #getEquipmentCapabilities() <em>Equipment Capabilities</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentCapabilities()
	 * @generated
	 * @ordered
	 */
	protected EquipmentCapability equipmentCapabilities;

	/**
	 * The cached value of the '{@link #getPhysicalAssetCapabilities() <em>Physical Asset Capabilities</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetCapabilities()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetCapability physicalAssetCapabilities;

	/**
	 * The cached value of the '{@link #getMaterialCapabilities() <em>Material Capabilities</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialCapabilities()
	 * @generated
	 * @ordered
	 */
	protected MaterialCapability materialCapabilities;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCapacityType() <em>Capacity Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapacityType()
	 * @generated
	 * @ordered
	 */
	protected static final CapabilityType CAPACITY_TYPE_EDEFAULT = CapabilityType.USED;

	/**
	 * The cached value of the '{@link #getCapacityType() <em>Capacity Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapacityType()
	 * @generated
	 * @ordered
	 */
	protected CapabilityType capacityType = CAPACITY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getReason() <em>Reason</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected static final String REASON_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReason() <em>Reason</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected String reason = REASON_EDEFAULT;

	/**
	 * The default value of the '{@link #getConfidenceFactor() <em>Confidence Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceFactor()
	 * @generated
	 * @ordered
	 */
	protected static final String CONFIDENCE_FACTOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConfidenceFactor() <em>Confidence Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceFactor()
	 * @generated
	 * @ordered
	 */
	protected String confidenceFactor = CONFIDENCE_FACTOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime startTime = START_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime END_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime endTime = END_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPublishedDate() <em>Published Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublishedDate()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime PUBLISHED_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPublishedDate() <em>Published Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublishedDate()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime publishedDate = PUBLISHED_DATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScope hierarchyScope;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsCapabilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.OPERATIONS_CAPABILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessSegmentCapability> getProcessSegmentCapabilities() {
		if (processSegmentCapabilities == null) {
			processSegmentCapabilities = new EObjectContainmentEList<ProcessSegmentCapability>(
					ProcessSegmentCapability.class, this,
					Iec62264Package.OPERATIONS_CAPABILITY__PROCESS_SEGMENT_CAPABILITIES);
		}
		return processSegmentCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelCapability> getPersonnelCapabilities() {
		if (personnelCapabilities == null) {
			personnelCapabilities = new EObjectContainmentEList<PersonnelCapability>(PersonnelCapability.class, this,
					Iec62264Package.OPERATIONS_CAPABILITY__PERSONNEL_CAPABILITIES);
		}
		return personnelCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentCapability getEquipmentCapabilities() {
		if (equipmentCapabilities != null && equipmentCapabilities.eIsProxy()) {
			InternalEObject oldEquipmentCapabilities = (InternalEObject) equipmentCapabilities;
			equipmentCapabilities = (EquipmentCapability) eResolveProxy(oldEquipmentCapabilities);
			if (equipmentCapabilities != oldEquipmentCapabilities) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.OPERATIONS_CAPABILITY__EQUIPMENT_CAPABILITIES, oldEquipmentCapabilities,
							equipmentCapabilities));
			}
		}
		return equipmentCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentCapability basicGetEquipmentCapabilities() {
		return equipmentCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentCapabilities(EquipmentCapability newEquipmentCapabilities) {
		EquipmentCapability oldEquipmentCapabilities = equipmentCapabilities;
		equipmentCapabilities = newEquipmentCapabilities;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_CAPABILITY__EQUIPMENT_CAPABILITIES, oldEquipmentCapabilities,
					equipmentCapabilities));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetCapability getPhysicalAssetCapabilities() {
		if (physicalAssetCapabilities != null && physicalAssetCapabilities.eIsProxy()) {
			InternalEObject oldPhysicalAssetCapabilities = (InternalEObject) physicalAssetCapabilities;
			physicalAssetCapabilities = (PhysicalAssetCapability) eResolveProxy(oldPhysicalAssetCapabilities);
			if (physicalAssetCapabilities != oldPhysicalAssetCapabilities) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.OPERATIONS_CAPABILITY__PHYSICAL_ASSET_CAPABILITIES,
							oldPhysicalAssetCapabilities, physicalAssetCapabilities));
			}
		}
		return physicalAssetCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetCapability basicGetPhysicalAssetCapabilities() {
		return physicalAssetCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetCapabilities(PhysicalAssetCapability newPhysicalAssetCapabilities) {
		PhysicalAssetCapability oldPhysicalAssetCapabilities = physicalAssetCapabilities;
		physicalAssetCapabilities = newPhysicalAssetCapabilities;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_CAPABILITY__PHYSICAL_ASSET_CAPABILITIES, oldPhysicalAssetCapabilities,
					physicalAssetCapabilities));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialCapability getMaterialCapabilities() {
		if (materialCapabilities != null && materialCapabilities.eIsProxy()) {
			InternalEObject oldMaterialCapabilities = (InternalEObject) materialCapabilities;
			materialCapabilities = (MaterialCapability) eResolveProxy(oldMaterialCapabilities);
			if (materialCapabilities != oldMaterialCapabilities) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.OPERATIONS_CAPABILITY__MATERIAL_CAPABILITIES, oldMaterialCapabilities,
							materialCapabilities));
			}
		}
		return materialCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialCapability basicGetMaterialCapabilities() {
		return materialCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialCapabilities(MaterialCapability newMaterialCapabilities) {
		MaterialCapability oldMaterialCapabilities = materialCapabilities;
		materialCapabilities = newMaterialCapabilities;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_CAPABILITY__MATERIAL_CAPABILITIES, oldMaterialCapabilities,
					materialCapabilities));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_CAPABILITY__ID, oldId,
					id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_CAPABILITY__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityType getCapacityType() {
		return capacityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCapacityType(CapabilityType newCapacityType) {
		CapabilityType oldCapacityType = capacityType;
		capacityType = newCapacityType == null ? CAPACITY_TYPE_EDEFAULT : newCapacityType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_CAPABILITY__CAPACITY_TYPE,
					oldCapacityType, capacityType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReason(String newReason) {
		String oldReason = reason;
		reason = newReason;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_CAPABILITY__REASON,
					oldReason, reason));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConfidenceFactor() {
		return confidenceFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfidenceFactor(String newConfidenceFactor) {
		String oldConfidenceFactor = confidenceFactor;
		confidenceFactor = newConfidenceFactor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_CAPABILITY__CONFIDENCE_FACTOR, oldConfidenceFactor, confidenceFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(ZonedDateTime newStartTime) {
		ZonedDateTime oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_CAPABILITY__START_TIME,
					oldStartTime, startTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(ZonedDateTime newEndTime) {
		ZonedDateTime oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_CAPABILITY__END_TIME,
					oldEndTime, endTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getPublishedDate() {
		return publishedDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublishedDate(ZonedDateTime newPublishedDate) {
		ZonedDateTime oldPublishedDate = publishedDate;
		publishedDate = newPublishedDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_CAPABILITY__PUBLISHED_DATE,
					oldPublishedDate, publishedDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope getHierarchyScope() {
		if (hierarchyScope != null && hierarchyScope.eIsProxy()) {
			InternalEObject oldHierarchyScope = (InternalEObject) hierarchyScope;
			hierarchyScope = (HierarchyScope) eResolveProxy(oldHierarchyScope);
			if (hierarchyScope != oldHierarchyScope) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.OPERATIONS_CAPABILITY__HIERARCHY_SCOPE, oldHierarchyScope, hierarchyScope));
			}
		}
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope basicGetHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScope newHierarchyScope) {
		HierarchyScope oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_CAPABILITY__HIERARCHY_SCOPE, oldHierarchyScope, hierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_CAPABILITY__PROCESS_SEGMENT_CAPABILITIES:
			return ((InternalEList<?>) getProcessSegmentCapabilities()).basicRemove(otherEnd, msgs);
		case Iec62264Package.OPERATIONS_CAPABILITY__PERSONNEL_CAPABILITIES:
			return ((InternalEList<?>) getPersonnelCapabilities()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_CAPABILITY__PROCESS_SEGMENT_CAPABILITIES:
			return getProcessSegmentCapabilities();
		case Iec62264Package.OPERATIONS_CAPABILITY__PERSONNEL_CAPABILITIES:
			return getPersonnelCapabilities();
		case Iec62264Package.OPERATIONS_CAPABILITY__EQUIPMENT_CAPABILITIES:
			if (resolve)
				return getEquipmentCapabilities();
			return basicGetEquipmentCapabilities();
		case Iec62264Package.OPERATIONS_CAPABILITY__PHYSICAL_ASSET_CAPABILITIES:
			if (resolve)
				return getPhysicalAssetCapabilities();
			return basicGetPhysicalAssetCapabilities();
		case Iec62264Package.OPERATIONS_CAPABILITY__MATERIAL_CAPABILITIES:
			if (resolve)
				return getMaterialCapabilities();
			return basicGetMaterialCapabilities();
		case Iec62264Package.OPERATIONS_CAPABILITY__ID:
			return getId();
		case Iec62264Package.OPERATIONS_CAPABILITY__DESCRIPTION:
			return getDescription();
		case Iec62264Package.OPERATIONS_CAPABILITY__CAPACITY_TYPE:
			return getCapacityType();
		case Iec62264Package.OPERATIONS_CAPABILITY__REASON:
			return getReason();
		case Iec62264Package.OPERATIONS_CAPABILITY__CONFIDENCE_FACTOR:
			return getConfidenceFactor();
		case Iec62264Package.OPERATIONS_CAPABILITY__START_TIME:
			return getStartTime();
		case Iec62264Package.OPERATIONS_CAPABILITY__END_TIME:
			return getEndTime();
		case Iec62264Package.OPERATIONS_CAPABILITY__PUBLISHED_DATE:
			return getPublishedDate();
		case Iec62264Package.OPERATIONS_CAPABILITY__HIERARCHY_SCOPE:
			if (resolve)
				return getHierarchyScope();
			return basicGetHierarchyScope();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_CAPABILITY__PROCESS_SEGMENT_CAPABILITIES:
			getProcessSegmentCapabilities().clear();
			getProcessSegmentCapabilities().addAll((Collection<? extends ProcessSegmentCapability>) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__PERSONNEL_CAPABILITIES:
			getPersonnelCapabilities().clear();
			getPersonnelCapabilities().addAll((Collection<? extends PersonnelCapability>) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__EQUIPMENT_CAPABILITIES:
			setEquipmentCapabilities((EquipmentCapability) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__PHYSICAL_ASSET_CAPABILITIES:
			setPhysicalAssetCapabilities((PhysicalAssetCapability) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__MATERIAL_CAPABILITIES:
			setMaterialCapabilities((MaterialCapability) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__CAPACITY_TYPE:
			setCapacityType((CapabilityType) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__REASON:
			setReason((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__CONFIDENCE_FACTOR:
			setConfidenceFactor((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__START_TIME:
			setStartTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__END_TIME:
			setEndTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__PUBLISHED_DATE:
			setPublishedDate((ZonedDateTime) newValue);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_CAPABILITY__PROCESS_SEGMENT_CAPABILITIES:
			getProcessSegmentCapabilities().clear();
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__PERSONNEL_CAPABILITIES:
			getPersonnelCapabilities().clear();
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__EQUIPMENT_CAPABILITIES:
			setEquipmentCapabilities((EquipmentCapability) null);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__PHYSICAL_ASSET_CAPABILITIES:
			setPhysicalAssetCapabilities((PhysicalAssetCapability) null);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__MATERIAL_CAPABILITIES:
			setMaterialCapabilities((MaterialCapability) null);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__CAPACITY_TYPE:
			setCapacityType(CAPACITY_TYPE_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__REASON:
			setReason(REASON_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__CONFIDENCE_FACTOR:
			setConfidenceFactor(CONFIDENCE_FACTOR_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__START_TIME:
			setStartTime(START_TIME_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__END_TIME:
			setEndTime(END_TIME_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__PUBLISHED_DATE:
			setPublishedDate(PUBLISHED_DATE_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_CAPABILITY__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_CAPABILITY__PROCESS_SEGMENT_CAPABILITIES:
			return processSegmentCapabilities != null && !processSegmentCapabilities.isEmpty();
		case Iec62264Package.OPERATIONS_CAPABILITY__PERSONNEL_CAPABILITIES:
			return personnelCapabilities != null && !personnelCapabilities.isEmpty();
		case Iec62264Package.OPERATIONS_CAPABILITY__EQUIPMENT_CAPABILITIES:
			return equipmentCapabilities != null;
		case Iec62264Package.OPERATIONS_CAPABILITY__PHYSICAL_ASSET_CAPABILITIES:
			return physicalAssetCapabilities != null;
		case Iec62264Package.OPERATIONS_CAPABILITY__MATERIAL_CAPABILITIES:
			return materialCapabilities != null;
		case Iec62264Package.OPERATIONS_CAPABILITY__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.OPERATIONS_CAPABILITY__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.OPERATIONS_CAPABILITY__CAPACITY_TYPE:
			return capacityType != CAPACITY_TYPE_EDEFAULT;
		case Iec62264Package.OPERATIONS_CAPABILITY__REASON:
			return REASON_EDEFAULT == null ? reason != null : !REASON_EDEFAULT.equals(reason);
		case Iec62264Package.OPERATIONS_CAPABILITY__CONFIDENCE_FACTOR:
			return CONFIDENCE_FACTOR_EDEFAULT == null ? confidenceFactor != null
					: !CONFIDENCE_FACTOR_EDEFAULT.equals(confidenceFactor);
		case Iec62264Package.OPERATIONS_CAPABILITY__START_TIME:
			return START_TIME_EDEFAULT == null ? startTime != null : !START_TIME_EDEFAULT.equals(startTime);
		case Iec62264Package.OPERATIONS_CAPABILITY__END_TIME:
			return END_TIME_EDEFAULT == null ? endTime != null : !END_TIME_EDEFAULT.equals(endTime);
		case Iec62264Package.OPERATIONS_CAPABILITY__PUBLISHED_DATE:
			return PUBLISHED_DATE_EDEFAULT == null ? publishedDate != null
					: !PUBLISHED_DATE_EDEFAULT.equals(publishedDate);
		case Iec62264Package.OPERATIONS_CAPABILITY__HIERARCHY_SCOPE:
			return hierarchyScope != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", capacityType: ");
		result.append(capacityType);
		result.append(", reason: ");
		result.append(reason);
		result.append(", confidenceFactor: ");
		result.append(confidenceFactor);
		result.append(", startTime: ");
		result.append(startTime);
		result.append(", endTime: ");
		result.append(endTime);
		result.append(", publishedDate: ");
		result.append(publishedDate);
		result.append(')');
		return result.toString();
	}

} //OperationsCapabilityImpl
