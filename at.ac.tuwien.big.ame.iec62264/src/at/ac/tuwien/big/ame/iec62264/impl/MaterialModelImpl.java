/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialClass;
import at.ac.tuwien.big.ame.iec62264.MaterialDefinition;
import at.ac.tuwien.big.ame.iec62264.MaterialLot;
import at.ac.tuwien.big.ame.iec62264.MaterialModel;
import at.ac.tuwien.big.ame.iec62264.MaterialSublot;
import at.ac.tuwien.big.ame.iec62264.MaterialTestResult;
import at.ac.tuwien.big.ame.iec62264.MaterialTestSpecification;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialModelImpl#getMaterialClasses <em>Material Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialModelImpl#getMaterialDefinitions <em>Material Definitions</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialModelImpl#getMaterialLots <em>Material Lots</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialModelImpl#getMaterialSublots <em>Material Sublots</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialModelImpl#getTestSpecifications <em>Test Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialModelImpl#getTestResults <em>Test Results</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MaterialModelImpl extends MinimalEObjectImpl.Container implements MaterialModel {
	/**
	 * The cached value of the '{@link #getMaterialClasses() <em>Material Classes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialClass> materialClasses;

	/**
	 * The cached value of the '{@link #getMaterialDefinitions() <em>Material Definitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinitions()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialDefinition> materialDefinitions;

	/**
	 * The cached value of the '{@link #getMaterialLots() <em>Material Lots</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialLots()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialLot> materialLots;

	/**
	 * The cached value of the '{@link #getMaterialSublots() <em>Material Sublots</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSublots()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSublot> materialSublots;

	/**
	 * The cached value of the '{@link #getTestSpecifications() <em>Test Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialTestSpecification> testSpecifications;

	/**
	 * The cached value of the '{@link #getTestResults() <em>Test Results</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestResults()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialTestResult> testResults;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.MATERIAL_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialClass> getMaterialClasses() {
		if (materialClasses == null) {
			materialClasses = new EObjectContainmentEList<MaterialClass>(MaterialClass.class, this,
					Iec62264Package.MATERIAL_MODEL__MATERIAL_CLASSES);
		}
		return materialClasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialDefinition> getMaterialDefinitions() {
		if (materialDefinitions == null) {
			materialDefinitions = new EObjectContainmentEList<MaterialDefinition>(MaterialDefinition.class, this,
					Iec62264Package.MATERIAL_MODEL__MATERIAL_DEFINITIONS);
		}
		return materialDefinitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialLot> getMaterialLots() {
		if (materialLots == null) {
			materialLots = new EObjectContainmentEList<MaterialLot>(MaterialLot.class, this,
					Iec62264Package.MATERIAL_MODEL__MATERIAL_LOTS);
		}
		return materialLots;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSublot> getMaterialSublots() {
		if (materialSublots == null) {
			materialSublots = new EObjectContainmentEList<MaterialSublot>(MaterialSublot.class, this,
					Iec62264Package.MATERIAL_MODEL__MATERIAL_SUBLOTS);
		}
		return materialSublots;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialTestSpecification> getTestSpecifications() {
		if (testSpecifications == null) {
			testSpecifications = new EObjectContainmentEList<MaterialTestSpecification>(MaterialTestSpecification.class,
					this, Iec62264Package.MATERIAL_MODEL__TEST_SPECIFICATIONS);
		}
		return testSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialTestResult> getTestResults() {
		if (testResults == null) {
			testResults = new EObjectContainmentEList<MaterialTestResult>(MaterialTestResult.class, this,
					Iec62264Package.MATERIAL_MODEL__TEST_RESULTS);
		}
		return testResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_CLASSES:
			return ((InternalEList<?>) getMaterialClasses()).basicRemove(otherEnd, msgs);
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_DEFINITIONS:
			return ((InternalEList<?>) getMaterialDefinitions()).basicRemove(otherEnd, msgs);
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_LOTS:
			return ((InternalEList<?>) getMaterialLots()).basicRemove(otherEnd, msgs);
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_SUBLOTS:
			return ((InternalEList<?>) getMaterialSublots()).basicRemove(otherEnd, msgs);
		case Iec62264Package.MATERIAL_MODEL__TEST_SPECIFICATIONS:
			return ((InternalEList<?>) getTestSpecifications()).basicRemove(otherEnd, msgs);
		case Iec62264Package.MATERIAL_MODEL__TEST_RESULTS:
			return ((InternalEList<?>) getTestResults()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_CLASSES:
			return getMaterialClasses();
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_DEFINITIONS:
			return getMaterialDefinitions();
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_LOTS:
			return getMaterialLots();
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_SUBLOTS:
			return getMaterialSublots();
		case Iec62264Package.MATERIAL_MODEL__TEST_SPECIFICATIONS:
			return getTestSpecifications();
		case Iec62264Package.MATERIAL_MODEL__TEST_RESULTS:
			return getTestResults();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_CLASSES:
			getMaterialClasses().clear();
			getMaterialClasses().addAll((Collection<? extends MaterialClass>) newValue);
			return;
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_DEFINITIONS:
			getMaterialDefinitions().clear();
			getMaterialDefinitions().addAll((Collection<? extends MaterialDefinition>) newValue);
			return;
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_LOTS:
			getMaterialLots().clear();
			getMaterialLots().addAll((Collection<? extends MaterialLot>) newValue);
			return;
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_SUBLOTS:
			getMaterialSublots().clear();
			getMaterialSublots().addAll((Collection<? extends MaterialSublot>) newValue);
			return;
		case Iec62264Package.MATERIAL_MODEL__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			getTestSpecifications().addAll((Collection<? extends MaterialTestSpecification>) newValue);
			return;
		case Iec62264Package.MATERIAL_MODEL__TEST_RESULTS:
			getTestResults().clear();
			getTestResults().addAll((Collection<? extends MaterialTestResult>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_CLASSES:
			getMaterialClasses().clear();
			return;
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_DEFINITIONS:
			getMaterialDefinitions().clear();
			return;
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_LOTS:
			getMaterialLots().clear();
			return;
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_SUBLOTS:
			getMaterialSublots().clear();
			return;
		case Iec62264Package.MATERIAL_MODEL__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			return;
		case Iec62264Package.MATERIAL_MODEL__TEST_RESULTS:
			getTestResults().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_CLASSES:
			return materialClasses != null && !materialClasses.isEmpty();
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_DEFINITIONS:
			return materialDefinitions != null && !materialDefinitions.isEmpty();
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_LOTS:
			return materialLots != null && !materialLots.isEmpty();
		case Iec62264Package.MATERIAL_MODEL__MATERIAL_SUBLOTS:
			return materialSublots != null && !materialSublots.isEmpty();
		case Iec62264Package.MATERIAL_MODEL__TEST_SPECIFICATIONS:
			return testSpecifications != null && !testSpecifications.isEmpty();
		case Iec62264Package.MATERIAL_MODEL__TEST_RESULTS:
			return testResults != null && !testResults.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MaterialModelImpl
