/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty;

import java.time.ZonedDateTime;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Capability Test Result</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestResultImpl#getTestSpecification <em>Test Specification</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestResultImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestResultImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestResultImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestResultImpl#getDate <em>Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestResultImpl#getResult <em>Result</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestResultImpl#getResultUnitOfMeasure <em>Result Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestResultImpl#getExpiration <em>Expiration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalAssetCapabilityTestResultImpl extends MinimalEObjectImpl.Container
		implements PhysicalAssetCapabilityTestResult {
	/**
	 * The cached value of the '{@link #getTestSpecification() <em>Test Specification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestSpecification()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetCapabilityTestSpecification testSpecification;

	/**
	 * The cached value of the '{@link #getProperty() <em>Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetProperty property;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDate() <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDate() <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime date = DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getResult() <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResult()
	 * @generated
	 * @ordered
	 */
	protected static final String RESULT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResult() <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResult()
	 * @generated
	 * @ordered
	 */
	protected String result = RESULT_EDEFAULT;

	/**
	 * The default value of the '{@link #getResultUnitOfMeasure() <em>Result Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String RESULT_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResultUnitOfMeasure() <em>Result Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String resultUnitOfMeasure = RESULT_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getExpiration() <em>Expiration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpiration()
	 * @generated
	 * @ordered
	 */
	protected static final XMLGregorianCalendar EXPIRATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExpiration() <em>Expiration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpiration()
	 * @generated
	 * @ordered
	 */
	protected XMLGregorianCalendar expiration = EXPIRATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetCapabilityTestResultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetCapabilityTestSpecification getTestSpecification() {
		if (testSpecification != null && testSpecification.eIsProxy()) {
			InternalEObject oldTestSpecification = (InternalEObject) testSpecification;
			testSpecification = (PhysicalAssetCapabilityTestSpecification) eResolveProxy(oldTestSpecification);
			if (testSpecification != oldTestSpecification) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__TEST_SPECIFICATION,
							oldTestSpecification, testSpecification));
			}
		}
		return testSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetCapabilityTestSpecification basicGetTestSpecification() {
		return testSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestSpecification(PhysicalAssetCapabilityTestSpecification newTestSpecification) {
		PhysicalAssetCapabilityTestSpecification oldTestSpecification = testSpecification;
		testSpecification = newTestSpecification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__TEST_SPECIFICATION, oldTestSpecification,
					testSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetProperty getProperty() {
		if (property != null && property.eIsProxy()) {
			InternalEObject oldProperty = (InternalEObject) property;
			property = (PhysicalAssetProperty) eResolveProxy(oldProperty);
			if (property != oldProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__PROPERTY, oldProperty, property));
			}
		}
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetProperty basicGetProperty() {
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProperty(PhysicalAssetProperty newProperty) {
		PhysicalAssetProperty oldProperty = property;
		property = newProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__PROPERTY, oldProperty, property));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getDate() {
		return date;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDate(ZonedDateTime newDate) {
		ZonedDateTime oldDate = date;
		date = newDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DATE, oldDate, date));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getResult() {
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResult(String newResult) {
		String oldResult = result;
		result = newResult;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT, oldResult, result));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getResultUnitOfMeasure() {
		return resultUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResultUnitOfMeasure(String newResultUnitOfMeasure) {
		String oldResultUnitOfMeasure = resultUnitOfMeasure;
		resultUnitOfMeasure = newResultUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT_UNIT_OF_MEASURE,
					oldResultUnitOfMeasure, resultUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLGregorianCalendar getExpiration() {
		return expiration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpiration(XMLGregorianCalendar newExpiration) {
		XMLGregorianCalendar oldExpiration = expiration;
		expiration = newExpiration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__EXPIRATION, oldExpiration, expiration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__TEST_SPECIFICATION:
			if (resolve)
				return getTestSpecification();
			return basicGetTestSpecification();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__PROPERTY:
			if (resolve)
				return getProperty();
			return basicGetProperty();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__ID:
			return getId();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DESCRIPTION:
			return getDescription();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DATE:
			return getDate();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT:
			return getResult();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT_UNIT_OF_MEASURE:
			return getResultUnitOfMeasure();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__EXPIRATION:
			return getExpiration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__TEST_SPECIFICATION:
			setTestSpecification((PhysicalAssetCapabilityTestSpecification) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__PROPERTY:
			setProperty((PhysicalAssetProperty) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DATE:
			setDate((ZonedDateTime) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT:
			setResult((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT_UNIT_OF_MEASURE:
			setResultUnitOfMeasure((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__EXPIRATION:
			setExpiration((XMLGregorianCalendar) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__TEST_SPECIFICATION:
			setTestSpecification((PhysicalAssetCapabilityTestSpecification) null);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__PROPERTY:
			setProperty((PhysicalAssetProperty) null);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DATE:
			setDate(DATE_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT:
			setResult(RESULT_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT_UNIT_OF_MEASURE:
			setResultUnitOfMeasure(RESULT_UNIT_OF_MEASURE_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__EXPIRATION:
			setExpiration(EXPIRATION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__TEST_SPECIFICATION:
			return testSpecification != null;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__PROPERTY:
			return property != null;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DATE:
			return DATE_EDEFAULT == null ? date != null : !DATE_EDEFAULT.equals(date);
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT:
			return RESULT_EDEFAULT == null ? result != null : !RESULT_EDEFAULT.equals(result);
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT_UNIT_OF_MEASURE:
			return RESULT_UNIT_OF_MEASURE_EDEFAULT == null ? resultUnitOfMeasure != null
					: !RESULT_UNIT_OF_MEASURE_EDEFAULT.equals(resultUnitOfMeasure);
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__EXPIRATION:
			return EXPIRATION_EDEFAULT == null ? expiration != null : !EXPIRATION_EDEFAULT.equals(expiration);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", date: ");
		result.append(date);
		result.append(", result: ");
		result.append(result);
		result.append(", resultUnitOfMeasure: ");
		result.append(resultUnitOfMeasure);
		result.append(", expiration: ");
		result.append(expiration);
		result.append(')');
		return result.toString();
	}

} //PhysicalAssetCapabilityTestResultImpl
