/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Equipment;
import at.ac.tuwien.big.ame.iec62264.EquipmentActual;
import at.ac.tuwien.big.ame.iec62264.EquipmentActualProperty;
import at.ac.tuwien.big.ame.iec62264.EquipmentClass;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;

import java.math.BigDecimal;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Actual</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualImpl#getEquipmentActualProperties <em>Equipment Actual Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualImpl#getEquipmentUse <em>Equipment Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualImpl#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualImpl#getEquipmentClasses <em>Equipment Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualImpl#getEquipments <em>Equipments</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EquipmentActualImpl extends MinimalEObjectImpl.Container implements EquipmentActual {
	/**
	 * The cached value of the '{@link #getEquipmentActualProperties() <em>Equipment Actual Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentActualProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentActualProperty> equipmentActualProperties;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getEquipmentUse() <em>Equipment Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentUse()
	 * @generated
	 * @ordered
	 */
	protected static final String EQUIPMENT_USE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEquipmentUse() <em>Equipment Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentUse()
	 * @generated
	 * @ordered
	 */
	protected String equipmentUse = EQUIPMENT_USE_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal QUANTITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal quantity = QUANTITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String QUANTITY_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String quantityUnitOfMeasure = QUANTITY_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEquipmentClasses() <em>Equipment Classes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentClass> equipmentClasses;

	/**
	 * The cached value of the '{@link #getEquipments() <em>Equipments</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipments()
	 * @generated
	 * @ordered
	 */
	protected EList<Equipment> equipments;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentActualImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.EQUIPMENT_ACTUAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentActualProperty> getEquipmentActualProperties() {
		if (equipmentActualProperties == null) {
			equipmentActualProperties = new EObjectContainmentEList<EquipmentActualProperty>(
					EquipmentActualProperty.class, this, Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_ACTUAL_PROPERTIES);
		}
		return equipmentActualProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_ACTUAL__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEquipmentUse() {
		return equipmentUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentUse(String newEquipmentUse) {
		String oldEquipmentUse = equipmentUse;
		equipmentUse = newEquipmentUse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_USE,
					oldEquipmentUse, equipmentUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(BigDecimal newQuantity) {
		BigDecimal oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_ACTUAL__QUANTITY,
					oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQuantityUnitOfMeasure() {
		return quantityUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantityUnitOfMeasure(String newQuantityUnitOfMeasure) {
		String oldQuantityUnitOfMeasure = quantityUnitOfMeasure;
		quantityUnitOfMeasure = newQuantityUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.EQUIPMENT_ACTUAL__QUANTITY_UNIT_OF_MEASURE, oldQuantityUnitOfMeasure,
					quantityUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentClass> getEquipmentClasses() {
		if (equipmentClasses == null) {
			equipmentClasses = new EObjectResolvingEList<EquipmentClass>(EquipmentClass.class, this,
					Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_CLASSES);
		}
		return equipmentClasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Equipment> getEquipments() {
		if (equipments == null) {
			equipments = new EObjectResolvingEList<Equipment>(Equipment.class, this,
					Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENTS);
		}
		return equipments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_ACTUAL_PROPERTIES:
			return ((InternalEList<?>) getEquipmentActualProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_ACTUAL_PROPERTIES:
			return getEquipmentActualProperties();
		case Iec62264Package.EQUIPMENT_ACTUAL__DESCRIPTION:
			return getDescription();
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_USE:
			return getEquipmentUse();
		case Iec62264Package.EQUIPMENT_ACTUAL__QUANTITY:
			return getQuantity();
		case Iec62264Package.EQUIPMENT_ACTUAL__QUANTITY_UNIT_OF_MEASURE:
			return getQuantityUnitOfMeasure();
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_CLASSES:
			return getEquipmentClasses();
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENTS:
			return getEquipments();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_ACTUAL_PROPERTIES:
			getEquipmentActualProperties().clear();
			getEquipmentActualProperties().addAll((Collection<? extends EquipmentActualProperty>) newValue);
			return;
		case Iec62264Package.EQUIPMENT_ACTUAL__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_USE:
			setEquipmentUse((String) newValue);
			return;
		case Iec62264Package.EQUIPMENT_ACTUAL__QUANTITY:
			setQuantity((BigDecimal) newValue);
			return;
		case Iec62264Package.EQUIPMENT_ACTUAL__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure((String) newValue);
			return;
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_CLASSES:
			getEquipmentClasses().clear();
			getEquipmentClasses().addAll((Collection<? extends EquipmentClass>) newValue);
			return;
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENTS:
			getEquipments().clear();
			getEquipments().addAll((Collection<? extends Equipment>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_ACTUAL_PROPERTIES:
			getEquipmentActualProperties().clear();
			return;
		case Iec62264Package.EQUIPMENT_ACTUAL__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_USE:
			setEquipmentUse(EQUIPMENT_USE_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_ACTUAL__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_ACTUAL__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure(QUANTITY_UNIT_OF_MEASURE_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_CLASSES:
			getEquipmentClasses().clear();
			return;
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENTS:
			getEquipments().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_ACTUAL_PROPERTIES:
			return equipmentActualProperties != null && !equipmentActualProperties.isEmpty();
		case Iec62264Package.EQUIPMENT_ACTUAL__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_USE:
			return EQUIPMENT_USE_EDEFAULT == null ? equipmentUse != null : !EQUIPMENT_USE_EDEFAULT.equals(equipmentUse);
		case Iec62264Package.EQUIPMENT_ACTUAL__QUANTITY:
			return QUANTITY_EDEFAULT == null ? quantity != null : !QUANTITY_EDEFAULT.equals(quantity);
		case Iec62264Package.EQUIPMENT_ACTUAL__QUANTITY_UNIT_OF_MEASURE:
			return QUANTITY_UNIT_OF_MEASURE_EDEFAULT == null ? quantityUnitOfMeasure != null
					: !QUANTITY_UNIT_OF_MEASURE_EDEFAULT.equals(quantityUnitOfMeasure);
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENT_CLASSES:
			return equipmentClasses != null && !equipmentClasses.isEmpty();
		case Iec62264Package.EQUIPMENT_ACTUAL__EQUIPMENTS:
			return equipments != null && !equipments.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", equipmentUse: ");
		result.append(equipmentUse);
		result.append(", quantity: ");
		result.append(quantity);
		result.append(", quantityUnitOfMeasure: ");
		result.append(quantityUnitOfMeasure);
		result.append(')');
		return result.toString();
	}

} //EquipmentActualImpl
