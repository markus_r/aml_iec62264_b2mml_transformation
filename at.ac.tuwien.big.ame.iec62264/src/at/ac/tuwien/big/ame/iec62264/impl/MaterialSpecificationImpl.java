/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.AssemblyRelationship;
import at.ac.tuwien.big.ame.iec62264.AssemblyType;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialClass;
import at.ac.tuwien.big.ame.iec62264.MaterialDefinition;
import at.ac.tuwien.big.ame.iec62264.MaterialSpecification;
import at.ac.tuwien.big.ame.iec62264.MaterialSpecificationProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialUse;

import java.math.BigDecimal;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl#getAssembledFromSpecifications <em>Assembled From Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl#getMaterialSpecificationProperties <em>Material Specification Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl#getMaterialClasses <em>Material Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl#getMaterialDefinitions <em>Material Definitions</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MaterialSpecificationImpl extends MinimalEObjectImpl.Container implements MaterialSpecification {
	/**
	 * The cached value of the '{@link #getAssembledFromSpecifications() <em>Assembled From Specifications</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssembledFromSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSpecification> assembledFromSpecifications;

	/**
	 * The cached value of the '{@link #getMaterialSpecificationProperties() <em>Material Specification Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSpecificationProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSpecificationProperty> materialSpecificationProperties;

	/**
	 * The cached value of the '{@link #getMaterialClasses() <em>Material Classes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialClass> materialClasses;

	/**
	 * The cached value of the '{@link #getMaterialDefinitions() <em>Material Definitions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinitions()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialDefinition> materialDefinitions;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaterialUse() <em>Material Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialUse()
	 * @generated
	 * @ordered
	 */
	protected static final MaterialUse MATERIAL_USE_EDEFAULT = MaterialUse.CONSUMABLE;

	/**
	 * The cached value of the '{@link #getMaterialUse() <em>Material Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialUse()
	 * @generated
	 * @ordered
	 */
	protected MaterialUse materialUse = MATERIAL_USE_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal QUANTITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal quantity = QUANTITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String QUANTITY_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String quantityUnitOfMeasure = QUANTITY_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected static final AssemblyType ASSEMBLY_TYPE_EDEFAULT = AssemblyType.PHYSICAL;

	/**
	 * The cached value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected AssemblyType assemblyType = ASSEMBLY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected static final AssemblyRelationship ASSEMBLY_RELATIONSHIP_EDEFAULT = AssemblyRelationship.PERMANENT;

	/**
	 * The cached value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected AssemblyRelationship assemblyRelationship = ASSEMBLY_RELATIONSHIP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.MATERIAL_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSpecification> getAssembledFromSpecifications() {
		if (assembledFromSpecifications == null) {
			assembledFromSpecifications = new EObjectResolvingEList<MaterialSpecification>(MaterialSpecification.class,
					this, Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLED_FROM_SPECIFICATIONS);
		}
		return assembledFromSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSpecificationProperty> getMaterialSpecificationProperties() {
		if (materialSpecificationProperties == null) {
			materialSpecificationProperties = new EObjectContainmentEList<MaterialSpecificationProperty>(
					MaterialSpecificationProperty.class, this,
					Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_SPECIFICATION_PROPERTIES);
		}
		return materialSpecificationProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialClass> getMaterialClasses() {
		if (materialClasses == null) {
			materialClasses = new EObjectResolvingEList<MaterialClass>(MaterialClass.class, this,
					Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_CLASSES);
		}
		return materialClasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialDefinition> getMaterialDefinitions() {
		if (materialDefinitions == null) {
			materialDefinitions = new EObjectResolvingEList<MaterialDefinition>(MaterialDefinition.class, this,
					Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_DEFINITIONS);
		}
		return materialDefinitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_SPECIFICATION__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialUse getMaterialUse() {
		return materialUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialUse(MaterialUse newMaterialUse) {
		MaterialUse oldMaterialUse = materialUse;
		materialUse = newMaterialUse == null ? MATERIAL_USE_EDEFAULT : newMaterialUse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_USE,
					oldMaterialUse, materialUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(BigDecimal newQuantity) {
		BigDecimal oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_SPECIFICATION__QUANTITY,
					oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQuantityUnitOfMeasure() {
		return quantityUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantityUnitOfMeasure(String newQuantityUnitOfMeasure) {
		String oldQuantityUnitOfMeasure = quantityUnitOfMeasure;
		quantityUnitOfMeasure = newQuantityUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.MATERIAL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE, oldQuantityUnitOfMeasure,
					quantityUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyType getAssemblyType() {
		return assemblyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyType(AssemblyType newAssemblyType) {
		AssemblyType oldAssemblyType = assemblyType;
		assemblyType = newAssemblyType == null ? ASSEMBLY_TYPE_EDEFAULT : newAssemblyType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLY_TYPE,
					oldAssemblyType, assemblyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationship getAssemblyRelationship() {
		return assemblyRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyRelationship(AssemblyRelationship newAssemblyRelationship) {
		AssemblyRelationship oldAssemblyRelationship = assemblyRelationship;
		assemblyRelationship = newAssemblyRelationship == null ? ASSEMBLY_RELATIONSHIP_EDEFAULT
				: newAssemblyRelationship;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLY_RELATIONSHIP, oldAssemblyRelationship,
					assemblyRelationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_SPECIFICATION_PROPERTIES:
			return ((InternalEList<?>) getMaterialSpecificationProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLED_FROM_SPECIFICATIONS:
			return getAssembledFromSpecifications();
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_SPECIFICATION_PROPERTIES:
			return getMaterialSpecificationProperties();
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_CLASSES:
			return getMaterialClasses();
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_DEFINITIONS:
			return getMaterialDefinitions();
		case Iec62264Package.MATERIAL_SPECIFICATION__DESCRIPTION:
			return getDescription();
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_USE:
			return getMaterialUse();
		case Iec62264Package.MATERIAL_SPECIFICATION__QUANTITY:
			return getQuantity();
		case Iec62264Package.MATERIAL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			return getQuantityUnitOfMeasure();
		case Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLY_TYPE:
			return getAssemblyType();
		case Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLY_RELATIONSHIP:
			return getAssemblyRelationship();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLED_FROM_SPECIFICATIONS:
			getAssembledFromSpecifications().clear();
			getAssembledFromSpecifications().addAll((Collection<? extends MaterialSpecification>) newValue);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_SPECIFICATION_PROPERTIES:
			getMaterialSpecificationProperties().clear();
			getMaterialSpecificationProperties().addAll((Collection<? extends MaterialSpecificationProperty>) newValue);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_CLASSES:
			getMaterialClasses().clear();
			getMaterialClasses().addAll((Collection<? extends MaterialClass>) newValue);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_DEFINITIONS:
			getMaterialDefinitions().clear();
			getMaterialDefinitions().addAll((Collection<? extends MaterialDefinition>) newValue);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_USE:
			setMaterialUse((MaterialUse) newValue);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__QUANTITY:
			setQuantity((BigDecimal) newValue);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure((String) newValue);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLY_TYPE:
			setAssemblyType((AssemblyType) newValue);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLY_RELATIONSHIP:
			setAssemblyRelationship((AssemblyRelationship) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLED_FROM_SPECIFICATIONS:
			getAssembledFromSpecifications().clear();
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_SPECIFICATION_PROPERTIES:
			getMaterialSpecificationProperties().clear();
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_CLASSES:
			getMaterialClasses().clear();
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_DEFINITIONS:
			getMaterialDefinitions().clear();
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_USE:
			setMaterialUse(MATERIAL_USE_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure(QUANTITY_UNIT_OF_MEASURE_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLY_TYPE:
			setAssemblyType(ASSEMBLY_TYPE_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLY_RELATIONSHIP:
			setAssemblyRelationship(ASSEMBLY_RELATIONSHIP_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLED_FROM_SPECIFICATIONS:
			return assembledFromSpecifications != null && !assembledFromSpecifications.isEmpty();
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_SPECIFICATION_PROPERTIES:
			return materialSpecificationProperties != null && !materialSpecificationProperties.isEmpty();
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_CLASSES:
			return materialClasses != null && !materialClasses.isEmpty();
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_DEFINITIONS:
			return materialDefinitions != null && !materialDefinitions.isEmpty();
		case Iec62264Package.MATERIAL_SPECIFICATION__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.MATERIAL_SPECIFICATION__MATERIAL_USE:
			return materialUse != MATERIAL_USE_EDEFAULT;
		case Iec62264Package.MATERIAL_SPECIFICATION__QUANTITY:
			return QUANTITY_EDEFAULT == null ? quantity != null : !QUANTITY_EDEFAULT.equals(quantity);
		case Iec62264Package.MATERIAL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			return QUANTITY_UNIT_OF_MEASURE_EDEFAULT == null ? quantityUnitOfMeasure != null
					: !QUANTITY_UNIT_OF_MEASURE_EDEFAULT.equals(quantityUnitOfMeasure);
		case Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLY_TYPE:
			return assemblyType != ASSEMBLY_TYPE_EDEFAULT;
		case Iec62264Package.MATERIAL_SPECIFICATION__ASSEMBLY_RELATIONSHIP:
			return assemblyRelationship != ASSEMBLY_RELATIONSHIP_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", materialUse: ");
		result.append(materialUse);
		result.append(", quantity: ");
		result.append(quantity);
		result.append(", quantityUnitOfMeasure: ");
		result.append(quantityUnitOfMeasure);
		result.append(", assemblyType: ");
		result.append(assemblyType);
		result.append(", assemblyRelationship: ");
		result.append(assemblyRelationship);
		result.append(')');
		return result.toString();
	}

} //MaterialSpecificationImpl
