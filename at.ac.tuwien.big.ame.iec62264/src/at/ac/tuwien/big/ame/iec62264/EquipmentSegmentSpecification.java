/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Segment Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getProperties <em>Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getEquipmentClasses <em>Equipment Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getEquipments <em>Equipments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getEquipmentUse <em>Equipment Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getQuantityUnitOfMeasurement <em>Quantity Unit Of Measurement</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentSegmentSpecification()
 * @model
 * @generated
 */
public interface EquipmentSegmentSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecificationProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentSegmentSpecification_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<EquipmentSegmentSpecificationProperty> getProperties();

	/**
	 * Returns the value of the '<em><b>Equipment Classes</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.EquipmentClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Classes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Classes</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentSegmentSpecification_EquipmentClasses()
	 * @model
	 * @generated
	 */
	EList<EquipmentClass> getEquipmentClasses();

	/**
	 * Returns the value of the '<em><b>Equipments</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.Equipment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipments</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipments</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentSegmentSpecification_Equipments()
	 * @model
	 * @generated
	 */
	EList<Equipment> getEquipments();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentSegmentSpecification_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Equipment Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Use</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Use</em>' attribute.
	 * @see #setEquipmentUse(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentSegmentSpecification_EquipmentUse()
	 * @model
	 * @generated
	 */
	String getEquipmentUse();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getEquipmentUse <em>Equipment Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Use</em>' attribute.
	 * @see #getEquipmentUse()
	 * @generated
	 */
	void setEquipmentUse(String value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(BigDecimal)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentSegmentSpecification_Quantity()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 * @generated
	 */
	BigDecimal getQuantity();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getQuantity <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Quantity Unit Of Measurement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity Unit Of Measurement</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity Unit Of Measurement</em>' attribute.
	 * @see #setQuantityUnitOfMeasurement(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentSegmentSpecification_QuantityUnitOfMeasurement()
	 * @model
	 * @generated
	 */
	String getQuantityUnitOfMeasurement();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getQuantityUnitOfMeasurement <em>Quantity Unit Of Measurement</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity Unit Of Measurement</em>' attribute.
	 * @see #getQuantityUnitOfMeasurement()
	 * @generated
	 */
	void setQuantityUnitOfMeasurement(String value);

} // EquipmentSegmentSpecification
