/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.time.ZonedDateTime;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Asset Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getPhysicalAsset <em>Physical Asset</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getEquipment <em>Equipment</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getEndTime <em>End Time</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentAssetMapping()
 * @model
 * @generated
 */
public interface EquipmentAssetMapping extends EObject {
	/**
	 * Returns the value of the '<em><b>Physical Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset</em>' reference.
	 * @see #setPhysicalAsset(PhysicalAsset)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentAssetMapping_PhysicalAsset()
	 * @model required="true"
	 * @generated
	 */
	PhysicalAsset getPhysicalAsset();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getPhysicalAsset <em>Physical Asset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset</em>' reference.
	 * @see #getPhysicalAsset()
	 * @generated
	 */
	void setPhysicalAsset(PhysicalAsset value);

	/**
	 * Returns the value of the '<em><b>Equipment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment</em>' reference.
	 * @see #setEquipment(Equipment)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentAssetMapping_Equipment()
	 * @model required="true"
	 * @generated
	 */
	Equipment getEquipment();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getEquipment <em>Equipment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment</em>' reference.
	 * @see #getEquipment()
	 * @generated
	 */
	void setEquipment(Equipment value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentAssetMapping_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentAssetMapping_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentAssetMapping_StartTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentAssetMapping_EndTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(ZonedDateTime value);

} // EquipmentAssetMapping
