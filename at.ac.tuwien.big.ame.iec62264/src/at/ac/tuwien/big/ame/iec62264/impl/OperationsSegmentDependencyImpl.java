/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.OperationsSegment;
import at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Segment Dependency</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentDependencyImpl#getSubject <em>Subject</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentDependencyImpl#getDependency <em>Dependency</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentDependencyImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentDependencyImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentDependencyImpl#getDependencyType <em>Dependency Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentDependencyImpl#getDependencyFactor <em>Dependency Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentDependencyImpl#getUnitOfMeasure <em>Unit Of Measure</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsSegmentDependencyImpl extends MinimalEObjectImpl.Container
		implements OperationsSegmentDependency {
	/**
	 * The cached value of the '{@link #getSubject() <em>Subject</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubject()
	 * @generated
	 * @ordered
	 */
	protected OperationsSegment subject;

	/**
	 * The cached value of the '{@link #getDependency() <em>Dependency</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependency()
	 * @generated
	 * @ordered
	 */
	protected OperationsSegment dependency;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDependencyType() <em>Dependency Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencyType()
	 * @generated
	 * @ordered
	 */
	protected static final String DEPENDENCY_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDependencyType() <em>Dependency Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencyType()
	 * @generated
	 * @ordered
	 */
	protected String dependencyType = DEPENDENCY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDependencyFactor() <em>Dependency Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencyFactor()
	 * @generated
	 * @ordered
	 */
	protected static final String DEPENDENCY_FACTOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDependencyFactor() <em>Dependency Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencyFactor()
	 * @generated
	 * @ordered
	 */
	protected String dependencyFactor = DEPENDENCY_FACTOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitOfMeasure() <em>Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitOfMeasure() <em>Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String unitOfMeasure = UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsSegmentDependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.OPERATIONS_SEGMENT_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsSegment getSubject() {
		if (subject != null && subject.eIsProxy()) {
			InternalEObject oldSubject = (InternalEObject) subject;
			subject = (OperationsSegment) eResolveProxy(oldSubject);
			if (subject != oldSubject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__SUBJECT, oldSubject, subject));
			}
		}
		return subject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsSegment basicGetSubject() {
		return subject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubject(OperationsSegment newSubject) {
		OperationsSegment oldSubject = subject;
		subject = newSubject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__SUBJECT, oldSubject, subject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsSegment getDependency() {
		if (dependency != null && dependency.eIsProxy()) {
			InternalEObject oldDependency = (InternalEObject) dependency;
			dependency = (OperationsSegment) eResolveProxy(oldDependency);
			if (dependency != oldDependency) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY, oldDependency, dependency));
			}
		}
		return dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsSegment basicGetDependency() {
		return dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependency(OperationsSegment newDependency) {
		OperationsSegment oldDependency = dependency;
		dependency = newDependency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY, oldDependency, dependency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__ID,
					oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDependencyType() {
		return dependencyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependencyType(String newDependencyType) {
		String oldDependencyType = dependencyType;
		dependencyType = newDependencyType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE, oldDependencyType, dependencyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDependencyFactor() {
		return dependencyFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependencyFactor(String newDependencyFactor) {
		String oldDependencyFactor = dependencyFactor;
		dependencyFactor = newDependencyFactor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR, oldDependencyFactor,
					dependencyFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitOfMeasure(String newUnitOfMeasure) {
		String oldUnitOfMeasure = unitOfMeasure;
		unitOfMeasure = newUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__UNIT_OF_MEASURE, oldUnitOfMeasure, unitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__SUBJECT:
			if (resolve)
				return getSubject();
			return basicGetSubject();
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY:
			if (resolve)
				return getDependency();
			return basicGetDependency();
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__ID:
			return getId();
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DESCRIPTION:
			return getDescription();
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE:
			return getDependencyType();
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR:
			return getDependencyFactor();
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__UNIT_OF_MEASURE:
			return getUnitOfMeasure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__SUBJECT:
			setSubject((OperationsSegment) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY:
			setDependency((OperationsSegment) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE:
			setDependencyType((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR:
			setDependencyFactor((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__UNIT_OF_MEASURE:
			setUnitOfMeasure((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__SUBJECT:
			setSubject((OperationsSegment) null);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY:
			setDependency((OperationsSegment) null);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE:
			setDependencyType(DEPENDENCY_TYPE_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR:
			setDependencyFactor(DEPENDENCY_FACTOR_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__UNIT_OF_MEASURE:
			setUnitOfMeasure(UNIT_OF_MEASURE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__SUBJECT:
			return subject != null;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY:
			return dependency != null;
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE:
			return DEPENDENCY_TYPE_EDEFAULT == null ? dependencyType != null
					: !DEPENDENCY_TYPE_EDEFAULT.equals(dependencyType);
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR:
			return DEPENDENCY_FACTOR_EDEFAULT == null ? dependencyFactor != null
					: !DEPENDENCY_FACTOR_EDEFAULT.equals(dependencyFactor);
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY__UNIT_OF_MEASURE:
			return UNIT_OF_MEASURE_EDEFAULT == null ? unitOfMeasure != null
					: !UNIT_OF_MEASURE_EDEFAULT.equals(unitOfMeasure);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", dependencyType: ");
		result.append(dependencyType);
		result.append(", dependencyFactor: ");
		result.append(dependencyFactor);
		result.append(", unitOfMeasure: ");
		result.append(unitOfMeasure);
		result.append(')');
		return result.toString();
	}

} //OperationsSegmentDependencyImpl
