/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getAssembledFromSpecifications <em>Assembled From Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialSpecificationProperties <em>Material Specification Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialClasses <em>Material Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialDefinitions <em>Material Definitions</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSpecification()
 * @model
 * @generated
 */
public interface MaterialSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Assembled From Specifications</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembled From Specifications</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembled From Specifications</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSpecification_AssembledFromSpecifications()
	 * @model
	 * @generated
	 */
	EList<MaterialSpecification> getAssembledFromSpecifications();

	/**
	 * Returns the value of the '<em><b>Material Specification Properties</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialSpecificationProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Specification Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Specification Properties</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSpecification_MaterialSpecificationProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<MaterialSpecificationProperty> getMaterialSpecificationProperties();

	/**
	 * Returns the value of the '<em><b>Material Classes</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Classes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Classes</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSpecification_MaterialClasses()
	 * @model
	 * @generated
	 */
	EList<MaterialClass> getMaterialClasses();

	/**
	 * Returns the value of the '<em><b>Material Definitions</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Definitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Definitions</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSpecification_MaterialDefinitions()
	 * @model
	 * @generated
	 */
	EList<MaterialDefinition> getMaterialDefinitions();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSpecification_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Material Use</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.MaterialUse}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Use</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Use</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialUse
	 * @see #setMaterialUse(MaterialUse)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSpecification_MaterialUse()
	 * @model
	 * @generated
	 */
	MaterialUse getMaterialUse();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialUse <em>Material Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Use</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialUse
	 * @see #getMaterialUse()
	 * @generated
	 */
	void setMaterialUse(MaterialUse value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(BigDecimal)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSpecification_Quantity()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 * @generated
	 */
	BigDecimal getQuantity();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getQuantity <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity Unit Of Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity Unit Of Measure</em>' attribute.
	 * @see #setQuantityUnitOfMeasure(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSpecification_QuantityUnitOfMeasure()
	 * @model
	 * @generated
	 */
	String getQuantityUnitOfMeasure();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity Unit Of Measure</em>' attribute.
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 */
	void setQuantityUnitOfMeasure(String value);

	/**
	 * Returns the value of the '<em><b>Assembly Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.AssemblyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyType
	 * @see #setAssemblyType(AssemblyType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSpecification_AssemblyType()
	 * @model
	 * @generated
	 */
	AssemblyType getAssemblyType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getAssemblyType <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyType
	 * @see #getAssemblyType()
	 * @generated
	 */
	void setAssemblyType(AssemblyType value);

	/**
	 * Returns the value of the '<em><b>Assembly Relationship</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.AssemblyRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Relationship</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Relationship</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyRelationship
	 * @see #setAssemblyRelationship(AssemblyRelationship)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSpecification_AssemblyRelationship()
	 * @model
	 * @generated
	 */
	AssemblyRelationship getAssemblyRelationship();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getAssemblyRelationship <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Relationship</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyRelationship
	 * @see #getAssemblyRelationship()
	 * @generated
	 */
	void setAssemblyRelationship(AssemblyRelationship value);

} // MaterialSpecification
