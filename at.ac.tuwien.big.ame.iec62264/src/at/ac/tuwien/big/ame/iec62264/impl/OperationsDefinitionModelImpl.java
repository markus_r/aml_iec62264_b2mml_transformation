/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.OperationsDefinition;
import at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel;
import at.ac.tuwien.big.ame.iec62264.OperationsSegment;
import at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Definition Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionModelImpl#getOperationsDefinitions <em>Operations Definitions</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionModelImpl#getOperationsSegments <em>Operations Segments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionModelImpl#getOperationsSegmentDependencies <em>Operations Segment Dependencies</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsDefinitionModelImpl extends MinimalEObjectImpl.Container implements OperationsDefinitionModel {
	/**
	 * The cached value of the '{@link #getOperationsDefinitions() <em>Operations Definitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsDefinitions()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsDefinition> operationsDefinitions;

	/**
	 * The cached value of the '{@link #getOperationsSegments() <em>Operations Segments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsSegments()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsSegment> operationsSegments;

	/**
	 * The cached value of the '{@link #getOperationsSegmentDependencies() <em>Operations Segment Dependencies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsSegmentDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsSegmentDependency> operationsSegmentDependencies;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsDefinitionModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.OPERATIONS_DEFINITION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsDefinition> getOperationsDefinitions() {
		if (operationsDefinitions == null) {
			operationsDefinitions = new EObjectContainmentEList<OperationsDefinition>(OperationsDefinition.class, this,
					Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_DEFINITIONS);
		}
		return operationsDefinitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsSegment> getOperationsSegments() {
		if (operationsSegments == null) {
			operationsSegments = new EObjectContainmentEList<OperationsSegment>(OperationsSegment.class, this,
					Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENTS);
		}
		return operationsSegments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsSegmentDependency> getOperationsSegmentDependencies() {
		if (operationsSegmentDependencies == null) {
			operationsSegmentDependencies = new EObjectContainmentEList<OperationsSegmentDependency>(
					OperationsSegmentDependency.class, this,
					Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENT_DEPENDENCIES);
		}
		return operationsSegmentDependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_DEFINITIONS:
			return ((InternalEList<?>) getOperationsDefinitions()).basicRemove(otherEnd, msgs);
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENTS:
			return ((InternalEList<?>) getOperationsSegments()).basicRemove(otherEnd, msgs);
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENT_DEPENDENCIES:
			return ((InternalEList<?>) getOperationsSegmentDependencies()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_DEFINITIONS:
			return getOperationsDefinitions();
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENTS:
			return getOperationsSegments();
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENT_DEPENDENCIES:
			return getOperationsSegmentDependencies();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_DEFINITIONS:
			getOperationsDefinitions().clear();
			getOperationsDefinitions().addAll((Collection<? extends OperationsDefinition>) newValue);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENTS:
			getOperationsSegments().clear();
			getOperationsSegments().addAll((Collection<? extends OperationsSegment>) newValue);
			return;
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENT_DEPENDENCIES:
			getOperationsSegmentDependencies().clear();
			getOperationsSegmentDependencies().addAll((Collection<? extends OperationsSegmentDependency>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_DEFINITIONS:
			getOperationsDefinitions().clear();
			return;
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENTS:
			getOperationsSegments().clear();
			return;
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENT_DEPENDENCIES:
			getOperationsSegmentDependencies().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_DEFINITIONS:
			return operationsDefinitions != null && !operationsDefinitions.isEmpty();
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENTS:
			return operationsSegments != null && !operationsSegments.isEmpty();
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENT_DEPENDENCIES:
			return operationsSegmentDependencies != null && !operationsSegmentDependencies.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OperationsDefinitionModelImpl
