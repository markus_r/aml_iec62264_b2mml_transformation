/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicalAssetSpecificationProperties <em>Physical Asset Specification Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicAlassetClasses <em>Physic Alasset Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicalAssets <em>Physical Assets</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicalAssetUse <em>Physical Asset Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetSpecification()
 * @model
 * @generated
 */
public interface PhysicalAssetSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Physical Asset Specification Properties</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecificationProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Specification Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Specification Properties</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetSpecification_PhysicalAssetSpecificationProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<PhysicalAssetSpecificationProperty> getPhysicalAssetSpecificationProperties();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetSpecification_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Physic Alasset Classes</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physic Alasset Classes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physic Alasset Classes</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetSpecification_PhysicAlassetClasses()
	 * @model
	 * @generated
	 */
	EList<PhysicalAssetClass> getPhysicAlassetClasses();

	/**
	 * Returns the value of the '<em><b>Physical Assets</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Assets</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Assets</em>' reference.
	 * @see #setPhysicalAssets(PhysicalAsset)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetSpecification_PhysicalAssets()
	 * @model
	 * @generated
	 */
	PhysicalAsset getPhysicalAssets();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicalAssets <em>Physical Assets</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Assets</em>' reference.
	 * @see #getPhysicalAssets()
	 * @generated
	 */
	void setPhysicalAssets(PhysicalAsset value);

	/**
	 * Returns the value of the '<em><b>Physical Asset Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Use</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Use</em>' attribute.
	 * @see #setPhysicalAssetUse(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetSpecification_PhysicalAssetUse()
	 * @model
	 * @generated
	 */
	String getPhysicalAssetUse();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicalAssetUse <em>Physical Asset Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Use</em>' attribute.
	 * @see #getPhysicalAssetUse()
	 * @generated
	 */
	void setPhysicalAssetUse(String value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(BigDecimal)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetSpecification_Quantity()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 * @generated
	 */
	BigDecimal getQuantity();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getQuantity <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity Unit Of Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity Unit Of Measure</em>' attribute.
	 * @see #setQuantityUnitOfMeasure(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetSpecification_QuantityUnitOfMeasure()
	 * @model
	 * @generated
	 */
	String getQuantityUnitOfMeasure();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity Unit Of Measure</em>' attribute.
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 */
	void setQuantityUnitOfMeasure(String value);

} // PhysicalAssetSpecification
