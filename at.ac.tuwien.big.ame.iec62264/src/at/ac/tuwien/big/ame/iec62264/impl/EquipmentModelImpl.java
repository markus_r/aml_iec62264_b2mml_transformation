/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Equipment;
import at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult;
import at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification;
import at.ac.tuwien.big.ame.iec62264.EquipmentClass;
import at.ac.tuwien.big.ame.iec62264.EquipmentModel;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentModelImpl#getEquipmentClasses <em>Equipment Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentModelImpl#getEquipments <em>Equipments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentModelImpl#getTestSpecifications <em>Test Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentModelImpl#getTestResults <em>Test Results</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EquipmentModelImpl extends MinimalEObjectImpl.Container implements EquipmentModel {
	/**
	 * The cached value of the '{@link #getEquipmentClasses() <em>Equipment Classes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentClass> equipmentClasses;

	/**
	 * The cached value of the '{@link #getEquipments() <em>Equipments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipments()
	 * @generated
	 * @ordered
	 */
	protected EList<Equipment> equipments;

	/**
	 * The cached value of the '{@link #getTestSpecifications() <em>Test Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentCapabilityTestSpecification> testSpecifications;

	/**
	 * The cached value of the '{@link #getTestResults() <em>Test Results</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestResults()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentCapabilityTestResult> testResults;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.EQUIPMENT_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentClass> getEquipmentClasses() {
		if (equipmentClasses == null) {
			equipmentClasses = new EObjectContainmentEList<EquipmentClass>(EquipmentClass.class, this,
					Iec62264Package.EQUIPMENT_MODEL__EQUIPMENT_CLASSES);
		}
		return equipmentClasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Equipment> getEquipments() {
		if (equipments == null) {
			equipments = new EObjectContainmentEList<Equipment>(Equipment.class, this,
					Iec62264Package.EQUIPMENT_MODEL__EQUIPMENTS);
		}
		return equipments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentCapabilityTestSpecification> getTestSpecifications() {
		if (testSpecifications == null) {
			testSpecifications = new EObjectContainmentEList<EquipmentCapabilityTestSpecification>(
					EquipmentCapabilityTestSpecification.class, this,
					Iec62264Package.EQUIPMENT_MODEL__TEST_SPECIFICATIONS);
		}
		return testSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentCapabilityTestResult> getTestResults() {
		if (testResults == null) {
			testResults = new EObjectContainmentEList<EquipmentCapabilityTestResult>(
					EquipmentCapabilityTestResult.class, this, Iec62264Package.EQUIPMENT_MODEL__TEST_RESULTS);
		}
		return testResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_MODEL__EQUIPMENT_CLASSES:
			return ((InternalEList<?>) getEquipmentClasses()).basicRemove(otherEnd, msgs);
		case Iec62264Package.EQUIPMENT_MODEL__EQUIPMENTS:
			return ((InternalEList<?>) getEquipments()).basicRemove(otherEnd, msgs);
		case Iec62264Package.EQUIPMENT_MODEL__TEST_SPECIFICATIONS:
			return ((InternalEList<?>) getTestSpecifications()).basicRemove(otherEnd, msgs);
		case Iec62264Package.EQUIPMENT_MODEL__TEST_RESULTS:
			return ((InternalEList<?>) getTestResults()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_MODEL__EQUIPMENT_CLASSES:
			return getEquipmentClasses();
		case Iec62264Package.EQUIPMENT_MODEL__EQUIPMENTS:
			return getEquipments();
		case Iec62264Package.EQUIPMENT_MODEL__TEST_SPECIFICATIONS:
			return getTestSpecifications();
		case Iec62264Package.EQUIPMENT_MODEL__TEST_RESULTS:
			return getTestResults();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_MODEL__EQUIPMENT_CLASSES:
			getEquipmentClasses().clear();
			getEquipmentClasses().addAll((Collection<? extends EquipmentClass>) newValue);
			return;
		case Iec62264Package.EQUIPMENT_MODEL__EQUIPMENTS:
			getEquipments().clear();
			getEquipments().addAll((Collection<? extends Equipment>) newValue);
			return;
		case Iec62264Package.EQUIPMENT_MODEL__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			getTestSpecifications().addAll((Collection<? extends EquipmentCapabilityTestSpecification>) newValue);
			return;
		case Iec62264Package.EQUIPMENT_MODEL__TEST_RESULTS:
			getTestResults().clear();
			getTestResults().addAll((Collection<? extends EquipmentCapabilityTestResult>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_MODEL__EQUIPMENT_CLASSES:
			getEquipmentClasses().clear();
			return;
		case Iec62264Package.EQUIPMENT_MODEL__EQUIPMENTS:
			getEquipments().clear();
			return;
		case Iec62264Package.EQUIPMENT_MODEL__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			return;
		case Iec62264Package.EQUIPMENT_MODEL__TEST_RESULTS:
			getTestResults().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_MODEL__EQUIPMENT_CLASSES:
			return equipmentClasses != null && !equipmentClasses.isEmpty();
		case Iec62264Package.EQUIPMENT_MODEL__EQUIPMENTS:
			return equipments != null && !equipments.isEmpty();
		case Iec62264Package.EQUIPMENT_MODEL__TEST_SPECIFICATIONS:
			return testSpecifications != null && !testSpecifications.isEmpty();
		case Iec62264Package.EQUIPMENT_MODEL__TEST_RESULTS:
			return testResults != null && !testResults.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EquipmentModelImpl
