/**
 */
package at.ac.tuwien.big.ame.iec62264;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Referential Material Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialProperty#getMaterialLotProperty <em>Material Lot Property</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialMaterialProperty()
 * @model abstract="true"
 * @generated
 */
public interface ReferentialMaterialProperty extends ReferentialMaterialTypeProperty {
	/**
	 * Returns the value of the '<em><b>Material Lot Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Lot Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Lot Property</em>' reference.
	 * @see #setMaterialLotProperty(MaterialLotProperty)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialMaterialProperty_MaterialLotProperty()
	 * @model
	 * @generated
	 */
	MaterialLotProperty getMaterialLotProperty();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialProperty#getMaterialLotProperty <em>Material Lot Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Lot Property</em>' reference.
	 * @see #getMaterialLotProperty()
	 * @generated
	 */
	void setMaterialLotProperty(MaterialLotProperty value);

} // ReferentialMaterialProperty
