/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.AssemblyRelationship;
import at.ac.tuwien.big.ame.iec62264.AssemblyType;
import at.ac.tuwien.big.ame.iec62264.CapabilityType;
import at.ac.tuwien.big.ame.iec62264.CapacityType;
import at.ac.tuwien.big.ame.iec62264.Equipment;
import at.ac.tuwien.big.ame.iec62264.EquipmentActual;
import at.ac.tuwien.big.ame.iec62264.EquipmentActualProperty;
import at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping;
import at.ac.tuwien.big.ame.iec62264.EquipmentCapability;
import at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityProperty;
import at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult;
import at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification;
import at.ac.tuwien.big.ame.iec62264.EquipmentClass;
import at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty;
import at.ac.tuwien.big.ame.iec62264.EquipmentModel;
import at.ac.tuwien.big.ame.iec62264.EquipmentProperty;
import at.ac.tuwien.big.ame.iec62264.EquipmentRequirement;
import at.ac.tuwien.big.ame.iec62264.EquipmentRequirementProperty;
import at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification;
import at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecificationProperty;
import at.ac.tuwien.big.ame.iec62264.EquipmentSpecification;
import at.ac.tuwien.big.ame.iec62264.EquipmentSpecificationProperty;
import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Factory;
import at.ac.tuwien.big.ame.iec62264.Iec62264Model;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialActual;
import at.ac.tuwien.big.ame.iec62264.MaterialActualProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialCapability;
import at.ac.tuwien.big.ame.iec62264.MaterialCapabilityProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialClass;
import at.ac.tuwien.big.ame.iec62264.MaterialClassProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialDefinition;
import at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialLot;
import at.ac.tuwien.big.ame.iec62264.MaterialLotProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialModel;
import at.ac.tuwien.big.ame.iec62264.MaterialRequirement;
import at.ac.tuwien.big.ame.iec62264.MaterialRequirementProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification;
import at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecificationProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialSpecification;
import at.ac.tuwien.big.ame.iec62264.MaterialSpecificationProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialSublot;
import at.ac.tuwien.big.ame.iec62264.MaterialTestResult;
import at.ac.tuwien.big.ame.iec62264.MaterialTestSpecification;
import at.ac.tuwien.big.ame.iec62264.MaterialUse;
import at.ac.tuwien.big.ame.iec62264.OperationsCapability;
import at.ac.tuwien.big.ame.iec62264.OperationsCapabilityModel;
import at.ac.tuwien.big.ame.iec62264.OperationsDefinition;
import at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel;
import at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill;
import at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem;
import at.ac.tuwien.big.ame.iec62264.OperationsPerformance;
import at.ac.tuwien.big.ame.iec62264.OperationsPerformanceModel;
import at.ac.tuwien.big.ame.iec62264.OperationsRequest;
import at.ac.tuwien.big.ame.iec62264.OperationsResponse;
import at.ac.tuwien.big.ame.iec62264.OperationsSchedule;
import at.ac.tuwien.big.ame.iec62264.OperationsScheduleModel;
import at.ac.tuwien.big.ame.iec62264.OperationsSegment;
import at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency;
import at.ac.tuwien.big.ame.iec62264.OperationsType;
import at.ac.tuwien.big.ame.iec62264.ParameterSpecification;
import at.ac.tuwien.big.ame.iec62264.PerformanceState;
import at.ac.tuwien.big.ame.iec62264.Person;
import at.ac.tuwien.big.ame.iec62264.PersonProperty;
import at.ac.tuwien.big.ame.iec62264.PersonnelActual;
import at.ac.tuwien.big.ame.iec62264.PersonnelActualProperty;
import at.ac.tuwien.big.ame.iec62264.PersonnelCapability;
import at.ac.tuwien.big.ame.iec62264.PersonnelCapabilityProperty;
import at.ac.tuwien.big.ame.iec62264.PersonnelClass;
import at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty;
import at.ac.tuwien.big.ame.iec62264.PersonnelModel;
import at.ac.tuwien.big.ame.iec62264.PersonnelRequirement;
import at.ac.tuwien.big.ame.iec62264.PersonnelRequirementProperty;
import at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification;
import at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecificationProperty;
import at.ac.tuwien.big.ame.iec62264.PersonnelSpecification;
import at.ac.tuwien.big.ame.iec62264.PersonnelSpecificationProperty;
import at.ac.tuwien.big.ame.iec62264.PhysicalAsset;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetActualProperty;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityProperty;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirementProperty;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecificationProperty;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecificationProperty;
import at.ac.tuwien.big.ame.iec62264.ProcessSegment;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapabilityModel;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter;
import at.ac.tuwien.big.ame.iec62264.QualificationTestResult;
import at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification;
import at.ac.tuwien.big.ame.iec62264.ReferencialProperty;
import at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty;
import at.ac.tuwien.big.ame.iec62264.ReferentialMaterialProperty;
import at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty;
import at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty;
import at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty;
import at.ac.tuwien.big.ame.iec62264.RequestedSegmentResponse;
import at.ac.tuwien.big.ame.iec62264.ScheduleState;
import at.ac.tuwien.big.ame.iec62264.SegmentData;
import at.ac.tuwien.big.ame.iec62264.SegmentParameter;
import at.ac.tuwien.big.ame.iec62264.SegmentRequirement;
import at.ac.tuwien.big.ame.iec62264.SegmentResponse;
import at.ac.tuwien.big.ame.iec62264.UseType;

import java.time.ZonedDateTime;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Iec62264PackageImpl extends EPackageImpl implements Iec62264Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass qualificationTestSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelClassPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass qualificationTestResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentCapabilityTestResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentCapabilityTestSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentClassPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetCapabilityTestResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetCapabilityTestSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetClassPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentAssetMappingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialTestResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialTestSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialSublotEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialLotEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialLotPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialDefinitionPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialClassPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processSegmentModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processSegmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelSegmentSpecificationPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentSegmentSpecificationPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetSegmentSpecificationPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialSegmentSpecificationPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialSegmentSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetSegmentSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentSegmentSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelSegmentSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processSegmentParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processSegmentDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iec62264ModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsDefinitionModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsScheduleModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsPerformanceModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsCapabilityModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processSegmentCapabilityModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hierarchyScopeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialSpecificationPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetSpecificationPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentSpecificationPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelSpecificationPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsSegmentDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsMaterialBillItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsMaterialBillEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsSegmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialRequirementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialRequirementPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetRequirementPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentRequirementPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelRequirementPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsScheduleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsRequestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requestedSegmentResponseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass segmentRequirementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentRequirementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetRequirementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelRequirementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass segmentParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialActualPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetActualPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentActualPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelActualPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialActualEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetActualEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentActualEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelActualEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass segmentDataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass segmentResponseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsResponseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsPerformanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsCapabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialCapabilityPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetCapabilityPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentCapabilityPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelCapabilityPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialCapabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetCapabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentCapabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelCapabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processSegmentCapabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referencialPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referentialPersonnelPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referentialEquipmentPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referentialPhysicalAssetPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referentialMaterialTypePropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referentialMaterialPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum assemblyTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum assemblyRelationshipEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum operationsTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum materialUseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum useTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum scheduleStateEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum performanceStateEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum capabilityTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum capacityTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType zonedDateTimeEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Iec62264PackageImpl() {
		super(eNS_URI, Iec62264Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Iec62264Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Iec62264Package init() {
		if (isInited)
			return (Iec62264Package) EPackage.Registry.INSTANCE.getEPackage(Iec62264Package.eNS_URI);

		// Obtain or create and register package
		Iec62264PackageImpl theIec62264Package = (Iec62264PackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof Iec62264PackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new Iec62264PackageImpl());

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theIec62264Package.createPackageContents();

		// Initialize created meta-data
		theIec62264Package.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theIec62264Package.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Iec62264Package.eNS_URI, theIec62264Package);
		return theIec62264Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerson() {
		return personEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerson_PersonnelClasses() {
		return (EReference) personEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerson_Properties() {
		return (EReference) personEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerson_TestSpecifications() {
		return (EReference) personEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerson_Id() {
		return (EAttribute) personEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerson_Description() {
		return (EAttribute) personEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerson_Name() {
		return (EAttribute) personEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelClass() {
		return personnelClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClass_Properties() {
		return (EReference) personnelClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClass_TestSpecifications() {
		return (EReference) personnelClassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelClass_Id() {
		return (EAttribute) personnelClassEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelClass_Description() {
		return (EAttribute) personnelClassEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQualificationTestSpecification() {
		return qualificationTestSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQualificationTestSpecification_Id() {
		return (EAttribute) qualificationTestSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQualificationTestSpecification_Description() {
		return (EAttribute) qualificationTestSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQualificationTestSpecification_Version() {
		return (EAttribute) qualificationTestSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelClassProperty() {
		return personnelClassPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassProperty_TestSpecifications() {
		return (EReference) personnelClassPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassProperty_SubProperties() {
		return (EReference) personnelClassPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelClassProperty_Id() {
		return (EAttribute) personnelClassPropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelClassProperty_Description() {
		return (EAttribute) personnelClassPropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelClassProperty_Value() {
		return (EAttribute) personnelClassPropertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelClassProperty_ValueUnitOfMeasure() {
		return (EAttribute) personnelClassPropertyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonProperty() {
		return personPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonProperty_SubProperties() {
		return (EReference) personPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonProperty_PersonnelClassProperty() {
		return (EReference) personPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonProperty_Id() {
		return (EAttribute) personPropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonProperty_Description() {
		return (EAttribute) personPropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonProperty_Value() {
		return (EAttribute) personPropertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonProperty_ValueUnitOfMeasure() {
		return (EAttribute) personPropertyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQualificationTestResult() {
		return qualificationTestResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualificationTestResult_TestSpecification() {
		return (EReference) qualificationTestResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualificationTestResult_Property() {
		return (EReference) qualificationTestResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQualificationTestResult_Id() {
		return (EAttribute) qualificationTestResultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQualificationTestResult_Description() {
		return (EAttribute) qualificationTestResultEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQualificationTestResult_Date() {
		return (EAttribute) qualificationTestResultEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQualificationTestResult_Result() {
		return (EAttribute) qualificationTestResultEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQualificationTestResult_ResultUnitOfMeasure() {
		return (EAttribute) qualificationTestResultEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQualificationTestResult_Expiration() {
		return (EAttribute) qualificationTestResultEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelModel() {
		return personnelModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelModel_PersonnelClasses() {
		return (EReference) personnelModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelModel_Persons() {
		return (EReference) personnelModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelModel_TestSpecifications() {
		return (EReference) personnelModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelModel_TestResults() {
		return (EReference) personnelModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelModel_OperationsCapabilityModel() {
		return (EReference) personnelModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentModel() {
		return equipmentModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentModel_EquipmentClasses() {
		return (EReference) equipmentModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentModel_Equipments() {
		return (EReference) equipmentModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentModel_TestSpecifications() {
		return (EReference) equipmentModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentModel_TestResults() {
		return (EReference) equipmentModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentClass() {
		return equipmentClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClass_Properties() {
		return (EReference) equipmentClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClass_TestSpecifications() {
		return (EReference) equipmentClassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentClass_Id() {
		return (EAttribute) equipmentClassEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentClass_Description() {
		return (EAttribute) equipmentClassEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentClass_EquipmentLevel() {
		return (EAttribute) equipmentClassEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentCapabilityTestResult() {
		return equipmentCapabilityTestResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapabilityTestResult_TestSpecification() {
		return (EReference) equipmentCapabilityTestResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapabilityTestResult_Property() {
		return (EReference) equipmentCapabilityTestResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapabilityTestResult_Id() {
		return (EAttribute) equipmentCapabilityTestResultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapabilityTestResult_Description() {
		return (EAttribute) equipmentCapabilityTestResultEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapabilityTestResult_Date() {
		return (EAttribute) equipmentCapabilityTestResultEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapabilityTestResult_Result() {
		return (EAttribute) equipmentCapabilityTestResultEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapabilityTestResult_ResultUnitOfMeasure() {
		return (EAttribute) equipmentCapabilityTestResultEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapabilityTestResult_Expiration() {
		return (EAttribute) equipmentCapabilityTestResultEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentCapabilityTestSpecification() {
		return equipmentCapabilityTestSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapabilityTestSpecification_Id() {
		return (EAttribute) equipmentCapabilityTestSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapabilityTestSpecification_Description() {
		return (EAttribute) equipmentCapabilityTestSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapabilityTestSpecification_Version() {
		return (EAttribute) equipmentCapabilityTestSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentClassProperty() {
		return equipmentClassPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassProperty_SubProperties() {
		return (EReference) equipmentClassPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassProperty_TestSpecifications() {
		return (EReference) equipmentClassPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentClassProperty_Id() {
		return (EAttribute) equipmentClassPropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentClassProperty_Description() {
		return (EAttribute) equipmentClassPropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentClassProperty_Value() {
		return (EAttribute) equipmentClassPropertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentClassProperty_ValueUnitOfMeasure() {
		return (EAttribute) equipmentClassPropertyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentProperty() {
		return equipmentPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentProperty_SubProperties() {
		return (EReference) equipmentPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentProperty_EquipmentClassProperty() {
		return (EReference) equipmentPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentProperty_Id() {
		return (EAttribute) equipmentPropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentProperty_Description() {
		return (EAttribute) equipmentPropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentProperty_Value() {
		return (EAttribute) equipmentPropertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentProperty_ValueUnitOfMeasure() {
		return (EAttribute) equipmentPropertyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipment() {
		return equipmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipment_Children() {
		return (EReference) equipmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipment_Properties() {
		return (EReference) equipmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipment_TestSpecifications() {
		return (EReference) equipmentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipment_EquipmentClass() {
		return (EReference) equipmentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipment_Id() {
		return (EAttribute) equipmentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipment_Description() {
		return (EAttribute) equipmentEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipment_EquipmentLevel() {
		return (EAttribute) equipmentEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetModel() {
		return physicalAssetModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetModel_PhysicalAssets() {
		return (EReference) physicalAssetModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetModel_PhysicalAssetClasses() {
		return (EReference) physicalAssetModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetModel_TestSpecifications() {
		return (EReference) physicalAssetModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetModel_EquipmentAssetMappings() {
		return (EReference) physicalAssetModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetModel_TestResults() {
		return (EReference) physicalAssetModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetClass() {
		return physicalAssetClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClass_Properties() {
		return (EReference) physicalAssetClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClass_TestSpecifications() {
		return (EReference) physicalAssetClassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetClass_Id() {
		return (EAttribute) physicalAssetClassEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetClass_Manufacturer() {
		return (EAttribute) physicalAssetClassEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetClass_Description() {
		return (EAttribute) physicalAssetClassEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetCapabilityTestResult() {
		return physicalAssetCapabilityTestResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapabilityTestResult_TestSpecification() {
		return (EReference) physicalAssetCapabilityTestResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapabilityTestResult_Property() {
		return (EReference) physicalAssetCapabilityTestResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapabilityTestResult_Id() {
		return (EAttribute) physicalAssetCapabilityTestResultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapabilityTestResult_Description() {
		return (EAttribute) physicalAssetCapabilityTestResultEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapabilityTestResult_Date() {
		return (EAttribute) physicalAssetCapabilityTestResultEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapabilityTestResult_Result() {
		return (EAttribute) physicalAssetCapabilityTestResultEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapabilityTestResult_ResultUnitOfMeasure() {
		return (EAttribute) physicalAssetCapabilityTestResultEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapabilityTestResult_Expiration() {
		return (EAttribute) physicalAssetCapabilityTestResultEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetProperty() {
		return physicalAssetPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetProperty_SubProperties() {
		return (EReference) physicalAssetPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetProperty_Id() {
		return (EAttribute) physicalAssetPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetProperty_Description() {
		return (EAttribute) physicalAssetPropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetProperty_Value() {
		return (EAttribute) physicalAssetPropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetProperty_ValueUnitOfMeasure() {
		return (EAttribute) physicalAssetPropertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetProperty_PhysicalAssetClassProperty() {
		return (EReference) physicalAssetPropertyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAsset() {
		return physicalAssetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAsset_Children() {
		return (EReference) physicalAssetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAsset_Properties() {
		return (EReference) physicalAssetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAsset_PhysicalAssetClass() {
		return (EReference) physicalAssetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAsset_TestSpecifications() {
		return (EReference) physicalAssetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAsset_Id() {
		return (EAttribute) physicalAssetEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAsset_Description() {
		return (EAttribute) physicalAssetEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAsset_PhysicalLocation() {
		return (EAttribute) physicalAssetEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAsset_FixedAssetId() {
		return (EAttribute) physicalAssetEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAsset_VendorId() {
		return (EAttribute) physicalAssetEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetCapabilityTestSpecification() {
		return physicalAssetCapabilityTestSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapabilityTestSpecification_Id() {
		return (EAttribute) physicalAssetCapabilityTestSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapabilityTestSpecification_Description() {
		return (EAttribute) physicalAssetCapabilityTestSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapabilityTestSpecification_Version() {
		return (EAttribute) physicalAssetCapabilityTestSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetClassProperty() {
		return physicalAssetClassPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassProperty_SubProperties() {
		return (EReference) physicalAssetClassPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassProperty_TestSpecifications() {
		return (EReference) physicalAssetClassPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetClassProperty_Id() {
		return (EAttribute) physicalAssetClassPropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetClassProperty_Description() {
		return (EAttribute) physicalAssetClassPropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetClassProperty_Value() {
		return (EAttribute) physicalAssetClassPropertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetClassProperty_ValueUnitOfMeasure() {
		return (EAttribute) physicalAssetClassPropertyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentAssetMapping() {
		return equipmentAssetMappingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentAssetMapping_PhysicalAsset() {
		return (EReference) equipmentAssetMappingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentAssetMapping_Equipment() {
		return (EReference) equipmentAssetMappingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentAssetMapping_Id() {
		return (EAttribute) equipmentAssetMappingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentAssetMapping_Description() {
		return (EAttribute) equipmentAssetMappingEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentAssetMapping_StartTime() {
		return (EAttribute) equipmentAssetMappingEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentAssetMapping_EndTime() {
		return (EAttribute) equipmentAssetMappingEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialModel() {
		return materialModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialModel_MaterialClasses() {
		return (EReference) materialModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialModel_MaterialDefinitions() {
		return (EReference) materialModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialModel_MaterialLots() {
		return (EReference) materialModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialModel_MaterialSublots() {
		return (EReference) materialModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialModel_TestSpecifications() {
		return (EReference) materialModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialModel_TestResults() {
		return (EReference) materialModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialClass() {
		return materialClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClass_Properties() {
		return (EReference) materialClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClass_AssembledFromClasses() {
		return (EReference) materialClassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClass_TestSpecifications() {
		return (EReference) materialClassEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialClass_Id() {
		return (EAttribute) materialClassEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialClass_Description() {
		return (EAttribute) materialClassEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialClass_AssemblyType() {
		return (EAttribute) materialClassEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialClass_AssemblyRelationship() {
		return (EAttribute) materialClassEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialTestResult() {
		return materialTestResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialTestResult_TestSpecification() {
		return (EReference) materialTestResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialTestResult_Property() {
		return (EReference) materialTestResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialTestResult_Id() {
		return (EAttribute) materialTestResultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialTestResult_Description() {
		return (EAttribute) materialTestResultEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialTestResult_Date() {
		return (EAttribute) materialTestResultEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialTestResult_Result() {
		return (EAttribute) materialTestResultEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialTestResult_ResultUnitOfMeasure() {
		return (EAttribute) materialTestResultEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialTestResult_Expiration() {
		return (EAttribute) materialTestResultEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialTestSpecification() {
		return materialTestSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialTestSpecification_Id() {
		return (EAttribute) materialTestSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialTestSpecification_Description() {
		return (EAttribute) materialTestSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialTestSpecification_Version() {
		return (EAttribute) materialTestSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialSublot() {
		return materialSublotEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSublot_Properties() {
		return (EReference) materialSublotEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSublot_AssembledFromLots() {
		return (EReference) materialSublotEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSublot_AssembledFromSublots() {
		return (EReference) materialSublotEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSublot_Sublots() {
		return (EReference) materialSublotEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSublot_Id() {
		return (EAttribute) materialSublotEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSublot_Description() {
		return (EAttribute) materialSublotEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSublot_AssemblyType() {
		return (EAttribute) materialSublotEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSublot_AssemblyRelationship() {
		return (EAttribute) materialSublotEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSublot_Status() {
		return (EAttribute) materialSublotEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSublot_StorageLocation() {
		return (EAttribute) materialSublotEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSublot_Quantity() {
		return (EAttribute) materialSublotEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSublot_QuantityUnitOfMeasure() {
		return (EAttribute) materialSublotEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialLot() {
		return materialLotEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLot_Properties() {
		return (EReference) materialLotEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLot_AssembledFromLots() {
		return (EReference) materialLotEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLot_AssembledFromSublots() {
		return (EReference) materialLotEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLot_Sublots() {
		return (EReference) materialLotEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLot_MaterialDefinition() {
		return (EReference) materialLotEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLot_TestSpecifications() {
		return (EReference) materialLotEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialLot_Id() {
		return (EAttribute) materialLotEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialLot_Description() {
		return (EAttribute) materialLotEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialLot_AssemblyType() {
		return (EAttribute) materialLotEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialLot_AssemblyRelationship() {
		return (EAttribute) materialLotEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialLot_Status() {
		return (EAttribute) materialLotEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialLot_StorageLocation() {
		return (EAttribute) materialLotEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialLot_Quantity() {
		return (EAttribute) materialLotEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialLot_QuantityUnitOfMeasure() {
		return (EAttribute) materialLotEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialLotProperty() {
		return materialLotPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotProperty_SubProperties() {
		return (EReference) materialLotPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotProperty_MaterialDefinitionProperty() {
		return (EReference) materialLotPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialLotProperty_Id() {
		return (EAttribute) materialLotPropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialLotProperty_Description() {
		return (EAttribute) materialLotPropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialLotProperty_Value() {
		return (EAttribute) materialLotPropertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialLotProperty_ValueUnitOfMeasure() {
		return (EAttribute) materialLotPropertyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialDefinition() {
		return materialDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinition_Properties() {
		return (EReference) materialDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinition_AssembledFromDefinitions() {
		return (EReference) materialDefinitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinition_MaterialClasses() {
		return (EReference) materialDefinitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinition_TestSpecifications() {
		return (EReference) materialDefinitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialDefinition_Id() {
		return (EAttribute) materialDefinitionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialDefinition_Description() {
		return (EAttribute) materialDefinitionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialDefinition_AssemblyType() {
		return (EAttribute) materialDefinitionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialDefinition_AssemblyRelationship() {
		return (EAttribute) materialDefinitionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialDefinitionProperty() {
		return materialDefinitionPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionProperty_SubProperties() {
		return (EReference) materialDefinitionPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionProperty_MaterialClassProperty() {
		return (EReference) materialDefinitionPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionProperty_TestSpecifications() {
		return (EReference) materialDefinitionPropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialDefinitionProperty_Id() {
		return (EAttribute) materialDefinitionPropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialDefinitionProperty_Description() {
		return (EAttribute) materialDefinitionPropertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialDefinitionProperty_Value() {
		return (EAttribute) materialDefinitionPropertyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialDefinitionProperty_ValueUnitOfMeasure() {
		return (EAttribute) materialDefinitionPropertyEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialClassProperty() {
		return materialClassPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassProperty_SubProperties() {
		return (EReference) materialClassPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassProperty_TestSpecifications() {
		return (EReference) materialClassPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialClassProperty_Id() {
		return (EAttribute) materialClassPropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialClassProperty_Description() {
		return (EAttribute) materialClassPropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialClassProperty_Value() {
		return (EAttribute) materialClassPropertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialClassProperty_ValueUnitOfMeasure() {
		return (EAttribute) materialClassPropertyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessSegmentModel() {
		return processSegmentModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentModel_ProcessSegments() {
		return (EReference) processSegmentModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentModel_ProcessSegmentDependencies() {
		return (EReference) processSegmentModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessSegment() {
		return processSegmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegment_Children() {
		return (EReference) processSegmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegment_ProcessSegmentParameters() {
		return (EReference) processSegmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegment_PersonnelSegmentSpecifications() {
		return (EReference) processSegmentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegment_EquipmentSegmentSpecifications() {
		return (EReference) processSegmentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegment_PhysicalAssetSegmentSpecifications() {
		return (EReference) processSegmentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegment_MaterialSegmentSpecifications() {
		return (EReference) processSegmentEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegment_Id() {
		return (EAttribute) processSegmentEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegment_Description() {
		return (EAttribute) processSegmentEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegment_OperationsType() {
		return (EAttribute) processSegmentEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegment_Duration() {
		return (EAttribute) processSegmentEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegment_DurationUnitOfMeasure() {
		return (EAttribute) processSegmentEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegment_HierarchyScope() {
		return (EReference) processSegmentEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelSegmentSpecificationProperty() {
		return personnelSegmentSpecificationPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecificationProperty_SubProperties() {
		return (EReference) personnelSegmentSpecificationPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentSegmentSpecificationProperty() {
		return equipmentSegmentSpecificationPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecificationProperty_SubProperties() {
		return (EReference) equipmentSegmentSpecificationPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetSegmentSpecificationProperty() {
		return physicalAssetSegmentSpecificationPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecificationProperty_SubProperties() {
		return (EReference) physicalAssetSegmentSpecificationPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialSegmentSpecificationProperty() {
		return materialSegmentSpecificationPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationProperty_SubProperties() {
		return (EReference) materialSegmentSpecificationPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialSegmentSpecification() {
		return materialSegmentSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecification_AssembledFromSpecifications() {
		return (EReference) materialSegmentSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecification_Properties() {
		return (EReference) materialSegmentSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecification_MaterialClasses() {
		return (EReference) materialSegmentSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecification_MaterialDefinitions() {
		return (EReference) materialSegmentSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSegmentSpecification_Description() {
		return (EAttribute) materialSegmentSpecificationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSegmentSpecification_AssemblyType() {
		return (EAttribute) materialSegmentSpecificationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSegmentSpecification_AssemblyRelationship() {
		return (EAttribute) materialSegmentSpecificationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSegmentSpecification_MaterialUse() {
		return (EAttribute) materialSegmentSpecificationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSegmentSpecification_Quantity() {
		return (EAttribute) materialSegmentSpecificationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSegmentSpecification_QuantityUnitOfMeasurement() {
		return (EAttribute) materialSegmentSpecificationEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetSegmentSpecification() {
		return physicalAssetSegmentSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecification_Properties() {
		return (EReference) physicalAssetSegmentSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecification_PhysicalAssetClasses() {
		return (EReference) physicalAssetSegmentSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecification_PhysicalAssets() {
		return (EReference) physicalAssetSegmentSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetSegmentSpecification_Description() {
		return (EAttribute) physicalAssetSegmentSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetSegmentSpecification_PhysicalAssetUse() {
		return (EAttribute) physicalAssetSegmentSpecificationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetSegmentSpecification_Quantity() {
		return (EAttribute) physicalAssetSegmentSpecificationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetSegmentSpecification_QuantityUnitOfMeasure() {
		return (EAttribute) physicalAssetSegmentSpecificationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentSegmentSpecification() {
		return equipmentSegmentSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecification_Properties() {
		return (EReference) equipmentSegmentSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecification_EquipmentClasses() {
		return (EReference) equipmentSegmentSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecification_Equipments() {
		return (EReference) equipmentSegmentSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentSegmentSpecification_Description() {
		return (EAttribute) equipmentSegmentSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentSegmentSpecification_EquipmentUse() {
		return (EAttribute) equipmentSegmentSpecificationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentSegmentSpecification_Quantity() {
		return (EAttribute) equipmentSegmentSpecificationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentSegmentSpecification_QuantityUnitOfMeasurement() {
		return (EAttribute) equipmentSegmentSpecificationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelSegmentSpecification() {
		return personnelSegmentSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecification_Properties() {
		return (EReference) personnelSegmentSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecification_PersonnelClasses() {
		return (EReference) personnelSegmentSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecification_Persons() {
		return (EReference) personnelSegmentSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelSegmentSpecification_Description() {
		return (EAttribute) personnelSegmentSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelSegmentSpecification_PersonnelUse() {
		return (EAttribute) personnelSegmentSpecificationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelSegmentSpecification_Quantity() {
		return (EAttribute) personnelSegmentSpecificationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelSegmentSpecification_QuantityUnitOfMeasure() {
		return (EAttribute) personnelSegmentSpecificationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessSegmentParameter() {
		return processSegmentParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentParameter_SubParameters() {
		return (EReference) processSegmentParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentParameter_Id() {
		return (EAttribute) processSegmentParameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentParameter_Description() {
		return (EAttribute) processSegmentParameterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentParameter_Value() {
		return (EAttribute) processSegmentParameterEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentParameter_ValueUnitOfMeasure() {
		return (EAttribute) processSegmentParameterEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessSegmentDependency() {
		return processSegmentDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentDependency_Subject() {
		return (EReference) processSegmentDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentDependency_Dependency() {
		return (EReference) processSegmentDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentDependency_Id() {
		return (EAttribute) processSegmentDependencyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentDependency_Description() {
		return (EAttribute) processSegmentDependencyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentDependency_DependencyType() {
		return (EAttribute) processSegmentDependencyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentDependency_DependencyFactor() {
		return (EAttribute) processSegmentDependencyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentDependency_FactorUnitOfMeasure() {
		return (EAttribute) processSegmentDependencyEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIec62264Model() {
		return iec62264ModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIec62264Model_PersonnelModel() {
		return (EReference) iec62264ModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIec62264Model_EquipmentModel() {
		return (EReference) iec62264ModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIec62264Model_PhysicalAssetModel() {
		return (EReference) iec62264ModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIec62264Model_MaterialModel() {
		return (EReference) iec62264ModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIec62264Model_ProcessSegmentModel() {
		return (EReference) iec62264ModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIec62264Model_OperationsDefinitionModel() {
		return (EReference) iec62264ModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIec62264Model_OperationsScheduleModel() {
		return (EReference) iec62264ModelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIec62264Model_OperationsPerformanceModel() {
		return (EReference) iec62264ModelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIec62264Model_ProcessSegmentCapabilityModel() {
		return (EReference) iec62264ModelEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIec62264Model_HierarchyScopes() {
		return (EReference) iec62264ModelEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIec62264Model_Name() {
		return (EAttribute) iec62264ModelEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsDefinitionModel() {
		return operationsDefinitionModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionModel_OperationsDefinitions() {
		return (EReference) operationsDefinitionModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionModel_OperationsSegments() {
		return (EReference) operationsDefinitionModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionModel_OperationsSegmentDependencies() {
		return (EReference) operationsDefinitionModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsScheduleModel() {
		return operationsScheduleModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsScheduleModel_OperationsSchedules() {
		return (EReference) operationsScheduleModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsPerformanceModel() {
		return operationsPerformanceModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformanceModel_OperationsPerformances() {
		return (EReference) operationsPerformanceModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsCapabilityModel() {
		return operationsCapabilityModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityModel_OperationsCapabilities() {
		return (EReference) operationsCapabilityModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessSegmentCapabilityModel() {
		return processSegmentCapabilityModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentCapabilityModel_ProcessSegmentCapabilities() {
		return (EReference) processSegmentCapabilityModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHierarchyScope() {
		return hierarchyScopeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHierarchyScope_SubScope() {
		return (EReference) hierarchyScopeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHierarchyScope_Equipment() {
		return (EReference) hierarchyScopeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHierarchyScope_EquipmentElementLevel() {
		return (EAttribute) hierarchyScopeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialSpecificationProperty() {
		return materialSpecificationPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSpecificationProperty_SubProperties() {
		return (EReference) materialSpecificationPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetSpecificationProperty() {
		return physicalAssetSpecificationPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSpecificationProperty_SubProperties() {
		return (EReference) physicalAssetSpecificationPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentSpecificationProperty() {
		return equipmentSpecificationPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSpecificationProperty_SubProperties() {
		return (EReference) equipmentSpecificationPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelSpecificationProperty() {
		return personnelSpecificationPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSpecificationProperty_SubProperties() {
		return (EReference) personnelSpecificationPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialSpecification() {
		return materialSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSpecification_AssembledFromSpecifications() {
		return (EReference) materialSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSpecification_MaterialSpecificationProperties() {
		return (EReference) materialSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSpecification_MaterialClasses() {
		return (EReference) materialSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSpecification_MaterialDefinitions() {
		return (EReference) materialSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSpecification_Description() {
		return (EAttribute) materialSpecificationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSpecification_MaterialUse() {
		return (EAttribute) materialSpecificationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSpecification_Quantity() {
		return (EAttribute) materialSpecificationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSpecification_QuantityUnitOfMeasure() {
		return (EAttribute) materialSpecificationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSpecification_AssemblyType() {
		return (EAttribute) materialSpecificationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialSpecification_AssemblyRelationship() {
		return (EAttribute) materialSpecificationEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetSpecification() {
		return physicalAssetSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSpecification_PhysicalAssetSpecificationProperties() {
		return (EReference) physicalAssetSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetSpecification_Description() {
		return (EAttribute) physicalAssetSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSpecification_PhysicAlassetClasses() {
		return (EReference) physicalAssetSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSpecification_PhysicalAssets() {
		return (EReference) physicalAssetSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetSpecification_PhysicalAssetUse() {
		return (EAttribute) physicalAssetSpecificationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetSpecification_Quantity() {
		return (EAttribute) physicalAssetSpecificationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetSpecification_QuantityUnitOfMeasure() {
		return (EAttribute) physicalAssetSpecificationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentSpecification() {
		return equipmentSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSpecification_EquipmentSpecificationProperties() {
		return (EReference) equipmentSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSpecification_EquipmentClasses() {
		return (EReference) equipmentSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSpecification_Equipments() {
		return (EReference) equipmentSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentSpecification_Description() {
		return (EAttribute) equipmentSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentSpecification_EquipmentUse() {
		return (EAttribute) equipmentSpecificationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentSpecification_Quantity() {
		return (EAttribute) equipmentSpecificationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentSpecification_QuantityUnitOfMeasure() {
		return (EAttribute) equipmentSpecificationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelSpecification() {
		return personnelSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSpecification_PersonnelSpecificationProperties() {
		return (EReference) personnelSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSpecification_PersonnelClasses() {
		return (EReference) personnelSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSpecification_Persons() {
		return (EReference) personnelSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelSpecification_Description() {
		return (EAttribute) personnelSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelSpecification_PersonnelUse() {
		return (EAttribute) personnelSpecificationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelSpecification_Quantity() {
		return (EAttribute) personnelSpecificationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelSpecification_QuantityUnitOfMeasure() {
		return (EAttribute) personnelSpecificationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterSpecification() {
		return parameterSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterSpecification_SubParameters() {
		return (EReference) parameterSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSpecification_Id() {
		return (EAttribute) parameterSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSpecification_Description() {
		return (EAttribute) parameterSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSpecification_Value() {
		return (EAttribute) parameterSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSpecification_ValueUnitOfMeasurement() {
		return (EAttribute) parameterSpecificationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsSegmentDependency() {
		return operationsSegmentDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentDependency_Subject() {
		return (EReference) operationsSegmentDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentDependency_Dependency() {
		return (EReference) operationsSegmentDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSegmentDependency_Id() {
		return (EAttribute) operationsSegmentDependencyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSegmentDependency_Description() {
		return (EAttribute) operationsSegmentDependencyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSegmentDependency_DependencyType() {
		return (EAttribute) operationsSegmentDependencyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSegmentDependency_DependencyFactor() {
		return (EAttribute) operationsSegmentDependencyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSegmentDependency_UnitOfMeasure() {
		return (EAttribute) operationsSegmentDependencyEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsMaterialBillItem() {
		return operationsMaterialBillItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItem_MaterialSpecifications() {
		return (EReference) operationsMaterialBillItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItem_AssembledFromItems() {
		return (EReference) operationsMaterialBillItemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsMaterialBillItem_Id() {
		return (EAttribute) operationsMaterialBillItemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsMaterialBillItem_Description() {
		return (EAttribute) operationsMaterialBillItemEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItem_MaterialClasses() {
		return (EReference) operationsMaterialBillItemEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItem_MaterialDefinitions() {
		return (EReference) operationsMaterialBillItemEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsMaterialBillItem_UseType() {
		return (EAttribute) operationsMaterialBillItemEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsMaterialBillItem_AssemblyType() {
		return (EAttribute) operationsMaterialBillItemEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsMaterialBillItem_AssemblyRelationship() {
		return (EAttribute) operationsMaterialBillItemEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsMaterialBillItem_Quantities() {
		return (EAttribute) operationsMaterialBillItemEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsMaterialBillItem_QuantityUnitOfMeasures() {
		return (EAttribute) operationsMaterialBillItemEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsMaterialBill() {
		return operationsMaterialBillEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBill_OperationsMaterialBillItems() {
		return (EReference) operationsMaterialBillEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsMaterialBill_Id() {
		return (EAttribute) operationsMaterialBillEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsMaterialBill_Description() {
		return (EAttribute) operationsMaterialBillEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsSegment() {
		return operationsSegmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegment_ProcessSegments() {
		return (EReference) operationsSegmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegment_SubSegments() {
		return (EReference) operationsSegmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegment_ParameterSpecifications() {
		return (EReference) operationsSegmentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegment_PersonnelSpecifications() {
		return (EReference) operationsSegmentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegment_EquipmentSpecifications() {
		return (EReference) operationsSegmentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegment_PhysicalAssetSpecifications() {
		return (EReference) operationsSegmentEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegment_MaterialSpecifications() {
		return (EReference) operationsSegmentEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSegment_Id() {
		return (EAttribute) operationsSegmentEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSegment_Description() {
		return (EAttribute) operationsSegmentEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSegment_Duration() {
		return (EAttribute) operationsSegmentEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSegment_DurationUnitOfMeasure() {
		return (EAttribute) operationsSegmentEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSegment_OperationsType() {
		return (EAttribute) operationsSegmentEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSegment_WorkDefinitionId() {
		return (EAttribute) operationsSegmentEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsDefinition() {
		return operationsDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinition_OperationsSegments() {
		return (EReference) operationsDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinition_OperationsMaterialBills() {
		return (EReference) operationsDefinitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsDefinition_Id() {
		return (EAttribute) operationsDefinitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsDefinition_Version() {
		return (EAttribute) operationsDefinitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsDefinition_Description() {
		return (EAttribute) operationsDefinitionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsDefinition_OperationsType() {
		return (EAttribute) operationsDefinitionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsDefinition_BillOfMaterialId() {
		return (EAttribute) operationsDefinitionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsDefinition_WorkDefinitionId() {
		return (EAttribute) operationsDefinitionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsDefinition_BillOfResourceId() {
		return (EAttribute) operationsDefinitionEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinition_HierarchysScope() {
		return (EReference) operationsDefinitionEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialRequirement() {
		return materialRequirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialRequirement_MaterialRequirementProperties() {
		return (EReference) materialRequirementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialRequirement_AssembledFromRequirements() {
		return (EReference) materialRequirementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialRequirement_MaterialClasses() {
		return (EReference) materialRequirementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialRequirement_MaterialDefinitions() {
		return (EReference) materialRequirementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialRequirement_MaterialLots() {
		return (EReference) materialRequirementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialRequirement_MaterialSublot() {
		return (EReference) materialRequirementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialRequirement_Description() {
		return (EAttribute) materialRequirementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialRequirement_MaterialUse() {
		return (EAttribute) materialRequirementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialRequirement_StorageLocation() {
		return (EAttribute) materialRequirementEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialRequirement_Quantity() {
		return (EAttribute) materialRequirementEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialRequirement_QuantityUnitOfMeasure() {
		return (EAttribute) materialRequirementEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialRequirement_AssemblyType() {
		return (EAttribute) materialRequirementEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialRequirement_AssemblyRelationship() {
		return (EAttribute) materialRequirementEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialRequirementProperty() {
		return materialRequirementPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialRequirementProperty_SubProperties() {
		return (EReference) materialRequirementPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetRequirementProperty() {
		return physicalAssetRequirementPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetRequirementProperty_SubProperties() {
		return (EReference) physicalAssetRequirementPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentRequirementProperty() {
		return equipmentRequirementPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentRequirementProperty_SubProperties() {
		return (EReference) equipmentRequirementPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelRequirementProperty() {
		return personnelRequirementPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelRequirementProperty_SubProperties() {
		return (EReference) personnelRequirementPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsSchedule() {
		return operationsScheduleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSchedule_OperationsRequests() {
		return (EReference) operationsScheduleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSchedule_Id() {
		return (EAttribute) operationsScheduleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSchedule_Description() {
		return (EAttribute) operationsScheduleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSchedule_OperationsType() {
		return (EAttribute) operationsScheduleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSchedule_StartTime() {
		return (EAttribute) operationsScheduleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSchedule_EndTime() {
		return (EAttribute) operationsScheduleEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSchedule_PublishedDate() {
		return (EAttribute) operationsScheduleEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSchedule_ScheduledState() {
		return (EAttribute) operationsScheduleEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSchedule_HierarchyScope() {
		return (EReference) operationsScheduleEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsRequest() {
		return operationsRequestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequest_SegmentRequirements() {
		return (EReference) operationsRequestEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequest_RequestedSegmentResponses() {
		return (EReference) operationsRequestEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequest_OperationsDefinition() {
		return (EReference) operationsRequestEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsRequest_Id() {
		return (EAttribute) operationsRequestEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsRequest_Description() {
		return (EAttribute) operationsRequestEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsRequest_OperationsType() {
		return (EAttribute) operationsRequestEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsRequest_StartTime() {
		return (EAttribute) operationsRequestEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsRequest_EndTime() {
		return (EAttribute) operationsRequestEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsRequest_Priority() {
		return (EAttribute) operationsRequestEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsRequest_RequestState() {
		return (EAttribute) operationsRequestEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequest_HierarchyScope() {
		return (EReference) operationsRequestEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequestedSegmentResponse() {
		return requestedSegmentResponseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSegmentRequirement() {
		return segmentRequirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentRequirement_SubRequirements() {
		return (EReference) segmentRequirementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentRequirement_ProcessSegment() {
		return (EReference) segmentRequirementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentRequirement_OperationsDefinition() {
		return (EReference) segmentRequirementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentRequirement_SegmentParameters() {
		return (EReference) segmentRequirementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentRequirement_PersonnelRequirements() {
		return (EReference) segmentRequirementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentRequirement_EquipmentRequirements() {
		return (EReference) segmentRequirementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentRequirement_PhysicalAssetRequirements() {
		return (EReference) segmentRequirementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentRequirement_MaterialRequirements() {
		return (EReference) segmentRequirementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentRequirement_Id() {
		return (EAttribute) segmentRequirementEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentRequirement_Description() {
		return (EAttribute) segmentRequirementEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentRequirement_OperationsType() {
		return (EAttribute) segmentRequirementEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentRequirement_EarliestStartTime() {
		return (EAttribute) segmentRequirementEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentRequirement_LatestStartTime() {
		return (EAttribute) segmentRequirementEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentRequirement_Duration() {
		return (EAttribute) segmentRequirementEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentRequirement_DurationUnitOfMeasurement() {
		return (EAttribute) segmentRequirementEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentRequirement_SegmentState() {
		return (EAttribute) segmentRequirementEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentRequirement_HierarchyScope() {
		return (EReference) segmentRequirementEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentRequirement() {
		return equipmentRequirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentRequirement_EquipmentRequirementProperties() {
		return (EReference) equipmentRequirementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentRequirement_EquipmentClasses() {
		return (EReference) equipmentRequirementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentRequirement_Equipments() {
		return (EReference) equipmentRequirementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentRequirement_Description() {
		return (EAttribute) equipmentRequirementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentRequirement_EquipmentUse() {
		return (EAttribute) equipmentRequirementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentRequirement_Quantity() {
		return (EAttribute) equipmentRequirementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentRequirement_QuantityUnitOfMeasure() {
		return (EAttribute) equipmentRequirementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentRequirement_EquipmentLevel() {
		return (EAttribute) equipmentRequirementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetRequirement() {
		return physicalAssetRequirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetRequirement_PhysicalAssetRequirementProperties() {
		return (EReference) physicalAssetRequirementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetRequirement_PhysicalAssetClasses() {
		return (EReference) physicalAssetRequirementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetRequirement_PhysicalAssets() {
		return (EReference) physicalAssetRequirementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetRequirement_Description() {
		return (EAttribute) physicalAssetRequirementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetRequirement_PhysicalAssetUse() {
		return (EAttribute) physicalAssetRequirementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetRequirement_Quantity() {
		return (EAttribute) physicalAssetRequirementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetRequirement_QuantityUnitOfMeasure() {
		return (EAttribute) physicalAssetRequirementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetRequirement_EquipmentLevel() {
		return (EAttribute) physicalAssetRequirementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelRequirement() {
		return personnelRequirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelRequirement_PersonnelRequirementProperties() {
		return (EReference) personnelRequirementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelRequirement_PersonnelClasses() {
		return (EReference) personnelRequirementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelRequirement_Persons() {
		return (EReference) personnelRequirementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelRequirement_Description() {
		return (EAttribute) personnelRequirementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelRequirement_PersonnelUse() {
		return (EAttribute) personnelRequirementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelRequirement_Quantity() {
		return (EAttribute) personnelRequirementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelRequirement_QuantityUnitOfMeasure() {
		return (EAttribute) personnelRequirementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSegmentParameter() {
		return segmentParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentParameter_SubParameters() {
		return (EReference) segmentParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentParameter_Id() {
		return (EAttribute) segmentParameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentParameter_Description() {
		return (EAttribute) segmentParameterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentParameter_Value() {
		return (EAttribute) segmentParameterEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentParameter_ValueUnitOfMeasure() {
		return (EAttribute) segmentParameterEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialActualProperty() {
		return materialActualPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialActualProperty_SubProperties() {
		return (EReference) materialActualPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetActualProperty() {
		return physicalAssetActualPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetActualProperty_SubProperties() {
		return (EReference) physicalAssetActualPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentActualProperty() {
		return equipmentActualPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentActualProperty_SubProperties() {
		return (EReference) equipmentActualPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelActualProperty() {
		return personnelActualPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelActualProperty_SubProperties() {
		return (EReference) personnelActualPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialActual() {
		return materialActualEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialActual_AssembledFromActuals() {
		return (EReference) materialActualEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialActual_MaterialActualProperties() {
		return (EReference) materialActualEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialActual_Description() {
		return (EAttribute) materialActualEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialActual_MaterialUse() {
		return (EAttribute) materialActualEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialActual_StorageLocation() {
		return (EAttribute) materialActualEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialActual_Quantity() {
		return (EAttribute) materialActualEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialActual_QuantityUnitOfMeasure() {
		return (EAttribute) materialActualEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialActual_AssemblyType() {
		return (EAttribute) materialActualEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialActual_AssemblyRelationship() {
		return (EAttribute) materialActualEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialActual_MaterialClasses() {
		return (EReference) materialActualEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialActual_MaterialDefinitions() {
		return (EReference) materialActualEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialActual_MaterialLots() {
		return (EReference) materialActualEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialActual_MaterialSublots() {
		return (EReference) materialActualEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetActual() {
		return physicalAssetActualEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetActual_PhysicalAssetActualProperties() {
		return (EReference) physicalAssetActualEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetActual_Description() {
		return (EAttribute) physicalAssetActualEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetActual_PhysicalAssetUse() {
		return (EAttribute) physicalAssetActualEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetActual_Quantity() {
		return (EAttribute) physicalAssetActualEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetActual_QuantityUnitOfMeasure() {
		return (EAttribute) physicalAssetActualEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetActual_PhysicalAssetClasses() {
		return (EReference) physicalAssetActualEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetActual_PhysicalAssets() {
		return (EReference) physicalAssetActualEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentActual() {
		return equipmentActualEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentActual_EquipmentActualProperties() {
		return (EReference) equipmentActualEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentActual_Description() {
		return (EAttribute) equipmentActualEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentActual_EquipmentUse() {
		return (EAttribute) equipmentActualEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentActual_Quantity() {
		return (EAttribute) equipmentActualEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentActual_QuantityUnitOfMeasure() {
		return (EAttribute) equipmentActualEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentActual_EquipmentClasses() {
		return (EReference) equipmentActualEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentActual_Equipments() {
		return (EReference) equipmentActualEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelActual() {
		return personnelActualEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelActual_PersonnelActualProperties() {
		return (EReference) personnelActualEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelActual_Description() {
		return (EAttribute) personnelActualEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelActual_PersonnelUse() {
		return (EAttribute) personnelActualEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelActual_Quantity() {
		return (EAttribute) personnelActualEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelActual_QuantityUnitOfMeasure() {
		return (EAttribute) personnelActualEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelActual_PersonnelClasses() {
		return (EReference) personnelActualEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelActual_Persons() {
		return (EReference) personnelActualEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSegmentData() {
		return segmentDataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentData_SubData() {
		return (EReference) segmentDataEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentData_Id() {
		return (EAttribute) segmentDataEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentData_Description() {
		return (EAttribute) segmentDataEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentData_Value() {
		return (EAttribute) segmentDataEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentData_ValueUnitOfMeasure() {
		return (EAttribute) segmentDataEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSegmentResponse() {
		return segmentResponseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentResponse_SubResponses() {
		return (EReference) segmentResponseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentResponse_ProcessSegment() {
		return (EReference) segmentResponseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentResponse_OperationsDefinition() {
		return (EReference) segmentResponseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentResponse_SegmentData() {
		return (EReference) segmentResponseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentResponse_PersonnelActuals() {
		return (EReference) segmentResponseEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentResponse_EquipmentActuals() {
		return (EReference) segmentResponseEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentResponse_PhysicalAssetActuals() {
		return (EReference) segmentResponseEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentResponse_MaterialActuals() {
		return (EReference) segmentResponseEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentResponse_Id() {
		return (EAttribute) segmentResponseEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentResponse_Description() {
		return (EAttribute) segmentResponseEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentResponse_OperationsType() {
		return (EAttribute) segmentResponseEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentResponse_ActualStartTime() {
		return (EAttribute) segmentResponseEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentResponse_ActualEndTime() {
		return (EAttribute) segmentResponseEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentResponse_SegmentState() {
		return (EAttribute) segmentResponseEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentResponse_HierarchyScope() {
		return (EReference) segmentResponseEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsResponse() {
		return operationsResponseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponse_SegmentResponses() {
		return (EReference) operationsResponseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsResponse_Id() {
		return (EAttribute) operationsResponseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsResponse_Description() {
		return (EAttribute) operationsResponseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsResponse_OperationsType() {
		return (EAttribute) operationsResponseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsResponse_StartTime() {
		return (EAttribute) operationsResponseEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsResponse_EndTime() {
		return (EAttribute) operationsResponseEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsResponse_ResponseState() {
		return (EAttribute) operationsResponseEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponse_OperationsRequest() {
		return (EReference) operationsResponseEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponse_HierarchyScope() {
		return (EReference) operationsResponseEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponse_OperationsDefinition() {
		return (EReference) operationsResponseEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsPerformance() {
		return operationsPerformanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformance_OperationsResponses() {
		return (EReference) operationsPerformanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsPerformance_Id() {
		return (EAttribute) operationsPerformanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsPerformance_Description() {
		return (EAttribute) operationsPerformanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsPerformance_OperationsType() {
		return (EAttribute) operationsPerformanceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsPerformance_StartTime() {
		return (EAttribute) operationsPerformanceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformance_OperationsSchedule() {
		return (EReference) operationsPerformanceEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsPerformance_EndTime() {
		return (EAttribute) operationsPerformanceEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsPerformance_PerformanceState() {
		return (EAttribute) operationsPerformanceEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformance_HierarchyScope() {
		return (EReference) operationsPerformanceEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsPerformance_PublishedDate() {
		return (EAttribute) operationsPerformanceEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsCapability() {
		return operationsCapabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapability_ProcessSegmentCapabilities() {
		return (EReference) operationsCapabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapability_PersonnelCapabilities() {
		return (EReference) operationsCapabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapability_EquipmentCapabilities() {
		return (EReference) operationsCapabilityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapability_PhysicalAssetCapabilities() {
		return (EReference) operationsCapabilityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapability_MaterialCapabilities() {
		return (EReference) operationsCapabilityEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsCapability_Id() {
		return (EAttribute) operationsCapabilityEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsCapability_Description() {
		return (EAttribute) operationsCapabilityEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsCapability_CapacityType() {
		return (EAttribute) operationsCapabilityEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsCapability_Reason() {
		return (EAttribute) operationsCapabilityEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsCapability_ConfidenceFactor() {
		return (EAttribute) operationsCapabilityEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsCapability_StartTime() {
		return (EAttribute) operationsCapabilityEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsCapability_EndTime() {
		return (EAttribute) operationsCapabilityEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsCapability_PublishedDate() {
		return (EAttribute) operationsCapabilityEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapability_HierarchyScope() {
		return (EReference) operationsCapabilityEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialCapabilityProperty() {
		return materialCapabilityPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialCapabilityProperty_SubProperties() {
		return (EReference) materialCapabilityPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetCapabilityProperty() {
		return physicalAssetCapabilityPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapabilityProperty_SubProperties() {
		return (EReference) physicalAssetCapabilityPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentCapabilityProperty() {
		return equipmentCapabilityPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapabilityProperty_SubProperties() {
		return (EReference) equipmentCapabilityPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelCapabilityProperty() {
		return personnelCapabilityPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelCapabilityProperty_SubProperties() {
		return (EReference) personnelCapabilityPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialCapability() {
		return materialCapabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialCapability_AssembledFromCapabilities() {
		return (EReference) materialCapabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialCapability_MaterialCapabilityProperty() {
		return (EReference) materialCapabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialCapability_MaterialClass() {
		return (EReference) materialCapabilityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialCapability_MaterialDefinition() {
		return (EReference) materialCapabilityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialCapability_MaterialLot() {
		return (EReference) materialCapabilityEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialCapability_MaterialSublot() {
		return (EReference) materialCapabilityEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialCapability_Description() {
		return (EAttribute) materialCapabilityEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialCapability_CapabilityType() {
		return (EAttribute) materialCapabilityEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialCapability_Reason() {
		return (EAttribute) materialCapabilityEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialCapability_ConfidenceFactor() {
		return (EAttribute) materialCapabilityEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialCapability_MaterialUse() {
		return (EAttribute) materialCapabilityEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialCapability_StartTime() {
		return (EAttribute) materialCapabilityEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialCapability_EndTime() {
		return (EAttribute) materialCapabilityEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialCapability_Quantity() {
		return (EAttribute) materialCapabilityEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialCapability_QuantityUnitOfMeasure() {
		return (EAttribute) materialCapabilityEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialCapability_Hierarchyscope() {
		return (EReference) materialCapabilityEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialCapability_AssemblyType() {
		return (EAttribute) materialCapabilityEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialCapability_AssemblyRelationship() {
		return (EAttribute) materialCapabilityEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetCapability() {
		return physicalAssetCapabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapability_PhysicalAssetCapabilityProperty() {
		return (EReference) physicalAssetCapabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapability_PhysicalAssetClass() {
		return (EReference) physicalAssetCapabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapability_PhysicalAsset() {
		return (EReference) physicalAssetCapabilityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapability_Description() {
		return (EAttribute) physicalAssetCapabilityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapability_CapabilityType() {
		return (EAttribute) physicalAssetCapabilityEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapability_Reason() {
		return (EAttribute) physicalAssetCapabilityEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapability_ConfidenceFactor() {
		return (EAttribute) physicalAssetCapabilityEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapability_PhysicalAssetUse() {
		return (EAttribute) physicalAssetCapabilityEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapability_HierarchyScope() {
		return (EReference) physicalAssetCapabilityEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapability_StartTime() {
		return (EAttribute) physicalAssetCapabilityEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapability_EndTime() {
		return (EAttribute) physicalAssetCapabilityEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapability_Quantity() {
		return (EAttribute) physicalAssetCapabilityEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalAssetCapability_QuantityUnitOfMeasure() {
		return (EAttribute) physicalAssetCapabilityEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentCapability() {
		return equipmentCapabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapability_EquipmentCapabilityProperty() {
		return (EReference) equipmentCapabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapability_EquipmentClass() {
		return (EReference) equipmentCapabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapability_Equipment() {
		return (EReference) equipmentCapabilityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapability_Description() {
		return (EAttribute) equipmentCapabilityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapability_CapabilityType() {
		return (EAttribute) equipmentCapabilityEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapability_Reason() {
		return (EAttribute) equipmentCapabilityEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapability_ConfidenceFactor() {
		return (EAttribute) equipmentCapabilityEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapability_EquipmentUse() {
		return (EAttribute) equipmentCapabilityEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapability_StartTime() {
		return (EAttribute) equipmentCapabilityEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapability_EndTime() {
		return (EAttribute) equipmentCapabilityEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapability_Quantity() {
		return (EAttribute) equipmentCapabilityEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentCapability_QuantityUnitOfMeasure() {
		return (EAttribute) equipmentCapabilityEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapability_HierarchyScope() {
		return (EReference) equipmentCapabilityEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelCapability() {
		return personnelCapabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelCapability_PersonnelCapabilityProperty() {
		return (EReference) personnelCapabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelCapability_PersonnelClass() {
		return (EReference) personnelCapabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelCapability_Person() {
		return (EReference) personnelCapabilityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelCapability_Description() {
		return (EAttribute) personnelCapabilityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelCapability_CapabilityType() {
		return (EAttribute) personnelCapabilityEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelCapability_Reason() {
		return (EAttribute) personnelCapabilityEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelCapability_ConfidenceFactor() {
		return (EAttribute) personnelCapabilityEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelCapability_HierarchyScope() {
		return (EReference) personnelCapabilityEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelCapability_PersonnelUse() {
		return (EAttribute) personnelCapabilityEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelCapability_StartTime() {
		return (EAttribute) personnelCapabilityEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelCapability_EndTime() {
		return (EAttribute) personnelCapabilityEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelCapability_Quantity() {
		return (EAttribute) personnelCapabilityEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPersonnelCapability_QuantityUnitOfMeasure() {
		return (EAttribute) personnelCapabilityEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessSegmentCapability() {
		return processSegmentCapabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentCapability_SubCapabilities() {
		return (EReference) processSegmentCapabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentCapability_PersonnelCapabilities() {
		return (EReference) processSegmentCapabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentCapability_EquipmentCapabilities() {
		return (EReference) processSegmentCapabilityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentCapability_PhysicalAssetCapabilities() {
		return (EReference) processSegmentCapabilityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentCapability_MaterialCapabilities() {
		return (EReference) processSegmentCapabilityEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentCapability_ProcessSegment() {
		return (EReference) processSegmentCapabilityEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentCapability_Id() {
		return (EAttribute) processSegmentCapabilityEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentCapability_Description() {
		return (EAttribute) processSegmentCapabilityEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentCapability_CapacityType() {
		return (EAttribute) processSegmentCapabilityEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentCapability_Reason() {
		return (EAttribute) processSegmentCapabilityEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentCapability_StartTime() {
		return (EAttribute) processSegmentCapabilityEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentCapability_EndTime() {
		return (EAttribute) processSegmentCapabilityEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentCapability_HierarchyScope() {
		return (EReference) processSegmentCapabilityEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferencialProperty() {
		return referencialPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReferencialProperty_Description() {
		return (EAttribute) referencialPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReferencialProperty_Value() {
		return (EAttribute) referencialPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReferencialProperty_ValueUnitOfMeasure() {
		return (EAttribute) referencialPropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReferencialProperty_Quantity() {
		return (EAttribute) referencialPropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReferencialProperty_QuantityUnitOfMeasure() {
		return (EAttribute) referencialPropertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferentialPersonnelProperty() {
		return referentialPersonnelPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferentialPersonnelProperty_PersonnelClassProperty() {
		return (EReference) referentialPersonnelPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferentialPersonnelProperty_PersonProperty() {
		return (EReference) referentialPersonnelPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferentialEquipmentProperty() {
		return referentialEquipmentPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferentialEquipmentProperty_EquipmentClassProperty() {
		return (EReference) referentialEquipmentPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferentialEquipmentProperty_EquipmentProperty() {
		return (EReference) referentialEquipmentPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferentialPhysicalAssetProperty() {
		return referentialPhysicalAssetPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferentialPhysicalAssetProperty_PhysicalAssetClassProperty() {
		return (EReference) referentialPhysicalAssetPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferentialPhysicalAssetProperty_PhysicalAssetProperty() {
		return (EReference) referentialPhysicalAssetPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferentialMaterialTypeProperty() {
		return referentialMaterialTypePropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferentialMaterialTypeProperty_MaterialClassProperty() {
		return (EReference) referentialMaterialTypePropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferentialMaterialTypeProperty_MaterialDefinitionProperty() {
		return (EReference) referentialMaterialTypePropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferentialMaterialProperty() {
		return referentialMaterialPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferentialMaterialProperty_MaterialLotProperty() {
		return (EReference) referentialMaterialPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAssemblyType() {
		return assemblyTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAssemblyRelationship() {
		return assemblyRelationshipEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOperationsType() {
		return operationsTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMaterialUse() {
		return materialUseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getUseType() {
		return useTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getScheduleState() {
		return scheduleStateEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPerformanceState() {
		return performanceStateEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCapabilityType() {
		return capabilityTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCapacityType() {
		return capacityTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getZonedDateTime() {
		return zonedDateTimeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iec62264Factory getIec62264Factory() {
		return (Iec62264Factory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		personEClass = createEClass(PERSON);
		createEReference(personEClass, PERSON__PERSONNEL_CLASSES);
		createEReference(personEClass, PERSON__PROPERTIES);
		createEReference(personEClass, PERSON__TEST_SPECIFICATIONS);
		createEAttribute(personEClass, PERSON__ID);
		createEAttribute(personEClass, PERSON__DESCRIPTION);
		createEAttribute(personEClass, PERSON__NAME);

		personnelClassEClass = createEClass(PERSONNEL_CLASS);
		createEReference(personnelClassEClass, PERSONNEL_CLASS__PROPERTIES);
		createEReference(personnelClassEClass, PERSONNEL_CLASS__TEST_SPECIFICATIONS);
		createEAttribute(personnelClassEClass, PERSONNEL_CLASS__ID);
		createEAttribute(personnelClassEClass, PERSONNEL_CLASS__DESCRIPTION);

		qualificationTestSpecificationEClass = createEClass(QUALIFICATION_TEST_SPECIFICATION);
		createEAttribute(qualificationTestSpecificationEClass, QUALIFICATION_TEST_SPECIFICATION__ID);
		createEAttribute(qualificationTestSpecificationEClass, QUALIFICATION_TEST_SPECIFICATION__DESCRIPTION);
		createEAttribute(qualificationTestSpecificationEClass, QUALIFICATION_TEST_SPECIFICATION__VERSION);

		personnelClassPropertyEClass = createEClass(PERSONNEL_CLASS_PROPERTY);
		createEReference(personnelClassPropertyEClass, PERSONNEL_CLASS_PROPERTY__TEST_SPECIFICATIONS);
		createEReference(personnelClassPropertyEClass, PERSONNEL_CLASS_PROPERTY__SUB_PROPERTIES);
		createEAttribute(personnelClassPropertyEClass, PERSONNEL_CLASS_PROPERTY__ID);
		createEAttribute(personnelClassPropertyEClass, PERSONNEL_CLASS_PROPERTY__DESCRIPTION);
		createEAttribute(personnelClassPropertyEClass, PERSONNEL_CLASS_PROPERTY__VALUE);
		createEAttribute(personnelClassPropertyEClass, PERSONNEL_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE);

		personPropertyEClass = createEClass(PERSON_PROPERTY);
		createEReference(personPropertyEClass, PERSON_PROPERTY__SUB_PROPERTIES);
		createEReference(personPropertyEClass, PERSON_PROPERTY__PERSONNEL_CLASS_PROPERTY);
		createEAttribute(personPropertyEClass, PERSON_PROPERTY__ID);
		createEAttribute(personPropertyEClass, PERSON_PROPERTY__DESCRIPTION);
		createEAttribute(personPropertyEClass, PERSON_PROPERTY__VALUE);
		createEAttribute(personPropertyEClass, PERSON_PROPERTY__VALUE_UNIT_OF_MEASURE);

		qualificationTestResultEClass = createEClass(QUALIFICATION_TEST_RESULT);
		createEReference(qualificationTestResultEClass, QUALIFICATION_TEST_RESULT__TEST_SPECIFICATION);
		createEReference(qualificationTestResultEClass, QUALIFICATION_TEST_RESULT__PROPERTY);
		createEAttribute(qualificationTestResultEClass, QUALIFICATION_TEST_RESULT__ID);
		createEAttribute(qualificationTestResultEClass, QUALIFICATION_TEST_RESULT__DESCRIPTION);
		createEAttribute(qualificationTestResultEClass, QUALIFICATION_TEST_RESULT__DATE);
		createEAttribute(qualificationTestResultEClass, QUALIFICATION_TEST_RESULT__RESULT);
		createEAttribute(qualificationTestResultEClass, QUALIFICATION_TEST_RESULT__RESULT_UNIT_OF_MEASURE);
		createEAttribute(qualificationTestResultEClass, QUALIFICATION_TEST_RESULT__EXPIRATION);

		personnelModelEClass = createEClass(PERSONNEL_MODEL);
		createEReference(personnelModelEClass, PERSONNEL_MODEL__PERSONNEL_CLASSES);
		createEReference(personnelModelEClass, PERSONNEL_MODEL__PERSONS);
		createEReference(personnelModelEClass, PERSONNEL_MODEL__TEST_SPECIFICATIONS);
		createEReference(personnelModelEClass, PERSONNEL_MODEL__TEST_RESULTS);
		createEReference(personnelModelEClass, PERSONNEL_MODEL__OPERATIONS_CAPABILITY_MODEL);

		equipmentModelEClass = createEClass(EQUIPMENT_MODEL);
		createEReference(equipmentModelEClass, EQUIPMENT_MODEL__EQUIPMENT_CLASSES);
		createEReference(equipmentModelEClass, EQUIPMENT_MODEL__EQUIPMENTS);
		createEReference(equipmentModelEClass, EQUIPMENT_MODEL__TEST_SPECIFICATIONS);
		createEReference(equipmentModelEClass, EQUIPMENT_MODEL__TEST_RESULTS);

		equipmentClassEClass = createEClass(EQUIPMENT_CLASS);
		createEReference(equipmentClassEClass, EQUIPMENT_CLASS__PROPERTIES);
		createEReference(equipmentClassEClass, EQUIPMENT_CLASS__TEST_SPECIFICATIONS);
		createEAttribute(equipmentClassEClass, EQUIPMENT_CLASS__ID);
		createEAttribute(equipmentClassEClass, EQUIPMENT_CLASS__DESCRIPTION);
		createEAttribute(equipmentClassEClass, EQUIPMENT_CLASS__EQUIPMENT_LEVEL);

		equipmentCapabilityTestResultEClass = createEClass(EQUIPMENT_CAPABILITY_TEST_RESULT);
		createEReference(equipmentCapabilityTestResultEClass, EQUIPMENT_CAPABILITY_TEST_RESULT__TEST_SPECIFICATION);
		createEReference(equipmentCapabilityTestResultEClass, EQUIPMENT_CAPABILITY_TEST_RESULT__PROPERTY);
		createEAttribute(equipmentCapabilityTestResultEClass, EQUIPMENT_CAPABILITY_TEST_RESULT__ID);
		createEAttribute(equipmentCapabilityTestResultEClass, EQUIPMENT_CAPABILITY_TEST_RESULT__DESCRIPTION);
		createEAttribute(equipmentCapabilityTestResultEClass, EQUIPMENT_CAPABILITY_TEST_RESULT__DATE);
		createEAttribute(equipmentCapabilityTestResultEClass, EQUIPMENT_CAPABILITY_TEST_RESULT__RESULT);
		createEAttribute(equipmentCapabilityTestResultEClass, EQUIPMENT_CAPABILITY_TEST_RESULT__RESULT_UNIT_OF_MEASURE);
		createEAttribute(equipmentCapabilityTestResultEClass, EQUIPMENT_CAPABILITY_TEST_RESULT__EXPIRATION);

		equipmentCapabilityTestSpecificationEClass = createEClass(EQUIPMENT_CAPABILITY_TEST_SPECIFICATION);
		createEAttribute(equipmentCapabilityTestSpecificationEClass, EQUIPMENT_CAPABILITY_TEST_SPECIFICATION__ID);
		createEAttribute(equipmentCapabilityTestSpecificationEClass,
				EQUIPMENT_CAPABILITY_TEST_SPECIFICATION__DESCRIPTION);
		createEAttribute(equipmentCapabilityTestSpecificationEClass, EQUIPMENT_CAPABILITY_TEST_SPECIFICATION__VERSION);

		equipmentClassPropertyEClass = createEClass(EQUIPMENT_CLASS_PROPERTY);
		createEReference(equipmentClassPropertyEClass, EQUIPMENT_CLASS_PROPERTY__SUB_PROPERTIES);
		createEReference(equipmentClassPropertyEClass, EQUIPMENT_CLASS_PROPERTY__TEST_SPECIFICATIONS);
		createEAttribute(equipmentClassPropertyEClass, EQUIPMENT_CLASS_PROPERTY__ID);
		createEAttribute(equipmentClassPropertyEClass, EQUIPMENT_CLASS_PROPERTY__DESCRIPTION);
		createEAttribute(equipmentClassPropertyEClass, EQUIPMENT_CLASS_PROPERTY__VALUE);
		createEAttribute(equipmentClassPropertyEClass, EQUIPMENT_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE);

		equipmentPropertyEClass = createEClass(EQUIPMENT_PROPERTY);
		createEReference(equipmentPropertyEClass, EQUIPMENT_PROPERTY__SUB_PROPERTIES);
		createEReference(equipmentPropertyEClass, EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY);
		createEAttribute(equipmentPropertyEClass, EQUIPMENT_PROPERTY__ID);
		createEAttribute(equipmentPropertyEClass, EQUIPMENT_PROPERTY__DESCRIPTION);
		createEAttribute(equipmentPropertyEClass, EQUIPMENT_PROPERTY__VALUE);
		createEAttribute(equipmentPropertyEClass, EQUIPMENT_PROPERTY__VALUE_UNIT_OF_MEASURE);

		equipmentEClass = createEClass(EQUIPMENT);
		createEReference(equipmentEClass, EQUIPMENT__CHILDREN);
		createEReference(equipmentEClass, EQUIPMENT__PROPERTIES);
		createEReference(equipmentEClass, EQUIPMENT__TEST_SPECIFICATIONS);
		createEReference(equipmentEClass, EQUIPMENT__EQUIPMENT_CLASS);
		createEAttribute(equipmentEClass, EQUIPMENT__ID);
		createEAttribute(equipmentEClass, EQUIPMENT__DESCRIPTION);
		createEAttribute(equipmentEClass, EQUIPMENT__EQUIPMENT_LEVEL);

		physicalAssetModelEClass = createEClass(PHYSICAL_ASSET_MODEL);
		createEReference(physicalAssetModelEClass, PHYSICAL_ASSET_MODEL__PHYSICAL_ASSETS);
		createEReference(physicalAssetModelEClass, PHYSICAL_ASSET_MODEL__PHYSICAL_ASSET_CLASSES);
		createEReference(physicalAssetModelEClass, PHYSICAL_ASSET_MODEL__TEST_SPECIFICATIONS);
		createEReference(physicalAssetModelEClass, PHYSICAL_ASSET_MODEL__EQUIPMENT_ASSET_MAPPINGS);
		createEReference(physicalAssetModelEClass, PHYSICAL_ASSET_MODEL__TEST_RESULTS);

		physicalAssetClassEClass = createEClass(PHYSICAL_ASSET_CLASS);
		createEReference(physicalAssetClassEClass, PHYSICAL_ASSET_CLASS__PROPERTIES);
		createEReference(physicalAssetClassEClass, PHYSICAL_ASSET_CLASS__TEST_SPECIFICATIONS);
		createEAttribute(physicalAssetClassEClass, PHYSICAL_ASSET_CLASS__ID);
		createEAttribute(physicalAssetClassEClass, PHYSICAL_ASSET_CLASS__MANUFACTURER);
		createEAttribute(physicalAssetClassEClass, PHYSICAL_ASSET_CLASS__DESCRIPTION);

		physicalAssetCapabilityTestResultEClass = createEClass(PHYSICAL_ASSET_CAPABILITY_TEST_RESULT);
		createEReference(physicalAssetCapabilityTestResultEClass,
				PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__TEST_SPECIFICATION);
		createEReference(physicalAssetCapabilityTestResultEClass, PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__PROPERTY);
		createEAttribute(physicalAssetCapabilityTestResultEClass, PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__ID);
		createEAttribute(physicalAssetCapabilityTestResultEClass, PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DESCRIPTION);
		createEAttribute(physicalAssetCapabilityTestResultEClass, PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DATE);
		createEAttribute(physicalAssetCapabilityTestResultEClass, PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT);
		createEAttribute(physicalAssetCapabilityTestResultEClass,
				PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT_UNIT_OF_MEASURE);
		createEAttribute(physicalAssetCapabilityTestResultEClass, PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__EXPIRATION);

		physicalAssetPropertyEClass = createEClass(PHYSICAL_ASSET_PROPERTY);
		createEReference(physicalAssetPropertyEClass, PHYSICAL_ASSET_PROPERTY__SUB_PROPERTIES);
		createEAttribute(physicalAssetPropertyEClass, PHYSICAL_ASSET_PROPERTY__ID);
		createEAttribute(physicalAssetPropertyEClass, PHYSICAL_ASSET_PROPERTY__DESCRIPTION);
		createEAttribute(physicalAssetPropertyEClass, PHYSICAL_ASSET_PROPERTY__VALUE);
		createEAttribute(physicalAssetPropertyEClass, PHYSICAL_ASSET_PROPERTY__VALUE_UNIT_OF_MEASURE);
		createEReference(physicalAssetPropertyEClass, PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY);

		physicalAssetEClass = createEClass(PHYSICAL_ASSET);
		createEReference(physicalAssetEClass, PHYSICAL_ASSET__CHILDREN);
		createEReference(physicalAssetEClass, PHYSICAL_ASSET__PROPERTIES);
		createEReference(physicalAssetEClass, PHYSICAL_ASSET__PHYSICAL_ASSET_CLASS);
		createEReference(physicalAssetEClass, PHYSICAL_ASSET__TEST_SPECIFICATIONS);
		createEAttribute(physicalAssetEClass, PHYSICAL_ASSET__ID);
		createEAttribute(physicalAssetEClass, PHYSICAL_ASSET__DESCRIPTION);
		createEAttribute(physicalAssetEClass, PHYSICAL_ASSET__PHYSICAL_LOCATION);
		createEAttribute(physicalAssetEClass, PHYSICAL_ASSET__FIXED_ASSET_ID);
		createEAttribute(physicalAssetEClass, PHYSICAL_ASSET__VENDOR_ID);

		physicalAssetCapabilityTestSpecificationEClass = createEClass(PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION);
		createEAttribute(physicalAssetCapabilityTestSpecificationEClass,
				PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION__ID);
		createEAttribute(physicalAssetCapabilityTestSpecificationEClass,
				PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION__DESCRIPTION);
		createEAttribute(physicalAssetCapabilityTestSpecificationEClass,
				PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION__VERSION);

		physicalAssetClassPropertyEClass = createEClass(PHYSICAL_ASSET_CLASS_PROPERTY);
		createEReference(physicalAssetClassPropertyEClass, PHYSICAL_ASSET_CLASS_PROPERTY__SUB_PROPERTIES);
		createEReference(physicalAssetClassPropertyEClass, PHYSICAL_ASSET_CLASS_PROPERTY__TEST_SPECIFICATIONS);
		createEAttribute(physicalAssetClassPropertyEClass, PHYSICAL_ASSET_CLASS_PROPERTY__ID);
		createEAttribute(physicalAssetClassPropertyEClass, PHYSICAL_ASSET_CLASS_PROPERTY__DESCRIPTION);
		createEAttribute(physicalAssetClassPropertyEClass, PHYSICAL_ASSET_CLASS_PROPERTY__VALUE);
		createEAttribute(physicalAssetClassPropertyEClass, PHYSICAL_ASSET_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE);

		equipmentAssetMappingEClass = createEClass(EQUIPMENT_ASSET_MAPPING);
		createEReference(equipmentAssetMappingEClass, EQUIPMENT_ASSET_MAPPING__PHYSICAL_ASSET);
		createEReference(equipmentAssetMappingEClass, EQUIPMENT_ASSET_MAPPING__EQUIPMENT);
		createEAttribute(equipmentAssetMappingEClass, EQUIPMENT_ASSET_MAPPING__ID);
		createEAttribute(equipmentAssetMappingEClass, EQUIPMENT_ASSET_MAPPING__DESCRIPTION);
		createEAttribute(equipmentAssetMappingEClass, EQUIPMENT_ASSET_MAPPING__START_TIME);
		createEAttribute(equipmentAssetMappingEClass, EQUIPMENT_ASSET_MAPPING__END_TIME);

		materialModelEClass = createEClass(MATERIAL_MODEL);
		createEReference(materialModelEClass, MATERIAL_MODEL__MATERIAL_CLASSES);
		createEReference(materialModelEClass, MATERIAL_MODEL__MATERIAL_DEFINITIONS);
		createEReference(materialModelEClass, MATERIAL_MODEL__MATERIAL_LOTS);
		createEReference(materialModelEClass, MATERIAL_MODEL__MATERIAL_SUBLOTS);
		createEReference(materialModelEClass, MATERIAL_MODEL__TEST_SPECIFICATIONS);
		createEReference(materialModelEClass, MATERIAL_MODEL__TEST_RESULTS);

		materialClassEClass = createEClass(MATERIAL_CLASS);
		createEReference(materialClassEClass, MATERIAL_CLASS__PROPERTIES);
		createEReference(materialClassEClass, MATERIAL_CLASS__ASSEMBLED_FROM_CLASSES);
		createEReference(materialClassEClass, MATERIAL_CLASS__TEST_SPECIFICATIONS);
		createEAttribute(materialClassEClass, MATERIAL_CLASS__ID);
		createEAttribute(materialClassEClass, MATERIAL_CLASS__DESCRIPTION);
		createEAttribute(materialClassEClass, MATERIAL_CLASS__ASSEMBLY_TYPE);
		createEAttribute(materialClassEClass, MATERIAL_CLASS__ASSEMBLY_RELATIONSHIP);

		materialTestResultEClass = createEClass(MATERIAL_TEST_RESULT);
		createEReference(materialTestResultEClass, MATERIAL_TEST_RESULT__TEST_SPECIFICATION);
		createEReference(materialTestResultEClass, MATERIAL_TEST_RESULT__PROPERTY);
		createEAttribute(materialTestResultEClass, MATERIAL_TEST_RESULT__ID);
		createEAttribute(materialTestResultEClass, MATERIAL_TEST_RESULT__DESCRIPTION);
		createEAttribute(materialTestResultEClass, MATERIAL_TEST_RESULT__DATE);
		createEAttribute(materialTestResultEClass, MATERIAL_TEST_RESULT__RESULT);
		createEAttribute(materialTestResultEClass, MATERIAL_TEST_RESULT__RESULT_UNIT_OF_MEASURE);
		createEAttribute(materialTestResultEClass, MATERIAL_TEST_RESULT__EXPIRATION);

		materialTestSpecificationEClass = createEClass(MATERIAL_TEST_SPECIFICATION);
		createEAttribute(materialTestSpecificationEClass, MATERIAL_TEST_SPECIFICATION__ID);
		createEAttribute(materialTestSpecificationEClass, MATERIAL_TEST_SPECIFICATION__DESCRIPTION);
		createEAttribute(materialTestSpecificationEClass, MATERIAL_TEST_SPECIFICATION__VERSION);

		materialSublotEClass = createEClass(MATERIAL_SUBLOT);
		createEReference(materialSublotEClass, MATERIAL_SUBLOT__PROPERTIES);
		createEReference(materialSublotEClass, MATERIAL_SUBLOT__ASSEMBLED_FROM_LOTS);
		createEReference(materialSublotEClass, MATERIAL_SUBLOT__ASSEMBLED_FROM_SUBLOTS);
		createEReference(materialSublotEClass, MATERIAL_SUBLOT__SUBLOTS);
		createEAttribute(materialSublotEClass, MATERIAL_SUBLOT__ID);
		createEAttribute(materialSublotEClass, MATERIAL_SUBLOT__DESCRIPTION);
		createEAttribute(materialSublotEClass, MATERIAL_SUBLOT__ASSEMBLY_TYPE);
		createEAttribute(materialSublotEClass, MATERIAL_SUBLOT__ASSEMBLY_RELATIONSHIP);
		createEAttribute(materialSublotEClass, MATERIAL_SUBLOT__STATUS);
		createEAttribute(materialSublotEClass, MATERIAL_SUBLOT__STORAGE_LOCATION);
		createEAttribute(materialSublotEClass, MATERIAL_SUBLOT__QUANTITY);
		createEAttribute(materialSublotEClass, MATERIAL_SUBLOT__QUANTITY_UNIT_OF_MEASURE);

		materialLotEClass = createEClass(MATERIAL_LOT);
		createEReference(materialLotEClass, MATERIAL_LOT__PROPERTIES);
		createEReference(materialLotEClass, MATERIAL_LOT__ASSEMBLED_FROM_LOTS);
		createEReference(materialLotEClass, MATERIAL_LOT__ASSEMBLED_FROM_SUBLOTS);
		createEReference(materialLotEClass, MATERIAL_LOT__SUBLOTS);
		createEReference(materialLotEClass, MATERIAL_LOT__MATERIAL_DEFINITION);
		createEReference(materialLotEClass, MATERIAL_LOT__TEST_SPECIFICATIONS);
		createEAttribute(materialLotEClass, MATERIAL_LOT__ID);
		createEAttribute(materialLotEClass, MATERIAL_LOT__DESCRIPTION);
		createEAttribute(materialLotEClass, MATERIAL_LOT__ASSEMBLY_TYPE);
		createEAttribute(materialLotEClass, MATERIAL_LOT__ASSEMBLY_RELATIONSHIP);
		createEAttribute(materialLotEClass, MATERIAL_LOT__STATUS);
		createEAttribute(materialLotEClass, MATERIAL_LOT__STORAGE_LOCATION);
		createEAttribute(materialLotEClass, MATERIAL_LOT__QUANTITY);
		createEAttribute(materialLotEClass, MATERIAL_LOT__QUANTITY_UNIT_OF_MEASURE);

		materialLotPropertyEClass = createEClass(MATERIAL_LOT_PROPERTY);
		createEReference(materialLotPropertyEClass, MATERIAL_LOT_PROPERTY__SUB_PROPERTIES);
		createEReference(materialLotPropertyEClass, MATERIAL_LOT_PROPERTY__MATERIAL_DEFINITION_PROPERTY);
		createEAttribute(materialLotPropertyEClass, MATERIAL_LOT_PROPERTY__ID);
		createEAttribute(materialLotPropertyEClass, MATERIAL_LOT_PROPERTY__DESCRIPTION);
		createEAttribute(materialLotPropertyEClass, MATERIAL_LOT_PROPERTY__VALUE);
		createEAttribute(materialLotPropertyEClass, MATERIAL_LOT_PROPERTY__VALUE_UNIT_OF_MEASURE);

		materialDefinitionEClass = createEClass(MATERIAL_DEFINITION);
		createEReference(materialDefinitionEClass, MATERIAL_DEFINITION__PROPERTIES);
		createEReference(materialDefinitionEClass, MATERIAL_DEFINITION__ASSEMBLED_FROM_DEFINITIONS);
		createEReference(materialDefinitionEClass, MATERIAL_DEFINITION__MATERIAL_CLASSES);
		createEReference(materialDefinitionEClass, MATERIAL_DEFINITION__TEST_SPECIFICATIONS);
		createEAttribute(materialDefinitionEClass, MATERIAL_DEFINITION__ID);
		createEAttribute(materialDefinitionEClass, MATERIAL_DEFINITION__DESCRIPTION);
		createEAttribute(materialDefinitionEClass, MATERIAL_DEFINITION__ASSEMBLY_TYPE);
		createEAttribute(materialDefinitionEClass, MATERIAL_DEFINITION__ASSEMBLY_RELATIONSHIP);

		materialDefinitionPropertyEClass = createEClass(MATERIAL_DEFINITION_PROPERTY);
		createEReference(materialDefinitionPropertyEClass, MATERIAL_DEFINITION_PROPERTY__SUB_PROPERTIES);
		createEReference(materialDefinitionPropertyEClass, MATERIAL_DEFINITION_PROPERTY__MATERIAL_CLASS_PROPERTY);
		createEReference(materialDefinitionPropertyEClass, MATERIAL_DEFINITION_PROPERTY__TEST_SPECIFICATIONS);
		createEAttribute(materialDefinitionPropertyEClass, MATERIAL_DEFINITION_PROPERTY__ID);
		createEAttribute(materialDefinitionPropertyEClass, MATERIAL_DEFINITION_PROPERTY__DESCRIPTION);
		createEAttribute(materialDefinitionPropertyEClass, MATERIAL_DEFINITION_PROPERTY__VALUE);
		createEAttribute(materialDefinitionPropertyEClass, MATERIAL_DEFINITION_PROPERTY__VALUE_UNIT_OF_MEASURE);

		materialClassPropertyEClass = createEClass(MATERIAL_CLASS_PROPERTY);
		createEReference(materialClassPropertyEClass, MATERIAL_CLASS_PROPERTY__SUB_PROPERTIES);
		createEReference(materialClassPropertyEClass, MATERIAL_CLASS_PROPERTY__TEST_SPECIFICATIONS);
		createEAttribute(materialClassPropertyEClass, MATERIAL_CLASS_PROPERTY__ID);
		createEAttribute(materialClassPropertyEClass, MATERIAL_CLASS_PROPERTY__DESCRIPTION);
		createEAttribute(materialClassPropertyEClass, MATERIAL_CLASS_PROPERTY__VALUE);
		createEAttribute(materialClassPropertyEClass, MATERIAL_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE);

		processSegmentModelEClass = createEClass(PROCESS_SEGMENT_MODEL);
		createEReference(processSegmentModelEClass, PROCESS_SEGMENT_MODEL__PROCESS_SEGMENTS);
		createEReference(processSegmentModelEClass, PROCESS_SEGMENT_MODEL__PROCESS_SEGMENT_DEPENDENCIES);

		processSegmentEClass = createEClass(PROCESS_SEGMENT);
		createEReference(processSegmentEClass, PROCESS_SEGMENT__CHILDREN);
		createEReference(processSegmentEClass, PROCESS_SEGMENT__PROCESS_SEGMENT_PARAMETERS);
		createEReference(processSegmentEClass, PROCESS_SEGMENT__PERSONNEL_SEGMENT_SPECIFICATIONS);
		createEReference(processSegmentEClass, PROCESS_SEGMENT__EQUIPMENT_SEGMENT_SPECIFICATIONS);
		createEReference(processSegmentEClass, PROCESS_SEGMENT__PHYSICAL_ASSET_SEGMENT_SPECIFICATIONS);
		createEReference(processSegmentEClass, PROCESS_SEGMENT__MATERIAL_SEGMENT_SPECIFICATIONS);
		createEAttribute(processSegmentEClass, PROCESS_SEGMENT__ID);
		createEAttribute(processSegmentEClass, PROCESS_SEGMENT__DESCRIPTION);
		createEAttribute(processSegmentEClass, PROCESS_SEGMENT__OPERATIONS_TYPE);
		createEAttribute(processSegmentEClass, PROCESS_SEGMENT__DURATION);
		createEAttribute(processSegmentEClass, PROCESS_SEGMENT__DURATION_UNIT_OF_MEASURE);
		createEReference(processSegmentEClass, PROCESS_SEGMENT__HIERARCHY_SCOPE);

		personnelSegmentSpecificationPropertyEClass = createEClass(PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY);
		createEReference(personnelSegmentSpecificationPropertyEClass,
				PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES);

		equipmentSegmentSpecificationPropertyEClass = createEClass(EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY);
		createEReference(equipmentSegmentSpecificationPropertyEClass,
				EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES);

		physicalAssetSegmentSpecificationPropertyEClass = createEClass(PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY);
		createEReference(physicalAssetSegmentSpecificationPropertyEClass,
				PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES);

		materialSegmentSpecificationPropertyEClass = createEClass(MATERIAL_SEGMENT_SPECIFICATION_PROPERTY);
		createEReference(materialSegmentSpecificationPropertyEClass,
				MATERIAL_SEGMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES);

		materialSegmentSpecificationEClass = createEClass(MATERIAL_SEGMENT_SPECIFICATION);
		createEReference(materialSegmentSpecificationEClass,
				MATERIAL_SEGMENT_SPECIFICATION__ASSEMBLED_FROM_SPECIFICATIONS);
		createEReference(materialSegmentSpecificationEClass, MATERIAL_SEGMENT_SPECIFICATION__PROPERTIES);
		createEReference(materialSegmentSpecificationEClass, MATERIAL_SEGMENT_SPECIFICATION__MATERIAL_CLASSES);
		createEReference(materialSegmentSpecificationEClass, MATERIAL_SEGMENT_SPECIFICATION__MATERIAL_DEFINITIONS);
		createEAttribute(materialSegmentSpecificationEClass, MATERIAL_SEGMENT_SPECIFICATION__DESCRIPTION);
		createEAttribute(materialSegmentSpecificationEClass, MATERIAL_SEGMENT_SPECIFICATION__ASSEMBLY_TYPE);
		createEAttribute(materialSegmentSpecificationEClass, MATERIAL_SEGMENT_SPECIFICATION__ASSEMBLY_RELATIONSHIP);
		createEAttribute(materialSegmentSpecificationEClass, MATERIAL_SEGMENT_SPECIFICATION__MATERIAL_USE);
		createEAttribute(materialSegmentSpecificationEClass, MATERIAL_SEGMENT_SPECIFICATION__QUANTITY);
		createEAttribute(materialSegmentSpecificationEClass,
				MATERIAL_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASUREMENT);

		physicalAssetSegmentSpecificationEClass = createEClass(PHYSICAL_ASSET_SEGMENT_SPECIFICATION);
		createEReference(physicalAssetSegmentSpecificationEClass, PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PROPERTIES);
		createEReference(physicalAssetSegmentSpecificationEClass,
				PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_CLASSES);
		createEReference(physicalAssetSegmentSpecificationEClass,
				PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSETS);
		createEAttribute(physicalAssetSegmentSpecificationEClass, PHYSICAL_ASSET_SEGMENT_SPECIFICATION__DESCRIPTION);
		createEAttribute(physicalAssetSegmentSpecificationEClass,
				PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_USE);
		createEAttribute(physicalAssetSegmentSpecificationEClass, PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY);
		createEAttribute(physicalAssetSegmentSpecificationEClass,
				PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE);

		equipmentSegmentSpecificationEClass = createEClass(EQUIPMENT_SEGMENT_SPECIFICATION);
		createEReference(equipmentSegmentSpecificationEClass, EQUIPMENT_SEGMENT_SPECIFICATION__PROPERTIES);
		createEReference(equipmentSegmentSpecificationEClass, EQUIPMENT_SEGMENT_SPECIFICATION__EQUIPMENT_CLASSES);
		createEReference(equipmentSegmentSpecificationEClass, EQUIPMENT_SEGMENT_SPECIFICATION__EQUIPMENTS);
		createEAttribute(equipmentSegmentSpecificationEClass, EQUIPMENT_SEGMENT_SPECIFICATION__DESCRIPTION);
		createEAttribute(equipmentSegmentSpecificationEClass, EQUIPMENT_SEGMENT_SPECIFICATION__EQUIPMENT_USE);
		createEAttribute(equipmentSegmentSpecificationEClass, EQUIPMENT_SEGMENT_SPECIFICATION__QUANTITY);
		createEAttribute(equipmentSegmentSpecificationEClass,
				EQUIPMENT_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASUREMENT);

		personnelSegmentSpecificationEClass = createEClass(PERSONNEL_SEGMENT_SPECIFICATION);
		createEReference(personnelSegmentSpecificationEClass, PERSONNEL_SEGMENT_SPECIFICATION__PROPERTIES);
		createEReference(personnelSegmentSpecificationEClass, PERSONNEL_SEGMENT_SPECIFICATION__PERSONNEL_CLASSES);
		createEReference(personnelSegmentSpecificationEClass, PERSONNEL_SEGMENT_SPECIFICATION__PERSONS);
		createEAttribute(personnelSegmentSpecificationEClass, PERSONNEL_SEGMENT_SPECIFICATION__DESCRIPTION);
		createEAttribute(personnelSegmentSpecificationEClass, PERSONNEL_SEGMENT_SPECIFICATION__PERSONNEL_USE);
		createEAttribute(personnelSegmentSpecificationEClass, PERSONNEL_SEGMENT_SPECIFICATION__QUANTITY);
		createEAttribute(personnelSegmentSpecificationEClass,
				PERSONNEL_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE);

		processSegmentParameterEClass = createEClass(PROCESS_SEGMENT_PARAMETER);
		createEReference(processSegmentParameterEClass, PROCESS_SEGMENT_PARAMETER__SUB_PARAMETERS);
		createEAttribute(processSegmentParameterEClass, PROCESS_SEGMENT_PARAMETER__ID);
		createEAttribute(processSegmentParameterEClass, PROCESS_SEGMENT_PARAMETER__DESCRIPTION);
		createEAttribute(processSegmentParameterEClass, PROCESS_SEGMENT_PARAMETER__VALUE);
		createEAttribute(processSegmentParameterEClass, PROCESS_SEGMENT_PARAMETER__VALUE_UNIT_OF_MEASURE);

		processSegmentDependencyEClass = createEClass(PROCESS_SEGMENT_DEPENDENCY);
		createEReference(processSegmentDependencyEClass, PROCESS_SEGMENT_DEPENDENCY__SUBJECT);
		createEReference(processSegmentDependencyEClass, PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY);
		createEAttribute(processSegmentDependencyEClass, PROCESS_SEGMENT_DEPENDENCY__ID);
		createEAttribute(processSegmentDependencyEClass, PROCESS_SEGMENT_DEPENDENCY__DESCRIPTION);
		createEAttribute(processSegmentDependencyEClass, PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE);
		createEAttribute(processSegmentDependencyEClass, PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR);
		createEAttribute(processSegmentDependencyEClass, PROCESS_SEGMENT_DEPENDENCY__FACTOR_UNIT_OF_MEASURE);

		iec62264ModelEClass = createEClass(IEC62264_MODEL);
		createEReference(iec62264ModelEClass, IEC62264_MODEL__PERSONNEL_MODEL);
		createEReference(iec62264ModelEClass, IEC62264_MODEL__EQUIPMENT_MODEL);
		createEReference(iec62264ModelEClass, IEC62264_MODEL__PHYSICAL_ASSET_MODEL);
		createEReference(iec62264ModelEClass, IEC62264_MODEL__MATERIAL_MODEL);
		createEReference(iec62264ModelEClass, IEC62264_MODEL__PROCESS_SEGMENT_MODEL);
		createEReference(iec62264ModelEClass, IEC62264_MODEL__OPERATIONS_DEFINITION_MODEL);
		createEReference(iec62264ModelEClass, IEC62264_MODEL__OPERATIONS_SCHEDULE_MODEL);
		createEReference(iec62264ModelEClass, IEC62264_MODEL__OPERATIONS_PERFORMANCE_MODEL);
		createEReference(iec62264ModelEClass, IEC62264_MODEL__PROCESS_SEGMENT_CAPABILITY_MODEL);
		createEReference(iec62264ModelEClass, IEC62264_MODEL__HIERARCHY_SCOPES);
		createEAttribute(iec62264ModelEClass, IEC62264_MODEL__NAME);

		operationsDefinitionModelEClass = createEClass(OPERATIONS_DEFINITION_MODEL);
		createEReference(operationsDefinitionModelEClass, OPERATIONS_DEFINITION_MODEL__OPERATIONS_DEFINITIONS);
		createEReference(operationsDefinitionModelEClass, OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENTS);
		createEReference(operationsDefinitionModelEClass, OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENT_DEPENDENCIES);

		operationsScheduleModelEClass = createEClass(OPERATIONS_SCHEDULE_MODEL);
		createEReference(operationsScheduleModelEClass, OPERATIONS_SCHEDULE_MODEL__OPERATIONS_SCHEDULES);

		operationsPerformanceModelEClass = createEClass(OPERATIONS_PERFORMANCE_MODEL);
		createEReference(operationsPerformanceModelEClass, OPERATIONS_PERFORMANCE_MODEL__OPERATIONS_PERFORMANCES);

		operationsCapabilityModelEClass = createEClass(OPERATIONS_CAPABILITY_MODEL);
		createEReference(operationsCapabilityModelEClass, OPERATIONS_CAPABILITY_MODEL__OPERATIONS_CAPABILITIES);

		processSegmentCapabilityModelEClass = createEClass(PROCESS_SEGMENT_CAPABILITY_MODEL);
		createEReference(processSegmentCapabilityModelEClass,
				PROCESS_SEGMENT_CAPABILITY_MODEL__PROCESS_SEGMENT_CAPABILITIES);

		hierarchyScopeEClass = createEClass(HIERARCHY_SCOPE);
		createEReference(hierarchyScopeEClass, HIERARCHY_SCOPE__SUB_SCOPE);
		createEReference(hierarchyScopeEClass, HIERARCHY_SCOPE__EQUIPMENT);
		createEAttribute(hierarchyScopeEClass, HIERARCHY_SCOPE__EQUIPMENT_ELEMENT_LEVEL);

		materialSpecificationPropertyEClass = createEClass(MATERIAL_SPECIFICATION_PROPERTY);
		createEReference(materialSpecificationPropertyEClass, MATERIAL_SPECIFICATION_PROPERTY__SUB_PROPERTIES);

		physicalAssetSpecificationPropertyEClass = createEClass(PHYSICAL_ASSET_SPECIFICATION_PROPERTY);
		createEReference(physicalAssetSpecificationPropertyEClass,
				PHYSICAL_ASSET_SPECIFICATION_PROPERTY__SUB_PROPERTIES);

		equipmentSpecificationPropertyEClass = createEClass(EQUIPMENT_SPECIFICATION_PROPERTY);
		createEReference(equipmentSpecificationPropertyEClass, EQUIPMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES);

		personnelSpecificationPropertyEClass = createEClass(PERSONNEL_SPECIFICATION_PROPERTY);
		createEReference(personnelSpecificationPropertyEClass, PERSONNEL_SPECIFICATION_PROPERTY__SUB_PROPERTIES);

		materialSpecificationEClass = createEClass(MATERIAL_SPECIFICATION);
		createEReference(materialSpecificationEClass, MATERIAL_SPECIFICATION__ASSEMBLED_FROM_SPECIFICATIONS);
		createEReference(materialSpecificationEClass, MATERIAL_SPECIFICATION__MATERIAL_SPECIFICATION_PROPERTIES);
		createEReference(materialSpecificationEClass, MATERIAL_SPECIFICATION__MATERIAL_CLASSES);
		createEReference(materialSpecificationEClass, MATERIAL_SPECIFICATION__MATERIAL_DEFINITIONS);
		createEAttribute(materialSpecificationEClass, MATERIAL_SPECIFICATION__DESCRIPTION);
		createEAttribute(materialSpecificationEClass, MATERIAL_SPECIFICATION__MATERIAL_USE);
		createEAttribute(materialSpecificationEClass, MATERIAL_SPECIFICATION__QUANTITY);
		createEAttribute(materialSpecificationEClass, MATERIAL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE);
		createEAttribute(materialSpecificationEClass, MATERIAL_SPECIFICATION__ASSEMBLY_TYPE);
		createEAttribute(materialSpecificationEClass, MATERIAL_SPECIFICATION__ASSEMBLY_RELATIONSHIP);

		physicalAssetSpecificationEClass = createEClass(PHYSICAL_ASSET_SPECIFICATION);
		createEReference(physicalAssetSpecificationEClass,
				PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_SPECIFICATION_PROPERTIES);
		createEAttribute(physicalAssetSpecificationEClass, PHYSICAL_ASSET_SPECIFICATION__DESCRIPTION);
		createEReference(physicalAssetSpecificationEClass, PHYSICAL_ASSET_SPECIFICATION__PHYSIC_ALASSET_CLASSES);
		createEReference(physicalAssetSpecificationEClass, PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSETS);
		createEAttribute(physicalAssetSpecificationEClass, PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_USE);
		createEAttribute(physicalAssetSpecificationEClass, PHYSICAL_ASSET_SPECIFICATION__QUANTITY);
		createEAttribute(physicalAssetSpecificationEClass, PHYSICAL_ASSET_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE);

		equipmentSpecificationEClass = createEClass(EQUIPMENT_SPECIFICATION);
		createEReference(equipmentSpecificationEClass, EQUIPMENT_SPECIFICATION__EQUIPMENT_SPECIFICATION_PROPERTIES);
		createEReference(equipmentSpecificationEClass, EQUIPMENT_SPECIFICATION__EQUIPMENT_CLASSES);
		createEReference(equipmentSpecificationEClass, EQUIPMENT_SPECIFICATION__EQUIPMENTS);
		createEAttribute(equipmentSpecificationEClass, EQUIPMENT_SPECIFICATION__DESCRIPTION);
		createEAttribute(equipmentSpecificationEClass, EQUIPMENT_SPECIFICATION__EQUIPMENT_USE);
		createEAttribute(equipmentSpecificationEClass, EQUIPMENT_SPECIFICATION__QUANTITY);
		createEAttribute(equipmentSpecificationEClass, EQUIPMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE);

		personnelSpecificationEClass = createEClass(PERSONNEL_SPECIFICATION);
		createEReference(personnelSpecificationEClass, PERSONNEL_SPECIFICATION__PERSONNEL_SPECIFICATION_PROPERTIES);
		createEReference(personnelSpecificationEClass, PERSONNEL_SPECIFICATION__PERSONNEL_CLASSES);
		createEReference(personnelSpecificationEClass, PERSONNEL_SPECIFICATION__PERSONS);
		createEAttribute(personnelSpecificationEClass, PERSONNEL_SPECIFICATION__DESCRIPTION);
		createEAttribute(personnelSpecificationEClass, PERSONNEL_SPECIFICATION__PERSONNEL_USE);
		createEAttribute(personnelSpecificationEClass, PERSONNEL_SPECIFICATION__QUANTITY);
		createEAttribute(personnelSpecificationEClass, PERSONNEL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE);

		parameterSpecificationEClass = createEClass(PARAMETER_SPECIFICATION);
		createEReference(parameterSpecificationEClass, PARAMETER_SPECIFICATION__SUB_PARAMETERS);
		createEAttribute(parameterSpecificationEClass, PARAMETER_SPECIFICATION__ID);
		createEAttribute(parameterSpecificationEClass, PARAMETER_SPECIFICATION__DESCRIPTION);
		createEAttribute(parameterSpecificationEClass, PARAMETER_SPECIFICATION__VALUE);
		createEAttribute(parameterSpecificationEClass, PARAMETER_SPECIFICATION__VALUE_UNIT_OF_MEASUREMENT);

		operationsSegmentDependencyEClass = createEClass(OPERATIONS_SEGMENT_DEPENDENCY);
		createEReference(operationsSegmentDependencyEClass, OPERATIONS_SEGMENT_DEPENDENCY__SUBJECT);
		createEReference(operationsSegmentDependencyEClass, OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY);
		createEAttribute(operationsSegmentDependencyEClass, OPERATIONS_SEGMENT_DEPENDENCY__ID);
		createEAttribute(operationsSegmentDependencyEClass, OPERATIONS_SEGMENT_DEPENDENCY__DESCRIPTION);
		createEAttribute(operationsSegmentDependencyEClass, OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE);
		createEAttribute(operationsSegmentDependencyEClass, OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR);
		createEAttribute(operationsSegmentDependencyEClass, OPERATIONS_SEGMENT_DEPENDENCY__UNIT_OF_MEASURE);

		operationsMaterialBillItemEClass = createEClass(OPERATIONS_MATERIAL_BILL_ITEM);
		createEReference(operationsMaterialBillItemEClass, OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_SPECIFICATIONS);
		createEReference(operationsMaterialBillItemEClass, OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLED_FROM_ITEMS);
		createEAttribute(operationsMaterialBillItemEClass, OPERATIONS_MATERIAL_BILL_ITEM__ID);
		createEAttribute(operationsMaterialBillItemEClass, OPERATIONS_MATERIAL_BILL_ITEM__DESCRIPTION);
		createEReference(operationsMaterialBillItemEClass, OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_CLASSES);
		createEReference(operationsMaterialBillItemEClass, OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_DEFINITIONS);
		createEAttribute(operationsMaterialBillItemEClass, OPERATIONS_MATERIAL_BILL_ITEM__USE_TYPE);
		createEAttribute(operationsMaterialBillItemEClass, OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_TYPE);
		createEAttribute(operationsMaterialBillItemEClass, OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_RELATIONSHIP);
		createEAttribute(operationsMaterialBillItemEClass, OPERATIONS_MATERIAL_BILL_ITEM__QUANTITIES);
		createEAttribute(operationsMaterialBillItemEClass, OPERATIONS_MATERIAL_BILL_ITEM__QUANTITY_UNIT_OF_MEASURES);

		operationsMaterialBillEClass = createEClass(OPERATIONS_MATERIAL_BILL);
		createEReference(operationsMaterialBillEClass, OPERATIONS_MATERIAL_BILL__OPERATIONS_MATERIAL_BILL_ITEMS);
		createEAttribute(operationsMaterialBillEClass, OPERATIONS_MATERIAL_BILL__ID);
		createEAttribute(operationsMaterialBillEClass, OPERATIONS_MATERIAL_BILL__DESCRIPTION);

		operationsSegmentEClass = createEClass(OPERATIONS_SEGMENT);
		createEReference(operationsSegmentEClass, OPERATIONS_SEGMENT__PROCESS_SEGMENTS);
		createEReference(operationsSegmentEClass, OPERATIONS_SEGMENT__SUB_SEGMENTS);
		createEReference(operationsSegmentEClass, OPERATIONS_SEGMENT__PARAMETER_SPECIFICATIONS);
		createEReference(operationsSegmentEClass, OPERATIONS_SEGMENT__PERSONNEL_SPECIFICATIONS);
		createEReference(operationsSegmentEClass, OPERATIONS_SEGMENT__EQUIPMENT_SPECIFICATIONS);
		createEReference(operationsSegmentEClass, OPERATIONS_SEGMENT__PHYSICAL_ASSET_SPECIFICATIONS);
		createEReference(operationsSegmentEClass, OPERATIONS_SEGMENT__MATERIAL_SPECIFICATIONS);
		createEAttribute(operationsSegmentEClass, OPERATIONS_SEGMENT__ID);
		createEAttribute(operationsSegmentEClass, OPERATIONS_SEGMENT__DESCRIPTION);
		createEAttribute(operationsSegmentEClass, OPERATIONS_SEGMENT__DURATION);
		createEAttribute(operationsSegmentEClass, OPERATIONS_SEGMENT__DURATION_UNIT_OF_MEASURE);
		createEAttribute(operationsSegmentEClass, OPERATIONS_SEGMENT__OPERATIONS_TYPE);
		createEAttribute(operationsSegmentEClass, OPERATIONS_SEGMENT__WORK_DEFINITION_ID);

		operationsDefinitionEClass = createEClass(OPERATIONS_DEFINITION);
		createEReference(operationsDefinitionEClass, OPERATIONS_DEFINITION__OPERATIONS_SEGMENTS);
		createEReference(operationsDefinitionEClass, OPERATIONS_DEFINITION__OPERATIONS_MATERIAL_BILLS);
		createEAttribute(operationsDefinitionEClass, OPERATIONS_DEFINITION__ID);
		createEAttribute(operationsDefinitionEClass, OPERATIONS_DEFINITION__VERSION);
		createEAttribute(operationsDefinitionEClass, OPERATIONS_DEFINITION__DESCRIPTION);
		createEAttribute(operationsDefinitionEClass, OPERATIONS_DEFINITION__OPERATIONS_TYPE);
		createEAttribute(operationsDefinitionEClass, OPERATIONS_DEFINITION__BILL_OF_MATERIAL_ID);
		createEAttribute(operationsDefinitionEClass, OPERATIONS_DEFINITION__WORK_DEFINITION_ID);
		createEAttribute(operationsDefinitionEClass, OPERATIONS_DEFINITION__BILL_OF_RESOURCE_ID);
		createEReference(operationsDefinitionEClass, OPERATIONS_DEFINITION__HIERARCHYS_SCOPE);

		materialRequirementEClass = createEClass(MATERIAL_REQUIREMENT);
		createEReference(materialRequirementEClass, MATERIAL_REQUIREMENT__MATERIAL_REQUIREMENT_PROPERTIES);
		createEReference(materialRequirementEClass, MATERIAL_REQUIREMENT__ASSEMBLED_FROM_REQUIREMENTS);
		createEReference(materialRequirementEClass, MATERIAL_REQUIREMENT__MATERIAL_CLASSES);
		createEReference(materialRequirementEClass, MATERIAL_REQUIREMENT__MATERIAL_DEFINITIONS);
		createEReference(materialRequirementEClass, MATERIAL_REQUIREMENT__MATERIAL_LOTS);
		createEReference(materialRequirementEClass, MATERIAL_REQUIREMENT__MATERIAL_SUBLOT);
		createEAttribute(materialRequirementEClass, MATERIAL_REQUIREMENT__DESCRIPTION);
		createEAttribute(materialRequirementEClass, MATERIAL_REQUIREMENT__MATERIAL_USE);
		createEAttribute(materialRequirementEClass, MATERIAL_REQUIREMENT__STORAGE_LOCATION);
		createEAttribute(materialRequirementEClass, MATERIAL_REQUIREMENT__QUANTITY);
		createEAttribute(materialRequirementEClass, MATERIAL_REQUIREMENT__QUANTITY_UNIT_OF_MEASURE);
		createEAttribute(materialRequirementEClass, MATERIAL_REQUIREMENT__ASSEMBLY_TYPE);
		createEAttribute(materialRequirementEClass, MATERIAL_REQUIREMENT__ASSEMBLY_RELATIONSHIP);

		materialRequirementPropertyEClass = createEClass(MATERIAL_REQUIREMENT_PROPERTY);
		createEReference(materialRequirementPropertyEClass, MATERIAL_REQUIREMENT_PROPERTY__SUB_PROPERTIES);

		physicalAssetRequirementPropertyEClass = createEClass(PHYSICAL_ASSET_REQUIREMENT_PROPERTY);
		createEReference(physicalAssetRequirementPropertyEClass, PHYSICAL_ASSET_REQUIREMENT_PROPERTY__SUB_PROPERTIES);

		equipmentRequirementPropertyEClass = createEClass(EQUIPMENT_REQUIREMENT_PROPERTY);
		createEReference(equipmentRequirementPropertyEClass, EQUIPMENT_REQUIREMENT_PROPERTY__SUB_PROPERTIES);

		personnelRequirementPropertyEClass = createEClass(PERSONNEL_REQUIREMENT_PROPERTY);
		createEReference(personnelRequirementPropertyEClass, PERSONNEL_REQUIREMENT_PROPERTY__SUB_PROPERTIES);

		operationsScheduleEClass = createEClass(OPERATIONS_SCHEDULE);
		createEReference(operationsScheduleEClass, OPERATIONS_SCHEDULE__OPERATIONS_REQUESTS);
		createEAttribute(operationsScheduleEClass, OPERATIONS_SCHEDULE__ID);
		createEAttribute(operationsScheduleEClass, OPERATIONS_SCHEDULE__DESCRIPTION);
		createEAttribute(operationsScheduleEClass, OPERATIONS_SCHEDULE__OPERATIONS_TYPE);
		createEAttribute(operationsScheduleEClass, OPERATIONS_SCHEDULE__START_TIME);
		createEAttribute(operationsScheduleEClass, OPERATIONS_SCHEDULE__END_TIME);
		createEAttribute(operationsScheduleEClass, OPERATIONS_SCHEDULE__PUBLISHED_DATE);
		createEAttribute(operationsScheduleEClass, OPERATIONS_SCHEDULE__SCHEDULED_STATE);
		createEReference(operationsScheduleEClass, OPERATIONS_SCHEDULE__HIERARCHY_SCOPE);

		operationsRequestEClass = createEClass(OPERATIONS_REQUEST);
		createEReference(operationsRequestEClass, OPERATIONS_REQUEST__SEGMENT_REQUIREMENTS);
		createEReference(operationsRequestEClass, OPERATIONS_REQUEST__REQUESTED_SEGMENT_RESPONSES);
		createEReference(operationsRequestEClass, OPERATIONS_REQUEST__OPERATIONS_DEFINITION);
		createEAttribute(operationsRequestEClass, OPERATIONS_REQUEST__ID);
		createEAttribute(operationsRequestEClass, OPERATIONS_REQUEST__DESCRIPTION);
		createEAttribute(operationsRequestEClass, OPERATIONS_REQUEST__OPERATIONS_TYPE);
		createEAttribute(operationsRequestEClass, OPERATIONS_REQUEST__START_TIME);
		createEAttribute(operationsRequestEClass, OPERATIONS_REQUEST__END_TIME);
		createEAttribute(operationsRequestEClass, OPERATIONS_REQUEST__PRIORITY);
		createEAttribute(operationsRequestEClass, OPERATIONS_REQUEST__REQUEST_STATE);
		createEReference(operationsRequestEClass, OPERATIONS_REQUEST__HIERARCHY_SCOPE);

		requestedSegmentResponseEClass = createEClass(REQUESTED_SEGMENT_RESPONSE);

		segmentRequirementEClass = createEClass(SEGMENT_REQUIREMENT);
		createEReference(segmentRequirementEClass, SEGMENT_REQUIREMENT__SUB_REQUIREMENTS);
		createEReference(segmentRequirementEClass, SEGMENT_REQUIREMENT__PROCESS_SEGMENT);
		createEReference(segmentRequirementEClass, SEGMENT_REQUIREMENT__OPERATIONS_DEFINITION);
		createEReference(segmentRequirementEClass, SEGMENT_REQUIREMENT__SEGMENT_PARAMETERS);
		createEReference(segmentRequirementEClass, SEGMENT_REQUIREMENT__PERSONNEL_REQUIREMENTS);
		createEReference(segmentRequirementEClass, SEGMENT_REQUIREMENT__EQUIPMENT_REQUIREMENTS);
		createEReference(segmentRequirementEClass, SEGMENT_REQUIREMENT__PHYSICAL_ASSET_REQUIREMENTS);
		createEReference(segmentRequirementEClass, SEGMENT_REQUIREMENT__MATERIAL_REQUIREMENTS);
		createEAttribute(segmentRequirementEClass, SEGMENT_REQUIREMENT__ID);
		createEAttribute(segmentRequirementEClass, SEGMENT_REQUIREMENT__DESCRIPTION);
		createEAttribute(segmentRequirementEClass, SEGMENT_REQUIREMENT__OPERATIONS_TYPE);
		createEAttribute(segmentRequirementEClass, SEGMENT_REQUIREMENT__EARLIEST_START_TIME);
		createEAttribute(segmentRequirementEClass, SEGMENT_REQUIREMENT__LATEST_START_TIME);
		createEAttribute(segmentRequirementEClass, SEGMENT_REQUIREMENT__DURATION);
		createEAttribute(segmentRequirementEClass, SEGMENT_REQUIREMENT__DURATION_UNIT_OF_MEASUREMENT);
		createEAttribute(segmentRequirementEClass, SEGMENT_REQUIREMENT__SEGMENT_STATE);
		createEReference(segmentRequirementEClass, SEGMENT_REQUIREMENT__HIERARCHY_SCOPE);

		equipmentRequirementEClass = createEClass(EQUIPMENT_REQUIREMENT);
		createEReference(equipmentRequirementEClass, EQUIPMENT_REQUIREMENT__EQUIPMENT_REQUIREMENT_PROPERTIES);
		createEReference(equipmentRequirementEClass, EQUIPMENT_REQUIREMENT__EQUIPMENT_CLASSES);
		createEReference(equipmentRequirementEClass, EQUIPMENT_REQUIREMENT__EQUIPMENTS);
		createEAttribute(equipmentRequirementEClass, EQUIPMENT_REQUIREMENT__DESCRIPTION);
		createEAttribute(equipmentRequirementEClass, EQUIPMENT_REQUIREMENT__EQUIPMENT_USE);
		createEAttribute(equipmentRequirementEClass, EQUIPMENT_REQUIREMENT__QUANTITY);
		createEAttribute(equipmentRequirementEClass, EQUIPMENT_REQUIREMENT__QUANTITY_UNIT_OF_MEASURE);
		createEAttribute(equipmentRequirementEClass, EQUIPMENT_REQUIREMENT__EQUIPMENT_LEVEL);

		physicalAssetRequirementEClass = createEClass(PHYSICAL_ASSET_REQUIREMENT);
		createEReference(physicalAssetRequirementEClass,
				PHYSICAL_ASSET_REQUIREMENT__PHYSICAL_ASSET_REQUIREMENT_PROPERTIES);
		createEReference(physicalAssetRequirementEClass, PHYSICAL_ASSET_REQUIREMENT__PHYSICAL_ASSET_CLASSES);
		createEReference(physicalAssetRequirementEClass, PHYSICAL_ASSET_REQUIREMENT__PHYSICAL_ASSETS);
		createEAttribute(physicalAssetRequirementEClass, PHYSICAL_ASSET_REQUIREMENT__DESCRIPTION);
		createEAttribute(physicalAssetRequirementEClass, PHYSICAL_ASSET_REQUIREMENT__PHYSICAL_ASSET_USE);
		createEAttribute(physicalAssetRequirementEClass, PHYSICAL_ASSET_REQUIREMENT__QUANTITY);
		createEAttribute(physicalAssetRequirementEClass, PHYSICAL_ASSET_REQUIREMENT__QUANTITY_UNIT_OF_MEASURE);
		createEAttribute(physicalAssetRequirementEClass, PHYSICAL_ASSET_REQUIREMENT__EQUIPMENT_LEVEL);

		personnelRequirementEClass = createEClass(PERSONNEL_REQUIREMENT);
		createEReference(personnelRequirementEClass, PERSONNEL_REQUIREMENT__PERSONNEL_REQUIREMENT_PROPERTIES);
		createEReference(personnelRequirementEClass, PERSONNEL_REQUIREMENT__PERSONNEL_CLASSES);
		createEReference(personnelRequirementEClass, PERSONNEL_REQUIREMENT__PERSONS);
		createEAttribute(personnelRequirementEClass, PERSONNEL_REQUIREMENT__DESCRIPTION);
		createEAttribute(personnelRequirementEClass, PERSONNEL_REQUIREMENT__PERSONNEL_USE);
		createEAttribute(personnelRequirementEClass, PERSONNEL_REQUIREMENT__QUANTITY);
		createEAttribute(personnelRequirementEClass, PERSONNEL_REQUIREMENT__QUANTITY_UNIT_OF_MEASURE);

		segmentParameterEClass = createEClass(SEGMENT_PARAMETER);
		createEReference(segmentParameterEClass, SEGMENT_PARAMETER__SUB_PARAMETERS);
		createEAttribute(segmentParameterEClass, SEGMENT_PARAMETER__ID);
		createEAttribute(segmentParameterEClass, SEGMENT_PARAMETER__DESCRIPTION);
		createEAttribute(segmentParameterEClass, SEGMENT_PARAMETER__VALUE);
		createEAttribute(segmentParameterEClass, SEGMENT_PARAMETER__VALUE_UNIT_OF_MEASURE);

		materialActualPropertyEClass = createEClass(MATERIAL_ACTUAL_PROPERTY);
		createEReference(materialActualPropertyEClass, MATERIAL_ACTUAL_PROPERTY__SUB_PROPERTIES);

		physicalAssetActualPropertyEClass = createEClass(PHYSICAL_ASSET_ACTUAL_PROPERTY);
		createEReference(physicalAssetActualPropertyEClass, PHYSICAL_ASSET_ACTUAL_PROPERTY__SUB_PROPERTIES);

		equipmentActualPropertyEClass = createEClass(EQUIPMENT_ACTUAL_PROPERTY);
		createEReference(equipmentActualPropertyEClass, EQUIPMENT_ACTUAL_PROPERTY__SUB_PROPERTIES);

		personnelActualPropertyEClass = createEClass(PERSONNEL_ACTUAL_PROPERTY);
		createEReference(personnelActualPropertyEClass, PERSONNEL_ACTUAL_PROPERTY__SUB_PROPERTIES);

		materialActualEClass = createEClass(MATERIAL_ACTUAL);
		createEReference(materialActualEClass, MATERIAL_ACTUAL__ASSEMBLED_FROM_ACTUALS);
		createEReference(materialActualEClass, MATERIAL_ACTUAL__MATERIAL_ACTUAL_PROPERTIES);
		createEAttribute(materialActualEClass, MATERIAL_ACTUAL__DESCRIPTION);
		createEAttribute(materialActualEClass, MATERIAL_ACTUAL__MATERIAL_USE);
		createEAttribute(materialActualEClass, MATERIAL_ACTUAL__STORAGE_LOCATION);
		createEAttribute(materialActualEClass, MATERIAL_ACTUAL__QUANTITY);
		createEAttribute(materialActualEClass, MATERIAL_ACTUAL__QUANTITY_UNIT_OF_MEASURE);
		createEAttribute(materialActualEClass, MATERIAL_ACTUAL__ASSEMBLY_TYPE);
		createEAttribute(materialActualEClass, MATERIAL_ACTUAL__ASSEMBLY_RELATIONSHIP);
		createEReference(materialActualEClass, MATERIAL_ACTUAL__MATERIAL_CLASSES);
		createEReference(materialActualEClass, MATERIAL_ACTUAL__MATERIAL_DEFINITIONS);
		createEReference(materialActualEClass, MATERIAL_ACTUAL__MATERIAL_LOTS);
		createEReference(materialActualEClass, MATERIAL_ACTUAL__MATERIAL_SUBLOTS);

		physicalAssetActualEClass = createEClass(PHYSICAL_ASSET_ACTUAL);
		createEReference(physicalAssetActualEClass, PHYSICAL_ASSET_ACTUAL__PHYSICAL_ASSET_ACTUAL_PROPERTIES);
		createEAttribute(physicalAssetActualEClass, PHYSICAL_ASSET_ACTUAL__DESCRIPTION);
		createEAttribute(physicalAssetActualEClass, PHYSICAL_ASSET_ACTUAL__PHYSICAL_ASSET_USE);
		createEAttribute(physicalAssetActualEClass, PHYSICAL_ASSET_ACTUAL__QUANTITY);
		createEAttribute(physicalAssetActualEClass, PHYSICAL_ASSET_ACTUAL__QUANTITY_UNIT_OF_MEASURE);
		createEReference(physicalAssetActualEClass, PHYSICAL_ASSET_ACTUAL__PHYSICAL_ASSET_CLASSES);
		createEReference(physicalAssetActualEClass, PHYSICAL_ASSET_ACTUAL__PHYSICAL_ASSETS);

		equipmentActualEClass = createEClass(EQUIPMENT_ACTUAL);
		createEReference(equipmentActualEClass, EQUIPMENT_ACTUAL__EQUIPMENT_ACTUAL_PROPERTIES);
		createEAttribute(equipmentActualEClass, EQUIPMENT_ACTUAL__DESCRIPTION);
		createEAttribute(equipmentActualEClass, EQUIPMENT_ACTUAL__EQUIPMENT_USE);
		createEAttribute(equipmentActualEClass, EQUIPMENT_ACTUAL__QUANTITY);
		createEAttribute(equipmentActualEClass, EQUIPMENT_ACTUAL__QUANTITY_UNIT_OF_MEASURE);
		createEReference(equipmentActualEClass, EQUIPMENT_ACTUAL__EQUIPMENT_CLASSES);
		createEReference(equipmentActualEClass, EQUIPMENT_ACTUAL__EQUIPMENTS);

		personnelActualEClass = createEClass(PERSONNEL_ACTUAL);
		createEReference(personnelActualEClass, PERSONNEL_ACTUAL__PERSONNEL_ACTUAL_PROPERTIES);
		createEAttribute(personnelActualEClass, PERSONNEL_ACTUAL__DESCRIPTION);
		createEAttribute(personnelActualEClass, PERSONNEL_ACTUAL__PERSONNEL_USE);
		createEAttribute(personnelActualEClass, PERSONNEL_ACTUAL__QUANTITY);
		createEAttribute(personnelActualEClass, PERSONNEL_ACTUAL__QUANTITY_UNIT_OF_MEASURE);
		createEReference(personnelActualEClass, PERSONNEL_ACTUAL__PERSONNEL_CLASSES);
		createEReference(personnelActualEClass, PERSONNEL_ACTUAL__PERSONS);

		segmentDataEClass = createEClass(SEGMENT_DATA);
		createEReference(segmentDataEClass, SEGMENT_DATA__SUB_DATA);
		createEAttribute(segmentDataEClass, SEGMENT_DATA__ID);
		createEAttribute(segmentDataEClass, SEGMENT_DATA__DESCRIPTION);
		createEAttribute(segmentDataEClass, SEGMENT_DATA__VALUE);
		createEAttribute(segmentDataEClass, SEGMENT_DATA__VALUE_UNIT_OF_MEASURE);

		segmentResponseEClass = createEClass(SEGMENT_RESPONSE);
		createEReference(segmentResponseEClass, SEGMENT_RESPONSE__SUB_RESPONSES);
		createEReference(segmentResponseEClass, SEGMENT_RESPONSE__PROCESS_SEGMENT);
		createEReference(segmentResponseEClass, SEGMENT_RESPONSE__OPERATIONS_DEFINITION);
		createEReference(segmentResponseEClass, SEGMENT_RESPONSE__SEGMENT_DATA);
		createEReference(segmentResponseEClass, SEGMENT_RESPONSE__PERSONNEL_ACTUALS);
		createEReference(segmentResponseEClass, SEGMENT_RESPONSE__EQUIPMENT_ACTUALS);
		createEReference(segmentResponseEClass, SEGMENT_RESPONSE__PHYSICAL_ASSET_ACTUALS);
		createEReference(segmentResponseEClass, SEGMENT_RESPONSE__MATERIAL_ACTUALS);
		createEAttribute(segmentResponseEClass, SEGMENT_RESPONSE__ID);
		createEAttribute(segmentResponseEClass, SEGMENT_RESPONSE__DESCRIPTION);
		createEAttribute(segmentResponseEClass, SEGMENT_RESPONSE__OPERATIONS_TYPE);
		createEAttribute(segmentResponseEClass, SEGMENT_RESPONSE__ACTUAL_START_TIME);
		createEAttribute(segmentResponseEClass, SEGMENT_RESPONSE__ACTUAL_END_TIME);
		createEAttribute(segmentResponseEClass, SEGMENT_RESPONSE__SEGMENT_STATE);
		createEReference(segmentResponseEClass, SEGMENT_RESPONSE__HIERARCHY_SCOPE);

		operationsResponseEClass = createEClass(OPERATIONS_RESPONSE);
		createEReference(operationsResponseEClass, OPERATIONS_RESPONSE__SEGMENT_RESPONSES);
		createEAttribute(operationsResponseEClass, OPERATIONS_RESPONSE__ID);
		createEAttribute(operationsResponseEClass, OPERATIONS_RESPONSE__DESCRIPTION);
		createEAttribute(operationsResponseEClass, OPERATIONS_RESPONSE__OPERATIONS_TYPE);
		createEAttribute(operationsResponseEClass, OPERATIONS_RESPONSE__START_TIME);
		createEAttribute(operationsResponseEClass, OPERATIONS_RESPONSE__END_TIME);
		createEAttribute(operationsResponseEClass, OPERATIONS_RESPONSE__RESPONSE_STATE);
		createEReference(operationsResponseEClass, OPERATIONS_RESPONSE__OPERATIONS_REQUEST);
		createEReference(operationsResponseEClass, OPERATIONS_RESPONSE__HIERARCHY_SCOPE);
		createEReference(operationsResponseEClass, OPERATIONS_RESPONSE__OPERATIONS_DEFINITION);

		operationsPerformanceEClass = createEClass(OPERATIONS_PERFORMANCE);
		createEReference(operationsPerformanceEClass, OPERATIONS_PERFORMANCE__OPERATIONS_RESPONSES);
		createEAttribute(operationsPerformanceEClass, OPERATIONS_PERFORMANCE__ID);
		createEAttribute(operationsPerformanceEClass, OPERATIONS_PERFORMANCE__DESCRIPTION);
		createEAttribute(operationsPerformanceEClass, OPERATIONS_PERFORMANCE__OPERATIONS_TYPE);
		createEAttribute(operationsPerformanceEClass, OPERATIONS_PERFORMANCE__START_TIME);
		createEReference(operationsPerformanceEClass, OPERATIONS_PERFORMANCE__OPERATIONS_SCHEDULE);
		createEAttribute(operationsPerformanceEClass, OPERATIONS_PERFORMANCE__END_TIME);
		createEAttribute(operationsPerformanceEClass, OPERATIONS_PERFORMANCE__PERFORMANCE_STATE);
		createEReference(operationsPerformanceEClass, OPERATIONS_PERFORMANCE__HIERARCHY_SCOPE);
		createEAttribute(operationsPerformanceEClass, OPERATIONS_PERFORMANCE__PUBLISHED_DATE);

		operationsCapabilityEClass = createEClass(OPERATIONS_CAPABILITY);
		createEReference(operationsCapabilityEClass, OPERATIONS_CAPABILITY__PROCESS_SEGMENT_CAPABILITIES);
		createEReference(operationsCapabilityEClass, OPERATIONS_CAPABILITY__PERSONNEL_CAPABILITIES);
		createEReference(operationsCapabilityEClass, OPERATIONS_CAPABILITY__EQUIPMENT_CAPABILITIES);
		createEReference(operationsCapabilityEClass, OPERATIONS_CAPABILITY__PHYSICAL_ASSET_CAPABILITIES);
		createEReference(operationsCapabilityEClass, OPERATIONS_CAPABILITY__MATERIAL_CAPABILITIES);
		createEAttribute(operationsCapabilityEClass, OPERATIONS_CAPABILITY__ID);
		createEAttribute(operationsCapabilityEClass, OPERATIONS_CAPABILITY__DESCRIPTION);
		createEAttribute(operationsCapabilityEClass, OPERATIONS_CAPABILITY__CAPACITY_TYPE);
		createEAttribute(operationsCapabilityEClass, OPERATIONS_CAPABILITY__REASON);
		createEAttribute(operationsCapabilityEClass, OPERATIONS_CAPABILITY__CONFIDENCE_FACTOR);
		createEAttribute(operationsCapabilityEClass, OPERATIONS_CAPABILITY__START_TIME);
		createEAttribute(operationsCapabilityEClass, OPERATIONS_CAPABILITY__END_TIME);
		createEAttribute(operationsCapabilityEClass, OPERATIONS_CAPABILITY__PUBLISHED_DATE);
		createEReference(operationsCapabilityEClass, OPERATIONS_CAPABILITY__HIERARCHY_SCOPE);

		materialCapabilityPropertyEClass = createEClass(MATERIAL_CAPABILITY_PROPERTY);
		createEReference(materialCapabilityPropertyEClass, MATERIAL_CAPABILITY_PROPERTY__SUB_PROPERTIES);

		physicalAssetCapabilityPropertyEClass = createEClass(PHYSICAL_ASSET_CAPABILITY_PROPERTY);
		createEReference(physicalAssetCapabilityPropertyEClass, PHYSICAL_ASSET_CAPABILITY_PROPERTY__SUB_PROPERTIES);

		equipmentCapabilityPropertyEClass = createEClass(EQUIPMENT_CAPABILITY_PROPERTY);
		createEReference(equipmentCapabilityPropertyEClass, EQUIPMENT_CAPABILITY_PROPERTY__SUB_PROPERTIES);

		personnelCapabilityPropertyEClass = createEClass(PERSONNEL_CAPABILITY_PROPERTY);
		createEReference(personnelCapabilityPropertyEClass, PERSONNEL_CAPABILITY_PROPERTY__SUB_PROPERTIES);

		materialCapabilityEClass = createEClass(MATERIAL_CAPABILITY);
		createEReference(materialCapabilityEClass, MATERIAL_CAPABILITY__ASSEMBLED_FROM_CAPABILITIES);
		createEReference(materialCapabilityEClass, MATERIAL_CAPABILITY__MATERIAL_CAPABILITY_PROPERTY);
		createEReference(materialCapabilityEClass, MATERIAL_CAPABILITY__MATERIAL_CLASS);
		createEReference(materialCapabilityEClass, MATERIAL_CAPABILITY__MATERIAL_DEFINITION);
		createEReference(materialCapabilityEClass, MATERIAL_CAPABILITY__MATERIAL_LOT);
		createEReference(materialCapabilityEClass, MATERIAL_CAPABILITY__MATERIAL_SUBLOT);
		createEAttribute(materialCapabilityEClass, MATERIAL_CAPABILITY__DESCRIPTION);
		createEAttribute(materialCapabilityEClass, MATERIAL_CAPABILITY__CAPABILITY_TYPE);
		createEAttribute(materialCapabilityEClass, MATERIAL_CAPABILITY__REASON);
		createEAttribute(materialCapabilityEClass, MATERIAL_CAPABILITY__CONFIDENCE_FACTOR);
		createEAttribute(materialCapabilityEClass, MATERIAL_CAPABILITY__MATERIAL_USE);
		createEAttribute(materialCapabilityEClass, MATERIAL_CAPABILITY__START_TIME);
		createEAttribute(materialCapabilityEClass, MATERIAL_CAPABILITY__END_TIME);
		createEAttribute(materialCapabilityEClass, MATERIAL_CAPABILITY__QUANTITY);
		createEAttribute(materialCapabilityEClass, MATERIAL_CAPABILITY__QUANTITY_UNIT_OF_MEASURE);
		createEReference(materialCapabilityEClass, MATERIAL_CAPABILITY__HIERARCHYSCOPE);
		createEAttribute(materialCapabilityEClass, MATERIAL_CAPABILITY__ASSEMBLY_TYPE);
		createEAttribute(materialCapabilityEClass, MATERIAL_CAPABILITY__ASSEMBLY_RELATIONSHIP);

		physicalAssetCapabilityEClass = createEClass(PHYSICAL_ASSET_CAPABILITY);
		createEReference(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CAPABILITY_PROPERTY);
		createEReference(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CLASS);
		createEReference(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET);
		createEAttribute(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__DESCRIPTION);
		createEAttribute(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__CAPABILITY_TYPE);
		createEAttribute(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__REASON);
		createEAttribute(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__CONFIDENCE_FACTOR);
		createEAttribute(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_USE);
		createEReference(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__HIERARCHY_SCOPE);
		createEAttribute(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__START_TIME);
		createEAttribute(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__END_TIME);
		createEAttribute(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__QUANTITY);
		createEAttribute(physicalAssetCapabilityEClass, PHYSICAL_ASSET_CAPABILITY__QUANTITY_UNIT_OF_MEASURE);

		equipmentCapabilityEClass = createEClass(EQUIPMENT_CAPABILITY);
		createEReference(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__EQUIPMENT_CAPABILITY_PROPERTY);
		createEReference(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__EQUIPMENT_CLASS);
		createEReference(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__EQUIPMENT);
		createEAttribute(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__DESCRIPTION);
		createEAttribute(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__CAPABILITY_TYPE);
		createEAttribute(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__REASON);
		createEAttribute(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__CONFIDENCE_FACTOR);
		createEAttribute(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__EQUIPMENT_USE);
		createEAttribute(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__START_TIME);
		createEAttribute(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__END_TIME);
		createEAttribute(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__QUANTITY);
		createEAttribute(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__QUANTITY_UNIT_OF_MEASURE);
		createEReference(equipmentCapabilityEClass, EQUIPMENT_CAPABILITY__HIERARCHY_SCOPE);

		personnelCapabilityEClass = createEClass(PERSONNEL_CAPABILITY);
		createEReference(personnelCapabilityEClass, PERSONNEL_CAPABILITY__PERSONNEL_CAPABILITY_PROPERTY);
		createEReference(personnelCapabilityEClass, PERSONNEL_CAPABILITY__PERSONNEL_CLASS);
		createEReference(personnelCapabilityEClass, PERSONNEL_CAPABILITY__PERSON);
		createEAttribute(personnelCapabilityEClass, PERSONNEL_CAPABILITY__DESCRIPTION);
		createEAttribute(personnelCapabilityEClass, PERSONNEL_CAPABILITY__CAPABILITY_TYPE);
		createEAttribute(personnelCapabilityEClass, PERSONNEL_CAPABILITY__REASON);
		createEAttribute(personnelCapabilityEClass, PERSONNEL_CAPABILITY__CONFIDENCE_FACTOR);
		createEReference(personnelCapabilityEClass, PERSONNEL_CAPABILITY__HIERARCHY_SCOPE);
		createEAttribute(personnelCapabilityEClass, PERSONNEL_CAPABILITY__PERSONNEL_USE);
		createEAttribute(personnelCapabilityEClass, PERSONNEL_CAPABILITY__START_TIME);
		createEAttribute(personnelCapabilityEClass, PERSONNEL_CAPABILITY__END_TIME);
		createEAttribute(personnelCapabilityEClass, PERSONNEL_CAPABILITY__QUANTITY);
		createEAttribute(personnelCapabilityEClass, PERSONNEL_CAPABILITY__QUANTITY_UNIT_OF_MEASURE);

		processSegmentCapabilityEClass = createEClass(PROCESS_SEGMENT_CAPABILITY);
		createEReference(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__SUB_CAPABILITIES);
		createEReference(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__PERSONNEL_CAPABILITIES);
		createEReference(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__EQUIPMENT_CAPABILITIES);
		createEReference(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__PHYSICAL_ASSET_CAPABILITIES);
		createEReference(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__MATERIAL_CAPABILITIES);
		createEReference(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__PROCESS_SEGMENT);
		createEAttribute(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__ID);
		createEAttribute(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__DESCRIPTION);
		createEAttribute(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__CAPACITY_TYPE);
		createEAttribute(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__REASON);
		createEAttribute(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__START_TIME);
		createEAttribute(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__END_TIME);
		createEReference(processSegmentCapabilityEClass, PROCESS_SEGMENT_CAPABILITY__HIERARCHY_SCOPE);

		referencialPropertyEClass = createEClass(REFERENCIAL_PROPERTY);
		createEAttribute(referencialPropertyEClass, REFERENCIAL_PROPERTY__DESCRIPTION);
		createEAttribute(referencialPropertyEClass, REFERENCIAL_PROPERTY__VALUE);
		createEAttribute(referencialPropertyEClass, REFERENCIAL_PROPERTY__VALUE_UNIT_OF_MEASURE);
		createEAttribute(referencialPropertyEClass, REFERENCIAL_PROPERTY__QUANTITY);
		createEAttribute(referencialPropertyEClass, REFERENCIAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE);

		referentialPersonnelPropertyEClass = createEClass(REFERENTIAL_PERSONNEL_PROPERTY);
		createEReference(referentialPersonnelPropertyEClass, REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY);
		createEReference(referentialPersonnelPropertyEClass, REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY);

		referentialEquipmentPropertyEClass = createEClass(REFERENTIAL_EQUIPMENT_PROPERTY);
		createEReference(referentialEquipmentPropertyEClass, REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY);
		createEReference(referentialEquipmentPropertyEClass, REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY);

		referentialPhysicalAssetPropertyEClass = createEClass(REFERENTIAL_PHYSICAL_ASSET_PROPERTY);
		createEReference(referentialPhysicalAssetPropertyEClass,
				REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY);
		createEReference(referentialPhysicalAssetPropertyEClass,
				REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY);

		referentialMaterialTypePropertyEClass = createEClass(REFERENTIAL_MATERIAL_TYPE_PROPERTY);
		createEReference(referentialMaterialTypePropertyEClass,
				REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_CLASS_PROPERTY);
		createEReference(referentialMaterialTypePropertyEClass,
				REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_DEFINITION_PROPERTY);

		referentialMaterialPropertyEClass = createEClass(REFERENTIAL_MATERIAL_PROPERTY);
		createEReference(referentialMaterialPropertyEClass, REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_LOT_PROPERTY);

		// Create enums
		assemblyTypeEEnum = createEEnum(ASSEMBLY_TYPE);
		assemblyRelationshipEEnum = createEEnum(ASSEMBLY_RELATIONSHIP);
		operationsTypeEEnum = createEEnum(OPERATIONS_TYPE);
		materialUseEEnum = createEEnum(MATERIAL_USE);
		useTypeEEnum = createEEnum(USE_TYPE);
		scheduleStateEEnum = createEEnum(SCHEDULE_STATE);
		performanceStateEEnum = createEEnum(PERFORMANCE_STATE);
		capabilityTypeEEnum = createEEnum(CAPABILITY_TYPE);
		capacityTypeEEnum = createEEnum(CAPACITY_TYPE);

		// Create data types
		zonedDateTimeEDataType = createEDataType(ZONED_DATE_TIME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage) EPackage.Registry.INSTANCE
				.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		personnelSegmentSpecificationPropertyEClass.getESuperTypes().add(this.getReferentialPersonnelProperty());
		equipmentSegmentSpecificationPropertyEClass.getESuperTypes().add(this.getReferentialEquipmentProperty());
		physicalAssetSegmentSpecificationPropertyEClass.getESuperTypes()
				.add(this.getReferentialPhysicalAssetProperty());
		materialSegmentSpecificationPropertyEClass.getESuperTypes().add(this.getReferentialMaterialTypeProperty());
		materialSpecificationPropertyEClass.getESuperTypes().add(this.getReferentialMaterialTypeProperty());
		physicalAssetSpecificationPropertyEClass.getESuperTypes().add(this.getReferentialPhysicalAssetProperty());
		equipmentSpecificationPropertyEClass.getESuperTypes().add(this.getReferentialEquipmentProperty());
		personnelSpecificationPropertyEClass.getESuperTypes().add(this.getReferentialPersonnelProperty());
		materialRequirementPropertyEClass.getESuperTypes().add(this.getReferentialMaterialProperty());
		physicalAssetRequirementPropertyEClass.getESuperTypes().add(this.getReferentialPhysicalAssetProperty());
		equipmentRequirementPropertyEClass.getESuperTypes().add(this.getReferentialEquipmentProperty());
		personnelRequirementPropertyEClass.getESuperTypes().add(this.getReferentialPersonnelProperty());
		requestedSegmentResponseEClass.getESuperTypes().add(this.getSegmentResponse());
		materialActualPropertyEClass.getESuperTypes().add(this.getReferentialMaterialProperty());
		physicalAssetActualPropertyEClass.getESuperTypes().add(this.getReferentialPhysicalAssetProperty());
		equipmentActualPropertyEClass.getESuperTypes().add(this.getReferentialEquipmentProperty());
		personnelActualPropertyEClass.getESuperTypes().add(this.getReferentialPersonnelProperty());
		materialCapabilityPropertyEClass.getESuperTypes().add(this.getReferentialMaterialProperty());
		physicalAssetCapabilityPropertyEClass.getESuperTypes().add(this.getReferentialPhysicalAssetProperty());
		equipmentCapabilityPropertyEClass.getESuperTypes().add(this.getReferentialEquipmentProperty());
		personnelCapabilityPropertyEClass.getESuperTypes().add(this.getReferentialPersonnelProperty());
		referentialPersonnelPropertyEClass.getESuperTypes().add(this.getReferencialProperty());
		referentialEquipmentPropertyEClass.getESuperTypes().add(this.getReferencialProperty());
		referentialPhysicalAssetPropertyEClass.getESuperTypes().add(this.getReferencialProperty());
		referentialMaterialTypePropertyEClass.getESuperTypes().add(this.getReferencialProperty());
		referentialMaterialPropertyEClass.getESuperTypes().add(this.getReferentialMaterialTypeProperty());

		// Initialize classes, features, and operations; add parameters
		initEClass(personEClass, Person.class, "Person", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerson_PersonnelClasses(), this.getPersonnelClass(), null, "personnelClasses", null, 0, -1,
				Person.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerson_Properties(), this.getPersonProperty(), null, "properties", null, 0, -1, Person.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerson_TestSpecifications(), this.getQualificationTestSpecification(), null,
				"testSpecifications", null, 0, -1, Person.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerson_Id(), ecorePackage.getEString(), "id", null, 1, 1, Person.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerson_Description(), ecorePackage.getEString(), "description", null, 0, 1, Person.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerson_Name(), ecorePackage.getEString(), "name", null, 0, 1, Person.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelClassEClass, PersonnelClass.class, "PersonnelClass", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelClass_Properties(), this.getPersonnelClassProperty(), null, "properties", null, 0,
				-1, PersonnelClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelClass_TestSpecifications(), this.getQualificationTestSpecification(), null,
				"testSpecifications", null, 0, -1, PersonnelClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelClass_Id(), ecorePackage.getEString(), "id", null, 1, 1, PersonnelClass.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelClass_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PersonnelClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(qualificationTestSpecificationEClass, QualificationTestSpecification.class,
				"QualificationTestSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getQualificationTestSpecification_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				QualificationTestSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQualificationTestSpecification_Description(), ecorePackage.getEString(), "description", null,
				0, 1, QualificationTestSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQualificationTestSpecification_Version(), ecorePackage.getEString(), "version", null, 0, 1,
				QualificationTestSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelClassPropertyEClass, PersonnelClassProperty.class, "PersonnelClassProperty", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelClassProperty_TestSpecifications(), this.getQualificationTestSpecification(), null,
				"testSpecifications", null, 0, -1, PersonnelClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelClassProperty_SubProperties(), this.getPersonnelClassProperty(), null,
				"subProperties", null, 0, -1, PersonnelClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelClassProperty_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				PersonnelClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelClassProperty_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PersonnelClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				!IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelClassProperty_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				PersonnelClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelClassProperty_ValueUnitOfMeasure(), ecorePackage.getEString(), "valueUnitOfMeasure",
				null, 0, 1, PersonnelClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personPropertyEClass, PersonProperty.class, "PersonProperty", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonProperty_SubProperties(), this.getPersonProperty(), null, "subProperties", null, 0, -1,
				PersonProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonProperty_PersonnelClassProperty(), this.getPersonnelClassProperty(), null,
				"personnelClassProperty", null, 0, 1, PersonProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonProperty_Id(), ecorePackage.getEString(), "id", null, 1, 1, PersonProperty.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonProperty_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PersonProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonProperty_Value(), ecorePackage.getEString(), "value", null, 0, 1, PersonProperty.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonProperty_ValueUnitOfMeasure(), ecorePackage.getEString(), "valueUnitOfMeasure", null, 0,
				1, PersonProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(qualificationTestResultEClass, QualificationTestResult.class, "QualificationTestResult",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getQualificationTestResult_TestSpecification(), this.getQualificationTestSpecification(), null,
				"testSpecification", null, 1, 1, QualificationTestResult.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getQualificationTestResult_Property(), this.getPersonProperty(), null, "property", null, 1, 1,
				QualificationTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQualificationTestResult_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				QualificationTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQualificationTestResult_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				QualificationTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQualificationTestResult_Date(), this.getZonedDateTime(), "date", null, 0, 1,
				QualificationTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQualificationTestResult_Result(), ecorePackage.getEString(), "result", null, 0, 1,
				QualificationTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQualificationTestResult_ResultUnitOfMeasure(), ecorePackage.getEString(),
				"resultUnitOfMeasure", null, 0, 1, QualificationTestResult.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQualificationTestResult_Expiration(), theXMLTypePackage.getDateTime(), "expiration", null, 0,
				1, QualificationTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelModelEClass, PersonnelModel.class, "PersonnelModel", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelModel_PersonnelClasses(), this.getPersonnelClass(), null, "personnelClasses", null,
				0, -1, PersonnelModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelModel_Persons(), this.getPerson(), null, "persons", null, 0, -1,
				PersonnelModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelModel_TestSpecifications(), this.getQualificationTestSpecification(), null,
				"testSpecifications", null, 0, -1, PersonnelModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelModel_TestResults(), this.getQualificationTestResult(), null, "testResults", null, 0,
				-1, PersonnelModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelModel_OperationsCapabilityModel(), this.getOperationsCapabilityModel(), null,
				"operationsCapabilityModel", null, 0, 1, PersonnelModel.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentModelEClass, EquipmentModel.class, "EquipmentModel", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentModel_EquipmentClasses(), this.getEquipmentClass(), null, "equipmentClasses", null,
				0, -1, EquipmentModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentModel_Equipments(), this.getEquipment(), null, "equipments", null, 0, -1,
				EquipmentModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentModel_TestSpecifications(), this.getEquipmentCapabilityTestSpecification(), null,
				"testSpecifications", null, 0, -1, EquipmentModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentModel_TestResults(), this.getEquipmentCapabilityTestResult(), null, "testResults",
				null, 0, -1, EquipmentModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentClassEClass, EquipmentClass.class, "EquipmentClass", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentClass_Properties(), this.getEquipmentClassProperty(), null, "properties", null, 0,
				-1, EquipmentClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentClass_TestSpecifications(), this.getEquipmentCapabilityTestSpecification(), null,
				"testSpecifications", null, 0, -1, EquipmentClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentClass_Id(), ecorePackage.getEString(), "id", null, 1, 1, EquipmentClass.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentClass_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				EquipmentClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentClass_EquipmentLevel(), ecorePackage.getEString(), "equipmentLevel", null, 0, 1,
				EquipmentClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(equipmentCapabilityTestResultEClass, EquipmentCapabilityTestResult.class,
				"EquipmentCapabilityTestResult", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentCapabilityTestResult_TestSpecification(),
				this.getEquipmentCapabilityTestSpecification(), null, "testSpecification", null, 1, 1,
				EquipmentCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentCapabilityTestResult_Property(), this.getEquipmentProperty(), null, "property", null,
				1, 1, EquipmentCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapabilityTestResult_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				EquipmentCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapabilityTestResult_Description(), ecorePackage.getEString(), "description", null,
				0, 1, EquipmentCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapabilityTestResult_Date(), this.getZonedDateTime(), "date", null, 0, 1,
				EquipmentCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapabilityTestResult_Result(), ecorePackage.getEString(), "result", null, 0, 1,
				EquipmentCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapabilityTestResult_ResultUnitOfMeasure(), ecorePackage.getEString(),
				"resultUnitOfMeasure", null, 0, 1, EquipmentCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapabilityTestResult_Expiration(), theXMLTypePackage.getDateTime(), "expiration",
				null, 0, 1, EquipmentCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentCapabilityTestSpecificationEClass, EquipmentCapabilityTestSpecification.class,
				"EquipmentCapabilityTestSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEquipmentCapabilityTestSpecification_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				EquipmentCapabilityTestSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapabilityTestSpecification_Description(), ecorePackage.getEString(), "description",
				null, 0, 1, EquipmentCapabilityTestSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapabilityTestSpecification_Version(), ecorePackage.getEString(), "version", null, 0,
				1, EquipmentCapabilityTestSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentClassPropertyEClass, EquipmentClassProperty.class, "EquipmentClassProperty", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentClassProperty_SubProperties(), this.getEquipmentClassProperty(), null,
				"subProperties", null, 0, -1, EquipmentClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentClassProperty_TestSpecifications(), this.getEquipmentCapabilityTestSpecification(),
				null, "testSpecifications", null, 0, -1, EquipmentClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentClassProperty_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				EquipmentClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentClassProperty_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				EquipmentClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentClassProperty_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				EquipmentClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentClassProperty_ValueUnitOfMeasure(), ecorePackage.getEString(), "valueUnitOfMeasure",
				null, 0, 1, EquipmentClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentPropertyEClass, EquipmentProperty.class, "EquipmentProperty", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentProperty_SubProperties(), this.getEquipmentProperty(), null, "subProperties", null,
				0, -1, EquipmentProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentProperty_EquipmentClassProperty(), this.getEquipmentClassProperty(), null,
				"equipmentClassProperty", null, 0, 1, EquipmentProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentProperty_Id(), ecorePackage.getEString(), "id", null, 1, 1, EquipmentProperty.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentProperty_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				EquipmentProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentProperty_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				EquipmentProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentProperty_ValueUnitOfMeasure(), ecorePackage.getEString(), "valueUnitOfMeasure", null,
				0, 1, EquipmentProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentEClass, Equipment.class, "Equipment", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipment_Children(), this.getEquipment(), null, "children", null, 0, -1, Equipment.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipment_Properties(), this.getEquipmentProperty(), null, "properties", null, 0, -1,
				Equipment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipment_TestSpecifications(), this.getEquipmentCapabilityTestSpecification(), null,
				"testSpecifications", null, 0, -1, Equipment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipment_EquipmentClass(), this.getEquipmentClass(), null, "equipmentClass", null, 0, 1,
				Equipment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipment_Id(), ecorePackage.getEString(), "id", null, 1, 1, Equipment.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipment_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				Equipment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipment_EquipmentLevel(), ecorePackage.getEString(), "equipmentLevel", null, 0, 1,
				Equipment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetModelEClass, PhysicalAssetModel.class, "PhysicalAssetModel", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetModel_PhysicalAssets(), this.getPhysicalAsset(), null, "physicalAssets", null, 0,
				-1, PhysicalAssetModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetModel_PhysicalAssetClasses(), this.getPhysicalAssetClass(), null,
				"physicalAssetClasses", null, 0, -1, PhysicalAssetModel.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetModel_TestSpecifications(), this.getPhysicalAssetCapabilityTestSpecification(),
				null, "testSpecifications", null, 0, -1, PhysicalAssetModel.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetModel_EquipmentAssetMappings(), this.getEquipmentAssetMapping(), null,
				"equipmentAssetMappings", null, 0, -1, PhysicalAssetModel.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetModel_TestResults(), this.getPhysicalAssetCapabilityTestResult(), null,
				"testResults", null, 0, -1, PhysicalAssetModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetClassEClass, PhysicalAssetClass.class, "PhysicalAssetClass", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetClass_Properties(), this.getPhysicalAssetClassProperty(), null, "properties",
				null, 0, -1, PhysicalAssetClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetClass_TestSpecifications(), this.getPhysicalAssetCapabilityTestSpecification(),
				null, "testSpecifications", null, 0, -1, PhysicalAssetClass.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetClass_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				PhysicalAssetClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetClass_Manufacturer(), ecorePackage.getEString(), "manufacturer", null, 0, 1,
				PhysicalAssetClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetClass_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PhysicalAssetClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetCapabilityTestResultEClass, PhysicalAssetCapabilityTestResult.class,
				"PhysicalAssetCapabilityTestResult", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetCapabilityTestResult_TestSpecification(),
				this.getPhysicalAssetCapabilityTestSpecification(), null, "testSpecification", null, 1, 1,
				PhysicalAssetCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetCapabilityTestResult_Property(), this.getPhysicalAssetProperty(), null,
				"property", null, 1, 1, PhysicalAssetCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapabilityTestResult_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				PhysicalAssetCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapabilityTestResult_Description(), ecorePackage.getEString(), "description",
				null, 0, 1, PhysicalAssetCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapabilityTestResult_Date(), this.getZonedDateTime(), "date", null, 0, 1,
				PhysicalAssetCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapabilityTestResult_Result(), ecorePackage.getEString(), "result", null, 0, 1,
				PhysicalAssetCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapabilityTestResult_ResultUnitOfMeasure(), ecorePackage.getEString(),
				"resultUnitOfMeasure", null, 0, 1, PhysicalAssetCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapabilityTestResult_Expiration(), theXMLTypePackage.getDateTime(), "expiration",
				null, 0, 1, PhysicalAssetCapabilityTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetPropertyEClass, PhysicalAssetProperty.class, "PhysicalAssetProperty", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetProperty_SubProperties(), this.getPhysicalAssetProperty(), null, "subProperties",
				null, 0, -1, PhysicalAssetProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetProperty_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				PhysicalAssetProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetProperty_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PhysicalAssetProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetProperty_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				PhysicalAssetProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetProperty_ValueUnitOfMeasure(), ecorePackage.getEString(), "valueUnitOfMeasure",
				null, 0, 1, PhysicalAssetProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetProperty_PhysicalAssetClassProperty(), this.getPhysicalAssetClassProperty(),
				null, "physicalAssetClassProperty", null, 0, 1, PhysicalAssetProperty.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(physicalAssetEClass, PhysicalAsset.class, "PhysicalAsset", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAsset_Children(), this.getPhysicalAsset(), null, "children", null, 0, -1,
				PhysicalAsset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAsset_Properties(), this.getPhysicalAssetProperty(), null, "properties", null, 0, -1,
				PhysicalAsset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAsset_PhysicalAssetClass(), this.getPhysicalAssetClass(), null, "physicalAssetClass",
				null, 1, 1, PhysicalAsset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAsset_TestSpecifications(), this.getPhysicalAssetCapabilityTestSpecification(), null,
				"testSpecifications", null, 0, -1, PhysicalAsset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAsset_Id(), ecorePackage.getEString(), "id", null, 1, 1, PhysicalAsset.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAsset_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PhysicalAsset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAsset_PhysicalLocation(), ecorePackage.getEString(), "physicalLocation", null, 0, 1,
				PhysicalAsset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAsset_FixedAssetId(), ecorePackage.getEString(), "fixedAssetId", null, 0, 1,
				PhysicalAsset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAsset_VendorId(), ecorePackage.getEString(), "vendorId", null, 0, 1,
				PhysicalAsset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetCapabilityTestSpecificationEClass, PhysicalAssetCapabilityTestSpecification.class,
				"PhysicalAssetCapabilityTestSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPhysicalAssetCapabilityTestSpecification_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				PhysicalAssetCapabilityTestSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapabilityTestSpecification_Description(), ecorePackage.getEString(),
				"description", null, 0, 1, PhysicalAssetCapabilityTestSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapabilityTestSpecification_Version(), ecorePackage.getEString(), "version",
				null, 0, 1, PhysicalAssetCapabilityTestSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetClassPropertyEClass, PhysicalAssetClassProperty.class, "PhysicalAssetClassProperty",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetClassProperty_SubProperties(), this.getPhysicalAssetClassProperty(), null,
				"subProperties", null, 0, -1, PhysicalAssetClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetClassProperty_TestSpecifications(),
				this.getPhysicalAssetCapabilityTestSpecification(), null, "testSpecifications", null, 0, -1,
				PhysicalAssetClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetClassProperty_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				PhysicalAssetClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetClassProperty_Description(), ecorePackage.getEString(), "description", null, 0,
				1, PhysicalAssetClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetClassProperty_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				PhysicalAssetClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetClassProperty_ValueUnitOfMeasure(), ecorePackage.getEString(),
				"valueUnitOfMeasure", null, 0, 1, PhysicalAssetClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentAssetMappingEClass, EquipmentAssetMapping.class, "EquipmentAssetMapping", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentAssetMapping_PhysicalAsset(), this.getPhysicalAsset(), null, "physicalAsset", null,
				1, 1, EquipmentAssetMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentAssetMapping_Equipment(), this.getEquipment(), null, "equipment", null, 1, 1,
				EquipmentAssetMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentAssetMapping_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				EquipmentAssetMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentAssetMapping_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				EquipmentAssetMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentAssetMapping_StartTime(), this.getZonedDateTime(), "startTime", null, 0, 1,
				EquipmentAssetMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentAssetMapping_EndTime(), this.getZonedDateTime(), "endTime", null, 0, 1,
				EquipmentAssetMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialModelEClass, MaterialModel.class, "MaterialModel", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialModel_MaterialClasses(), this.getMaterialClass(), null, "materialClasses", null, 0,
				-1, MaterialModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialModel_MaterialDefinitions(), this.getMaterialDefinition(), null,
				"materialDefinitions", null, 0, -1, MaterialModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialModel_MaterialLots(), this.getMaterialLot(), null, "materialLots", null, 0, -1,
				MaterialModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialModel_MaterialSublots(), this.getMaterialSublot(), null, "materialSublots", null, 0,
				-1, MaterialModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialModel_TestSpecifications(), this.getMaterialTestSpecification(), null,
				"testSpecifications", null, 0, -1, MaterialModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialModel_TestResults(), this.getMaterialTestResult(), null, "testResults", null, 0, -1,
				MaterialModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialClassEClass, MaterialClass.class, "MaterialClass", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialClass_Properties(), this.getMaterialClassProperty(), null, "properties", null, 0, -1,
				MaterialClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialClass_AssembledFromClasses(), this.getMaterialClass(), null, "assembledFromClasses",
				null, 0, -1, MaterialClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialClass_TestSpecifications(), this.getMaterialTestSpecification(), null,
				"testSpecifications", null, 0, -1, MaterialClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialClass_Id(), ecorePackage.getEString(), "id", null, 1, 1, MaterialClass.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialClass_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MaterialClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialClass_AssemblyType(), this.getAssemblyType(), "assemblyType", null, 0, 1,
				MaterialClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialClass_AssemblyRelationship(), this.getAssemblyRelationship(), "assemblyRelationship",
				null, 0, 1, MaterialClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialTestResultEClass, MaterialTestResult.class, "MaterialTestResult", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialTestResult_TestSpecification(), this.getMaterialTestSpecification(), null,
				"testSpecification", null, 1, 1, MaterialTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialTestResult_Property(), this.getMaterialLotProperty(), null, "property", null, 1, 1,
				MaterialTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialTestResult_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				MaterialTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialTestResult_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MaterialTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialTestResult_Date(), this.getZonedDateTime(), "date", null, 0, 1,
				MaterialTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialTestResult_Result(), ecorePackage.getEString(), "result", null, 0, 1,
				MaterialTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialTestResult_ResultUnitOfMeasure(), ecorePackage.getEString(), "resultUnitOfMeasure",
				null, 0, 1, MaterialTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialTestResult_Expiration(), theXMLTypePackage.getDateTime(), "expiration", null, 0, 1,
				MaterialTestResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(materialTestSpecificationEClass, MaterialTestSpecification.class, "MaterialTestSpecification",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMaterialTestSpecification_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				MaterialTestSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialTestSpecification_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MaterialTestSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialTestSpecification_Version(), ecorePackage.getEString(), "version", null, 0, 1,
				MaterialTestSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialSublotEClass, MaterialSublot.class, "MaterialSublot", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialSublot_Properties(), this.getMaterialLotProperty(), null, "properties", null, 0, -1,
				MaterialSublot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialSublot_AssembledFromLots(), this.getMaterialLot(), null, "assembledFromLots", null, 0,
				-1, MaterialSublot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialSublot_AssembledFromSublots(), this.getMaterialSublot(), null, "assembledFromSublots",
				null, 0, -1, MaterialSublot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialSublot_Sublots(), this.getMaterialSublot(), null, "sublots", null, 0, 1,
				MaterialSublot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSublot_Id(), ecorePackage.getEString(), "id", null, 1, 1, MaterialSublot.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSublot_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MaterialSublot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSublot_AssemblyType(), this.getAssemblyType(), "assemblyType", null, 0, 1,
				MaterialSublot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSublot_AssemblyRelationship(), this.getAssemblyRelationship(), "assemblyRelationship",
				null, 0, 1, MaterialSublot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSublot_Status(), ecorePackage.getEString(), "status", null, 0, 1,
				MaterialSublot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSublot_StorageLocation(), ecorePackage.getEString(), "storageLocation", null, 0, 1,
				MaterialSublot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSublot_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				MaterialSublot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSublot_QuantityUnitOfMeasure(), ecorePackage.getEString(), "quantityUnitOfMeasure",
				null, 0, 1, MaterialSublot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialLotEClass, MaterialLot.class, "MaterialLot", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialLot_Properties(), this.getMaterialLotProperty(), null, "properties", null, 0, -1,
				MaterialLot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialLot_AssembledFromLots(), this.getMaterialLot(), null, "assembledFromLots", null, 0,
				-1, MaterialLot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialLot_AssembledFromSublots(), this.getMaterialSublot(), null, "assembledFromSublots",
				null, 0, -1, MaterialLot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialLot_Sublots(), this.getMaterialSublot(), null, "sublots", null, 0, -1,
				MaterialLot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialLot_MaterialDefinition(), this.getMaterialDefinition(), null, "materialDefinition",
				null, 1, 1, MaterialLot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialLot_TestSpecifications(), this.getMaterialTestSpecification(), null,
				"testSpecifications", null, 0, -1, MaterialLot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialLot_Id(), ecorePackage.getEString(), "id", null, 1, 1, MaterialLot.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialLot_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MaterialLot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialLot_AssemblyType(), this.getAssemblyType(), "assemblyType", null, 0, 1,
				MaterialLot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialLot_AssemblyRelationship(), this.getAssemblyRelationship(), "assemblyRelationship",
				null, 0, 1, MaterialLot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialLot_Status(), ecorePackage.getEString(), "status", null, 0, 1, MaterialLot.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialLot_StorageLocation(), ecorePackage.getEString(), "storageLocation", null, 0, 1,
				MaterialLot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialLot_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				MaterialLot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialLot_QuantityUnitOfMeasure(), ecorePackage.getEString(), "quantityUnitOfMeasure", null,
				0, 1, MaterialLot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(materialLotPropertyEClass, MaterialLotProperty.class, "MaterialLotProperty", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialLotProperty_SubProperties(), this.getMaterialLotProperty(), null, "subProperties",
				null, 0, -1, MaterialLotProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialLotProperty_MaterialDefinitionProperty(), this.getMaterialDefinitionProperty(), null,
				"materialDefinitionProperty", null, 0, 1, MaterialLotProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialLotProperty_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				MaterialLotProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialLotProperty_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MaterialLotProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialLotProperty_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				MaterialLotProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialLotProperty_ValueUnitOfMeasure(), ecorePackage.getEString(), "valueUnitOfMeasure",
				null, 0, 1, MaterialLotProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialDefinitionEClass, MaterialDefinition.class, "MaterialDefinition", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialDefinition_Properties(), this.getMaterialDefinitionProperty(), null, "properties",
				null, 0, -1, MaterialDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialDefinition_AssembledFromDefinitions(), this.getMaterialDefinition(), null,
				"assembledFromDefinitions", null, 0, -1, MaterialDefinition.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialDefinition_MaterialClasses(), this.getMaterialClass(), null, "materialClasses", null,
				0, -1, MaterialDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialDefinition_TestSpecifications(), this.getMaterialTestSpecification(), null,
				"testSpecifications", null, 0, -1, MaterialDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialDefinition_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				MaterialDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialDefinition_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MaterialDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialDefinition_AssemblyType(), this.getAssemblyType(), "assemblyType", null, 0, 1,
				MaterialDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialDefinition_AssemblyRelationship(), this.getAssemblyRelationship(),
				"assemblyRelationship", null, 0, 1, MaterialDefinition.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialDefinitionPropertyEClass, MaterialDefinitionProperty.class, "MaterialDefinitionProperty",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialDefinitionProperty_SubProperties(), this.getMaterialDefinitionProperty(), null,
				"subProperties", null, 0, -1, MaterialDefinitionProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialDefinitionProperty_MaterialClassProperty(), this.getMaterialClassProperty(), null,
				"materialClassProperty", null, 0, 1, MaterialDefinitionProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialDefinitionProperty_TestSpecifications(), this.getMaterialTestSpecification(), null,
				"testSpecifications", null, 0, -1, MaterialDefinitionProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialDefinitionProperty_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				MaterialDefinitionProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialDefinitionProperty_Description(), ecorePackage.getEString(), "description", null, 0,
				1, MaterialDefinitionProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialDefinitionProperty_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				MaterialDefinitionProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialDefinitionProperty_ValueUnitOfMeasure(), ecorePackage.getEString(),
				"valueUnitOfMeasure", null, 0, 1, MaterialDefinitionProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialClassPropertyEClass, MaterialClassProperty.class, "MaterialClassProperty", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialClassProperty_SubProperties(), this.getMaterialClassProperty(), null, "subProperties",
				null, 0, -1, MaterialClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialClassProperty_TestSpecifications(), this.getMaterialTestSpecification(), null,
				"testSpecifications", null, 0, -1, MaterialClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialClassProperty_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				MaterialClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialClassProperty_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MaterialClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialClassProperty_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				MaterialClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialClassProperty_ValueUnitOfMeasure(), ecorePackage.getEString(), "valueUnitOfMeasure",
				null, 0, 1, MaterialClassProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(processSegmentModelEClass, ProcessSegmentModel.class, "ProcessSegmentModel", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcessSegmentModel_ProcessSegments(), this.getProcessSegment(), null, "processSegments",
				null, 0, -1, ProcessSegmentModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegmentModel_ProcessSegmentDependencies(), this.getProcessSegmentDependency(), null,
				"processSegmentDependencies", null, 0, -1, ProcessSegmentModel.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(processSegmentEClass, ProcessSegment.class, "ProcessSegment", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcessSegment_Children(), this.getProcessSegment(), null, "children", null, 0, -1,
				ProcessSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegment_ProcessSegmentParameters(), this.getProcessSegmentParameter(), null,
				"processSegmentParameters", null, 0, -1, ProcessSegment.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegment_PersonnelSegmentSpecifications(), this.getPersonnelSegmentSpecification(),
				null, "personnelSegmentSpecifications", null, 0, -1, ProcessSegment.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegment_EquipmentSegmentSpecifications(), this.getEquipmentSegmentSpecification(),
				null, "equipmentSegmentSpecifications", null, 0, -1, ProcessSegment.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegment_PhysicalAssetSegmentSpecifications(),
				this.getPhysicalAssetSegmentSpecification(), null, "physicalAssetSegmentSpecifications", null, 0, -1,
				ProcessSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegment_MaterialSegmentSpecifications(), this.getMaterialSegmentSpecification(), null,
				"materialSegmentSpecifications", null, 0, -1, ProcessSegment.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegment_Id(), ecorePackage.getEString(), "id", null, 1, 1, ProcessSegment.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegment_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				ProcessSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegment_OperationsType(), this.getOperationsType(), "operationsType", null, 0, 1,
				ProcessSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegment_Duration(), theXMLTypePackage.getDecimal(), "duration", null, 0, 1,
				ProcessSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegment_DurationUnitOfMeasure(), ecorePackage.getEString(), "durationUnitOfMeasure",
				null, 0, 1, ProcessSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegment_HierarchyScope(), this.getHierarchyScope(), null, "hierarchyScope", null, 0, 1,
				ProcessSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelSegmentSpecificationPropertyEClass, PersonnelSegmentSpecificationProperty.class,
				"PersonnelSegmentSpecificationProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelSegmentSpecificationProperty_SubProperties(),
				this.getPersonnelSegmentSpecificationProperty(), null, "subProperties", null, 0, -1,
				PersonnelSegmentSpecificationProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentSegmentSpecificationPropertyEClass, EquipmentSegmentSpecificationProperty.class,
				"EquipmentSegmentSpecificationProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentSegmentSpecificationProperty_SubProperties(),
				this.getEquipmentSegmentSpecificationProperty(), null, "subProperties", null, 0, -1,
				EquipmentSegmentSpecificationProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetSegmentSpecificationPropertyEClass, PhysicalAssetSegmentSpecificationProperty.class,
				"PhysicalAssetSegmentSpecificationProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetSegmentSpecificationProperty_SubProperties(),
				this.getPhysicalAssetSegmentSpecificationProperty(), null, "subProperties", null, 0, -1,
				PhysicalAssetSegmentSpecificationProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialSegmentSpecificationPropertyEClass, MaterialSegmentSpecificationProperty.class,
				"MaterialSegmentSpecificationProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialSegmentSpecificationProperty_SubProperties(),
				this.getMaterialSegmentSpecificationProperty(), null, "subProperties", null, 0, -1,
				MaterialSegmentSpecificationProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialSegmentSpecificationEClass, MaterialSegmentSpecification.class,
				"MaterialSegmentSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialSegmentSpecification_AssembledFromSpecifications(),
				this.getMaterialSegmentSpecification(), null, "assembledFromSpecifications", null, 0, -1,
				MaterialSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialSegmentSpecification_Properties(), this.getMaterialSegmentSpecificationProperty(),
				null, "properties", null, 0, -1, MaterialSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialSegmentSpecification_MaterialClasses(), this.getMaterialClass(), null,
				"materialClasses", null, 0, -1, MaterialSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialSegmentSpecification_MaterialDefinitions(), this.getMaterialDefinition(), null,
				"materialDefinitions", null, 0, -1, MaterialSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSegmentSpecification_Description(), ecorePackage.getEString(), "description", null, 0,
				1, MaterialSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSegmentSpecification_AssemblyType(), this.getAssemblyType(), "assemblyType", null, 0,
				1, MaterialSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSegmentSpecification_AssemblyRelationship(), this.getAssemblyRelationship(),
				"assemblyRelationship", null, 0, 1, MaterialSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSegmentSpecification_MaterialUse(), this.getMaterialUse(), "materialUse", null, 0, 1,
				MaterialSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSegmentSpecification_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0,
				1, MaterialSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSegmentSpecification_QuantityUnitOfMeasurement(), ecorePackage.getEString(),
				"quantityUnitOfMeasurement", null, 0, 1, MaterialSegmentSpecification.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetSegmentSpecificationEClass, PhysicalAssetSegmentSpecification.class,
				"PhysicalAssetSegmentSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetSegmentSpecification_Properties(),
				this.getPhysicalAssetSegmentSpecificationProperty(), null, "properties", null, 0, -1,
				PhysicalAssetSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetSegmentSpecification_PhysicalAssetClasses(), this.getPhysicalAssetClass(), null,
				"physicalAssetClasses", null, 0, -1, PhysicalAssetSegmentSpecification.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getPhysicalAssetSegmentSpecification_PhysicalAssets(), this.getPhysicalAsset(), null,
				"physicalAssets", null, 0, -1, PhysicalAssetSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetSegmentSpecification_Description(), ecorePackage.getEString(), "description",
				null, 0, 1, PhysicalAssetSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetSegmentSpecification_PhysicalAssetUse(), ecorePackage.getEString(),
				"physicalAssetUse", null, 0, 1, PhysicalAssetSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetSegmentSpecification_Quantity(), theXMLTypePackage.getDecimal(), "quantity",
				null, 0, 1, PhysicalAssetSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetSegmentSpecification_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, PhysicalAssetSegmentSpecification.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentSegmentSpecificationEClass, EquipmentSegmentSpecification.class,
				"EquipmentSegmentSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentSegmentSpecification_Properties(), this.getEquipmentSegmentSpecificationProperty(),
				null, "properties", null, 0, -1, EquipmentSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentSegmentSpecification_EquipmentClasses(), this.getEquipmentClass(), null,
				"equipmentClasses", null, 0, -1, EquipmentSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentSegmentSpecification_Equipments(), this.getEquipment(), null, "equipments", null, 0,
				-1, EquipmentSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentSegmentSpecification_Description(), ecorePackage.getEString(), "description", null,
				0, 1, EquipmentSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentSegmentSpecification_EquipmentUse(), ecorePackage.getEString(), "equipmentUse", null,
				0, 1, EquipmentSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentSegmentSpecification_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0,
				1, EquipmentSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentSegmentSpecification_QuantityUnitOfMeasurement(), ecorePackage.getEString(),
				"quantityUnitOfMeasurement", null, 0, 1, EquipmentSegmentSpecification.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelSegmentSpecificationEClass, PersonnelSegmentSpecification.class,
				"PersonnelSegmentSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelSegmentSpecification_Properties(), this.getPersonnelSegmentSpecificationProperty(),
				null, "properties", null, 0, -1, PersonnelSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelSegmentSpecification_PersonnelClasses(), this.getPersonnelClass(), null,
				"personnelClasses", null, 0, -1, PersonnelSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelSegmentSpecification_Persons(), this.getPerson(), null, "persons", null, 0, -1,
				PersonnelSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelSegmentSpecification_Description(), ecorePackage.getEString(), "description", null,
				0, 1, PersonnelSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelSegmentSpecification_PersonnelUse(), ecorePackage.getEString(), "personnelUse", null,
				0, 1, PersonnelSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelSegmentSpecification_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0,
				1, PersonnelSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelSegmentSpecification_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, PersonnelSegmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(processSegmentParameterEClass, ProcessSegmentParameter.class, "ProcessSegmentParameter",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcessSegmentParameter_SubParameters(), this.getProcessSegmentParameter(), null,
				"subParameters", null, 0, -1, ProcessSegmentParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentParameter_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				ProcessSegmentParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentParameter_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				ProcessSegmentParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentParameter_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				ProcessSegmentParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentParameter_ValueUnitOfMeasure(), ecorePackage.getEString(), "valueUnitOfMeasure",
				null, 0, 1, ProcessSegmentParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(processSegmentDependencyEClass, ProcessSegmentDependency.class, "ProcessSegmentDependency",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcessSegmentDependency_Subject(), this.getProcessSegment(), null, "subject", null, 1, 1,
				ProcessSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegmentDependency_Dependency(), this.getProcessSegment(), null, "dependency", null, 1,
				1, ProcessSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentDependency_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				ProcessSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentDependency_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				ProcessSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentDependency_DependencyType(), ecorePackage.getEString(), "dependencyType", null,
				0, 1, ProcessSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentDependency_DependencyFactor(), ecorePackage.getEString(), "dependencyFactor",
				null, 0, 1, ProcessSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentDependency_FactorUnitOfMeasure(), ecorePackage.getEString(),
				"factorUnitOfMeasure", null, 0, 1, ProcessSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iec62264ModelEClass, Iec62264Model.class, "Iec62264Model", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIec62264Model_PersonnelModel(), this.getPersonnelModel(), null, "personnelModel", null, 0, 1,
				Iec62264Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIec62264Model_EquipmentModel(), this.getEquipmentModel(), null, "equipmentModel", null, 0, 1,
				Iec62264Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIec62264Model_PhysicalAssetModel(), this.getPhysicalAssetModel(), null, "physicalAssetModel",
				null, 0, 1, Iec62264Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIec62264Model_MaterialModel(), this.getMaterialModel(), null, "materialModel", null, 0, 1,
				Iec62264Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIec62264Model_ProcessSegmentModel(), this.getProcessSegmentModel(), null,
				"processSegmentModel", null, 0, 1, Iec62264Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIec62264Model_OperationsDefinitionModel(), this.getOperationsDefinitionModel(), null,
				"operationsDefinitionModel", null, 0, 1, Iec62264Model.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIec62264Model_OperationsScheduleModel(), this.getOperationsScheduleModel(), null,
				"operationsScheduleModel", null, 0, 1, Iec62264Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIec62264Model_OperationsPerformanceModel(), this.getOperationsPerformanceModel(), null,
				"operationsPerformanceModel", null, 0, 1, Iec62264Model.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIec62264Model_ProcessSegmentCapabilityModel(), this.getProcessSegmentCapabilityModel(), null,
				"processSegmentCapabilityModel", null, 0, 1, Iec62264Model.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIec62264Model_HierarchyScopes(), this.getHierarchyScope(), null, "hierarchyScopes", null, 0,
				-1, Iec62264Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIec62264Model_Name(), ecorePackage.getEString(), "name", null, 0, 1, Iec62264Model.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsDefinitionModelEClass, OperationsDefinitionModel.class, "OperationsDefinitionModel",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsDefinitionModel_OperationsDefinitions(), this.getOperationsDefinition(), null,
				"operationsDefinitions", null, 0, -1, OperationsDefinitionModel.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsDefinitionModel_OperationsSegments(), this.getOperationsSegment(), null,
				"operationsSegments", null, 0, -1, OperationsDefinitionModel.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsDefinitionModel_OperationsSegmentDependencies(),
				this.getOperationsSegmentDependency(), null, "operationsSegmentDependencies", null, 0, -1,
				OperationsDefinitionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsScheduleModelEClass, OperationsScheduleModel.class, "OperationsScheduleModel",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsScheduleModel_OperationsSchedules(), this.getOperationsSchedule(), null,
				"operationsSchedules", null, 0, -1, OperationsScheduleModel.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsPerformanceModelEClass, OperationsPerformanceModel.class, "OperationsPerformanceModel",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsPerformanceModel_OperationsPerformances(), this.getOperationsPerformance(), null,
				"operationsPerformances", null, 0, -1, OperationsPerformanceModel.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsCapabilityModelEClass, OperationsCapabilityModel.class, "OperationsCapabilityModel",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsCapabilityModel_OperationsCapabilities(), this.getOperationsCapability(), null,
				"operationsCapabilities", null, 0, -1, OperationsCapabilityModel.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(processSegmentCapabilityModelEClass, ProcessSegmentCapabilityModel.class,
				"ProcessSegmentCapabilityModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcessSegmentCapabilityModel_ProcessSegmentCapabilities(),
				this.getProcessSegmentCapability(), null, "processSegmentCapabilities", null, 0, -1,
				ProcessSegmentCapabilityModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hierarchyScopeEClass, HierarchyScope.class, "HierarchyScope", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHierarchyScope_SubScope(), this.getHierarchyScope(), null, "subScope", null, 0, 1,
				HierarchyScope.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHierarchyScope_Equipment(), this.getEquipment(), null, "equipment", null, 0, 1,
				HierarchyScope.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHierarchyScope_EquipmentElementLevel(), ecorePackage.getEString(), "equipmentElementLevel",
				null, 0, 1, HierarchyScope.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialSpecificationPropertyEClass, MaterialSpecificationProperty.class,
				"MaterialSpecificationProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialSpecificationProperty_SubProperties(), this.getMaterialSpecificationProperty(), null,
				"subProperties", null, 0, -1, MaterialSpecificationProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetSpecificationPropertyEClass, PhysicalAssetSpecificationProperty.class,
				"PhysicalAssetSpecificationProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetSpecificationProperty_SubProperties(),
				this.getPhysicalAssetSpecificationProperty(), null, "subProperties", null, 0, -1,
				PhysicalAssetSpecificationProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentSpecificationPropertyEClass, EquipmentSpecificationProperty.class,
				"EquipmentSpecificationProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentSpecificationProperty_SubProperties(), this.getEquipmentSpecificationProperty(),
				null, "subProperties", null, 0, -1, EquipmentSpecificationProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelSpecificationPropertyEClass, PersonnelSpecificationProperty.class,
				"PersonnelSpecificationProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelSpecificationProperty_SubProperties(), this.getPersonnelSpecificationProperty(),
				null, "subProperties", null, 0, -1, PersonnelSpecificationProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialSpecificationEClass, MaterialSpecification.class, "MaterialSpecification", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialSpecification_AssembledFromSpecifications(), this.getMaterialSpecification(), null,
				"assembledFromSpecifications", null, 0, -1, MaterialSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialSpecification_MaterialSpecificationProperties(),
				this.getMaterialSpecificationProperty(), null, "materialSpecificationProperties", null, 0, -1,
				MaterialSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialSpecification_MaterialClasses(), this.getMaterialClass(), null, "materialClasses",
				null, 0, -1, MaterialSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialSpecification_MaterialDefinitions(), this.getMaterialDefinition(), null,
				"materialDefinitions", null, 0, -1, MaterialSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSpecification_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MaterialSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSpecification_MaterialUse(), this.getMaterialUse(), "materialUse", null, 0, 1,
				MaterialSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSpecification_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				MaterialSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSpecification_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, MaterialSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSpecification_AssemblyType(), this.getAssemblyType(), "assemblyType", null, 0, 1,
				MaterialSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialSpecification_AssemblyRelationship(), this.getAssemblyRelationship(),
				"assemblyRelationship", null, 0, 1, MaterialSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetSpecificationEClass, PhysicalAssetSpecification.class, "PhysicalAssetSpecification",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetSpecification_PhysicalAssetSpecificationProperties(),
				this.getPhysicalAssetSpecificationProperty(), null, "physicalAssetSpecificationProperties", null, 0, -1,
				PhysicalAssetSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetSpecification_Description(), ecorePackage.getEString(), "description", null, 0,
				1, PhysicalAssetSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetSpecification_PhysicAlassetClasses(), this.getPhysicalAssetClass(), null,
				"physicAlassetClasses", null, 0, -1, PhysicalAssetSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetSpecification_PhysicalAssets(), this.getPhysicalAsset(), null, "physicalAssets",
				null, 0, 1, PhysicalAssetSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetSpecification_PhysicalAssetUse(), ecorePackage.getEString(), "physicalAssetUse",
				null, 0, 1, PhysicalAssetSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetSpecification_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				PhysicalAssetSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetSpecification_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, PhysicalAssetSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentSpecificationEClass, EquipmentSpecification.class, "EquipmentSpecification", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentSpecification_EquipmentSpecificationProperties(),
				this.getEquipmentSpecificationProperty(), null, "equipmentSpecificationProperties", null, 0, -1,
				EquipmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentSpecification_EquipmentClasses(), this.getEquipmentClass(), null, "equipmentClasses",
				null, 0, -1, EquipmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentSpecification_Equipments(), this.getEquipment(), null, "equipments", null, 0, -1,
				EquipmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentSpecification_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				EquipmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentSpecification_EquipmentUse(), ecorePackage.getEString(), "equipmentUse", null, 0, 1,
				EquipmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentSpecification_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				EquipmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentSpecification_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, EquipmentSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelSpecificationEClass, PersonnelSpecification.class, "PersonnelSpecification", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelSpecification_PersonnelSpecificationProperties(),
				this.getPersonnelSpecificationProperty(), null, "personnelSpecificationProperties", null, 0, -1,
				PersonnelSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelSpecification_PersonnelClasses(), this.getPersonnelClass(), null, "personnelClasses",
				null, 0, -1, PersonnelSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelSpecification_Persons(), this.getPerson(), null, "persons", null, 0, -1,
				PersonnelSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelSpecification_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PersonnelSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelSpecification_PersonnelUse(), ecorePackage.getEString(), "personnelUse", null, 0, 1,
				PersonnelSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelSpecification_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				PersonnelSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelSpecification_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, PersonnelSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(parameterSpecificationEClass, ParameterSpecification.class, "ParameterSpecification", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getParameterSpecification_SubParameters(), this.getParameterSpecification(), null,
				"subParameters", null, 0, -1, ParameterSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getParameterSpecification_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				ParameterSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getParameterSpecification_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				ParameterSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getParameterSpecification_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				ParameterSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getParameterSpecification_ValueUnitOfMeasurement(), ecorePackage.getEString(),
				"valueUnitOfMeasurement", null, 0, 1, ParameterSpecification.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsSegmentDependencyEClass, OperationsSegmentDependency.class, "OperationsSegmentDependency",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsSegmentDependency_Subject(), this.getOperationsSegment(), null, "subject", null, 1,
				1, OperationsSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsSegmentDependency_Dependency(), this.getOperationsSegment(), null, "dependency",
				null, 1, 1, OperationsSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSegmentDependency_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				OperationsSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSegmentDependency_Description(), ecorePackage.getEString(), "description", null, 0,
				1, OperationsSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSegmentDependency_DependencyType(), ecorePackage.getEString(), "dependencyType",
				null, 0, 1, OperationsSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSegmentDependency_DependencyFactor(), ecorePackage.getEString(), "dependencyFactor",
				null, 0, 1, OperationsSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSegmentDependency_UnitOfMeasure(), ecorePackage.getEString(), "unitOfMeasure", null,
				0, 1, OperationsSegmentDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsMaterialBillItemEClass, OperationsMaterialBillItem.class, "OperationsMaterialBillItem",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsMaterialBillItem_MaterialSpecifications(), this.getMaterialSpecification(), null,
				"materialSpecifications", null, 1, -1, OperationsMaterialBillItem.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsMaterialBillItem_AssembledFromItems(), this.getOperationsMaterialBillItem(), null,
				"assembledFromItems", null, 0, -1, OperationsMaterialBillItem.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsMaterialBillItem_Id(), ecorePackage.getEString(), "id", null, 0, 1,
				OperationsMaterialBillItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsMaterialBillItem_Description(), ecorePackage.getEString(), "description", null, 0,
				1, OperationsMaterialBillItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsMaterialBillItem_MaterialClasses(), this.getMaterialClass(), null,
				"materialClasses", null, 0, -1, OperationsMaterialBillItem.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsMaterialBillItem_MaterialDefinitions(), this.getMaterialDefinition(), null,
				"materialDefinitions", null, 0, -1, OperationsMaterialBillItem.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsMaterialBillItem_UseType(), this.getUseType(), "useType", null, 0, 1,
				OperationsMaterialBillItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsMaterialBillItem_AssemblyType(), this.getAssemblyType(), "assemblyType", null, 0, 1,
				OperationsMaterialBillItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsMaterialBillItem_AssemblyRelationship(), this.getAssemblyRelationship(),
				"assemblyRelationship", null, 0, 1, OperationsMaterialBillItem.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsMaterialBillItem_Quantities(), theXMLTypePackage.getDecimal(), "quantities", null,
				0, -1, OperationsMaterialBillItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsMaterialBillItem_QuantityUnitOfMeasures(), ecorePackage.getEString(),
				"quantityUnitOfMeasures", null, 0, -1, OperationsMaterialBillItem.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsMaterialBillEClass, OperationsMaterialBill.class, "OperationsMaterialBill", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsMaterialBill_OperationsMaterialBillItems(), this.getOperationsMaterialBillItem(),
				null, "operationsMaterialBillItems", null, 0, -1, OperationsMaterialBill.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getOperationsMaterialBill_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				OperationsMaterialBill.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsMaterialBill_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				OperationsMaterialBill.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsSegmentEClass, OperationsSegment.class, "OperationsSegment", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsSegment_ProcessSegments(), this.getProcessSegment(), null, "processSegments", null,
				1, -1, OperationsSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsSegment_SubSegments(), this.getOperationsSegment(), null, "subSegments", null, 0,
				-1, OperationsSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsSegment_ParameterSpecifications(), this.getParameterSpecification(), null,
				"parameterSpecifications", null, 0, -1, OperationsSegment.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsSegment_PersonnelSpecifications(), this.getPersonnelSpecification(), null,
				"personnelSpecifications", null, 0, -1, OperationsSegment.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsSegment_EquipmentSpecifications(), this.getEquipmentSpecification(), null,
				"equipmentSpecifications", null, 0, -1, OperationsSegment.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsSegment_PhysicalAssetSpecifications(), this.getPhysicalAssetSpecification(), null,
				"physicalAssetSpecifications", null, 0, -1, OperationsSegment.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsSegment_MaterialSpecifications(), this.getMaterialSpecification(), null,
				"materialSpecifications", null, 0, -1, OperationsSegment.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSegment_Id(), ecorePackage.getEString(), "id", null, 0, 1, OperationsSegment.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSegment_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				OperationsSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSegment_Duration(), theXMLTypePackage.getDecimal(), "duration", null, 0, 1,
				OperationsSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSegment_DurationUnitOfMeasure(), ecorePackage.getEString(), "durationUnitOfMeasure",
				null, 0, 1, OperationsSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSegment_OperationsType(), this.getOperationsType(), "operationsType", null, 1, 1,
				OperationsSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSegment_WorkDefinitionId(), ecorePackage.getEString(), "workDefinitionId", null, 0,
				1, OperationsSegment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsDefinitionEClass, OperationsDefinition.class, "OperationsDefinition", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsDefinition_OperationsSegments(), this.getOperationsSegment(), null,
				"operationsSegments", null, 1, -1, OperationsDefinition.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsDefinition_OperationsMaterialBills(), this.getOperationsMaterialBill(), null,
				"operationsMaterialBills", null, 0, -1, OperationsDefinition.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsDefinition_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				OperationsDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsDefinition_Version(), ecorePackage.getEString(), "version", null, 0, 1,
				OperationsDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsDefinition_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				OperationsDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsDefinition_OperationsType(), this.getOperationsType(), "operationsType", null, 1, 1,
				OperationsDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsDefinition_BillOfMaterialId(), ecorePackage.getEString(), "billOfMaterialId", null,
				0, 1, OperationsDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsDefinition_WorkDefinitionId(), ecorePackage.getEString(), "workDefinitionId", null,
				0, 1, OperationsDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsDefinition_BillOfResourceId(), ecorePackage.getEString(), "billOfResourceId", null,
				0, 1, OperationsDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsDefinition_HierarchysScope(), this.getHierarchyScope(), null, "hierarchysScope",
				null, 0, 1, OperationsDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialRequirementEClass, MaterialRequirement.class, "MaterialRequirement", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialRequirement_MaterialRequirementProperties(), this.getMaterialRequirementProperty(),
				null, "materialRequirementProperties", null, 0, -1, MaterialRequirement.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMaterialRequirement_AssembledFromRequirements(), this.getMaterialRequirement(), null,
				"assembledFromRequirements", null, 0, -1, MaterialRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialRequirement_MaterialClasses(), this.getMaterialClass(), null, "materialClasses", null,
				0, -1, MaterialRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialRequirement_MaterialDefinitions(), this.getMaterialDefinition(), null,
				"materialDefinitions", null, 0, -1, MaterialRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialRequirement_MaterialLots(), this.getMaterialLot(), null, "materialLots", null, 0, -1,
				MaterialRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialRequirement_MaterialSublot(), this.getMaterialSublot(), null, "materialSublot", null,
				0, -1, MaterialRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialRequirement_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MaterialRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialRequirement_MaterialUse(), ecorePackage.getEString(), "materialUse", null, 0, 1,
				MaterialRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialRequirement_StorageLocation(), ecorePackage.getEString(), "storageLocation", null, 0,
				1, MaterialRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialRequirement_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				MaterialRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialRequirement_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, MaterialRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialRequirement_AssemblyType(), this.getAssemblyType(), "assemblyType", null, 0, 1,
				MaterialRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialRequirement_AssemblyRelationship(), this.getAssemblyRelationship(),
				"assemblyRelationship", null, 0, 1, MaterialRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialRequirementPropertyEClass, MaterialRequirementProperty.class, "MaterialRequirementProperty",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialRequirementProperty_SubProperties(), this.getMaterialRequirementProperty(), null,
				"subProperties", null, 0, -1, MaterialRequirementProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetRequirementPropertyEClass, PhysicalAssetRequirementProperty.class,
				"PhysicalAssetRequirementProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetRequirementProperty_SubProperties(), this.getPhysicalAssetRequirementProperty(),
				null, "subProperties", null, 0, -1, PhysicalAssetRequirementProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentRequirementPropertyEClass, EquipmentRequirementProperty.class,
				"EquipmentRequirementProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentRequirementProperty_SubProperties(), this.getEquipmentRequirementProperty(), null,
				"subProperties", null, 0, -1, EquipmentRequirementProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelRequirementPropertyEClass, PersonnelRequirementProperty.class,
				"PersonnelRequirementProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelRequirementProperty_SubProperties(), this.getPersonnelRequirementProperty(), null,
				"subProperties", null, 0, -1, PersonnelRequirementProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsScheduleEClass, OperationsSchedule.class, "OperationsSchedule", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsSchedule_OperationsRequests(), this.getOperationsRequest(), null,
				"operationsRequests", null, 1, -1, OperationsSchedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSchedule_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				OperationsSchedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSchedule_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				OperationsSchedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSchedule_OperationsType(), this.getOperationsType(), "operationsType", null, 1, 1,
				OperationsSchedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSchedule_StartTime(), this.getZonedDateTime(), "startTime", null, 0, 1,
				OperationsSchedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSchedule_EndTime(), this.getZonedDateTime(), "endTime", null, 0, 1,
				OperationsSchedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSchedule_PublishedDate(), this.getZonedDateTime(), "publishedDate", null, 0, 1,
				OperationsSchedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsSchedule_ScheduledState(), this.getScheduleState(), "scheduledState", null, 0, 1,
				OperationsSchedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsSchedule_HierarchyScope(), this.getHierarchyScope(), null, "hierarchyScope", null,
				0, 1, OperationsSchedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsRequestEClass, OperationsRequest.class, "OperationsRequest", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsRequest_SegmentRequirements(), this.getSegmentRequirement(), null,
				"segmentRequirements", null, 1, -1, OperationsRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsRequest_RequestedSegmentResponses(), this.getRequestedSegmentResponse(), null,
				"requestedSegmentResponses", null, 0, -1, OperationsRequest.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsRequest_OperationsDefinition(), this.getOperationsDefinition(), null,
				"operationsDefinition", null, 0, 1, OperationsRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsRequest_Id(), ecorePackage.getEString(), "id", null, 1, 1, OperationsRequest.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsRequest_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				OperationsRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsRequest_OperationsType(), this.getOperationsType(), "operationsType", null, 1, 1,
				OperationsRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsRequest_StartTime(), this.getZonedDateTime(), "startTime", null, 0, 1,
				OperationsRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsRequest_EndTime(), this.getZonedDateTime(), "endTime", null, 0, 1,
				OperationsRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsRequest_Priority(), ecorePackage.getEString(), "priority", null, 0, 1,
				OperationsRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsRequest_RequestState(), this.getScheduleState(), "requestState", null, 0, 1,
				OperationsRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsRequest_HierarchyScope(), this.getHierarchyScope(), null, "hierarchyScope", null, 0,
				1, OperationsRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(requestedSegmentResponseEClass, RequestedSegmentResponse.class, "RequestedSegmentResponse",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(segmentRequirementEClass, SegmentRequirement.class, "SegmentRequirement", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSegmentRequirement_SubRequirements(), this.getSegmentRequirement(), null, "subRequirements",
				null, 0, -1, SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentRequirement_ProcessSegment(), this.getProcessSegment(), null, "processSegment", null,
				1, 1, SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentRequirement_OperationsDefinition(), this.getOperationsDefinition(), null,
				"operationsDefinition", null, 0, 1, SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentRequirement_SegmentParameters(), this.getSegmentParameter(), null, "segmentParameters",
				null, 0, -1, SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentRequirement_PersonnelRequirements(), this.getPersonnelRequirement(), null,
				"personnelRequirements", null, 0, -1, SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentRequirement_EquipmentRequirements(), this.getEquipmentRequirement(), null,
				"equipmentRequirements", null, 0, -1, SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentRequirement_PhysicalAssetRequirements(), this.getPhysicalAssetRequirement(), null,
				"physicalAssetRequirements", null, 0, -1, SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentRequirement_MaterialRequirements(), this.getMaterialRequirement(), null,
				"materialRequirements", null, 0, -1, SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentRequirement_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentRequirement_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentRequirement_OperationsType(), this.getOperationsType(), "operationsType", null, 1, 1,
				SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentRequirement_EarliestStartTime(), this.getZonedDateTime(), "earliestStartTime", null, 0,
				1, SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentRequirement_LatestStartTime(), this.getZonedDateTime(), "latestStartTime", null, 0, 1,
				SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentRequirement_Duration(), theXMLTypePackage.getDecimal(), "duration", null, 0, 1,
				SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentRequirement_DurationUnitOfMeasurement(), ecorePackage.getEString(),
				"durationUnitOfMeasurement", null, 0, 1, SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentRequirement_SegmentState(), this.getScheduleState(), "segmentState", null, 0, 1,
				SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentRequirement_HierarchyScope(), this.getHierarchyScope(), null, "hierarchyScope", null,
				0, 1, SegmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentRequirementEClass, EquipmentRequirement.class, "EquipmentRequirement", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentRequirement_EquipmentRequirementProperties(), this.getEquipmentRequirementProperty(),
				null, "equipmentRequirementProperties", null, 0, -1, EquipmentRequirement.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getEquipmentRequirement_EquipmentClasses(), this.getEquipmentClass(), null, "equipmentClasses",
				null, 0, -1, EquipmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentRequirement_Equipments(), this.getEquipment(), null, "equipments", null, 0, -1,
				EquipmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentRequirement_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				EquipmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentRequirement_EquipmentUse(), ecorePackage.getEString(), "equipmentUse", null, 0, 1,
				EquipmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentRequirement_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				EquipmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentRequirement_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, EquipmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentRequirement_EquipmentLevel(), ecorePackage.getEString(), "equipmentLevel", null, 0,
				1, EquipmentRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetRequirementEClass, PhysicalAssetRequirement.class, "PhysicalAssetRequirement",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetRequirement_PhysicalAssetRequirementProperties(),
				this.getPhysicalAssetRequirementProperty(), null, "physicalAssetRequirementProperties", null, 0, -1,
				PhysicalAssetRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetRequirement_PhysicalAssetClasses(), this.getPhysicalAssetClass(), null,
				"physicalAssetClasses", null, 0, -1, PhysicalAssetRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetRequirement_PhysicalAssets(), this.getPhysicalAsset(), null, "physicalAssets",
				null, 0, -1, PhysicalAssetRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetRequirement_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PhysicalAssetRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetRequirement_PhysicalAssetUse(), ecorePackage.getEString(), "physicalAssetUse",
				null, 0, 1, PhysicalAssetRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetRequirement_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				PhysicalAssetRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetRequirement_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, PhysicalAssetRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetRequirement_EquipmentLevel(), ecorePackage.getEString(), "equipmentLevel", null,
				0, 1, PhysicalAssetRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelRequirementEClass, PersonnelRequirement.class, "PersonnelRequirement", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelRequirement_PersonnelRequirementProperties(), this.getPersonnelRequirementProperty(),
				null, "personnelRequirementProperties", null, 0, -1, PersonnelRequirement.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getPersonnelRequirement_PersonnelClasses(), this.getPersonnelClass(), null, "personnelClasses",
				null, 0, -1, PersonnelRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelRequirement_Persons(), this.getPerson(), null, "persons", null, 0, -1,
				PersonnelRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelRequirement_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PersonnelRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelRequirement_PersonnelUse(), ecorePackage.getEString(), "personnelUse", null, 0, 1,
				PersonnelRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelRequirement_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				PersonnelRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelRequirement_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, PersonnelRequirement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(segmentParameterEClass, SegmentParameter.class, "SegmentParameter", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSegmentParameter_SubParameters(), this.getSegmentParameter(), null, "subParameters", null, 0,
				-1, SegmentParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentParameter_Id(), ecorePackage.getEString(), "id", null, 1, 1, SegmentParameter.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentParameter_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				SegmentParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentParameter_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				SegmentParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentParameter_ValueUnitOfMeasure(), ecorePackage.getEString(), "valueUnitOfMeasure", null,
				0, 1, SegmentParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialActualPropertyEClass, MaterialActualProperty.class, "MaterialActualProperty", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialActualProperty_SubProperties(), this.getMaterialActualProperty(), null,
				"subProperties", null, 0, -1, MaterialActualProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetActualPropertyEClass, PhysicalAssetActualProperty.class, "PhysicalAssetActualProperty",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetActualProperty_SubProperties(), this.getPhysicalAssetActualProperty(), null,
				"subProperties", null, 0, -1, PhysicalAssetActualProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentActualPropertyEClass, EquipmentActualProperty.class, "EquipmentActualProperty",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentActualProperty_SubProperties(), this.getEquipmentActualProperty(), null,
				"subProperties", null, 0, -1, EquipmentActualProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelActualPropertyEClass, PersonnelActualProperty.class, "PersonnelActualProperty",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelActualProperty_SubProperties(), this.getPersonnelActualProperty(), null,
				"subProperties", null, 0, -1, PersonnelActualProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialActualEClass, MaterialActual.class, "MaterialActual", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialActual_AssembledFromActuals(), this.getMaterialActual(), null, "assembledFromActuals",
				null, 0, -1, MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialActual_MaterialActualProperties(), this.getMaterialActualProperty(), null,
				"materialActualProperties", null, 0, -1, MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialActual_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialActual_MaterialUse(), ecorePackage.getEString(), "materialUse", null, 0, 1,
				MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialActual_StorageLocation(), ecorePackage.getEString(), "storageLocation", null, 0, 1,
				MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialActual_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialActual_QuantityUnitOfMeasure(), ecorePackage.getEString(), "quantityUnitOfMeasure",
				null, 0, 1, MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialActual_AssemblyType(), this.getAssemblyType(), "assemblyType", null, 0, 1,
				MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialActual_AssemblyRelationship(), this.getAssemblyRelationship(), "assemblyRelationship",
				null, 0, 1, MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialActual_MaterialClasses(), this.getMaterialClass(), null, "materialClasses", null, 0,
				-1, MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialActual_MaterialDefinitions(), this.getMaterialDefinition(), null,
				"materialDefinitions", null, 0, -1, MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialActual_MaterialLots(), this.getMaterialLot(), null, "materialLots", null, 0, -1,
				MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialActual_MaterialSublots(), this.getMaterialSublot(), null, "materialSublots", null, 0,
				-1, MaterialActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetActualEClass, PhysicalAssetActual.class, "PhysicalAssetActual", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetActual_PhysicalAssetActualProperties(), this.getPhysicalAssetActualProperty(),
				null, "physicalAssetActualProperties", null, 0, -1, PhysicalAssetActual.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getPhysicalAssetActual_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PhysicalAssetActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetActual_PhysicalAssetUse(), ecorePackage.getEString(), "physicalAssetUse", null,
				0, 1, PhysicalAssetActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetActual_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				PhysicalAssetActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetActual_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, PhysicalAssetActual.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetActual_PhysicalAssetClasses(), this.getPhysicalAssetClass(), null,
				"physicalAssetClasses", null, 0, -1, PhysicalAssetActual.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetActual_PhysicalAssets(), this.getPhysicalAsset(), null, "physicalAssets", null,
				0, -1, PhysicalAssetActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentActualEClass, EquipmentActual.class, "EquipmentActual", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentActual_EquipmentActualProperties(), this.getEquipmentActualProperty(), null,
				"equipmentActualProperties", null, 0, -1, EquipmentActual.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentActual_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				EquipmentActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentActual_EquipmentUse(), ecorePackage.getEString(), "equipmentUse", null, 0, 1,
				EquipmentActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentActual_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				EquipmentActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentActual_QuantityUnitOfMeasure(), ecorePackage.getEString(), "quantityUnitOfMeasure",
				null, 0, 1, EquipmentActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentActual_EquipmentClasses(), this.getEquipmentClass(), null, "equipmentClasses", null,
				0, -1, EquipmentActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentActual_Equipments(), this.getEquipment(), null, "equipments", null, 0, -1,
				EquipmentActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelActualEClass, PersonnelActual.class, "PersonnelActual", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelActual_PersonnelActualProperties(), this.getPersonnelActualProperty(), null,
				"personnelActualProperties", null, 0, -1, PersonnelActual.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelActual_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PersonnelActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelActual_PersonnelUse(), ecorePackage.getEString(), "personnelUse", null, 0, 1,
				PersonnelActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelActual_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				PersonnelActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelActual_QuantityUnitOfMeasure(), ecorePackage.getEString(), "quantityUnitOfMeasure",
				null, 0, 1, PersonnelActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelActual_PersonnelClasses(), this.getPersonnelClass(), null, "personnelClasses", null,
				0, -1, PersonnelActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelActual_Persons(), this.getPerson(), null, "persons", null, 0, -1,
				PersonnelActual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(segmentDataEClass, SegmentData.class, "SegmentData", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSegmentData_SubData(), this.getSegmentData(), null, "subData", null, 0, -1, SegmentData.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentData_Id(), ecorePackage.getEString(), "id", null, 1, 1, SegmentData.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentData_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				SegmentData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentData_Value(), ecorePackage.getEString(), "value", null, 0, 1, SegmentData.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentData_ValueUnitOfMeasure(), ecorePackage.getEString(), "valueUnitOfMeasure", null, 0, 1,
				SegmentData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(segmentResponseEClass, SegmentResponse.class, "SegmentResponse", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSegmentResponse_SubResponses(), this.getSegmentResponse(), null, "subResponses", null, 0, -1,
				SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentResponse_ProcessSegment(), this.getProcessSegment(), null, "processSegment", null, 1,
				1, SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentResponse_OperationsDefinition(), this.getOperationsDefinition(), null,
				"operationsDefinition", null, 0, 1, SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentResponse_SegmentData(), this.getSegmentData(), null, "segmentData", null, 0, -1,
				SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentResponse_PersonnelActuals(), this.getPersonnelActual(), null, "personnelActuals", null,
				0, -1, SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentResponse_EquipmentActuals(), this.getEquipmentActual(), null, "equipmentActuals", null,
				0, 1, SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentResponse_PhysicalAssetActuals(), this.getPhysicalAssetActual(), null,
				"physicalAssetActuals", null, 0, -1, SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentResponse_MaterialActuals(), this.getMaterialActual(), null, "materialActuals", null, 0,
				-1, SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentResponse_Id(), ecorePackage.getEString(), "id", null, 1, 1, SegmentResponse.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentResponse_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentResponse_OperationsType(), this.getOperationsType(), "operationsType", null, 1, 1,
				SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentResponse_ActualStartTime(), this.getZonedDateTime(), "actualStartTime", null, 0, 1,
				SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentResponse_ActualEndTime(), this.getZonedDateTime(), "actualEndTime", null, 0, 1,
				SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getSegmentResponse_SegmentState(), this.getPerformanceState(), "segmentState", null, 0, 1,
				SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getSegmentResponse_HierarchyScope(), this.getHierarchyScope(), null, "hierarchyScope", null, 0,
				1, SegmentResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsResponseEClass, OperationsResponse.class, "OperationsResponse", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsResponse_SegmentResponses(), this.getSegmentResponse(), null, "segmentResponses",
				null, 1, -1, OperationsResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsResponse_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				OperationsResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsResponse_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				OperationsResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsResponse_OperationsType(), this.getOperationsType(), "operationsType", null, 1, 1,
				OperationsResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsResponse_StartTime(), this.getZonedDateTime(), "startTime", null, 0, 1,
				OperationsResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsResponse_EndTime(), this.getZonedDateTime(), "endTime", null, 0, 1,
				OperationsResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsResponse_ResponseState(), this.getPerformanceState(), "responseState", null, 0, 1,
				OperationsResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsResponse_OperationsRequest(), this.getOperationsRequest(), null,
				"operationsRequest", null, 0, 1, OperationsResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsResponse_HierarchyScope(), this.getHierarchyScope(), null, "hierarchyScope", null,
				0, 1, OperationsResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsResponse_OperationsDefinition(), this.getOperationsDefinition(), null,
				"operationsDefinition", null, 0, 1, OperationsResponse.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsPerformanceEClass, OperationsPerformance.class, "OperationsPerformance", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsPerformance_OperationsResponses(), this.getOperationsResponse(), null,
				"operationsResponses", null, 1, -1, OperationsPerformance.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsPerformance_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				OperationsPerformance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsPerformance_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				OperationsPerformance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsPerformance_OperationsType(), this.getOperationsType(), "operationsType", null, 1,
				1, OperationsPerformance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsPerformance_StartTime(), this.getZonedDateTime(), "startTime", null, 0, 1,
				OperationsPerformance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsPerformance_OperationsSchedule(), this.getOperationsSchedule(), null,
				"operationsSchedule", null, 0, 1, OperationsPerformance.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsPerformance_EndTime(), this.getZonedDateTime(), "endTime", null, 0, 1,
				OperationsPerformance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsPerformance_PerformanceState(), this.getPerformanceState(), "performanceState",
				null, 0, 1, OperationsPerformance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsPerformance_HierarchyScope(), this.getHierarchyScope(), null, "hierarchyScope",
				null, 0, 1, OperationsPerformance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsPerformance_PublishedDate(), this.getZonedDateTime(), "publishedDate", null, 0, 1,
				OperationsPerformance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationsCapabilityEClass, OperationsCapability.class, "OperationsCapability", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationsCapability_ProcessSegmentCapabilities(), this.getProcessSegmentCapability(), null,
				"processSegmentCapabilities", null, 0, -1, OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsCapability_PersonnelCapabilities(), this.getPersonnelCapability(), null,
				"personnelCapabilities", null, 0, -1, OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsCapability_EquipmentCapabilities(), this.getEquipmentCapability(), null,
				"equipmentCapabilities", null, 0, 1, OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsCapability_PhysicalAssetCapabilities(), this.getPhysicalAssetCapability(), null,
				"physicalAssetCapabilities", null, 0, 1, OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsCapability_MaterialCapabilities(), this.getMaterialCapability(), null,
				"materialCapabilities", null, 0, 1, OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsCapability_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsCapability_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsCapability_CapacityType(), this.getCapabilityType(), "capacityType", null, 0, 1,
				OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsCapability_Reason(), ecorePackage.getEString(), "reason", null, 0, 1,
				OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsCapability_ConfidenceFactor(), ecorePackage.getEString(), "confidenceFactor", null,
				0, 1, OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsCapability_StartTime(), this.getZonedDateTime(), "startTime", null, 0, 1,
				OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsCapability_EndTime(), this.getZonedDateTime(), "endTime", null, 0, 1,
				OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationsCapability_PublishedDate(), this.getZonedDateTime(), "publishedDate", null, 0, 1,
				OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationsCapability_HierarchyScope(), this.getHierarchyScope(), null, "hierarchyScope", null,
				0, 1, OperationsCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialCapabilityPropertyEClass, MaterialCapabilityProperty.class, "MaterialCapabilityProperty",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialCapabilityProperty_SubProperties(), this.getMaterialCapabilityProperty(), null,
				"subProperties", null, 0, -1, MaterialCapabilityProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetCapabilityPropertyEClass, PhysicalAssetCapabilityProperty.class,
				"PhysicalAssetCapabilityProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetCapabilityProperty_SubProperties(), this.getPhysicalAssetCapabilityProperty(),
				null, "subProperties", null, 0, -1, PhysicalAssetCapabilityProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentCapabilityPropertyEClass, EquipmentCapabilityProperty.class, "EquipmentCapabilityProperty",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentCapabilityProperty_SubProperties(), this.getEquipmentCapabilityProperty(), null,
				"subProperties", null, 0, -1, EquipmentCapabilityProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelCapabilityPropertyEClass, PersonnelCapabilityProperty.class, "PersonnelCapabilityProperty",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelCapabilityProperty_SubProperties(), this.getPersonnelCapabilityProperty(), null,
				"subProperties", null, 0, -1, PersonnelCapabilityProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(materialCapabilityEClass, MaterialCapability.class, "MaterialCapability", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMaterialCapability_AssembledFromCapabilities(), this.getMaterialCapability(), null,
				"assembledFromCapabilities", null, 0, -1, MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialCapability_MaterialCapabilityProperty(), this.getMaterialCapabilityProperty(), null,
				"materialCapabilityProperty", null, 0, -1, MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialCapability_MaterialClass(), this.getMaterialClass(), null, "materialClass", null, 0,
				1, MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialCapability_MaterialDefinition(), this.getMaterialDefinition(), null,
				"materialDefinition", null, 0, 1, MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialCapability_MaterialLot(), this.getMaterialLot(), null, "materialLot", null, 0, 1,
				MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialCapability_MaterialSublot(), this.getMaterialSublot(), null, "materialSublot", null,
				0, 1, MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialCapability_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialCapability_CapabilityType(), this.getCapabilityType(), "capabilityType", null, 0, 1,
				MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialCapability_Reason(), ecorePackage.getEString(), "reason", null, 0, 1,
				MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialCapability_ConfidenceFactor(), ecorePackage.getEString(), "confidenceFactor", null, 0,
				1, MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialCapability_MaterialUse(), ecorePackage.getEString(), "materialUse", null, 0, 1,
				MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialCapability_StartTime(), this.getZonedDateTime(), "startTime", null, 0, 1,
				MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialCapability_EndTime(), this.getZonedDateTime(), "endTime", null, 0, 1,
				MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialCapability_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialCapability_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMaterialCapability_Hierarchyscope(), this.getHierarchyScope(), null, "hierarchyscope", null,
				0, 1, MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialCapability_AssemblyType(), this.getAssemblyType(), "assemblyType", null, 0, 1,
				MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMaterialCapability_AssemblyRelationship(), this.getAssemblyRelationship(),
				"assemblyRelationship", null, 0, 1, MaterialCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(physicalAssetCapabilityEClass, PhysicalAssetCapability.class, "PhysicalAssetCapability",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhysicalAssetCapability_PhysicalAssetCapabilityProperty(),
				this.getPhysicalAssetCapabilityProperty(), null, "physicalAssetCapabilityProperty", null, 0, -1,
				PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetCapability_PhysicalAssetClass(), this.getPhysicalAssetClass(), null,
				"physicalAssetClass", null, 0, 1, PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetCapability_PhysicalAsset(), this.getPhysicalAsset(), null, "physicalAsset", null,
				0, 1, PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapability_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapability_CapabilityType(), this.getCapabilityType(), "capabilityType", null, 0,
				1, PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapability_Reason(), ecorePackage.getEString(), "reason", null, 0, 1,
				PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapability_ConfidenceFactor(), ecorePackage.getEString(), "confidenceFactor",
				null, 0, 1, PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapability_PhysicalAssetUse(), ecorePackage.getEString(), "physicalAssetUse",
				null, 0, 1, PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhysicalAssetCapability_HierarchyScope(), this.getHierarchyScope(), null, "hierarchyScope",
				null, 0, 1, PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapability_StartTime(), this.getZonedDateTime(), "startTime", null, 0, 1,
				PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapability_EndTime(), this.getZonedDateTime(), "endTime", null, 0, 1,
				PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapability_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhysicalAssetCapability_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, PhysicalAssetCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(equipmentCapabilityEClass, EquipmentCapability.class, "EquipmentCapability", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEquipmentCapability_EquipmentCapabilityProperty(), this.getEquipmentCapabilityProperty(),
				null, "equipmentCapabilityProperty", null, 0, -1, EquipmentCapability.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getEquipmentCapability_EquipmentClass(), this.getEquipmentClass(), null, "equipmentClass", null,
				0, 1, EquipmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentCapability_Equipment(), this.getEquipment(), null, "equipment", null, 0, 1,
				EquipmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapability_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				EquipmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapability_CapabilityType(), this.getCapabilityType(), "capabilityType", null, 0, 1,
				EquipmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapability_Reason(), ecorePackage.getEString(), "reason", null, 0, 1,
				EquipmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapability_ConfidenceFactor(), ecorePackage.getEString(), "confidenceFactor", null,
				0, 1, EquipmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapability_EquipmentUse(), ecorePackage.getEString(), "equipmentUse", null, 0, 1,
				EquipmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapability_StartTime(), this.getZonedDateTime(), "startTime", null, 0, 1,
				EquipmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapability_EndTime(), this.getZonedDateTime(), "endTime", null, 0, 1,
				EquipmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapability_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				EquipmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEquipmentCapability_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, EquipmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEquipmentCapability_HierarchyScope(), this.getHierarchyScope(), null, "hierarchyScope", null,
				0, 1, EquipmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personnelCapabilityEClass, PersonnelCapability.class, "PersonnelCapability", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonnelCapability_PersonnelCapabilityProperty(), this.getPersonnelCapabilityProperty(),
				null, "personnelCapabilityProperty", null, 0, -1, PersonnelCapability.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getPersonnelCapability_PersonnelClass(), this.getPersonnelClass(), null, "personnelClass", null,
				0, 1, PersonnelCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelCapability_Person(), this.getPerson(), null, "person", null, 0, 1,
				PersonnelCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelCapability_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				PersonnelCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelCapability_CapabilityType(), this.getCapabilityType(), "capabilityType", null, 0, 1,
				PersonnelCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelCapability_Reason(), ecorePackage.getEString(), "reason", null, 0, 1,
				PersonnelCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelCapability_ConfidenceFactor(), ecorePackage.getEString(), "confidenceFactor", null,
				0, 1, PersonnelCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPersonnelCapability_HierarchyScope(), this.getHierarchyScope(), null, "hierarchyScope", null,
				0, 1, PersonnelCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelCapability_PersonnelUse(), ecorePackage.getEString(), "personnelUse", null, 0, 1,
				PersonnelCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelCapability_StartTime(), this.getZonedDateTime(), "startTime", null, 0, 1,
				PersonnelCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelCapability_EndTime(), this.getZonedDateTime(), "endTime", null, 0, 1,
				PersonnelCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelCapability_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				PersonnelCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPersonnelCapability_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, PersonnelCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(processSegmentCapabilityEClass, ProcessSegmentCapability.class, "ProcessSegmentCapability",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcessSegmentCapability_SubCapabilities(), this.getProcessSegmentCapability(), null,
				"subCapabilities", null, 0, -1, ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegmentCapability_PersonnelCapabilities(), this.getPersonnelCapability(), null,
				"personnelCapabilities", null, 0, -1, ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegmentCapability_EquipmentCapabilities(), this.getEquipmentCapability(), null,
				"equipmentCapabilities", null, 0, -1, ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegmentCapability_PhysicalAssetCapabilities(), this.getPhysicalAssetCapability(), null,
				"physicalAssetCapabilities", null, 0, -1, ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegmentCapability_MaterialCapabilities(), this.getMaterialCapability(), null,
				"materialCapabilities", null, 0, -1, ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegmentCapability_ProcessSegment(), this.getProcessSegment(), null, "processSegment",
				null, 1, 1, ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentCapability_Id(), ecorePackage.getEString(), "id", null, 1, 1,
				ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentCapability_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentCapability_CapacityType(), this.getCapacityType(), "capacityType", null, 0, 1,
				ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentCapability_Reason(), ecorePackage.getEString(), "reason", null, 0, 1,
				ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentCapability_StartTime(), this.getZonedDateTime(), "startTime", null, 0, 1,
				ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessSegmentCapability_EndTime(), this.getZonedDateTime(), "endTime", null, 0, 1,
				ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessSegmentCapability_HierarchyScope(), this.getHierarchyScope(), null, "hierarchyScope",
				null, 0, 1, ProcessSegmentCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referencialPropertyEClass, ReferencialProperty.class, "ReferencialProperty", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getReferencialProperty_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				ReferencialProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReferencialProperty_Value(), ecorePackage.getEString(), "value", null, 0, 1,
				ReferencialProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReferencialProperty_ValueUnitOfMeasure(), ecorePackage.getEString(), "valueUnitOfMeasure",
				null, 0, 1, ReferencialProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReferencialProperty_Quantity(), theXMLTypePackage.getDecimal(), "quantity", null, 0, 1,
				ReferencialProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReferencialProperty_QuantityUnitOfMeasure(), ecorePackage.getEString(),
				"quantityUnitOfMeasure", null, 0, 1, ReferencialProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referentialPersonnelPropertyEClass, ReferentialPersonnelProperty.class,
				"ReferentialPersonnelProperty", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferentialPersonnelProperty_PersonnelClassProperty(), this.getPersonnelClassProperty(), null,
				"personnelClassProperty", null, 0, 1, ReferentialPersonnelProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReferentialPersonnelProperty_PersonProperty(), this.getPersonProperty(), null,
				"personProperty", null, 0, 1, ReferentialPersonnelProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referentialEquipmentPropertyEClass, ReferentialEquipmentProperty.class,
				"ReferentialEquipmentProperty", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferentialEquipmentProperty_EquipmentClassProperty(), this.getEquipmentClassProperty(), null,
				"equipmentClassProperty", null, 0, 1, ReferentialEquipmentProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReferentialEquipmentProperty_EquipmentProperty(), this.getEquipmentProperty(), null,
				"equipmentProperty", null, 0, 1, ReferentialEquipmentProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referentialPhysicalAssetPropertyEClass, ReferentialPhysicalAssetProperty.class,
				"ReferentialPhysicalAssetProperty", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferentialPhysicalAssetProperty_PhysicalAssetClassProperty(),
				this.getPhysicalAssetClassProperty(), null, "physicalAssetClassProperty", null, 0, 1,
				ReferentialPhysicalAssetProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReferentialPhysicalAssetProperty_PhysicalAssetProperty(), this.getPhysicalAssetProperty(),
				null, "physicalAssetProperty", null, 0, 1, ReferentialPhysicalAssetProperty.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(referentialMaterialTypePropertyEClass, ReferentialMaterialTypeProperty.class,
				"ReferentialMaterialTypeProperty", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferentialMaterialTypeProperty_MaterialClassProperty(), this.getMaterialClassProperty(),
				null, "materialClassProperty", null, 0, 1, ReferentialMaterialTypeProperty.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getReferentialMaterialTypeProperty_MaterialDefinitionProperty(),
				this.getMaterialDefinitionProperty(), null, "materialDefinitionProperty", null, 0, 1,
				ReferentialMaterialTypeProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referentialMaterialPropertyEClass, ReferentialMaterialProperty.class, "ReferentialMaterialProperty",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferentialMaterialProperty_MaterialLotProperty(), this.getMaterialLotProperty(), null,
				"materialLotProperty", null, 0, 1, ReferentialMaterialProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(assemblyTypeEEnum, AssemblyType.class, "AssemblyType");
		addEEnumLiteral(assemblyTypeEEnum, AssemblyType.PHYSICAL);
		addEEnumLiteral(assemblyTypeEEnum, AssemblyType.LOGICAL);

		initEEnum(assemblyRelationshipEEnum, AssemblyRelationship.class, "AssemblyRelationship");
		addEEnumLiteral(assemblyRelationshipEEnum, AssemblyRelationship.PERMANENT);
		addEEnumLiteral(assemblyRelationshipEEnum, AssemblyRelationship.TRANSIENT);

		initEEnum(operationsTypeEEnum, OperationsType.class, "OperationsType");
		addEEnumLiteral(operationsTypeEEnum, OperationsType.PRODUCTION);
		addEEnumLiteral(operationsTypeEEnum, OperationsType.MAINTENANCE);
		addEEnumLiteral(operationsTypeEEnum, OperationsType.QUALITY);
		addEEnumLiteral(operationsTypeEEnum, OperationsType.INVENTORY);
		addEEnumLiteral(operationsTypeEEnum, OperationsType.MIXED);

		initEEnum(materialUseEEnum, MaterialUse.class, "MaterialUse");
		addEEnumLiteral(materialUseEEnum, MaterialUse.CONSUMABLE);
		addEEnumLiteral(materialUseEEnum, MaterialUse.MATERIAL_CONSUMED);
		addEEnumLiteral(materialUseEEnum, MaterialUse.MATERIAL_PRODUCED);

		initEEnum(useTypeEEnum, UseType.class, "UseType");
		addEEnumLiteral(useTypeEEnum, UseType.CONSUMED);
		addEEnumLiteral(useTypeEEnum, UseType.PRODUCED);

		initEEnum(scheduleStateEEnum, ScheduleState.class, "ScheduleState");
		addEEnumLiteral(scheduleStateEEnum, ScheduleState.FORECAST);
		addEEnumLiteral(scheduleStateEEnum, ScheduleState.RELEASED);

		initEEnum(performanceStateEEnum, PerformanceState.class, "PerformanceState");
		addEEnumLiteral(performanceStateEEnum, PerformanceState.READY);
		addEEnumLiteral(performanceStateEEnum, PerformanceState.COMPLETED);
		addEEnumLiteral(performanceStateEEnum, PerformanceState.ABORTED);
		addEEnumLiteral(performanceStateEEnum, PerformanceState.HOLDING);

		initEEnum(capabilityTypeEEnum, CapabilityType.class, "CapabilityType");
		addEEnumLiteral(capabilityTypeEEnum, CapabilityType.USED);
		addEEnumLiteral(capabilityTypeEEnum, CapabilityType.UNUSED);
		addEEnumLiteral(capabilityTypeEEnum, CapabilityType.TOTAL);
		addEEnumLiteral(capabilityTypeEEnum, CapabilityType.AVAILABLE);
		addEEnumLiteral(capabilityTypeEEnum, CapabilityType.UNATTAINABLE);
		addEEnumLiteral(capabilityTypeEEnum, CapabilityType.COMMITTED);

		initEEnum(capacityTypeEEnum, CapacityType.class, "CapacityType");
		addEEnumLiteral(capacityTypeEEnum, CapacityType.AVAILABLE);
		addEEnumLiteral(capacityTypeEEnum, CapacityType.UNATTAINABLE);
		addEEnumLiteral(capacityTypeEEnum, CapacityType.COMMITTED);

		// Initialize data types
		initEDataType(zonedDateTimeEDataType, ZonedDateTime.class, "ZonedDateTime", IS_SERIALIZABLE,
				!IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";
		addAnnotation(this, source, new String[] {});
	}

} //Iec62264PackageImpl
