/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.math.BigDecimal;

import java.time.ZonedDateTime;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Segment Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getSubRequirements <em>Sub Requirements</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getProcessSegment <em>Process Segment</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getOperationsDefinition <em>Operations Definition</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getSegmentParameters <em>Segment Parameters</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getPersonnelRequirements <em>Personnel Requirements</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getEquipmentRequirements <em>Equipment Requirements</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getPhysicalAssetRequirements <em>Physical Asset Requirements</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getMaterialRequirements <em>Material Requirements</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getEarliestStartTime <em>Earliest Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getLatestStartTime <em>Latest Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getDuration <em>Duration</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getDurationUnitOfMeasurement <em>Duration Unit Of Measurement</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getSegmentState <em>Segment State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement()
 * @model
 * @generated
 */
public interface SegmentRequirement extends EObject {
	/**
	 * Returns the value of the '<em><b>Sub Requirements</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Requirements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Requirements</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_SubRequirements()
	 * @model
	 * @generated
	 */
	EList<SegmentRequirement> getSubRequirements();

	/**
	 * Returns the value of the '<em><b>Process Segment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment</em>' reference.
	 * @see #setProcessSegment(ProcessSegment)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_ProcessSegment()
	 * @model required="true"
	 * @generated
	 */
	ProcessSegment getProcessSegment();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getProcessSegment <em>Process Segment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Segment</em>' reference.
	 * @see #getProcessSegment()
	 * @generated
	 */
	void setProcessSegment(ProcessSegment value);

	/**
	 * Returns the value of the '<em><b>Operations Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition</em>' reference.
	 * @see #setOperationsDefinition(OperationsDefinition)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_OperationsDefinition()
	 * @model
	 * @generated
	 */
	OperationsDefinition getOperationsDefinition();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getOperationsDefinition <em>Operations Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Definition</em>' reference.
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	void setOperationsDefinition(OperationsDefinition value);

	/**
	 * Returns the value of the '<em><b>Segment Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.SegmentParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment Parameters</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_SegmentParameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<SegmentParameter> getSegmentParameters();

	/**
	 * Returns the value of the '<em><b>Personnel Requirements</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Requirements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Requirements</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_PersonnelRequirements()
	 * @model containment="true"
	 * @generated
	 */
	EList<PersonnelRequirement> getPersonnelRequirements();

	/**
	 * Returns the value of the '<em><b>Equipment Requirements</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Requirements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Requirements</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_EquipmentRequirements()
	 * @model containment="true"
	 * @generated
	 */
	EList<EquipmentRequirement> getEquipmentRequirements();

	/**
	 * Returns the value of the '<em><b>Physical Asset Requirements</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Requirements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Requirements</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_PhysicalAssetRequirements()
	 * @model containment="true"
	 * @generated
	 */
	EList<PhysicalAssetRequirement> getPhysicalAssetRequirements();

	/**
	 * Returns the value of the '<em><b>Material Requirements</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Requirements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Requirements</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_MaterialRequirements()
	 * @model containment="true"
	 * @generated
	 */
	EList<MaterialRequirement> getMaterialRequirements();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.OperationsType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #setOperationsType(OperationsType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_OperationsType()
	 * @model required="true"
	 * @generated
	 */
	OperationsType getOperationsType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getOperationsType <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsType value);

	/**
	 * Returns the value of the '<em><b>Earliest Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Earliest Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Earliest Start Time</em>' attribute.
	 * @see #setEarliestStartTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_EarliestStartTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getEarliestStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getEarliestStartTime <em>Earliest Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Earliest Start Time</em>' attribute.
	 * @see #getEarliestStartTime()
	 * @generated
	 */
	void setEarliestStartTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Latest Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Latest Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Latest Start Time</em>' attribute.
	 * @see #setLatestStartTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_LatestStartTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getLatestStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getLatestStartTime <em>Latest Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Latest Start Time</em>' attribute.
	 * @see #getLatestStartTime()
	 * @generated
	 */
	void setLatestStartTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(BigDecimal)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_Duration()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 * @generated
	 */
	BigDecimal getDuration();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Duration Unit Of Measurement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration Unit Of Measurement</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration Unit Of Measurement</em>' attribute.
	 * @see #setDurationUnitOfMeasurement(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_DurationUnitOfMeasurement()
	 * @model
	 * @generated
	 */
	String getDurationUnitOfMeasurement();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getDurationUnitOfMeasurement <em>Duration Unit Of Measurement</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration Unit Of Measurement</em>' attribute.
	 * @see #getDurationUnitOfMeasurement()
	 * @generated
	 */
	void setDurationUnitOfMeasurement(String value);

	/**
	 * Returns the value of the '<em><b>Segment State</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.ScheduleState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment State</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.ScheduleState
	 * @see #setSegmentState(ScheduleState)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_SegmentState()
	 * @model
	 * @generated
	 */
	ScheduleState getSegmentState();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getSegmentState <em>Segment State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Segment State</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.ScheduleState
	 * @see #getSegmentState()
	 * @generated
	 */
	void setSegmentState(ScheduleState value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #setHierarchyScope(HierarchyScope)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentRequirement_HierarchyScope()
	 * @model
	 * @generated
	 */
	HierarchyScope getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getHierarchyScope <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScope value);

} // SegmentRequirement
