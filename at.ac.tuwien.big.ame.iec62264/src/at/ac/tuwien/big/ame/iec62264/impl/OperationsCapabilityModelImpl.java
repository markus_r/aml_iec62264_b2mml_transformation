/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.OperationsCapability;
import at.ac.tuwien.big.ame.iec62264.OperationsCapabilityModel;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Capability Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityModelImpl#getOperationsCapabilities <em>Operations Capabilities</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsCapabilityModelImpl extends MinimalEObjectImpl.Container implements OperationsCapabilityModel {
	/**
	 * The cached value of the '{@link #getOperationsCapabilities() <em>Operations Capabilities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsCapabilities()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsCapability> operationsCapabilities;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsCapabilityModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.OPERATIONS_CAPABILITY_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsCapability> getOperationsCapabilities() {
		if (operationsCapabilities == null) {
			operationsCapabilities = new EObjectContainmentEList<OperationsCapability>(OperationsCapability.class, this,
					Iec62264Package.OPERATIONS_CAPABILITY_MODEL__OPERATIONS_CAPABILITIES);
		}
		return operationsCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_CAPABILITY_MODEL__OPERATIONS_CAPABILITIES:
			return ((InternalEList<?>) getOperationsCapabilities()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_CAPABILITY_MODEL__OPERATIONS_CAPABILITIES:
			return getOperationsCapabilities();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_CAPABILITY_MODEL__OPERATIONS_CAPABILITIES:
			getOperationsCapabilities().clear();
			getOperationsCapabilities().addAll((Collection<? extends OperationsCapability>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_CAPABILITY_MODEL__OPERATIONS_CAPABILITIES:
			getOperationsCapabilities().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_CAPABILITY_MODEL__OPERATIONS_CAPABILITIES:
			return operationsCapabilities != null && !operationsCapabilities.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OperationsCapabilityModelImpl
