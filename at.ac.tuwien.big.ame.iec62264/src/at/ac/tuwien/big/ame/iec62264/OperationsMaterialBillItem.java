/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Material Bill Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getMaterialSpecifications <em>Material Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getAssembledFromItems <em>Assembled From Items</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getMaterialClasses <em>Material Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getMaterialDefinitions <em>Material Definitions</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getUseType <em>Use Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getQuantities <em>Quantities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getQuantityUnitOfMeasures <em>Quantity Unit Of Measures</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsMaterialBillItem()
 * @model
 * @generated
 */
public interface OperationsMaterialBillItem extends EObject {
	/**
	 * Returns the value of the '<em><b>Material Specifications</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Specifications</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Specifications</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsMaterialBillItem_MaterialSpecifications()
	 * @model required="true"
	 * @generated
	 */
	EList<MaterialSpecification> getMaterialSpecifications();

	/**
	 * Returns the value of the '<em><b>Assembled From Items</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembled From Items</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembled From Items</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsMaterialBillItem_AssembledFromItems()
	 * @model
	 * @generated
	 */
	EList<OperationsMaterialBillItem> getAssembledFromItems();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsMaterialBillItem_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsMaterialBillItem_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Material Classes</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Classes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Classes</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsMaterialBillItem_MaterialClasses()
	 * @model
	 * @generated
	 */
	EList<MaterialClass> getMaterialClasses();

	/**
	 * Returns the value of the '<em><b>Material Definitions</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Definitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Definitions</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsMaterialBillItem_MaterialDefinitions()
	 * @model
	 * @generated
	 */
	EList<MaterialDefinition> getMaterialDefinitions();

	/**
	 * Returns the value of the '<em><b>Use Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.UseType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.UseType
	 * @see #setUseType(UseType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsMaterialBillItem_UseType()
	 * @model
	 * @generated
	 */
	UseType getUseType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getUseType <em>Use Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.UseType
	 * @see #getUseType()
	 * @generated
	 */
	void setUseType(UseType value);

	/**
	 * Returns the value of the '<em><b>Assembly Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.AssemblyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyType
	 * @see #setAssemblyType(AssemblyType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsMaterialBillItem_AssemblyType()
	 * @model
	 * @generated
	 */
	AssemblyType getAssemblyType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getAssemblyType <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyType
	 * @see #getAssemblyType()
	 * @generated
	 */
	void setAssemblyType(AssemblyType value);

	/**
	 * Returns the value of the '<em><b>Assembly Relationship</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.AssemblyRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Relationship</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Relationship</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyRelationship
	 * @see #setAssemblyRelationship(AssemblyRelationship)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsMaterialBillItem_AssemblyRelationship()
	 * @model
	 * @generated
	 */
	AssemblyRelationship getAssemblyRelationship();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getAssemblyRelationship <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Relationship</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyRelationship
	 * @see #getAssemblyRelationship()
	 * @generated
	 */
	void setAssemblyRelationship(AssemblyRelationship value);

	/**
	 * Returns the value of the '<em><b>Quantities</b></em>' attribute list.
	 * The list contents are of type {@link java.math.BigDecimal}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantities</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantities</em>' attribute list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsMaterialBillItem_Quantities()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 * @generated
	 */
	EList<BigDecimal> getQuantities();

	/**
	 * Returns the value of the '<em><b>Quantity Unit Of Measures</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity Unit Of Measures</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity Unit Of Measures</em>' attribute list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsMaterialBillItem_QuantityUnitOfMeasures()
	 * @model
	 * @generated
	 */
	EList<String> getQuantityUnitOfMeasures();

} // OperationsMaterialBillItem
