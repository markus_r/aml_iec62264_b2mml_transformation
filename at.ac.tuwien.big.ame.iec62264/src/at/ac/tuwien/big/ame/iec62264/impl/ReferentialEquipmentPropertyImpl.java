/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty;
import at.ac.tuwien.big.ame.iec62264.EquipmentProperty;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Referential Equipment Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialEquipmentPropertyImpl#getEquipmentClassProperty <em>Equipment Class Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialEquipmentPropertyImpl#getEquipmentProperty <em>Equipment Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ReferentialEquipmentPropertyImpl extends ReferencialPropertyImpl
		implements ReferentialEquipmentProperty {
	/**
	 * The cached value of the '{@link #getEquipmentClassProperty() <em>Equipment Class Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentClassProperty()
	 * @generated
	 * @ordered
	 */
	protected EquipmentClassProperty equipmentClassProperty;

	/**
	 * The cached value of the '{@link #getEquipmentProperty() <em>Equipment Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentProperty()
	 * @generated
	 * @ordered
	 */
	protected EquipmentProperty equipmentProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferentialEquipmentPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.REFERENTIAL_EQUIPMENT_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClassProperty getEquipmentClassProperty() {
		if (equipmentClassProperty != null && equipmentClassProperty.eIsProxy()) {
			InternalEObject oldEquipmentClassProperty = (InternalEObject) equipmentClassProperty;
			equipmentClassProperty = (EquipmentClassProperty) eResolveProxy(oldEquipmentClassProperty);
			if (equipmentClassProperty != oldEquipmentClassProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY,
							oldEquipmentClassProperty, equipmentClassProperty));
			}
		}
		return equipmentClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClassProperty basicGetEquipmentClassProperty() {
		return equipmentClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentClassProperty(EquipmentClassProperty newEquipmentClassProperty) {
		EquipmentClassProperty oldEquipmentClassProperty = equipmentClassProperty;
		equipmentClassProperty = newEquipmentClassProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY, oldEquipmentClassProperty,
					equipmentClassProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentProperty getEquipmentProperty() {
		if (equipmentProperty != null && equipmentProperty.eIsProxy()) {
			InternalEObject oldEquipmentProperty = (InternalEObject) equipmentProperty;
			equipmentProperty = (EquipmentProperty) eResolveProxy(oldEquipmentProperty);
			if (equipmentProperty != oldEquipmentProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY, oldEquipmentProperty,
							equipmentProperty));
			}
		}
		return equipmentProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentProperty basicGetEquipmentProperty() {
		return equipmentProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentProperty(EquipmentProperty newEquipmentProperty) {
		EquipmentProperty oldEquipmentProperty = equipmentProperty;
		equipmentProperty = newEquipmentProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY, oldEquipmentProperty,
					equipmentProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY:
			if (resolve)
				return getEquipmentClassProperty();
			return basicGetEquipmentClassProperty();
		case Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY:
			if (resolve)
				return getEquipmentProperty();
			return basicGetEquipmentProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY:
			setEquipmentClassProperty((EquipmentClassProperty) newValue);
			return;
		case Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY:
			setEquipmentProperty((EquipmentProperty) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY:
			setEquipmentClassProperty((EquipmentClassProperty) null);
			return;
		case Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY:
			setEquipmentProperty((EquipmentProperty) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY:
			return equipmentClassProperty != null;
		case Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY:
			return equipmentProperty != null;
		}
		return super.eIsSet(featureID);
	}

} //ReferentialEquipmentPropertyImpl
