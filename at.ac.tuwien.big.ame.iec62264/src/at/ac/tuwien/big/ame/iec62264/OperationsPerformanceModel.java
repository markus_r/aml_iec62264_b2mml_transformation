/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Performance Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformanceModel#getOperationsPerformances <em>Operations Performances</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformanceModel()
 * @model
 * @generated
 */
public interface OperationsPerformanceModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Operations Performances</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Performances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Performances</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformanceModel_OperationsPerformances()
	 * @model containment="true"
	 * @generated
	 */
	EList<OperationsPerformance> getOperationsPerformances();

} // OperationsPerformanceModel
