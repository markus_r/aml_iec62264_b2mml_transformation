/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapabilityModel;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process Segment Capability Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentCapabilityModelImpl#getProcessSegmentCapabilities <em>Process Segment Capabilities</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcessSegmentCapabilityModelImpl extends MinimalEObjectImpl.Container
		implements ProcessSegmentCapabilityModel {
	/**
	 * The cached value of the '{@link #getProcessSegmentCapabilities() <em>Process Segment Capabilities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegmentCapabilities()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessSegmentCapability> processSegmentCapabilities;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessSegmentCapabilityModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PROCESS_SEGMENT_CAPABILITY_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessSegmentCapability> getProcessSegmentCapabilities() {
		if (processSegmentCapabilities == null) {
			processSegmentCapabilities = new EObjectContainmentEList<ProcessSegmentCapability>(
					ProcessSegmentCapability.class, this,
					Iec62264Package.PROCESS_SEGMENT_CAPABILITY_MODEL__PROCESS_SEGMENT_CAPABILITIES);
		}
		return processSegmentCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_CAPABILITY_MODEL__PROCESS_SEGMENT_CAPABILITIES:
			return ((InternalEList<?>) getProcessSegmentCapabilities()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_CAPABILITY_MODEL__PROCESS_SEGMENT_CAPABILITIES:
			return getProcessSegmentCapabilities();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_CAPABILITY_MODEL__PROCESS_SEGMENT_CAPABILITIES:
			getProcessSegmentCapabilities().clear();
			getProcessSegmentCapabilities().addAll((Collection<? extends ProcessSegmentCapability>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_CAPABILITY_MODEL__PROCESS_SEGMENT_CAPABILITIES:
			getProcessSegmentCapabilities().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_CAPABILITY_MODEL__PROCESS_SEGMENT_CAPABILITIES:
			return processSegmentCapabilities != null && !processSegmentCapabilities.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ProcessSegmentCapabilityModelImpl
