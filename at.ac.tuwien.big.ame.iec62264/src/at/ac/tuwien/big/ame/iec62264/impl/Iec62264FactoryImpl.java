/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.*;

import java.time.ZonedDateTime;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Iec62264FactoryImpl extends EFactoryImpl implements Iec62264Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Iec62264Factory init() {
		try {
			Iec62264Factory theIec62264Factory = (Iec62264Factory) EPackage.Registry.INSTANCE
					.getEFactory(Iec62264Package.eNS_URI);
			if (theIec62264Factory != null) {
				return theIec62264Factory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Iec62264FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iec62264FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case Iec62264Package.PERSON:
			return createPerson();
		case Iec62264Package.PERSONNEL_CLASS:
			return createPersonnelClass();
		case Iec62264Package.QUALIFICATION_TEST_SPECIFICATION:
			return createQualificationTestSpecification();
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY:
			return createPersonnelClassProperty();
		case Iec62264Package.PERSON_PROPERTY:
			return createPersonProperty();
		case Iec62264Package.QUALIFICATION_TEST_RESULT:
			return createQualificationTestResult();
		case Iec62264Package.PERSONNEL_MODEL:
			return createPersonnelModel();
		case Iec62264Package.EQUIPMENT_MODEL:
			return createEquipmentModel();
		case Iec62264Package.EQUIPMENT_CLASS:
			return createEquipmentClass();
		case Iec62264Package.EQUIPMENT_CAPABILITY_TEST_RESULT:
			return createEquipmentCapabilityTestResult();
		case Iec62264Package.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION:
			return createEquipmentCapabilityTestSpecification();
		case Iec62264Package.EQUIPMENT_CLASS_PROPERTY:
			return createEquipmentClassProperty();
		case Iec62264Package.EQUIPMENT_PROPERTY:
			return createEquipmentProperty();
		case Iec62264Package.EQUIPMENT:
			return createEquipment();
		case Iec62264Package.PHYSICAL_ASSET_MODEL:
			return createPhysicalAssetModel();
		case Iec62264Package.PHYSICAL_ASSET_CLASS:
			return createPhysicalAssetClass();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT:
			return createPhysicalAssetCapabilityTestResult();
		case Iec62264Package.PHYSICAL_ASSET_PROPERTY:
			return createPhysicalAssetProperty();
		case Iec62264Package.PHYSICAL_ASSET:
			return createPhysicalAsset();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION:
			return createPhysicalAssetCapabilityTestSpecification();
		case Iec62264Package.PHYSICAL_ASSET_CLASS_PROPERTY:
			return createPhysicalAssetClassProperty();
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING:
			return createEquipmentAssetMapping();
		case Iec62264Package.MATERIAL_MODEL:
			return createMaterialModel();
		case Iec62264Package.MATERIAL_CLASS:
			return createMaterialClass();
		case Iec62264Package.MATERIAL_TEST_RESULT:
			return createMaterialTestResult();
		case Iec62264Package.MATERIAL_TEST_SPECIFICATION:
			return createMaterialTestSpecification();
		case Iec62264Package.MATERIAL_SUBLOT:
			return createMaterialSublot();
		case Iec62264Package.MATERIAL_LOT:
			return createMaterialLot();
		case Iec62264Package.MATERIAL_LOT_PROPERTY:
			return createMaterialLotProperty();
		case Iec62264Package.MATERIAL_DEFINITION:
			return createMaterialDefinition();
		case Iec62264Package.MATERIAL_DEFINITION_PROPERTY:
			return createMaterialDefinitionProperty();
		case Iec62264Package.MATERIAL_CLASS_PROPERTY:
			return createMaterialClassProperty();
		case Iec62264Package.PROCESS_SEGMENT_MODEL:
			return createProcessSegmentModel();
		case Iec62264Package.PROCESS_SEGMENT:
			return createProcessSegment();
		case Iec62264Package.PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY:
			return createPersonnelSegmentSpecificationProperty();
		case Iec62264Package.EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY:
			return createEquipmentSegmentSpecificationProperty();
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY:
			return createPhysicalAssetSegmentSpecificationProperty();
		case Iec62264Package.MATERIAL_SEGMENT_SPECIFICATION_PROPERTY:
			return createMaterialSegmentSpecificationProperty();
		case Iec62264Package.MATERIAL_SEGMENT_SPECIFICATION:
			return createMaterialSegmentSpecification();
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION:
			return createPhysicalAssetSegmentSpecification();
		case Iec62264Package.EQUIPMENT_SEGMENT_SPECIFICATION:
			return createEquipmentSegmentSpecification();
		case Iec62264Package.PERSONNEL_SEGMENT_SPECIFICATION:
			return createPersonnelSegmentSpecification();
		case Iec62264Package.PROCESS_SEGMENT_PARAMETER:
			return createProcessSegmentParameter();
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY:
			return createProcessSegmentDependency();
		case Iec62264Package.IEC62264_MODEL:
			return createIec62264Model();
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL:
			return createOperationsDefinitionModel();
		case Iec62264Package.OPERATIONS_SCHEDULE_MODEL:
			return createOperationsScheduleModel();
		case Iec62264Package.OPERATIONS_PERFORMANCE_MODEL:
			return createOperationsPerformanceModel();
		case Iec62264Package.OPERATIONS_CAPABILITY_MODEL:
			return createOperationsCapabilityModel();
		case Iec62264Package.PROCESS_SEGMENT_CAPABILITY_MODEL:
			return createProcessSegmentCapabilityModel();
		case Iec62264Package.HIERARCHY_SCOPE:
			return createHierarchyScope();
		case Iec62264Package.MATERIAL_SPECIFICATION_PROPERTY:
			return createMaterialSpecificationProperty();
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION_PROPERTY:
			return createPhysicalAssetSpecificationProperty();
		case Iec62264Package.EQUIPMENT_SPECIFICATION_PROPERTY:
			return createEquipmentSpecificationProperty();
		case Iec62264Package.PERSONNEL_SPECIFICATION_PROPERTY:
			return createPersonnelSpecificationProperty();
		case Iec62264Package.MATERIAL_SPECIFICATION:
			return createMaterialSpecification();
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION:
			return createPhysicalAssetSpecification();
		case Iec62264Package.EQUIPMENT_SPECIFICATION:
			return createEquipmentSpecification();
		case Iec62264Package.PERSONNEL_SPECIFICATION:
			return createPersonnelSpecification();
		case Iec62264Package.PARAMETER_SPECIFICATION:
			return createParameterSpecification();
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY:
			return createOperationsSegmentDependency();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM:
			return createOperationsMaterialBillItem();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL:
			return createOperationsMaterialBill();
		case Iec62264Package.OPERATIONS_SEGMENT:
			return createOperationsSegment();
		case Iec62264Package.OPERATIONS_DEFINITION:
			return createOperationsDefinition();
		case Iec62264Package.MATERIAL_REQUIREMENT:
			return createMaterialRequirement();
		case Iec62264Package.MATERIAL_REQUIREMENT_PROPERTY:
			return createMaterialRequirementProperty();
		case Iec62264Package.PHYSICAL_ASSET_REQUIREMENT_PROPERTY:
			return createPhysicalAssetRequirementProperty();
		case Iec62264Package.EQUIPMENT_REQUIREMENT_PROPERTY:
			return createEquipmentRequirementProperty();
		case Iec62264Package.PERSONNEL_REQUIREMENT_PROPERTY:
			return createPersonnelRequirementProperty();
		case Iec62264Package.OPERATIONS_SCHEDULE:
			return createOperationsSchedule();
		case Iec62264Package.OPERATIONS_REQUEST:
			return createOperationsRequest();
		case Iec62264Package.REQUESTED_SEGMENT_RESPONSE:
			return createRequestedSegmentResponse();
		case Iec62264Package.SEGMENT_REQUIREMENT:
			return createSegmentRequirement();
		case Iec62264Package.EQUIPMENT_REQUIREMENT:
			return createEquipmentRequirement();
		case Iec62264Package.PHYSICAL_ASSET_REQUIREMENT:
			return createPhysicalAssetRequirement();
		case Iec62264Package.PERSONNEL_REQUIREMENT:
			return createPersonnelRequirement();
		case Iec62264Package.SEGMENT_PARAMETER:
			return createSegmentParameter();
		case Iec62264Package.MATERIAL_ACTUAL_PROPERTY:
			return createMaterialActualProperty();
		case Iec62264Package.PHYSICAL_ASSET_ACTUAL_PROPERTY:
			return createPhysicalAssetActualProperty();
		case Iec62264Package.EQUIPMENT_ACTUAL_PROPERTY:
			return createEquipmentActualProperty();
		case Iec62264Package.PERSONNEL_ACTUAL_PROPERTY:
			return createPersonnelActualProperty();
		case Iec62264Package.MATERIAL_ACTUAL:
			return createMaterialActual();
		case Iec62264Package.PHYSICAL_ASSET_ACTUAL:
			return createPhysicalAssetActual();
		case Iec62264Package.EQUIPMENT_ACTUAL:
			return createEquipmentActual();
		case Iec62264Package.PERSONNEL_ACTUAL:
			return createPersonnelActual();
		case Iec62264Package.SEGMENT_DATA:
			return createSegmentData();
		case Iec62264Package.SEGMENT_RESPONSE:
			return createSegmentResponse();
		case Iec62264Package.OPERATIONS_RESPONSE:
			return createOperationsResponse();
		case Iec62264Package.OPERATIONS_PERFORMANCE:
			return createOperationsPerformance();
		case Iec62264Package.OPERATIONS_CAPABILITY:
			return createOperationsCapability();
		case Iec62264Package.MATERIAL_CAPABILITY_PROPERTY:
			return createMaterialCapabilityProperty();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_PROPERTY:
			return createPhysicalAssetCapabilityProperty();
		case Iec62264Package.EQUIPMENT_CAPABILITY_PROPERTY:
			return createEquipmentCapabilityProperty();
		case Iec62264Package.PERSONNEL_CAPABILITY_PROPERTY:
			return createPersonnelCapabilityProperty();
		case Iec62264Package.MATERIAL_CAPABILITY:
			return createMaterialCapability();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY:
			return createPhysicalAssetCapability();
		case Iec62264Package.EQUIPMENT_CAPABILITY:
			return createEquipmentCapability();
		case Iec62264Package.PERSONNEL_CAPABILITY:
			return createPersonnelCapability();
		case Iec62264Package.PROCESS_SEGMENT_CAPABILITY:
			return createProcessSegmentCapability();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case Iec62264Package.ASSEMBLY_TYPE:
			return createAssemblyTypeFromString(eDataType, initialValue);
		case Iec62264Package.ASSEMBLY_RELATIONSHIP:
			return createAssemblyRelationshipFromString(eDataType, initialValue);
		case Iec62264Package.OPERATIONS_TYPE:
			return createOperationsTypeFromString(eDataType, initialValue);
		case Iec62264Package.MATERIAL_USE:
			return createMaterialUseFromString(eDataType, initialValue);
		case Iec62264Package.USE_TYPE:
			return createUseTypeFromString(eDataType, initialValue);
		case Iec62264Package.SCHEDULE_STATE:
			return createScheduleStateFromString(eDataType, initialValue);
		case Iec62264Package.PERFORMANCE_STATE:
			return createPerformanceStateFromString(eDataType, initialValue);
		case Iec62264Package.CAPABILITY_TYPE:
			return createCapabilityTypeFromString(eDataType, initialValue);
		case Iec62264Package.CAPACITY_TYPE:
			return createCapacityTypeFromString(eDataType, initialValue);
		case Iec62264Package.ZONED_DATE_TIME:
			return createZonedDateTimeFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case Iec62264Package.ASSEMBLY_TYPE:
			return convertAssemblyTypeToString(eDataType, instanceValue);
		case Iec62264Package.ASSEMBLY_RELATIONSHIP:
			return convertAssemblyRelationshipToString(eDataType, instanceValue);
		case Iec62264Package.OPERATIONS_TYPE:
			return convertOperationsTypeToString(eDataType, instanceValue);
		case Iec62264Package.MATERIAL_USE:
			return convertMaterialUseToString(eDataType, instanceValue);
		case Iec62264Package.USE_TYPE:
			return convertUseTypeToString(eDataType, instanceValue);
		case Iec62264Package.SCHEDULE_STATE:
			return convertScheduleStateToString(eDataType, instanceValue);
		case Iec62264Package.PERFORMANCE_STATE:
			return convertPerformanceStateToString(eDataType, instanceValue);
		case Iec62264Package.CAPABILITY_TYPE:
			return convertCapabilityTypeToString(eDataType, instanceValue);
		case Iec62264Package.CAPACITY_TYPE:
			return convertCapacityTypeToString(eDataType, instanceValue);
		case Iec62264Package.ZONED_DATE_TIME:
			return convertZonedDateTimeToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Person createPerson() {
		PersonImpl person = new PersonImpl();
		return person;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelClass createPersonnelClass() {
		PersonnelClassImpl personnelClass = new PersonnelClassImpl();
		return personnelClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualificationTestSpecification createQualificationTestSpecification() {
		QualificationTestSpecificationImpl qualificationTestSpecification = new QualificationTestSpecificationImpl();
		return qualificationTestSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelClassProperty createPersonnelClassProperty() {
		PersonnelClassPropertyImpl personnelClassProperty = new PersonnelClassPropertyImpl();
		return personnelClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonProperty createPersonProperty() {
		PersonPropertyImpl personProperty = new PersonPropertyImpl();
		return personProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualificationTestResult createQualificationTestResult() {
		QualificationTestResultImpl qualificationTestResult = new QualificationTestResultImpl();
		return qualificationTestResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelModel createPersonnelModel() {
		PersonnelModelImpl personnelModel = new PersonnelModelImpl();
		return personnelModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentModel createEquipmentModel() {
		EquipmentModelImpl equipmentModel = new EquipmentModelImpl();
		return equipmentModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClass createEquipmentClass() {
		EquipmentClassImpl equipmentClass = new EquipmentClassImpl();
		return equipmentClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentCapabilityTestResult createEquipmentCapabilityTestResult() {
		EquipmentCapabilityTestResultImpl equipmentCapabilityTestResult = new EquipmentCapabilityTestResultImpl();
		return equipmentCapabilityTestResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentCapabilityTestSpecification createEquipmentCapabilityTestSpecification() {
		EquipmentCapabilityTestSpecificationImpl equipmentCapabilityTestSpecification = new EquipmentCapabilityTestSpecificationImpl();
		return equipmentCapabilityTestSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClassProperty createEquipmentClassProperty() {
		EquipmentClassPropertyImpl equipmentClassProperty = new EquipmentClassPropertyImpl();
		return equipmentClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentProperty createEquipmentProperty() {
		EquipmentPropertyImpl equipmentProperty = new EquipmentPropertyImpl();
		return equipmentProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Equipment createEquipment() {
		EquipmentImpl equipment = new EquipmentImpl();
		return equipment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetModel createPhysicalAssetModel() {
		PhysicalAssetModelImpl physicalAssetModel = new PhysicalAssetModelImpl();
		return physicalAssetModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClass createPhysicalAssetClass() {
		PhysicalAssetClassImpl physicalAssetClass = new PhysicalAssetClassImpl();
		return physicalAssetClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetCapabilityTestResult createPhysicalAssetCapabilityTestResult() {
		PhysicalAssetCapabilityTestResultImpl physicalAssetCapabilityTestResult = new PhysicalAssetCapabilityTestResultImpl();
		return physicalAssetCapabilityTestResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetProperty createPhysicalAssetProperty() {
		PhysicalAssetPropertyImpl physicalAssetProperty = new PhysicalAssetPropertyImpl();
		return physicalAssetProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAsset createPhysicalAsset() {
		PhysicalAssetImpl physicalAsset = new PhysicalAssetImpl();
		return physicalAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetCapabilityTestSpecification createPhysicalAssetCapabilityTestSpecification() {
		PhysicalAssetCapabilityTestSpecificationImpl physicalAssetCapabilityTestSpecification = new PhysicalAssetCapabilityTestSpecificationImpl();
		return physicalAssetCapabilityTestSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClassProperty createPhysicalAssetClassProperty() {
		PhysicalAssetClassPropertyImpl physicalAssetClassProperty = new PhysicalAssetClassPropertyImpl();
		return physicalAssetClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentAssetMapping createEquipmentAssetMapping() {
		EquipmentAssetMappingImpl equipmentAssetMapping = new EquipmentAssetMappingImpl();
		return equipmentAssetMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialModel createMaterialModel() {
		MaterialModelImpl materialModel = new MaterialModelImpl();
		return materialModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialClass createMaterialClass() {
		MaterialClassImpl materialClass = new MaterialClassImpl();
		return materialClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialTestResult createMaterialTestResult() {
		MaterialTestResultImpl materialTestResult = new MaterialTestResultImpl();
		return materialTestResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialTestSpecification createMaterialTestSpecification() {
		MaterialTestSpecificationImpl materialTestSpecification = new MaterialTestSpecificationImpl();
		return materialTestSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSublot createMaterialSublot() {
		MaterialSublotImpl materialSublot = new MaterialSublotImpl();
		return materialSublot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialLot createMaterialLot() {
		MaterialLotImpl materialLot = new MaterialLotImpl();
		return materialLot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialLotProperty createMaterialLotProperty() {
		MaterialLotPropertyImpl materialLotProperty = new MaterialLotPropertyImpl();
		return materialLotProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialDefinition createMaterialDefinition() {
		MaterialDefinitionImpl materialDefinition = new MaterialDefinitionImpl();
		return materialDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialDefinitionProperty createMaterialDefinitionProperty() {
		MaterialDefinitionPropertyImpl materialDefinitionProperty = new MaterialDefinitionPropertyImpl();
		return materialDefinitionProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialClassProperty createMaterialClassProperty() {
		MaterialClassPropertyImpl materialClassProperty = new MaterialClassPropertyImpl();
		return materialClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentModel createProcessSegmentModel() {
		ProcessSegmentModelImpl processSegmentModel = new ProcessSegmentModelImpl();
		return processSegmentModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegment createProcessSegment() {
		ProcessSegmentImpl processSegment = new ProcessSegmentImpl();
		return processSegment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelSegmentSpecificationProperty createPersonnelSegmentSpecificationProperty() {
		PersonnelSegmentSpecificationPropertyImpl personnelSegmentSpecificationProperty = new PersonnelSegmentSpecificationPropertyImpl();
		return personnelSegmentSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentSegmentSpecificationProperty createEquipmentSegmentSpecificationProperty() {
		EquipmentSegmentSpecificationPropertyImpl equipmentSegmentSpecificationProperty = new EquipmentSegmentSpecificationPropertyImpl();
		return equipmentSegmentSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetSegmentSpecificationProperty createPhysicalAssetSegmentSpecificationProperty() {
		PhysicalAssetSegmentSpecificationPropertyImpl physicalAssetSegmentSpecificationProperty = new PhysicalAssetSegmentSpecificationPropertyImpl();
		return physicalAssetSegmentSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSegmentSpecificationProperty createMaterialSegmentSpecificationProperty() {
		MaterialSegmentSpecificationPropertyImpl materialSegmentSpecificationProperty = new MaterialSegmentSpecificationPropertyImpl();
		return materialSegmentSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSegmentSpecification createMaterialSegmentSpecification() {
		MaterialSegmentSpecificationImpl materialSegmentSpecification = new MaterialSegmentSpecificationImpl();
		return materialSegmentSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetSegmentSpecification createPhysicalAssetSegmentSpecification() {
		PhysicalAssetSegmentSpecificationImpl physicalAssetSegmentSpecification = new PhysicalAssetSegmentSpecificationImpl();
		return physicalAssetSegmentSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentSegmentSpecification createEquipmentSegmentSpecification() {
		EquipmentSegmentSpecificationImpl equipmentSegmentSpecification = new EquipmentSegmentSpecificationImpl();
		return equipmentSegmentSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelSegmentSpecification createPersonnelSegmentSpecification() {
		PersonnelSegmentSpecificationImpl personnelSegmentSpecification = new PersonnelSegmentSpecificationImpl();
		return personnelSegmentSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentParameter createProcessSegmentParameter() {
		ProcessSegmentParameterImpl processSegmentParameter = new ProcessSegmentParameterImpl();
		return processSegmentParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentDependency createProcessSegmentDependency() {
		ProcessSegmentDependencyImpl processSegmentDependency = new ProcessSegmentDependencyImpl();
		return processSegmentDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iec62264Model createIec62264Model() {
		Iec62264ModelImpl iec62264Model = new Iec62264ModelImpl();
		return iec62264Model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinitionModel createOperationsDefinitionModel() {
		OperationsDefinitionModelImpl operationsDefinitionModel = new OperationsDefinitionModelImpl();
		return operationsDefinitionModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsScheduleModel createOperationsScheduleModel() {
		OperationsScheduleModelImpl operationsScheduleModel = new OperationsScheduleModelImpl();
		return operationsScheduleModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsPerformanceModel createOperationsPerformanceModel() {
		OperationsPerformanceModelImpl operationsPerformanceModel = new OperationsPerformanceModelImpl();
		return operationsPerformanceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsCapabilityModel createOperationsCapabilityModel() {
		OperationsCapabilityModelImpl operationsCapabilityModel = new OperationsCapabilityModelImpl();
		return operationsCapabilityModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentCapabilityModel createProcessSegmentCapabilityModel() {
		ProcessSegmentCapabilityModelImpl processSegmentCapabilityModel = new ProcessSegmentCapabilityModelImpl();
		return processSegmentCapabilityModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope createHierarchyScope() {
		HierarchyScopeImpl hierarchyScope = new HierarchyScopeImpl();
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSpecificationProperty createMaterialSpecificationProperty() {
		MaterialSpecificationPropertyImpl materialSpecificationProperty = new MaterialSpecificationPropertyImpl();
		return materialSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetSpecificationProperty createPhysicalAssetSpecificationProperty() {
		PhysicalAssetSpecificationPropertyImpl physicalAssetSpecificationProperty = new PhysicalAssetSpecificationPropertyImpl();
		return physicalAssetSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentSpecificationProperty createEquipmentSpecificationProperty() {
		EquipmentSpecificationPropertyImpl equipmentSpecificationProperty = new EquipmentSpecificationPropertyImpl();
		return equipmentSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelSpecificationProperty createPersonnelSpecificationProperty() {
		PersonnelSpecificationPropertyImpl personnelSpecificationProperty = new PersonnelSpecificationPropertyImpl();
		return personnelSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSpecification createMaterialSpecification() {
		MaterialSpecificationImpl materialSpecification = new MaterialSpecificationImpl();
		return materialSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetSpecification createPhysicalAssetSpecification() {
		PhysicalAssetSpecificationImpl physicalAssetSpecification = new PhysicalAssetSpecificationImpl();
		return physicalAssetSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentSpecification createEquipmentSpecification() {
		EquipmentSpecificationImpl equipmentSpecification = new EquipmentSpecificationImpl();
		return equipmentSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelSpecification createPersonnelSpecification() {
		PersonnelSpecificationImpl personnelSpecification = new PersonnelSpecificationImpl();
		return personnelSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSpecification createParameterSpecification() {
		ParameterSpecificationImpl parameterSpecification = new ParameterSpecificationImpl();
		return parameterSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsSegmentDependency createOperationsSegmentDependency() {
		OperationsSegmentDependencyImpl operationsSegmentDependency = new OperationsSegmentDependencyImpl();
		return operationsSegmentDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsMaterialBillItem createOperationsMaterialBillItem() {
		OperationsMaterialBillItemImpl operationsMaterialBillItem = new OperationsMaterialBillItemImpl();
		return operationsMaterialBillItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsMaterialBill createOperationsMaterialBill() {
		OperationsMaterialBillImpl operationsMaterialBill = new OperationsMaterialBillImpl();
		return operationsMaterialBill;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsSegment createOperationsSegment() {
		OperationsSegmentImpl operationsSegment = new OperationsSegmentImpl();
		return operationsSegment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinition createOperationsDefinition() {
		OperationsDefinitionImpl operationsDefinition = new OperationsDefinitionImpl();
		return operationsDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialRequirement createMaterialRequirement() {
		MaterialRequirementImpl materialRequirement = new MaterialRequirementImpl();
		return materialRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialRequirementProperty createMaterialRequirementProperty() {
		MaterialRequirementPropertyImpl materialRequirementProperty = new MaterialRequirementPropertyImpl();
		return materialRequirementProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetRequirementProperty createPhysicalAssetRequirementProperty() {
		PhysicalAssetRequirementPropertyImpl physicalAssetRequirementProperty = new PhysicalAssetRequirementPropertyImpl();
		return physicalAssetRequirementProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentRequirementProperty createEquipmentRequirementProperty() {
		EquipmentRequirementPropertyImpl equipmentRequirementProperty = new EquipmentRequirementPropertyImpl();
		return equipmentRequirementProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelRequirementProperty createPersonnelRequirementProperty() {
		PersonnelRequirementPropertyImpl personnelRequirementProperty = new PersonnelRequirementPropertyImpl();
		return personnelRequirementProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsSchedule createOperationsSchedule() {
		OperationsScheduleImpl operationsSchedule = new OperationsScheduleImpl();
		return operationsSchedule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsRequest createOperationsRequest() {
		OperationsRequestImpl operationsRequest = new OperationsRequestImpl();
		return operationsRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequestedSegmentResponse createRequestedSegmentResponse() {
		RequestedSegmentResponseImpl requestedSegmentResponse = new RequestedSegmentResponseImpl();
		return requestedSegmentResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SegmentRequirement createSegmentRequirement() {
		SegmentRequirementImpl segmentRequirement = new SegmentRequirementImpl();
		return segmentRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentRequirement createEquipmentRequirement() {
		EquipmentRequirementImpl equipmentRequirement = new EquipmentRequirementImpl();
		return equipmentRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetRequirement createPhysicalAssetRequirement() {
		PhysicalAssetRequirementImpl physicalAssetRequirement = new PhysicalAssetRequirementImpl();
		return physicalAssetRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelRequirement createPersonnelRequirement() {
		PersonnelRequirementImpl personnelRequirement = new PersonnelRequirementImpl();
		return personnelRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SegmentParameter createSegmentParameter() {
		SegmentParameterImpl segmentParameter = new SegmentParameterImpl();
		return segmentParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialActualProperty createMaterialActualProperty() {
		MaterialActualPropertyImpl materialActualProperty = new MaterialActualPropertyImpl();
		return materialActualProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetActualProperty createPhysicalAssetActualProperty() {
		PhysicalAssetActualPropertyImpl physicalAssetActualProperty = new PhysicalAssetActualPropertyImpl();
		return physicalAssetActualProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentActualProperty createEquipmentActualProperty() {
		EquipmentActualPropertyImpl equipmentActualProperty = new EquipmentActualPropertyImpl();
		return equipmentActualProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelActualProperty createPersonnelActualProperty() {
		PersonnelActualPropertyImpl personnelActualProperty = new PersonnelActualPropertyImpl();
		return personnelActualProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialActual createMaterialActual() {
		MaterialActualImpl materialActual = new MaterialActualImpl();
		return materialActual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetActual createPhysicalAssetActual() {
		PhysicalAssetActualImpl physicalAssetActual = new PhysicalAssetActualImpl();
		return physicalAssetActual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentActual createEquipmentActual() {
		EquipmentActualImpl equipmentActual = new EquipmentActualImpl();
		return equipmentActual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelActual createPersonnelActual() {
		PersonnelActualImpl personnelActual = new PersonnelActualImpl();
		return personnelActual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SegmentData createSegmentData() {
		SegmentDataImpl segmentData = new SegmentDataImpl();
		return segmentData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SegmentResponse createSegmentResponse() {
		SegmentResponseImpl segmentResponse = new SegmentResponseImpl();
		return segmentResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsResponse createOperationsResponse() {
		OperationsResponseImpl operationsResponse = new OperationsResponseImpl();
		return operationsResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsPerformance createOperationsPerformance() {
		OperationsPerformanceImpl operationsPerformance = new OperationsPerformanceImpl();
		return operationsPerformance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsCapability createOperationsCapability() {
		OperationsCapabilityImpl operationsCapability = new OperationsCapabilityImpl();
		return operationsCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialCapabilityProperty createMaterialCapabilityProperty() {
		MaterialCapabilityPropertyImpl materialCapabilityProperty = new MaterialCapabilityPropertyImpl();
		return materialCapabilityProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetCapabilityProperty createPhysicalAssetCapabilityProperty() {
		PhysicalAssetCapabilityPropertyImpl physicalAssetCapabilityProperty = new PhysicalAssetCapabilityPropertyImpl();
		return physicalAssetCapabilityProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentCapabilityProperty createEquipmentCapabilityProperty() {
		EquipmentCapabilityPropertyImpl equipmentCapabilityProperty = new EquipmentCapabilityPropertyImpl();
		return equipmentCapabilityProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelCapabilityProperty createPersonnelCapabilityProperty() {
		PersonnelCapabilityPropertyImpl personnelCapabilityProperty = new PersonnelCapabilityPropertyImpl();
		return personnelCapabilityProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialCapability createMaterialCapability() {
		MaterialCapabilityImpl materialCapability = new MaterialCapabilityImpl();
		return materialCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetCapability createPhysicalAssetCapability() {
		PhysicalAssetCapabilityImpl physicalAssetCapability = new PhysicalAssetCapabilityImpl();
		return physicalAssetCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentCapability createEquipmentCapability() {
		EquipmentCapabilityImpl equipmentCapability = new EquipmentCapabilityImpl();
		return equipmentCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelCapability createPersonnelCapability() {
		PersonnelCapabilityImpl personnelCapability = new PersonnelCapabilityImpl();
		return personnelCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentCapability createProcessSegmentCapability() {
		ProcessSegmentCapabilityImpl processSegmentCapability = new ProcessSegmentCapabilityImpl();
		return processSegmentCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyType createAssemblyTypeFromString(EDataType eDataType, String initialValue) {
		AssemblyType result = AssemblyType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssemblyTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationship createAssemblyRelationshipFromString(EDataType eDataType, String initialValue) {
		AssemblyRelationship result = AssemblyRelationship.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssemblyRelationshipToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsType createOperationsTypeFromString(EDataType eDataType, String initialValue) {
		OperationsType result = OperationsType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperationsTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialUse createMaterialUseFromString(EDataType eDataType, String initialValue) {
		MaterialUse result = MaterialUse.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMaterialUseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseType createUseTypeFromString(EDataType eDataType, String initialValue) {
		UseType result = UseType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUseTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScheduleState createScheduleStateFromString(EDataType eDataType, String initialValue) {
		ScheduleState result = ScheduleState.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertScheduleStateToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerformanceState createPerformanceStateFromString(EDataType eDataType, String initialValue) {
		PerformanceState result = PerformanceState.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPerformanceStateToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityType createCapabilityTypeFromString(EDataType eDataType, String initialValue) {
		CapabilityType result = CapabilityType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCapabilityTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapacityType createCapacityTypeFromString(EDataType eDataType, String initialValue) {
		CapacityType result = CapacityType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCapacityTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ZonedDateTime createZonedDateTimeFromString(EDataType eDataType, String initialValue) {
		try {
			return (ZonedDateTime) super.createFromString(eDataType, initialValue);
		} catch (IllegalArgumentException e) {
			return ZonedDateTime.parse(initialValue);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertZonedDateTimeToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iec62264Package getIec62264Package() {
		return (Iec62264Package) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Iec62264Package getPackage() {
		return Iec62264Package.eINSTANCE;
	}

} //Iec62264FactoryImpl
