/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Segment Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getAssembledFromSpecifications <em>Assembled From Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getProperties <em>Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getMaterialClasses <em>Material Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getMaterialDefinitions <em>Material Definitions</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getQuantityUnitOfMeasurement <em>Quantity Unit Of Measurement</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSegmentSpecification()
 * @model
 * @generated
 */
public interface MaterialSegmentSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Assembled From Specifications</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembled From Specifications</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembled From Specifications</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSegmentSpecification_AssembledFromSpecifications()
	 * @model
	 * @generated
	 */
	EList<MaterialSegmentSpecification> getAssembledFromSpecifications();

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecificationProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSegmentSpecification_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<MaterialSegmentSpecificationProperty> getProperties();

	/**
	 * Returns the value of the '<em><b>Material Classes</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Classes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Classes</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSegmentSpecification_MaterialClasses()
	 * @model
	 * @generated
	 */
	EList<MaterialClass> getMaterialClasses();

	/**
	 * Returns the value of the '<em><b>Material Definitions</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Definitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Definitions</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSegmentSpecification_MaterialDefinitions()
	 * @model
	 * @generated
	 */
	EList<MaterialDefinition> getMaterialDefinitions();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSegmentSpecification_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Assembly Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.AssemblyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyType
	 * @see #setAssemblyType(AssemblyType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSegmentSpecification_AssemblyType()
	 * @model
	 * @generated
	 */
	AssemblyType getAssemblyType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getAssemblyType <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyType
	 * @see #getAssemblyType()
	 * @generated
	 */
	void setAssemblyType(AssemblyType value);

	/**
	 * Returns the value of the '<em><b>Assembly Relationship</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.AssemblyRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Relationship</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Relationship</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyRelationship
	 * @see #setAssemblyRelationship(AssemblyRelationship)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSegmentSpecification_AssemblyRelationship()
	 * @model
	 * @generated
	 */
	AssemblyRelationship getAssemblyRelationship();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getAssemblyRelationship <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Relationship</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyRelationship
	 * @see #getAssemblyRelationship()
	 * @generated
	 */
	void setAssemblyRelationship(AssemblyRelationship value);

	/**
	 * Returns the value of the '<em><b>Material Use</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.MaterialUse}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Use</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Use</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialUse
	 * @see #setMaterialUse(MaterialUse)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSegmentSpecification_MaterialUse()
	 * @model
	 * @generated
	 */
	MaterialUse getMaterialUse();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getMaterialUse <em>Material Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Use</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialUse
	 * @see #getMaterialUse()
	 * @generated
	 */
	void setMaterialUse(MaterialUse value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(BigDecimal)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSegmentSpecification_Quantity()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 * @generated
	 */
	BigDecimal getQuantity();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getQuantity <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Quantity Unit Of Measurement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity Unit Of Measurement</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity Unit Of Measurement</em>' attribute.
	 * @see #setQuantityUnitOfMeasurement(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialSegmentSpecification_QuantityUnitOfMeasurement()
	 * @model
	 * @generated
	 */
	String getQuantityUnitOfMeasurement();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getQuantityUnitOfMeasurement <em>Quantity Unit Of Measurement</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity Unit Of Measurement</em>' attribute.
	 * @see #getQuantityUnitOfMeasurement()
	 * @generated
	 */
	void setQuantityUnitOfMeasurement(String value);

} // MaterialSegmentSpecification
