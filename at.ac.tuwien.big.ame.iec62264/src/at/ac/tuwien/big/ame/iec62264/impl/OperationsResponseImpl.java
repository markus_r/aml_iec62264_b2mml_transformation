/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.OperationsDefinition;
import at.ac.tuwien.big.ame.iec62264.OperationsRequest;
import at.ac.tuwien.big.ame.iec62264.OperationsResponse;
import at.ac.tuwien.big.ame.iec62264.OperationsType;
import at.ac.tuwien.big.ame.iec62264.PerformanceState;
import at.ac.tuwien.big.ame.iec62264.SegmentResponse;

import java.time.ZonedDateTime;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Response</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl#getSegmentResponses <em>Segment Responses</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl#getResponseState <em>Response State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl#getOperationsRequest <em>Operations Request</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl#getOperationsDefinition <em>Operations Definition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsResponseImpl extends MinimalEObjectImpl.Container implements OperationsResponse {
	/**
	 * The cached value of the '{@link #getSegmentResponses() <em>Segment Responses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentResponses()
	 * @generated
	 * @ordered
	 */
	protected EList<SegmentResponse> segmentResponses;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected static final OperationsType OPERATIONS_TYPE_EDEFAULT = OperationsType.PRODUCTION;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsType operationsType = OPERATIONS_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime startTime = START_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime END_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime endTime = END_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getResponseState() <em>Response State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseState()
	 * @generated
	 * @ordered
	 */
	protected static final PerformanceState RESPONSE_STATE_EDEFAULT = PerformanceState.READY;

	/**
	 * The cached value of the '{@link #getResponseState() <em>Response State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseState()
	 * @generated
	 * @ordered
	 */
	protected PerformanceState responseState = RESPONSE_STATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOperationsRequest() <em>Operations Request</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsRequest()
	 * @generated
	 * @ordered
	 */
	protected OperationsRequest operationsRequest;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScope hierarchyScope;

	/**
	 * The cached value of the '{@link #getOperationsDefinition() <em>Operations Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsDefinition()
	 * @generated
	 * @ordered
	 */
	protected OperationsDefinition operationsDefinition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsResponseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.OPERATIONS_RESPONSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SegmentResponse> getSegmentResponses() {
		if (segmentResponses == null) {
			segmentResponses = new EObjectContainmentEList<SegmentResponse>(SegmentResponse.class, this,
					Iec62264Package.OPERATIONS_RESPONSE__SEGMENT_RESPONSES);
		}
		return segmentResponses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_RESPONSE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_RESPONSE__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsType newOperationsType) {
		OperationsType oldOperationsType = operationsType;
		operationsType = newOperationsType == null ? OPERATIONS_TYPE_EDEFAULT : newOperationsType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_TYPE,
					oldOperationsType, operationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(ZonedDateTime newStartTime) {
		ZonedDateTime oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_RESPONSE__START_TIME,
					oldStartTime, startTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(ZonedDateTime newEndTime) {
		ZonedDateTime oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_RESPONSE__END_TIME,
					oldEndTime, endTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerformanceState getResponseState() {
		return responseState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponseState(PerformanceState newResponseState) {
		PerformanceState oldResponseState = responseState;
		responseState = newResponseState == null ? RESPONSE_STATE_EDEFAULT : newResponseState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_RESPONSE__RESPONSE_STATE,
					oldResponseState, responseState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsRequest getOperationsRequest() {
		if (operationsRequest != null && operationsRequest.eIsProxy()) {
			InternalEObject oldOperationsRequest = (InternalEObject) operationsRequest;
			operationsRequest = (OperationsRequest) eResolveProxy(oldOperationsRequest);
			if (operationsRequest != oldOperationsRequest) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_REQUEST, oldOperationsRequest,
							operationsRequest));
			}
		}
		return operationsRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsRequest basicGetOperationsRequest() {
		return operationsRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsRequest(OperationsRequest newOperationsRequest) {
		OperationsRequest oldOperationsRequest = operationsRequest;
		operationsRequest = newOperationsRequest;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_REQUEST, oldOperationsRequest, operationsRequest));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope getHierarchyScope() {
		if (hierarchyScope != null && hierarchyScope.eIsProxy()) {
			InternalEObject oldHierarchyScope = (InternalEObject) hierarchyScope;
			hierarchyScope = (HierarchyScope) eResolveProxy(oldHierarchyScope);
			if (hierarchyScope != oldHierarchyScope) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.OPERATIONS_RESPONSE__HIERARCHY_SCOPE, oldHierarchyScope, hierarchyScope));
			}
		}
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope basicGetHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScope newHierarchyScope) {
		HierarchyScope oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_RESPONSE__HIERARCHY_SCOPE,
					oldHierarchyScope, hierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinition getOperationsDefinition() {
		if (operationsDefinition != null && operationsDefinition.eIsProxy()) {
			InternalEObject oldOperationsDefinition = (InternalEObject) operationsDefinition;
			operationsDefinition = (OperationsDefinition) eResolveProxy(oldOperationsDefinition);
			if (operationsDefinition != oldOperationsDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_DEFINITION, oldOperationsDefinition,
							operationsDefinition));
			}
		}
		return operationsDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinition basicGetOperationsDefinition() {
		return operationsDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsDefinition(OperationsDefinition newOperationsDefinition) {
		OperationsDefinition oldOperationsDefinition = operationsDefinition;
		operationsDefinition = newOperationsDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_DEFINITION, oldOperationsDefinition,
					operationsDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_RESPONSE__SEGMENT_RESPONSES:
			return ((InternalEList<?>) getSegmentResponses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_RESPONSE__SEGMENT_RESPONSES:
			return getSegmentResponses();
		case Iec62264Package.OPERATIONS_RESPONSE__ID:
			return getId();
		case Iec62264Package.OPERATIONS_RESPONSE__DESCRIPTION:
			return getDescription();
		case Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_TYPE:
			return getOperationsType();
		case Iec62264Package.OPERATIONS_RESPONSE__START_TIME:
			return getStartTime();
		case Iec62264Package.OPERATIONS_RESPONSE__END_TIME:
			return getEndTime();
		case Iec62264Package.OPERATIONS_RESPONSE__RESPONSE_STATE:
			return getResponseState();
		case Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_REQUEST:
			if (resolve)
				return getOperationsRequest();
			return basicGetOperationsRequest();
		case Iec62264Package.OPERATIONS_RESPONSE__HIERARCHY_SCOPE:
			if (resolve)
				return getHierarchyScope();
			return basicGetHierarchyScope();
		case Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_DEFINITION:
			if (resolve)
				return getOperationsDefinition();
			return basicGetOperationsDefinition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_RESPONSE__SEGMENT_RESPONSES:
			getSegmentResponses().clear();
			getSegmentResponses().addAll((Collection<? extends SegmentResponse>) newValue);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_TYPE:
			setOperationsType((OperationsType) newValue);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__START_TIME:
			setStartTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__END_TIME:
			setEndTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__RESPONSE_STATE:
			setResponseState((PerformanceState) newValue);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_REQUEST:
			setOperationsRequest((OperationsRequest) newValue);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) newValue);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_DEFINITION:
			setOperationsDefinition((OperationsDefinition) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_RESPONSE__SEGMENT_RESPONSES:
			getSegmentResponses().clear();
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_TYPE:
			setOperationsType(OPERATIONS_TYPE_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__START_TIME:
			setStartTime(START_TIME_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__END_TIME:
			setEndTime(END_TIME_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__RESPONSE_STATE:
			setResponseState(RESPONSE_STATE_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_REQUEST:
			setOperationsRequest((OperationsRequest) null);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) null);
			return;
		case Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_DEFINITION:
			setOperationsDefinition((OperationsDefinition) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_RESPONSE__SEGMENT_RESPONSES:
			return segmentResponses != null && !segmentResponses.isEmpty();
		case Iec62264Package.OPERATIONS_RESPONSE__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.OPERATIONS_RESPONSE__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_TYPE:
			return operationsType != OPERATIONS_TYPE_EDEFAULT;
		case Iec62264Package.OPERATIONS_RESPONSE__START_TIME:
			return START_TIME_EDEFAULT == null ? startTime != null : !START_TIME_EDEFAULT.equals(startTime);
		case Iec62264Package.OPERATIONS_RESPONSE__END_TIME:
			return END_TIME_EDEFAULT == null ? endTime != null : !END_TIME_EDEFAULT.equals(endTime);
		case Iec62264Package.OPERATIONS_RESPONSE__RESPONSE_STATE:
			return responseState != RESPONSE_STATE_EDEFAULT;
		case Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_REQUEST:
			return operationsRequest != null;
		case Iec62264Package.OPERATIONS_RESPONSE__HIERARCHY_SCOPE:
			return hierarchyScope != null;
		case Iec62264Package.OPERATIONS_RESPONSE__OPERATIONS_DEFINITION:
			return operationsDefinition != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", operationsType: ");
		result.append(operationsType);
		result.append(", startTime: ");
		result.append(startTime);
		result.append(", endTime: ");
		result.append(endTime);
		result.append(", responseState: ");
		result.append(responseState);
		result.append(')');
		return result.toString();
	}

} //OperationsResponseImpl
