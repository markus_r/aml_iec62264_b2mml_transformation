/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.time.ZonedDateTime;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Performance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getOperationsResponses <em>Operations Responses</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getOperationsSchedule <em>Operations Schedule</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getPerformanceState <em>Performance State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getPublishedDate <em>Published Date</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformance()
 * @model
 * @generated
 */
public interface OperationsPerformance extends EObject {
	/**
	 * Returns the value of the '<em><b>Operations Responses</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.OperationsResponse}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Responses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Responses</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformance_OperationsResponses()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<OperationsResponse> getOperationsResponses();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformance_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformance_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.OperationsType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #setOperationsType(OperationsType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformance_OperationsType()
	 * @model required="true"
	 * @generated
	 */
	OperationsType getOperationsType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getOperationsType <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsType value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformance_StartTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Operations Schedule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Schedule</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Schedule</em>' reference.
	 * @see #setOperationsSchedule(OperationsSchedule)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformance_OperationsSchedule()
	 * @model
	 * @generated
	 */
	OperationsSchedule getOperationsSchedule();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getOperationsSchedule <em>Operations Schedule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Schedule</em>' reference.
	 * @see #getOperationsSchedule()
	 * @generated
	 */
	void setOperationsSchedule(OperationsSchedule value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformance_EndTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Performance State</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.PerformanceState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Performance State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Performance State</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.PerformanceState
	 * @see #setPerformanceState(PerformanceState)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformance_PerformanceState()
	 * @model
	 * @generated
	 */
	PerformanceState getPerformanceState();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getPerformanceState <em>Performance State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Performance State</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.PerformanceState
	 * @see #getPerformanceState()
	 * @generated
	 */
	void setPerformanceState(PerformanceState value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #setHierarchyScope(HierarchyScope)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformance_HierarchyScope()
	 * @model
	 * @generated
	 */
	HierarchyScope getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getHierarchyScope <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScope value);

	/**
	 * Returns the value of the '<em><b>Published Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Published Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Published Date</em>' attribute.
	 * @see #setPublishedDate(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsPerformance_PublishedDate()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getPublishedDate();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getPublishedDate <em>Published Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Published Date</em>' attribute.
	 * @see #getPublishedDate()
	 * @generated
	 */
	void setPublishedDate(ZonedDateTime value);

} // OperationsPerformance
