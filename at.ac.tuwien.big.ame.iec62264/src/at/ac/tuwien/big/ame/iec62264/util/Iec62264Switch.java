/**
 */
package at.ac.tuwien.big.ame.iec62264.util;

import at.ac.tuwien.big.ame.iec62264.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package
 * @generated
 */
public class Iec62264Switch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Iec62264Package modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iec62264Switch() {
		if (modelPackage == null) {
			modelPackage = Iec62264Package.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case Iec62264Package.PERSON: {
			Person person = (Person) theEObject;
			T result = casePerson(person);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_CLASS: {
			PersonnelClass personnelClass = (PersonnelClass) theEObject;
			T result = casePersonnelClass(personnelClass);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.QUALIFICATION_TEST_SPECIFICATION: {
			QualificationTestSpecification qualificationTestSpecification = (QualificationTestSpecification) theEObject;
			T result = caseQualificationTestSpecification(qualificationTestSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_CLASS_PROPERTY: {
			PersonnelClassProperty personnelClassProperty = (PersonnelClassProperty) theEObject;
			T result = casePersonnelClassProperty(personnelClassProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSON_PROPERTY: {
			PersonProperty personProperty = (PersonProperty) theEObject;
			T result = casePersonProperty(personProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.QUALIFICATION_TEST_RESULT: {
			QualificationTestResult qualificationTestResult = (QualificationTestResult) theEObject;
			T result = caseQualificationTestResult(qualificationTestResult);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_MODEL: {
			PersonnelModel personnelModel = (PersonnelModel) theEObject;
			T result = casePersonnelModel(personnelModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_MODEL: {
			EquipmentModel equipmentModel = (EquipmentModel) theEObject;
			T result = caseEquipmentModel(equipmentModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_CLASS: {
			EquipmentClass equipmentClass = (EquipmentClass) theEObject;
			T result = caseEquipmentClass(equipmentClass);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_CAPABILITY_TEST_RESULT: {
			EquipmentCapabilityTestResult equipmentCapabilityTestResult = (EquipmentCapabilityTestResult) theEObject;
			T result = caseEquipmentCapabilityTestResult(equipmentCapabilityTestResult);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION: {
			EquipmentCapabilityTestSpecification equipmentCapabilityTestSpecification = (EquipmentCapabilityTestSpecification) theEObject;
			T result = caseEquipmentCapabilityTestSpecification(equipmentCapabilityTestSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_CLASS_PROPERTY: {
			EquipmentClassProperty equipmentClassProperty = (EquipmentClassProperty) theEObject;
			T result = caseEquipmentClassProperty(equipmentClassProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_PROPERTY: {
			EquipmentProperty equipmentProperty = (EquipmentProperty) theEObject;
			T result = caseEquipmentProperty(equipmentProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT: {
			Equipment equipment = (Equipment) theEObject;
			T result = caseEquipment(equipment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_MODEL: {
			PhysicalAssetModel physicalAssetModel = (PhysicalAssetModel) theEObject;
			T result = casePhysicalAssetModel(physicalAssetModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_CLASS: {
			PhysicalAssetClass physicalAssetClass = (PhysicalAssetClass) theEObject;
			T result = casePhysicalAssetClass(physicalAssetClass);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_RESULT: {
			PhysicalAssetCapabilityTestResult physicalAssetCapabilityTestResult = (PhysicalAssetCapabilityTestResult) theEObject;
			T result = casePhysicalAssetCapabilityTestResult(physicalAssetCapabilityTestResult);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_PROPERTY: {
			PhysicalAssetProperty physicalAssetProperty = (PhysicalAssetProperty) theEObject;
			T result = casePhysicalAssetProperty(physicalAssetProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET: {
			PhysicalAsset physicalAsset = (PhysicalAsset) theEObject;
			T result = casePhysicalAsset(physicalAsset);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION: {
			PhysicalAssetCapabilityTestSpecification physicalAssetCapabilityTestSpecification = (PhysicalAssetCapabilityTestSpecification) theEObject;
			T result = casePhysicalAssetCapabilityTestSpecification(physicalAssetCapabilityTestSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_CLASS_PROPERTY: {
			PhysicalAssetClassProperty physicalAssetClassProperty = (PhysicalAssetClassProperty) theEObject;
			T result = casePhysicalAssetClassProperty(physicalAssetClassProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING: {
			EquipmentAssetMapping equipmentAssetMapping = (EquipmentAssetMapping) theEObject;
			T result = caseEquipmentAssetMapping(equipmentAssetMapping);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_MODEL: {
			MaterialModel materialModel = (MaterialModel) theEObject;
			T result = caseMaterialModel(materialModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_CLASS: {
			MaterialClass materialClass = (MaterialClass) theEObject;
			T result = caseMaterialClass(materialClass);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_TEST_RESULT: {
			MaterialTestResult materialTestResult = (MaterialTestResult) theEObject;
			T result = caseMaterialTestResult(materialTestResult);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_TEST_SPECIFICATION: {
			MaterialTestSpecification materialTestSpecification = (MaterialTestSpecification) theEObject;
			T result = caseMaterialTestSpecification(materialTestSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_SUBLOT: {
			MaterialSublot materialSublot = (MaterialSublot) theEObject;
			T result = caseMaterialSublot(materialSublot);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_LOT: {
			MaterialLot materialLot = (MaterialLot) theEObject;
			T result = caseMaterialLot(materialLot);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_LOT_PROPERTY: {
			MaterialLotProperty materialLotProperty = (MaterialLotProperty) theEObject;
			T result = caseMaterialLotProperty(materialLotProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_DEFINITION: {
			MaterialDefinition materialDefinition = (MaterialDefinition) theEObject;
			T result = caseMaterialDefinition(materialDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_DEFINITION_PROPERTY: {
			MaterialDefinitionProperty materialDefinitionProperty = (MaterialDefinitionProperty) theEObject;
			T result = caseMaterialDefinitionProperty(materialDefinitionProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_CLASS_PROPERTY: {
			MaterialClassProperty materialClassProperty = (MaterialClassProperty) theEObject;
			T result = caseMaterialClassProperty(materialClassProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PROCESS_SEGMENT_MODEL: {
			ProcessSegmentModel processSegmentModel = (ProcessSegmentModel) theEObject;
			T result = caseProcessSegmentModel(processSegmentModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PROCESS_SEGMENT: {
			ProcessSegment processSegment = (ProcessSegment) theEObject;
			T result = caseProcessSegment(processSegment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY: {
			PersonnelSegmentSpecificationProperty personnelSegmentSpecificationProperty = (PersonnelSegmentSpecificationProperty) theEObject;
			T result = casePersonnelSegmentSpecificationProperty(personnelSegmentSpecificationProperty);
			if (result == null)
				result = caseReferentialPersonnelProperty(personnelSegmentSpecificationProperty);
			if (result == null)
				result = caseReferencialProperty(personnelSegmentSpecificationProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY: {
			EquipmentSegmentSpecificationProperty equipmentSegmentSpecificationProperty = (EquipmentSegmentSpecificationProperty) theEObject;
			T result = caseEquipmentSegmentSpecificationProperty(equipmentSegmentSpecificationProperty);
			if (result == null)
				result = caseReferentialEquipmentProperty(equipmentSegmentSpecificationProperty);
			if (result == null)
				result = caseReferencialProperty(equipmentSegmentSpecificationProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY: {
			PhysicalAssetSegmentSpecificationProperty physicalAssetSegmentSpecificationProperty = (PhysicalAssetSegmentSpecificationProperty) theEObject;
			T result = casePhysicalAssetSegmentSpecificationProperty(physicalAssetSegmentSpecificationProperty);
			if (result == null)
				result = caseReferentialPhysicalAssetProperty(physicalAssetSegmentSpecificationProperty);
			if (result == null)
				result = caseReferencialProperty(physicalAssetSegmentSpecificationProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_SEGMENT_SPECIFICATION_PROPERTY: {
			MaterialSegmentSpecificationProperty materialSegmentSpecificationProperty = (MaterialSegmentSpecificationProperty) theEObject;
			T result = caseMaterialSegmentSpecificationProperty(materialSegmentSpecificationProperty);
			if (result == null)
				result = caseReferentialMaterialTypeProperty(materialSegmentSpecificationProperty);
			if (result == null)
				result = caseReferencialProperty(materialSegmentSpecificationProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_SEGMENT_SPECIFICATION: {
			MaterialSegmentSpecification materialSegmentSpecification = (MaterialSegmentSpecification) theEObject;
			T result = caseMaterialSegmentSpecification(materialSegmentSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION: {
			PhysicalAssetSegmentSpecification physicalAssetSegmentSpecification = (PhysicalAssetSegmentSpecification) theEObject;
			T result = casePhysicalAssetSegmentSpecification(physicalAssetSegmentSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_SEGMENT_SPECIFICATION: {
			EquipmentSegmentSpecification equipmentSegmentSpecification = (EquipmentSegmentSpecification) theEObject;
			T result = caseEquipmentSegmentSpecification(equipmentSegmentSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_SEGMENT_SPECIFICATION: {
			PersonnelSegmentSpecification personnelSegmentSpecification = (PersonnelSegmentSpecification) theEObject;
			T result = casePersonnelSegmentSpecification(personnelSegmentSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PROCESS_SEGMENT_PARAMETER: {
			ProcessSegmentParameter processSegmentParameter = (ProcessSegmentParameter) theEObject;
			T result = caseProcessSegmentParameter(processSegmentParameter);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY: {
			ProcessSegmentDependency processSegmentDependency = (ProcessSegmentDependency) theEObject;
			T result = caseProcessSegmentDependency(processSegmentDependency);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.IEC62264_MODEL: {
			Iec62264Model iec62264Model = (Iec62264Model) theEObject;
			T result = caseIec62264Model(iec62264Model);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_DEFINITION_MODEL: {
			OperationsDefinitionModel operationsDefinitionModel = (OperationsDefinitionModel) theEObject;
			T result = caseOperationsDefinitionModel(operationsDefinitionModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_SCHEDULE_MODEL: {
			OperationsScheduleModel operationsScheduleModel = (OperationsScheduleModel) theEObject;
			T result = caseOperationsScheduleModel(operationsScheduleModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_PERFORMANCE_MODEL: {
			OperationsPerformanceModel operationsPerformanceModel = (OperationsPerformanceModel) theEObject;
			T result = caseOperationsPerformanceModel(operationsPerformanceModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_CAPABILITY_MODEL: {
			OperationsCapabilityModel operationsCapabilityModel = (OperationsCapabilityModel) theEObject;
			T result = caseOperationsCapabilityModel(operationsCapabilityModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PROCESS_SEGMENT_CAPABILITY_MODEL: {
			ProcessSegmentCapabilityModel processSegmentCapabilityModel = (ProcessSegmentCapabilityModel) theEObject;
			T result = caseProcessSegmentCapabilityModel(processSegmentCapabilityModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.HIERARCHY_SCOPE: {
			HierarchyScope hierarchyScope = (HierarchyScope) theEObject;
			T result = caseHierarchyScope(hierarchyScope);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_SPECIFICATION_PROPERTY: {
			MaterialSpecificationProperty materialSpecificationProperty = (MaterialSpecificationProperty) theEObject;
			T result = caseMaterialSpecificationProperty(materialSpecificationProperty);
			if (result == null)
				result = caseReferentialMaterialTypeProperty(materialSpecificationProperty);
			if (result == null)
				result = caseReferencialProperty(materialSpecificationProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION_PROPERTY: {
			PhysicalAssetSpecificationProperty physicalAssetSpecificationProperty = (PhysicalAssetSpecificationProperty) theEObject;
			T result = casePhysicalAssetSpecificationProperty(physicalAssetSpecificationProperty);
			if (result == null)
				result = caseReferentialPhysicalAssetProperty(physicalAssetSpecificationProperty);
			if (result == null)
				result = caseReferencialProperty(physicalAssetSpecificationProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_SPECIFICATION_PROPERTY: {
			EquipmentSpecificationProperty equipmentSpecificationProperty = (EquipmentSpecificationProperty) theEObject;
			T result = caseEquipmentSpecificationProperty(equipmentSpecificationProperty);
			if (result == null)
				result = caseReferentialEquipmentProperty(equipmentSpecificationProperty);
			if (result == null)
				result = caseReferencialProperty(equipmentSpecificationProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_SPECIFICATION_PROPERTY: {
			PersonnelSpecificationProperty personnelSpecificationProperty = (PersonnelSpecificationProperty) theEObject;
			T result = casePersonnelSpecificationProperty(personnelSpecificationProperty);
			if (result == null)
				result = caseReferentialPersonnelProperty(personnelSpecificationProperty);
			if (result == null)
				result = caseReferencialProperty(personnelSpecificationProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_SPECIFICATION: {
			MaterialSpecification materialSpecification = (MaterialSpecification) theEObject;
			T result = caseMaterialSpecification(materialSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_SPECIFICATION: {
			PhysicalAssetSpecification physicalAssetSpecification = (PhysicalAssetSpecification) theEObject;
			T result = casePhysicalAssetSpecification(physicalAssetSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_SPECIFICATION: {
			EquipmentSpecification equipmentSpecification = (EquipmentSpecification) theEObject;
			T result = caseEquipmentSpecification(equipmentSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_SPECIFICATION: {
			PersonnelSpecification personnelSpecification = (PersonnelSpecification) theEObject;
			T result = casePersonnelSpecification(personnelSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PARAMETER_SPECIFICATION: {
			ParameterSpecification parameterSpecification = (ParameterSpecification) theEObject;
			T result = caseParameterSpecification(parameterSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_SEGMENT_DEPENDENCY: {
			OperationsSegmentDependency operationsSegmentDependency = (OperationsSegmentDependency) theEObject;
			T result = caseOperationsSegmentDependency(operationsSegmentDependency);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM: {
			OperationsMaterialBillItem operationsMaterialBillItem = (OperationsMaterialBillItem) theEObject;
			T result = caseOperationsMaterialBillItem(operationsMaterialBillItem);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_MATERIAL_BILL: {
			OperationsMaterialBill operationsMaterialBill = (OperationsMaterialBill) theEObject;
			T result = caseOperationsMaterialBill(operationsMaterialBill);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_SEGMENT: {
			OperationsSegment operationsSegment = (OperationsSegment) theEObject;
			T result = caseOperationsSegment(operationsSegment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_DEFINITION: {
			OperationsDefinition operationsDefinition = (OperationsDefinition) theEObject;
			T result = caseOperationsDefinition(operationsDefinition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_REQUIREMENT: {
			MaterialRequirement materialRequirement = (MaterialRequirement) theEObject;
			T result = caseMaterialRequirement(materialRequirement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_REQUIREMENT_PROPERTY: {
			MaterialRequirementProperty materialRequirementProperty = (MaterialRequirementProperty) theEObject;
			T result = caseMaterialRequirementProperty(materialRequirementProperty);
			if (result == null)
				result = caseReferentialMaterialProperty(materialRequirementProperty);
			if (result == null)
				result = caseReferentialMaterialTypeProperty(materialRequirementProperty);
			if (result == null)
				result = caseReferencialProperty(materialRequirementProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_REQUIREMENT_PROPERTY: {
			PhysicalAssetRequirementProperty physicalAssetRequirementProperty = (PhysicalAssetRequirementProperty) theEObject;
			T result = casePhysicalAssetRequirementProperty(physicalAssetRequirementProperty);
			if (result == null)
				result = caseReferentialPhysicalAssetProperty(physicalAssetRequirementProperty);
			if (result == null)
				result = caseReferencialProperty(physicalAssetRequirementProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_REQUIREMENT_PROPERTY: {
			EquipmentRequirementProperty equipmentRequirementProperty = (EquipmentRequirementProperty) theEObject;
			T result = caseEquipmentRequirementProperty(equipmentRequirementProperty);
			if (result == null)
				result = caseReferentialEquipmentProperty(equipmentRequirementProperty);
			if (result == null)
				result = caseReferencialProperty(equipmentRequirementProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_REQUIREMENT_PROPERTY: {
			PersonnelRequirementProperty personnelRequirementProperty = (PersonnelRequirementProperty) theEObject;
			T result = casePersonnelRequirementProperty(personnelRequirementProperty);
			if (result == null)
				result = caseReferentialPersonnelProperty(personnelRequirementProperty);
			if (result == null)
				result = caseReferencialProperty(personnelRequirementProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_SCHEDULE: {
			OperationsSchedule operationsSchedule = (OperationsSchedule) theEObject;
			T result = caseOperationsSchedule(operationsSchedule);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_REQUEST: {
			OperationsRequest operationsRequest = (OperationsRequest) theEObject;
			T result = caseOperationsRequest(operationsRequest);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.REQUESTED_SEGMENT_RESPONSE: {
			RequestedSegmentResponse requestedSegmentResponse = (RequestedSegmentResponse) theEObject;
			T result = caseRequestedSegmentResponse(requestedSegmentResponse);
			if (result == null)
				result = caseSegmentResponse(requestedSegmentResponse);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.SEGMENT_REQUIREMENT: {
			SegmentRequirement segmentRequirement = (SegmentRequirement) theEObject;
			T result = caseSegmentRequirement(segmentRequirement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_REQUIREMENT: {
			EquipmentRequirement equipmentRequirement = (EquipmentRequirement) theEObject;
			T result = caseEquipmentRequirement(equipmentRequirement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_REQUIREMENT: {
			PhysicalAssetRequirement physicalAssetRequirement = (PhysicalAssetRequirement) theEObject;
			T result = casePhysicalAssetRequirement(physicalAssetRequirement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_REQUIREMENT: {
			PersonnelRequirement personnelRequirement = (PersonnelRequirement) theEObject;
			T result = casePersonnelRequirement(personnelRequirement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.SEGMENT_PARAMETER: {
			SegmentParameter segmentParameter = (SegmentParameter) theEObject;
			T result = caseSegmentParameter(segmentParameter);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_ACTUAL_PROPERTY: {
			MaterialActualProperty materialActualProperty = (MaterialActualProperty) theEObject;
			T result = caseMaterialActualProperty(materialActualProperty);
			if (result == null)
				result = caseReferentialMaterialProperty(materialActualProperty);
			if (result == null)
				result = caseReferentialMaterialTypeProperty(materialActualProperty);
			if (result == null)
				result = caseReferencialProperty(materialActualProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_ACTUAL_PROPERTY: {
			PhysicalAssetActualProperty physicalAssetActualProperty = (PhysicalAssetActualProperty) theEObject;
			T result = casePhysicalAssetActualProperty(physicalAssetActualProperty);
			if (result == null)
				result = caseReferentialPhysicalAssetProperty(physicalAssetActualProperty);
			if (result == null)
				result = caseReferencialProperty(physicalAssetActualProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_ACTUAL_PROPERTY: {
			EquipmentActualProperty equipmentActualProperty = (EquipmentActualProperty) theEObject;
			T result = caseEquipmentActualProperty(equipmentActualProperty);
			if (result == null)
				result = caseReferentialEquipmentProperty(equipmentActualProperty);
			if (result == null)
				result = caseReferencialProperty(equipmentActualProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_ACTUAL_PROPERTY: {
			PersonnelActualProperty personnelActualProperty = (PersonnelActualProperty) theEObject;
			T result = casePersonnelActualProperty(personnelActualProperty);
			if (result == null)
				result = caseReferentialPersonnelProperty(personnelActualProperty);
			if (result == null)
				result = caseReferencialProperty(personnelActualProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_ACTUAL: {
			MaterialActual materialActual = (MaterialActual) theEObject;
			T result = caseMaterialActual(materialActual);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_ACTUAL: {
			PhysicalAssetActual physicalAssetActual = (PhysicalAssetActual) theEObject;
			T result = casePhysicalAssetActual(physicalAssetActual);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_ACTUAL: {
			EquipmentActual equipmentActual = (EquipmentActual) theEObject;
			T result = caseEquipmentActual(equipmentActual);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_ACTUAL: {
			PersonnelActual personnelActual = (PersonnelActual) theEObject;
			T result = casePersonnelActual(personnelActual);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.SEGMENT_DATA: {
			SegmentData segmentData = (SegmentData) theEObject;
			T result = caseSegmentData(segmentData);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.SEGMENT_RESPONSE: {
			SegmentResponse segmentResponse = (SegmentResponse) theEObject;
			T result = caseSegmentResponse(segmentResponse);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_RESPONSE: {
			OperationsResponse operationsResponse = (OperationsResponse) theEObject;
			T result = caseOperationsResponse(operationsResponse);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_PERFORMANCE: {
			OperationsPerformance operationsPerformance = (OperationsPerformance) theEObject;
			T result = caseOperationsPerformance(operationsPerformance);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.OPERATIONS_CAPABILITY: {
			OperationsCapability operationsCapability = (OperationsCapability) theEObject;
			T result = caseOperationsCapability(operationsCapability);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_CAPABILITY_PROPERTY: {
			MaterialCapabilityProperty materialCapabilityProperty = (MaterialCapabilityProperty) theEObject;
			T result = caseMaterialCapabilityProperty(materialCapabilityProperty);
			if (result == null)
				result = caseReferentialMaterialProperty(materialCapabilityProperty);
			if (result == null)
				result = caseReferentialMaterialTypeProperty(materialCapabilityProperty);
			if (result == null)
				result = caseReferencialProperty(materialCapabilityProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY_PROPERTY: {
			PhysicalAssetCapabilityProperty physicalAssetCapabilityProperty = (PhysicalAssetCapabilityProperty) theEObject;
			T result = casePhysicalAssetCapabilityProperty(physicalAssetCapabilityProperty);
			if (result == null)
				result = caseReferentialPhysicalAssetProperty(physicalAssetCapabilityProperty);
			if (result == null)
				result = caseReferencialProperty(physicalAssetCapabilityProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_CAPABILITY_PROPERTY: {
			EquipmentCapabilityProperty equipmentCapabilityProperty = (EquipmentCapabilityProperty) theEObject;
			T result = caseEquipmentCapabilityProperty(equipmentCapabilityProperty);
			if (result == null)
				result = caseReferentialEquipmentProperty(equipmentCapabilityProperty);
			if (result == null)
				result = caseReferencialProperty(equipmentCapabilityProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_CAPABILITY_PROPERTY: {
			PersonnelCapabilityProperty personnelCapabilityProperty = (PersonnelCapabilityProperty) theEObject;
			T result = casePersonnelCapabilityProperty(personnelCapabilityProperty);
			if (result == null)
				result = caseReferentialPersonnelProperty(personnelCapabilityProperty);
			if (result == null)
				result = caseReferencialProperty(personnelCapabilityProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.MATERIAL_CAPABILITY: {
			MaterialCapability materialCapability = (MaterialCapability) theEObject;
			T result = caseMaterialCapability(materialCapability);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY: {
			PhysicalAssetCapability physicalAssetCapability = (PhysicalAssetCapability) theEObject;
			T result = casePhysicalAssetCapability(physicalAssetCapability);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.EQUIPMENT_CAPABILITY: {
			EquipmentCapability equipmentCapability = (EquipmentCapability) theEObject;
			T result = caseEquipmentCapability(equipmentCapability);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PERSONNEL_CAPABILITY: {
			PersonnelCapability personnelCapability = (PersonnelCapability) theEObject;
			T result = casePersonnelCapability(personnelCapability);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.PROCESS_SEGMENT_CAPABILITY: {
			ProcessSegmentCapability processSegmentCapability = (ProcessSegmentCapability) theEObject;
			T result = caseProcessSegmentCapability(processSegmentCapability);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.REFERENCIAL_PROPERTY: {
			ReferencialProperty referencialProperty = (ReferencialProperty) theEObject;
			T result = caseReferencialProperty(referencialProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.REFERENTIAL_PERSONNEL_PROPERTY: {
			ReferentialPersonnelProperty referentialPersonnelProperty = (ReferentialPersonnelProperty) theEObject;
			T result = caseReferentialPersonnelProperty(referentialPersonnelProperty);
			if (result == null)
				result = caseReferencialProperty(referentialPersonnelProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.REFERENTIAL_EQUIPMENT_PROPERTY: {
			ReferentialEquipmentProperty referentialEquipmentProperty = (ReferentialEquipmentProperty) theEObject;
			T result = caseReferentialEquipmentProperty(referentialEquipmentProperty);
			if (result == null)
				result = caseReferencialProperty(referentialEquipmentProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY: {
			ReferentialPhysicalAssetProperty referentialPhysicalAssetProperty = (ReferentialPhysicalAssetProperty) theEObject;
			T result = caseReferentialPhysicalAssetProperty(referentialPhysicalAssetProperty);
			if (result == null)
				result = caseReferencialProperty(referentialPhysicalAssetProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY: {
			ReferentialMaterialTypeProperty referentialMaterialTypeProperty = (ReferentialMaterialTypeProperty) theEObject;
			T result = caseReferentialMaterialTypeProperty(referentialMaterialTypeProperty);
			if (result == null)
				result = caseReferencialProperty(referentialMaterialTypeProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case Iec62264Package.REFERENTIAL_MATERIAL_PROPERTY: {
			ReferentialMaterialProperty referentialMaterialProperty = (ReferentialMaterialProperty) theEObject;
			T result = caseReferentialMaterialProperty(referentialMaterialProperty);
			if (result == null)
				result = caseReferentialMaterialTypeProperty(referentialMaterialProperty);
			if (result == null)
				result = caseReferencialProperty(referentialMaterialProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Person</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Person</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerson(Person object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelClass(PersonnelClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Qualification Test Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Qualification Test Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQualificationTestSpecification(QualificationTestSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Class Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelClassProperty(PersonnelClassProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Person Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Person Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonProperty(PersonProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Qualification Test Result</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Qualification Test Result</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQualificationTestResult(QualificationTestResult object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelModel(PersonnelModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentModel(EquipmentModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentClass(EquipmentClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Capability Test Result</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Capability Test Result</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentCapabilityTestResult(EquipmentCapabilityTestResult object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Capability Test Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Capability Test Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentCapabilityTestSpecification(EquipmentCapabilityTestSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Class Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentClassProperty(EquipmentClassProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentProperty(EquipmentProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipment(Equipment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetModel(PhysicalAssetModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetClass(PhysicalAssetClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Capability Test Result</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Capability Test Result</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetCapabilityTestResult(PhysicalAssetCapabilityTestResult object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetProperty(PhysicalAssetProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAsset(PhysicalAsset object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Capability Test Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Capability Test Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetCapabilityTestSpecification(PhysicalAssetCapabilityTestSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Class Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetClassProperty(PhysicalAssetClassProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Asset Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Asset Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentAssetMapping(EquipmentAssetMapping object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialModel(MaterialModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialClass(MaterialClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Test Result</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Test Result</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialTestResult(MaterialTestResult object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Test Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Test Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialTestSpecification(MaterialTestSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Sublot</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Sublot</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialSublot(MaterialSublot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Lot</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Lot</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialLot(MaterialLot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Lot Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Lot Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialLotProperty(MaterialLotProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialDefinition(MaterialDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Definition Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Definition Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialDefinitionProperty(MaterialDefinitionProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Class Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialClassProperty(MaterialClassProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Segment Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Segment Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessSegmentModel(ProcessSegmentModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Segment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Segment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessSegment(ProcessSegment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Segment Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Segment Specification Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelSegmentSpecificationProperty(PersonnelSegmentSpecificationProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Segment Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Segment Specification Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentSegmentSpecificationProperty(EquipmentSegmentSpecificationProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Segment Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Segment Specification Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetSegmentSpecificationProperty(PhysicalAssetSegmentSpecificationProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Segment Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Segment Specification Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialSegmentSpecificationProperty(MaterialSegmentSpecificationProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Segment Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Segment Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialSegmentSpecification(MaterialSegmentSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Segment Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Segment Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetSegmentSpecification(PhysicalAssetSegmentSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Segment Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Segment Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentSegmentSpecification(EquipmentSegmentSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Segment Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Segment Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelSegmentSpecification(PersonnelSegmentSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Segment Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Segment Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessSegmentParameter(ProcessSegmentParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Segment Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Segment Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessSegmentDependency(ProcessSegmentDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIec62264Model(Iec62264Model object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Definition Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Definition Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsDefinitionModel(OperationsDefinitionModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Schedule Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Schedule Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsScheduleModel(OperationsScheduleModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Performance Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Performance Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsPerformanceModel(OperationsPerformanceModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Capability Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Capability Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsCapabilityModel(OperationsCapabilityModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Segment Capability Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Segment Capability Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessSegmentCapabilityModel(ProcessSegmentCapabilityModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hierarchy Scope</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hierarchy Scope</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHierarchyScope(HierarchyScope object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Specification Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialSpecificationProperty(MaterialSpecificationProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Specification Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetSpecificationProperty(PhysicalAssetSpecificationProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Specification Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentSpecificationProperty(EquipmentSpecificationProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Specification Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Specification Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelSpecificationProperty(PersonnelSpecificationProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialSpecification(MaterialSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetSpecification(PhysicalAssetSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentSpecification(EquipmentSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelSpecification(PersonnelSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterSpecification(ParameterSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Segment Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Segment Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsSegmentDependency(OperationsSegmentDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Material Bill Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Material Bill Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsMaterialBillItem(OperationsMaterialBillItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Material Bill</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Material Bill</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsMaterialBill(OperationsMaterialBill object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Segment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Segment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsSegment(OperationsSegment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsDefinition(OperationsDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Requirement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialRequirement(MaterialRequirement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Requirement Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Requirement Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialRequirementProperty(MaterialRequirementProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Requirement Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Requirement Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetRequirementProperty(PhysicalAssetRequirementProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Requirement Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Requirement Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentRequirementProperty(EquipmentRequirementProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Requirement Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Requirement Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelRequirementProperty(PersonnelRequirementProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Schedule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Schedule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsSchedule(OperationsSchedule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Request</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Request</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsRequest(OperationsRequest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Requested Segment Response</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Requested Segment Response</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequestedSegmentResponse(RequestedSegmentResponse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Segment Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Segment Requirement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSegmentRequirement(SegmentRequirement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Requirement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentRequirement(EquipmentRequirement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Requirement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetRequirement(PhysicalAssetRequirement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Requirement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelRequirement(PersonnelRequirement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Segment Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Segment Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSegmentParameter(SegmentParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Actual Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Actual Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialActualProperty(MaterialActualProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Actual Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Actual Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetActualProperty(PhysicalAssetActualProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Actual Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Actual Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentActualProperty(EquipmentActualProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Actual Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Actual Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelActualProperty(PersonnelActualProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Actual</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Actual</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialActual(MaterialActual object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Actual</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Actual</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetActual(PhysicalAssetActual object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Actual</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Actual</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentActual(EquipmentActual object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Actual</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Actual</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelActual(PersonnelActual object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Segment Data</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Segment Data</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSegmentData(SegmentData object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Segment Response</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Segment Response</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSegmentResponse(SegmentResponse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Response</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Response</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsResponse(OperationsResponse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Performance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Performance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsPerformance(OperationsPerformance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Capability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Capability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsCapability(OperationsCapability object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Capability Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Capability Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialCapabilityProperty(MaterialCapabilityProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Capability Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Capability Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetCapabilityProperty(PhysicalAssetCapabilityProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Capability Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Capability Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentCapabilityProperty(EquipmentCapabilityProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Capability Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Capability Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelCapabilityProperty(PersonnelCapabilityProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Capability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Capability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialCapability(MaterialCapability object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Capability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Capability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetCapability(PhysicalAssetCapability object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Capability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Capability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentCapability(EquipmentCapability object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Capability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Capability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelCapability(PersonnelCapability object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Segment Capability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Segment Capability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessSegmentCapability(ProcessSegmentCapability object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Referencial Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Referencial Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferencialProperty(ReferencialProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Referential Personnel Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Referential Personnel Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferentialPersonnelProperty(ReferentialPersonnelProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Referential Equipment Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Referential Equipment Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferentialEquipmentProperty(ReferentialEquipmentProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Referential Physical Asset Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Referential Physical Asset Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferentialPhysicalAssetProperty(ReferentialPhysicalAssetProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Referential Material Type Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Referential Material Type Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferentialMaterialTypeProperty(ReferentialMaterialTypeProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Referential Material Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Referential Material Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferentialMaterialProperty(ReferentialMaterialProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //Iec62264Switch
