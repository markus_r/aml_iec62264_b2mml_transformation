/**
 */
package at.ac.tuwien.big.ame.iec62264;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Referential Physical Asset Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty#getPhysicalAssetClassProperty <em>Physical Asset Class Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty#getPhysicalAssetProperty <em>Physical Asset Property</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialPhysicalAssetProperty()
 * @model abstract="true"
 * @generated
 */
public interface ReferentialPhysicalAssetProperty extends ReferencialProperty {
	/**
	 * Returns the value of the '<em><b>Physical Asset Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Class Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Class Property</em>' reference.
	 * @see #setPhysicalAssetClassProperty(PhysicalAssetClassProperty)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialPhysicalAssetProperty_PhysicalAssetClassProperty()
	 * @model
	 * @generated
	 */
	PhysicalAssetClassProperty getPhysicalAssetClassProperty();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty#getPhysicalAssetClassProperty <em>Physical Asset Class Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Class Property</em>' reference.
	 * @see #getPhysicalAssetClassProperty()
	 * @generated
	 */
	void setPhysicalAssetClassProperty(PhysicalAssetClassProperty value);

	/**
	 * Returns the value of the '<em><b>Physical Asset Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Property</em>' reference.
	 * @see #setPhysicalAssetProperty(PhysicalAssetProperty)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialPhysicalAssetProperty_PhysicalAssetProperty()
	 * @model
	 * @generated
	 */
	PhysicalAssetProperty getPhysicalAssetProperty();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty#getPhysicalAssetProperty <em>Physical Asset Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Property</em>' reference.
	 * @see #getPhysicalAssetProperty()
	 * @generated
	 */
	void setPhysicalAssetProperty(PhysicalAssetProperty value);

} // ReferentialPhysicalAssetProperty
