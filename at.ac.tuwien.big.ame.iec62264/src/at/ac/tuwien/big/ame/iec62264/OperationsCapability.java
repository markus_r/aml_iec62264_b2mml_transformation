/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.time.ZonedDateTime;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Capability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getProcessSegmentCapabilities <em>Process Segment Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getPersonnelCapabilities <em>Personnel Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getEquipmentCapabilities <em>Equipment Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getPhysicalAssetCapabilities <em>Physical Asset Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getMaterialCapabilities <em>Material Capabilities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getCapacityType <em>Capacity Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getReason <em>Reason</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability()
 * @model
 * @generated
 */
public interface OperationsCapability extends EObject {
	/**
	 * Returns the value of the '<em><b>Process Segment Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment Capabilities</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_ProcessSegmentCapabilities()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProcessSegmentCapability> getProcessSegmentCapabilities();

	/**
	 * Returns the value of the '<em><b>Personnel Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Capabilities</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_PersonnelCapabilities()
	 * @model containment="true"
	 * @generated
	 */
	EList<PersonnelCapability> getPersonnelCapabilities();

	/**
	 * Returns the value of the '<em><b>Equipment Capabilities</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Capabilities</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Capabilities</em>' reference.
	 * @see #setEquipmentCapabilities(EquipmentCapability)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_EquipmentCapabilities()
	 * @model
	 * @generated
	 */
	EquipmentCapability getEquipmentCapabilities();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getEquipmentCapabilities <em>Equipment Capabilities</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Capabilities</em>' reference.
	 * @see #getEquipmentCapabilities()
	 * @generated
	 */
	void setEquipmentCapabilities(EquipmentCapability value);

	/**
	 * Returns the value of the '<em><b>Physical Asset Capabilities</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Capabilities</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Capabilities</em>' reference.
	 * @see #setPhysicalAssetCapabilities(PhysicalAssetCapability)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_PhysicalAssetCapabilities()
	 * @model
	 * @generated
	 */
	PhysicalAssetCapability getPhysicalAssetCapabilities();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getPhysicalAssetCapabilities <em>Physical Asset Capabilities</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Capabilities</em>' reference.
	 * @see #getPhysicalAssetCapabilities()
	 * @generated
	 */
	void setPhysicalAssetCapabilities(PhysicalAssetCapability value);

	/**
	 * Returns the value of the '<em><b>Material Capabilities</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Capabilities</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Capabilities</em>' reference.
	 * @see #setMaterialCapabilities(MaterialCapability)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_MaterialCapabilities()
	 * @model
	 * @generated
	 */
	MaterialCapability getMaterialCapabilities();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getMaterialCapabilities <em>Material Capabilities</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Capabilities</em>' reference.
	 * @see #getMaterialCapabilities()
	 * @generated
	 */
	void setMaterialCapabilities(MaterialCapability value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Capacity Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.CapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capacity Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capacity Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.CapabilityType
	 * @see #setCapacityType(CapabilityType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_CapacityType()
	 * @model
	 * @generated
	 */
	CapabilityType getCapacityType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getCapacityType <em>Capacity Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Capacity Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.CapabilityType
	 * @see #getCapacityType()
	 * @generated
	 */
	void setCapacityType(CapabilityType value);

	/**
	 * Returns the value of the '<em><b>Reason</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reason</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reason</em>' attribute.
	 * @see #setReason(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_Reason()
	 * @model
	 * @generated
	 */
	String getReason();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getReason <em>Reason</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reason</em>' attribute.
	 * @see #getReason()
	 * @generated
	 */
	void setReason(String value);

	/**
	 * Returns the value of the '<em><b>Confidence Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Confidence Factor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confidence Factor</em>' attribute.
	 * @see #setConfidenceFactor(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_ConfidenceFactor()
	 * @model
	 * @generated
	 */
	String getConfidenceFactor();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getConfidenceFactor <em>Confidence Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confidence Factor</em>' attribute.
	 * @see #getConfidenceFactor()
	 * @generated
	 */
	void setConfidenceFactor(String value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_StartTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_EndTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Published Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Published Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Published Date</em>' attribute.
	 * @see #setPublishedDate(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_PublishedDate()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getPublishedDate();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getPublishedDate <em>Published Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Published Date</em>' attribute.
	 * @see #getPublishedDate()
	 * @generated
	 */
	void setPublishedDate(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #setHierarchyScope(HierarchyScope)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsCapability_HierarchyScope()
	 * @model
	 * @generated
	 */
	HierarchyScope getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getHierarchyScope <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScope value);

} // OperationsCapability
