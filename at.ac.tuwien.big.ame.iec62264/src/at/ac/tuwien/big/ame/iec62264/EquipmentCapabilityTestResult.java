/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.time.ZonedDateTime;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Capability Test Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getTestSpecification <em>Test Specification</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getProperty <em>Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getDate <em>Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getResult <em>Result</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getResultUnitOfMeasure <em>Result Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getExpiration <em>Expiration</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentCapabilityTestResult()
 * @model
 * @generated
 */
public interface EquipmentCapabilityTestResult extends EObject {
	/**
	 * Returns the value of the '<em><b>Test Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Specification</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Specification</em>' reference.
	 * @see #setTestSpecification(EquipmentCapabilityTestSpecification)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentCapabilityTestResult_TestSpecification()
	 * @model required="true"
	 * @generated
	 */
	EquipmentCapabilityTestSpecification getTestSpecification();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getTestSpecification <em>Test Specification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Specification</em>' reference.
	 * @see #getTestSpecification()
	 * @generated
	 */
	void setTestSpecification(EquipmentCapabilityTestSpecification value);

	/**
	 * Returns the value of the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' reference.
	 * @see #setProperty(EquipmentProperty)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentCapabilityTestResult_Property()
	 * @model required="true"
	 * @generated
	 */
	EquipmentProperty getProperty();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getProperty <em>Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' reference.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(EquipmentProperty value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentCapabilityTestResult_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentCapabilityTestResult_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #setDate(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentCapabilityTestResult_Date()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getDate();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #getDate()
	 * @generated
	 */
	void setDate(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result</em>' attribute.
	 * @see #setResult(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentCapabilityTestResult_Result()
	 * @model
	 * @generated
	 */
	String getResult();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getResult <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result</em>' attribute.
	 * @see #getResult()
	 * @generated
	 */
	void setResult(String value);

	/**
	 * Returns the value of the '<em><b>Result Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result Unit Of Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result Unit Of Measure</em>' attribute.
	 * @see #setResultUnitOfMeasure(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentCapabilityTestResult_ResultUnitOfMeasure()
	 * @model
	 * @generated
	 */
	String getResultUnitOfMeasure();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getResultUnitOfMeasure <em>Result Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result Unit Of Measure</em>' attribute.
	 * @see #getResultUnitOfMeasure()
	 * @generated
	 */
	void setResultUnitOfMeasure(String value);

	/**
	 * Returns the value of the '<em><b>Expiration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expiration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expiration</em>' attribute.
	 * @see #setExpiration(XMLGregorianCalendar)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentCapabilityTestResult_Expiration()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.DateTime"
	 * @generated
	 */
	XMLGregorianCalendar getExpiration();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getExpiration <em>Expiration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expiration</em>' attribute.
	 * @see #getExpiration()
	 * @generated
	 */
	void setExpiration(XMLGregorianCalendar value);

} // EquipmentCapabilityTestResult
