/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.AssemblyRelationship;
import at.ac.tuwien.big.ame.iec62264.AssemblyType;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialClass;
import at.ac.tuwien.big.ame.iec62264.MaterialDefinition;
import at.ac.tuwien.big.ame.iec62264.MaterialSpecification;
import at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem;
import at.ac.tuwien.big.ame.iec62264.UseType;

import java.math.BigDecimal;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Material Bill Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl#getMaterialSpecifications <em>Material Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl#getAssembledFromItems <em>Assembled From Items</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl#getMaterialClasses <em>Material Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl#getMaterialDefinitions <em>Material Definitions</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl#getUseType <em>Use Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl#getQuantities <em>Quantities</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl#getQuantityUnitOfMeasures <em>Quantity Unit Of Measures</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsMaterialBillItemImpl extends MinimalEObjectImpl.Container implements OperationsMaterialBillItem {
	/**
	 * The cached value of the '{@link #getMaterialSpecifications() <em>Material Specifications</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSpecification> materialSpecifications;

	/**
	 * The cached value of the '{@link #getAssembledFromItems() <em>Assembled From Items</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssembledFromItems()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsMaterialBillItem> assembledFromItems;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMaterialClasses() <em>Material Classes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialClass> materialClasses;

	/**
	 * The cached value of the '{@link #getMaterialDefinitions() <em>Material Definitions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinitions()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialDefinition> materialDefinitions;

	/**
	 * The default value of the '{@link #getUseType() <em>Use Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseType()
	 * @generated
	 * @ordered
	 */
	protected static final UseType USE_TYPE_EDEFAULT = UseType.CONSUMED;

	/**
	 * The cached value of the '{@link #getUseType() <em>Use Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseType()
	 * @generated
	 * @ordered
	 */
	protected UseType useType = USE_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected static final AssemblyType ASSEMBLY_TYPE_EDEFAULT = AssemblyType.PHYSICAL;

	/**
	 * The cached value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected AssemblyType assemblyType = ASSEMBLY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected static final AssemblyRelationship ASSEMBLY_RELATIONSHIP_EDEFAULT = AssemblyRelationship.PERMANENT;

	/**
	 * The cached value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected AssemblyRelationship assemblyRelationship = ASSEMBLY_RELATIONSHIP_EDEFAULT;

	/**
	 * The cached value of the '{@link #getQuantities() <em>Quantities</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantities()
	 * @generated
	 * @ordered
	 */
	protected EList<BigDecimal> quantities;

	/**
	 * The cached value of the '{@link #getQuantityUnitOfMeasures() <em>Quantity Unit Of Measures</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasures()
	 * @generated
	 * @ordered
	 */
	protected EList<String> quantityUnitOfMeasures;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsMaterialBillItemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.OPERATIONS_MATERIAL_BILL_ITEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSpecification> getMaterialSpecifications() {
		if (materialSpecifications == null) {
			materialSpecifications = new EObjectResolvingEList<MaterialSpecification>(MaterialSpecification.class, this,
					Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_SPECIFICATIONS);
		}
		return materialSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsMaterialBillItem> getAssembledFromItems() {
		if (assembledFromItems == null) {
			assembledFromItems = new EObjectResolvingEList<OperationsMaterialBillItem>(OperationsMaterialBillItem.class,
					this, Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLED_FROM_ITEMS);
		}
		return assembledFromItems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ID,
					oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialClass> getMaterialClasses() {
		if (materialClasses == null) {
			materialClasses = new EObjectResolvingEList<MaterialClass>(MaterialClass.class, this,
					Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_CLASSES);
		}
		return materialClasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialDefinition> getMaterialDefinitions() {
		if (materialDefinitions == null) {
			materialDefinitions = new EObjectResolvingEList<MaterialDefinition>(MaterialDefinition.class, this,
					Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_DEFINITIONS);
		}
		return materialDefinitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseType getUseType() {
		return useType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseType(UseType newUseType) {
		UseType oldUseType = useType;
		useType = newUseType == null ? USE_TYPE_EDEFAULT : newUseType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__USE_TYPE, oldUseType, useType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyType getAssemblyType() {
		return assemblyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyType(AssemblyType newAssemblyType) {
		AssemblyType oldAssemblyType = assemblyType;
		assemblyType = newAssemblyType == null ? ASSEMBLY_TYPE_EDEFAULT : newAssemblyType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_TYPE, oldAssemblyType, assemblyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationship getAssemblyRelationship() {
		return assemblyRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyRelationship(AssemblyRelationship newAssemblyRelationship) {
		AssemblyRelationship oldAssemblyRelationship = assemblyRelationship;
		assemblyRelationship = newAssemblyRelationship == null ? ASSEMBLY_RELATIONSHIP_EDEFAULT
				: newAssemblyRelationship;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_RELATIONSHIP, oldAssemblyRelationship,
					assemblyRelationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BigDecimal> getQuantities() {
		if (quantities == null) {
			quantities = new EDataTypeUniqueEList<BigDecimal>(BigDecimal.class, this,
					Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__QUANTITIES);
		}
		return quantities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getQuantityUnitOfMeasures() {
		if (quantityUnitOfMeasures == null) {
			quantityUnitOfMeasures = new EDataTypeUniqueEList<String>(String.class, this,
					Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__QUANTITY_UNIT_OF_MEASURES);
		}
		return quantityUnitOfMeasures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_SPECIFICATIONS:
			return getMaterialSpecifications();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLED_FROM_ITEMS:
			return getAssembledFromItems();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ID:
			return getId();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__DESCRIPTION:
			return getDescription();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_CLASSES:
			return getMaterialClasses();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_DEFINITIONS:
			return getMaterialDefinitions();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__USE_TYPE:
			return getUseType();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_TYPE:
			return getAssemblyType();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_RELATIONSHIP:
			return getAssemblyRelationship();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__QUANTITIES:
			return getQuantities();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__QUANTITY_UNIT_OF_MEASURES:
			return getQuantityUnitOfMeasures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_SPECIFICATIONS:
			getMaterialSpecifications().clear();
			getMaterialSpecifications().addAll((Collection<? extends MaterialSpecification>) newValue);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLED_FROM_ITEMS:
			getAssembledFromItems().clear();
			getAssembledFromItems().addAll((Collection<? extends OperationsMaterialBillItem>) newValue);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_CLASSES:
			getMaterialClasses().clear();
			getMaterialClasses().addAll((Collection<? extends MaterialClass>) newValue);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_DEFINITIONS:
			getMaterialDefinitions().clear();
			getMaterialDefinitions().addAll((Collection<? extends MaterialDefinition>) newValue);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__USE_TYPE:
			setUseType((UseType) newValue);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_TYPE:
			setAssemblyType((AssemblyType) newValue);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_RELATIONSHIP:
			setAssemblyRelationship((AssemblyRelationship) newValue);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__QUANTITIES:
			getQuantities().clear();
			getQuantities().addAll((Collection<? extends BigDecimal>) newValue);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__QUANTITY_UNIT_OF_MEASURES:
			getQuantityUnitOfMeasures().clear();
			getQuantityUnitOfMeasures().addAll((Collection<? extends String>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_SPECIFICATIONS:
			getMaterialSpecifications().clear();
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLED_FROM_ITEMS:
			getAssembledFromItems().clear();
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_CLASSES:
			getMaterialClasses().clear();
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_DEFINITIONS:
			getMaterialDefinitions().clear();
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__USE_TYPE:
			setUseType(USE_TYPE_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_TYPE:
			setAssemblyType(ASSEMBLY_TYPE_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_RELATIONSHIP:
			setAssemblyRelationship(ASSEMBLY_RELATIONSHIP_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__QUANTITIES:
			getQuantities().clear();
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__QUANTITY_UNIT_OF_MEASURES:
			getQuantityUnitOfMeasures().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_SPECIFICATIONS:
			return materialSpecifications != null && !materialSpecifications.isEmpty();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLED_FROM_ITEMS:
			return assembledFromItems != null && !assembledFromItems.isEmpty();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_CLASSES:
			return materialClasses != null && !materialClasses.isEmpty();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_DEFINITIONS:
			return materialDefinitions != null && !materialDefinitions.isEmpty();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__USE_TYPE:
			return useType != USE_TYPE_EDEFAULT;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_TYPE:
			return assemblyType != ASSEMBLY_TYPE_EDEFAULT;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_RELATIONSHIP:
			return assemblyRelationship != ASSEMBLY_RELATIONSHIP_EDEFAULT;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__QUANTITIES:
			return quantities != null && !quantities.isEmpty();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL_ITEM__QUANTITY_UNIT_OF_MEASURES:
			return quantityUnitOfMeasures != null && !quantityUnitOfMeasures.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", useType: ");
		result.append(useType);
		result.append(", assemblyType: ");
		result.append(assemblyType);
		result.append(", assemblyRelationship: ");
		result.append(assemblyRelationship);
		result.append(", quantities: ");
		result.append(quantities);
		result.append(", quantityUnitOfMeasures: ");
		result.append(quantityUnitOfMeasures);
		result.append(')');
		return result.toString();
	}

} //OperationsMaterialBillItemImpl
