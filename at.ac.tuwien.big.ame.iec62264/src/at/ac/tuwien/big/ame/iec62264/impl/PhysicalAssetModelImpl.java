/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.PhysicalAsset;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetModelImpl#getPhysicalAssets <em>Physical Assets</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetModelImpl#getPhysicalAssetClasses <em>Physical Asset Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetModelImpl#getTestSpecifications <em>Test Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetModelImpl#getEquipmentAssetMappings <em>Equipment Asset Mappings</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetModelImpl#getTestResults <em>Test Results</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalAssetModelImpl extends MinimalEObjectImpl.Container implements PhysicalAssetModel {
	/**
	 * The cached value of the '{@link #getPhysicalAssets() <em>Physical Assets</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssets()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAsset> physicalAssets;

	/**
	 * The cached value of the '{@link #getPhysicalAssetClasses() <em>Physical Asset Classes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetClass> physicalAssetClasses;

	/**
	 * The cached value of the '{@link #getTestSpecifications() <em>Test Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetCapabilityTestSpecification> testSpecifications;

	/**
	 * The cached value of the '{@link #getEquipmentAssetMappings() <em>Equipment Asset Mappings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentAssetMappings()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentAssetMapping> equipmentAssetMappings;

	/**
	 * The cached value of the '{@link #getTestResults() <em>Test Results</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestResults()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetCapabilityTestResult> testResults;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PHYSICAL_ASSET_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAsset> getPhysicalAssets() {
		if (physicalAssets == null) {
			physicalAssets = new EObjectContainmentEList<PhysicalAsset>(PhysicalAsset.class, this,
					Iec62264Package.PHYSICAL_ASSET_MODEL__PHYSICAL_ASSETS);
		}
		return physicalAssets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetClass> getPhysicalAssetClasses() {
		if (physicalAssetClasses == null) {
			physicalAssetClasses = new EObjectContainmentEList<PhysicalAssetClass>(PhysicalAssetClass.class, this,
					Iec62264Package.PHYSICAL_ASSET_MODEL__PHYSICAL_ASSET_CLASSES);
		}
		return physicalAssetClasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetCapabilityTestSpecification> getTestSpecifications() {
		if (testSpecifications == null) {
			testSpecifications = new EObjectContainmentEList<PhysicalAssetCapabilityTestSpecification>(
					PhysicalAssetCapabilityTestSpecification.class, this,
					Iec62264Package.PHYSICAL_ASSET_MODEL__TEST_SPECIFICATIONS);
		}
		return testSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentAssetMapping> getEquipmentAssetMappings() {
		if (equipmentAssetMappings == null) {
			equipmentAssetMappings = new EObjectContainmentEList<EquipmentAssetMapping>(EquipmentAssetMapping.class,
					this, Iec62264Package.PHYSICAL_ASSET_MODEL__EQUIPMENT_ASSET_MAPPINGS);
		}
		return equipmentAssetMappings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetCapabilityTestResult> getTestResults() {
		if (testResults == null) {
			testResults = new EObjectContainmentEList<PhysicalAssetCapabilityTestResult>(
					PhysicalAssetCapabilityTestResult.class, this, Iec62264Package.PHYSICAL_ASSET_MODEL__TEST_RESULTS);
		}
		return testResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_MODEL__PHYSICAL_ASSETS:
			return ((InternalEList<?>) getPhysicalAssets()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PHYSICAL_ASSET_MODEL__PHYSICAL_ASSET_CLASSES:
			return ((InternalEList<?>) getPhysicalAssetClasses()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PHYSICAL_ASSET_MODEL__TEST_SPECIFICATIONS:
			return ((InternalEList<?>) getTestSpecifications()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PHYSICAL_ASSET_MODEL__EQUIPMENT_ASSET_MAPPINGS:
			return ((InternalEList<?>) getEquipmentAssetMappings()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PHYSICAL_ASSET_MODEL__TEST_RESULTS:
			return ((InternalEList<?>) getTestResults()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_MODEL__PHYSICAL_ASSETS:
			return getPhysicalAssets();
		case Iec62264Package.PHYSICAL_ASSET_MODEL__PHYSICAL_ASSET_CLASSES:
			return getPhysicalAssetClasses();
		case Iec62264Package.PHYSICAL_ASSET_MODEL__TEST_SPECIFICATIONS:
			return getTestSpecifications();
		case Iec62264Package.PHYSICAL_ASSET_MODEL__EQUIPMENT_ASSET_MAPPINGS:
			return getEquipmentAssetMappings();
		case Iec62264Package.PHYSICAL_ASSET_MODEL__TEST_RESULTS:
			return getTestResults();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_MODEL__PHYSICAL_ASSETS:
			getPhysicalAssets().clear();
			getPhysicalAssets().addAll((Collection<? extends PhysicalAsset>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_MODEL__PHYSICAL_ASSET_CLASSES:
			getPhysicalAssetClasses().clear();
			getPhysicalAssetClasses().addAll((Collection<? extends PhysicalAssetClass>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_MODEL__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			getTestSpecifications().addAll((Collection<? extends PhysicalAssetCapabilityTestSpecification>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_MODEL__EQUIPMENT_ASSET_MAPPINGS:
			getEquipmentAssetMappings().clear();
			getEquipmentAssetMappings().addAll((Collection<? extends EquipmentAssetMapping>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_MODEL__TEST_RESULTS:
			getTestResults().clear();
			getTestResults().addAll((Collection<? extends PhysicalAssetCapabilityTestResult>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_MODEL__PHYSICAL_ASSETS:
			getPhysicalAssets().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET_MODEL__PHYSICAL_ASSET_CLASSES:
			getPhysicalAssetClasses().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET_MODEL__TEST_SPECIFICATIONS:
			getTestSpecifications().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET_MODEL__EQUIPMENT_ASSET_MAPPINGS:
			getEquipmentAssetMappings().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET_MODEL__TEST_RESULTS:
			getTestResults().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_MODEL__PHYSICAL_ASSETS:
			return physicalAssets != null && !physicalAssets.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET_MODEL__PHYSICAL_ASSET_CLASSES:
			return physicalAssetClasses != null && !physicalAssetClasses.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET_MODEL__TEST_SPECIFICATIONS:
			return testSpecifications != null && !testSpecifications.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET_MODEL__EQUIPMENT_ASSET_MAPPINGS:
			return equipmentAssetMappings != null && !equipmentAssetMappings.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET_MODEL__TEST_RESULTS:
			return testResults != null && !testResults.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PhysicalAssetModelImpl
