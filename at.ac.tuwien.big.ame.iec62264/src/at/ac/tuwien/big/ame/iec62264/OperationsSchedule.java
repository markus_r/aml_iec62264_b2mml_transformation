/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.time.ZonedDateTime;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Schedule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getOperationsRequests <em>Operations Requests</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getScheduledState <em>Scheduled State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSchedule()
 * @model
 * @generated
 */
public interface OperationsSchedule extends EObject {
	/**
	 * Returns the value of the '<em><b>Operations Requests</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.OperationsRequest}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Requests</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Requests</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSchedule_OperationsRequests()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<OperationsRequest> getOperationsRequests();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSchedule_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSchedule_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.OperationsType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #setOperationsType(OperationsType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSchedule_OperationsType()
	 * @model required="true"
	 * @generated
	 */
	OperationsType getOperationsType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getOperationsType <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsType value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSchedule_StartTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSchedule_EndTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Published Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Published Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Published Date</em>' attribute.
	 * @see #setPublishedDate(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSchedule_PublishedDate()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getPublishedDate();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getPublishedDate <em>Published Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Published Date</em>' attribute.
	 * @see #getPublishedDate()
	 * @generated
	 */
	void setPublishedDate(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Scheduled State</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.ScheduleState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheduled State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheduled State</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.ScheduleState
	 * @see #setScheduledState(ScheduleState)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSchedule_ScheduledState()
	 * @model
	 * @generated
	 */
	ScheduleState getScheduledState();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getScheduledState <em>Scheduled State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheduled State</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.ScheduleState
	 * @see #getScheduledState()
	 * @generated
	 */
	void setScheduledState(ScheduleState value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #setHierarchyScope(HierarchyScope)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSchedule_HierarchyScope()
	 * @model
	 * @generated
	 */
	HierarchyScope getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getHierarchyScope <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScope value);

} // OperationsSchedule
