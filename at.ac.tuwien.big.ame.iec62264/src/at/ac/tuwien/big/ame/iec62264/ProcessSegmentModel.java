/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process Segment Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel#getProcessSegments <em>Process Segments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel#getProcessSegmentDependencies <em>Process Segment Dependencies</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentModel()
 * @model
 * @generated
 */
public interface ProcessSegmentModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Process Segments</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.ProcessSegment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segments</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentModel_ProcessSegments()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProcessSegment> getProcessSegments();

	/**
	 * Returns the value of the '<em><b>Process Segment Dependencies</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment Dependencies</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment Dependencies</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentModel_ProcessSegmentDependencies()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProcessSegmentDependency> getProcessSegmentDependencies();

} // ProcessSegmentModel
