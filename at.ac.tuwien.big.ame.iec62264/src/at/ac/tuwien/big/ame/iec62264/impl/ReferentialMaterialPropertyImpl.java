/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialLotProperty;
import at.ac.tuwien.big.ame.iec62264.ReferentialMaterialProperty;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Referential Material Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialMaterialPropertyImpl#getMaterialLotProperty <em>Material Lot Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ReferentialMaterialPropertyImpl extends ReferentialMaterialTypePropertyImpl
		implements ReferentialMaterialProperty {
	/**
	 * The cached value of the '{@link #getMaterialLotProperty() <em>Material Lot Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialLotProperty()
	 * @generated
	 * @ordered
	 */
	protected MaterialLotProperty materialLotProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferentialMaterialPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.REFERENTIAL_MATERIAL_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialLotProperty getMaterialLotProperty() {
		if (materialLotProperty != null && materialLotProperty.eIsProxy()) {
			InternalEObject oldMaterialLotProperty = (InternalEObject) materialLotProperty;
			materialLotProperty = (MaterialLotProperty) eResolveProxy(oldMaterialLotProperty);
			if (materialLotProperty != oldMaterialLotProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_LOT_PROPERTY,
							oldMaterialLotProperty, materialLotProperty));
			}
		}
		return materialLotProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialLotProperty basicGetMaterialLotProperty() {
		return materialLotProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialLotProperty(MaterialLotProperty newMaterialLotProperty) {
		MaterialLotProperty oldMaterialLotProperty = materialLotProperty;
		materialLotProperty = newMaterialLotProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_LOT_PROPERTY, oldMaterialLotProperty,
					materialLotProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_LOT_PROPERTY:
			if (resolve)
				return getMaterialLotProperty();
			return basicGetMaterialLotProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_LOT_PROPERTY:
			setMaterialLotProperty((MaterialLotProperty) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_LOT_PROPERTY:
			setMaterialLotProperty((MaterialLotProperty) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_LOT_PROPERTY:
			return materialLotProperty != null;
		}
		return super.eIsSet(featureID);
	}

} //ReferentialMaterialPropertyImpl
