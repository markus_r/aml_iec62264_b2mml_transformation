/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill;
import at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Material Bill</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillImpl#getOperationsMaterialBillItems <em>Operations Material Bill Items</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillImpl#getDescription <em>Description</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsMaterialBillImpl extends MinimalEObjectImpl.Container implements OperationsMaterialBill {
	/**
	 * The cached value of the '{@link #getOperationsMaterialBillItems() <em>Operations Material Bill Items</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsMaterialBillItems()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsMaterialBillItem> operationsMaterialBillItems;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsMaterialBillImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.OPERATIONS_MATERIAL_BILL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsMaterialBillItem> getOperationsMaterialBillItems() {
		if (operationsMaterialBillItems == null) {
			operationsMaterialBillItems = new EObjectContainmentEList<OperationsMaterialBillItem>(
					OperationsMaterialBillItem.class, this,
					Iec62264Package.OPERATIONS_MATERIAL_BILL__OPERATIONS_MATERIAL_BILL_ITEMS);
		}
		return operationsMaterialBillItems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_MATERIAL_BILL__ID, oldId,
					id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_MATERIAL_BILL__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__OPERATIONS_MATERIAL_BILL_ITEMS:
			return ((InternalEList<?>) getOperationsMaterialBillItems()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__OPERATIONS_MATERIAL_BILL_ITEMS:
			return getOperationsMaterialBillItems();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__ID:
			return getId();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__DESCRIPTION:
			return getDescription();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__OPERATIONS_MATERIAL_BILL_ITEMS:
			getOperationsMaterialBillItems().clear();
			getOperationsMaterialBillItems().addAll((Collection<? extends OperationsMaterialBillItem>) newValue);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__DESCRIPTION:
			setDescription((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__OPERATIONS_MATERIAL_BILL_ITEMS:
			getOperationsMaterialBillItems().clear();
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__OPERATIONS_MATERIAL_BILL_ITEMS:
			return operationsMaterialBillItems != null && !operationsMaterialBillItems.isEmpty();
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.OPERATIONS_MATERIAL_BILL__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(')');
		return result.toString();
	}

} //OperationsMaterialBillImpl
