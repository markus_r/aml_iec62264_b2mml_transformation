/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.OperationsPerformance;
import at.ac.tuwien.big.ame.iec62264.OperationsResponse;
import at.ac.tuwien.big.ame.iec62264.OperationsSchedule;
import at.ac.tuwien.big.ame.iec62264.OperationsType;
import at.ac.tuwien.big.ame.iec62264.PerformanceState;

import java.time.ZonedDateTime;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Performance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl#getOperationsResponses <em>Operations Responses</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl#getOperationsSchedule <em>Operations Schedule</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl#getPerformanceState <em>Performance State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl#getPublishedDate <em>Published Date</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsPerformanceImpl extends MinimalEObjectImpl.Container implements OperationsPerformance {
	/**
	 * The cached value of the '{@link #getOperationsResponses() <em>Operations Responses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsResponses()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsResponse> operationsResponses;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected static final OperationsType OPERATIONS_TYPE_EDEFAULT = OperationsType.PRODUCTION;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsType operationsType = OPERATIONS_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime startTime = START_TIME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOperationsSchedule() <em>Operations Schedule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsSchedule()
	 * @generated
	 * @ordered
	 */
	protected OperationsSchedule operationsSchedule;

	/**
	 * The default value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime END_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime endTime = END_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPerformanceState() <em>Performance State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPerformanceState()
	 * @generated
	 * @ordered
	 */
	protected static final PerformanceState PERFORMANCE_STATE_EDEFAULT = PerformanceState.READY;

	/**
	 * The cached value of the '{@link #getPerformanceState() <em>Performance State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPerformanceState()
	 * @generated
	 * @ordered
	 */
	protected PerformanceState performanceState = PERFORMANCE_STATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScope hierarchyScope;

	/**
	 * The default value of the '{@link #getPublishedDate() <em>Published Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublishedDate()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime PUBLISHED_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPublishedDate() <em>Published Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublishedDate()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime publishedDate = PUBLISHED_DATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsPerformanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.OPERATIONS_PERFORMANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsResponse> getOperationsResponses() {
		if (operationsResponses == null) {
			operationsResponses = new EObjectContainmentEList<OperationsResponse>(OperationsResponse.class, this,
					Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_RESPONSES);
		}
		return operationsResponses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_PERFORMANCE__ID, oldId,
					id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_PERFORMANCE__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsType newOperationsType) {
		OperationsType oldOperationsType = operationsType;
		operationsType = newOperationsType == null ? OPERATIONS_TYPE_EDEFAULT : newOperationsType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_TYPE, oldOperationsType, operationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(ZonedDateTime newStartTime) {
		ZonedDateTime oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_PERFORMANCE__START_TIME,
					oldStartTime, startTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsSchedule getOperationsSchedule() {
		if (operationsSchedule != null && operationsSchedule.eIsProxy()) {
			InternalEObject oldOperationsSchedule = (InternalEObject) operationsSchedule;
			operationsSchedule = (OperationsSchedule) eResolveProxy(oldOperationsSchedule);
			if (operationsSchedule != oldOperationsSchedule) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_SCHEDULE, oldOperationsSchedule,
							operationsSchedule));
			}
		}
		return operationsSchedule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsSchedule basicGetOperationsSchedule() {
		return operationsSchedule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsSchedule(OperationsSchedule newOperationsSchedule) {
		OperationsSchedule oldOperationsSchedule = operationsSchedule;
		operationsSchedule = newOperationsSchedule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_SCHEDULE, oldOperationsSchedule,
					operationsSchedule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(ZonedDateTime newEndTime) {
		ZonedDateTime oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.OPERATIONS_PERFORMANCE__END_TIME,
					oldEndTime, endTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerformanceState getPerformanceState() {
		return performanceState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPerformanceState(PerformanceState newPerformanceState) {
		PerformanceState oldPerformanceState = performanceState;
		performanceState = newPerformanceState == null ? PERFORMANCE_STATE_EDEFAULT : newPerformanceState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_PERFORMANCE__PERFORMANCE_STATE, oldPerformanceState, performanceState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope getHierarchyScope() {
		if (hierarchyScope != null && hierarchyScope.eIsProxy()) {
			InternalEObject oldHierarchyScope = (InternalEObject) hierarchyScope;
			hierarchyScope = (HierarchyScope) eResolveProxy(oldHierarchyScope);
			if (hierarchyScope != oldHierarchyScope) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.OPERATIONS_PERFORMANCE__HIERARCHY_SCOPE, oldHierarchyScope,
							hierarchyScope));
			}
		}
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope basicGetHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScope newHierarchyScope) {
		HierarchyScope oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_PERFORMANCE__HIERARCHY_SCOPE, oldHierarchyScope, hierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getPublishedDate() {
		return publishedDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublishedDate(ZonedDateTime newPublishedDate) {
		ZonedDateTime oldPublishedDate = publishedDate;
		publishedDate = newPublishedDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.OPERATIONS_PERFORMANCE__PUBLISHED_DATE, oldPublishedDate, publishedDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_RESPONSES:
			return ((InternalEList<?>) getOperationsResponses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_RESPONSES:
			return getOperationsResponses();
		case Iec62264Package.OPERATIONS_PERFORMANCE__ID:
			return getId();
		case Iec62264Package.OPERATIONS_PERFORMANCE__DESCRIPTION:
			return getDescription();
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_TYPE:
			return getOperationsType();
		case Iec62264Package.OPERATIONS_PERFORMANCE__START_TIME:
			return getStartTime();
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_SCHEDULE:
			if (resolve)
				return getOperationsSchedule();
			return basicGetOperationsSchedule();
		case Iec62264Package.OPERATIONS_PERFORMANCE__END_TIME:
			return getEndTime();
		case Iec62264Package.OPERATIONS_PERFORMANCE__PERFORMANCE_STATE:
			return getPerformanceState();
		case Iec62264Package.OPERATIONS_PERFORMANCE__HIERARCHY_SCOPE:
			if (resolve)
				return getHierarchyScope();
			return basicGetHierarchyScope();
		case Iec62264Package.OPERATIONS_PERFORMANCE__PUBLISHED_DATE:
			return getPublishedDate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_RESPONSES:
			getOperationsResponses().clear();
			getOperationsResponses().addAll((Collection<? extends OperationsResponse>) newValue);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_TYPE:
			setOperationsType((OperationsType) newValue);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__START_TIME:
			setStartTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_SCHEDULE:
			setOperationsSchedule((OperationsSchedule) newValue);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__END_TIME:
			setEndTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__PERFORMANCE_STATE:
			setPerformanceState((PerformanceState) newValue);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) newValue);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__PUBLISHED_DATE:
			setPublishedDate((ZonedDateTime) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_RESPONSES:
			getOperationsResponses().clear();
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_TYPE:
			setOperationsType(OPERATIONS_TYPE_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__START_TIME:
			setStartTime(START_TIME_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_SCHEDULE:
			setOperationsSchedule((OperationsSchedule) null);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__END_TIME:
			setEndTime(END_TIME_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__PERFORMANCE_STATE:
			setPerformanceState(PERFORMANCE_STATE_EDEFAULT);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) null);
			return;
		case Iec62264Package.OPERATIONS_PERFORMANCE__PUBLISHED_DATE:
			setPublishedDate(PUBLISHED_DATE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_RESPONSES:
			return operationsResponses != null && !operationsResponses.isEmpty();
		case Iec62264Package.OPERATIONS_PERFORMANCE__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.OPERATIONS_PERFORMANCE__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_TYPE:
			return operationsType != OPERATIONS_TYPE_EDEFAULT;
		case Iec62264Package.OPERATIONS_PERFORMANCE__START_TIME:
			return START_TIME_EDEFAULT == null ? startTime != null : !START_TIME_EDEFAULT.equals(startTime);
		case Iec62264Package.OPERATIONS_PERFORMANCE__OPERATIONS_SCHEDULE:
			return operationsSchedule != null;
		case Iec62264Package.OPERATIONS_PERFORMANCE__END_TIME:
			return END_TIME_EDEFAULT == null ? endTime != null : !END_TIME_EDEFAULT.equals(endTime);
		case Iec62264Package.OPERATIONS_PERFORMANCE__PERFORMANCE_STATE:
			return performanceState != PERFORMANCE_STATE_EDEFAULT;
		case Iec62264Package.OPERATIONS_PERFORMANCE__HIERARCHY_SCOPE:
			return hierarchyScope != null;
		case Iec62264Package.OPERATIONS_PERFORMANCE__PUBLISHED_DATE:
			return PUBLISHED_DATE_EDEFAULT == null ? publishedDate != null
					: !PUBLISHED_DATE_EDEFAULT.equals(publishedDate);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", operationsType: ");
		result.append(operationsType);
		result.append(", startTime: ");
		result.append(startTime);
		result.append(", endTime: ");
		result.append(endTime);
		result.append(", performanceState: ");
		result.append(performanceState);
		result.append(", publishedDate: ");
		result.append(publishedDate);
		result.append(')');
		return result.toString();
	}

} //OperationsPerformanceImpl
