/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Equipment;
import at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.PhysicalAsset;

import java.time.ZonedDateTime;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Asset Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentAssetMappingImpl#getPhysicalAsset <em>Physical Asset</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentAssetMappingImpl#getEquipment <em>Equipment</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentAssetMappingImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentAssetMappingImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentAssetMappingImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentAssetMappingImpl#getEndTime <em>End Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EquipmentAssetMappingImpl extends MinimalEObjectImpl.Container implements EquipmentAssetMapping {
	/**
	 * The cached value of the '{@link #getPhysicalAsset() <em>Physical Asset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAsset()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAsset physicalAsset;

	/**
	 * The cached value of the '{@link #getEquipment() <em>Equipment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipment()
	 * @generated
	 * @ordered
	 */
	protected Equipment equipment;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime startTime = START_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime END_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime endTime = END_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentAssetMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.EQUIPMENT_ASSET_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAsset getPhysicalAsset() {
		if (physicalAsset != null && physicalAsset.eIsProxy()) {
			InternalEObject oldPhysicalAsset = (InternalEObject) physicalAsset;
			physicalAsset = (PhysicalAsset) eResolveProxy(oldPhysicalAsset);
			if (physicalAsset != oldPhysicalAsset) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.EQUIPMENT_ASSET_MAPPING__PHYSICAL_ASSET, oldPhysicalAsset, physicalAsset));
			}
		}
		return physicalAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAsset basicGetPhysicalAsset() {
		return physicalAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAsset(PhysicalAsset newPhysicalAsset) {
		PhysicalAsset oldPhysicalAsset = physicalAsset;
		physicalAsset = newPhysicalAsset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.EQUIPMENT_ASSET_MAPPING__PHYSICAL_ASSET, oldPhysicalAsset, physicalAsset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Equipment getEquipment() {
		if (equipment != null && equipment.eIsProxy()) {
			InternalEObject oldEquipment = (InternalEObject) equipment;
			equipment = (Equipment) eResolveProxy(oldEquipment);
			if (equipment != oldEquipment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.EQUIPMENT_ASSET_MAPPING__EQUIPMENT, oldEquipment, equipment));
			}
		}
		return equipment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Equipment basicGetEquipment() {
		return equipment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipment(Equipment newEquipment) {
		Equipment oldEquipment = equipment;
		equipment = newEquipment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_ASSET_MAPPING__EQUIPMENT,
					oldEquipment, equipment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_ASSET_MAPPING__ID, oldId,
					id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_ASSET_MAPPING__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(ZonedDateTime newStartTime) {
		ZonedDateTime oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_ASSET_MAPPING__START_TIME,
					oldStartTime, startTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(ZonedDateTime newEndTime) {
		ZonedDateTime oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_ASSET_MAPPING__END_TIME,
					oldEndTime, endTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__PHYSICAL_ASSET:
			if (resolve)
				return getPhysicalAsset();
			return basicGetPhysicalAsset();
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__EQUIPMENT:
			if (resolve)
				return getEquipment();
			return basicGetEquipment();
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__ID:
			return getId();
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__DESCRIPTION:
			return getDescription();
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__START_TIME:
			return getStartTime();
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__END_TIME:
			return getEndTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__PHYSICAL_ASSET:
			setPhysicalAsset((PhysicalAsset) newValue);
			return;
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__EQUIPMENT:
			setEquipment((Equipment) newValue);
			return;
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__START_TIME:
			setStartTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__END_TIME:
			setEndTime((ZonedDateTime) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__PHYSICAL_ASSET:
			setPhysicalAsset((PhysicalAsset) null);
			return;
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__EQUIPMENT:
			setEquipment((Equipment) null);
			return;
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__START_TIME:
			setStartTime(START_TIME_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__END_TIME:
			setEndTime(END_TIME_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__PHYSICAL_ASSET:
			return physicalAsset != null;
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__EQUIPMENT:
			return equipment != null;
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__START_TIME:
			return START_TIME_EDEFAULT == null ? startTime != null : !START_TIME_EDEFAULT.equals(startTime);
		case Iec62264Package.EQUIPMENT_ASSET_MAPPING__END_TIME:
			return END_TIME_EDEFAULT == null ? endTime != null : !END_TIME_EDEFAULT.equals(endTime);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", startTime: ");
		result.append(startTime);
		result.append(", endTime: ");
		result.append(endTime);
		result.append(')');
		return result.toString();
	}

} //EquipmentAssetMappingImpl
