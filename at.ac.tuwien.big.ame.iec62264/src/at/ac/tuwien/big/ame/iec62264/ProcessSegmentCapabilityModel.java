/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process Segment Capability Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapabilityModel#getProcessSegmentCapabilities <em>Process Segment Capabilities</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapabilityModel()
 * @model
 * @generated
 */
public interface ProcessSegmentCapabilityModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Process Segment Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment Capabilities</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getProcessSegmentCapabilityModel_ProcessSegmentCapabilities()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProcessSegmentCapability> getProcessSegmentCapabilities();

} // ProcessSegmentCapabilityModel
