/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.math.BigDecimal;

import java.time.ZonedDateTime;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Capability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAssetCapabilityProperty <em>Physical Asset Capability Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAssetClass <em>Physical Asset Class</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAsset <em>Physical Asset</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getReason <em>Reason</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAssetUse <em>Physical Asset Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability()
 * @model
 * @generated
 */
public interface PhysicalAssetCapability extends EObject {
	/**
	 * Returns the value of the '<em><b>Physical Asset Capability Property</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Capability Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Capability Property</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_PhysicalAssetCapabilityProperty()
	 * @model containment="true"
	 * @generated
	 */
	EList<PhysicalAssetCapabilityProperty> getPhysicalAssetCapabilityProperty();

	/**
	 * Returns the value of the '<em><b>Physical Asset Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Class</em>' reference.
	 * @see #setPhysicalAssetClass(PhysicalAssetClass)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_PhysicalAssetClass()
	 * @model
	 * @generated
	 */
	PhysicalAssetClass getPhysicalAssetClass();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAssetClass <em>Physical Asset Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Class</em>' reference.
	 * @see #getPhysicalAssetClass()
	 * @generated
	 */
	void setPhysicalAssetClass(PhysicalAssetClass value);

	/**
	 * Returns the value of the '<em><b>Physical Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset</em>' reference.
	 * @see #setPhysicalAsset(PhysicalAsset)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_PhysicalAsset()
	 * @model
	 * @generated
	 */
	PhysicalAsset getPhysicalAsset();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAsset <em>Physical Asset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset</em>' reference.
	 * @see #getPhysicalAsset()
	 * @generated
	 */
	void setPhysicalAsset(PhysicalAsset value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Capability Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.CapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capability Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capability Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.CapabilityType
	 * @see #setCapabilityType(CapabilityType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_CapabilityType()
	 * @model
	 * @generated
	 */
	CapabilityType getCapabilityType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getCapabilityType <em>Capability Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Capability Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.CapabilityType
	 * @see #getCapabilityType()
	 * @generated
	 */
	void setCapabilityType(CapabilityType value);

	/**
	 * Returns the value of the '<em><b>Reason</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reason</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reason</em>' attribute.
	 * @see #setReason(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_Reason()
	 * @model
	 * @generated
	 */
	String getReason();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getReason <em>Reason</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reason</em>' attribute.
	 * @see #getReason()
	 * @generated
	 */
	void setReason(String value);

	/**
	 * Returns the value of the '<em><b>Confidence Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Confidence Factor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confidence Factor</em>' attribute.
	 * @see #setConfidenceFactor(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_ConfidenceFactor()
	 * @model
	 * @generated
	 */
	String getConfidenceFactor();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getConfidenceFactor <em>Confidence Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confidence Factor</em>' attribute.
	 * @see #getConfidenceFactor()
	 * @generated
	 */
	void setConfidenceFactor(String value);

	/**
	 * Returns the value of the '<em><b>Physical Asset Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Use</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Use</em>' attribute.
	 * @see #setPhysicalAssetUse(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_PhysicalAssetUse()
	 * @model
	 * @generated
	 */
	String getPhysicalAssetUse();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAssetUse <em>Physical Asset Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Use</em>' attribute.
	 * @see #getPhysicalAssetUse()
	 * @generated
	 */
	void setPhysicalAssetUse(String value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #setHierarchyScope(HierarchyScope)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_HierarchyScope()
	 * @model
	 * @generated
	 */
	HierarchyScope getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getHierarchyScope <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScope value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_StartTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_EndTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(BigDecimal)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_Quantity()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 * @generated
	 */
	BigDecimal getQuantity();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getQuantity <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity Unit Of Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity Unit Of Measure</em>' attribute.
	 * @see #setQuantityUnitOfMeasure(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPhysicalAssetCapability_QuantityUnitOfMeasure()
	 * @model
	 * @generated
	 */
	String getQuantityUnitOfMeasure();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity Unit Of Measure</em>' attribute.
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 */
	void setQuantityUnitOfMeasure(String value);

} // PhysicalAssetCapability
