/**
 */
package at.ac.tuwien.big.ame.iec62264;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Referential Equipment Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty#getEquipmentClassProperty <em>Equipment Class Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty#getEquipmentProperty <em>Equipment Property</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialEquipmentProperty()
 * @model abstract="true"
 * @generated
 */
public interface ReferentialEquipmentProperty extends ReferencialProperty {
	/**
	 * Returns the value of the '<em><b>Equipment Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Class Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Class Property</em>' reference.
	 * @see #setEquipmentClassProperty(EquipmentClassProperty)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialEquipmentProperty_EquipmentClassProperty()
	 * @model
	 * @generated
	 */
	EquipmentClassProperty getEquipmentClassProperty();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty#getEquipmentClassProperty <em>Equipment Class Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Class Property</em>' reference.
	 * @see #getEquipmentClassProperty()
	 * @generated
	 */
	void setEquipmentClassProperty(EquipmentClassProperty value);

	/**
	 * Returns the value of the '<em><b>Equipment Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Property</em>' reference.
	 * @see #setEquipmentProperty(EquipmentProperty)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialEquipmentProperty_EquipmentProperty()
	 * @model
	 * @generated
	 */
	EquipmentProperty getEquipmentProperty();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty#getEquipmentProperty <em>Equipment Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Property</em>' reference.
	 * @see #getEquipmentProperty()
	 * @generated
	 */
	void setEquipmentProperty(EquipmentProperty value);

} // ReferentialEquipmentProperty
