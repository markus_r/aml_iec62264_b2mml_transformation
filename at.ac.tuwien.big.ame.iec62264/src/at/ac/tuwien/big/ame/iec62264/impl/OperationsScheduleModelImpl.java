/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.OperationsSchedule;
import at.ac.tuwien.big.ame.iec62264.OperationsScheduleModel;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Schedule Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsScheduleModelImpl#getOperationsSchedules <em>Operations Schedules</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsScheduleModelImpl extends MinimalEObjectImpl.Container implements OperationsScheduleModel {
	/**
	 * The cached value of the '{@link #getOperationsSchedules() <em>Operations Schedules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsSchedules()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsSchedule> operationsSchedules;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsScheduleModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.OPERATIONS_SCHEDULE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsSchedule> getOperationsSchedules() {
		if (operationsSchedules == null) {
			operationsSchedules = new EObjectContainmentEList<OperationsSchedule>(OperationsSchedule.class, this,
					Iec62264Package.OPERATIONS_SCHEDULE_MODEL__OPERATIONS_SCHEDULES);
		}
		return operationsSchedules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SCHEDULE_MODEL__OPERATIONS_SCHEDULES:
			return ((InternalEList<?>) getOperationsSchedules()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SCHEDULE_MODEL__OPERATIONS_SCHEDULES:
			return getOperationsSchedules();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SCHEDULE_MODEL__OPERATIONS_SCHEDULES:
			getOperationsSchedules().clear();
			getOperationsSchedules().addAll((Collection<? extends OperationsSchedule>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SCHEDULE_MODEL__OPERATIONS_SCHEDULES:
			getOperationsSchedules().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_SCHEDULE_MODEL__OPERATIONS_SCHEDULES:
			return operationsSchedules != null && !operationsSchedules.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OperationsScheduleModelImpl
