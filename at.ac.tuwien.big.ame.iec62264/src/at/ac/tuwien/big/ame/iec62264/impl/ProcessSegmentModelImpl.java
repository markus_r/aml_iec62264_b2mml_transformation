/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.ProcessSegment;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process Segment Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentModelImpl#getProcessSegments <em>Process Segments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentModelImpl#getProcessSegmentDependencies <em>Process Segment Dependencies</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcessSegmentModelImpl extends MinimalEObjectImpl.Container implements ProcessSegmentModel {
	/**
	 * The cached value of the '{@link #getProcessSegments() <em>Process Segments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegments()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessSegment> processSegments;

	/**
	 * The cached value of the '{@link #getProcessSegmentDependencies() <em>Process Segment Dependencies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegmentDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessSegmentDependency> processSegmentDependencies;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessSegmentModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PROCESS_SEGMENT_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessSegment> getProcessSegments() {
		if (processSegments == null) {
			processSegments = new EObjectContainmentEList<ProcessSegment>(ProcessSegment.class, this,
					Iec62264Package.PROCESS_SEGMENT_MODEL__PROCESS_SEGMENTS);
		}
		return processSegments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessSegmentDependency> getProcessSegmentDependencies() {
		if (processSegmentDependencies == null) {
			processSegmentDependencies = new EObjectContainmentEList<ProcessSegmentDependency>(
					ProcessSegmentDependency.class, this,
					Iec62264Package.PROCESS_SEGMENT_MODEL__PROCESS_SEGMENT_DEPENDENCIES);
		}
		return processSegmentDependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_MODEL__PROCESS_SEGMENTS:
			return ((InternalEList<?>) getProcessSegments()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PROCESS_SEGMENT_MODEL__PROCESS_SEGMENT_DEPENDENCIES:
			return ((InternalEList<?>) getProcessSegmentDependencies()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_MODEL__PROCESS_SEGMENTS:
			return getProcessSegments();
		case Iec62264Package.PROCESS_SEGMENT_MODEL__PROCESS_SEGMENT_DEPENDENCIES:
			return getProcessSegmentDependencies();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_MODEL__PROCESS_SEGMENTS:
			getProcessSegments().clear();
			getProcessSegments().addAll((Collection<? extends ProcessSegment>) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT_MODEL__PROCESS_SEGMENT_DEPENDENCIES:
			getProcessSegmentDependencies().clear();
			getProcessSegmentDependencies().addAll((Collection<? extends ProcessSegmentDependency>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_MODEL__PROCESS_SEGMENTS:
			getProcessSegments().clear();
			return;
		case Iec62264Package.PROCESS_SEGMENT_MODEL__PROCESS_SEGMENT_DEPENDENCIES:
			getProcessSegmentDependencies().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_MODEL__PROCESS_SEGMENTS:
			return processSegments != null && !processSegments.isEmpty();
		case Iec62264Package.PROCESS_SEGMENT_MODEL__PROCESS_SEGMENT_DEPENDENCIES:
			return processSegmentDependencies != null && !processSegmentDependencies.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ProcessSegmentModelImpl
