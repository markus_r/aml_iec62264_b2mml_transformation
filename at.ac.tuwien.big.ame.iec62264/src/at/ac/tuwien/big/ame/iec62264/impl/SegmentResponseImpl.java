/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.EquipmentActual;
import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialActual;
import at.ac.tuwien.big.ame.iec62264.OperationsDefinition;
import at.ac.tuwien.big.ame.iec62264.OperationsType;
import at.ac.tuwien.big.ame.iec62264.PerformanceState;
import at.ac.tuwien.big.ame.iec62264.PersonnelActual;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual;
import at.ac.tuwien.big.ame.iec62264.ProcessSegment;
import at.ac.tuwien.big.ame.iec62264.SegmentData;
import at.ac.tuwien.big.ame.iec62264.SegmentResponse;

import java.time.ZonedDateTime;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Segment Response</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getSubResponses <em>Sub Responses</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getProcessSegment <em>Process Segment</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getOperationsDefinition <em>Operations Definition</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getSegmentData <em>Segment Data</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getPersonnelActuals <em>Personnel Actuals</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getEquipmentActuals <em>Equipment Actuals</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getPhysicalAssetActuals <em>Physical Asset Actuals</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getMaterialActuals <em>Material Actuals</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getActualStartTime <em>Actual Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getActualEndTime <em>Actual End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getSegmentState <em>Segment State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SegmentResponseImpl extends MinimalEObjectImpl.Container implements SegmentResponse {
	/**
	 * The cached value of the '{@link #getSubResponses() <em>Sub Responses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubResponses()
	 * @generated
	 * @ordered
	 */
	protected EList<SegmentResponse> subResponses;

	/**
	 * The cached value of the '{@link #getProcessSegment() <em>Process Segment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegment()
	 * @generated
	 * @ordered
	 */
	protected ProcessSegment processSegment;

	/**
	 * The cached value of the '{@link #getOperationsDefinition() <em>Operations Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsDefinition()
	 * @generated
	 * @ordered
	 */
	protected OperationsDefinition operationsDefinition;

	/**
	 * The cached value of the '{@link #getSegmentData() <em>Segment Data</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentData()
	 * @generated
	 * @ordered
	 */
	protected EList<SegmentData> segmentData;

	/**
	 * The cached value of the '{@link #getPersonnelActuals() <em>Personnel Actuals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelActuals()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelActual> personnelActuals;

	/**
	 * The cached value of the '{@link #getEquipmentActuals() <em>Equipment Actuals</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentActuals()
	 * @generated
	 * @ordered
	 */
	protected EquipmentActual equipmentActuals;

	/**
	 * The cached value of the '{@link #getPhysicalAssetActuals() <em>Physical Asset Actuals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetActuals()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetActual> physicalAssetActuals;

	/**
	 * The cached value of the '{@link #getMaterialActuals() <em>Material Actuals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialActuals()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialActual> materialActuals;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected static final OperationsType OPERATIONS_TYPE_EDEFAULT = OperationsType.PRODUCTION;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsType operationsType = OPERATIONS_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getActualStartTime() <em>Actual Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime ACTUAL_START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getActualStartTime() <em>Actual Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualStartTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime actualStartTime = ACTUAL_START_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getActualEndTime() <em>Actual End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualEndTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime ACTUAL_END_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getActualEndTime() <em>Actual End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualEndTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime actualEndTime = ACTUAL_END_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSegmentState() <em>Segment State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentState()
	 * @generated
	 * @ordered
	 */
	protected static final PerformanceState SEGMENT_STATE_EDEFAULT = PerformanceState.READY;

	/**
	 * The cached value of the '{@link #getSegmentState() <em>Segment State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentState()
	 * @generated
	 * @ordered
	 */
	protected PerformanceState segmentState = SEGMENT_STATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScope hierarchyScope;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SegmentResponseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.SEGMENT_RESPONSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SegmentResponse> getSubResponses() {
		if (subResponses == null) {
			subResponses = new EObjectResolvingEList<SegmentResponse>(SegmentResponse.class, this,
					Iec62264Package.SEGMENT_RESPONSE__SUB_RESPONSES);
		}
		return subResponses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegment getProcessSegment() {
		if (processSegment != null && processSegment.eIsProxy()) {
			InternalEObject oldProcessSegment = (InternalEObject) processSegment;
			processSegment = (ProcessSegment) eResolveProxy(oldProcessSegment);
			if (processSegment != oldProcessSegment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.SEGMENT_RESPONSE__PROCESS_SEGMENT, oldProcessSegment, processSegment));
			}
		}
		return processSegment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegment basicGetProcessSegment() {
		return processSegment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessSegment(ProcessSegment newProcessSegment) {
		ProcessSegment oldProcessSegment = processSegment;
		processSegment = newProcessSegment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_RESPONSE__PROCESS_SEGMENT,
					oldProcessSegment, processSegment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinition getOperationsDefinition() {
		if (operationsDefinition != null && operationsDefinition.eIsProxy()) {
			InternalEObject oldOperationsDefinition = (InternalEObject) operationsDefinition;
			operationsDefinition = (OperationsDefinition) eResolveProxy(oldOperationsDefinition);
			if (operationsDefinition != oldOperationsDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.SEGMENT_RESPONSE__OPERATIONS_DEFINITION, oldOperationsDefinition,
							operationsDefinition));
			}
		}
		return operationsDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinition basicGetOperationsDefinition() {
		return operationsDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsDefinition(OperationsDefinition newOperationsDefinition) {
		OperationsDefinition oldOperationsDefinition = operationsDefinition;
		operationsDefinition = newOperationsDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.SEGMENT_RESPONSE__OPERATIONS_DEFINITION, oldOperationsDefinition,
					operationsDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SegmentData> getSegmentData() {
		if (segmentData == null) {
			segmentData = new EObjectContainmentEList<SegmentData>(SegmentData.class, this,
					Iec62264Package.SEGMENT_RESPONSE__SEGMENT_DATA);
		}
		return segmentData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelActual> getPersonnelActuals() {
		if (personnelActuals == null) {
			personnelActuals = new EObjectContainmentEList<PersonnelActual>(PersonnelActual.class, this,
					Iec62264Package.SEGMENT_RESPONSE__PERSONNEL_ACTUALS);
		}
		return personnelActuals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentActual getEquipmentActuals() {
		if (equipmentActuals != null && equipmentActuals.eIsProxy()) {
			InternalEObject oldEquipmentActuals = (InternalEObject) equipmentActuals;
			equipmentActuals = (EquipmentActual) eResolveProxy(oldEquipmentActuals);
			if (equipmentActuals != oldEquipmentActuals) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.SEGMENT_RESPONSE__EQUIPMENT_ACTUALS, oldEquipmentActuals,
							equipmentActuals));
			}
		}
		return equipmentActuals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentActual basicGetEquipmentActuals() {
		return equipmentActuals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentActuals(EquipmentActual newEquipmentActuals) {
		EquipmentActual oldEquipmentActuals = equipmentActuals;
		equipmentActuals = newEquipmentActuals;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_RESPONSE__EQUIPMENT_ACTUALS,
					oldEquipmentActuals, equipmentActuals));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetActual> getPhysicalAssetActuals() {
		if (physicalAssetActuals == null) {
			physicalAssetActuals = new EObjectContainmentEList<PhysicalAssetActual>(PhysicalAssetActual.class, this,
					Iec62264Package.SEGMENT_RESPONSE__PHYSICAL_ASSET_ACTUALS);
		}
		return physicalAssetActuals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialActual> getMaterialActuals() {
		if (materialActuals == null) {
			materialActuals = new EObjectContainmentEList<MaterialActual>(MaterialActual.class, this,
					Iec62264Package.SEGMENT_RESPONSE__MATERIAL_ACTUALS);
		}
		return materialActuals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_RESPONSE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_RESPONSE__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsType newOperationsType) {
		OperationsType oldOperationsType = operationsType;
		operationsType = newOperationsType == null ? OPERATIONS_TYPE_EDEFAULT : newOperationsType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_RESPONSE__OPERATIONS_TYPE,
					oldOperationsType, operationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getActualStartTime() {
		return actualStartTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActualStartTime(ZonedDateTime newActualStartTime) {
		ZonedDateTime oldActualStartTime = actualStartTime;
		actualStartTime = newActualStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_RESPONSE__ACTUAL_START_TIME,
					oldActualStartTime, actualStartTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getActualEndTime() {
		return actualEndTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActualEndTime(ZonedDateTime newActualEndTime) {
		ZonedDateTime oldActualEndTime = actualEndTime;
		actualEndTime = newActualEndTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_RESPONSE__ACTUAL_END_TIME,
					oldActualEndTime, actualEndTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerformanceState getSegmentState() {
		return segmentState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSegmentState(PerformanceState newSegmentState) {
		PerformanceState oldSegmentState = segmentState;
		segmentState = newSegmentState == null ? SEGMENT_STATE_EDEFAULT : newSegmentState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_RESPONSE__SEGMENT_STATE,
					oldSegmentState, segmentState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope getHierarchyScope() {
		if (hierarchyScope != null && hierarchyScope.eIsProxy()) {
			InternalEObject oldHierarchyScope = (InternalEObject) hierarchyScope;
			hierarchyScope = (HierarchyScope) eResolveProxy(oldHierarchyScope);
			if (hierarchyScope != oldHierarchyScope) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.SEGMENT_RESPONSE__HIERARCHY_SCOPE, oldHierarchyScope, hierarchyScope));
			}
		}
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope basicGetHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScope newHierarchyScope) {
		HierarchyScope oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_RESPONSE__HIERARCHY_SCOPE,
					oldHierarchyScope, hierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.SEGMENT_RESPONSE__SEGMENT_DATA:
			return ((InternalEList<?>) getSegmentData()).basicRemove(otherEnd, msgs);
		case Iec62264Package.SEGMENT_RESPONSE__PERSONNEL_ACTUALS:
			return ((InternalEList<?>) getPersonnelActuals()).basicRemove(otherEnd, msgs);
		case Iec62264Package.SEGMENT_RESPONSE__PHYSICAL_ASSET_ACTUALS:
			return ((InternalEList<?>) getPhysicalAssetActuals()).basicRemove(otherEnd, msgs);
		case Iec62264Package.SEGMENT_RESPONSE__MATERIAL_ACTUALS:
			return ((InternalEList<?>) getMaterialActuals()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.SEGMENT_RESPONSE__SUB_RESPONSES:
			return getSubResponses();
		case Iec62264Package.SEGMENT_RESPONSE__PROCESS_SEGMENT:
			if (resolve)
				return getProcessSegment();
			return basicGetProcessSegment();
		case Iec62264Package.SEGMENT_RESPONSE__OPERATIONS_DEFINITION:
			if (resolve)
				return getOperationsDefinition();
			return basicGetOperationsDefinition();
		case Iec62264Package.SEGMENT_RESPONSE__SEGMENT_DATA:
			return getSegmentData();
		case Iec62264Package.SEGMENT_RESPONSE__PERSONNEL_ACTUALS:
			return getPersonnelActuals();
		case Iec62264Package.SEGMENT_RESPONSE__EQUIPMENT_ACTUALS:
			if (resolve)
				return getEquipmentActuals();
			return basicGetEquipmentActuals();
		case Iec62264Package.SEGMENT_RESPONSE__PHYSICAL_ASSET_ACTUALS:
			return getPhysicalAssetActuals();
		case Iec62264Package.SEGMENT_RESPONSE__MATERIAL_ACTUALS:
			return getMaterialActuals();
		case Iec62264Package.SEGMENT_RESPONSE__ID:
			return getId();
		case Iec62264Package.SEGMENT_RESPONSE__DESCRIPTION:
			return getDescription();
		case Iec62264Package.SEGMENT_RESPONSE__OPERATIONS_TYPE:
			return getOperationsType();
		case Iec62264Package.SEGMENT_RESPONSE__ACTUAL_START_TIME:
			return getActualStartTime();
		case Iec62264Package.SEGMENT_RESPONSE__ACTUAL_END_TIME:
			return getActualEndTime();
		case Iec62264Package.SEGMENT_RESPONSE__SEGMENT_STATE:
			return getSegmentState();
		case Iec62264Package.SEGMENT_RESPONSE__HIERARCHY_SCOPE:
			if (resolve)
				return getHierarchyScope();
			return basicGetHierarchyScope();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.SEGMENT_RESPONSE__SUB_RESPONSES:
			getSubResponses().clear();
			getSubResponses().addAll((Collection<? extends SegmentResponse>) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__PROCESS_SEGMENT:
			setProcessSegment((ProcessSegment) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__OPERATIONS_DEFINITION:
			setOperationsDefinition((OperationsDefinition) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__SEGMENT_DATA:
			getSegmentData().clear();
			getSegmentData().addAll((Collection<? extends SegmentData>) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__PERSONNEL_ACTUALS:
			getPersonnelActuals().clear();
			getPersonnelActuals().addAll((Collection<? extends PersonnelActual>) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__EQUIPMENT_ACTUALS:
			setEquipmentActuals((EquipmentActual) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__PHYSICAL_ASSET_ACTUALS:
			getPhysicalAssetActuals().clear();
			getPhysicalAssetActuals().addAll((Collection<? extends PhysicalAssetActual>) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__MATERIAL_ACTUALS:
			getMaterialActuals().clear();
			getMaterialActuals().addAll((Collection<? extends MaterialActual>) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__OPERATIONS_TYPE:
			setOperationsType((OperationsType) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__ACTUAL_START_TIME:
			setActualStartTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__ACTUAL_END_TIME:
			setActualEndTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__SEGMENT_STATE:
			setSegmentState((PerformanceState) newValue);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.SEGMENT_RESPONSE__SUB_RESPONSES:
			getSubResponses().clear();
			return;
		case Iec62264Package.SEGMENT_RESPONSE__PROCESS_SEGMENT:
			setProcessSegment((ProcessSegment) null);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__OPERATIONS_DEFINITION:
			setOperationsDefinition((OperationsDefinition) null);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__SEGMENT_DATA:
			getSegmentData().clear();
			return;
		case Iec62264Package.SEGMENT_RESPONSE__PERSONNEL_ACTUALS:
			getPersonnelActuals().clear();
			return;
		case Iec62264Package.SEGMENT_RESPONSE__EQUIPMENT_ACTUALS:
			setEquipmentActuals((EquipmentActual) null);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__PHYSICAL_ASSET_ACTUALS:
			getPhysicalAssetActuals().clear();
			return;
		case Iec62264Package.SEGMENT_RESPONSE__MATERIAL_ACTUALS:
			getMaterialActuals().clear();
			return;
		case Iec62264Package.SEGMENT_RESPONSE__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__OPERATIONS_TYPE:
			setOperationsType(OPERATIONS_TYPE_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__ACTUAL_START_TIME:
			setActualStartTime(ACTUAL_START_TIME_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__ACTUAL_END_TIME:
			setActualEndTime(ACTUAL_END_TIME_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__SEGMENT_STATE:
			setSegmentState(SEGMENT_STATE_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_RESPONSE__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.SEGMENT_RESPONSE__SUB_RESPONSES:
			return subResponses != null && !subResponses.isEmpty();
		case Iec62264Package.SEGMENT_RESPONSE__PROCESS_SEGMENT:
			return processSegment != null;
		case Iec62264Package.SEGMENT_RESPONSE__OPERATIONS_DEFINITION:
			return operationsDefinition != null;
		case Iec62264Package.SEGMENT_RESPONSE__SEGMENT_DATA:
			return segmentData != null && !segmentData.isEmpty();
		case Iec62264Package.SEGMENT_RESPONSE__PERSONNEL_ACTUALS:
			return personnelActuals != null && !personnelActuals.isEmpty();
		case Iec62264Package.SEGMENT_RESPONSE__EQUIPMENT_ACTUALS:
			return equipmentActuals != null;
		case Iec62264Package.SEGMENT_RESPONSE__PHYSICAL_ASSET_ACTUALS:
			return physicalAssetActuals != null && !physicalAssetActuals.isEmpty();
		case Iec62264Package.SEGMENT_RESPONSE__MATERIAL_ACTUALS:
			return materialActuals != null && !materialActuals.isEmpty();
		case Iec62264Package.SEGMENT_RESPONSE__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.SEGMENT_RESPONSE__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.SEGMENT_RESPONSE__OPERATIONS_TYPE:
			return operationsType != OPERATIONS_TYPE_EDEFAULT;
		case Iec62264Package.SEGMENT_RESPONSE__ACTUAL_START_TIME:
			return ACTUAL_START_TIME_EDEFAULT == null ? actualStartTime != null
					: !ACTUAL_START_TIME_EDEFAULT.equals(actualStartTime);
		case Iec62264Package.SEGMENT_RESPONSE__ACTUAL_END_TIME:
			return ACTUAL_END_TIME_EDEFAULT == null ? actualEndTime != null
					: !ACTUAL_END_TIME_EDEFAULT.equals(actualEndTime);
		case Iec62264Package.SEGMENT_RESPONSE__SEGMENT_STATE:
			return segmentState != SEGMENT_STATE_EDEFAULT;
		case Iec62264Package.SEGMENT_RESPONSE__HIERARCHY_SCOPE:
			return hierarchyScope != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", operationsType: ");
		result.append(operationsType);
		result.append(", actualStartTime: ");
		result.append(actualStartTime);
		result.append(", actualEndTime: ");
		result.append(actualEndTime);
		result.append(", segmentState: ");
		result.append(segmentState);
		result.append(')');
		return result.toString();
	}

} //SegmentResponseImpl
