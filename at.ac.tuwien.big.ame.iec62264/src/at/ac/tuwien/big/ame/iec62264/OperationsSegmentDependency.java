/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Segment Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getSubject <em>Subject</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDependency <em>Dependency</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDependencyType <em>Dependency Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDependencyFactor <em>Dependency Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getUnitOfMeasure <em>Unit Of Measure</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegmentDependency()
 * @model
 * @generated
 */
public interface OperationsSegmentDependency extends EObject {
	/**
	 * Returns the value of the '<em><b>Subject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subject</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subject</em>' reference.
	 * @see #setSubject(OperationsSegment)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegmentDependency_Subject()
	 * @model required="true"
	 * @generated
	 */
	OperationsSegment getSubject();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getSubject <em>Subject</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subject</em>' reference.
	 * @see #getSubject()
	 * @generated
	 */
	void setSubject(OperationsSegment value);

	/**
	 * Returns the value of the '<em><b>Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependency</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependency</em>' reference.
	 * @see #setDependency(OperationsSegment)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegmentDependency_Dependency()
	 * @model required="true"
	 * @generated
	 */
	OperationsSegment getDependency();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDependency <em>Dependency</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dependency</em>' reference.
	 * @see #getDependency()
	 * @generated
	 */
	void setDependency(OperationsSegment value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegmentDependency_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegmentDependency_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Dependency Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependency Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependency Type</em>' attribute.
	 * @see #setDependencyType(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegmentDependency_DependencyType()
	 * @model
	 * @generated
	 */
	String getDependencyType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDependencyType <em>Dependency Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dependency Type</em>' attribute.
	 * @see #getDependencyType()
	 * @generated
	 */
	void setDependencyType(String value);

	/**
	 * Returns the value of the '<em><b>Dependency Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependency Factor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependency Factor</em>' attribute.
	 * @see #setDependencyFactor(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegmentDependency_DependencyFactor()
	 * @model
	 * @generated
	 */
	String getDependencyFactor();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDependencyFactor <em>Dependency Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dependency Factor</em>' attribute.
	 * @see #getDependencyFactor()
	 * @generated
	 */
	void setDependencyFactor(String value);

	/**
	 * Returns the value of the '<em><b>Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Of Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Of Measure</em>' attribute.
	 * @see #setUnitOfMeasure(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegmentDependency_UnitOfMeasure()
	 * @model
	 * @generated
	 */
	String getUnitOfMeasure();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getUnitOfMeasure <em>Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Of Measure</em>' attribute.
	 * @see #getUnitOfMeasure()
	 * @generated
	 */
	void setUnitOfMeasure(String value);

} // OperationsSegmentDependency
