/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.ProcessSegment;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process Segment Dependency</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentDependencyImpl#getSubject <em>Subject</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentDependencyImpl#getDependency <em>Dependency</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentDependencyImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentDependencyImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentDependencyImpl#getDependencyType <em>Dependency Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentDependencyImpl#getDependencyFactor <em>Dependency Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentDependencyImpl#getFactorUnitOfMeasure <em>Factor Unit Of Measure</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcessSegmentDependencyImpl extends MinimalEObjectImpl.Container implements ProcessSegmentDependency {
	/**
	 * The cached value of the '{@link #getSubject() <em>Subject</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubject()
	 * @generated
	 * @ordered
	 */
	protected ProcessSegment subject;

	/**
	 * The cached value of the '{@link #getDependency() <em>Dependency</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependency()
	 * @generated
	 * @ordered
	 */
	protected ProcessSegment dependency;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDependencyType() <em>Dependency Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencyType()
	 * @generated
	 * @ordered
	 */
	protected static final String DEPENDENCY_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDependencyType() <em>Dependency Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencyType()
	 * @generated
	 * @ordered
	 */
	protected String dependencyType = DEPENDENCY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDependencyFactor() <em>Dependency Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencyFactor()
	 * @generated
	 * @ordered
	 */
	protected static final String DEPENDENCY_FACTOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDependencyFactor() <em>Dependency Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencyFactor()
	 * @generated
	 * @ordered
	 */
	protected String dependencyFactor = DEPENDENCY_FACTOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getFactorUnitOfMeasure() <em>Factor Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFactorUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String FACTOR_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFactorUnitOfMeasure() <em>Factor Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFactorUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String factorUnitOfMeasure = FACTOR_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessSegmentDependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PROCESS_SEGMENT_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegment getSubject() {
		if (subject != null && subject.eIsProxy()) {
			InternalEObject oldSubject = (InternalEObject) subject;
			subject = (ProcessSegment) eResolveProxy(oldSubject);
			if (subject != oldSubject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__SUBJECT, oldSubject, subject));
			}
		}
		return subject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegment basicGetSubject() {
		return subject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubject(ProcessSegment newSubject) {
		ProcessSegment oldSubject = subject;
		subject = newSubject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__SUBJECT,
					oldSubject, subject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegment getDependency() {
		if (dependency != null && dependency.eIsProxy()) {
			InternalEObject oldDependency = (InternalEObject) dependency;
			dependency = (ProcessSegment) eResolveProxy(oldDependency);
			if (dependency != oldDependency) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY, oldDependency, dependency));
			}
		}
		return dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegment basicGetDependency() {
		return dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependency(ProcessSegment newDependency) {
		ProcessSegment oldDependency = dependency;
		dependency = newDependency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY, oldDependency, dependency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__ID, oldId,
					id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDependencyType() {
		return dependencyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependencyType(String newDependencyType) {
		String oldDependencyType = dependencyType;
		dependencyType = newDependencyType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE, oldDependencyType, dependencyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDependencyFactor() {
		return dependencyFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependencyFactor(String newDependencyFactor) {
		String oldDependencyFactor = dependencyFactor;
		dependencyFactor = newDependencyFactor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR, oldDependencyFactor,
					dependencyFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFactorUnitOfMeasure() {
		return factorUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFactorUnitOfMeasure(String newFactorUnitOfMeasure) {
		String oldFactorUnitOfMeasure = factorUnitOfMeasure;
		factorUnitOfMeasure = newFactorUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__FACTOR_UNIT_OF_MEASURE, oldFactorUnitOfMeasure,
					factorUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__SUBJECT:
			if (resolve)
				return getSubject();
			return basicGetSubject();
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY:
			if (resolve)
				return getDependency();
			return basicGetDependency();
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__ID:
			return getId();
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DESCRIPTION:
			return getDescription();
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE:
			return getDependencyType();
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR:
			return getDependencyFactor();
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__FACTOR_UNIT_OF_MEASURE:
			return getFactorUnitOfMeasure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__SUBJECT:
			setSubject((ProcessSegment) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY:
			setDependency((ProcessSegment) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE:
			setDependencyType((String) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR:
			setDependencyFactor((String) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__FACTOR_UNIT_OF_MEASURE:
			setFactorUnitOfMeasure((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__SUBJECT:
			setSubject((ProcessSegment) null);
			return;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY:
			setDependency((ProcessSegment) null);
			return;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE:
			setDependencyType(DEPENDENCY_TYPE_EDEFAULT);
			return;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR:
			setDependencyFactor(DEPENDENCY_FACTOR_EDEFAULT);
			return;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__FACTOR_UNIT_OF_MEASURE:
			setFactorUnitOfMeasure(FACTOR_UNIT_OF_MEASURE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__SUBJECT:
			return subject != null;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY:
			return dependency != null;
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE:
			return DEPENDENCY_TYPE_EDEFAULT == null ? dependencyType != null
					: !DEPENDENCY_TYPE_EDEFAULT.equals(dependencyType);
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR:
			return DEPENDENCY_FACTOR_EDEFAULT == null ? dependencyFactor != null
					: !DEPENDENCY_FACTOR_EDEFAULT.equals(dependencyFactor);
		case Iec62264Package.PROCESS_SEGMENT_DEPENDENCY__FACTOR_UNIT_OF_MEASURE:
			return FACTOR_UNIT_OF_MEASURE_EDEFAULT == null ? factorUnitOfMeasure != null
					: !FACTOR_UNIT_OF_MEASURE_EDEFAULT.equals(factorUnitOfMeasure);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", dependencyType: ");
		result.append(dependencyType);
		result.append(", dependencyFactor: ");
		result.append(dependencyFactor);
		result.append(", factorUnitOfMeasure: ");
		result.append(factorUnitOfMeasure);
		result.append(')');
		return result.toString();
	}

} //ProcessSegmentDependencyImpl
