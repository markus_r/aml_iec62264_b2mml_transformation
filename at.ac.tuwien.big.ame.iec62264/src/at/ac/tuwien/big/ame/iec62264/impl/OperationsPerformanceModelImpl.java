/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.OperationsPerformance;
import at.ac.tuwien.big.ame.iec62264.OperationsPerformanceModel;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Performance Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceModelImpl#getOperationsPerformances <em>Operations Performances</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsPerformanceModelImpl extends MinimalEObjectImpl.Container implements OperationsPerformanceModel {
	/**
	 * The cached value of the '{@link #getOperationsPerformances() <em>Operations Performances</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsPerformances()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsPerformance> operationsPerformances;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsPerformanceModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.OPERATIONS_PERFORMANCE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsPerformance> getOperationsPerformances() {
		if (operationsPerformances == null) {
			operationsPerformances = new EObjectContainmentEList<OperationsPerformance>(OperationsPerformance.class,
					this, Iec62264Package.OPERATIONS_PERFORMANCE_MODEL__OPERATIONS_PERFORMANCES);
		}
		return operationsPerformances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_PERFORMANCE_MODEL__OPERATIONS_PERFORMANCES:
			return ((InternalEList<?>) getOperationsPerformances()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_PERFORMANCE_MODEL__OPERATIONS_PERFORMANCES:
			return getOperationsPerformances();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_PERFORMANCE_MODEL__OPERATIONS_PERFORMANCES:
			getOperationsPerformances().clear();
			getOperationsPerformances().addAll((Collection<? extends OperationsPerformance>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_PERFORMANCE_MODEL__OPERATIONS_PERFORMANCES:
			getOperationsPerformances().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.OPERATIONS_PERFORMANCE_MODEL__OPERATIONS_PERFORMANCES:
			return operationsPerformances != null && !operationsPerformances.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OperationsPerformanceModelImpl
