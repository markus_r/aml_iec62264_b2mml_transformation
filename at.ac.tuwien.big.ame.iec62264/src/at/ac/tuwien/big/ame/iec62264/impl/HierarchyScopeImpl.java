/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Equipment;
import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hierarchy Scope</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.HierarchyScopeImpl#getSubScope <em>Sub Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.HierarchyScopeImpl#getEquipment <em>Equipment</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.HierarchyScopeImpl#getEquipmentElementLevel <em>Equipment Element Level</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HierarchyScopeImpl extends MinimalEObjectImpl.Container implements HierarchyScope {
	/**
	 * The cached value of the '{@link #getSubScope() <em>Sub Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScope subScope;

	/**
	 * The cached value of the '{@link #getEquipment() <em>Equipment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipment()
	 * @generated
	 * @ordered
	 */
	protected Equipment equipment;

	/**
	 * The default value of the '{@link #getEquipmentElementLevel() <em>Equipment Element Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentElementLevel()
	 * @generated
	 * @ordered
	 */
	protected static final String EQUIPMENT_ELEMENT_LEVEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEquipmentElementLevel() <em>Equipment Element Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentElementLevel()
	 * @generated
	 * @ordered
	 */
	protected String equipmentElementLevel = EQUIPMENT_ELEMENT_LEVEL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HierarchyScopeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.HIERARCHY_SCOPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope getSubScope() {
		return subScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubScope(HierarchyScope newSubScope, NotificationChain msgs) {
		HierarchyScope oldSubScope = subScope;
		subScope = newSubScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Iec62264Package.HIERARCHY_SCOPE__SUB_SCOPE, oldSubScope, newSubScope);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubScope(HierarchyScope newSubScope) {
		if (newSubScope != subScope) {
			NotificationChain msgs = null;
			if (subScope != null)
				msgs = ((InternalEObject) subScope).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.HIERARCHY_SCOPE__SUB_SCOPE, null, msgs);
			if (newSubScope != null)
				msgs = ((InternalEObject) newSubScope).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.HIERARCHY_SCOPE__SUB_SCOPE, null, msgs);
			msgs = basicSetSubScope(newSubScope, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.HIERARCHY_SCOPE__SUB_SCOPE,
					newSubScope, newSubScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Equipment getEquipment() {
		if (equipment != null && equipment.eIsProxy()) {
			InternalEObject oldEquipment = (InternalEObject) equipment;
			equipment = (Equipment) eResolveProxy(oldEquipment);
			if (equipment != oldEquipment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.HIERARCHY_SCOPE__EQUIPMENT, oldEquipment, equipment));
			}
		}
		return equipment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Equipment basicGetEquipment() {
		return equipment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipment(Equipment newEquipment) {
		Equipment oldEquipment = equipment;
		equipment = newEquipment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.HIERARCHY_SCOPE__EQUIPMENT,
					oldEquipment, equipment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEquipmentElementLevel() {
		return equipmentElementLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentElementLevel(String newEquipmentElementLevel) {
		String oldEquipmentElementLevel = equipmentElementLevel;
		equipmentElementLevel = newEquipmentElementLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.HIERARCHY_SCOPE__EQUIPMENT_ELEMENT_LEVEL, oldEquipmentElementLevel,
					equipmentElementLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.HIERARCHY_SCOPE__SUB_SCOPE:
			return basicSetSubScope(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.HIERARCHY_SCOPE__SUB_SCOPE:
			return getSubScope();
		case Iec62264Package.HIERARCHY_SCOPE__EQUIPMENT:
			if (resolve)
				return getEquipment();
			return basicGetEquipment();
		case Iec62264Package.HIERARCHY_SCOPE__EQUIPMENT_ELEMENT_LEVEL:
			return getEquipmentElementLevel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.HIERARCHY_SCOPE__SUB_SCOPE:
			setSubScope((HierarchyScope) newValue);
			return;
		case Iec62264Package.HIERARCHY_SCOPE__EQUIPMENT:
			setEquipment((Equipment) newValue);
			return;
		case Iec62264Package.HIERARCHY_SCOPE__EQUIPMENT_ELEMENT_LEVEL:
			setEquipmentElementLevel((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.HIERARCHY_SCOPE__SUB_SCOPE:
			setSubScope((HierarchyScope) null);
			return;
		case Iec62264Package.HIERARCHY_SCOPE__EQUIPMENT:
			setEquipment((Equipment) null);
			return;
		case Iec62264Package.HIERARCHY_SCOPE__EQUIPMENT_ELEMENT_LEVEL:
			setEquipmentElementLevel(EQUIPMENT_ELEMENT_LEVEL_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.HIERARCHY_SCOPE__SUB_SCOPE:
			return subScope != null;
		case Iec62264Package.HIERARCHY_SCOPE__EQUIPMENT:
			return equipment != null;
		case Iec62264Package.HIERARCHY_SCOPE__EQUIPMENT_ELEMENT_LEVEL:
			return EQUIPMENT_ELEMENT_LEVEL_EDEFAULT == null ? equipmentElementLevel != null
					: !EQUIPMENT_ELEMENT_LEVEL_EDEFAULT.equals(equipmentElementLevel);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (equipmentElementLevel: ");
		result.append(equipmentElementLevel);
		result.append(')');
		return result.toString();
	}

} //HierarchyScopeImpl
