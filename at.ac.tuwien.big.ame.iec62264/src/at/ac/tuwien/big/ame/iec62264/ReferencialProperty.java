/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.math.BigDecimal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Referencial Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferencialProperty()
 * @model abstract="true"
 * @generated
 */
public interface ReferencialProperty extends EObject {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferencialProperty_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferencialProperty_Value()
	 * @model
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Unit Of Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Unit Of Measure</em>' attribute.
	 * @see #setValueUnitOfMeasure(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferencialProperty_ValueUnitOfMeasure()
	 * @model
	 * @generated
	 */
	String getValueUnitOfMeasure();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Unit Of Measure</em>' attribute.
	 * @see #getValueUnitOfMeasure()
	 * @generated
	 */
	void setValueUnitOfMeasure(String value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(BigDecimal)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferencialProperty_Quantity()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 * @generated
	 */
	BigDecimal getQuantity();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getQuantity <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity Unit Of Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity Unit Of Measure</em>' attribute.
	 * @see #setQuantityUnitOfMeasure(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferencialProperty_QuantityUnitOfMeasure()
	 * @model
	 * @generated
	 */
	String getQuantityUnitOfMeasure();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity Unit Of Measure</em>' attribute.
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 */
	void setQuantityUnitOfMeasure(String value);

} // ReferencialProperty
