/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Definition Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel#getOperationsDefinitions <em>Operations Definitions</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel#getOperationsSegments <em>Operations Segments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel#getOperationsSegmentDependencies <em>Operations Segment Dependencies</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinitionModel()
 * @model
 * @generated
 */
public interface OperationsDefinitionModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Operations Definitions</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definitions</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinitionModel_OperationsDefinitions()
	 * @model containment="true"
	 * @generated
	 */
	EList<OperationsDefinition> getOperationsDefinitions();

	/**
	 * Returns the value of the '<em><b>Operations Segments</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.OperationsSegment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Segments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Segments</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinitionModel_OperationsSegments()
	 * @model containment="true"
	 * @generated
	 */
	EList<OperationsSegment> getOperationsSegments();

	/**
	 * Returns the value of the '<em><b>Operations Segment Dependencies</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Segment Dependencies</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Segment Dependencies</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinitionModel_OperationsSegmentDependencies()
	 * @model containment="true"
	 * @generated
	 */
	EList<OperationsSegmentDependency> getOperationsSegmentDependencies();

} // OperationsDefinitionModel
