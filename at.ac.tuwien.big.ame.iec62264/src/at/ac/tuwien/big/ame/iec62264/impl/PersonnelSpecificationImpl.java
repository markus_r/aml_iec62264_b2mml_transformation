/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.Person;
import at.ac.tuwien.big.ame.iec62264.PersonnelClass;
import at.ac.tuwien.big.ame.iec62264.PersonnelSpecification;
import at.ac.tuwien.big.ame.iec62264.PersonnelSpecificationProperty;

import java.math.BigDecimal;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Personnel Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationImpl#getPersonnelSpecificationProperties <em>Personnel Specification Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationImpl#getPersonnelClasses <em>Personnel Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationImpl#getPersons <em>Persons</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationImpl#getPersonnelUse <em>Personnel Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationImpl#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonnelSpecificationImpl extends MinimalEObjectImpl.Container implements PersonnelSpecification {
	/**
	 * The cached value of the '{@link #getPersonnelSpecificationProperties() <em>Personnel Specification Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelSpecificationProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelSpecificationProperty> personnelSpecificationProperties;

	/**
	 * The cached value of the '{@link #getPersonnelClasses() <em>Personnel Classes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelClass> personnelClasses;

	/**
	 * The cached value of the '{@link #getPersons() <em>Persons</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersons()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> persons;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPersonnelUse() <em>Personnel Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelUse()
	 * @generated
	 * @ordered
	 */
	protected static final String PERSONNEL_USE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPersonnelUse() <em>Personnel Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelUse()
	 * @generated
	 * @ordered
	 */
	protected String personnelUse = PERSONNEL_USE_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal QUANTITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal quantity = QUANTITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String QUANTITY_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String quantityUnitOfMeasure = QUANTITY_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonnelSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PERSONNEL_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelSpecificationProperty> getPersonnelSpecificationProperties() {
		if (personnelSpecificationProperties == null) {
			personnelSpecificationProperties = new EObjectContainmentEList<PersonnelSpecificationProperty>(
					PersonnelSpecificationProperty.class, this,
					Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_SPECIFICATION_PROPERTIES);
		}
		return personnelSpecificationProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelClass> getPersonnelClasses() {
		if (personnelClasses == null) {
			personnelClasses = new EObjectResolvingEList<PersonnelClass>(PersonnelClass.class, this,
					Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_CLASSES);
		}
		return personnelClasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Person> getPersons() {
		if (persons == null) {
			persons = new EObjectResolvingEList<Person>(Person.class, this,
					Iec62264Package.PERSONNEL_SPECIFICATION__PERSONS);
		}
		return persons;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PERSONNEL_SPECIFICATION__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPersonnelUse() {
		return personnelUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonnelUse(String newPersonnelUse) {
		String oldPersonnelUse = personnelUse;
		personnelUse = newPersonnelUse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_USE, oldPersonnelUse, personnelUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(BigDecimal newQuantity) {
		BigDecimal oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PERSONNEL_SPECIFICATION__QUANTITY,
					oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQuantityUnitOfMeasure() {
		return quantityUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantityUnitOfMeasure(String newQuantityUnitOfMeasure) {
		String oldQuantityUnitOfMeasure = quantityUnitOfMeasure;
		quantityUnitOfMeasure = newQuantityUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PERSONNEL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE, oldQuantityUnitOfMeasure,
					quantityUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_SPECIFICATION_PROPERTIES:
			return ((InternalEList<?>) getPersonnelSpecificationProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_SPECIFICATION_PROPERTIES:
			return getPersonnelSpecificationProperties();
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_CLASSES:
			return getPersonnelClasses();
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONS:
			return getPersons();
		case Iec62264Package.PERSONNEL_SPECIFICATION__DESCRIPTION:
			return getDescription();
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_USE:
			return getPersonnelUse();
		case Iec62264Package.PERSONNEL_SPECIFICATION__QUANTITY:
			return getQuantity();
		case Iec62264Package.PERSONNEL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			return getQuantityUnitOfMeasure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_SPECIFICATION_PROPERTIES:
			getPersonnelSpecificationProperties().clear();
			getPersonnelSpecificationProperties()
					.addAll((Collection<? extends PersonnelSpecificationProperty>) newValue);
			return;
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_CLASSES:
			getPersonnelClasses().clear();
			getPersonnelClasses().addAll((Collection<? extends PersonnelClass>) newValue);
			return;
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONS:
			getPersons().clear();
			getPersons().addAll((Collection<? extends Person>) newValue);
			return;
		case Iec62264Package.PERSONNEL_SPECIFICATION__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_USE:
			setPersonnelUse((String) newValue);
			return;
		case Iec62264Package.PERSONNEL_SPECIFICATION__QUANTITY:
			setQuantity((BigDecimal) newValue);
			return;
		case Iec62264Package.PERSONNEL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_SPECIFICATION_PROPERTIES:
			getPersonnelSpecificationProperties().clear();
			return;
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_CLASSES:
			getPersonnelClasses().clear();
			return;
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONS:
			getPersons().clear();
			return;
		case Iec62264Package.PERSONNEL_SPECIFICATION__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_USE:
			setPersonnelUse(PERSONNEL_USE_EDEFAULT);
			return;
		case Iec62264Package.PERSONNEL_SPECIFICATION__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case Iec62264Package.PERSONNEL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure(QUANTITY_UNIT_OF_MEASURE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_SPECIFICATION_PROPERTIES:
			return personnelSpecificationProperties != null && !personnelSpecificationProperties.isEmpty();
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_CLASSES:
			return personnelClasses != null && !personnelClasses.isEmpty();
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONS:
			return persons != null && !persons.isEmpty();
		case Iec62264Package.PERSONNEL_SPECIFICATION__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.PERSONNEL_SPECIFICATION__PERSONNEL_USE:
			return PERSONNEL_USE_EDEFAULT == null ? personnelUse != null : !PERSONNEL_USE_EDEFAULT.equals(personnelUse);
		case Iec62264Package.PERSONNEL_SPECIFICATION__QUANTITY:
			return QUANTITY_EDEFAULT == null ? quantity != null : !QUANTITY_EDEFAULT.equals(quantity);
		case Iec62264Package.PERSONNEL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			return QUANTITY_UNIT_OF_MEASURE_EDEFAULT == null ? quantityUnitOfMeasure != null
					: !QUANTITY_UNIT_OF_MEASURE_EDEFAULT.equals(quantityUnitOfMeasure);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", personnelUse: ");
		result.append(personnelUse);
		result.append(", quantity: ");
		result.append(quantity);
		result.append(", quantityUnitOfMeasure: ");
		result.append(quantityUnitOfMeasure);
		result.append(')');
		return result.toString();
	}

} //PersonnelSpecificationImpl
