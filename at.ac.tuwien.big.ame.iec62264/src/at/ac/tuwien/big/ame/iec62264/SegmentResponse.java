/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.time.ZonedDateTime;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Segment Response</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getSubResponses <em>Sub Responses</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getProcessSegment <em>Process Segment</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getOperationsDefinition <em>Operations Definition</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getSegmentData <em>Segment Data</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getPersonnelActuals <em>Personnel Actuals</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getEquipmentActuals <em>Equipment Actuals</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getPhysicalAssetActuals <em>Physical Asset Actuals</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getMaterialActuals <em>Material Actuals</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getActualStartTime <em>Actual Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getActualEndTime <em>Actual End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getSegmentState <em>Segment State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse()
 * @model
 * @generated
 */
public interface SegmentResponse extends EObject {
	/**
	 * Returns the value of the '<em><b>Sub Responses</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.SegmentResponse}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Responses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Responses</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_SubResponses()
	 * @model
	 * @generated
	 */
	EList<SegmentResponse> getSubResponses();

	/**
	 * Returns the value of the '<em><b>Process Segment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment</em>' reference.
	 * @see #setProcessSegment(ProcessSegment)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_ProcessSegment()
	 * @model required="true"
	 * @generated
	 */
	ProcessSegment getProcessSegment();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getProcessSegment <em>Process Segment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Segment</em>' reference.
	 * @see #getProcessSegment()
	 * @generated
	 */
	void setProcessSegment(ProcessSegment value);

	/**
	 * Returns the value of the '<em><b>Operations Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition</em>' reference.
	 * @see #setOperationsDefinition(OperationsDefinition)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_OperationsDefinition()
	 * @model
	 * @generated
	 */
	OperationsDefinition getOperationsDefinition();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getOperationsDefinition <em>Operations Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Definition</em>' reference.
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	void setOperationsDefinition(OperationsDefinition value);

	/**
	 * Returns the value of the '<em><b>Segment Data</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.SegmentData}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment Data</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment Data</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_SegmentData()
	 * @model containment="true"
	 * @generated
	 */
	EList<SegmentData> getSegmentData();

	/**
	 * Returns the value of the '<em><b>Personnel Actuals</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PersonnelActual}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Actuals</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Actuals</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_PersonnelActuals()
	 * @model containment="true"
	 * @generated
	 */
	EList<PersonnelActual> getPersonnelActuals();

	/**
	 * Returns the value of the '<em><b>Equipment Actuals</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Actuals</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Actuals</em>' reference.
	 * @see #setEquipmentActuals(EquipmentActual)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_EquipmentActuals()
	 * @model
	 * @generated
	 */
	EquipmentActual getEquipmentActuals();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getEquipmentActuals <em>Equipment Actuals</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Actuals</em>' reference.
	 * @see #getEquipmentActuals()
	 * @generated
	 */
	void setEquipmentActuals(EquipmentActual value);

	/**
	 * Returns the value of the '<em><b>Physical Asset Actuals</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Actuals</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Actuals</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_PhysicalAssetActuals()
	 * @model containment="true"
	 * @generated
	 */
	EList<PhysicalAssetActual> getPhysicalAssetActuals();

	/**
	 * Returns the value of the '<em><b>Material Actuals</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialActual}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Actuals</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Actuals</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_MaterialActuals()
	 * @model containment="true"
	 * @generated
	 */
	EList<MaterialActual> getMaterialActuals();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.OperationsType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #setOperationsType(OperationsType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_OperationsType()
	 * @model required="true"
	 * @generated
	 */
	OperationsType getOperationsType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getOperationsType <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsType value);

	/**
	 * Returns the value of the '<em><b>Actual Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual Start Time</em>' attribute.
	 * @see #setActualStartTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_ActualStartTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getActualStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getActualStartTime <em>Actual Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actual Start Time</em>' attribute.
	 * @see #getActualStartTime()
	 * @generated
	 */
	void setActualStartTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Actual End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual End Time</em>' attribute.
	 * @see #setActualEndTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_ActualEndTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getActualEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getActualEndTime <em>Actual End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actual End Time</em>' attribute.
	 * @see #getActualEndTime()
	 * @generated
	 */
	void setActualEndTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Segment State</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.PerformanceState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment State</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.PerformanceState
	 * @see #setSegmentState(PerformanceState)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_SegmentState()
	 * @model
	 * @generated
	 */
	PerformanceState getSegmentState();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getSegmentState <em>Segment State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Segment State</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.PerformanceState
	 * @see #getSegmentState()
	 * @generated
	 */
	void setSegmentState(PerformanceState value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #setHierarchyScope(HierarchyScope)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getSegmentResponse_HierarchyScope()
	 * @model
	 * @generated
	 */
	HierarchyScope getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getHierarchyScope <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScope value);

} // SegmentResponse
