/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Material Use</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getMaterialUse()
 * @model
 * @generated
 */
public enum MaterialUse implements Enumerator {
	/**
	 * The '<em><b>CONSUMABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONSUMABLE_VALUE
	 * @generated
	 * @ordered
	 */
	CONSUMABLE(0, "CONSUMABLE", "CONSUMABLE"),

	/**
	 * The '<em><b>MATERIAL CONSUMED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_CONSUMED_VALUE
	 * @generated
	 * @ordered
	 */
	MATERIAL_CONSUMED(1, "MATERIAL_CONSUMED", "MATERIAL_CONSUMED"),

	/**
	 * The '<em><b>MATERIAL PRODUCED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_PRODUCED_VALUE
	 * @generated
	 * @ordered
	 */
	MATERIAL_PRODUCED(2, "MATERIAL_PRODUCED", "MATERIAL_PRODUCED");

	/**
	 * The '<em><b>CONSUMABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CONSUMABLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSUMABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CONSUMABLE_VALUE = 0;

	/**
	 * The '<em><b>MATERIAL CONSUMED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MATERIAL CONSUMED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_CONSUMED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MATERIAL_CONSUMED_VALUE = 1;

	/**
	 * The '<em><b>MATERIAL PRODUCED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MATERIAL PRODUCED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_PRODUCED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MATERIAL_PRODUCED_VALUE = 2;

	/**
	 * An array of all the '<em><b>Material Use</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MaterialUse[] VALUES_ARRAY = new MaterialUse[] { CONSUMABLE, MATERIAL_CONSUMED,
			MATERIAL_PRODUCED, };

	/**
	 * A public read-only list of all the '<em><b>Material Use</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MaterialUse> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Material Use</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MaterialUse get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MaterialUse result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Material Use</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MaterialUse getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MaterialUse result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Material Use</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MaterialUse get(int value) {
		switch (value) {
		case CONSUMABLE_VALUE:
			return CONSUMABLE;
		case MATERIAL_CONSUMED_VALUE:
			return MATERIAL_CONSUMED;
		case MATERIAL_PRODUCED_VALUE:
			return MATERIAL_PRODUCED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MaterialUse(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MaterialUse
