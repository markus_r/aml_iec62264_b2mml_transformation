/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.EquipmentModel;
import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Model;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialModel;
import at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel;
import at.ac.tuwien.big.ame.iec62264.OperationsPerformanceModel;
import at.ac.tuwien.big.ame.iec62264.OperationsScheduleModel;
import at.ac.tuwien.big.ame.iec62264.PersonnelModel;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapabilityModel;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl#getPersonnelModel <em>Personnel Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl#getEquipmentModel <em>Equipment Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl#getPhysicalAssetModel <em>Physical Asset Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl#getMaterialModel <em>Material Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl#getProcessSegmentModel <em>Process Segment Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl#getOperationsDefinitionModel <em>Operations Definition Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl#getOperationsScheduleModel <em>Operations Schedule Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl#getOperationsPerformanceModel <em>Operations Performance Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl#getProcessSegmentCapabilityModel <em>Process Segment Capability Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl#getHierarchyScopes <em>Hierarchy Scopes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Iec62264ModelImpl extends MinimalEObjectImpl.Container implements Iec62264Model {
	/**
	 * The cached value of the '{@link #getPersonnelModel() <em>Personnel Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelModel()
	 * @generated
	 * @ordered
	 */
	protected PersonnelModel personnelModel;

	/**
	 * The cached value of the '{@link #getEquipmentModel() <em>Equipment Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentModel()
	 * @generated
	 * @ordered
	 */
	protected EquipmentModel equipmentModel;

	/**
	 * The cached value of the '{@link #getPhysicalAssetModel() <em>Physical Asset Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetModel()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetModel physicalAssetModel;

	/**
	 * The cached value of the '{@link #getMaterialModel() <em>Material Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialModel()
	 * @generated
	 * @ordered
	 */
	protected MaterialModel materialModel;

	/**
	 * The cached value of the '{@link #getProcessSegmentModel() <em>Process Segment Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegmentModel()
	 * @generated
	 * @ordered
	 */
	protected ProcessSegmentModel processSegmentModel;

	/**
	 * The cached value of the '{@link #getOperationsDefinitionModel() <em>Operations Definition Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsDefinitionModel()
	 * @generated
	 * @ordered
	 */
	protected OperationsDefinitionModel operationsDefinitionModel;

	/**
	 * The cached value of the '{@link #getOperationsScheduleModel() <em>Operations Schedule Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsScheduleModel()
	 * @generated
	 * @ordered
	 */
	protected OperationsScheduleModel operationsScheduleModel;

	/**
	 * The cached value of the '{@link #getOperationsPerformanceModel() <em>Operations Performance Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsPerformanceModel()
	 * @generated
	 * @ordered
	 */
	protected OperationsPerformanceModel operationsPerformanceModel;

	/**
	 * The cached value of the '{@link #getProcessSegmentCapabilityModel() <em>Process Segment Capability Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegmentCapabilityModel()
	 * @generated
	 * @ordered
	 */
	protected ProcessSegmentCapabilityModel processSegmentCapabilityModel;

	/**
	 * The cached value of the '{@link #getHierarchyScopes() <em>Hierarchy Scopes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScopes()
	 * @generated
	 * @ordered
	 */
	protected EList<HierarchyScope> hierarchyScopes;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Iec62264ModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.IEC62264_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelModel getPersonnelModel() {
		return personnelModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPersonnelModel(PersonnelModel newPersonnelModel, NotificationChain msgs) {
		PersonnelModel oldPersonnelModel = personnelModel;
		personnelModel = newPersonnelModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__PERSONNEL_MODEL, oldPersonnelModel, newPersonnelModel);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonnelModel(PersonnelModel newPersonnelModel) {
		if (newPersonnelModel != personnelModel) {
			NotificationChain msgs = null;
			if (personnelModel != null)
				msgs = ((InternalEObject) personnelModel).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__PERSONNEL_MODEL, null, msgs);
			if (newPersonnelModel != null)
				msgs = ((InternalEObject) newPersonnelModel).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__PERSONNEL_MODEL, null, msgs);
			msgs = basicSetPersonnelModel(newPersonnelModel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.IEC62264_MODEL__PERSONNEL_MODEL,
					newPersonnelModel, newPersonnelModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentModel getEquipmentModel() {
		return equipmentModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentModel(EquipmentModel newEquipmentModel, NotificationChain msgs) {
		EquipmentModel oldEquipmentModel = equipmentModel;
		equipmentModel = newEquipmentModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__EQUIPMENT_MODEL, oldEquipmentModel, newEquipmentModel);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentModel(EquipmentModel newEquipmentModel) {
		if (newEquipmentModel != equipmentModel) {
			NotificationChain msgs = null;
			if (equipmentModel != null)
				msgs = ((InternalEObject) equipmentModel).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__EQUIPMENT_MODEL, null, msgs);
			if (newEquipmentModel != null)
				msgs = ((InternalEObject) newEquipmentModel).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__EQUIPMENT_MODEL, null, msgs);
			msgs = basicSetEquipmentModel(newEquipmentModel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.IEC62264_MODEL__EQUIPMENT_MODEL,
					newEquipmentModel, newEquipmentModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetModel getPhysicalAssetModel() {
		return physicalAssetModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPhysicalAssetModel(PhysicalAssetModel newPhysicalAssetModel,
			NotificationChain msgs) {
		PhysicalAssetModel oldPhysicalAssetModel = physicalAssetModel;
		physicalAssetModel = newPhysicalAssetModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__PHYSICAL_ASSET_MODEL, oldPhysicalAssetModel, newPhysicalAssetModel);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetModel(PhysicalAssetModel newPhysicalAssetModel) {
		if (newPhysicalAssetModel != physicalAssetModel) {
			NotificationChain msgs = null;
			if (physicalAssetModel != null)
				msgs = ((InternalEObject) physicalAssetModel).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__PHYSICAL_ASSET_MODEL, null, msgs);
			if (newPhysicalAssetModel != null)
				msgs = ((InternalEObject) newPhysicalAssetModel).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__PHYSICAL_ASSET_MODEL, null, msgs);
			msgs = basicSetPhysicalAssetModel(newPhysicalAssetModel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.IEC62264_MODEL__PHYSICAL_ASSET_MODEL,
					newPhysicalAssetModel, newPhysicalAssetModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialModel getMaterialModel() {
		return materialModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialModel(MaterialModel newMaterialModel, NotificationChain msgs) {
		MaterialModel oldMaterialModel = materialModel;
		materialModel = newMaterialModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__MATERIAL_MODEL, oldMaterialModel, newMaterialModel);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialModel(MaterialModel newMaterialModel) {
		if (newMaterialModel != materialModel) {
			NotificationChain msgs = null;
			if (materialModel != null)
				msgs = ((InternalEObject) materialModel).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__MATERIAL_MODEL, null, msgs);
			if (newMaterialModel != null)
				msgs = ((InternalEObject) newMaterialModel).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__MATERIAL_MODEL, null, msgs);
			msgs = basicSetMaterialModel(newMaterialModel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.IEC62264_MODEL__MATERIAL_MODEL,
					newMaterialModel, newMaterialModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentModel getProcessSegmentModel() {
		return processSegmentModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessSegmentModel(ProcessSegmentModel newProcessSegmentModel,
			NotificationChain msgs) {
		ProcessSegmentModel oldProcessSegmentModel = processSegmentModel;
		processSegmentModel = newProcessSegmentModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_MODEL, oldProcessSegmentModel,
					newProcessSegmentModel);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessSegmentModel(ProcessSegmentModel newProcessSegmentModel) {
		if (newProcessSegmentModel != processSegmentModel) {
			NotificationChain msgs = null;
			if (processSegmentModel != null)
				msgs = ((InternalEObject) processSegmentModel).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_MODEL, null, msgs);
			if (newProcessSegmentModel != null)
				msgs = ((InternalEObject) newProcessSegmentModel).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_MODEL, null, msgs);
			msgs = basicSetProcessSegmentModel(newProcessSegmentModel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_MODEL,
					newProcessSegmentModel, newProcessSegmentModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinitionModel getOperationsDefinitionModel() {
		return operationsDefinitionModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsDefinitionModel(OperationsDefinitionModel newOperationsDefinitionModel,
			NotificationChain msgs) {
		OperationsDefinitionModel oldOperationsDefinitionModel = operationsDefinitionModel;
		operationsDefinitionModel = newOperationsDefinitionModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__OPERATIONS_DEFINITION_MODEL, oldOperationsDefinitionModel,
					newOperationsDefinitionModel);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsDefinitionModel(OperationsDefinitionModel newOperationsDefinitionModel) {
		if (newOperationsDefinitionModel != operationsDefinitionModel) {
			NotificationChain msgs = null;
			if (operationsDefinitionModel != null)
				msgs = ((InternalEObject) operationsDefinitionModel).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__OPERATIONS_DEFINITION_MODEL, null,
						msgs);
			if (newOperationsDefinitionModel != null)
				msgs = ((InternalEObject) newOperationsDefinitionModel).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__OPERATIONS_DEFINITION_MODEL, null,
						msgs);
			msgs = basicSetOperationsDefinitionModel(newOperationsDefinitionModel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__OPERATIONS_DEFINITION_MODEL, newOperationsDefinitionModel,
					newOperationsDefinitionModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsScheduleModel getOperationsScheduleModel() {
		return operationsScheduleModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsScheduleModel(OperationsScheduleModel newOperationsScheduleModel,
			NotificationChain msgs) {
		OperationsScheduleModel oldOperationsScheduleModel = operationsScheduleModel;
		operationsScheduleModel = newOperationsScheduleModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__OPERATIONS_SCHEDULE_MODEL, oldOperationsScheduleModel,
					newOperationsScheduleModel);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsScheduleModel(OperationsScheduleModel newOperationsScheduleModel) {
		if (newOperationsScheduleModel != operationsScheduleModel) {
			NotificationChain msgs = null;
			if (operationsScheduleModel != null)
				msgs = ((InternalEObject) operationsScheduleModel).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__OPERATIONS_SCHEDULE_MODEL, null, msgs);
			if (newOperationsScheduleModel != null)
				msgs = ((InternalEObject) newOperationsScheduleModel).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__OPERATIONS_SCHEDULE_MODEL, null, msgs);
			msgs = basicSetOperationsScheduleModel(newOperationsScheduleModel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__OPERATIONS_SCHEDULE_MODEL, newOperationsScheduleModel,
					newOperationsScheduleModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsPerformanceModel getOperationsPerformanceModel() {
		return operationsPerformanceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsPerformanceModel(
			OperationsPerformanceModel newOperationsPerformanceModel, NotificationChain msgs) {
		OperationsPerformanceModel oldOperationsPerformanceModel = operationsPerformanceModel;
		operationsPerformanceModel = newOperationsPerformanceModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__OPERATIONS_PERFORMANCE_MODEL, oldOperationsPerformanceModel,
					newOperationsPerformanceModel);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsPerformanceModel(OperationsPerformanceModel newOperationsPerformanceModel) {
		if (newOperationsPerformanceModel != operationsPerformanceModel) {
			NotificationChain msgs = null;
			if (operationsPerformanceModel != null)
				msgs = ((InternalEObject) operationsPerformanceModel).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__OPERATIONS_PERFORMANCE_MODEL, null,
						msgs);
			if (newOperationsPerformanceModel != null)
				msgs = ((InternalEObject) newOperationsPerformanceModel).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__OPERATIONS_PERFORMANCE_MODEL, null,
						msgs);
			msgs = basicSetOperationsPerformanceModel(newOperationsPerformanceModel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__OPERATIONS_PERFORMANCE_MODEL, newOperationsPerformanceModel,
					newOperationsPerformanceModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentCapabilityModel getProcessSegmentCapabilityModel() {
		return processSegmentCapabilityModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessSegmentCapabilityModel(
			ProcessSegmentCapabilityModel newProcessSegmentCapabilityModel, NotificationChain msgs) {
		ProcessSegmentCapabilityModel oldProcessSegmentCapabilityModel = processSegmentCapabilityModel;
		processSegmentCapabilityModel = newProcessSegmentCapabilityModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_CAPABILITY_MODEL, oldProcessSegmentCapabilityModel,
					newProcessSegmentCapabilityModel);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessSegmentCapabilityModel(ProcessSegmentCapabilityModel newProcessSegmentCapabilityModel) {
		if (newProcessSegmentCapabilityModel != processSegmentCapabilityModel) {
			NotificationChain msgs = null;
			if (processSegmentCapabilityModel != null)
				msgs = ((InternalEObject) processSegmentCapabilityModel).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_CAPABILITY_MODEL, null,
						msgs);
			if (newProcessSegmentCapabilityModel != null)
				msgs = ((InternalEObject) newProcessSegmentCapabilityModel).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_CAPABILITY_MODEL, null,
						msgs);
			msgs = basicSetProcessSegmentCapabilityModel(newProcessSegmentCapabilityModel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_CAPABILITY_MODEL, newProcessSegmentCapabilityModel,
					newProcessSegmentCapabilityModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HierarchyScope> getHierarchyScopes() {
		if (hierarchyScopes == null) {
			hierarchyScopes = new EObjectContainmentEList<HierarchyScope>(HierarchyScope.class, this,
					Iec62264Package.IEC62264_MODEL__HIERARCHY_SCOPES);
		}
		return hierarchyScopes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.IEC62264_MODEL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.IEC62264_MODEL__PERSONNEL_MODEL:
			return basicSetPersonnelModel(null, msgs);
		case Iec62264Package.IEC62264_MODEL__EQUIPMENT_MODEL:
			return basicSetEquipmentModel(null, msgs);
		case Iec62264Package.IEC62264_MODEL__PHYSICAL_ASSET_MODEL:
			return basicSetPhysicalAssetModel(null, msgs);
		case Iec62264Package.IEC62264_MODEL__MATERIAL_MODEL:
			return basicSetMaterialModel(null, msgs);
		case Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_MODEL:
			return basicSetProcessSegmentModel(null, msgs);
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_DEFINITION_MODEL:
			return basicSetOperationsDefinitionModel(null, msgs);
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_SCHEDULE_MODEL:
			return basicSetOperationsScheduleModel(null, msgs);
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_PERFORMANCE_MODEL:
			return basicSetOperationsPerformanceModel(null, msgs);
		case Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_CAPABILITY_MODEL:
			return basicSetProcessSegmentCapabilityModel(null, msgs);
		case Iec62264Package.IEC62264_MODEL__HIERARCHY_SCOPES:
			return ((InternalEList<?>) getHierarchyScopes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.IEC62264_MODEL__PERSONNEL_MODEL:
			return getPersonnelModel();
		case Iec62264Package.IEC62264_MODEL__EQUIPMENT_MODEL:
			return getEquipmentModel();
		case Iec62264Package.IEC62264_MODEL__PHYSICAL_ASSET_MODEL:
			return getPhysicalAssetModel();
		case Iec62264Package.IEC62264_MODEL__MATERIAL_MODEL:
			return getMaterialModel();
		case Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_MODEL:
			return getProcessSegmentModel();
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_DEFINITION_MODEL:
			return getOperationsDefinitionModel();
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_SCHEDULE_MODEL:
			return getOperationsScheduleModel();
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_PERFORMANCE_MODEL:
			return getOperationsPerformanceModel();
		case Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_CAPABILITY_MODEL:
			return getProcessSegmentCapabilityModel();
		case Iec62264Package.IEC62264_MODEL__HIERARCHY_SCOPES:
			return getHierarchyScopes();
		case Iec62264Package.IEC62264_MODEL__NAME:
			return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.IEC62264_MODEL__PERSONNEL_MODEL:
			setPersonnelModel((PersonnelModel) newValue);
			return;
		case Iec62264Package.IEC62264_MODEL__EQUIPMENT_MODEL:
			setEquipmentModel((EquipmentModel) newValue);
			return;
		case Iec62264Package.IEC62264_MODEL__PHYSICAL_ASSET_MODEL:
			setPhysicalAssetModel((PhysicalAssetModel) newValue);
			return;
		case Iec62264Package.IEC62264_MODEL__MATERIAL_MODEL:
			setMaterialModel((MaterialModel) newValue);
			return;
		case Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_MODEL:
			setProcessSegmentModel((ProcessSegmentModel) newValue);
			return;
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_DEFINITION_MODEL:
			setOperationsDefinitionModel((OperationsDefinitionModel) newValue);
			return;
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_SCHEDULE_MODEL:
			setOperationsScheduleModel((OperationsScheduleModel) newValue);
			return;
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_PERFORMANCE_MODEL:
			setOperationsPerformanceModel((OperationsPerformanceModel) newValue);
			return;
		case Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_CAPABILITY_MODEL:
			setProcessSegmentCapabilityModel((ProcessSegmentCapabilityModel) newValue);
			return;
		case Iec62264Package.IEC62264_MODEL__HIERARCHY_SCOPES:
			getHierarchyScopes().clear();
			getHierarchyScopes().addAll((Collection<? extends HierarchyScope>) newValue);
			return;
		case Iec62264Package.IEC62264_MODEL__NAME:
			setName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.IEC62264_MODEL__PERSONNEL_MODEL:
			setPersonnelModel((PersonnelModel) null);
			return;
		case Iec62264Package.IEC62264_MODEL__EQUIPMENT_MODEL:
			setEquipmentModel((EquipmentModel) null);
			return;
		case Iec62264Package.IEC62264_MODEL__PHYSICAL_ASSET_MODEL:
			setPhysicalAssetModel((PhysicalAssetModel) null);
			return;
		case Iec62264Package.IEC62264_MODEL__MATERIAL_MODEL:
			setMaterialModel((MaterialModel) null);
			return;
		case Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_MODEL:
			setProcessSegmentModel((ProcessSegmentModel) null);
			return;
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_DEFINITION_MODEL:
			setOperationsDefinitionModel((OperationsDefinitionModel) null);
			return;
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_SCHEDULE_MODEL:
			setOperationsScheduleModel((OperationsScheduleModel) null);
			return;
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_PERFORMANCE_MODEL:
			setOperationsPerformanceModel((OperationsPerformanceModel) null);
			return;
		case Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_CAPABILITY_MODEL:
			setProcessSegmentCapabilityModel((ProcessSegmentCapabilityModel) null);
			return;
		case Iec62264Package.IEC62264_MODEL__HIERARCHY_SCOPES:
			getHierarchyScopes().clear();
			return;
		case Iec62264Package.IEC62264_MODEL__NAME:
			setName(NAME_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.IEC62264_MODEL__PERSONNEL_MODEL:
			return personnelModel != null;
		case Iec62264Package.IEC62264_MODEL__EQUIPMENT_MODEL:
			return equipmentModel != null;
		case Iec62264Package.IEC62264_MODEL__PHYSICAL_ASSET_MODEL:
			return physicalAssetModel != null;
		case Iec62264Package.IEC62264_MODEL__MATERIAL_MODEL:
			return materialModel != null;
		case Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_MODEL:
			return processSegmentModel != null;
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_DEFINITION_MODEL:
			return operationsDefinitionModel != null;
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_SCHEDULE_MODEL:
			return operationsScheduleModel != null;
		case Iec62264Package.IEC62264_MODEL__OPERATIONS_PERFORMANCE_MODEL:
			return operationsPerformanceModel != null;
		case Iec62264Package.IEC62264_MODEL__PROCESS_SEGMENT_CAPABILITY_MODEL:
			return processSegmentCapabilityModel != null;
		case Iec62264Package.IEC62264_MODEL__HIERARCHY_SCOPES:
			return hierarchyScopes != null && !hierarchyScopes.isEmpty();
		case Iec62264Package.IEC62264_MODEL__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //Iec62264ModelImpl
