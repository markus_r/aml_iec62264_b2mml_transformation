/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.time.ZonedDateTime;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Request</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getSegmentRequirements <em>Segment Requirements</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getRequestedSegmentResponses <em>Requested Segment Responses</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getOperationsDefinition <em>Operations Definition</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getPriority <em>Priority</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getRequestState <em>Request State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsRequest()
 * @model
 * @generated
 */
public interface OperationsRequest extends EObject {
	/**
	 * Returns the value of the '<em><b>Segment Requirements</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment Requirements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment Requirements</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsRequest_SegmentRequirements()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<SegmentRequirement> getSegmentRequirements();

	/**
	 * Returns the value of the '<em><b>Requested Segment Responses</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.RequestedSegmentResponse}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requested Segment Responses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requested Segment Responses</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsRequest_RequestedSegmentResponses()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequestedSegmentResponse> getRequestedSegmentResponses();

	/**
	 * Returns the value of the '<em><b>Operations Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition</em>' reference.
	 * @see #setOperationsDefinition(OperationsDefinition)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsRequest_OperationsDefinition()
	 * @model
	 * @generated
	 */
	OperationsDefinition getOperationsDefinition();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getOperationsDefinition <em>Operations Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Definition</em>' reference.
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	void setOperationsDefinition(OperationsDefinition value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsRequest_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsRequest_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.OperationsType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #setOperationsType(OperationsType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsRequest_OperationsType()
	 * @model required="true"
	 * @generated
	 */
	OperationsType getOperationsType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getOperationsType <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsType value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsRequest_StartTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(ZonedDateTime)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsRequest_EndTime()
	 * @model dataType="at.ac.tuwien.big.ame.iec62264.ZonedDateTime"
	 * @generated
	 */
	ZonedDateTime getEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(ZonedDateTime value);

	/**
	 * Returns the value of the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority</em>' attribute.
	 * @see #setPriority(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsRequest_Priority()
	 * @model
	 * @generated
	 */
	String getPriority();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getPriority <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority</em>' attribute.
	 * @see #getPriority()
	 * @generated
	 */
	void setPriority(String value);

	/**
	 * Returns the value of the '<em><b>Request State</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.ScheduleState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Request State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Request State</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.ScheduleState
	 * @see #setRequestState(ScheduleState)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsRequest_RequestState()
	 * @model
	 * @generated
	 */
	ScheduleState getRequestState();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getRequestState <em>Request State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Request State</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.ScheduleState
	 * @see #getRequestState()
	 * @generated
	 */
	void setRequestState(ScheduleState value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #setHierarchyScope(HierarchyScope)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsRequest_HierarchyScope()
	 * @model
	 * @generated
	 */
	HierarchyScope getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getHierarchyScope <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScope value);

} // OperationsRequest
