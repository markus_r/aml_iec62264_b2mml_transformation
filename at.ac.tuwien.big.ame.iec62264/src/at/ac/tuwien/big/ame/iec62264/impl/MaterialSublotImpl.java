/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.AssemblyRelationship;
import at.ac.tuwien.big.ame.iec62264.AssemblyType;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialLot;
import at.ac.tuwien.big.ame.iec62264.MaterialLotProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialSublot;

import java.math.BigDecimal;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Sublot</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl#getAssembledFromLots <em>Assembled From Lots</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl#getAssembledFromSublots <em>Assembled From Sublots</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl#getSublots <em>Sublots</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl#getStorageLocation <em>Storage Location</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MaterialSublotImpl extends MinimalEObjectImpl.Container implements MaterialSublot {
	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialLotProperty> properties;

	/**
	 * The cached value of the '{@link #getAssembledFromLots() <em>Assembled From Lots</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssembledFromLots()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialLot> assembledFromLots;

	/**
	 * The cached value of the '{@link #getAssembledFromSublots() <em>Assembled From Sublots</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssembledFromSublots()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSublot> assembledFromSublots;

	/**
	 * The cached value of the '{@link #getSublots() <em>Sublots</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSublots()
	 * @generated
	 * @ordered
	 */
	protected MaterialSublot sublots;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected static final AssemblyType ASSEMBLY_TYPE_EDEFAULT = AssemblyType.PHYSICAL;

	/**
	 * The cached value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected AssemblyType assemblyType = ASSEMBLY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected static final AssemblyRelationship ASSEMBLY_RELATIONSHIP_EDEFAULT = AssemblyRelationship.PERMANENT;

	/**
	 * The cached value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected AssemblyRelationship assemblyRelationship = ASSEMBLY_RELATIONSHIP_EDEFAULT;

	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final String STATUS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected String status = STATUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getStorageLocation() <em>Storage Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String STORAGE_LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStorageLocation() <em>Storage Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageLocation()
	 * @generated
	 * @ordered
	 */
	protected String storageLocation = STORAGE_LOCATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal QUANTITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal quantity = QUANTITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String QUANTITY_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String quantityUnitOfMeasure = QUANTITY_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialSublotImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.MATERIAL_SUBLOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialLotProperty> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<MaterialLotProperty>(MaterialLotProperty.class, this,
					Iec62264Package.MATERIAL_SUBLOT__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialLot> getAssembledFromLots() {
		if (assembledFromLots == null) {
			assembledFromLots = new EObjectResolvingEList<MaterialLot>(MaterialLot.class, this,
					Iec62264Package.MATERIAL_SUBLOT__ASSEMBLED_FROM_LOTS);
		}
		return assembledFromLots;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSublot> getAssembledFromSublots() {
		if (assembledFromSublots == null) {
			assembledFromSublots = new EObjectResolvingEList<MaterialSublot>(MaterialSublot.class, this,
					Iec62264Package.MATERIAL_SUBLOT__ASSEMBLED_FROM_SUBLOTS);
		}
		return assembledFromSublots;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSublot getSublots() {
		if (sublots != null && sublots.eIsProxy()) {
			InternalEObject oldSublots = (InternalEObject) sublots;
			sublots = (MaterialSublot) eResolveProxy(oldSublots);
			if (sublots != oldSublots) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Iec62264Package.MATERIAL_SUBLOT__SUBLOTS,
							oldSublots, sublots));
			}
		}
		return sublots;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSublot basicGetSublots() {
		return sublots;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSublots(MaterialSublot newSublots) {
		MaterialSublot oldSublots = sublots;
		sublots = newSublots;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_SUBLOT__SUBLOTS, oldSublots,
					sublots));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_SUBLOT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_SUBLOT__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyType getAssemblyType() {
		return assemblyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyType(AssemblyType newAssemblyType) {
		AssemblyType oldAssemblyType = assemblyType;
		assemblyType = newAssemblyType == null ? ASSEMBLY_TYPE_EDEFAULT : newAssemblyType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_SUBLOT__ASSEMBLY_TYPE,
					oldAssemblyType, assemblyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationship getAssemblyRelationship() {
		return assemblyRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyRelationship(AssemblyRelationship newAssemblyRelationship) {
		AssemblyRelationship oldAssemblyRelationship = assemblyRelationship;
		assemblyRelationship = newAssemblyRelationship == null ? ASSEMBLY_RELATIONSHIP_EDEFAULT
				: newAssemblyRelationship;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.MATERIAL_SUBLOT__ASSEMBLY_RELATIONSHIP, oldAssemblyRelationship,
					assemblyRelationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(String newStatus) {
		String oldStatus = status;
		status = newStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_SUBLOT__STATUS, oldStatus,
					status));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStorageLocation() {
		return storageLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageLocation(String newStorageLocation) {
		String oldStorageLocation = storageLocation;
		storageLocation = newStorageLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_SUBLOT__STORAGE_LOCATION,
					oldStorageLocation, storageLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(BigDecimal newQuantity) {
		BigDecimal oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_SUBLOT__QUANTITY,
					oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQuantityUnitOfMeasure() {
		return quantityUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantityUnitOfMeasure(String newQuantityUnitOfMeasure) {
		String oldQuantityUnitOfMeasure = quantityUnitOfMeasure;
		quantityUnitOfMeasure = newQuantityUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.MATERIAL_SUBLOT__QUANTITY_UNIT_OF_MEASURE, oldQuantityUnitOfMeasure,
					quantityUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_SUBLOT__PROPERTIES:
			return ((InternalEList<?>) getProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_SUBLOT__PROPERTIES:
			return getProperties();
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLED_FROM_LOTS:
			return getAssembledFromLots();
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLED_FROM_SUBLOTS:
			return getAssembledFromSublots();
		case Iec62264Package.MATERIAL_SUBLOT__SUBLOTS:
			if (resolve)
				return getSublots();
			return basicGetSublots();
		case Iec62264Package.MATERIAL_SUBLOT__ID:
			return getId();
		case Iec62264Package.MATERIAL_SUBLOT__DESCRIPTION:
			return getDescription();
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLY_TYPE:
			return getAssemblyType();
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLY_RELATIONSHIP:
			return getAssemblyRelationship();
		case Iec62264Package.MATERIAL_SUBLOT__STATUS:
			return getStatus();
		case Iec62264Package.MATERIAL_SUBLOT__STORAGE_LOCATION:
			return getStorageLocation();
		case Iec62264Package.MATERIAL_SUBLOT__QUANTITY:
			return getQuantity();
		case Iec62264Package.MATERIAL_SUBLOT__QUANTITY_UNIT_OF_MEASURE:
			return getQuantityUnitOfMeasure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_SUBLOT__PROPERTIES:
			getProperties().clear();
			getProperties().addAll((Collection<? extends MaterialLotProperty>) newValue);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLED_FROM_LOTS:
			getAssembledFromLots().clear();
			getAssembledFromLots().addAll((Collection<? extends MaterialLot>) newValue);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLED_FROM_SUBLOTS:
			getAssembledFromSublots().clear();
			getAssembledFromSublots().addAll((Collection<? extends MaterialSublot>) newValue);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__SUBLOTS:
			setSublots((MaterialSublot) newValue);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLY_TYPE:
			setAssemblyType((AssemblyType) newValue);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLY_RELATIONSHIP:
			setAssemblyRelationship((AssemblyRelationship) newValue);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__STATUS:
			setStatus((String) newValue);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__STORAGE_LOCATION:
			setStorageLocation((String) newValue);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__QUANTITY:
			setQuantity((BigDecimal) newValue);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_SUBLOT__PROPERTIES:
			getProperties().clear();
			return;
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLED_FROM_LOTS:
			getAssembledFromLots().clear();
			return;
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLED_FROM_SUBLOTS:
			getAssembledFromSublots().clear();
			return;
		case Iec62264Package.MATERIAL_SUBLOT__SUBLOTS:
			setSublots((MaterialSublot) null);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLY_TYPE:
			setAssemblyType(ASSEMBLY_TYPE_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLY_RELATIONSHIP:
			setAssemblyRelationship(ASSEMBLY_RELATIONSHIP_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__STATUS:
			setStatus(STATUS_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__STORAGE_LOCATION:
			setStorageLocation(STORAGE_LOCATION_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_SUBLOT__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure(QUANTITY_UNIT_OF_MEASURE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_SUBLOT__PROPERTIES:
			return properties != null && !properties.isEmpty();
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLED_FROM_LOTS:
			return assembledFromLots != null && !assembledFromLots.isEmpty();
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLED_FROM_SUBLOTS:
			return assembledFromSublots != null && !assembledFromSublots.isEmpty();
		case Iec62264Package.MATERIAL_SUBLOT__SUBLOTS:
			return sublots != null;
		case Iec62264Package.MATERIAL_SUBLOT__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.MATERIAL_SUBLOT__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLY_TYPE:
			return assemblyType != ASSEMBLY_TYPE_EDEFAULT;
		case Iec62264Package.MATERIAL_SUBLOT__ASSEMBLY_RELATIONSHIP:
			return assemblyRelationship != ASSEMBLY_RELATIONSHIP_EDEFAULT;
		case Iec62264Package.MATERIAL_SUBLOT__STATUS:
			return STATUS_EDEFAULT == null ? status != null : !STATUS_EDEFAULT.equals(status);
		case Iec62264Package.MATERIAL_SUBLOT__STORAGE_LOCATION:
			return STORAGE_LOCATION_EDEFAULT == null ? storageLocation != null
					: !STORAGE_LOCATION_EDEFAULT.equals(storageLocation);
		case Iec62264Package.MATERIAL_SUBLOT__QUANTITY:
			return QUANTITY_EDEFAULT == null ? quantity != null : !QUANTITY_EDEFAULT.equals(quantity);
		case Iec62264Package.MATERIAL_SUBLOT__QUANTITY_UNIT_OF_MEASURE:
			return QUANTITY_UNIT_OF_MEASURE_EDEFAULT == null ? quantityUnitOfMeasure != null
					: !QUANTITY_UNIT_OF_MEASURE_EDEFAULT.equals(quantityUnitOfMeasure);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", assemblyType: ");
		result.append(assemblyType);
		result.append(", assemblyRelationship: ");
		result.append(assemblyRelationship);
		result.append(", status: ");
		result.append(status);
		result.append(", storageLocation: ");
		result.append(storageLocation);
		result.append(", quantity: ");
		result.append(quantity);
		result.append(", quantityUnitOfMeasure: ");
		result.append(quantityUnitOfMeasure);
		result.append(')');
		return result.toString();
	}

} //MaterialSublotImpl
