/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getOperationsSegments <em>Operations Segments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getOperationsMaterialBills <em>Operations Material Bills</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getVersion <em>Version</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getBillOfMaterialId <em>Bill Of Material Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getWorkDefinitionId <em>Work Definition Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getBillOfResourceId <em>Bill Of Resource Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getHierarchysScope <em>Hierarchys Scope</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinition()
 * @model
 * @generated
 */
public interface OperationsDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Operations Segments</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.OperationsSegment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Segments</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Segments</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinition_OperationsSegments()
	 * @model required="true"
	 * @generated
	 */
	EList<OperationsSegment> getOperationsSegments();

	/**
	 * Returns the value of the '<em><b>Operations Material Bills</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Material Bills</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Material Bills</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinition_OperationsMaterialBills()
	 * @model containment="true"
	 * @generated
	 */
	EList<OperationsMaterialBill> getOperationsMaterialBills();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinition_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinition_Version()
	 * @model
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinition_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.OperationsType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #setOperationsType(OperationsType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinition_OperationsType()
	 * @model required="true"
	 * @generated
	 */
	OperationsType getOperationsType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getOperationsType <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsType value);

	/**
	 * Returns the value of the '<em><b>Bill Of Material Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bill Of Material Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bill Of Material Id</em>' attribute.
	 * @see #setBillOfMaterialId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinition_BillOfMaterialId()
	 * @model
	 * @generated
	 */
	String getBillOfMaterialId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getBillOfMaterialId <em>Bill Of Material Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bill Of Material Id</em>' attribute.
	 * @see #getBillOfMaterialId()
	 * @generated
	 */
	void setBillOfMaterialId(String value);

	/**
	 * Returns the value of the '<em><b>Work Definition Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Work Definition Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Work Definition Id</em>' attribute.
	 * @see #setWorkDefinitionId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinition_WorkDefinitionId()
	 * @model
	 * @generated
	 */
	String getWorkDefinitionId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getWorkDefinitionId <em>Work Definition Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Work Definition Id</em>' attribute.
	 * @see #getWorkDefinitionId()
	 * @generated
	 */
	void setWorkDefinitionId(String value);

	/**
	 * Returns the value of the '<em><b>Bill Of Resource Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bill Of Resource Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bill Of Resource Id</em>' attribute.
	 * @see #setBillOfResourceId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinition_BillOfResourceId()
	 * @model
	 * @generated
	 */
	String getBillOfResourceId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getBillOfResourceId <em>Bill Of Resource Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bill Of Resource Id</em>' attribute.
	 * @see #getBillOfResourceId()
	 * @generated
	 */
	void setBillOfResourceId(String value);

	/**
	 * Returns the value of the '<em><b>Hierarchys Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchys Scope</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchys Scope</em>' reference.
	 * @see #setHierarchysScope(HierarchyScope)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsDefinition_HierarchysScope()
	 * @model
	 * @generated
	 */
	HierarchyScope getHierarchysScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getHierarchysScope <em>Hierarchys Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchys Scope</em>' reference.
	 * @see #getHierarchysScope()
	 * @generated
	 */
	void setHierarchysScope(HierarchyScope value);

} // OperationsDefinition
