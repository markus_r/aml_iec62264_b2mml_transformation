/**
 */
package at.ac.tuwien.big.ame.iec62264;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Segment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getProcessSegments <em>Process Segments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getSubSegments <em>Sub Segments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getParameterSpecifications <em>Parameter Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getPersonnelSpecifications <em>Personnel Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getEquipmentSpecifications <em>Equipment Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getPhysicalAssetSpecifications <em>Physical Asset Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getMaterialSpecifications <em>Material Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getDuration <em>Duration</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getDurationUnitOfMeasure <em>Duration Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getWorkDefinitionId <em>Work Definition Id</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment()
 * @model
 * @generated
 */
public interface OperationsSegment extends EObject {
	/**
	 * Returns the value of the '<em><b>Process Segments</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.ProcessSegment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segments</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segments</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_ProcessSegments()
	 * @model required="true"
	 * @generated
	 */
	EList<ProcessSegment> getProcessSegments();

	/**
	 * Returns the value of the '<em><b>Sub Segments</b></em>' reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.OperationsSegment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Segments</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Segments</em>' reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_SubSegments()
	 * @model
	 * @generated
	 */
	EList<OperationsSegment> getSubSegments();

	/**
	 * Returns the value of the '<em><b>Parameter Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.ParameterSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Specifications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Specifications</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_ParameterSpecifications()
	 * @model containment="true"
	 * @generated
	 */
	EList<ParameterSpecification> getParameterSpecifications();

	/**
	 * Returns the value of the '<em><b>Personnel Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Specifications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Specifications</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_PersonnelSpecifications()
	 * @model containment="true"
	 * @generated
	 */
	EList<PersonnelSpecification> getPersonnelSpecifications();

	/**
	 * Returns the value of the '<em><b>Equipment Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Specifications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Specifications</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_EquipmentSpecifications()
	 * @model containment="true"
	 * @generated
	 */
	EList<EquipmentSpecification> getEquipmentSpecifications();

	/**
	 * Returns the value of the '<em><b>Physical Asset Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Specifications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Specifications</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_PhysicalAssetSpecifications()
	 * @model containment="true"
	 * @generated
	 */
	EList<PhysicalAssetSpecification> getPhysicalAssetSpecifications();

	/**
	 * Returns the value of the '<em><b>Material Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Specifications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Specifications</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_MaterialSpecifications()
	 * @model containment="true"
	 * @generated
	 */
	EList<MaterialSpecification> getMaterialSpecifications();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(BigDecimal)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_Duration()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 * @generated
	 */
	BigDecimal getDuration();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Duration Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration Unit Of Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration Unit Of Measure</em>' attribute.
	 * @see #setDurationUnitOfMeasure(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_DurationUnitOfMeasure()
	 * @model
	 * @generated
	 */
	String getDurationUnitOfMeasure();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getDurationUnitOfMeasure <em>Duration Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration Unit Of Measure</em>' attribute.
	 * @see #getDurationUnitOfMeasure()
	 * @generated
	 */
	void setDurationUnitOfMeasure(String value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.iec62264.OperationsType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #setOperationsType(OperationsType)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_OperationsType()
	 * @model required="true"
	 * @generated
	 */
	OperationsType getOperationsType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getOperationsType <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' attribute.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsType value);

	/**
	 * Returns the value of the '<em><b>Work Definition Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Work Definition Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Work Definition Id</em>' attribute.
	 * @see #setWorkDefinitionId(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getOperationsSegment_WorkDefinitionId()
	 * @model
	 * @generated
	 */
	String getWorkDefinitionId();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getWorkDefinitionId <em>Work Definition Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Work Definition Id</em>' attribute.
	 * @see #getWorkDefinitionId()
	 * @generated
	 */
	void setWorkDefinitionId(String value);

} // OperationsSegment
