/**
 */
package at.ac.tuwien.big.ame.iec62264;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Referential Personnel Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty#getPersonnelClassProperty <em>Personnel Class Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty#getPersonProperty <em>Person Property</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialPersonnelProperty()
 * @model abstract="true"
 * @generated
 */
public interface ReferentialPersonnelProperty extends ReferencialProperty {
	/**
	 * Returns the value of the '<em><b>Personnel Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Class Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Class Property</em>' reference.
	 * @see #setPersonnelClassProperty(PersonnelClassProperty)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialPersonnelProperty_PersonnelClassProperty()
	 * @model
	 * @generated
	 */
	PersonnelClassProperty getPersonnelClassProperty();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty#getPersonnelClassProperty <em>Personnel Class Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Personnel Class Property</em>' reference.
	 * @see #getPersonnelClassProperty()
	 * @generated
	 */
	void setPersonnelClassProperty(PersonnelClassProperty value);

	/**
	 * Returns the value of the '<em><b>Person Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person Property</em>' reference.
	 * @see #setPersonProperty(PersonProperty)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialPersonnelProperty_PersonProperty()
	 * @model
	 * @generated
	 */
	PersonProperty getPersonProperty();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty#getPersonProperty <em>Person Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Person Property</em>' reference.
	 * @see #getPersonProperty()
	 * @generated
	 */
	void setPersonProperty(PersonProperty value);

} // ReferentialPersonnelProperty
