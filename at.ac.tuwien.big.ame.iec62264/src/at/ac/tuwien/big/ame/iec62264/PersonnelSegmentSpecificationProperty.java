/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Personnel Segment Specification Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecificationProperty#getSubProperties <em>Sub Properties</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPersonnelSegmentSpecificationProperty()
 * @model
 * @generated
 */
public interface PersonnelSegmentSpecificationProperty extends ReferentialPersonnelProperty {
	/**
	 * Returns the value of the '<em><b>Sub Properties</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecificationProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Properties</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getPersonnelSegmentSpecificationProperty_SubProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<PersonnelSegmentSpecificationProperty> getSubProperties();

} // PersonnelSegmentSpecificationProperty
