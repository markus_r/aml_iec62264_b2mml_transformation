/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.CapabilityType;
import at.ac.tuwien.big.ame.iec62264.Equipment;
import at.ac.tuwien.big.ame.iec62264.EquipmentCapability;
import at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityProperty;
import at.ac.tuwien.big.ame.iec62264.EquipmentClass;
import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;

import java.math.BigDecimal;

import java.time.ZonedDateTime;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Capability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getEquipmentCapabilityProperty <em>Equipment Capability Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getEquipmentClass <em>Equipment Class</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getEquipment <em>Equipment</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getReason <em>Reason</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getEquipmentUse <em>Equipment Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EquipmentCapabilityImpl extends MinimalEObjectImpl.Container implements EquipmentCapability {
	/**
	 * The cached value of the '{@link #getEquipmentCapabilityProperty() <em>Equipment Capability Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentCapabilityProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentCapabilityProperty> equipmentCapabilityProperty;

	/**
	 * The cached value of the '{@link #getEquipmentClass() <em>Equipment Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentClass()
	 * @generated
	 * @ordered
	 */
	protected EquipmentClass equipmentClass;

	/**
	 * The cached value of the '{@link #getEquipment() <em>Equipment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipment()
	 * @generated
	 * @ordered
	 */
	protected Equipment equipment;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCapabilityType() <em>Capability Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilityType()
	 * @generated
	 * @ordered
	 */
	protected static final CapabilityType CAPABILITY_TYPE_EDEFAULT = CapabilityType.USED;

	/**
	 * The cached value of the '{@link #getCapabilityType() <em>Capability Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilityType()
	 * @generated
	 * @ordered
	 */
	protected CapabilityType capabilityType = CAPABILITY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getReason() <em>Reason</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected static final String REASON_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReason() <em>Reason</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected String reason = REASON_EDEFAULT;

	/**
	 * The default value of the '{@link #getConfidenceFactor() <em>Confidence Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceFactor()
	 * @generated
	 * @ordered
	 */
	protected static final String CONFIDENCE_FACTOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConfidenceFactor() <em>Confidence Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceFactor()
	 * @generated
	 * @ordered
	 */
	protected String confidenceFactor = CONFIDENCE_FACTOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getEquipmentUse() <em>Equipment Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentUse()
	 * @generated
	 * @ordered
	 */
	protected static final String EQUIPMENT_USE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEquipmentUse() <em>Equipment Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentUse()
	 * @generated
	 * @ordered
	 */
	protected String equipmentUse = EQUIPMENT_USE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime startTime = START_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime END_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime endTime = END_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal QUANTITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal quantity = QUANTITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String QUANTITY_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String quantityUnitOfMeasure = QUANTITY_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScope hierarchyScope;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentCapabilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.EQUIPMENT_CAPABILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentCapabilityProperty> getEquipmentCapabilityProperty() {
		if (equipmentCapabilityProperty == null) {
			equipmentCapabilityProperty = new EObjectContainmentEList<EquipmentCapabilityProperty>(
					EquipmentCapabilityProperty.class, this,
					Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_CAPABILITY_PROPERTY);
		}
		return equipmentCapabilityProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClass getEquipmentClass() {
		if (equipmentClass != null && equipmentClass.eIsProxy()) {
			InternalEObject oldEquipmentClass = (InternalEObject) equipmentClass;
			equipmentClass = (EquipmentClass) eResolveProxy(oldEquipmentClass);
			if (equipmentClass != oldEquipmentClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_CLASS, oldEquipmentClass, equipmentClass));
			}
		}
		return equipmentClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClass basicGetEquipmentClass() {
		return equipmentClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentClass(EquipmentClass newEquipmentClass) {
		EquipmentClass oldEquipmentClass = equipmentClass;
		equipmentClass = newEquipmentClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_CLASS,
					oldEquipmentClass, equipmentClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Equipment getEquipment() {
		if (equipment != null && equipment.eIsProxy()) {
			InternalEObject oldEquipment = (InternalEObject) equipment;
			equipment = (Equipment) eResolveProxy(oldEquipment);
			if (equipment != oldEquipment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT, oldEquipment, equipment));
			}
		}
		return equipment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Equipment basicGetEquipment() {
		return equipment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipment(Equipment newEquipment) {
		Equipment oldEquipment = equipment;
		equipment = newEquipment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT,
					oldEquipment, equipment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_CAPABILITY__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityType getCapabilityType() {
		return capabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCapabilityType(CapabilityType newCapabilityType) {
		CapabilityType oldCapabilityType = capabilityType;
		capabilityType = newCapabilityType == null ? CAPABILITY_TYPE_EDEFAULT : newCapabilityType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_CAPABILITY__CAPABILITY_TYPE,
					oldCapabilityType, capabilityType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReason(String newReason) {
		String oldReason = reason;
		reason = newReason;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_CAPABILITY__REASON,
					oldReason, reason));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConfidenceFactor() {
		return confidenceFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfidenceFactor(String newConfidenceFactor) {
		String oldConfidenceFactor = confidenceFactor;
		confidenceFactor = newConfidenceFactor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.EQUIPMENT_CAPABILITY__CONFIDENCE_FACTOR, oldConfidenceFactor, confidenceFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEquipmentUse() {
		return equipmentUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentUse(String newEquipmentUse) {
		String oldEquipmentUse = equipmentUse;
		equipmentUse = newEquipmentUse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_USE,
					oldEquipmentUse, equipmentUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(ZonedDateTime newStartTime) {
		ZonedDateTime oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_CAPABILITY__START_TIME,
					oldStartTime, startTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(ZonedDateTime newEndTime) {
		ZonedDateTime oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_CAPABILITY__END_TIME,
					oldEndTime, endTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(BigDecimal newQuantity) {
		BigDecimal oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_CAPABILITY__QUANTITY,
					oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQuantityUnitOfMeasure() {
		return quantityUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantityUnitOfMeasure(String newQuantityUnitOfMeasure) {
		String oldQuantityUnitOfMeasure = quantityUnitOfMeasure;
		quantityUnitOfMeasure = newQuantityUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.EQUIPMENT_CAPABILITY__QUANTITY_UNIT_OF_MEASURE, oldQuantityUnitOfMeasure,
					quantityUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope getHierarchyScope() {
		if (hierarchyScope != null && hierarchyScope.eIsProxy()) {
			InternalEObject oldHierarchyScope = (InternalEObject) hierarchyScope;
			hierarchyScope = (HierarchyScope) eResolveProxy(oldHierarchyScope);
			if (hierarchyScope != oldHierarchyScope) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.EQUIPMENT_CAPABILITY__HIERARCHY_SCOPE, oldHierarchyScope, hierarchyScope));
			}
		}
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope basicGetHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScope newHierarchyScope) {
		HierarchyScope oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.EQUIPMENT_CAPABILITY__HIERARCHY_SCOPE,
					oldHierarchyScope, hierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_CAPABILITY_PROPERTY:
			return ((InternalEList<?>) getEquipmentCapabilityProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_CAPABILITY_PROPERTY:
			return getEquipmentCapabilityProperty();
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_CLASS:
			if (resolve)
				return getEquipmentClass();
			return basicGetEquipmentClass();
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT:
			if (resolve)
				return getEquipment();
			return basicGetEquipment();
		case Iec62264Package.EQUIPMENT_CAPABILITY__DESCRIPTION:
			return getDescription();
		case Iec62264Package.EQUIPMENT_CAPABILITY__CAPABILITY_TYPE:
			return getCapabilityType();
		case Iec62264Package.EQUIPMENT_CAPABILITY__REASON:
			return getReason();
		case Iec62264Package.EQUIPMENT_CAPABILITY__CONFIDENCE_FACTOR:
			return getConfidenceFactor();
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_USE:
			return getEquipmentUse();
		case Iec62264Package.EQUIPMENT_CAPABILITY__START_TIME:
			return getStartTime();
		case Iec62264Package.EQUIPMENT_CAPABILITY__END_TIME:
			return getEndTime();
		case Iec62264Package.EQUIPMENT_CAPABILITY__QUANTITY:
			return getQuantity();
		case Iec62264Package.EQUIPMENT_CAPABILITY__QUANTITY_UNIT_OF_MEASURE:
			return getQuantityUnitOfMeasure();
		case Iec62264Package.EQUIPMENT_CAPABILITY__HIERARCHY_SCOPE:
			if (resolve)
				return getHierarchyScope();
			return basicGetHierarchyScope();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_CAPABILITY_PROPERTY:
			getEquipmentCapabilityProperty().clear();
			getEquipmentCapabilityProperty().addAll((Collection<? extends EquipmentCapabilityProperty>) newValue);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_CLASS:
			setEquipmentClass((EquipmentClass) newValue);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT:
			setEquipment((Equipment) newValue);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__CAPABILITY_TYPE:
			setCapabilityType((CapabilityType) newValue);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__REASON:
			setReason((String) newValue);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__CONFIDENCE_FACTOR:
			setConfidenceFactor((String) newValue);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_USE:
			setEquipmentUse((String) newValue);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__START_TIME:
			setStartTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__END_TIME:
			setEndTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__QUANTITY:
			setQuantity((BigDecimal) newValue);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure((String) newValue);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_CAPABILITY_PROPERTY:
			getEquipmentCapabilityProperty().clear();
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_CLASS:
			setEquipmentClass((EquipmentClass) null);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT:
			setEquipment((Equipment) null);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__CAPABILITY_TYPE:
			setCapabilityType(CAPABILITY_TYPE_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__REASON:
			setReason(REASON_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__CONFIDENCE_FACTOR:
			setConfidenceFactor(CONFIDENCE_FACTOR_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_USE:
			setEquipmentUse(EQUIPMENT_USE_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__START_TIME:
			setStartTime(START_TIME_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__END_TIME:
			setEndTime(END_TIME_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure(QUANTITY_UNIT_OF_MEASURE_EDEFAULT);
			return;
		case Iec62264Package.EQUIPMENT_CAPABILITY__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_CAPABILITY_PROPERTY:
			return equipmentCapabilityProperty != null && !equipmentCapabilityProperty.isEmpty();
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_CLASS:
			return equipmentClass != null;
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT:
			return equipment != null;
		case Iec62264Package.EQUIPMENT_CAPABILITY__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.EQUIPMENT_CAPABILITY__CAPABILITY_TYPE:
			return capabilityType != CAPABILITY_TYPE_EDEFAULT;
		case Iec62264Package.EQUIPMENT_CAPABILITY__REASON:
			return REASON_EDEFAULT == null ? reason != null : !REASON_EDEFAULT.equals(reason);
		case Iec62264Package.EQUIPMENT_CAPABILITY__CONFIDENCE_FACTOR:
			return CONFIDENCE_FACTOR_EDEFAULT == null ? confidenceFactor != null
					: !CONFIDENCE_FACTOR_EDEFAULT.equals(confidenceFactor);
		case Iec62264Package.EQUIPMENT_CAPABILITY__EQUIPMENT_USE:
			return EQUIPMENT_USE_EDEFAULT == null ? equipmentUse != null : !EQUIPMENT_USE_EDEFAULT.equals(equipmentUse);
		case Iec62264Package.EQUIPMENT_CAPABILITY__START_TIME:
			return START_TIME_EDEFAULT == null ? startTime != null : !START_TIME_EDEFAULT.equals(startTime);
		case Iec62264Package.EQUIPMENT_CAPABILITY__END_TIME:
			return END_TIME_EDEFAULT == null ? endTime != null : !END_TIME_EDEFAULT.equals(endTime);
		case Iec62264Package.EQUIPMENT_CAPABILITY__QUANTITY:
			return QUANTITY_EDEFAULT == null ? quantity != null : !QUANTITY_EDEFAULT.equals(quantity);
		case Iec62264Package.EQUIPMENT_CAPABILITY__QUANTITY_UNIT_OF_MEASURE:
			return QUANTITY_UNIT_OF_MEASURE_EDEFAULT == null ? quantityUnitOfMeasure != null
					: !QUANTITY_UNIT_OF_MEASURE_EDEFAULT.equals(quantityUnitOfMeasure);
		case Iec62264Package.EQUIPMENT_CAPABILITY__HIERARCHY_SCOPE:
			return hierarchyScope != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", capabilityType: ");
		result.append(capabilityType);
		result.append(", reason: ");
		result.append(reason);
		result.append(", confidenceFactor: ");
		result.append(confidenceFactor);
		result.append(", equipmentUse: ");
		result.append(equipmentUse);
		result.append(", startTime: ");
		result.append(startTime);
		result.append(", endTime: ");
		result.append(endTime);
		result.append(", quantity: ");
		result.append(quantity);
		result.append(", quantityUnitOfMeasure: ");
		result.append(quantityUnitOfMeasure);
		result.append(')');
		return result.toString();
	}

} //EquipmentCapabilityImpl
