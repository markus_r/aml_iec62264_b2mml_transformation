/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.CapabilityType;
import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.PhysicalAsset;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityProperty;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass;

import java.math.BigDecimal;

import java.time.ZonedDateTime;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Capability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getPhysicalAssetCapabilityProperty <em>Physical Asset Capability Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getPhysicalAssetClass <em>Physical Asset Class</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getPhysicalAsset <em>Physical Asset</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getReason <em>Reason</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getPhysicalAssetUse <em>Physical Asset Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalAssetCapabilityImpl extends MinimalEObjectImpl.Container implements PhysicalAssetCapability {
	/**
	 * The cached value of the '{@link #getPhysicalAssetCapabilityProperty() <em>Physical Asset Capability Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetCapabilityProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetCapabilityProperty> physicalAssetCapabilityProperty;

	/**
	 * The cached value of the '{@link #getPhysicalAssetClass() <em>Physical Asset Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClass()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetClass physicalAssetClass;

	/**
	 * The cached value of the '{@link #getPhysicalAsset() <em>Physical Asset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAsset()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAsset physicalAsset;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCapabilityType() <em>Capability Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilityType()
	 * @generated
	 * @ordered
	 */
	protected static final CapabilityType CAPABILITY_TYPE_EDEFAULT = CapabilityType.USED;

	/**
	 * The cached value of the '{@link #getCapabilityType() <em>Capability Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilityType()
	 * @generated
	 * @ordered
	 */
	protected CapabilityType capabilityType = CAPABILITY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getReason() <em>Reason</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected static final String REASON_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReason() <em>Reason</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected String reason = REASON_EDEFAULT;

	/**
	 * The default value of the '{@link #getConfidenceFactor() <em>Confidence Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceFactor()
	 * @generated
	 * @ordered
	 */
	protected static final String CONFIDENCE_FACTOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConfidenceFactor() <em>Confidence Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceFactor()
	 * @generated
	 * @ordered
	 */
	protected String confidenceFactor = CONFIDENCE_FACTOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getPhysicalAssetUse() <em>Physical Asset Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetUse()
	 * @generated
	 * @ordered
	 */
	protected static final String PHYSICAL_ASSET_USE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPhysicalAssetUse() <em>Physical Asset Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetUse()
	 * @generated
	 * @ordered
	 */
	protected String physicalAssetUse = PHYSICAL_ASSET_USE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScope hierarchyScope;

	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime startTime = START_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime END_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime endTime = END_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal QUANTITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal quantity = QUANTITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String QUANTITY_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String quantityUnitOfMeasure = QUANTITY_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetCapabilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PHYSICAL_ASSET_CAPABILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetCapabilityProperty> getPhysicalAssetCapabilityProperty() {
		if (physicalAssetCapabilityProperty == null) {
			physicalAssetCapabilityProperty = new EObjectContainmentEList<PhysicalAssetCapabilityProperty>(
					PhysicalAssetCapabilityProperty.class, this,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CAPABILITY_PROPERTY);
		}
		return physicalAssetCapabilityProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClass getPhysicalAssetClass() {
		if (physicalAssetClass != null && physicalAssetClass.eIsProxy()) {
			InternalEObject oldPhysicalAssetClass = (InternalEObject) physicalAssetClass;
			physicalAssetClass = (PhysicalAssetClass) eResolveProxy(oldPhysicalAssetClass);
			if (physicalAssetClass != oldPhysicalAssetClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CLASS, oldPhysicalAssetClass,
							physicalAssetClass));
			}
		}
		return physicalAssetClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClass basicGetPhysicalAssetClass() {
		return physicalAssetClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetClass(PhysicalAssetClass newPhysicalAssetClass) {
		PhysicalAssetClass oldPhysicalAssetClass = physicalAssetClass;
		physicalAssetClass = newPhysicalAssetClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CLASS, oldPhysicalAssetClass,
					physicalAssetClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAsset getPhysicalAsset() {
		if (physicalAsset != null && physicalAsset.eIsProxy()) {
			InternalEObject oldPhysicalAsset = (InternalEObject) physicalAsset;
			physicalAsset = (PhysicalAsset) eResolveProxy(oldPhysicalAsset);
			if (physicalAsset != oldPhysicalAsset) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET, oldPhysicalAsset,
							physicalAsset));
			}
		}
		return physicalAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAsset basicGetPhysicalAsset() {
		return physicalAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAsset(PhysicalAsset newPhysicalAsset) {
		PhysicalAsset oldPhysicalAsset = physicalAsset;
		physicalAsset = newPhysicalAsset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET, oldPhysicalAsset, physicalAsset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityType getCapabilityType() {
		return capabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCapabilityType(CapabilityType newCapabilityType) {
		CapabilityType oldCapabilityType = capabilityType;
		capabilityType = newCapabilityType == null ? CAPABILITY_TYPE_EDEFAULT : newCapabilityType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY__CAPABILITY_TYPE, oldCapabilityType, capabilityType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReason(String newReason) {
		String oldReason = reason;
		reason = newReason;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PHYSICAL_ASSET_CAPABILITY__REASON,
					oldReason, reason));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConfidenceFactor() {
		return confidenceFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfidenceFactor(String newConfidenceFactor) {
		String oldConfidenceFactor = confidenceFactor;
		confidenceFactor = newConfidenceFactor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY__CONFIDENCE_FACTOR, oldConfidenceFactor,
					confidenceFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPhysicalAssetUse() {
		return physicalAssetUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetUse(String newPhysicalAssetUse) {
		String oldPhysicalAssetUse = physicalAssetUse;
		physicalAssetUse = newPhysicalAssetUse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_USE, oldPhysicalAssetUse,
					physicalAssetUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope getHierarchyScope() {
		if (hierarchyScope != null && hierarchyScope.eIsProxy()) {
			InternalEObject oldHierarchyScope = (InternalEObject) hierarchyScope;
			hierarchyScope = (HierarchyScope) eResolveProxy(oldHierarchyScope);
			if (hierarchyScope != oldHierarchyScope) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.PHYSICAL_ASSET_CAPABILITY__HIERARCHY_SCOPE, oldHierarchyScope,
							hierarchyScope));
			}
		}
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope basicGetHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScope newHierarchyScope) {
		HierarchyScope oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY__HIERARCHY_SCOPE, oldHierarchyScope, hierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(ZonedDateTime newStartTime) {
		ZonedDateTime oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PHYSICAL_ASSET_CAPABILITY__START_TIME,
					oldStartTime, startTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(ZonedDateTime newEndTime) {
		ZonedDateTime oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PHYSICAL_ASSET_CAPABILITY__END_TIME,
					oldEndTime, endTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(BigDecimal newQuantity) {
		BigDecimal oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PHYSICAL_ASSET_CAPABILITY__QUANTITY,
					oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQuantityUnitOfMeasure() {
		return quantityUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantityUnitOfMeasure(String newQuantityUnitOfMeasure) {
		String oldQuantityUnitOfMeasure = quantityUnitOfMeasure;
		quantityUnitOfMeasure = newQuantityUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_CAPABILITY__QUANTITY_UNIT_OF_MEASURE, oldQuantityUnitOfMeasure,
					quantityUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CAPABILITY_PROPERTY:
			return ((InternalEList<?>) getPhysicalAssetCapabilityProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CAPABILITY_PROPERTY:
			return getPhysicalAssetCapabilityProperty();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CLASS:
			if (resolve)
				return getPhysicalAssetClass();
			return basicGetPhysicalAssetClass();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET:
			if (resolve)
				return getPhysicalAsset();
			return basicGetPhysicalAsset();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__DESCRIPTION:
			return getDescription();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__CAPABILITY_TYPE:
			return getCapabilityType();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__REASON:
			return getReason();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__CONFIDENCE_FACTOR:
			return getConfidenceFactor();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_USE:
			return getPhysicalAssetUse();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__HIERARCHY_SCOPE:
			if (resolve)
				return getHierarchyScope();
			return basicGetHierarchyScope();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__START_TIME:
			return getStartTime();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__END_TIME:
			return getEndTime();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__QUANTITY:
			return getQuantity();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__QUANTITY_UNIT_OF_MEASURE:
			return getQuantityUnitOfMeasure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CAPABILITY_PROPERTY:
			getPhysicalAssetCapabilityProperty().clear();
			getPhysicalAssetCapabilityProperty()
					.addAll((Collection<? extends PhysicalAssetCapabilityProperty>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CLASS:
			setPhysicalAssetClass((PhysicalAssetClass) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET:
			setPhysicalAsset((PhysicalAsset) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__CAPABILITY_TYPE:
			setCapabilityType((CapabilityType) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__REASON:
			setReason((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__CONFIDENCE_FACTOR:
			setConfidenceFactor((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_USE:
			setPhysicalAssetUse((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__START_TIME:
			setStartTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__END_TIME:
			setEndTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__QUANTITY:
			setQuantity((BigDecimal) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CAPABILITY_PROPERTY:
			getPhysicalAssetCapabilityProperty().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CLASS:
			setPhysicalAssetClass((PhysicalAssetClass) null);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET:
			setPhysicalAsset((PhysicalAsset) null);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__CAPABILITY_TYPE:
			setCapabilityType(CAPABILITY_TYPE_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__REASON:
			setReason(REASON_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__CONFIDENCE_FACTOR:
			setConfidenceFactor(CONFIDENCE_FACTOR_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_USE:
			setPhysicalAssetUse(PHYSICAL_ASSET_USE_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) null);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__START_TIME:
			setStartTime(START_TIME_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__END_TIME:
			setEndTime(END_TIME_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure(QUANTITY_UNIT_OF_MEASURE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CAPABILITY_PROPERTY:
			return physicalAssetCapabilityProperty != null && !physicalAssetCapabilityProperty.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CLASS:
			return physicalAssetClass != null;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET:
			return physicalAsset != null;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__CAPABILITY_TYPE:
			return capabilityType != CAPABILITY_TYPE_EDEFAULT;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__REASON:
			return REASON_EDEFAULT == null ? reason != null : !REASON_EDEFAULT.equals(reason);
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__CONFIDENCE_FACTOR:
			return CONFIDENCE_FACTOR_EDEFAULT == null ? confidenceFactor != null
					: !CONFIDENCE_FACTOR_EDEFAULT.equals(confidenceFactor);
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_USE:
			return PHYSICAL_ASSET_USE_EDEFAULT == null ? physicalAssetUse != null
					: !PHYSICAL_ASSET_USE_EDEFAULT.equals(physicalAssetUse);
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__HIERARCHY_SCOPE:
			return hierarchyScope != null;
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__START_TIME:
			return START_TIME_EDEFAULT == null ? startTime != null : !START_TIME_EDEFAULT.equals(startTime);
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__END_TIME:
			return END_TIME_EDEFAULT == null ? endTime != null : !END_TIME_EDEFAULT.equals(endTime);
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__QUANTITY:
			return QUANTITY_EDEFAULT == null ? quantity != null : !QUANTITY_EDEFAULT.equals(quantity);
		case Iec62264Package.PHYSICAL_ASSET_CAPABILITY__QUANTITY_UNIT_OF_MEASURE:
			return QUANTITY_UNIT_OF_MEASURE_EDEFAULT == null ? quantityUnitOfMeasure != null
					: !QUANTITY_UNIT_OF_MEASURE_EDEFAULT.equals(quantityUnitOfMeasure);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", capabilityType: ");
		result.append(capabilityType);
		result.append(", reason: ");
		result.append(reason);
		result.append(", confidenceFactor: ");
		result.append(confidenceFactor);
		result.append(", physicalAssetUse: ");
		result.append(physicalAssetUse);
		result.append(", startTime: ");
		result.append(startTime);
		result.append(", endTime: ");
		result.append(endTime);
		result.append(", quantity: ");
		result.append(quantity);
		result.append(", quantityUnitOfMeasure: ");
		result.append(quantityUnitOfMeasure);
		result.append(')');
		return result.toString();
	}

} //PhysicalAssetCapabilityImpl
