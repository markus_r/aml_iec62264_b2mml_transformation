/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty;
import at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Referential Physical Asset Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialPhysicalAssetPropertyImpl#getPhysicalAssetClassProperty <em>Physical Asset Class Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialPhysicalAssetPropertyImpl#getPhysicalAssetProperty <em>Physical Asset Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ReferentialPhysicalAssetPropertyImpl extends ReferencialPropertyImpl
		implements ReferentialPhysicalAssetProperty {
	/**
	 * The cached value of the '{@link #getPhysicalAssetClassProperty() <em>Physical Asset Class Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClassProperty()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetClassProperty physicalAssetClassProperty;

	/**
	 * The cached value of the '{@link #getPhysicalAssetProperty() <em>Physical Asset Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetProperty()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetProperty physicalAssetProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferentialPhysicalAssetPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.REFERENTIAL_PHYSICAL_ASSET_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClassProperty getPhysicalAssetClassProperty() {
		if (physicalAssetClassProperty != null && physicalAssetClassProperty.eIsProxy()) {
			InternalEObject oldPhysicalAssetClassProperty = (InternalEObject) physicalAssetClassProperty;
			physicalAssetClassProperty = (PhysicalAssetClassProperty) eResolveProxy(oldPhysicalAssetClassProperty);
			if (physicalAssetClassProperty != oldPhysicalAssetClassProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY,
							oldPhysicalAssetClassProperty, physicalAssetClassProperty));
			}
		}
		return physicalAssetClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClassProperty basicGetPhysicalAssetClassProperty() {
		return physicalAssetClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetClassProperty(PhysicalAssetClassProperty newPhysicalAssetClassProperty) {
		PhysicalAssetClassProperty oldPhysicalAssetClassProperty = physicalAssetClassProperty;
		physicalAssetClassProperty = newPhysicalAssetClassProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY,
					oldPhysicalAssetClassProperty, physicalAssetClassProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetProperty getPhysicalAssetProperty() {
		if (physicalAssetProperty != null && physicalAssetProperty.eIsProxy()) {
			InternalEObject oldPhysicalAssetProperty = (InternalEObject) physicalAssetProperty;
			physicalAssetProperty = (PhysicalAssetProperty) eResolveProxy(oldPhysicalAssetProperty);
			if (physicalAssetProperty != oldPhysicalAssetProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY,
							oldPhysicalAssetProperty, physicalAssetProperty));
			}
		}
		return physicalAssetProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetProperty basicGetPhysicalAssetProperty() {
		return physicalAssetProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetProperty(PhysicalAssetProperty newPhysicalAssetProperty) {
		PhysicalAssetProperty oldPhysicalAssetProperty = physicalAssetProperty;
		physicalAssetProperty = newPhysicalAssetProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY,
					oldPhysicalAssetProperty, physicalAssetProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY:
			if (resolve)
				return getPhysicalAssetClassProperty();
			return basicGetPhysicalAssetClassProperty();
		case Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY:
			if (resolve)
				return getPhysicalAssetProperty();
			return basicGetPhysicalAssetProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY:
			setPhysicalAssetClassProperty((PhysicalAssetClassProperty) newValue);
			return;
		case Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY:
			setPhysicalAssetProperty((PhysicalAssetProperty) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY:
			setPhysicalAssetClassProperty((PhysicalAssetClassProperty) null);
			return;
		case Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY:
			setPhysicalAssetProperty((PhysicalAssetProperty) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY:
			return physicalAssetClassProperty != null;
		case Iec62264Package.REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY:
			return physicalAssetProperty != null;
		}
		return super.eIsSet(featureID);
	}

} //ReferentialPhysicalAssetPropertyImpl
