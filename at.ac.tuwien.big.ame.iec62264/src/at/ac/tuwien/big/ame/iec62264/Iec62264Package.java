/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Factory
 * @model kind="package"
 * @generated
 */
public interface Iec62264Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "iec62264";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.big.tuwien.ac.at/iec62264";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "iec62264";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Iec62264Package eINSTANCE = at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl.init();

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonImpl <em>Person</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPerson()
	 * @generated
	 */
	int PERSON = 0;

	/**
	 * The feature id for the '<em><b>Personnel Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__PERSONNEL_CLASSES = 0;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__PROPERTIES = 1;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__TEST_SPECIFICATIONS = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__ID = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__NAME = 5;

	/**
	 * The number of structural features of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassImpl <em>Personnel Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelClass()
	 * @generated
	 */
	int PERSONNEL_CLASS = 1;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS__PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS__TEST_SPECIFICATIONS = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS__DESCRIPTION = 3;

	/**
	 * The number of structural features of the '<em>Personnel Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Personnel Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.QualificationTestSpecificationImpl <em>Qualification Test Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.QualificationTestSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getQualificationTestSpecification()
	 * @generated
	 */
	int QUALIFICATION_TEST_SPECIFICATION = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_SPECIFICATION__ID = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_SPECIFICATION__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_SPECIFICATION__VERSION = 2;

	/**
	 * The number of structural features of the '<em>Qualification Test Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_SPECIFICATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Qualification Test Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassPropertyImpl <em>Personnel Class Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelClassProperty()
	 * @generated
	 */
	int PERSONNEL_CLASS_PROPERTY = 3;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS_PROPERTY__TEST_SPECIFICATIONS = 0;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS_PROPERTY__SUB_PROPERTIES = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS_PROPERTY__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS_PROPERTY__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS_PROPERTY__VALUE = 4;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE = 5;

	/**
	 * The number of structural features of the '<em>Personnel Class Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS_PROPERTY_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Personnel Class Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CLASS_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonPropertyImpl <em>Person Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonProperty()
	 * @generated
	 */
	int PERSON_PROPERTY = 4;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_PROPERTY__SUB_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Personnel Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_PROPERTY__PERSONNEL_CLASS_PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_PROPERTY__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_PROPERTY__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_PROPERTY__VALUE = 4;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_PROPERTY__VALUE_UNIT_OF_MEASURE = 5;

	/**
	 * The number of structural features of the '<em>Person Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_PROPERTY_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Person Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.QualificationTestResultImpl <em>Qualification Test Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.QualificationTestResultImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getQualificationTestResult()
	 * @generated
	 */
	int QUALIFICATION_TEST_RESULT = 5;

	/**
	 * The feature id for the '<em><b>Test Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_RESULT__TEST_SPECIFICATION = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_RESULT__PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_RESULT__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_RESULT__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_RESULT__DATE = 4;

	/**
	 * The feature id for the '<em><b>Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_RESULT__RESULT = 5;

	/**
	 * The feature id for the '<em><b>Result Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_RESULT__RESULT_UNIT_OF_MEASURE = 6;

	/**
	 * The feature id for the '<em><b>Expiration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_RESULT__EXPIRATION = 7;

	/**
	 * The number of structural features of the '<em>Qualification Test Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_RESULT_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Qualification Test Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFICATION_TEST_RESULT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelModelImpl <em>Personnel Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelModelImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelModel()
	 * @generated
	 */
	int PERSONNEL_MODEL = 6;

	/**
	 * The feature id for the '<em><b>Personnel Classes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_MODEL__PERSONNEL_CLASSES = 0;

	/**
	 * The feature id for the '<em><b>Persons</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_MODEL__PERSONS = 1;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_MODEL__TEST_SPECIFICATIONS = 2;

	/**
	 * The feature id for the '<em><b>Test Results</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_MODEL__TEST_RESULTS = 3;

	/**
	 * The feature id for the '<em><b>Operations Capability Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_MODEL__OPERATIONS_CAPABILITY_MODEL = 4;

	/**
	 * The number of structural features of the '<em>Personnel Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_MODEL_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Personnel Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentModelImpl <em>Equipment Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentModelImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentModel()
	 * @generated
	 */
	int EQUIPMENT_MODEL = 7;

	/**
	 * The feature id for the '<em><b>Equipment Classes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_MODEL__EQUIPMENT_CLASSES = 0;

	/**
	 * The feature id for the '<em><b>Equipments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_MODEL__EQUIPMENTS = 1;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_MODEL__TEST_SPECIFICATIONS = 2;

	/**
	 * The feature id for the '<em><b>Test Results</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_MODEL__TEST_RESULTS = 3;

	/**
	 * The number of structural features of the '<em>Equipment Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_MODEL_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Equipment Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentClassImpl <em>Equipment Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentClassImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentClass()
	 * @generated
	 */
	int EQUIPMENT_CLASS = 8;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS__PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS__TEST_SPECIFICATIONS = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Equipment Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS__EQUIPMENT_LEVEL = 4;

	/**
	 * The number of structural features of the '<em>Equipment Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Equipment Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityTestResultImpl <em>Equipment Capability Test Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityTestResultImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentCapabilityTestResult()
	 * @generated
	 */
	int EQUIPMENT_CAPABILITY_TEST_RESULT = 9;

	/**
	 * The feature id for the '<em><b>Test Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_RESULT__TEST_SPECIFICATION = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_RESULT__PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_RESULT__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_RESULT__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_RESULT__DATE = 4;

	/**
	 * The feature id for the '<em><b>Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_RESULT__RESULT = 5;

	/**
	 * The feature id for the '<em><b>Result Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_RESULT__RESULT_UNIT_OF_MEASURE = 6;

	/**
	 * The feature id for the '<em><b>Expiration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_RESULT__EXPIRATION = 7;

	/**
	 * The number of structural features of the '<em>Equipment Capability Test Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_RESULT_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Equipment Capability Test Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_RESULT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityTestSpecificationImpl <em>Equipment Capability Test Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityTestSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentCapabilityTestSpecification()
	 * @generated
	 */
	int EQUIPMENT_CAPABILITY_TEST_SPECIFICATION = 10;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_SPECIFICATION__ID = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_SPECIFICATION__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_SPECIFICATION__VERSION = 2;

	/**
	 * The number of structural features of the '<em>Equipment Capability Test Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Equipment Capability Test Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentClassPropertyImpl <em>Equipment Class Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentClassPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentClassProperty()
	 * @generated
	 */
	int EQUIPMENT_CLASS_PROPERTY = 11;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS_PROPERTY__SUB_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS_PROPERTY__TEST_SPECIFICATIONS = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS_PROPERTY__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS_PROPERTY__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS_PROPERTY__VALUE = 4;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE = 5;

	/**
	 * The number of structural features of the '<em>Equipment Class Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS_PROPERTY_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Equipment Class Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CLASS_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentPropertyImpl <em>Equipment Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentProperty()
	 * @generated
	 */
	int EQUIPMENT_PROPERTY = 12;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_PROPERTY__SUB_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Equipment Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_PROPERTY__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_PROPERTY__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_PROPERTY__VALUE = 4;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_PROPERTY__VALUE_UNIT_OF_MEASURE = 5;

	/**
	 * The number of structural features of the '<em>Equipment Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_PROPERTY_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Equipment Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentImpl <em>Equipment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipment()
	 * @generated
	 */
	int EQUIPMENT = 13;

	/**
	 * The feature id for the '<em><b>Children</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT__CHILDREN = 0;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT__PROPERTIES = 1;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT__TEST_SPECIFICATIONS = 2;

	/**
	 * The feature id for the '<em><b>Equipment Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT__EQUIPMENT_CLASS = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT__ID = 4;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT__DESCRIPTION = 5;

	/**
	 * The feature id for the '<em><b>Equipment Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT__EQUIPMENT_LEVEL = 6;

	/**
	 * The number of structural features of the '<em>Equipment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Equipment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetModelImpl <em>Physical Asset Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetModelImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetModel()
	 * @generated
	 */
	int PHYSICAL_ASSET_MODEL = 14;

	/**
	 * The feature id for the '<em><b>Physical Assets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_MODEL__PHYSICAL_ASSETS = 0;

	/**
	 * The feature id for the '<em><b>Physical Asset Classes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_MODEL__PHYSICAL_ASSET_CLASSES = 1;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_MODEL__TEST_SPECIFICATIONS = 2;

	/**
	 * The feature id for the '<em><b>Equipment Asset Mappings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_MODEL__EQUIPMENT_ASSET_MAPPINGS = 3;

	/**
	 * The feature id for the '<em><b>Test Results</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_MODEL__TEST_RESULTS = 4;

	/**
	 * The number of structural features of the '<em>Physical Asset Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_MODEL_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Physical Asset Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetClassImpl <em>Physical Asset Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetClassImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetClass()
	 * @generated
	 */
	int PHYSICAL_ASSET_CLASS = 15;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS__PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS__TEST_SPECIFICATIONS = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS__ID = 2;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS__MANUFACTURER = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS__DESCRIPTION = 4;

	/**
	 * The number of structural features of the '<em>Physical Asset Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Physical Asset Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestResultImpl <em>Physical Asset Capability Test Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestResultImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetCapabilityTestResult()
	 * @generated
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_RESULT = 16;

	/**
	 * The feature id for the '<em><b>Test Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__TEST_SPECIFICATION = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DATE = 4;

	/**
	 * The feature id for the '<em><b>Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT = 5;

	/**
	 * The feature id for the '<em><b>Result Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT_UNIT_OF_MEASURE = 6;

	/**
	 * The feature id for the '<em><b>Expiration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__EXPIRATION = 7;

	/**
	 * The number of structural features of the '<em>Physical Asset Capability Test Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_RESULT_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Physical Asset Capability Test Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_RESULT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetPropertyImpl <em>Physical Asset Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetProperty()
	 * @generated
	 */
	int PHYSICAL_ASSET_PROPERTY = 17;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_PROPERTY__SUB_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_PROPERTY__ID = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_PROPERTY__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_PROPERTY__VALUE = 3;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_PROPERTY__VALUE_UNIT_OF_MEASURE = 4;

	/**
	 * The feature id for the '<em><b>Physical Asset Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY = 5;

	/**
	 * The number of structural features of the '<em>Physical Asset Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_PROPERTY_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Physical Asset Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl <em>Physical Asset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAsset()
	 * @generated
	 */
	int PHYSICAL_ASSET = 18;

	/**
	 * The feature id for the '<em><b>Children</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET__CHILDREN = 0;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET__PROPERTIES = 1;

	/**
	 * The feature id for the '<em><b>Physical Asset Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET__PHYSICAL_ASSET_CLASS = 2;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET__TEST_SPECIFICATIONS = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET__ID = 4;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET__DESCRIPTION = 5;

	/**
	 * The feature id for the '<em><b>Physical Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET__PHYSICAL_LOCATION = 6;

	/**
	 * The feature id for the '<em><b>Fixed Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET__FIXED_ASSET_ID = 7;

	/**
	 * The feature id for the '<em><b>Vendor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET__VENDOR_ID = 8;

	/**
	 * The number of structural features of the '<em>Physical Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Physical Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestSpecificationImpl <em>Physical Asset Capability Test Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetCapabilityTestSpecification()
	 * @generated
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION = 19;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION__ID = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION__VERSION = 2;

	/**
	 * The number of structural features of the '<em>Physical Asset Capability Test Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Physical Asset Capability Test Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetClassPropertyImpl <em>Physical Asset Class Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetClassPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetClassProperty()
	 * @generated
	 */
	int PHYSICAL_ASSET_CLASS_PROPERTY = 20;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS_PROPERTY__SUB_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS_PROPERTY__TEST_SPECIFICATIONS = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS_PROPERTY__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS_PROPERTY__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS_PROPERTY__VALUE = 4;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE = 5;

	/**
	 * The number of structural features of the '<em>Physical Asset Class Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS_PROPERTY_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Physical Asset Class Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CLASS_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentAssetMappingImpl <em>Equipment Asset Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentAssetMappingImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentAssetMapping()
	 * @generated
	 */
	int EQUIPMENT_ASSET_MAPPING = 21;

	/**
	 * The feature id for the '<em><b>Physical Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ASSET_MAPPING__PHYSICAL_ASSET = 0;

	/**
	 * The feature id for the '<em><b>Equipment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ASSET_MAPPING__EQUIPMENT = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ASSET_MAPPING__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ASSET_MAPPING__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ASSET_MAPPING__START_TIME = 4;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ASSET_MAPPING__END_TIME = 5;

	/**
	 * The number of structural features of the '<em>Equipment Asset Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ASSET_MAPPING_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Equipment Asset Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ASSET_MAPPING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialModelImpl <em>Material Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialModelImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialModel()
	 * @generated
	 */
	int MATERIAL_MODEL = 22;

	/**
	 * The feature id for the '<em><b>Material Classes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_MODEL__MATERIAL_CLASSES = 0;

	/**
	 * The feature id for the '<em><b>Material Definitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_MODEL__MATERIAL_DEFINITIONS = 1;

	/**
	 * The feature id for the '<em><b>Material Lots</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_MODEL__MATERIAL_LOTS = 2;

	/**
	 * The feature id for the '<em><b>Material Sublots</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_MODEL__MATERIAL_SUBLOTS = 3;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_MODEL__TEST_SPECIFICATIONS = 4;

	/**
	 * The feature id for the '<em><b>Test Results</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_MODEL__TEST_RESULTS = 5;

	/**
	 * The number of structural features of the '<em>Material Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_MODEL_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Material Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialClassImpl <em>Material Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialClassImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialClass()
	 * @generated
	 */
	int MATERIAL_CLASS = 23;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS__PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Assembled From Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS__ASSEMBLED_FROM_CLASSES = 1;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS__TEST_SPECIFICATIONS = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS__ID = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS__DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Assembly Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS__ASSEMBLY_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Assembly Relationship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS__ASSEMBLY_RELATIONSHIP = 6;

	/**
	 * The number of structural features of the '<em>Material Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Material Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialTestResultImpl <em>Material Test Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialTestResultImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialTestResult()
	 * @generated
	 */
	int MATERIAL_TEST_RESULT = 24;

	/**
	 * The feature id for the '<em><b>Test Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_RESULT__TEST_SPECIFICATION = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_RESULT__PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_RESULT__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_RESULT__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_RESULT__DATE = 4;

	/**
	 * The feature id for the '<em><b>Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_RESULT__RESULT = 5;

	/**
	 * The feature id for the '<em><b>Result Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_RESULT__RESULT_UNIT_OF_MEASURE = 6;

	/**
	 * The feature id for the '<em><b>Expiration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_RESULT__EXPIRATION = 7;

	/**
	 * The number of structural features of the '<em>Material Test Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_RESULT_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Material Test Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_RESULT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialTestSpecificationImpl <em>Material Test Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialTestSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialTestSpecification()
	 * @generated
	 */
	int MATERIAL_TEST_SPECIFICATION = 25;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_SPECIFICATION__ID = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_SPECIFICATION__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_SPECIFICATION__VERSION = 2;

	/**
	 * The number of structural features of the '<em>Material Test Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_SPECIFICATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Material Test Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_TEST_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl <em>Material Sublot</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialSublot()
	 * @generated
	 */
	int MATERIAL_SUBLOT = 26;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT__PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Assembled From Lots</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT__ASSEMBLED_FROM_LOTS = 1;

	/**
	 * The feature id for the '<em><b>Assembled From Sublots</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT__ASSEMBLED_FROM_SUBLOTS = 2;

	/**
	 * The feature id for the '<em><b>Sublots</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT__SUBLOTS = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT__ID = 4;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT__DESCRIPTION = 5;

	/**
	 * The feature id for the '<em><b>Assembly Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT__ASSEMBLY_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Assembly Relationship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT__ASSEMBLY_RELATIONSHIP = 7;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT__STATUS = 8;

	/**
	 * The feature id for the '<em><b>Storage Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT__STORAGE_LOCATION = 9;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT__QUANTITY = 10;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT__QUANTITY_UNIT_OF_MEASURE = 11;

	/**
	 * The number of structural features of the '<em>Material Sublot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT_FEATURE_COUNT = 12;

	/**
	 * The number of operations of the '<em>Material Sublot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SUBLOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialLotImpl <em>Material Lot</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialLotImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialLot()
	 * @generated
	 */
	int MATERIAL_LOT = 27;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Assembled From Lots</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__ASSEMBLED_FROM_LOTS = 1;

	/**
	 * The feature id for the '<em><b>Assembled From Sublots</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__ASSEMBLED_FROM_SUBLOTS = 2;

	/**
	 * The feature id for the '<em><b>Sublots</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__SUBLOTS = 3;

	/**
	 * The feature id for the '<em><b>Material Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__MATERIAL_DEFINITION = 4;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__TEST_SPECIFICATIONS = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__ID = 6;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__DESCRIPTION = 7;

	/**
	 * The feature id for the '<em><b>Assembly Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__ASSEMBLY_TYPE = 8;

	/**
	 * The feature id for the '<em><b>Assembly Relationship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__ASSEMBLY_RELATIONSHIP = 9;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__STATUS = 10;

	/**
	 * The feature id for the '<em><b>Storage Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__STORAGE_LOCATION = 11;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__QUANTITY = 12;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT__QUANTITY_UNIT_OF_MEASURE = 13;

	/**
	 * The number of structural features of the '<em>Material Lot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT_FEATURE_COUNT = 14;

	/**
	 * The number of operations of the '<em>Material Lot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialLotPropertyImpl <em>Material Lot Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialLotPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialLotProperty()
	 * @generated
	 */
	int MATERIAL_LOT_PROPERTY = 28;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT_PROPERTY__SUB_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Material Definition Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT_PROPERTY__MATERIAL_DEFINITION_PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT_PROPERTY__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT_PROPERTY__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT_PROPERTY__VALUE = 4;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT_PROPERTY__VALUE_UNIT_OF_MEASURE = 5;

	/**
	 * The number of structural features of the '<em>Material Lot Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT_PROPERTY_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Material Lot Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_LOT_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialDefinitionImpl <em>Material Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialDefinitionImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialDefinition()
	 * @generated
	 */
	int MATERIAL_DEFINITION = 29;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION__PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Assembled From Definitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION__ASSEMBLED_FROM_DEFINITIONS = 1;

	/**
	 * The feature id for the '<em><b>Material Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION__MATERIAL_CLASSES = 2;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION__TEST_SPECIFICATIONS = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION__ID = 4;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION__DESCRIPTION = 5;

	/**
	 * The feature id for the '<em><b>Assembly Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION__ASSEMBLY_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Assembly Relationship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION__ASSEMBLY_RELATIONSHIP = 7;

	/**
	 * The number of structural features of the '<em>Material Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Material Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialDefinitionPropertyImpl <em>Material Definition Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialDefinitionPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialDefinitionProperty()
	 * @generated
	 */
	int MATERIAL_DEFINITION_PROPERTY = 30;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION_PROPERTY__SUB_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Material Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION_PROPERTY__MATERIAL_CLASS_PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION_PROPERTY__TEST_SPECIFICATIONS = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION_PROPERTY__ID = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION_PROPERTY__DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION_PROPERTY__VALUE = 5;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION_PROPERTY__VALUE_UNIT_OF_MEASURE = 6;

	/**
	 * The number of structural features of the '<em>Material Definition Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION_PROPERTY_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Material Definition Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DEFINITION_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialClassPropertyImpl <em>Material Class Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialClassPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialClassProperty()
	 * @generated
	 */
	int MATERIAL_CLASS_PROPERTY = 31;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS_PROPERTY__SUB_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Test Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS_PROPERTY__TEST_SPECIFICATIONS = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS_PROPERTY__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS_PROPERTY__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS_PROPERTY__VALUE = 4;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE = 5;

	/**
	 * The number of structural features of the '<em>Material Class Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS_PROPERTY_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Material Class Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CLASS_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentModelImpl <em>Process Segment Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentModelImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getProcessSegmentModel()
	 * @generated
	 */
	int PROCESS_SEGMENT_MODEL = 32;

	/**
	 * The feature id for the '<em><b>Process Segments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_MODEL__PROCESS_SEGMENTS = 0;

	/**
	 * The feature id for the '<em><b>Process Segment Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_MODEL__PROCESS_SEGMENT_DEPENDENCIES = 1;

	/**
	 * The number of structural features of the '<em>Process Segment Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Process Segment Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl <em>Process Segment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getProcessSegment()
	 * @generated
	 */
	int PROCESS_SEGMENT = 33;

	/**
	 * The feature id for the '<em><b>Children</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT__CHILDREN = 0;

	/**
	 * The feature id for the '<em><b>Process Segment Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT__PROCESS_SEGMENT_PARAMETERS = 1;

	/**
	 * The feature id for the '<em><b>Personnel Segment Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT__PERSONNEL_SEGMENT_SPECIFICATIONS = 2;

	/**
	 * The feature id for the '<em><b>Equipment Segment Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT__EQUIPMENT_SEGMENT_SPECIFICATIONS = 3;

	/**
	 * The feature id for the '<em><b>Physical Asset Segment Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT__PHYSICAL_ASSET_SEGMENT_SPECIFICATIONS = 4;

	/**
	 * The feature id for the '<em><b>Material Segment Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT__MATERIAL_SEGMENT_SPECIFICATIONS = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT__ID = 6;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT__DESCRIPTION = 7;

	/**
	 * The feature id for the '<em><b>Operations Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT__OPERATIONS_TYPE = 8;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT__DURATION = 9;

	/**
	 * The feature id for the '<em><b>Duration Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT__DURATION_UNIT_OF_MEASURE = 10;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT__HIERARCHY_SCOPE = 11;

	/**
	 * The number of structural features of the '<em>Process Segment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_FEATURE_COUNT = 12;

	/**
	 * The number of operations of the '<em>Process Segment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ReferencialPropertyImpl <em>Referencial Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ReferencialPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getReferencialProperty()
	 * @generated
	 */
	int REFERENCIAL_PROPERTY = 100;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCIAL_PROPERTY__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCIAL_PROPERTY__VALUE = 1;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCIAL_PROPERTY__VALUE_UNIT_OF_MEASURE = 2;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCIAL_PROPERTY__QUANTITY = 3;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCIAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE = 4;

	/**
	 * The number of structural features of the '<em>Referencial Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCIAL_PROPERTY_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Referencial Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCIAL_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialPersonnelPropertyImpl <em>Referential Personnel Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ReferentialPersonnelPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getReferentialPersonnelProperty()
	 * @generated
	 */
	int REFERENTIAL_PERSONNEL_PROPERTY = 101;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PERSONNEL_PROPERTY__DESCRIPTION = REFERENCIAL_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PERSONNEL_PROPERTY__VALUE = REFERENCIAL_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PERSONNEL_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENCIAL_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PERSONNEL_PROPERTY__QUANTITY = REFERENCIAL_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PERSONNEL_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENCIAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Personnel Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY = REFERENCIAL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Person Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY = REFERENCIAL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Referential Personnel Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PERSONNEL_PROPERTY_FEATURE_COUNT = REFERENCIAL_PROPERTY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Referential Personnel Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PERSONNEL_PROPERTY_OPERATION_COUNT = REFERENCIAL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSegmentSpecificationPropertyImpl <em>Personnel Segment Specification Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelSegmentSpecificationPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelSegmentSpecificationProperty()
	 * @generated
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY = 34;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY__DESCRIPTION = REFERENTIAL_PERSONNEL_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY__VALUE = REFERENTIAL_PERSONNEL_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_PERSONNEL_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY__QUANTITY = REFERENTIAL_PERSONNEL_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_PERSONNEL_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Personnel Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY__PERSONNEL_CLASS_PROPERTY = REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Person Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY__PERSON_PROPERTY = REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES = REFERENTIAL_PERSONNEL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Personnel Segment Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY_FEATURE_COUNT = REFERENTIAL_PERSONNEL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Personnel Segment Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY_OPERATION_COUNT = REFERENTIAL_PERSONNEL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialEquipmentPropertyImpl <em>Referential Equipment Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ReferentialEquipmentPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getReferentialEquipmentProperty()
	 * @generated
	 */
	int REFERENTIAL_EQUIPMENT_PROPERTY = 102;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_EQUIPMENT_PROPERTY__DESCRIPTION = REFERENCIAL_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_EQUIPMENT_PROPERTY__VALUE = REFERENCIAL_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_EQUIPMENT_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENCIAL_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_EQUIPMENT_PROPERTY__QUANTITY = REFERENCIAL_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_EQUIPMENT_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENCIAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Equipment Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY = REFERENCIAL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Equipment Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY = REFERENCIAL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Referential Equipment Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_EQUIPMENT_PROPERTY_FEATURE_COUNT = REFERENCIAL_PROPERTY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Referential Equipment Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_EQUIPMENT_PROPERTY_OPERATION_COUNT = REFERENCIAL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentSegmentSpecificationPropertyImpl <em>Equipment Segment Specification Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentSegmentSpecificationPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentSegmentSpecificationProperty()
	 * @generated
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY = 35;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY__DESCRIPTION = REFERENTIAL_EQUIPMENT_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY__VALUE = REFERENTIAL_EQUIPMENT_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_EQUIPMENT_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY__QUANTITY = REFERENTIAL_EQUIPMENT_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_EQUIPMENT_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Equipment Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY__EQUIPMENT_CLASS_PROPERTY = REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Equipment Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY__EQUIPMENT_PROPERTY = REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES = REFERENTIAL_EQUIPMENT_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Equipment Segment Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY_FEATURE_COUNT = REFERENTIAL_EQUIPMENT_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Equipment Segment Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY_OPERATION_COUNT = REFERENTIAL_EQUIPMENT_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialPhysicalAssetPropertyImpl <em>Referential Physical Asset Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ReferentialPhysicalAssetPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getReferentialPhysicalAssetProperty()
	 * @generated
	 */
	int REFERENTIAL_PHYSICAL_ASSET_PROPERTY = 103;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PHYSICAL_ASSET_PROPERTY__DESCRIPTION = REFERENCIAL_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PHYSICAL_ASSET_PROPERTY__VALUE = REFERENCIAL_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PHYSICAL_ASSET_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENCIAL_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PHYSICAL_ASSET_PROPERTY__QUANTITY = REFERENCIAL_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PHYSICAL_ASSET_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENCIAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Physical Asset Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY = REFERENCIAL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Physical Asset Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY = REFERENCIAL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Referential Physical Asset Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PHYSICAL_ASSET_PROPERTY_FEATURE_COUNT = REFERENCIAL_PROPERTY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Referential Physical Asset Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_PHYSICAL_ASSET_PROPERTY_OPERATION_COUNT = REFERENCIAL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationPropertyImpl <em>Physical Asset Segment Specification Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetSegmentSpecificationProperty()
	 * @generated
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY = 36;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY__DESCRIPTION = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY__VALUE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY__QUANTITY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Physical Asset Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Physical Asset Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY__PHYSICAL_ASSET_PROPERTY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_FEATURE_COUNT
			+ 0;

	/**
	 * The number of structural features of the '<em>Physical Asset Segment Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY_FEATURE_COUNT = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_FEATURE_COUNT
			+ 1;

	/**
	 * The number of operations of the '<em>Physical Asset Segment Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY_OPERATION_COUNT = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialMaterialTypePropertyImpl <em>Referential Material Type Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ReferentialMaterialTypePropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getReferentialMaterialTypeProperty()
	 * @generated
	 */
	int REFERENTIAL_MATERIAL_TYPE_PROPERTY = 104;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_TYPE_PROPERTY__DESCRIPTION = REFERENCIAL_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_TYPE_PROPERTY__VALUE = REFERENCIAL_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_TYPE_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENCIAL_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_TYPE_PROPERTY__QUANTITY = REFERENCIAL_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_TYPE_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENCIAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Material Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_CLASS_PROPERTY = REFERENCIAL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Material Definition Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_DEFINITION_PROPERTY = REFERENCIAL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Referential Material Type Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_TYPE_PROPERTY_FEATURE_COUNT = REFERENCIAL_PROPERTY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Referential Material Type Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_TYPE_PROPERTY_OPERATION_COUNT = REFERENCIAL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSegmentSpecificationPropertyImpl <em>Material Segment Specification Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialSegmentSpecificationPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialSegmentSpecificationProperty()
	 * @generated
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_PROPERTY = 37;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_PROPERTY__DESCRIPTION = REFERENTIAL_MATERIAL_TYPE_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_PROPERTY__VALUE = REFERENTIAL_MATERIAL_TYPE_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_MATERIAL_TYPE_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_PROPERTY__QUANTITY = REFERENTIAL_MATERIAL_TYPE_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_MATERIAL_TYPE_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Material Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_PROPERTY__MATERIAL_CLASS_PROPERTY = REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Material Definition Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_PROPERTY__MATERIAL_DEFINITION_PROPERTY = REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_DEFINITION_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES = REFERENTIAL_MATERIAL_TYPE_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Material Segment Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_PROPERTY_FEATURE_COUNT = REFERENTIAL_MATERIAL_TYPE_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Material Segment Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_PROPERTY_OPERATION_COUNT = REFERENTIAL_MATERIAL_TYPE_PROPERTY_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSegmentSpecificationImpl <em>Material Segment Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialSegmentSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialSegmentSpecification()
	 * @generated
	 */
	int MATERIAL_SEGMENT_SPECIFICATION = 38;

	/**
	 * The feature id for the '<em><b>Assembled From Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION__ASSEMBLED_FROM_SPECIFICATIONS = 0;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION__PROPERTIES = 1;

	/**
	 * The feature id for the '<em><b>Material Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION__MATERIAL_CLASSES = 2;

	/**
	 * The feature id for the '<em><b>Material Definitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION__MATERIAL_DEFINITIONS = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION__DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Assembly Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION__ASSEMBLY_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Assembly Relationship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION__ASSEMBLY_RELATIONSHIP = 6;

	/**
	 * The feature id for the '<em><b>Material Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION__MATERIAL_USE = 7;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION__QUANTITY = 8;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measurement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASUREMENT = 9;

	/**
	 * The number of structural features of the '<em>Material Segment Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_FEATURE_COUNT = 10;

	/**
	 * The number of operations of the '<em>Material Segment Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SEGMENT_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationImpl <em>Physical Asset Segment Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetSegmentSpecification()
	 * @generated
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION = 39;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Physical Asset Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_CLASSES = 1;

	/**
	 * The feature id for the '<em><b>Physical Assets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSETS = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Physical Asset Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_USE = 4;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY = 5;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE = 6;

	/**
	 * The number of structural features of the '<em>Physical Asset Segment Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Physical Asset Segment Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SEGMENT_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentSegmentSpecificationImpl <em>Equipment Segment Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentSegmentSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentSegmentSpecification()
	 * @generated
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION = 40;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION__PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Equipment Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION__EQUIPMENT_CLASSES = 1;

	/**
	 * The feature id for the '<em><b>Equipments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION__EQUIPMENTS = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Equipment Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION__EQUIPMENT_USE = 4;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION__QUANTITY = 5;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measurement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASUREMENT = 6;

	/**
	 * The number of structural features of the '<em>Equipment Segment Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Equipment Segment Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SEGMENT_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSegmentSpecificationImpl <em>Personnel Segment Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelSegmentSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelSegmentSpecification()
	 * @generated
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION = 41;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION__PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Personnel Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION__PERSONNEL_CLASSES = 1;

	/**
	 * The feature id for the '<em><b>Persons</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION__PERSONS = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Personnel Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION__PERSONNEL_USE = 4;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION__QUANTITY = 5;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE = 6;

	/**
	 * The number of structural features of the '<em>Personnel Segment Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Personnel Segment Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SEGMENT_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentParameterImpl <em>Process Segment Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentParameterImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getProcessSegmentParameter()
	 * @generated
	 */
	int PROCESS_SEGMENT_PARAMETER = 42;

	/**
	 * The feature id for the '<em><b>Sub Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_PARAMETER__SUB_PARAMETERS = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_PARAMETER__ID = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_PARAMETER__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_PARAMETER__VALUE = 3;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_PARAMETER__VALUE_UNIT_OF_MEASURE = 4;

	/**
	 * The number of structural features of the '<em>Process Segment Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_PARAMETER_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Process Segment Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_PARAMETER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentDependencyImpl <em>Process Segment Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentDependencyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getProcessSegmentDependency()
	 * @generated
	 */
	int PROCESS_SEGMENT_DEPENDENCY = 43;

	/**
	 * The feature id for the '<em><b>Subject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_DEPENDENCY__SUBJECT = 0;

	/**
	 * The feature id for the '<em><b>Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_DEPENDENCY__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_DEPENDENCY__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Dependency Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Dependency Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR = 5;

	/**
	 * The feature id for the '<em><b>Factor Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_DEPENDENCY__FACTOR_UNIT_OF_MEASURE = 6;

	/**
	 * The number of structural features of the '<em>Process Segment Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_DEPENDENCY_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Process Segment Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_DEPENDENCY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getIec62264Model()
	 * @generated
	 */
	int IEC62264_MODEL = 44;

	/**
	 * The feature id for the '<em><b>Personnel Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL__PERSONNEL_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Equipment Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL__EQUIPMENT_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Physical Asset Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL__PHYSICAL_ASSET_MODEL = 2;

	/**
	 * The feature id for the '<em><b>Material Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL__MATERIAL_MODEL = 3;

	/**
	 * The feature id for the '<em><b>Process Segment Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL__PROCESS_SEGMENT_MODEL = 4;

	/**
	 * The feature id for the '<em><b>Operations Definition Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL__OPERATIONS_DEFINITION_MODEL = 5;

	/**
	 * The feature id for the '<em><b>Operations Schedule Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL__OPERATIONS_SCHEDULE_MODEL = 6;

	/**
	 * The feature id for the '<em><b>Operations Performance Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL__OPERATIONS_PERFORMANCE_MODEL = 7;

	/**
	 * The feature id for the '<em><b>Process Segment Capability Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL__PROCESS_SEGMENT_CAPABILITY_MODEL = 8;

	/**
	 * The feature id for the '<em><b>Hierarchy Scopes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL__HIERARCHY_SCOPES = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL__NAME = 10;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEC62264_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionModelImpl <em>Operations Definition Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionModelImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsDefinitionModel()
	 * @generated
	 */
	int OPERATIONS_DEFINITION_MODEL = 45;

	/**
	 * The feature id for the '<em><b>Operations Definitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION_MODEL__OPERATIONS_DEFINITIONS = 0;

	/**
	 * The feature id for the '<em><b>Operations Segments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENTS = 1;

	/**
	 * The feature id for the '<em><b>Operations Segment Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENT_DEPENDENCIES = 2;

	/**
	 * The number of structural features of the '<em>Operations Definition Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION_MODEL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Operations Definition Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsScheduleModelImpl <em>Operations Schedule Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsScheduleModelImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsScheduleModel()
	 * @generated
	 */
	int OPERATIONS_SCHEDULE_MODEL = 46;

	/**
	 * The feature id for the '<em><b>Operations Schedules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE_MODEL__OPERATIONS_SCHEDULES = 0;

	/**
	 * The number of structural features of the '<em>Operations Schedule Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE_MODEL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Operations Schedule Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceModelImpl <em>Operations Performance Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceModelImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsPerformanceModel()
	 * @generated
	 */
	int OPERATIONS_PERFORMANCE_MODEL = 47;

	/**
	 * The feature id for the '<em><b>Operations Performances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE_MODEL__OPERATIONS_PERFORMANCES = 0;

	/**
	 * The number of structural features of the '<em>Operations Performance Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE_MODEL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Operations Performance Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityModelImpl <em>Operations Capability Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityModelImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsCapabilityModel()
	 * @generated
	 */
	int OPERATIONS_CAPABILITY_MODEL = 48;

	/**
	 * The feature id for the '<em><b>Operations Capabilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY_MODEL__OPERATIONS_CAPABILITIES = 0;

	/**
	 * The number of structural features of the '<em>Operations Capability Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY_MODEL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Operations Capability Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentCapabilityModelImpl <em>Process Segment Capability Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentCapabilityModelImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getProcessSegmentCapabilityModel()
	 * @generated
	 */
	int PROCESS_SEGMENT_CAPABILITY_MODEL = 49;

	/**
	 * The feature id for the '<em><b>Process Segment Capabilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY_MODEL__PROCESS_SEGMENT_CAPABILITIES = 0;

	/**
	 * The number of structural features of the '<em>Process Segment Capability Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY_MODEL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Process Segment Capability Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.HierarchyScopeImpl <em>Hierarchy Scope</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.HierarchyScopeImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getHierarchyScope()
	 * @generated
	 */
	int HIERARCHY_SCOPE = 50;

	/**
	 * The feature id for the '<em><b>Sub Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHY_SCOPE__SUB_SCOPE = 0;

	/**
	 * The feature id for the '<em><b>Equipment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHY_SCOPE__EQUIPMENT = 1;

	/**
	 * The feature id for the '<em><b>Equipment Element Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHY_SCOPE__EQUIPMENT_ELEMENT_LEVEL = 2;

	/**
	 * The number of structural features of the '<em>Hierarchy Scope</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHY_SCOPE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Hierarchy Scope</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHY_SCOPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationPropertyImpl <em>Material Specification Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialSpecificationProperty()
	 * @generated
	 */
	int MATERIAL_SPECIFICATION_PROPERTY = 51;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION_PROPERTY__DESCRIPTION = REFERENTIAL_MATERIAL_TYPE_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION_PROPERTY__VALUE = REFERENTIAL_MATERIAL_TYPE_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_MATERIAL_TYPE_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION_PROPERTY__QUANTITY = REFERENTIAL_MATERIAL_TYPE_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_MATERIAL_TYPE_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Material Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION_PROPERTY__MATERIAL_CLASS_PROPERTY = REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Material Definition Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION_PROPERTY__MATERIAL_DEFINITION_PROPERTY = REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_DEFINITION_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION_PROPERTY__SUB_PROPERTIES = REFERENTIAL_MATERIAL_TYPE_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Material Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION_PROPERTY_FEATURE_COUNT = REFERENTIAL_MATERIAL_TYPE_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Material Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION_PROPERTY_OPERATION_COUNT = REFERENTIAL_MATERIAL_TYPE_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationPropertyImpl <em>Physical Asset Specification Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetSpecificationProperty()
	 * @generated
	 */
	int PHYSICAL_ASSET_SPECIFICATION_PROPERTY = 52;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION_PROPERTY__DESCRIPTION = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION_PROPERTY__VALUE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION_PROPERTY__QUANTITY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Physical Asset Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Physical Asset Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION_PROPERTY__PHYSICAL_ASSET_PROPERTY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION_PROPERTY__SUB_PROPERTIES = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Physical Asset Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION_PROPERTY_FEATURE_COUNT = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Physical Asset Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION_PROPERTY_OPERATION_COUNT = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentSpecificationPropertyImpl <em>Equipment Specification Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentSpecificationPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentSpecificationProperty()
	 * @generated
	 */
	int EQUIPMENT_SPECIFICATION_PROPERTY = 53;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION_PROPERTY__DESCRIPTION = REFERENTIAL_EQUIPMENT_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION_PROPERTY__VALUE = REFERENTIAL_EQUIPMENT_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_EQUIPMENT_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION_PROPERTY__QUANTITY = REFERENTIAL_EQUIPMENT_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_EQUIPMENT_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Equipment Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION_PROPERTY__EQUIPMENT_CLASS_PROPERTY = REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Equipment Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION_PROPERTY__EQUIPMENT_PROPERTY = REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES = REFERENTIAL_EQUIPMENT_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Equipment Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION_PROPERTY_FEATURE_COUNT = REFERENTIAL_EQUIPMENT_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Equipment Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION_PROPERTY_OPERATION_COUNT = REFERENTIAL_EQUIPMENT_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationPropertyImpl <em>Personnel Specification Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelSpecificationProperty()
	 * @generated
	 */
	int PERSONNEL_SPECIFICATION_PROPERTY = 54;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION_PROPERTY__DESCRIPTION = REFERENTIAL_PERSONNEL_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION_PROPERTY__VALUE = REFERENTIAL_PERSONNEL_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_PERSONNEL_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION_PROPERTY__QUANTITY = REFERENTIAL_PERSONNEL_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_PERSONNEL_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Personnel Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION_PROPERTY__PERSONNEL_CLASS_PROPERTY = REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Person Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION_PROPERTY__PERSON_PROPERTY = REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION_PROPERTY__SUB_PROPERTIES = REFERENTIAL_PERSONNEL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Personnel Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION_PROPERTY_FEATURE_COUNT = REFERENTIAL_PERSONNEL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Personnel Specification Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION_PROPERTY_OPERATION_COUNT = REFERENTIAL_PERSONNEL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl <em>Material Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialSpecification()
	 * @generated
	 */
	int MATERIAL_SPECIFICATION = 55;

	/**
	 * The feature id for the '<em><b>Assembled From Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION__ASSEMBLED_FROM_SPECIFICATIONS = 0;

	/**
	 * The feature id for the '<em><b>Material Specification Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION__MATERIAL_SPECIFICATION_PROPERTIES = 1;

	/**
	 * The feature id for the '<em><b>Material Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION__MATERIAL_CLASSES = 2;

	/**
	 * The feature id for the '<em><b>Material Definitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION__MATERIAL_DEFINITIONS = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION__DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Material Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION__MATERIAL_USE = 5;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION__QUANTITY = 6;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE = 7;

	/**
	 * The feature id for the '<em><b>Assembly Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION__ASSEMBLY_TYPE = 8;

	/**
	 * The feature id for the '<em><b>Assembly Relationship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION__ASSEMBLY_RELATIONSHIP = 9;

	/**
	 * The number of structural features of the '<em>Material Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION_FEATURE_COUNT = 10;

	/**
	 * The number of operations of the '<em>Material Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationImpl <em>Physical Asset Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetSpecification()
	 * @generated
	 */
	int PHYSICAL_ASSET_SPECIFICATION = 56;

	/**
	 * The feature id for the '<em><b>Physical Asset Specification Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_SPECIFICATION_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Physic Alasset Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION__PHYSIC_ALASSET_CLASSES = 2;

	/**
	 * The feature id for the '<em><b>Physical Assets</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSETS = 3;

	/**
	 * The feature id for the '<em><b>Physical Asset Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_USE = 4;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION__QUANTITY = 5;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE = 6;

	/**
	 * The number of structural features of the '<em>Physical Asset Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Physical Asset Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentSpecificationImpl <em>Equipment Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentSpecification()
	 * @generated
	 */
	int EQUIPMENT_SPECIFICATION = 57;

	/**
	 * The feature id for the '<em><b>Equipment Specification Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION__EQUIPMENT_SPECIFICATION_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Equipment Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION__EQUIPMENT_CLASSES = 1;

	/**
	 * The feature id for the '<em><b>Equipments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION__EQUIPMENTS = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Equipment Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION__EQUIPMENT_USE = 4;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION__QUANTITY = 5;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE = 6;

	/**
	 * The number of structural features of the '<em>Equipment Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Equipment Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationImpl <em>Personnel Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelSpecification()
	 * @generated
	 */
	int PERSONNEL_SPECIFICATION = 58;

	/**
	 * The feature id for the '<em><b>Personnel Specification Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION__PERSONNEL_SPECIFICATION_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Personnel Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION__PERSONNEL_CLASSES = 1;

	/**
	 * The feature id for the '<em><b>Persons</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION__PERSONS = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Personnel Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION__PERSONNEL_USE = 4;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION__QUANTITY = 5;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE = 6;

	/**
	 * The number of structural features of the '<em>Personnel Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Personnel Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ParameterSpecificationImpl <em>Parameter Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ParameterSpecificationImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getParameterSpecification()
	 * @generated
	 */
	int PARAMETER_SPECIFICATION = 59;

	/**
	 * The feature id for the '<em><b>Sub Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SPECIFICATION__SUB_PARAMETERS = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SPECIFICATION__ID = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SPECIFICATION__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SPECIFICATION__VALUE = 3;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measurement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SPECIFICATION__VALUE_UNIT_OF_MEASUREMENT = 4;

	/**
	 * The number of structural features of the '<em>Parameter Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SPECIFICATION_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Parameter Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentDependencyImpl <em>Operations Segment Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentDependencyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsSegmentDependency()
	 * @generated
	 */
	int OPERATIONS_SEGMENT_DEPENDENCY = 60;

	/**
	 * The feature id for the '<em><b>Subject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT_DEPENDENCY__SUBJECT = 0;

	/**
	 * The feature id for the '<em><b>Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT_DEPENDENCY__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT_DEPENDENCY__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Dependency Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Dependency Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR = 5;

	/**
	 * The feature id for the '<em><b>Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT_DEPENDENCY__UNIT_OF_MEASURE = 6;

	/**
	 * The number of structural features of the '<em>Operations Segment Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT_DEPENDENCY_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Operations Segment Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT_DEPENDENCY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl <em>Operations Material Bill Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsMaterialBillItem()
	 * @generated
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM = 61;

	/**
	 * The feature id for the '<em><b>Material Specifications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_SPECIFICATIONS = 0;

	/**
	 * The feature id for the '<em><b>Assembled From Items</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLED_FROM_ITEMS = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM__ID = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Material Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_CLASSES = 4;

	/**
	 * The feature id for the '<em><b>Material Definitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_DEFINITIONS = 5;

	/**
	 * The feature id for the '<em><b>Use Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM__USE_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Assembly Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Assembly Relationship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_RELATIONSHIP = 8;

	/**
	 * The feature id for the '<em><b>Quantities</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM__QUANTITIES = 9;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measures</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM__QUANTITY_UNIT_OF_MEASURES = 10;

	/**
	 * The number of structural features of the '<em>Operations Material Bill Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Operations Material Bill Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_ITEM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillImpl <em>Operations Material Bill</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsMaterialBill()
	 * @generated
	 */
	int OPERATIONS_MATERIAL_BILL = 62;

	/**
	 * The feature id for the '<em><b>Operations Material Bill Items</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL__OPERATIONS_MATERIAL_BILL_ITEMS = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL__ID = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL__DESCRIPTION = 2;

	/**
	 * The number of structural features of the '<em>Operations Material Bill</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Operations Material Bill</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_MATERIAL_BILL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl <em>Operations Segment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsSegment()
	 * @generated
	 */
	int OPERATIONS_SEGMENT = 63;

	/**
	 * The feature id for the '<em><b>Process Segments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__PROCESS_SEGMENTS = 0;

	/**
	 * The feature id for the '<em><b>Sub Segments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__SUB_SEGMENTS = 1;

	/**
	 * The feature id for the '<em><b>Parameter Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__PARAMETER_SPECIFICATIONS = 2;

	/**
	 * The feature id for the '<em><b>Personnel Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__PERSONNEL_SPECIFICATIONS = 3;

	/**
	 * The feature id for the '<em><b>Equipment Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__EQUIPMENT_SPECIFICATIONS = 4;

	/**
	 * The feature id for the '<em><b>Physical Asset Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__PHYSICAL_ASSET_SPECIFICATIONS = 5;

	/**
	 * The feature id for the '<em><b>Material Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__MATERIAL_SPECIFICATIONS = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__ID = 7;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__DESCRIPTION = 8;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__DURATION = 9;

	/**
	 * The feature id for the '<em><b>Duration Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__DURATION_UNIT_OF_MEASURE = 10;

	/**
	 * The feature id for the '<em><b>Operations Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__OPERATIONS_TYPE = 11;

	/**
	 * The feature id for the '<em><b>Work Definition Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT__WORK_DEFINITION_ID = 12;

	/**
	 * The number of structural features of the '<em>Operations Segment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT_FEATURE_COUNT = 13;

	/**
	 * The number of operations of the '<em>Operations Segment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SEGMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl <em>Operations Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsDefinition()
	 * @generated
	 */
	int OPERATIONS_DEFINITION = 64;

	/**
	 * The feature id for the '<em><b>Operations Segments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION__OPERATIONS_SEGMENTS = 0;

	/**
	 * The feature id for the '<em><b>Operations Material Bills</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION__OPERATIONS_MATERIAL_BILLS = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION__ID = 2;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION__VERSION = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION__DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Operations Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION__OPERATIONS_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Bill Of Material Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION__BILL_OF_MATERIAL_ID = 6;

	/**
	 * The feature id for the '<em><b>Work Definition Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION__WORK_DEFINITION_ID = 7;

	/**
	 * The feature id for the '<em><b>Bill Of Resource Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION__BILL_OF_RESOURCE_ID = 8;

	/**
	 * The feature id for the '<em><b>Hierarchys Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION__HIERARCHYS_SCOPE = 9;

	/**
	 * The number of structural features of the '<em>Operations Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION_FEATURE_COUNT = 10;

	/**
	 * The number of operations of the '<em>Operations Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_DEFINITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialRequirementImpl <em>Material Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialRequirementImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialRequirement()
	 * @generated
	 */
	int MATERIAL_REQUIREMENT = 65;

	/**
	 * The feature id for the '<em><b>Material Requirement Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__MATERIAL_REQUIREMENT_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Assembled From Requirements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__ASSEMBLED_FROM_REQUIREMENTS = 1;

	/**
	 * The feature id for the '<em><b>Material Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__MATERIAL_CLASSES = 2;

	/**
	 * The feature id for the '<em><b>Material Definitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__MATERIAL_DEFINITIONS = 3;

	/**
	 * The feature id for the '<em><b>Material Lots</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__MATERIAL_LOTS = 4;

	/**
	 * The feature id for the '<em><b>Material Sublot</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__MATERIAL_SUBLOT = 5;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__DESCRIPTION = 6;

	/**
	 * The feature id for the '<em><b>Material Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__MATERIAL_USE = 7;

	/**
	 * The feature id for the '<em><b>Storage Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__STORAGE_LOCATION = 8;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__QUANTITY = 9;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__QUANTITY_UNIT_OF_MEASURE = 10;

	/**
	 * The feature id for the '<em><b>Assembly Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__ASSEMBLY_TYPE = 11;

	/**
	 * The feature id for the '<em><b>Assembly Relationship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT__ASSEMBLY_RELATIONSHIP = 12;

	/**
	 * The number of structural features of the '<em>Material Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_FEATURE_COUNT = 13;

	/**
	 * The number of operations of the '<em>Material Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialMaterialPropertyImpl <em>Referential Material Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ReferentialMaterialPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getReferentialMaterialProperty()
	 * @generated
	 */
	int REFERENTIAL_MATERIAL_PROPERTY = 105;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_PROPERTY__DESCRIPTION = REFERENTIAL_MATERIAL_TYPE_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_PROPERTY__VALUE = REFERENTIAL_MATERIAL_TYPE_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_MATERIAL_TYPE_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_PROPERTY__QUANTITY = REFERENTIAL_MATERIAL_TYPE_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_MATERIAL_TYPE_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Material Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_CLASS_PROPERTY = REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Material Definition Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_DEFINITION_PROPERTY = REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_DEFINITION_PROPERTY;

	/**
	 * The feature id for the '<em><b>Material Lot Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_LOT_PROPERTY = REFERENTIAL_MATERIAL_TYPE_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Referential Material Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_PROPERTY_FEATURE_COUNT = REFERENTIAL_MATERIAL_TYPE_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Referential Material Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENTIAL_MATERIAL_PROPERTY_OPERATION_COUNT = REFERENTIAL_MATERIAL_TYPE_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialRequirementPropertyImpl <em>Material Requirement Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialRequirementPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialRequirementProperty()
	 * @generated
	 */
	int MATERIAL_REQUIREMENT_PROPERTY = 66;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_PROPERTY__DESCRIPTION = REFERENTIAL_MATERIAL_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_PROPERTY__VALUE = REFERENTIAL_MATERIAL_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_MATERIAL_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_PROPERTY__QUANTITY = REFERENTIAL_MATERIAL_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_MATERIAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Material Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_PROPERTY__MATERIAL_CLASS_PROPERTY = REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Material Definition Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_PROPERTY__MATERIAL_DEFINITION_PROPERTY = REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_DEFINITION_PROPERTY;

	/**
	 * The feature id for the '<em><b>Material Lot Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_PROPERTY__MATERIAL_LOT_PROPERTY = REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_LOT_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_PROPERTY__SUB_PROPERTIES = REFERENTIAL_MATERIAL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Material Requirement Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_PROPERTY_FEATURE_COUNT = REFERENTIAL_MATERIAL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Material Requirement Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_REQUIREMENT_PROPERTY_OPERATION_COUNT = REFERENTIAL_MATERIAL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetRequirementPropertyImpl <em>Physical Asset Requirement Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetRequirementPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetRequirementProperty()
	 * @generated
	 */
	int PHYSICAL_ASSET_REQUIREMENT_PROPERTY = 67;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT_PROPERTY__DESCRIPTION = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT_PROPERTY__VALUE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT_PROPERTY__QUANTITY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Physical Asset Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Physical Asset Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT_PROPERTY__PHYSICAL_ASSET_PROPERTY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT_PROPERTY__SUB_PROPERTIES = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Physical Asset Requirement Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT_PROPERTY_FEATURE_COUNT = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Physical Asset Requirement Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT_PROPERTY_OPERATION_COUNT = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentRequirementPropertyImpl <em>Equipment Requirement Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentRequirementPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentRequirementProperty()
	 * @generated
	 */
	int EQUIPMENT_REQUIREMENT_PROPERTY = 68;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT_PROPERTY__DESCRIPTION = REFERENTIAL_EQUIPMENT_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT_PROPERTY__VALUE = REFERENTIAL_EQUIPMENT_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_EQUIPMENT_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT_PROPERTY__QUANTITY = REFERENTIAL_EQUIPMENT_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_EQUIPMENT_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Equipment Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY = REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Equipment Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT_PROPERTY__EQUIPMENT_PROPERTY = REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT_PROPERTY__SUB_PROPERTIES = REFERENTIAL_EQUIPMENT_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Equipment Requirement Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT_PROPERTY_FEATURE_COUNT = REFERENTIAL_EQUIPMENT_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Equipment Requirement Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT_PROPERTY_OPERATION_COUNT = REFERENTIAL_EQUIPMENT_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelRequirementPropertyImpl <em>Personnel Requirement Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelRequirementPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelRequirementProperty()
	 * @generated
	 */
	int PERSONNEL_REQUIREMENT_PROPERTY = 69;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT_PROPERTY__DESCRIPTION = REFERENTIAL_PERSONNEL_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT_PROPERTY__VALUE = REFERENTIAL_PERSONNEL_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_PERSONNEL_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT_PROPERTY__QUANTITY = REFERENTIAL_PERSONNEL_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_PERSONNEL_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Personnel Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT_PROPERTY__PERSONNEL_CLASS_PROPERTY = REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Person Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT_PROPERTY__PERSON_PROPERTY = REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT_PROPERTY__SUB_PROPERTIES = REFERENTIAL_PERSONNEL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Personnel Requirement Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT_PROPERTY_FEATURE_COUNT = REFERENTIAL_PERSONNEL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Personnel Requirement Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT_PROPERTY_OPERATION_COUNT = REFERENTIAL_PERSONNEL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsScheduleImpl <em>Operations Schedule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsScheduleImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsSchedule()
	 * @generated
	 */
	int OPERATIONS_SCHEDULE = 70;

	/**
	 * The feature id for the '<em><b>Operations Requests</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE__OPERATIONS_REQUESTS = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE__ID = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Operations Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE__OPERATIONS_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE__START_TIME = 4;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE__END_TIME = 5;

	/**
	 * The feature id for the '<em><b>Published Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE__PUBLISHED_DATE = 6;

	/**
	 * The feature id for the '<em><b>Scheduled State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE__SCHEDULED_STATE = 7;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE__HIERARCHY_SCOPE = 8;

	/**
	 * The number of structural features of the '<em>Operations Schedule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Operations Schedule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_SCHEDULE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsRequestImpl <em>Operations Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsRequestImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsRequest()
	 * @generated
	 */
	int OPERATIONS_REQUEST = 71;

	/**
	 * The feature id for the '<em><b>Segment Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST__SEGMENT_REQUIREMENTS = 0;

	/**
	 * The feature id for the '<em><b>Requested Segment Responses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST__REQUESTED_SEGMENT_RESPONSES = 1;

	/**
	 * The feature id for the '<em><b>Operations Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST__OPERATIONS_DEFINITION = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST__ID = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST__DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Operations Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST__OPERATIONS_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST__START_TIME = 6;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST__END_TIME = 7;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST__PRIORITY = 8;

	/**
	 * The feature id for the '<em><b>Request State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST__REQUEST_STATE = 9;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST__HIERARCHY_SCOPE = 10;

	/**
	 * The number of structural features of the '<em>Operations Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Operations Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_REQUEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl <em>Segment Response</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getSegmentResponse()
	 * @generated
	 */
	int SEGMENT_RESPONSE = 87;

	/**
	 * The feature id for the '<em><b>Sub Responses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__SUB_RESPONSES = 0;

	/**
	 * The feature id for the '<em><b>Process Segment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__PROCESS_SEGMENT = 1;

	/**
	 * The feature id for the '<em><b>Operations Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__OPERATIONS_DEFINITION = 2;

	/**
	 * The feature id for the '<em><b>Segment Data</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__SEGMENT_DATA = 3;

	/**
	 * The feature id for the '<em><b>Personnel Actuals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__PERSONNEL_ACTUALS = 4;

	/**
	 * The feature id for the '<em><b>Equipment Actuals</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__EQUIPMENT_ACTUALS = 5;

	/**
	 * The feature id for the '<em><b>Physical Asset Actuals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__PHYSICAL_ASSET_ACTUALS = 6;

	/**
	 * The feature id for the '<em><b>Material Actuals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__MATERIAL_ACTUALS = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__ID = 8;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__DESCRIPTION = 9;

	/**
	 * The feature id for the '<em><b>Operations Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__OPERATIONS_TYPE = 10;

	/**
	 * The feature id for the '<em><b>Actual Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__ACTUAL_START_TIME = 11;

	/**
	 * The feature id for the '<em><b>Actual End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__ACTUAL_END_TIME = 12;

	/**
	 * The feature id for the '<em><b>Segment State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__SEGMENT_STATE = 13;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE__HIERARCHY_SCOPE = 14;

	/**
	 * The number of structural features of the '<em>Segment Response</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE_FEATURE_COUNT = 15;

	/**
	 * The number of operations of the '<em>Segment Response</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_RESPONSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.RequestedSegmentResponseImpl <em>Requested Segment Response</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.RequestedSegmentResponseImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getRequestedSegmentResponse()
	 * @generated
	 */
	int REQUESTED_SEGMENT_RESPONSE = 72;

	/**
	 * The feature id for the '<em><b>Sub Responses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__SUB_RESPONSES = SEGMENT_RESPONSE__SUB_RESPONSES;

	/**
	 * The feature id for the '<em><b>Process Segment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__PROCESS_SEGMENT = SEGMENT_RESPONSE__PROCESS_SEGMENT;

	/**
	 * The feature id for the '<em><b>Operations Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__OPERATIONS_DEFINITION = SEGMENT_RESPONSE__OPERATIONS_DEFINITION;

	/**
	 * The feature id for the '<em><b>Segment Data</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__SEGMENT_DATA = SEGMENT_RESPONSE__SEGMENT_DATA;

	/**
	 * The feature id for the '<em><b>Personnel Actuals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__PERSONNEL_ACTUALS = SEGMENT_RESPONSE__PERSONNEL_ACTUALS;

	/**
	 * The feature id for the '<em><b>Equipment Actuals</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__EQUIPMENT_ACTUALS = SEGMENT_RESPONSE__EQUIPMENT_ACTUALS;

	/**
	 * The feature id for the '<em><b>Physical Asset Actuals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__PHYSICAL_ASSET_ACTUALS = SEGMENT_RESPONSE__PHYSICAL_ASSET_ACTUALS;

	/**
	 * The feature id for the '<em><b>Material Actuals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__MATERIAL_ACTUALS = SEGMENT_RESPONSE__MATERIAL_ACTUALS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__ID = SEGMENT_RESPONSE__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__DESCRIPTION = SEGMENT_RESPONSE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Operations Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__OPERATIONS_TYPE = SEGMENT_RESPONSE__OPERATIONS_TYPE;

	/**
	 * The feature id for the '<em><b>Actual Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__ACTUAL_START_TIME = SEGMENT_RESPONSE__ACTUAL_START_TIME;

	/**
	 * The feature id for the '<em><b>Actual End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__ACTUAL_END_TIME = SEGMENT_RESPONSE__ACTUAL_END_TIME;

	/**
	 * The feature id for the '<em><b>Segment State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__SEGMENT_STATE = SEGMENT_RESPONSE__SEGMENT_STATE;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE__HIERARCHY_SCOPE = SEGMENT_RESPONSE__HIERARCHY_SCOPE;

	/**
	 * The number of structural features of the '<em>Requested Segment Response</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE_FEATURE_COUNT = SEGMENT_RESPONSE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Requested Segment Response</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUESTED_SEGMENT_RESPONSE_OPERATION_COUNT = SEGMENT_RESPONSE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl <em>Segment Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getSegmentRequirement()
	 * @generated
	 */
	int SEGMENT_REQUIREMENT = 73;

	/**
	 * The feature id for the '<em><b>Sub Requirements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__SUB_REQUIREMENTS = 0;

	/**
	 * The feature id for the '<em><b>Process Segment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__PROCESS_SEGMENT = 1;

	/**
	 * The feature id for the '<em><b>Operations Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__OPERATIONS_DEFINITION = 2;

	/**
	 * The feature id for the '<em><b>Segment Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__SEGMENT_PARAMETERS = 3;

	/**
	 * The feature id for the '<em><b>Personnel Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__PERSONNEL_REQUIREMENTS = 4;

	/**
	 * The feature id for the '<em><b>Equipment Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__EQUIPMENT_REQUIREMENTS = 5;

	/**
	 * The feature id for the '<em><b>Physical Asset Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__PHYSICAL_ASSET_REQUIREMENTS = 6;

	/**
	 * The feature id for the '<em><b>Material Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__MATERIAL_REQUIREMENTS = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__ID = 8;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__DESCRIPTION = 9;

	/**
	 * The feature id for the '<em><b>Operations Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__OPERATIONS_TYPE = 10;

	/**
	 * The feature id for the '<em><b>Earliest Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__EARLIEST_START_TIME = 11;

	/**
	 * The feature id for the '<em><b>Latest Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__LATEST_START_TIME = 12;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__DURATION = 13;

	/**
	 * The feature id for the '<em><b>Duration Unit Of Measurement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__DURATION_UNIT_OF_MEASUREMENT = 14;

	/**
	 * The feature id for the '<em><b>Segment State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__SEGMENT_STATE = 15;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT__HIERARCHY_SCOPE = 16;

	/**
	 * The number of structural features of the '<em>Segment Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT_FEATURE_COUNT = 17;

	/**
	 * The number of operations of the '<em>Segment Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_REQUIREMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentRequirementImpl <em>Equipment Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentRequirementImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentRequirement()
	 * @generated
	 */
	int EQUIPMENT_REQUIREMENT = 74;

	/**
	 * The feature id for the '<em><b>Equipment Requirement Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT__EQUIPMENT_REQUIREMENT_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Equipment Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT__EQUIPMENT_CLASSES = 1;

	/**
	 * The feature id for the '<em><b>Equipments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT__EQUIPMENTS = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Equipment Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT__EQUIPMENT_USE = 4;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT__QUANTITY = 5;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT__QUANTITY_UNIT_OF_MEASURE = 6;

	/**
	 * The feature id for the '<em><b>Equipment Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT__EQUIPMENT_LEVEL = 7;

	/**
	 * The number of structural features of the '<em>Equipment Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Equipment Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_REQUIREMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetRequirementImpl <em>Physical Asset Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetRequirementImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetRequirement()
	 * @generated
	 */
	int PHYSICAL_ASSET_REQUIREMENT = 75;

	/**
	 * The feature id for the '<em><b>Physical Asset Requirement Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT__PHYSICAL_ASSET_REQUIREMENT_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Physical Asset Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT__PHYSICAL_ASSET_CLASSES = 1;

	/**
	 * The feature id for the '<em><b>Physical Assets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT__PHYSICAL_ASSETS = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Physical Asset Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT__PHYSICAL_ASSET_USE = 4;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT__QUANTITY = 5;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT__QUANTITY_UNIT_OF_MEASURE = 6;

	/**
	 * The feature id for the '<em><b>Equipment Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT__EQUIPMENT_LEVEL = 7;

	/**
	 * The number of structural features of the '<em>Physical Asset Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Physical Asset Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_REQUIREMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelRequirementImpl <em>Personnel Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelRequirementImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelRequirement()
	 * @generated
	 */
	int PERSONNEL_REQUIREMENT = 76;

	/**
	 * The feature id for the '<em><b>Personnel Requirement Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT__PERSONNEL_REQUIREMENT_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Personnel Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT__PERSONNEL_CLASSES = 1;

	/**
	 * The feature id for the '<em><b>Persons</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT__PERSONS = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Personnel Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT__PERSONNEL_USE = 4;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT__QUANTITY = 5;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT__QUANTITY_UNIT_OF_MEASURE = 6;

	/**
	 * The number of structural features of the '<em>Personnel Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Personnel Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_REQUIREMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentParameterImpl <em>Segment Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.SegmentParameterImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getSegmentParameter()
	 * @generated
	 */
	int SEGMENT_PARAMETER = 77;

	/**
	 * The feature id for the '<em><b>Sub Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_PARAMETER__SUB_PARAMETERS = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_PARAMETER__ID = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_PARAMETER__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_PARAMETER__VALUE = 3;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_PARAMETER__VALUE_UNIT_OF_MEASURE = 4;

	/**
	 * The number of structural features of the '<em>Segment Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_PARAMETER_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Segment Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_PARAMETER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualPropertyImpl <em>Material Actual Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialActualPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialActualProperty()
	 * @generated
	 */
	int MATERIAL_ACTUAL_PROPERTY = 78;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_PROPERTY__DESCRIPTION = REFERENTIAL_MATERIAL_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_PROPERTY__VALUE = REFERENTIAL_MATERIAL_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_MATERIAL_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_PROPERTY__QUANTITY = REFERENTIAL_MATERIAL_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_MATERIAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Material Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_PROPERTY__MATERIAL_CLASS_PROPERTY = REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Material Definition Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_PROPERTY__MATERIAL_DEFINITION_PROPERTY = REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_DEFINITION_PROPERTY;

	/**
	 * The feature id for the '<em><b>Material Lot Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_PROPERTY__MATERIAL_LOT_PROPERTY = REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_LOT_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_PROPERTY__SUB_PROPERTIES = REFERENTIAL_MATERIAL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Material Actual Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_PROPERTY_FEATURE_COUNT = REFERENTIAL_MATERIAL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Material Actual Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_PROPERTY_OPERATION_COUNT = REFERENTIAL_MATERIAL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetActualPropertyImpl <em>Physical Asset Actual Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetActualPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetActualProperty()
	 * @generated
	 */
	int PHYSICAL_ASSET_ACTUAL_PROPERTY = 79;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL_PROPERTY__DESCRIPTION = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL_PROPERTY__VALUE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL_PROPERTY__QUANTITY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Physical Asset Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Physical Asset Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL_PROPERTY__PHYSICAL_ASSET_PROPERTY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL_PROPERTY__SUB_PROPERTIES = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Physical Asset Actual Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL_PROPERTY_FEATURE_COUNT = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Physical Asset Actual Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL_PROPERTY_OPERATION_COUNT = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualPropertyImpl <em>Equipment Actual Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentActualProperty()
	 * @generated
	 */
	int EQUIPMENT_ACTUAL_PROPERTY = 80;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL_PROPERTY__DESCRIPTION = REFERENTIAL_EQUIPMENT_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL_PROPERTY__VALUE = REFERENTIAL_EQUIPMENT_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_EQUIPMENT_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL_PROPERTY__QUANTITY = REFERENTIAL_EQUIPMENT_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_EQUIPMENT_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Equipment Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL_PROPERTY__EQUIPMENT_CLASS_PROPERTY = REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Equipment Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL_PROPERTY__EQUIPMENT_PROPERTY = REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL_PROPERTY__SUB_PROPERTIES = REFERENTIAL_EQUIPMENT_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Equipment Actual Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL_PROPERTY_FEATURE_COUNT = REFERENTIAL_EQUIPMENT_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Equipment Actual Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL_PROPERTY_OPERATION_COUNT = REFERENTIAL_EQUIPMENT_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelActualPropertyImpl <em>Personnel Actual Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelActualPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelActualProperty()
	 * @generated
	 */
	int PERSONNEL_ACTUAL_PROPERTY = 81;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL_PROPERTY__DESCRIPTION = REFERENTIAL_PERSONNEL_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL_PROPERTY__VALUE = REFERENTIAL_PERSONNEL_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_PERSONNEL_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL_PROPERTY__QUANTITY = REFERENTIAL_PERSONNEL_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_PERSONNEL_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Personnel Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL_PROPERTY__PERSONNEL_CLASS_PROPERTY = REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Person Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL_PROPERTY__PERSON_PROPERTY = REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL_PROPERTY__SUB_PROPERTIES = REFERENTIAL_PERSONNEL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Personnel Actual Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL_PROPERTY_FEATURE_COUNT = REFERENTIAL_PERSONNEL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Personnel Actual Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL_PROPERTY_OPERATION_COUNT = REFERENTIAL_PERSONNEL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl <em>Material Actual</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialActual()
	 * @generated
	 */
	int MATERIAL_ACTUAL = 82;

	/**
	 * The feature id for the '<em><b>Assembled From Actuals</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__ASSEMBLED_FROM_ACTUALS = 0;

	/**
	 * The feature id for the '<em><b>Material Actual Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__MATERIAL_ACTUAL_PROPERTIES = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Material Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__MATERIAL_USE = 3;

	/**
	 * The feature id for the '<em><b>Storage Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__STORAGE_LOCATION = 4;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__QUANTITY = 5;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__QUANTITY_UNIT_OF_MEASURE = 6;

	/**
	 * The feature id for the '<em><b>Assembly Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__ASSEMBLY_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Assembly Relationship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__ASSEMBLY_RELATIONSHIP = 8;

	/**
	 * The feature id for the '<em><b>Material Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__MATERIAL_CLASSES = 9;

	/**
	 * The feature id for the '<em><b>Material Definitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__MATERIAL_DEFINITIONS = 10;

	/**
	 * The feature id for the '<em><b>Material Lots</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__MATERIAL_LOTS = 11;

	/**
	 * The feature id for the '<em><b>Material Sublots</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL__MATERIAL_SUBLOTS = 12;

	/**
	 * The number of structural features of the '<em>Material Actual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_FEATURE_COUNT = 13;

	/**
	 * The number of operations of the '<em>Material Actual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_ACTUAL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetActualImpl <em>Physical Asset Actual</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetActualImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetActual()
	 * @generated
	 */
	int PHYSICAL_ASSET_ACTUAL = 83;

	/**
	 * The feature id for the '<em><b>Physical Asset Actual Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL__PHYSICAL_ASSET_ACTUAL_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Physical Asset Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL__PHYSICAL_ASSET_USE = 2;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL__QUANTITY = 3;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL__QUANTITY_UNIT_OF_MEASURE = 4;

	/**
	 * The feature id for the '<em><b>Physical Asset Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL__PHYSICAL_ASSET_CLASSES = 5;

	/**
	 * The feature id for the '<em><b>Physical Assets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL__PHYSICAL_ASSETS = 6;

	/**
	 * The number of structural features of the '<em>Physical Asset Actual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Physical Asset Actual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_ACTUAL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualImpl <em>Equipment Actual</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentActual()
	 * @generated
	 */
	int EQUIPMENT_ACTUAL = 84;

	/**
	 * The feature id for the '<em><b>Equipment Actual Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL__EQUIPMENT_ACTUAL_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Equipment Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL__EQUIPMENT_USE = 2;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL__QUANTITY = 3;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL__QUANTITY_UNIT_OF_MEASURE = 4;

	/**
	 * The feature id for the '<em><b>Equipment Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL__EQUIPMENT_CLASSES = 5;

	/**
	 * The feature id for the '<em><b>Equipments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL__EQUIPMENTS = 6;

	/**
	 * The number of structural features of the '<em>Equipment Actual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Equipment Actual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_ACTUAL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelActualImpl <em>Personnel Actual</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelActualImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelActual()
	 * @generated
	 */
	int PERSONNEL_ACTUAL = 85;

	/**
	 * The feature id for the '<em><b>Personnel Actual Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL__PERSONNEL_ACTUAL_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Personnel Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL__PERSONNEL_USE = 2;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL__QUANTITY = 3;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL__QUANTITY_UNIT_OF_MEASURE = 4;

	/**
	 * The feature id for the '<em><b>Personnel Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL__PERSONNEL_CLASSES = 5;

	/**
	 * The feature id for the '<em><b>Persons</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL__PERSONS = 6;

	/**
	 * The number of structural features of the '<em>Personnel Actual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Personnel Actual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_ACTUAL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentDataImpl <em>Segment Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.SegmentDataImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getSegmentData()
	 * @generated
	 */
	int SEGMENT_DATA = 86;

	/**
	 * The feature id for the '<em><b>Sub Data</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_DATA__SUB_DATA = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_DATA__ID = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_DATA__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_DATA__VALUE = 3;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_DATA__VALUE_UNIT_OF_MEASURE = 4;

	/**
	 * The number of structural features of the '<em>Segment Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_DATA_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Segment Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEGMENT_DATA_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl <em>Operations Response</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsResponse()
	 * @generated
	 */
	int OPERATIONS_RESPONSE = 88;

	/**
	 * The feature id for the '<em><b>Segment Responses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_RESPONSE__SEGMENT_RESPONSES = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_RESPONSE__ID = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_RESPONSE__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Operations Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_RESPONSE__OPERATIONS_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_RESPONSE__START_TIME = 4;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_RESPONSE__END_TIME = 5;

	/**
	 * The feature id for the '<em><b>Response State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_RESPONSE__RESPONSE_STATE = 6;

	/**
	 * The feature id for the '<em><b>Operations Request</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_RESPONSE__OPERATIONS_REQUEST = 7;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_RESPONSE__HIERARCHY_SCOPE = 8;

	/**
	 * The feature id for the '<em><b>Operations Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_RESPONSE__OPERATIONS_DEFINITION = 9;

	/**
	 * The number of structural features of the '<em>Operations Response</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_RESPONSE_FEATURE_COUNT = 10;

	/**
	 * The number of operations of the '<em>Operations Response</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_RESPONSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl <em>Operations Performance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsPerformance()
	 * @generated
	 */
	int OPERATIONS_PERFORMANCE = 89;

	/**
	 * The feature id for the '<em><b>Operations Responses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE__OPERATIONS_RESPONSES = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE__ID = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Operations Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE__OPERATIONS_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE__START_TIME = 4;

	/**
	 * The feature id for the '<em><b>Operations Schedule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE__OPERATIONS_SCHEDULE = 5;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE__END_TIME = 6;

	/**
	 * The feature id for the '<em><b>Performance State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE__PERFORMANCE_STATE = 7;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE__HIERARCHY_SCOPE = 8;

	/**
	 * The feature id for the '<em><b>Published Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE__PUBLISHED_DATE = 9;

	/**
	 * The number of structural features of the '<em>Operations Performance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE_FEATURE_COUNT = 10;

	/**
	 * The number of operations of the '<em>Operations Performance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_PERFORMANCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl <em>Operations Capability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsCapability()
	 * @generated
	 */
	int OPERATIONS_CAPABILITY = 90;

	/**
	 * The feature id for the '<em><b>Process Segment Capabilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__PROCESS_SEGMENT_CAPABILITIES = 0;

	/**
	 * The feature id for the '<em><b>Personnel Capabilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__PERSONNEL_CAPABILITIES = 1;

	/**
	 * The feature id for the '<em><b>Equipment Capabilities</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__EQUIPMENT_CAPABILITIES = 2;

	/**
	 * The feature id for the '<em><b>Physical Asset Capabilities</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__PHYSICAL_ASSET_CAPABILITIES = 3;

	/**
	 * The feature id for the '<em><b>Material Capabilities</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__MATERIAL_CAPABILITIES = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__ID = 5;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__DESCRIPTION = 6;

	/**
	 * The feature id for the '<em><b>Capacity Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__CAPACITY_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Reason</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__REASON = 8;

	/**
	 * The feature id for the '<em><b>Confidence Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__CONFIDENCE_FACTOR = 9;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__START_TIME = 10;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__END_TIME = 11;

	/**
	 * The feature id for the '<em><b>Published Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__PUBLISHED_DATE = 12;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY__HIERARCHY_SCOPE = 13;

	/**
	 * The number of structural features of the '<em>Operations Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY_FEATURE_COUNT = 14;

	/**
	 * The number of operations of the '<em>Operations Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONS_CAPABILITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityPropertyImpl <em>Material Capability Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialCapabilityProperty()
	 * @generated
	 */
	int MATERIAL_CAPABILITY_PROPERTY = 91;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_PROPERTY__DESCRIPTION = REFERENTIAL_MATERIAL_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_PROPERTY__VALUE = REFERENTIAL_MATERIAL_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_MATERIAL_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_PROPERTY__QUANTITY = REFERENTIAL_MATERIAL_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_MATERIAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Material Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_PROPERTY__MATERIAL_CLASS_PROPERTY = REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Material Definition Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_PROPERTY__MATERIAL_DEFINITION_PROPERTY = REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_DEFINITION_PROPERTY;

	/**
	 * The feature id for the '<em><b>Material Lot Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_PROPERTY__MATERIAL_LOT_PROPERTY = REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_LOT_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_PROPERTY__SUB_PROPERTIES = REFERENTIAL_MATERIAL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Material Capability Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_PROPERTY_FEATURE_COUNT = REFERENTIAL_MATERIAL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Material Capability Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_PROPERTY_OPERATION_COUNT = REFERENTIAL_MATERIAL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityPropertyImpl <em>Physical Asset Capability Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetCapabilityProperty()
	 * @generated
	 */
	int PHYSICAL_ASSET_CAPABILITY_PROPERTY = 92;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_PROPERTY__DESCRIPTION = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_PROPERTY__VALUE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_PROPERTY__QUANTITY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Physical Asset Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Physical Asset Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_PROPERTY__PHYSICAL_ASSET_PROPERTY = REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_PROPERTY__SUB_PROPERTIES = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Physical Asset Capability Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_PROPERTY_FEATURE_COUNT = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Physical Asset Capability Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_PROPERTY_OPERATION_COUNT = REFERENTIAL_PHYSICAL_ASSET_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityPropertyImpl <em>Equipment Capability Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentCapabilityProperty()
	 * @generated
	 */
	int EQUIPMENT_CAPABILITY_PROPERTY = 93;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_PROPERTY__DESCRIPTION = REFERENTIAL_EQUIPMENT_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_PROPERTY__VALUE = REFERENTIAL_EQUIPMENT_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_EQUIPMENT_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_PROPERTY__QUANTITY = REFERENTIAL_EQUIPMENT_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_EQUIPMENT_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Equipment Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_PROPERTY__EQUIPMENT_CLASS_PROPERTY = REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Equipment Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_PROPERTY__EQUIPMENT_PROPERTY = REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_PROPERTY__SUB_PROPERTIES = REFERENTIAL_EQUIPMENT_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Equipment Capability Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_PROPERTY_FEATURE_COUNT = REFERENTIAL_EQUIPMENT_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Equipment Capability Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_PROPERTY_OPERATION_COUNT = REFERENTIAL_EQUIPMENT_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelCapabilityPropertyImpl <em>Personnel Capability Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelCapabilityPropertyImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelCapabilityProperty()
	 * @generated
	 */
	int PERSONNEL_CAPABILITY_PROPERTY = 94;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY_PROPERTY__DESCRIPTION = REFERENTIAL_PERSONNEL_PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY_PROPERTY__VALUE = REFERENTIAL_PERSONNEL_PROPERTY__VALUE;

	/**
	 * The feature id for the '<em><b>Value Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY_PROPERTY__VALUE_UNIT_OF_MEASURE = REFERENTIAL_PERSONNEL_PROPERTY__VALUE_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY_PROPERTY__QUANTITY = REFERENTIAL_PERSONNEL_PROPERTY__QUANTITY;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY_PROPERTY__QUANTITY_UNIT_OF_MEASURE = REFERENTIAL_PERSONNEL_PROPERTY__QUANTITY_UNIT_OF_MEASURE;

	/**
	 * The feature id for the '<em><b>Personnel Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY_PROPERTY__PERSONNEL_CLASS_PROPERTY = REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Person Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY_PROPERTY__PERSON_PROPERTY = REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY;

	/**
	 * The feature id for the '<em><b>Sub Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY_PROPERTY__SUB_PROPERTIES = REFERENTIAL_PERSONNEL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Personnel Capability Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY_PROPERTY_FEATURE_COUNT = REFERENTIAL_PERSONNEL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Personnel Capability Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY_PROPERTY_OPERATION_COUNT = REFERENTIAL_PERSONNEL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl <em>Material Capability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialCapability()
	 * @generated
	 */
	int MATERIAL_CAPABILITY = 95;

	/**
	 * The feature id for the '<em><b>Assembled From Capabilities</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__ASSEMBLED_FROM_CAPABILITIES = 0;

	/**
	 * The feature id for the '<em><b>Material Capability Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__MATERIAL_CAPABILITY_PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Material Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__MATERIAL_CLASS = 2;

	/**
	 * The feature id for the '<em><b>Material Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__MATERIAL_DEFINITION = 3;

	/**
	 * The feature id for the '<em><b>Material Lot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__MATERIAL_LOT = 4;

	/**
	 * The feature id for the '<em><b>Material Sublot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__MATERIAL_SUBLOT = 5;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__DESCRIPTION = 6;

	/**
	 * The feature id for the '<em><b>Capability Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__CAPABILITY_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Reason</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__REASON = 8;

	/**
	 * The feature id for the '<em><b>Confidence Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__CONFIDENCE_FACTOR = 9;

	/**
	 * The feature id for the '<em><b>Material Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__MATERIAL_USE = 10;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__START_TIME = 11;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__END_TIME = 12;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__QUANTITY = 13;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__QUANTITY_UNIT_OF_MEASURE = 14;

	/**
	 * The feature id for the '<em><b>Hierarchyscope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__HIERARCHYSCOPE = 15;

	/**
	 * The feature id for the '<em><b>Assembly Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__ASSEMBLY_TYPE = 16;

	/**
	 * The feature id for the '<em><b>Assembly Relationship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY__ASSEMBLY_RELATIONSHIP = 17;

	/**
	 * The number of structural features of the '<em>Material Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_FEATURE_COUNT = 18;

	/**
	 * The number of operations of the '<em>Material Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_CAPABILITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl <em>Physical Asset Capability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetCapability()
	 * @generated
	 */
	int PHYSICAL_ASSET_CAPABILITY = 96;

	/**
	 * The feature id for the '<em><b>Physical Asset Capability Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CAPABILITY_PROPERTY = 0;

	/**
	 * The feature id for the '<em><b>Physical Asset Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CLASS = 1;

	/**
	 * The feature id for the '<em><b>Physical Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Capability Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__CAPABILITY_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Reason</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__REASON = 5;

	/**
	 * The feature id for the '<em><b>Confidence Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__CONFIDENCE_FACTOR = 6;

	/**
	 * The feature id for the '<em><b>Physical Asset Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_USE = 7;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__HIERARCHY_SCOPE = 8;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__START_TIME = 9;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__END_TIME = 10;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__QUANTITY = 11;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY__QUANTITY_UNIT_OF_MEASURE = 12;

	/**
	 * The number of structural features of the '<em>Physical Asset Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_FEATURE_COUNT = 13;

	/**
	 * The number of operations of the '<em>Physical Asset Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_ASSET_CAPABILITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl <em>Equipment Capability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentCapability()
	 * @generated
	 */
	int EQUIPMENT_CAPABILITY = 97;

	/**
	 * The feature id for the '<em><b>Equipment Capability Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__EQUIPMENT_CAPABILITY_PROPERTY = 0;

	/**
	 * The feature id for the '<em><b>Equipment Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__EQUIPMENT_CLASS = 1;

	/**
	 * The feature id for the '<em><b>Equipment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__EQUIPMENT = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Capability Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__CAPABILITY_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Reason</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__REASON = 5;

	/**
	 * The feature id for the '<em><b>Confidence Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__CONFIDENCE_FACTOR = 6;

	/**
	 * The feature id for the '<em><b>Equipment Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__EQUIPMENT_USE = 7;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__START_TIME = 8;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__END_TIME = 9;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__QUANTITY = 10;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__QUANTITY_UNIT_OF_MEASURE = 11;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY__HIERARCHY_SCOPE = 12;

	/**
	 * The number of structural features of the '<em>Equipment Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_FEATURE_COUNT = 13;

	/**
	 * The number of operations of the '<em>Equipment Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIPMENT_CAPABILITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelCapabilityImpl <em>Personnel Capability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelCapabilityImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelCapability()
	 * @generated
	 */
	int PERSONNEL_CAPABILITY = 98;

	/**
	 * The feature id for the '<em><b>Personnel Capability Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__PERSONNEL_CAPABILITY_PROPERTY = 0;

	/**
	 * The feature id for the '<em><b>Personnel Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__PERSONNEL_CLASS = 1;

	/**
	 * The feature id for the '<em><b>Person</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__PERSON = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Capability Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__CAPABILITY_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Reason</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__REASON = 5;

	/**
	 * The feature id for the '<em><b>Confidence Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__CONFIDENCE_FACTOR = 6;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__HIERARCHY_SCOPE = 7;

	/**
	 * The feature id for the '<em><b>Personnel Use</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__PERSONNEL_USE = 8;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__START_TIME = 9;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__END_TIME = 10;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__QUANTITY = 11;

	/**
	 * The feature id for the '<em><b>Quantity Unit Of Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY__QUANTITY_UNIT_OF_MEASURE = 12;

	/**
	 * The number of structural features of the '<em>Personnel Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY_FEATURE_COUNT = 13;

	/**
	 * The number of operations of the '<em>Personnel Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSONNEL_CAPABILITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentCapabilityImpl <em>Process Segment Capability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentCapabilityImpl
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getProcessSegmentCapability()
	 * @generated
	 */
	int PROCESS_SEGMENT_CAPABILITY = 99;

	/**
	 * The feature id for the '<em><b>Sub Capabilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__SUB_CAPABILITIES = 0;

	/**
	 * The feature id for the '<em><b>Personnel Capabilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__PERSONNEL_CAPABILITIES = 1;

	/**
	 * The feature id for the '<em><b>Equipment Capabilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__EQUIPMENT_CAPABILITIES = 2;

	/**
	 * The feature id for the '<em><b>Physical Asset Capabilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__PHYSICAL_ASSET_CAPABILITIES = 3;

	/**
	 * The feature id for the '<em><b>Material Capabilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__MATERIAL_CAPABILITIES = 4;

	/**
	 * The feature id for the '<em><b>Process Segment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__PROCESS_SEGMENT = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__ID = 6;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__DESCRIPTION = 7;

	/**
	 * The feature id for the '<em><b>Capacity Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__CAPACITY_TYPE = 8;

	/**
	 * The feature id for the '<em><b>Reason</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__REASON = 9;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__START_TIME = 10;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__END_TIME = 11;

	/**
	 * The feature id for the '<em><b>Hierarchy Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY__HIERARCHY_SCOPE = 12;

	/**
	 * The number of structural features of the '<em>Process Segment Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY_FEATURE_COUNT = 13;

	/**
	 * The number of operations of the '<em>Process Segment Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_SEGMENT_CAPABILITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.AssemblyType <em>Assembly Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyType
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getAssemblyType()
	 * @generated
	 */
	int ASSEMBLY_TYPE = 106;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.AssemblyRelationship <em>Assembly Relationship</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyRelationship
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getAssemblyRelationship()
	 * @generated
	 */
	int ASSEMBLY_RELATIONSHIP = 107;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.OperationsType <em>Operations Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsType()
	 * @generated
	 */
	int OPERATIONS_TYPE = 108;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.MaterialUse <em>Material Use</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialUse
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialUse()
	 * @generated
	 */
	int MATERIAL_USE = 109;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.UseType <em>Use Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.UseType
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getUseType()
	 * @generated
	 */
	int USE_TYPE = 110;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.ScheduleState <em>Schedule State</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.ScheduleState
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getScheduleState()
	 * @generated
	 */
	int SCHEDULE_STATE = 111;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.PerformanceState <em>Performance State</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.PerformanceState
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPerformanceState()
	 * @generated
	 */
	int PERFORMANCE_STATE = 112;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.CapabilityType <em>Capability Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.CapabilityType
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getCapabilityType()
	 * @generated
	 */
	int CAPABILITY_TYPE = 113;

	/**
	 * The meta object id for the '{@link at.ac.tuwien.big.ame.iec62264.CapacityType <em>Capacity Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see at.ac.tuwien.big.ame.iec62264.CapacityType
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getCapacityType()
	 * @generated
	 */
	int CAPACITY_TYPE = 114;

	/**
	 * The meta object id for the '<em>Zoned Date Time</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.time.ZonedDateTime
	 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getZonedDateTime()
	 * @generated
	 */
	int ZONED_DATE_TIME = 115;

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Person
	 * @generated
	 */
	EClass getPerson();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.Person#getPersonnelClasses <em>Personnel Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Personnel Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Person#getPersonnelClasses()
	 * @see #getPerson()
	 * @generated
	 */
	EReference getPerson_PersonnelClasses();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.Person#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Person#getProperties()
	 * @see #getPerson()
	 * @generated
	 */
	EReference getPerson_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.Person#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Person#getTestSpecifications()
	 * @see #getPerson()
	 * @generated
	 */
	EReference getPerson_TestSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.Person#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Person#getId()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.Person#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Person#getDescription()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.Person#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Person#getName()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_Name();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClass <em>Personnel Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Class</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClass
	 * @generated
	 */
	EClass getPersonnelClass();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClass#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClass#getProperties()
	 * @see #getPersonnelClass()
	 * @generated
	 */
	EReference getPersonnelClass_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClass#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClass#getTestSpecifications()
	 * @see #getPersonnelClass()
	 * @generated
	 */
	EReference getPersonnelClass_TestSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClass#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClass#getId()
	 * @see #getPersonnelClass()
	 * @generated
	 */
	EAttribute getPersonnelClass_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClass#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClass#getDescription()
	 * @see #getPersonnelClass()
	 * @generated
	 */
	EAttribute getPersonnelClass_Description();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification <em>Qualification Test Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Qualification Test Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification
	 * @generated
	 */
	EClass getQualificationTestSpecification();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification#getId()
	 * @see #getQualificationTestSpecification()
	 * @generated
	 */
	EAttribute getQualificationTestSpecification_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification#getDescription()
	 * @see #getQualificationTestSpecification()
	 * @generated
	 */
	EAttribute getQualificationTestSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestSpecification#getVersion()
	 * @see #getQualificationTestSpecification()
	 * @generated
	 */
	EAttribute getQualificationTestSpecification_Version();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty <em>Personnel Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Class Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty
	 * @generated
	 */
	EClass getPersonnelClassProperty();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty#getTestSpecifications()
	 * @see #getPersonnelClassProperty()
	 * @generated
	 */
	EReference getPersonnelClassProperty_TestSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty#getSubProperties()
	 * @see #getPersonnelClassProperty()
	 * @generated
	 */
	EReference getPersonnelClassProperty_SubProperties();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty#getId()
	 * @see #getPersonnelClassProperty()
	 * @generated
	 */
	EAttribute getPersonnelClassProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty#getDescription()
	 * @see #getPersonnelClassProperty()
	 * @generated
	 */
	EAttribute getPersonnelClassProperty_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty#getValue()
	 * @see #getPersonnelClassProperty()
	 * @generated
	 */
	EAttribute getPersonnelClassProperty_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty#getValueUnitOfMeasure()
	 * @see #getPersonnelClassProperty()
	 * @generated
	 */
	EAttribute getPersonnelClassProperty_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonProperty <em>Person Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonProperty
	 * @generated
	 */
	EClass getPersonProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonProperty#getSubProperties()
	 * @see #getPersonProperty()
	 * @generated
	 */
	EReference getPersonProperty_SubProperties();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.PersonProperty#getPersonnelClassProperty <em>Personnel Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Personnel Class Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonProperty#getPersonnelClassProperty()
	 * @see #getPersonProperty()
	 * @generated
	 */
	EReference getPersonProperty_PersonnelClassProperty();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonProperty#getId()
	 * @see #getPersonProperty()
	 * @generated
	 */
	EAttribute getPersonProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonProperty#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonProperty#getDescription()
	 * @see #getPersonProperty()
	 * @generated
	 */
	EAttribute getPersonProperty_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonProperty#getValue()
	 * @see #getPersonProperty()
	 * @generated
	 */
	EAttribute getPersonProperty_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonProperty#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonProperty#getValueUnitOfMeasure()
	 * @see #getPersonProperty()
	 * @generated
	 */
	EAttribute getPersonProperty_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestResult <em>Qualification Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Qualification Test Result</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestResult
	 * @generated
	 */
	EClass getQualificationTestResult();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getTestSpecification <em>Test Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Test Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getTestSpecification()
	 * @see #getQualificationTestResult()
	 * @generated
	 */
	EReference getQualificationTestResult_TestSpecification();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getProperty()
	 * @see #getQualificationTestResult()
	 * @generated
	 */
	EReference getQualificationTestResult_Property();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getId()
	 * @see #getQualificationTestResult()
	 * @generated
	 */
	EAttribute getQualificationTestResult_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getDescription()
	 * @see #getQualificationTestResult()
	 * @generated
	 */
	EAttribute getQualificationTestResult_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getDate()
	 * @see #getQualificationTestResult()
	 * @generated
	 */
	EAttribute getQualificationTestResult_Date();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getResult <em>Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getResult()
	 * @see #getQualificationTestResult()
	 * @generated
	 */
	EAttribute getQualificationTestResult_Result();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getResultUnitOfMeasure <em>Result Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getResultUnitOfMeasure()
	 * @see #getQualificationTestResult()
	 * @generated
	 */
	EAttribute getQualificationTestResult_ResultUnitOfMeasure();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getExpiration <em>Expiration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expiration</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.QualificationTestResult#getExpiration()
	 * @see #getQualificationTestResult()
	 * @generated
	 */
	EAttribute getQualificationTestResult_Expiration();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelModel <em>Personnel Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelModel
	 * @generated
	 */
	EClass getPersonnelModel();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelModel#getPersonnelClasses <em>Personnel Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Personnel Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelModel#getPersonnelClasses()
	 * @see #getPersonnelModel()
	 * @generated
	 */
	EReference getPersonnelModel_PersonnelClasses();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelModel#getPersons <em>Persons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Persons</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelModel#getPersons()
	 * @see #getPersonnelModel()
	 * @generated
	 */
	EReference getPersonnelModel_Persons();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelModel#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelModel#getTestSpecifications()
	 * @see #getPersonnelModel()
	 * @generated
	 */
	EReference getPersonnelModel_TestSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelModel#getTestResults <em>Test Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Results</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelModel#getTestResults()
	 * @see #getPersonnelModel()
	 * @generated
	 */
	EReference getPersonnelModel_TestResults();

	/**
	 * Returns the meta object for the containment reference '{@link at.ac.tuwien.big.ame.iec62264.PersonnelModel#getOperationsCapabilityModel <em>Operations Capability Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operations Capability Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelModel#getOperationsCapabilityModel()
	 * @see #getPersonnelModel()
	 * @generated
	 */
	EReference getPersonnelModel_OperationsCapabilityModel();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentModel <em>Equipment Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentModel
	 * @generated
	 */
	EClass getEquipmentModel();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentModel#getEquipmentClasses <em>Equipment Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equipment Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentModel#getEquipmentClasses()
	 * @see #getEquipmentModel()
	 * @generated
	 */
	EReference getEquipmentModel_EquipmentClasses();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentModel#getEquipments <em>Equipments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equipments</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentModel#getEquipments()
	 * @see #getEquipmentModel()
	 * @generated
	 */
	EReference getEquipmentModel_Equipments();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentModel#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentModel#getTestSpecifications()
	 * @see #getEquipmentModel()
	 * @generated
	 */
	EReference getEquipmentModel_TestSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentModel#getTestResults <em>Test Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Results</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentModel#getTestResults()
	 * @see #getEquipmentModel()
	 * @generated
	 */
	EReference getEquipmentModel_TestResults();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClass <em>Equipment Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Class</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClass
	 * @generated
	 */
	EClass getEquipmentClass();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClass#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClass#getProperties()
	 * @see #getEquipmentClass()
	 * @generated
	 */
	EReference getEquipmentClass_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClass#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClass#getTestSpecifications()
	 * @see #getEquipmentClass()
	 * @generated
	 */
	EReference getEquipmentClass_TestSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClass#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClass#getId()
	 * @see #getEquipmentClass()
	 * @generated
	 */
	EAttribute getEquipmentClass_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClass#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClass#getDescription()
	 * @see #getEquipmentClass()
	 * @generated
	 */
	EAttribute getEquipmentClass_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClass#getEquipmentLevel <em>Equipment Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Equipment Level</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClass#getEquipmentLevel()
	 * @see #getEquipmentClass()
	 * @generated
	 */
	EAttribute getEquipmentClass_EquipmentLevel();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult <em>Equipment Capability Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Capability Test Result</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult
	 * @generated
	 */
	EClass getEquipmentCapabilityTestResult();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getTestSpecification <em>Test Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Test Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getTestSpecification()
	 * @see #getEquipmentCapabilityTestResult()
	 * @generated
	 */
	EReference getEquipmentCapabilityTestResult_TestSpecification();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getProperty()
	 * @see #getEquipmentCapabilityTestResult()
	 * @generated
	 */
	EReference getEquipmentCapabilityTestResult_Property();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getId()
	 * @see #getEquipmentCapabilityTestResult()
	 * @generated
	 */
	EAttribute getEquipmentCapabilityTestResult_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getDescription()
	 * @see #getEquipmentCapabilityTestResult()
	 * @generated
	 */
	EAttribute getEquipmentCapabilityTestResult_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getDate()
	 * @see #getEquipmentCapabilityTestResult()
	 * @generated
	 */
	EAttribute getEquipmentCapabilityTestResult_Date();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getResult <em>Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getResult()
	 * @see #getEquipmentCapabilityTestResult()
	 * @generated
	 */
	EAttribute getEquipmentCapabilityTestResult_Result();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getResultUnitOfMeasure <em>Result Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getResultUnitOfMeasure()
	 * @see #getEquipmentCapabilityTestResult()
	 * @generated
	 */
	EAttribute getEquipmentCapabilityTestResult_ResultUnitOfMeasure();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getExpiration <em>Expiration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expiration</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult#getExpiration()
	 * @see #getEquipmentCapabilityTestResult()
	 * @generated
	 */
	EAttribute getEquipmentCapabilityTestResult_Expiration();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification <em>Equipment Capability Test Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Capability Test Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification
	 * @generated
	 */
	EClass getEquipmentCapabilityTestSpecification();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification#getId()
	 * @see #getEquipmentCapabilityTestSpecification()
	 * @generated
	 */
	EAttribute getEquipmentCapabilityTestSpecification_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification#getDescription()
	 * @see #getEquipmentCapabilityTestSpecification()
	 * @generated
	 */
	EAttribute getEquipmentCapabilityTestSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification#getVersion()
	 * @see #getEquipmentCapabilityTestSpecification()
	 * @generated
	 */
	EAttribute getEquipmentCapabilityTestSpecification_Version();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty <em>Equipment Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Class Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty
	 * @generated
	 */
	EClass getEquipmentClassProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty#getSubProperties()
	 * @see #getEquipmentClassProperty()
	 * @generated
	 */
	EReference getEquipmentClassProperty_SubProperties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty#getTestSpecifications()
	 * @see #getEquipmentClassProperty()
	 * @generated
	 */
	EReference getEquipmentClassProperty_TestSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty#getId()
	 * @see #getEquipmentClassProperty()
	 * @generated
	 */
	EAttribute getEquipmentClassProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty#getDescription()
	 * @see #getEquipmentClassProperty()
	 * @generated
	 */
	EAttribute getEquipmentClassProperty_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty#getValue()
	 * @see #getEquipmentClassProperty()
	 * @generated
	 */
	EAttribute getEquipmentClassProperty_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentClassProperty#getValueUnitOfMeasure()
	 * @see #getEquipmentClassProperty()
	 * @generated
	 */
	EAttribute getEquipmentClassProperty_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentProperty <em>Equipment Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentProperty
	 * @generated
	 */
	EClass getEquipmentProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentProperty#getSubProperties()
	 * @see #getEquipmentProperty()
	 * @generated
	 */
	EReference getEquipmentProperty_SubProperties();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.EquipmentProperty#getEquipmentClassProperty <em>Equipment Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Equipment Class Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentProperty#getEquipmentClassProperty()
	 * @see #getEquipmentProperty()
	 * @generated
	 */
	EReference getEquipmentProperty_EquipmentClassProperty();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentProperty#getId()
	 * @see #getEquipmentProperty()
	 * @generated
	 */
	EAttribute getEquipmentProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentProperty#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentProperty#getDescription()
	 * @see #getEquipmentProperty()
	 * @generated
	 */
	EAttribute getEquipmentProperty_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentProperty#getValue()
	 * @see #getEquipmentProperty()
	 * @generated
	 */
	EAttribute getEquipmentProperty_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentProperty#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentProperty#getValueUnitOfMeasure()
	 * @see #getEquipmentProperty()
	 * @generated
	 */
	EAttribute getEquipmentProperty_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.Equipment <em>Equipment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Equipment
	 * @generated
	 */
	EClass getEquipment();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.Equipment#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Children</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Equipment#getChildren()
	 * @see #getEquipment()
	 * @generated
	 */
	EReference getEquipment_Children();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.Equipment#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Equipment#getProperties()
	 * @see #getEquipment()
	 * @generated
	 */
	EReference getEquipment_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.Equipment#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Equipment#getTestSpecifications()
	 * @see #getEquipment()
	 * @generated
	 */
	EReference getEquipment_TestSpecifications();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.Equipment#getEquipmentClass <em>Equipment Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Equipment Class</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Equipment#getEquipmentClass()
	 * @see #getEquipment()
	 * @generated
	 */
	EReference getEquipment_EquipmentClass();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.Equipment#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Equipment#getId()
	 * @see #getEquipment()
	 * @generated
	 */
	EAttribute getEquipment_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.Equipment#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Equipment#getDescription()
	 * @see #getEquipment()
	 * @generated
	 */
	EAttribute getEquipment_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.Equipment#getEquipmentLevel <em>Equipment Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Equipment Level</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Equipment#getEquipmentLevel()
	 * @see #getEquipment()
	 * @generated
	 */
	EAttribute getEquipment_EquipmentLevel();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel <em>Physical Asset Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel
	 * @generated
	 */
	EClass getPhysicalAssetModel();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel#getPhysicalAssets <em>Physical Assets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Assets</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel#getPhysicalAssets()
	 * @see #getPhysicalAssetModel()
	 * @generated
	 */
	EReference getPhysicalAssetModel_PhysicalAssets();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel#getPhysicalAssetClasses <em>Physical Asset Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Asset Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel#getPhysicalAssetClasses()
	 * @see #getPhysicalAssetModel()
	 * @generated
	 */
	EReference getPhysicalAssetModel_PhysicalAssetClasses();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel#getTestSpecifications()
	 * @see #getPhysicalAssetModel()
	 * @generated
	 */
	EReference getPhysicalAssetModel_TestSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel#getEquipmentAssetMappings <em>Equipment Asset Mappings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equipment Asset Mappings</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel#getEquipmentAssetMappings()
	 * @see #getPhysicalAssetModel()
	 * @generated
	 */
	EReference getPhysicalAssetModel_EquipmentAssetMappings();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel#getTestResults <em>Test Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Results</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetModel#getTestResults()
	 * @see #getPhysicalAssetModel()
	 * @generated
	 */
	EReference getPhysicalAssetModel_TestResults();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass <em>Physical Asset Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Class</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass
	 * @generated
	 */
	EClass getPhysicalAssetClass();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass#getProperties()
	 * @see #getPhysicalAssetClass()
	 * @generated
	 */
	EReference getPhysicalAssetClass_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass#getTestSpecifications()
	 * @see #getPhysicalAssetClass()
	 * @generated
	 */
	EReference getPhysicalAssetClass_TestSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass#getId()
	 * @see #getPhysicalAssetClass()
	 * @generated
	 */
	EAttribute getPhysicalAssetClass_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass#getManufacturer <em>Manufacturer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Manufacturer</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass#getManufacturer()
	 * @see #getPhysicalAssetClass()
	 * @generated
	 */
	EAttribute getPhysicalAssetClass_Manufacturer();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass#getDescription()
	 * @see #getPhysicalAssetClass()
	 * @generated
	 */
	EAttribute getPhysicalAssetClass_Description();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult <em>Physical Asset Capability Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Capability Test Result</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult
	 * @generated
	 */
	EClass getPhysicalAssetCapabilityTestResult();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getTestSpecification <em>Test Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Test Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getTestSpecification()
	 * @see #getPhysicalAssetCapabilityTestResult()
	 * @generated
	 */
	EReference getPhysicalAssetCapabilityTestResult_TestSpecification();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getProperty()
	 * @see #getPhysicalAssetCapabilityTestResult()
	 * @generated
	 */
	EReference getPhysicalAssetCapabilityTestResult_Property();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getId()
	 * @see #getPhysicalAssetCapabilityTestResult()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapabilityTestResult_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getDescription()
	 * @see #getPhysicalAssetCapabilityTestResult()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapabilityTestResult_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getDate()
	 * @see #getPhysicalAssetCapabilityTestResult()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapabilityTestResult_Date();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getResult <em>Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getResult()
	 * @see #getPhysicalAssetCapabilityTestResult()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapabilityTestResult_Result();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getResultUnitOfMeasure <em>Result Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getResultUnitOfMeasure()
	 * @see #getPhysicalAssetCapabilityTestResult()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapabilityTestResult_ResultUnitOfMeasure();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getExpiration <em>Expiration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expiration</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestResult#getExpiration()
	 * @see #getPhysicalAssetCapabilityTestResult()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapabilityTestResult_Expiration();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty <em>Physical Asset Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty
	 * @generated
	 */
	EClass getPhysicalAssetProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty#getSubProperties()
	 * @see #getPhysicalAssetProperty()
	 * @generated
	 */
	EReference getPhysicalAssetProperty_SubProperties();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty#getId()
	 * @see #getPhysicalAssetProperty()
	 * @generated
	 */
	EAttribute getPhysicalAssetProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty#getDescription()
	 * @see #getPhysicalAssetProperty()
	 * @generated
	 */
	EAttribute getPhysicalAssetProperty_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty#getValue()
	 * @see #getPhysicalAssetProperty()
	 * @generated
	 */
	EAttribute getPhysicalAssetProperty_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty#getValueUnitOfMeasure()
	 * @see #getPhysicalAssetProperty()
	 * @generated
	 */
	EAttribute getPhysicalAssetProperty_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty#getPhysicalAssetClassProperty <em>Physical Asset Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Physical Asset Class Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetProperty#getPhysicalAssetClassProperty()
	 * @see #getPhysicalAssetProperty()
	 * @generated
	 */
	EReference getPhysicalAssetProperty_PhysicalAssetClassProperty();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset <em>Physical Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAsset
	 * @generated
	 */
	EClass getPhysicalAsset();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Children</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getChildren()
	 * @see #getPhysicalAsset()
	 * @generated
	 */
	EReference getPhysicalAsset_Children();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getProperties()
	 * @see #getPhysicalAsset()
	 * @generated
	 */
	EReference getPhysicalAsset_Properties();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getPhysicalAssetClass <em>Physical Asset Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Physical Asset Class</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getPhysicalAssetClass()
	 * @see #getPhysicalAsset()
	 * @generated
	 */
	EReference getPhysicalAsset_PhysicalAssetClass();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getTestSpecifications()
	 * @see #getPhysicalAsset()
	 * @generated
	 */
	EReference getPhysicalAsset_TestSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getId()
	 * @see #getPhysicalAsset()
	 * @generated
	 */
	EAttribute getPhysicalAsset_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getDescription()
	 * @see #getPhysicalAsset()
	 * @generated
	 */
	EAttribute getPhysicalAsset_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getPhysicalLocation <em>Physical Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Physical Location</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getPhysicalLocation()
	 * @see #getPhysicalAsset()
	 * @generated
	 */
	EAttribute getPhysicalAsset_PhysicalLocation();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getFixedAssetId <em>Fixed Asset Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fixed Asset Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getFixedAssetId()
	 * @see #getPhysicalAsset()
	 * @generated
	 */
	EAttribute getPhysicalAsset_FixedAssetId();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getVendorId <em>Vendor Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vendor Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAsset#getVendorId()
	 * @see #getPhysicalAsset()
	 * @generated
	 */
	EAttribute getPhysicalAsset_VendorId();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification <em>Physical Asset Capability Test Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Capability Test Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification
	 * @generated
	 */
	EClass getPhysicalAssetCapabilityTestSpecification();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification#getId()
	 * @see #getPhysicalAssetCapabilityTestSpecification()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapabilityTestSpecification_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification#getDescription()
	 * @see #getPhysicalAssetCapabilityTestSpecification()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapabilityTestSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityTestSpecification#getVersion()
	 * @see #getPhysicalAssetCapabilityTestSpecification()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapabilityTestSpecification_Version();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty <em>Physical Asset Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Class Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty
	 * @generated
	 */
	EClass getPhysicalAssetClassProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty#getSubProperties()
	 * @see #getPhysicalAssetClassProperty()
	 * @generated
	 */
	EReference getPhysicalAssetClassProperty_SubProperties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty#getTestSpecifications()
	 * @see #getPhysicalAssetClassProperty()
	 * @generated
	 */
	EReference getPhysicalAssetClassProperty_TestSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty#getId()
	 * @see #getPhysicalAssetClassProperty()
	 * @generated
	 */
	EAttribute getPhysicalAssetClassProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty#getDescription()
	 * @see #getPhysicalAssetClassProperty()
	 * @generated
	 */
	EAttribute getPhysicalAssetClassProperty_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty#getValue()
	 * @see #getPhysicalAssetClassProperty()
	 * @generated
	 */
	EAttribute getPhysicalAssetClassProperty_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetClassProperty#getValueUnitOfMeasure()
	 * @see #getPhysicalAssetClassProperty()
	 * @generated
	 */
	EAttribute getPhysicalAssetClassProperty_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping <em>Equipment Asset Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Asset Mapping</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping
	 * @generated
	 */
	EClass getEquipmentAssetMapping();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getPhysicalAsset <em>Physical Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Physical Asset</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getPhysicalAsset()
	 * @see #getEquipmentAssetMapping()
	 * @generated
	 */
	EReference getEquipmentAssetMapping_PhysicalAsset();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getEquipment <em>Equipment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Equipment</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getEquipment()
	 * @see #getEquipmentAssetMapping()
	 * @generated
	 */
	EReference getEquipmentAssetMapping_Equipment();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getId()
	 * @see #getEquipmentAssetMapping()
	 * @generated
	 */
	EAttribute getEquipmentAssetMapping_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getDescription()
	 * @see #getEquipmentAssetMapping()
	 * @generated
	 */
	EAttribute getEquipmentAssetMapping_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getStartTime()
	 * @see #getEquipmentAssetMapping()
	 * @generated
	 */
	EAttribute getEquipmentAssetMapping_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentAssetMapping#getEndTime()
	 * @see #getEquipmentAssetMapping()
	 * @generated
	 */
	EAttribute getEquipmentAssetMapping_EndTime();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialModel <em>Material Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialModel
	 * @generated
	 */
	EClass getMaterialModel();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialModel#getMaterialClasses <em>Material Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialModel#getMaterialClasses()
	 * @see #getMaterialModel()
	 * @generated
	 */
	EReference getMaterialModel_MaterialClasses();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialModel#getMaterialDefinitions <em>Material Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Definitions</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialModel#getMaterialDefinitions()
	 * @see #getMaterialModel()
	 * @generated
	 */
	EReference getMaterialModel_MaterialDefinitions();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialModel#getMaterialLots <em>Material Lots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Lots</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialModel#getMaterialLots()
	 * @see #getMaterialModel()
	 * @generated
	 */
	EReference getMaterialModel_MaterialLots();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialModel#getMaterialSublots <em>Material Sublots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Sublots</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialModel#getMaterialSublots()
	 * @see #getMaterialModel()
	 * @generated
	 */
	EReference getMaterialModel_MaterialSublots();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialModel#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialModel#getTestSpecifications()
	 * @see #getMaterialModel()
	 * @generated
	 */
	EReference getMaterialModel_TestSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialModel#getTestResults <em>Test Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Results</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialModel#getTestResults()
	 * @see #getMaterialModel()
	 * @generated
	 */
	EReference getMaterialModel_TestResults();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialClass <em>Material Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Class</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClass
	 * @generated
	 */
	EClass getMaterialClass();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialClass#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClass#getProperties()
	 * @see #getMaterialClass()
	 * @generated
	 */
	EReference getMaterialClass_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialClass#getAssembledFromClasses <em>Assembled From Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assembled From Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClass#getAssembledFromClasses()
	 * @see #getMaterialClass()
	 * @generated
	 */
	EReference getMaterialClass_AssembledFromClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialClass#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClass#getTestSpecifications()
	 * @see #getMaterialClass()
	 * @generated
	 */
	EReference getMaterialClass_TestSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialClass#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClass#getId()
	 * @see #getMaterialClass()
	 * @generated
	 */
	EAttribute getMaterialClass_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialClass#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClass#getDescription()
	 * @see #getMaterialClass()
	 * @generated
	 */
	EAttribute getMaterialClass_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialClass#getAssemblyType <em>Assembly Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClass#getAssemblyType()
	 * @see #getMaterialClass()
	 * @generated
	 */
	EAttribute getMaterialClass_AssemblyType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialClass#getAssemblyRelationship <em>Assembly Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Relationship</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClass#getAssemblyRelationship()
	 * @see #getMaterialClass()
	 * @generated
	 */
	EAttribute getMaterialClass_AssemblyRelationship();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestResult <em>Material Test Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Test Result</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestResult
	 * @generated
	 */
	EClass getMaterialTestResult();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getTestSpecification <em>Test Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Test Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getTestSpecification()
	 * @see #getMaterialTestResult()
	 * @generated
	 */
	EReference getMaterialTestResult_TestSpecification();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getProperty()
	 * @see #getMaterialTestResult()
	 * @generated
	 */
	EReference getMaterialTestResult_Property();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getId()
	 * @see #getMaterialTestResult()
	 * @generated
	 */
	EAttribute getMaterialTestResult_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getDescription()
	 * @see #getMaterialTestResult()
	 * @generated
	 */
	EAttribute getMaterialTestResult_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getDate()
	 * @see #getMaterialTestResult()
	 * @generated
	 */
	EAttribute getMaterialTestResult_Date();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getResult <em>Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getResult()
	 * @see #getMaterialTestResult()
	 * @generated
	 */
	EAttribute getMaterialTestResult_Result();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getResultUnitOfMeasure <em>Result Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getResultUnitOfMeasure()
	 * @see #getMaterialTestResult()
	 * @generated
	 */
	EAttribute getMaterialTestResult_ResultUnitOfMeasure();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getExpiration <em>Expiration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expiration</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestResult#getExpiration()
	 * @see #getMaterialTestResult()
	 * @generated
	 */
	EAttribute getMaterialTestResult_Expiration();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestSpecification <em>Material Test Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Test Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestSpecification
	 * @generated
	 */
	EClass getMaterialTestSpecification();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestSpecification#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestSpecification#getId()
	 * @see #getMaterialTestSpecification()
	 * @generated
	 */
	EAttribute getMaterialTestSpecification_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestSpecification#getDescription()
	 * @see #getMaterialTestSpecification()
	 * @generated
	 */
	EAttribute getMaterialTestSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialTestSpecification#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialTestSpecification#getVersion()
	 * @see #getMaterialTestSpecification()
	 * @generated
	 */
	EAttribute getMaterialTestSpecification_Version();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot <em>Material Sublot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Sublot</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot
	 * @generated
	 */
	EClass getMaterialSublot();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot#getProperties()
	 * @see #getMaterialSublot()
	 * @generated
	 */
	EReference getMaterialSublot_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot#getAssembledFromLots <em>Assembled From Lots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assembled From Lots</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot#getAssembledFromLots()
	 * @see #getMaterialSublot()
	 * @generated
	 */
	EReference getMaterialSublot_AssembledFromLots();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot#getAssembledFromSublots <em>Assembled From Sublots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assembled From Sublots</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot#getAssembledFromSublots()
	 * @see #getMaterialSublot()
	 * @generated
	 */
	EReference getMaterialSublot_AssembledFromSublots();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot#getSublots <em>Sublots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sublots</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot#getSublots()
	 * @see #getMaterialSublot()
	 * @generated
	 */
	EReference getMaterialSublot_Sublots();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot#getId()
	 * @see #getMaterialSublot()
	 * @generated
	 */
	EAttribute getMaterialSublot_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot#getDescription()
	 * @see #getMaterialSublot()
	 * @generated
	 */
	EAttribute getMaterialSublot_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot#getAssemblyType <em>Assembly Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot#getAssemblyType()
	 * @see #getMaterialSublot()
	 * @generated
	 */
	EAttribute getMaterialSublot_AssemblyType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot#getAssemblyRelationship <em>Assembly Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Relationship</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot#getAssemblyRelationship()
	 * @see #getMaterialSublot()
	 * @generated
	 */
	EAttribute getMaterialSublot_AssemblyRelationship();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot#getStatus()
	 * @see #getMaterialSublot()
	 * @generated
	 */
	EAttribute getMaterialSublot_Status();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot#getStorageLocation <em>Storage Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Storage Location</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot#getStorageLocation()
	 * @see #getMaterialSublot()
	 * @generated
	 */
	EAttribute getMaterialSublot_StorageLocation();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot#getQuantity()
	 * @see #getMaterialSublot()
	 * @generated
	 */
	EAttribute getMaterialSublot_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSublot#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSublot#getQuantityUnitOfMeasure()
	 * @see #getMaterialSublot()
	 * @generated
	 */
	EAttribute getMaterialSublot_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot <em>Material Lot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Lot</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot
	 * @generated
	 */
	EClass getMaterialLot();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getProperties()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EReference getMaterialLot_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getAssembledFromLots <em>Assembled From Lots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assembled From Lots</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getAssembledFromLots()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EReference getMaterialLot_AssembledFromLots();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getAssembledFromSublots <em>Assembled From Sublots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assembled From Sublots</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getAssembledFromSublots()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EReference getMaterialLot_AssembledFromSublots();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getSublots <em>Sublots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sublots</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getSublots()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EReference getMaterialLot_Sublots();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getMaterialDefinition <em>Material Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Material Definition</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getMaterialDefinition()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EReference getMaterialLot_MaterialDefinition();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getTestSpecifications()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EReference getMaterialLot_TestSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getId()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EAttribute getMaterialLot_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getDescription()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EAttribute getMaterialLot_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getAssemblyType <em>Assembly Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getAssemblyType()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EAttribute getMaterialLot_AssemblyType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getAssemblyRelationship <em>Assembly Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Relationship</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getAssemblyRelationship()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EAttribute getMaterialLot_AssemblyRelationship();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getStatus()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EAttribute getMaterialLot_Status();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getStorageLocation <em>Storage Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Storage Location</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getStorageLocation()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EAttribute getMaterialLot_StorageLocation();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getQuantity()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EAttribute getMaterialLot_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialLot#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLot#getQuantityUnitOfMeasure()
	 * @see #getMaterialLot()
	 * @generated
	 */
	EAttribute getMaterialLot_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialLotProperty <em>Material Lot Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Lot Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLotProperty
	 * @generated
	 */
	EClass getMaterialLotProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialLotProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLotProperty#getSubProperties()
	 * @see #getMaterialLotProperty()
	 * @generated
	 */
	EReference getMaterialLotProperty_SubProperties();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.MaterialLotProperty#getMaterialDefinitionProperty <em>Material Definition Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Material Definition Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLotProperty#getMaterialDefinitionProperty()
	 * @see #getMaterialLotProperty()
	 * @generated
	 */
	EReference getMaterialLotProperty_MaterialDefinitionProperty();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialLotProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLotProperty#getId()
	 * @see #getMaterialLotProperty()
	 * @generated
	 */
	EAttribute getMaterialLotProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialLotProperty#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLotProperty#getDescription()
	 * @see #getMaterialLotProperty()
	 * @generated
	 */
	EAttribute getMaterialLotProperty_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialLotProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLotProperty#getValue()
	 * @see #getMaterialLotProperty()
	 * @generated
	 */
	EAttribute getMaterialLotProperty_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialLotProperty#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialLotProperty#getValueUnitOfMeasure()
	 * @see #getMaterialLotProperty()
	 * @generated
	 */
	EAttribute getMaterialLotProperty_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition <em>Material Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Definition</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinition
	 * @generated
	 */
	EClass getMaterialDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getProperties()
	 * @see #getMaterialDefinition()
	 * @generated
	 */
	EReference getMaterialDefinition_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getAssembledFromDefinitions <em>Assembled From Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assembled From Definitions</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getAssembledFromDefinitions()
	 * @see #getMaterialDefinition()
	 * @generated
	 */
	EReference getMaterialDefinition_AssembledFromDefinitions();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getMaterialClasses <em>Material Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getMaterialClasses()
	 * @see #getMaterialDefinition()
	 * @generated
	 */
	EReference getMaterialDefinition_MaterialClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getTestSpecifications()
	 * @see #getMaterialDefinition()
	 * @generated
	 */
	EReference getMaterialDefinition_TestSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getId()
	 * @see #getMaterialDefinition()
	 * @generated
	 */
	EAttribute getMaterialDefinition_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getDescription()
	 * @see #getMaterialDefinition()
	 * @generated
	 */
	EAttribute getMaterialDefinition_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getAssemblyType <em>Assembly Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getAssemblyType()
	 * @see #getMaterialDefinition()
	 * @generated
	 */
	EAttribute getMaterialDefinition_AssemblyType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getAssemblyRelationship <em>Assembly Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Relationship</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinition#getAssemblyRelationship()
	 * @see #getMaterialDefinition()
	 * @generated
	 */
	EAttribute getMaterialDefinition_AssemblyRelationship();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty <em>Material Definition Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Definition Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty
	 * @generated
	 */
	EClass getMaterialDefinitionProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getSubProperties()
	 * @see #getMaterialDefinitionProperty()
	 * @generated
	 */
	EReference getMaterialDefinitionProperty_SubProperties();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getMaterialClassProperty <em>Material Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Material Class Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getMaterialClassProperty()
	 * @see #getMaterialDefinitionProperty()
	 * @generated
	 */
	EReference getMaterialDefinitionProperty_MaterialClassProperty();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getTestSpecifications()
	 * @see #getMaterialDefinitionProperty()
	 * @generated
	 */
	EReference getMaterialDefinitionProperty_TestSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getId()
	 * @see #getMaterialDefinitionProperty()
	 * @generated
	 */
	EAttribute getMaterialDefinitionProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getDescription()
	 * @see #getMaterialDefinitionProperty()
	 * @generated
	 */
	EAttribute getMaterialDefinitionProperty_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getValue()
	 * @see #getMaterialDefinitionProperty()
	 * @generated
	 */
	EAttribute getMaterialDefinitionProperty_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty#getValueUnitOfMeasure()
	 * @see #getMaterialDefinitionProperty()
	 * @generated
	 */
	EAttribute getMaterialDefinitionProperty_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialClassProperty <em>Material Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Class Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClassProperty
	 * @generated
	 */
	EClass getMaterialClassProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialClassProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClassProperty#getSubProperties()
	 * @see #getMaterialClassProperty()
	 * @generated
	 */
	EReference getMaterialClassProperty_SubProperties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialClassProperty#getTestSpecifications <em>Test Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClassProperty#getTestSpecifications()
	 * @see #getMaterialClassProperty()
	 * @generated
	 */
	EReference getMaterialClassProperty_TestSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialClassProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClassProperty#getId()
	 * @see #getMaterialClassProperty()
	 * @generated
	 */
	EAttribute getMaterialClassProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialClassProperty#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClassProperty#getDescription()
	 * @see #getMaterialClassProperty()
	 * @generated
	 */
	EAttribute getMaterialClassProperty_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialClassProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClassProperty#getValue()
	 * @see #getMaterialClassProperty()
	 * @generated
	 */
	EAttribute getMaterialClassProperty_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialClassProperty#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialClassProperty#getValueUnitOfMeasure()
	 * @see #getMaterialClassProperty()
	 * @generated
	 */
	EAttribute getMaterialClassProperty_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel <em>Process Segment Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process Segment Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel
	 * @generated
	 */
	EClass getProcessSegmentModel();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel#getProcessSegments <em>Process Segments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Process Segments</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel#getProcessSegments()
	 * @see #getProcessSegmentModel()
	 * @generated
	 */
	EReference getProcessSegmentModel_ProcessSegments();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel#getProcessSegmentDependencies <em>Process Segment Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Process Segment Dependencies</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentModel#getProcessSegmentDependencies()
	 * @see #getProcessSegmentModel()
	 * @generated
	 */
	EReference getProcessSegmentModel_ProcessSegmentDependencies();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment <em>Process Segment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process Segment</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment
	 * @generated
	 */
	EClass getProcessSegment();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Children</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment#getChildren()
	 * @see #getProcessSegment()
	 * @generated
	 */
	EReference getProcessSegment_Children();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getProcessSegmentParameters <em>Process Segment Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Process Segment Parameters</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment#getProcessSegmentParameters()
	 * @see #getProcessSegment()
	 * @generated
	 */
	EReference getProcessSegment_ProcessSegmentParameters();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getPersonnelSegmentSpecifications <em>Personnel Segment Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Personnel Segment Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment#getPersonnelSegmentSpecifications()
	 * @see #getProcessSegment()
	 * @generated
	 */
	EReference getProcessSegment_PersonnelSegmentSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getEquipmentSegmentSpecifications <em>Equipment Segment Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equipment Segment Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment#getEquipmentSegmentSpecifications()
	 * @see #getProcessSegment()
	 * @generated
	 */
	EReference getProcessSegment_EquipmentSegmentSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getPhysicalAssetSegmentSpecifications <em>Physical Asset Segment Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Asset Segment Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment#getPhysicalAssetSegmentSpecifications()
	 * @see #getProcessSegment()
	 * @generated
	 */
	EReference getProcessSegment_PhysicalAssetSegmentSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getMaterialSegmentSpecifications <em>Material Segment Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Segment Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment#getMaterialSegmentSpecifications()
	 * @see #getProcessSegment()
	 * @generated
	 */
	EReference getProcessSegment_MaterialSegmentSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment#getId()
	 * @see #getProcessSegment()
	 * @generated
	 */
	EAttribute getProcessSegment_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment#getDescription()
	 * @see #getProcessSegment()
	 * @generated
	 */
	EAttribute getProcessSegment_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getOperationsType <em>Operations Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operations Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment#getOperationsType()
	 * @see #getProcessSegment()
	 * @generated
	 */
	EAttribute getProcessSegment_OperationsType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment#getDuration()
	 * @see #getProcessSegment()
	 * @generated
	 */
	EAttribute getProcessSegment_Duration();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getDurationUnitOfMeasure <em>Duration Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment#getDurationUnitOfMeasure()
	 * @see #getProcessSegment()
	 * @generated
	 */
	EAttribute getProcessSegment_DurationUnitOfMeasure();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegment#getHierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegment#getHierarchyScope()
	 * @see #getProcessSegment()
	 * @generated
	 */
	EReference getProcessSegment_HierarchyScope();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecificationProperty <em>Personnel Segment Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Segment Specification Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecificationProperty
	 * @generated
	 */
	EClass getPersonnelSegmentSpecificationProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecificationProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecificationProperty#getSubProperties()
	 * @see #getPersonnelSegmentSpecificationProperty()
	 * @generated
	 */
	EReference getPersonnelSegmentSpecificationProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecificationProperty <em>Equipment Segment Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Segment Specification Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecificationProperty
	 * @generated
	 */
	EClass getEquipmentSegmentSpecificationProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecificationProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecificationProperty#getSubProperties()
	 * @see #getEquipmentSegmentSpecificationProperty()
	 * @generated
	 */
	EReference getEquipmentSegmentSpecificationProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecificationProperty <em>Physical Asset Segment Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Segment Specification Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecificationProperty
	 * @generated
	 */
	EClass getPhysicalAssetSegmentSpecificationProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecificationProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecificationProperty#getSubProperties()
	 * @see #getPhysicalAssetSegmentSpecificationProperty()
	 * @generated
	 */
	EReference getPhysicalAssetSegmentSpecificationProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecificationProperty <em>Material Segment Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Segment Specification Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecificationProperty
	 * @generated
	 */
	EClass getMaterialSegmentSpecificationProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecificationProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecificationProperty#getSubProperties()
	 * @see #getMaterialSegmentSpecificationProperty()
	 * @generated
	 */
	EReference getMaterialSegmentSpecificationProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification <em>Material Segment Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Segment Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification
	 * @generated
	 */
	EClass getMaterialSegmentSpecification();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getAssembledFromSpecifications <em>Assembled From Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assembled From Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getAssembledFromSpecifications()
	 * @see #getMaterialSegmentSpecification()
	 * @generated
	 */
	EReference getMaterialSegmentSpecification_AssembledFromSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getProperties()
	 * @see #getMaterialSegmentSpecification()
	 * @generated
	 */
	EReference getMaterialSegmentSpecification_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getMaterialClasses <em>Material Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getMaterialClasses()
	 * @see #getMaterialSegmentSpecification()
	 * @generated
	 */
	EReference getMaterialSegmentSpecification_MaterialClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getMaterialDefinitions <em>Material Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Definitions</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getMaterialDefinitions()
	 * @see #getMaterialSegmentSpecification()
	 * @generated
	 */
	EReference getMaterialSegmentSpecification_MaterialDefinitions();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getDescription()
	 * @see #getMaterialSegmentSpecification()
	 * @generated
	 */
	EAttribute getMaterialSegmentSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getAssemblyType <em>Assembly Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getAssemblyType()
	 * @see #getMaterialSegmentSpecification()
	 * @generated
	 */
	EAttribute getMaterialSegmentSpecification_AssemblyType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getAssemblyRelationship <em>Assembly Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Relationship</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getAssemblyRelationship()
	 * @see #getMaterialSegmentSpecification()
	 * @generated
	 */
	EAttribute getMaterialSegmentSpecification_AssemblyRelationship();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getMaterialUse <em>Material Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Material Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getMaterialUse()
	 * @see #getMaterialSegmentSpecification()
	 * @generated
	 */
	EAttribute getMaterialSegmentSpecification_MaterialUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getQuantity()
	 * @see #getMaterialSegmentSpecification()
	 * @generated
	 */
	EAttribute getMaterialSegmentSpecification_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getQuantityUnitOfMeasurement <em>Quantity Unit Of Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measurement</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification#getQuantityUnitOfMeasurement()
	 * @see #getMaterialSegmentSpecification()
	 * @generated
	 */
	EAttribute getMaterialSegmentSpecification_QuantityUnitOfMeasurement();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification <em>Physical Asset Segment Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Segment Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification
	 * @generated
	 */
	EClass getPhysicalAssetSegmentSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getProperties()
	 * @see #getPhysicalAssetSegmentSpecification()
	 * @generated
	 */
	EReference getPhysicalAssetSegmentSpecification_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getPhysicalAssetClasses <em>Physical Asset Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Physical Asset Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getPhysicalAssetClasses()
	 * @see #getPhysicalAssetSegmentSpecification()
	 * @generated
	 */
	EReference getPhysicalAssetSegmentSpecification_PhysicalAssetClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getPhysicalAssets <em>Physical Assets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Physical Assets</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getPhysicalAssets()
	 * @see #getPhysicalAssetSegmentSpecification()
	 * @generated
	 */
	EReference getPhysicalAssetSegmentSpecification_PhysicalAssets();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getDescription()
	 * @see #getPhysicalAssetSegmentSpecification()
	 * @generated
	 */
	EAttribute getPhysicalAssetSegmentSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getPhysicalAssetUse <em>Physical Asset Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Physical Asset Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getPhysicalAssetUse()
	 * @see #getPhysicalAssetSegmentSpecification()
	 * @generated
	 */
	EAttribute getPhysicalAssetSegmentSpecification_PhysicalAssetUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getQuantity()
	 * @see #getPhysicalAssetSegmentSpecification()
	 * @generated
	 */
	EAttribute getPhysicalAssetSegmentSpecification_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification#getQuantityUnitOfMeasure()
	 * @see #getPhysicalAssetSegmentSpecification()
	 * @generated
	 */
	EAttribute getPhysicalAssetSegmentSpecification_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification <em>Equipment Segment Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Segment Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification
	 * @generated
	 */
	EClass getEquipmentSegmentSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getProperties()
	 * @see #getEquipmentSegmentSpecification()
	 * @generated
	 */
	EReference getEquipmentSegmentSpecification_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getEquipmentClasses <em>Equipment Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Equipment Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getEquipmentClasses()
	 * @see #getEquipmentSegmentSpecification()
	 * @generated
	 */
	EReference getEquipmentSegmentSpecification_EquipmentClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getEquipments <em>Equipments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Equipments</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getEquipments()
	 * @see #getEquipmentSegmentSpecification()
	 * @generated
	 */
	EReference getEquipmentSegmentSpecification_Equipments();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getDescription()
	 * @see #getEquipmentSegmentSpecification()
	 * @generated
	 */
	EAttribute getEquipmentSegmentSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getEquipmentUse <em>Equipment Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Equipment Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getEquipmentUse()
	 * @see #getEquipmentSegmentSpecification()
	 * @generated
	 */
	EAttribute getEquipmentSegmentSpecification_EquipmentUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getQuantity()
	 * @see #getEquipmentSegmentSpecification()
	 * @generated
	 */
	EAttribute getEquipmentSegmentSpecification_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getQuantityUnitOfMeasurement <em>Quantity Unit Of Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measurement</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification#getQuantityUnitOfMeasurement()
	 * @see #getEquipmentSegmentSpecification()
	 * @generated
	 */
	EAttribute getEquipmentSegmentSpecification_QuantityUnitOfMeasurement();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification <em>Personnel Segment Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Segment Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification
	 * @generated
	 */
	EClass getPersonnelSegmentSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getProperties()
	 * @see #getPersonnelSegmentSpecification()
	 * @generated
	 */
	EReference getPersonnelSegmentSpecification_Properties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getPersonnelClasses <em>Personnel Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Personnel Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getPersonnelClasses()
	 * @see #getPersonnelSegmentSpecification()
	 * @generated
	 */
	EReference getPersonnelSegmentSpecification_PersonnelClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getPersons <em>Persons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Persons</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getPersons()
	 * @see #getPersonnelSegmentSpecification()
	 * @generated
	 */
	EReference getPersonnelSegmentSpecification_Persons();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getDescription()
	 * @see #getPersonnelSegmentSpecification()
	 * @generated
	 */
	EAttribute getPersonnelSegmentSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getPersonnelUse <em>Personnel Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Personnel Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getPersonnelUse()
	 * @see #getPersonnelSegmentSpecification()
	 * @generated
	 */
	EAttribute getPersonnelSegmentSpecification_PersonnelUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getQuantity()
	 * @see #getPersonnelSegmentSpecification()
	 * @generated
	 */
	EAttribute getPersonnelSegmentSpecification_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification#getQuantityUnitOfMeasure()
	 * @see #getPersonnelSegmentSpecification()
	 * @generated
	 */
	EAttribute getPersonnelSegmentSpecification_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter <em>Process Segment Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process Segment Parameter</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter
	 * @generated
	 */
	EClass getProcessSegmentParameter();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter#getSubParameters <em>Sub Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Parameters</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter#getSubParameters()
	 * @see #getProcessSegmentParameter()
	 * @generated
	 */
	EReference getProcessSegmentParameter_SubParameters();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter#getId()
	 * @see #getProcessSegmentParameter()
	 * @generated
	 */
	EAttribute getProcessSegmentParameter_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter#getDescription()
	 * @see #getProcessSegmentParameter()
	 * @generated
	 */
	EAttribute getProcessSegmentParameter_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter#getValue()
	 * @see #getProcessSegmentParameter()
	 * @generated
	 */
	EAttribute getProcessSegmentParameter_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter#getValueUnitOfMeasure()
	 * @see #getProcessSegmentParameter()
	 * @generated
	 */
	EAttribute getProcessSegmentParameter_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency <em>Process Segment Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process Segment Dependency</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency
	 * @generated
	 */
	EClass getProcessSegmentDependency();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getSubject <em>Subject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Subject</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getSubject()
	 * @see #getProcessSegmentDependency()
	 * @generated
	 */
	EReference getProcessSegmentDependency_Subject();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getDependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dependency</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getDependency()
	 * @see #getProcessSegmentDependency()
	 * @generated
	 */
	EReference getProcessSegmentDependency_Dependency();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getId()
	 * @see #getProcessSegmentDependency()
	 * @generated
	 */
	EAttribute getProcessSegmentDependency_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getDescription()
	 * @see #getProcessSegmentDependency()
	 * @generated
	 */
	EAttribute getProcessSegmentDependency_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getDependencyType <em>Dependency Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dependency Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getDependencyType()
	 * @see #getProcessSegmentDependency()
	 * @generated
	 */
	EAttribute getProcessSegmentDependency_DependencyType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getDependencyFactor <em>Dependency Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dependency Factor</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getDependencyFactor()
	 * @see #getProcessSegmentDependency()
	 * @generated
	 */
	EAttribute getProcessSegmentDependency_DependencyFactor();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getFactorUnitOfMeasure <em>Factor Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Factor Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentDependency#getFactorUnitOfMeasure()
	 * @see #getProcessSegmentDependency()
	 * @generated
	 */
	EAttribute getProcessSegmentDependency_FactorUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model
	 * @generated
	 */
	EClass getIec62264Model();

	/**
	 * Returns the meta object for the containment reference '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getPersonnelModel <em>Personnel Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Personnel Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model#getPersonnelModel()
	 * @see #getIec62264Model()
	 * @generated
	 */
	EReference getIec62264Model_PersonnelModel();

	/**
	 * Returns the meta object for the containment reference '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getEquipmentModel <em>Equipment Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Equipment Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model#getEquipmentModel()
	 * @see #getIec62264Model()
	 * @generated
	 */
	EReference getIec62264Model_EquipmentModel();

	/**
	 * Returns the meta object for the containment reference '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getPhysicalAssetModel <em>Physical Asset Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Physical Asset Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model#getPhysicalAssetModel()
	 * @see #getIec62264Model()
	 * @generated
	 */
	EReference getIec62264Model_PhysicalAssetModel();

	/**
	 * Returns the meta object for the containment reference '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getMaterialModel <em>Material Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Material Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model#getMaterialModel()
	 * @see #getIec62264Model()
	 * @generated
	 */
	EReference getIec62264Model_MaterialModel();

	/**
	 * Returns the meta object for the containment reference '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getProcessSegmentModel <em>Process Segment Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Process Segment Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model#getProcessSegmentModel()
	 * @see #getIec62264Model()
	 * @generated
	 */
	EReference getIec62264Model_ProcessSegmentModel();

	/**
	 * Returns the meta object for the containment reference '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getOperationsDefinitionModel <em>Operations Definition Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operations Definition Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model#getOperationsDefinitionModel()
	 * @see #getIec62264Model()
	 * @generated
	 */
	EReference getIec62264Model_OperationsDefinitionModel();

	/**
	 * Returns the meta object for the containment reference '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getOperationsScheduleModel <em>Operations Schedule Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operations Schedule Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model#getOperationsScheduleModel()
	 * @see #getIec62264Model()
	 * @generated
	 */
	EReference getIec62264Model_OperationsScheduleModel();

	/**
	 * Returns the meta object for the containment reference '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getOperationsPerformanceModel <em>Operations Performance Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operations Performance Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model#getOperationsPerformanceModel()
	 * @see #getIec62264Model()
	 * @generated
	 */
	EReference getIec62264Model_OperationsPerformanceModel();

	/**
	 * Returns the meta object for the containment reference '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getProcessSegmentCapabilityModel <em>Process Segment Capability Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Process Segment Capability Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model#getProcessSegmentCapabilityModel()
	 * @see #getIec62264Model()
	 * @generated
	 */
	EReference getIec62264Model_ProcessSegmentCapabilityModel();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getHierarchyScopes <em>Hierarchy Scopes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hierarchy Scopes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model#getHierarchyScopes()
	 * @see #getIec62264Model()
	 * @generated
	 */
	EReference getIec62264Model_HierarchyScopes();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Model#getName()
	 * @see #getIec62264Model()
	 * @generated
	 */
	EAttribute getIec62264Model_Name();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel <em>Operations Definition Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Definition Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel
	 * @generated
	 */
	EClass getOperationsDefinitionModel();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel#getOperationsDefinitions <em>Operations Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations Definitions</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel#getOperationsDefinitions()
	 * @see #getOperationsDefinitionModel()
	 * @generated
	 */
	EReference getOperationsDefinitionModel_OperationsDefinitions();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel#getOperationsSegments <em>Operations Segments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations Segments</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel#getOperationsSegments()
	 * @see #getOperationsDefinitionModel()
	 * @generated
	 */
	EReference getOperationsDefinitionModel_OperationsSegments();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel#getOperationsSegmentDependencies <em>Operations Segment Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations Segment Dependencies</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinitionModel#getOperationsSegmentDependencies()
	 * @see #getOperationsDefinitionModel()
	 * @generated
	 */
	EReference getOperationsDefinitionModel_OperationsSegmentDependencies();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsScheduleModel <em>Operations Schedule Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Schedule Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsScheduleModel
	 * @generated
	 */
	EClass getOperationsScheduleModel();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsScheduleModel#getOperationsSchedules <em>Operations Schedules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations Schedules</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsScheduleModel#getOperationsSchedules()
	 * @see #getOperationsScheduleModel()
	 * @generated
	 */
	EReference getOperationsScheduleModel_OperationsSchedules();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformanceModel <em>Operations Performance Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Performance Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformanceModel
	 * @generated
	 */
	EClass getOperationsPerformanceModel();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformanceModel#getOperationsPerformances <em>Operations Performances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations Performances</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformanceModel#getOperationsPerformances()
	 * @see #getOperationsPerformanceModel()
	 * @generated
	 */
	EReference getOperationsPerformanceModel_OperationsPerformances();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapabilityModel <em>Operations Capability Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Capability Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapabilityModel
	 * @generated
	 */
	EClass getOperationsCapabilityModel();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapabilityModel#getOperationsCapabilities <em>Operations Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapabilityModel#getOperationsCapabilities()
	 * @see #getOperationsCapabilityModel()
	 * @generated
	 */
	EReference getOperationsCapabilityModel_OperationsCapabilities();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapabilityModel <em>Process Segment Capability Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process Segment Capability Model</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapabilityModel
	 * @generated
	 */
	EClass getProcessSegmentCapabilityModel();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapabilityModel#getProcessSegmentCapabilities <em>Process Segment Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Process Segment Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapabilityModel#getProcessSegmentCapabilities()
	 * @see #getProcessSegmentCapabilityModel()
	 * @generated
	 */
	EReference getProcessSegmentCapabilityModel_ProcessSegmentCapabilities();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.HierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.HierarchyScope
	 * @generated
	 */
	EClass getHierarchyScope();

	/**
	 * Returns the meta object for the containment reference '{@link at.ac.tuwien.big.ame.iec62264.HierarchyScope#getSubScope <em>Sub Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sub Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.HierarchyScope#getSubScope()
	 * @see #getHierarchyScope()
	 * @generated
	 */
	EReference getHierarchyScope_SubScope();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.HierarchyScope#getEquipment <em>Equipment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Equipment</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.HierarchyScope#getEquipment()
	 * @see #getHierarchyScope()
	 * @generated
	 */
	EReference getHierarchyScope_Equipment();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.HierarchyScope#getEquipmentElementLevel <em>Equipment Element Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Equipment Element Level</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.HierarchyScope#getEquipmentElementLevel()
	 * @see #getHierarchyScope()
	 * @generated
	 */
	EAttribute getHierarchyScope_EquipmentElementLevel();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecificationProperty <em>Material Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Specification Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecificationProperty
	 * @generated
	 */
	EClass getMaterialSpecificationProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecificationProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecificationProperty#getSubProperties()
	 * @see #getMaterialSpecificationProperty()
	 * @generated
	 */
	EReference getMaterialSpecificationProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecificationProperty <em>Physical Asset Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Specification Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecificationProperty
	 * @generated
	 */
	EClass getPhysicalAssetSpecificationProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecificationProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecificationProperty#getSubProperties()
	 * @see #getPhysicalAssetSpecificationProperty()
	 * @generated
	 */
	EReference getPhysicalAssetSpecificationProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecificationProperty <em>Equipment Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Specification Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSpecificationProperty
	 * @generated
	 */
	EClass getEquipmentSpecificationProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecificationProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSpecificationProperty#getSubProperties()
	 * @see #getEquipmentSpecificationProperty()
	 * @generated
	 */
	EReference getEquipmentSpecificationProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecificationProperty <em>Personnel Specification Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Specification Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSpecificationProperty
	 * @generated
	 */
	EClass getPersonnelSpecificationProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecificationProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSpecificationProperty#getSubProperties()
	 * @see #getPersonnelSpecificationProperty()
	 * @generated
	 */
	EReference getPersonnelSpecificationProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification <em>Material Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecification
	 * @generated
	 */
	EClass getMaterialSpecification();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getAssembledFromSpecifications <em>Assembled From Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assembled From Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getAssembledFromSpecifications()
	 * @see #getMaterialSpecification()
	 * @generated
	 */
	EReference getMaterialSpecification_AssembledFromSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialSpecificationProperties <em>Material Specification Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Specification Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialSpecificationProperties()
	 * @see #getMaterialSpecification()
	 * @generated
	 */
	EReference getMaterialSpecification_MaterialSpecificationProperties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialClasses <em>Material Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialClasses()
	 * @see #getMaterialSpecification()
	 * @generated
	 */
	EReference getMaterialSpecification_MaterialClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialDefinitions <em>Material Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Definitions</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialDefinitions()
	 * @see #getMaterialSpecification()
	 * @generated
	 */
	EReference getMaterialSpecification_MaterialDefinitions();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getDescription()
	 * @see #getMaterialSpecification()
	 * @generated
	 */
	EAttribute getMaterialSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialUse <em>Material Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Material Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getMaterialUse()
	 * @see #getMaterialSpecification()
	 * @generated
	 */
	EAttribute getMaterialSpecification_MaterialUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getQuantity()
	 * @see #getMaterialSpecification()
	 * @generated
	 */
	EAttribute getMaterialSpecification_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getQuantityUnitOfMeasure()
	 * @see #getMaterialSpecification()
	 * @generated
	 */
	EAttribute getMaterialSpecification_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getAssemblyType <em>Assembly Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getAssemblyType()
	 * @see #getMaterialSpecification()
	 * @generated
	 */
	EAttribute getMaterialSpecification_AssemblyType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getAssemblyRelationship <em>Assembly Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Relationship</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialSpecification#getAssemblyRelationship()
	 * @see #getMaterialSpecification()
	 * @generated
	 */
	EAttribute getMaterialSpecification_AssemblyRelationship();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification <em>Physical Asset Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification
	 * @generated
	 */
	EClass getPhysicalAssetSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicalAssetSpecificationProperties <em>Physical Asset Specification Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Asset Specification Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicalAssetSpecificationProperties()
	 * @see #getPhysicalAssetSpecification()
	 * @generated
	 */
	EReference getPhysicalAssetSpecification_PhysicalAssetSpecificationProperties();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getDescription()
	 * @see #getPhysicalAssetSpecification()
	 * @generated
	 */
	EAttribute getPhysicalAssetSpecification_Description();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicAlassetClasses <em>Physic Alasset Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Physic Alasset Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicAlassetClasses()
	 * @see #getPhysicalAssetSpecification()
	 * @generated
	 */
	EReference getPhysicalAssetSpecification_PhysicAlassetClasses();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicalAssets <em>Physical Assets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Physical Assets</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicalAssets()
	 * @see #getPhysicalAssetSpecification()
	 * @generated
	 */
	EReference getPhysicalAssetSpecification_PhysicalAssets();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicalAssetUse <em>Physical Asset Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Physical Asset Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getPhysicalAssetUse()
	 * @see #getPhysicalAssetSpecification()
	 * @generated
	 */
	EAttribute getPhysicalAssetSpecification_PhysicalAssetUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getQuantity()
	 * @see #getPhysicalAssetSpecification()
	 * @generated
	 */
	EAttribute getPhysicalAssetSpecification_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetSpecification#getQuantityUnitOfMeasure()
	 * @see #getPhysicalAssetSpecification()
	 * @generated
	 */
	EAttribute getPhysicalAssetSpecification_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecification <em>Equipment Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSpecification
	 * @generated
	 */
	EClass getEquipmentSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getEquipmentSpecificationProperties <em>Equipment Specification Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equipment Specification Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getEquipmentSpecificationProperties()
	 * @see #getEquipmentSpecification()
	 * @generated
	 */
	EReference getEquipmentSpecification_EquipmentSpecificationProperties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getEquipmentClasses <em>Equipment Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Equipment Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getEquipmentClasses()
	 * @see #getEquipmentSpecification()
	 * @generated
	 */
	EReference getEquipmentSpecification_EquipmentClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getEquipments <em>Equipments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Equipments</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getEquipments()
	 * @see #getEquipmentSpecification()
	 * @generated
	 */
	EReference getEquipmentSpecification_Equipments();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getDescription()
	 * @see #getEquipmentSpecification()
	 * @generated
	 */
	EAttribute getEquipmentSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getEquipmentUse <em>Equipment Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Equipment Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getEquipmentUse()
	 * @see #getEquipmentSpecification()
	 * @generated
	 */
	EAttribute getEquipmentSpecification_EquipmentUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getQuantity()
	 * @see #getEquipmentSpecification()
	 * @generated
	 */
	EAttribute getEquipmentSpecification_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentSpecification#getQuantityUnitOfMeasure()
	 * @see #getEquipmentSpecification()
	 * @generated
	 */
	EAttribute getEquipmentSpecification_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecification <em>Personnel Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSpecification
	 * @generated
	 */
	EClass getPersonnelSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getPersonnelSpecificationProperties <em>Personnel Specification Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Personnel Specification Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getPersonnelSpecificationProperties()
	 * @see #getPersonnelSpecification()
	 * @generated
	 */
	EReference getPersonnelSpecification_PersonnelSpecificationProperties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getPersonnelClasses <em>Personnel Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Personnel Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getPersonnelClasses()
	 * @see #getPersonnelSpecification()
	 * @generated
	 */
	EReference getPersonnelSpecification_PersonnelClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getPersons <em>Persons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Persons</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getPersons()
	 * @see #getPersonnelSpecification()
	 * @generated
	 */
	EReference getPersonnelSpecification_Persons();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getDescription()
	 * @see #getPersonnelSpecification()
	 * @generated
	 */
	EAttribute getPersonnelSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getPersonnelUse <em>Personnel Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Personnel Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getPersonnelUse()
	 * @see #getPersonnelSpecification()
	 * @generated
	 */
	EAttribute getPersonnelSpecification_PersonnelUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getQuantity()
	 * @see #getPersonnelSpecification()
	 * @generated
	 */
	EAttribute getPersonnelSpecification_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelSpecification#getQuantityUnitOfMeasure()
	 * @see #getPersonnelSpecification()
	 * @generated
	 */
	EAttribute getPersonnelSpecification_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ParameterSpecification <em>Parameter Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Specification</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ParameterSpecification
	 * @generated
	 */
	EClass getParameterSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ParameterSpecification#getSubParameters <em>Sub Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Parameters</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ParameterSpecification#getSubParameters()
	 * @see #getParameterSpecification()
	 * @generated
	 */
	EReference getParameterSpecification_SubParameters();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ParameterSpecification#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ParameterSpecification#getId()
	 * @see #getParameterSpecification()
	 * @generated
	 */
	EAttribute getParameterSpecification_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ParameterSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ParameterSpecification#getDescription()
	 * @see #getParameterSpecification()
	 * @generated
	 */
	EAttribute getParameterSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ParameterSpecification#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ParameterSpecification#getValue()
	 * @see #getParameterSpecification()
	 * @generated
	 */
	EAttribute getParameterSpecification_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ParameterSpecification#getValueUnitOfMeasurement <em>Value Unit Of Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measurement</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ParameterSpecification#getValueUnitOfMeasurement()
	 * @see #getParameterSpecification()
	 * @generated
	 */
	EAttribute getParameterSpecification_ValueUnitOfMeasurement();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency <em>Operations Segment Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Segment Dependency</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency
	 * @generated
	 */
	EClass getOperationsSegmentDependency();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getSubject <em>Subject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Subject</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getSubject()
	 * @see #getOperationsSegmentDependency()
	 * @generated
	 */
	EReference getOperationsSegmentDependency_Subject();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dependency</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDependency()
	 * @see #getOperationsSegmentDependency()
	 * @generated
	 */
	EReference getOperationsSegmentDependency_Dependency();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getId()
	 * @see #getOperationsSegmentDependency()
	 * @generated
	 */
	EAttribute getOperationsSegmentDependency_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDescription()
	 * @see #getOperationsSegmentDependency()
	 * @generated
	 */
	EAttribute getOperationsSegmentDependency_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDependencyType <em>Dependency Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dependency Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDependencyType()
	 * @see #getOperationsSegmentDependency()
	 * @generated
	 */
	EAttribute getOperationsSegmentDependency_DependencyType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDependencyFactor <em>Dependency Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dependency Factor</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getDependencyFactor()
	 * @see #getOperationsSegmentDependency()
	 * @generated
	 */
	EAttribute getOperationsSegmentDependency_DependencyFactor();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getUnitOfMeasure <em>Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegmentDependency#getUnitOfMeasure()
	 * @see #getOperationsSegmentDependency()
	 * @generated
	 */
	EAttribute getOperationsSegmentDependency_UnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem <em>Operations Material Bill Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Material Bill Item</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem
	 * @generated
	 */
	EClass getOperationsMaterialBillItem();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getMaterialSpecifications <em>Material Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getMaterialSpecifications()
	 * @see #getOperationsMaterialBillItem()
	 * @generated
	 */
	EReference getOperationsMaterialBillItem_MaterialSpecifications();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getAssembledFromItems <em>Assembled From Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assembled From Items</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getAssembledFromItems()
	 * @see #getOperationsMaterialBillItem()
	 * @generated
	 */
	EReference getOperationsMaterialBillItem_AssembledFromItems();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getId()
	 * @see #getOperationsMaterialBillItem()
	 * @generated
	 */
	EAttribute getOperationsMaterialBillItem_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getDescription()
	 * @see #getOperationsMaterialBillItem()
	 * @generated
	 */
	EAttribute getOperationsMaterialBillItem_Description();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getMaterialClasses <em>Material Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getMaterialClasses()
	 * @see #getOperationsMaterialBillItem()
	 * @generated
	 */
	EReference getOperationsMaterialBillItem_MaterialClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getMaterialDefinitions <em>Material Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Definitions</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getMaterialDefinitions()
	 * @see #getOperationsMaterialBillItem()
	 * @generated
	 */
	EReference getOperationsMaterialBillItem_MaterialDefinitions();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getUseType <em>Use Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Use Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getUseType()
	 * @see #getOperationsMaterialBillItem()
	 * @generated
	 */
	EAttribute getOperationsMaterialBillItem_UseType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getAssemblyType <em>Assembly Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getAssemblyType()
	 * @see #getOperationsMaterialBillItem()
	 * @generated
	 */
	EAttribute getOperationsMaterialBillItem_AssemblyType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getAssemblyRelationship <em>Assembly Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Relationship</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getAssemblyRelationship()
	 * @see #getOperationsMaterialBillItem()
	 * @generated
	 */
	EAttribute getOperationsMaterialBillItem_AssemblyRelationship();

	/**
	 * Returns the meta object for the attribute list '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getQuantities <em>Quantities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Quantities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getQuantities()
	 * @see #getOperationsMaterialBillItem()
	 * @generated
	 */
	EAttribute getOperationsMaterialBillItem_Quantities();

	/**
	 * Returns the meta object for the attribute list '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getQuantityUnitOfMeasures <em>Quantity Unit Of Measures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Quantity Unit Of Measures</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBillItem#getQuantityUnitOfMeasures()
	 * @see #getOperationsMaterialBillItem()
	 * @generated
	 */
	EAttribute getOperationsMaterialBillItem_QuantityUnitOfMeasures();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill <em>Operations Material Bill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Material Bill</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill
	 * @generated
	 */
	EClass getOperationsMaterialBill();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill#getOperationsMaterialBillItems <em>Operations Material Bill Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations Material Bill Items</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill#getOperationsMaterialBillItems()
	 * @see #getOperationsMaterialBill()
	 * @generated
	 */
	EReference getOperationsMaterialBill_OperationsMaterialBillItems();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill#getId()
	 * @see #getOperationsMaterialBill()
	 * @generated
	 */
	EAttribute getOperationsMaterialBill_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsMaterialBill#getDescription()
	 * @see #getOperationsMaterialBill()
	 * @generated
	 */
	EAttribute getOperationsMaterialBill_Description();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment <em>Operations Segment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Segment</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment
	 * @generated
	 */
	EClass getOperationsSegment();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getProcessSegments <em>Process Segments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Process Segments</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getProcessSegments()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EReference getOperationsSegment_ProcessSegments();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getSubSegments <em>Sub Segments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Segments</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getSubSegments()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EReference getOperationsSegment_SubSegments();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getParameterSpecifications <em>Parameter Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getParameterSpecifications()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EReference getOperationsSegment_ParameterSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getPersonnelSpecifications <em>Personnel Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Personnel Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getPersonnelSpecifications()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EReference getOperationsSegment_PersonnelSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getEquipmentSpecifications <em>Equipment Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equipment Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getEquipmentSpecifications()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EReference getOperationsSegment_EquipmentSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getPhysicalAssetSpecifications <em>Physical Asset Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Asset Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getPhysicalAssetSpecifications()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EReference getOperationsSegment_PhysicalAssetSpecifications();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getMaterialSpecifications <em>Material Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Specifications</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getMaterialSpecifications()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EReference getOperationsSegment_MaterialSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getId()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EAttribute getOperationsSegment_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getDescription()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EAttribute getOperationsSegment_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getDuration()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EAttribute getOperationsSegment_Duration();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getDurationUnitOfMeasure <em>Duration Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getDurationUnitOfMeasure()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EAttribute getOperationsSegment_DurationUnitOfMeasure();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getOperationsType <em>Operations Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operations Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getOperationsType()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EAttribute getOperationsSegment_OperationsType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSegment#getWorkDefinitionId <em>Work Definition Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Work Definition Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSegment#getWorkDefinitionId()
	 * @see #getOperationsSegment()
	 * @generated
	 */
	EAttribute getOperationsSegment_WorkDefinitionId();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition <em>Operations Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Definition</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinition
	 * @generated
	 */
	EClass getOperationsDefinition();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getOperationsSegments <em>Operations Segments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Operations Segments</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getOperationsSegments()
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	EReference getOperationsDefinition_OperationsSegments();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getOperationsMaterialBills <em>Operations Material Bills</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations Material Bills</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getOperationsMaterialBills()
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	EReference getOperationsDefinition_OperationsMaterialBills();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getId()
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	EAttribute getOperationsDefinition_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getVersion()
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	EAttribute getOperationsDefinition_Version();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getDescription()
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	EAttribute getOperationsDefinition_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getOperationsType <em>Operations Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operations Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getOperationsType()
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	EAttribute getOperationsDefinition_OperationsType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getBillOfMaterialId <em>Bill Of Material Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bill Of Material Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getBillOfMaterialId()
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	EAttribute getOperationsDefinition_BillOfMaterialId();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getWorkDefinitionId <em>Work Definition Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Work Definition Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getWorkDefinitionId()
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	EAttribute getOperationsDefinition_WorkDefinitionId();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getBillOfResourceId <em>Bill Of Resource Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bill Of Resource Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getBillOfResourceId()
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	EAttribute getOperationsDefinition_BillOfResourceId();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getHierarchysScope <em>Hierarchys Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchys Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsDefinition#getHierarchysScope()
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	EReference getOperationsDefinition_HierarchysScope();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement <em>Material Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Requirement</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement
	 * @generated
	 */
	EClass getMaterialRequirement();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialRequirementProperties <em>Material Requirement Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Requirement Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialRequirementProperties()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EReference getMaterialRequirement_MaterialRequirementProperties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getAssembledFromRequirements <em>Assembled From Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assembled From Requirements</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getAssembledFromRequirements()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EReference getMaterialRequirement_AssembledFromRequirements();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialClasses <em>Material Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialClasses()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EReference getMaterialRequirement_MaterialClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialDefinitions <em>Material Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Definitions</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialDefinitions()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EReference getMaterialRequirement_MaterialDefinitions();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialLots <em>Material Lots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Lots</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialLots()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EReference getMaterialRequirement_MaterialLots();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialSublot <em>Material Sublot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Sublot</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialSublot()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EReference getMaterialRequirement_MaterialSublot();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getDescription()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EAttribute getMaterialRequirement_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialUse <em>Material Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Material Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getMaterialUse()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EAttribute getMaterialRequirement_MaterialUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getStorageLocation <em>Storage Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Storage Location</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getStorageLocation()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EAttribute getMaterialRequirement_StorageLocation();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getQuantity()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EAttribute getMaterialRequirement_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getQuantityUnitOfMeasure()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EAttribute getMaterialRequirement_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getAssemblyType <em>Assembly Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getAssemblyType()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EAttribute getMaterialRequirement_AssemblyType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getAssemblyRelationship <em>Assembly Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Relationship</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirement#getAssemblyRelationship()
	 * @see #getMaterialRequirement()
	 * @generated
	 */
	EAttribute getMaterialRequirement_AssemblyRelationship();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirementProperty <em>Material Requirement Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Requirement Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirementProperty
	 * @generated
	 */
	EClass getMaterialRequirementProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialRequirementProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialRequirementProperty#getSubProperties()
	 * @see #getMaterialRequirementProperty()
	 * @generated
	 */
	EReference getMaterialRequirementProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirementProperty <em>Physical Asset Requirement Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Requirement Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirementProperty
	 * @generated
	 */
	EClass getPhysicalAssetRequirementProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirementProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirementProperty#getSubProperties()
	 * @see #getPhysicalAssetRequirementProperty()
	 * @generated
	 */
	EReference getPhysicalAssetRequirementProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirementProperty <em>Equipment Requirement Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Requirement Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirementProperty
	 * @generated
	 */
	EClass getEquipmentRequirementProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirementProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirementProperty#getSubProperties()
	 * @see #getEquipmentRequirementProperty()
	 * @generated
	 */
	EReference getEquipmentRequirementProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirementProperty <em>Personnel Requirement Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Requirement Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelRequirementProperty
	 * @generated
	 */
	EClass getPersonnelRequirementProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirementProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelRequirementProperty#getSubProperties()
	 * @see #getPersonnelRequirementProperty()
	 * @generated
	 */
	EReference getPersonnelRequirementProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule <em>Operations Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Schedule</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSchedule
	 * @generated
	 */
	EClass getOperationsSchedule();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getOperationsRequests <em>Operations Requests</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations Requests</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getOperationsRequests()
	 * @see #getOperationsSchedule()
	 * @generated
	 */
	EReference getOperationsSchedule_OperationsRequests();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getId()
	 * @see #getOperationsSchedule()
	 * @generated
	 */
	EAttribute getOperationsSchedule_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getDescription()
	 * @see #getOperationsSchedule()
	 * @generated
	 */
	EAttribute getOperationsSchedule_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getOperationsType <em>Operations Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operations Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getOperationsType()
	 * @see #getOperationsSchedule()
	 * @generated
	 */
	EAttribute getOperationsSchedule_OperationsType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getStartTime()
	 * @see #getOperationsSchedule()
	 * @generated
	 */
	EAttribute getOperationsSchedule_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getEndTime()
	 * @see #getOperationsSchedule()
	 * @generated
	 */
	EAttribute getOperationsSchedule_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getPublishedDate <em>Published Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Published Date</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getPublishedDate()
	 * @see #getOperationsSchedule()
	 * @generated
	 */
	EAttribute getOperationsSchedule_PublishedDate();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getScheduledState <em>Scheduled State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scheduled State</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getScheduledState()
	 * @see #getOperationsSchedule()
	 * @generated
	 */
	EAttribute getOperationsSchedule_ScheduledState();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getHierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsSchedule#getHierarchyScope()
	 * @see #getOperationsSchedule()
	 * @generated
	 */
	EReference getOperationsSchedule_HierarchyScope();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest <em>Operations Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Request</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest
	 * @generated
	 */
	EClass getOperationsRequest();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getSegmentRequirements <em>Segment Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Segment Requirements</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest#getSegmentRequirements()
	 * @see #getOperationsRequest()
	 * @generated
	 */
	EReference getOperationsRequest_SegmentRequirements();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getRequestedSegmentResponses <em>Requested Segment Responses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Requested Segment Responses</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest#getRequestedSegmentResponses()
	 * @see #getOperationsRequest()
	 * @generated
	 */
	EReference getOperationsRequest_RequestedSegmentResponses();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getOperationsDefinition <em>Operations Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operations Definition</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest#getOperationsDefinition()
	 * @see #getOperationsRequest()
	 * @generated
	 */
	EReference getOperationsRequest_OperationsDefinition();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest#getId()
	 * @see #getOperationsRequest()
	 * @generated
	 */
	EAttribute getOperationsRequest_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest#getDescription()
	 * @see #getOperationsRequest()
	 * @generated
	 */
	EAttribute getOperationsRequest_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getOperationsType <em>Operations Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operations Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest#getOperationsType()
	 * @see #getOperationsRequest()
	 * @generated
	 */
	EAttribute getOperationsRequest_OperationsType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest#getStartTime()
	 * @see #getOperationsRequest()
	 * @generated
	 */
	EAttribute getOperationsRequest_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest#getEndTime()
	 * @see #getOperationsRequest()
	 * @generated
	 */
	EAttribute getOperationsRequest_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getPriority <em>Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Priority</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest#getPriority()
	 * @see #getOperationsRequest()
	 * @generated
	 */
	EAttribute getOperationsRequest_Priority();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getRequestState <em>Request State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Request State</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest#getRequestState()
	 * @see #getOperationsRequest()
	 * @generated
	 */
	EAttribute getOperationsRequest_RequestState();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsRequest#getHierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsRequest#getHierarchyScope()
	 * @see #getOperationsRequest()
	 * @generated
	 */
	EReference getOperationsRequest_HierarchyScope();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.RequestedSegmentResponse <em>Requested Segment Response</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requested Segment Response</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.RequestedSegmentResponse
	 * @generated
	 */
	EClass getRequestedSegmentResponse();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement <em>Segment Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Segment Requirement</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement
	 * @generated
	 */
	EClass getSegmentRequirement();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getSubRequirements <em>Sub Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Requirements</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getSubRequirements()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EReference getSegmentRequirement_SubRequirements();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getProcessSegment <em>Process Segment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Process Segment</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getProcessSegment()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EReference getSegmentRequirement_ProcessSegment();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getOperationsDefinition <em>Operations Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operations Definition</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getOperationsDefinition()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EReference getSegmentRequirement_OperationsDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getSegmentParameters <em>Segment Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Segment Parameters</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getSegmentParameters()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EReference getSegmentRequirement_SegmentParameters();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getPersonnelRequirements <em>Personnel Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Personnel Requirements</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getPersonnelRequirements()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EReference getSegmentRequirement_PersonnelRequirements();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getEquipmentRequirements <em>Equipment Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equipment Requirements</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getEquipmentRequirements()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EReference getSegmentRequirement_EquipmentRequirements();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getPhysicalAssetRequirements <em>Physical Asset Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Asset Requirements</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getPhysicalAssetRequirements()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EReference getSegmentRequirement_PhysicalAssetRequirements();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getMaterialRequirements <em>Material Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Requirements</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getMaterialRequirements()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EReference getSegmentRequirement_MaterialRequirements();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getId()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EAttribute getSegmentRequirement_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getDescription()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EAttribute getSegmentRequirement_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getOperationsType <em>Operations Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operations Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getOperationsType()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EAttribute getSegmentRequirement_OperationsType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getEarliestStartTime <em>Earliest Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Earliest Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getEarliestStartTime()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EAttribute getSegmentRequirement_EarliestStartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getLatestStartTime <em>Latest Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Latest Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getLatestStartTime()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EAttribute getSegmentRequirement_LatestStartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getDuration()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EAttribute getSegmentRequirement_Duration();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getDurationUnitOfMeasurement <em>Duration Unit Of Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration Unit Of Measurement</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getDurationUnitOfMeasurement()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EAttribute getSegmentRequirement_DurationUnitOfMeasurement();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getSegmentState <em>Segment State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Segment State</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getSegmentState()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EAttribute getSegmentRequirement_SegmentState();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getHierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentRequirement#getHierarchyScope()
	 * @see #getSegmentRequirement()
	 * @generated
	 */
	EReference getSegmentRequirement_HierarchyScope();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirement <em>Equipment Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Requirement</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirement
	 * @generated
	 */
	EClass getEquipmentRequirement();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getEquipmentRequirementProperties <em>Equipment Requirement Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equipment Requirement Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getEquipmentRequirementProperties()
	 * @see #getEquipmentRequirement()
	 * @generated
	 */
	EReference getEquipmentRequirement_EquipmentRequirementProperties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getEquipmentClasses <em>Equipment Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Equipment Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getEquipmentClasses()
	 * @see #getEquipmentRequirement()
	 * @generated
	 */
	EReference getEquipmentRequirement_EquipmentClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getEquipments <em>Equipments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Equipments</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getEquipments()
	 * @see #getEquipmentRequirement()
	 * @generated
	 */
	EReference getEquipmentRequirement_Equipments();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getDescription()
	 * @see #getEquipmentRequirement()
	 * @generated
	 */
	EAttribute getEquipmentRequirement_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getEquipmentUse <em>Equipment Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Equipment Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getEquipmentUse()
	 * @see #getEquipmentRequirement()
	 * @generated
	 */
	EAttribute getEquipmentRequirement_EquipmentUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getQuantity()
	 * @see #getEquipmentRequirement()
	 * @generated
	 */
	EAttribute getEquipmentRequirement_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getQuantityUnitOfMeasure()
	 * @see #getEquipmentRequirement()
	 * @generated
	 */
	EAttribute getEquipmentRequirement_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getEquipmentLevel <em>Equipment Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Equipment Level</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentRequirement#getEquipmentLevel()
	 * @see #getEquipmentRequirement()
	 * @generated
	 */
	EAttribute getEquipmentRequirement_EquipmentLevel();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement <em>Physical Asset Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Requirement</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement
	 * @generated
	 */
	EClass getPhysicalAssetRequirement();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getPhysicalAssetRequirementProperties <em>Physical Asset Requirement Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Asset Requirement Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getPhysicalAssetRequirementProperties()
	 * @see #getPhysicalAssetRequirement()
	 * @generated
	 */
	EReference getPhysicalAssetRequirement_PhysicalAssetRequirementProperties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getPhysicalAssetClasses <em>Physical Asset Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Physical Asset Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getPhysicalAssetClasses()
	 * @see #getPhysicalAssetRequirement()
	 * @generated
	 */
	EReference getPhysicalAssetRequirement_PhysicalAssetClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getPhysicalAssets <em>Physical Assets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Physical Assets</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getPhysicalAssets()
	 * @see #getPhysicalAssetRequirement()
	 * @generated
	 */
	EReference getPhysicalAssetRequirement_PhysicalAssets();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getDescription()
	 * @see #getPhysicalAssetRequirement()
	 * @generated
	 */
	EAttribute getPhysicalAssetRequirement_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getPhysicalAssetUse <em>Physical Asset Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Physical Asset Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getPhysicalAssetUse()
	 * @see #getPhysicalAssetRequirement()
	 * @generated
	 */
	EAttribute getPhysicalAssetRequirement_PhysicalAssetUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getQuantity()
	 * @see #getPhysicalAssetRequirement()
	 * @generated
	 */
	EAttribute getPhysicalAssetRequirement_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getQuantityUnitOfMeasure()
	 * @see #getPhysicalAssetRequirement()
	 * @generated
	 */
	EAttribute getPhysicalAssetRequirement_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getEquipmentLevel <em>Equipment Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Equipment Level</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement#getEquipmentLevel()
	 * @see #getPhysicalAssetRequirement()
	 * @generated
	 */
	EAttribute getPhysicalAssetRequirement_EquipmentLevel();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement <em>Personnel Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Requirement</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelRequirement
	 * @generated
	 */
	EClass getPersonnelRequirement();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersonnelRequirementProperties <em>Personnel Requirement Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Personnel Requirement Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersonnelRequirementProperties()
	 * @see #getPersonnelRequirement()
	 * @generated
	 */
	EReference getPersonnelRequirement_PersonnelRequirementProperties();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersonnelClasses <em>Personnel Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Personnel Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersonnelClasses()
	 * @see #getPersonnelRequirement()
	 * @generated
	 */
	EReference getPersonnelRequirement_PersonnelClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersons <em>Persons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Persons</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersons()
	 * @see #getPersonnelRequirement()
	 * @generated
	 */
	EReference getPersonnelRequirement_Persons();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getDescription()
	 * @see #getPersonnelRequirement()
	 * @generated
	 */
	EAttribute getPersonnelRequirement_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersonnelUse <em>Personnel Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Personnel Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getPersonnelUse()
	 * @see #getPersonnelRequirement()
	 * @generated
	 */
	EAttribute getPersonnelRequirement_PersonnelUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getQuantity()
	 * @see #getPersonnelRequirement()
	 * @generated
	 */
	EAttribute getPersonnelRequirement_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelRequirement#getQuantityUnitOfMeasure()
	 * @see #getPersonnelRequirement()
	 * @generated
	 */
	EAttribute getPersonnelRequirement_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.SegmentParameter <em>Segment Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Segment Parameter</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentParameter
	 * @generated
	 */
	EClass getSegmentParameter();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentParameter#getSubParameters <em>Sub Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Parameters</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentParameter#getSubParameters()
	 * @see #getSegmentParameter()
	 * @generated
	 */
	EReference getSegmentParameter_SubParameters();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentParameter#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentParameter#getId()
	 * @see #getSegmentParameter()
	 * @generated
	 */
	EAttribute getSegmentParameter_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentParameter#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentParameter#getDescription()
	 * @see #getSegmentParameter()
	 * @generated
	 */
	EAttribute getSegmentParameter_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentParameter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentParameter#getValue()
	 * @see #getSegmentParameter()
	 * @generated
	 */
	EAttribute getSegmentParameter_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentParameter#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentParameter#getValueUnitOfMeasure()
	 * @see #getSegmentParameter()
	 * @generated
	 */
	EAttribute getSegmentParameter_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialActualProperty <em>Material Actual Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Actual Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActualProperty
	 * @generated
	 */
	EClass getMaterialActualProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialActualProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActualProperty#getSubProperties()
	 * @see #getMaterialActualProperty()
	 * @generated
	 */
	EReference getMaterialActualProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActualProperty <em>Physical Asset Actual Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Actual Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetActualProperty
	 * @generated
	 */
	EClass getPhysicalAssetActualProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActualProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetActualProperty#getSubProperties()
	 * @see #getPhysicalAssetActualProperty()
	 * @generated
	 */
	EReference getPhysicalAssetActualProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentActualProperty <em>Equipment Actual Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Actual Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentActualProperty
	 * @generated
	 */
	EClass getEquipmentActualProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentActualProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentActualProperty#getSubProperties()
	 * @see #getEquipmentActualProperty()
	 * @generated
	 */
	EReference getEquipmentActualProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelActualProperty <em>Personnel Actual Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Actual Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelActualProperty
	 * @generated
	 */
	EClass getPersonnelActualProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelActualProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelActualProperty#getSubProperties()
	 * @see #getPersonnelActualProperty()
	 * @generated
	 */
	EReference getPersonnelActualProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual <em>Material Actual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Actual</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual
	 * @generated
	 */
	EClass getMaterialActual();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getAssembledFromActuals <em>Assembled From Actuals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assembled From Actuals</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getAssembledFromActuals()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EReference getMaterialActual_AssembledFromActuals();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getMaterialActualProperties <em>Material Actual Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Actual Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getMaterialActualProperties()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EReference getMaterialActual_MaterialActualProperties();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getDescription()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EAttribute getMaterialActual_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getMaterialUse <em>Material Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Material Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getMaterialUse()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EAttribute getMaterialActual_MaterialUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getStorageLocation <em>Storage Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Storage Location</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getStorageLocation()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EAttribute getMaterialActual_StorageLocation();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getQuantity()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EAttribute getMaterialActual_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getQuantityUnitOfMeasure()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EAttribute getMaterialActual_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getAssemblyType <em>Assembly Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getAssemblyType()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EAttribute getMaterialActual_AssemblyType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getAssemblyRelationship <em>Assembly Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Relationship</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getAssemblyRelationship()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EAttribute getMaterialActual_AssemblyRelationship();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getMaterialClasses <em>Material Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getMaterialClasses()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EReference getMaterialActual_MaterialClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getMaterialDefinitions <em>Material Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Definitions</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getMaterialDefinitions()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EReference getMaterialActual_MaterialDefinitions();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getMaterialLots <em>Material Lots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Lots</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getMaterialLots()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EReference getMaterialActual_MaterialLots();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialActual#getMaterialSublots <em>Material Sublots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Material Sublots</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialActual#getMaterialSublots()
	 * @see #getMaterialActual()
	 * @generated
	 */
	EReference getMaterialActual_MaterialSublots();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual <em>Physical Asset Actual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Actual</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual
	 * @generated
	 */
	EClass getPhysicalAssetActual();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getPhysicalAssetActualProperties <em>Physical Asset Actual Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Asset Actual Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getPhysicalAssetActualProperties()
	 * @see #getPhysicalAssetActual()
	 * @generated
	 */
	EReference getPhysicalAssetActual_PhysicalAssetActualProperties();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getDescription()
	 * @see #getPhysicalAssetActual()
	 * @generated
	 */
	EAttribute getPhysicalAssetActual_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getPhysicalAssetUse <em>Physical Asset Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Physical Asset Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getPhysicalAssetUse()
	 * @see #getPhysicalAssetActual()
	 * @generated
	 */
	EAttribute getPhysicalAssetActual_PhysicalAssetUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getQuantity()
	 * @see #getPhysicalAssetActual()
	 * @generated
	 */
	EAttribute getPhysicalAssetActual_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getQuantityUnitOfMeasure()
	 * @see #getPhysicalAssetActual()
	 * @generated
	 */
	EAttribute getPhysicalAssetActual_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getPhysicalAssetClasses <em>Physical Asset Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Physical Asset Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getPhysicalAssetClasses()
	 * @see #getPhysicalAssetActual()
	 * @generated
	 */
	EReference getPhysicalAssetActual_PhysicalAssetClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getPhysicalAssets <em>Physical Assets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Physical Assets</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetActual#getPhysicalAssets()
	 * @see #getPhysicalAssetActual()
	 * @generated
	 */
	EReference getPhysicalAssetActual_PhysicalAssets();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentActual <em>Equipment Actual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Actual</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentActual
	 * @generated
	 */
	EClass getEquipmentActual();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentActual#getEquipmentActualProperties <em>Equipment Actual Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equipment Actual Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentActual#getEquipmentActualProperties()
	 * @see #getEquipmentActual()
	 * @generated
	 */
	EReference getEquipmentActual_EquipmentActualProperties();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentActual#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentActual#getDescription()
	 * @see #getEquipmentActual()
	 * @generated
	 */
	EAttribute getEquipmentActual_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentActual#getEquipmentUse <em>Equipment Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Equipment Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentActual#getEquipmentUse()
	 * @see #getEquipmentActual()
	 * @generated
	 */
	EAttribute getEquipmentActual_EquipmentUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentActual#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentActual#getQuantity()
	 * @see #getEquipmentActual()
	 * @generated
	 */
	EAttribute getEquipmentActual_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentActual#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentActual#getQuantityUnitOfMeasure()
	 * @see #getEquipmentActual()
	 * @generated
	 */
	EAttribute getEquipmentActual_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentActual#getEquipmentClasses <em>Equipment Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Equipment Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentActual#getEquipmentClasses()
	 * @see #getEquipmentActual()
	 * @generated
	 */
	EReference getEquipmentActual_EquipmentClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentActual#getEquipments <em>Equipments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Equipments</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentActual#getEquipments()
	 * @see #getEquipmentActual()
	 * @generated
	 */
	EReference getEquipmentActual_Equipments();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelActual <em>Personnel Actual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Actual</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelActual
	 * @generated
	 */
	EClass getPersonnelActual();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelActual#getPersonnelActualProperties <em>Personnel Actual Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Personnel Actual Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelActual#getPersonnelActualProperties()
	 * @see #getPersonnelActual()
	 * @generated
	 */
	EReference getPersonnelActual_PersonnelActualProperties();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelActual#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelActual#getDescription()
	 * @see #getPersonnelActual()
	 * @generated
	 */
	EAttribute getPersonnelActual_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelActual#getPersonnelUse <em>Personnel Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Personnel Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelActual#getPersonnelUse()
	 * @see #getPersonnelActual()
	 * @generated
	 */
	EAttribute getPersonnelActual_PersonnelUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelActual#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelActual#getQuantity()
	 * @see #getPersonnelActual()
	 * @generated
	 */
	EAttribute getPersonnelActual_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelActual#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelActual#getQuantityUnitOfMeasure()
	 * @see #getPersonnelActual()
	 * @generated
	 */
	EAttribute getPersonnelActual_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelActual#getPersonnelClasses <em>Personnel Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Personnel Classes</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelActual#getPersonnelClasses()
	 * @see #getPersonnelActual()
	 * @generated
	 */
	EReference getPersonnelActual_PersonnelClasses();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelActual#getPersons <em>Persons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Persons</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelActual#getPersons()
	 * @see #getPersonnelActual()
	 * @generated
	 */
	EReference getPersonnelActual_Persons();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.SegmentData <em>Segment Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Segment Data</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentData
	 * @generated
	 */
	EClass getSegmentData();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentData#getSubData <em>Sub Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Data</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentData#getSubData()
	 * @see #getSegmentData()
	 * @generated
	 */
	EReference getSegmentData_SubData();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentData#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentData#getId()
	 * @see #getSegmentData()
	 * @generated
	 */
	EAttribute getSegmentData_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentData#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentData#getDescription()
	 * @see #getSegmentData()
	 * @generated
	 */
	EAttribute getSegmentData_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentData#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentData#getValue()
	 * @see #getSegmentData()
	 * @generated
	 */
	EAttribute getSegmentData_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentData#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentData#getValueUnitOfMeasure()
	 * @see #getSegmentData()
	 * @generated
	 */
	EAttribute getSegmentData_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse <em>Segment Response</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Segment Response</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse
	 * @generated
	 */
	EClass getSegmentResponse();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getSubResponses <em>Sub Responses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Responses</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getSubResponses()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EReference getSegmentResponse_SubResponses();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getProcessSegment <em>Process Segment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Process Segment</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getProcessSegment()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EReference getSegmentResponse_ProcessSegment();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getOperationsDefinition <em>Operations Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operations Definition</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getOperationsDefinition()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EReference getSegmentResponse_OperationsDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getSegmentData <em>Segment Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Segment Data</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getSegmentData()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EReference getSegmentResponse_SegmentData();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getPersonnelActuals <em>Personnel Actuals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Personnel Actuals</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getPersonnelActuals()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EReference getSegmentResponse_PersonnelActuals();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getEquipmentActuals <em>Equipment Actuals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Equipment Actuals</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getEquipmentActuals()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EReference getSegmentResponse_EquipmentActuals();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getPhysicalAssetActuals <em>Physical Asset Actuals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Asset Actuals</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getPhysicalAssetActuals()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EReference getSegmentResponse_PhysicalAssetActuals();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getMaterialActuals <em>Material Actuals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Actuals</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getMaterialActuals()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EReference getSegmentResponse_MaterialActuals();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getId()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EAttribute getSegmentResponse_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getDescription()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EAttribute getSegmentResponse_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getOperationsType <em>Operations Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operations Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getOperationsType()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EAttribute getSegmentResponse_OperationsType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getActualStartTime <em>Actual Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Actual Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getActualStartTime()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EAttribute getSegmentResponse_ActualStartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getActualEndTime <em>Actual End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Actual End Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getActualEndTime()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EAttribute getSegmentResponse_ActualEndTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getSegmentState <em>Segment State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Segment State</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getSegmentState()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EAttribute getSegmentResponse_SegmentState();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.SegmentResponse#getHierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.SegmentResponse#getHierarchyScope()
	 * @see #getSegmentResponse()
	 * @generated
	 */
	EReference getSegmentResponse_HierarchyScope();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsResponse <em>Operations Response</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Response</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsResponse
	 * @generated
	 */
	EClass getOperationsResponse();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsResponse#getSegmentResponses <em>Segment Responses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Segment Responses</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsResponse#getSegmentResponses()
	 * @see #getOperationsResponse()
	 * @generated
	 */
	EReference getOperationsResponse_SegmentResponses();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsResponse#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsResponse#getId()
	 * @see #getOperationsResponse()
	 * @generated
	 */
	EAttribute getOperationsResponse_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsResponse#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsResponse#getDescription()
	 * @see #getOperationsResponse()
	 * @generated
	 */
	EAttribute getOperationsResponse_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsResponse#getOperationsType <em>Operations Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operations Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsResponse#getOperationsType()
	 * @see #getOperationsResponse()
	 * @generated
	 */
	EAttribute getOperationsResponse_OperationsType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsResponse#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsResponse#getStartTime()
	 * @see #getOperationsResponse()
	 * @generated
	 */
	EAttribute getOperationsResponse_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsResponse#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsResponse#getEndTime()
	 * @see #getOperationsResponse()
	 * @generated
	 */
	EAttribute getOperationsResponse_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsResponse#getResponseState <em>Response State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Response State</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsResponse#getResponseState()
	 * @see #getOperationsResponse()
	 * @generated
	 */
	EAttribute getOperationsResponse_ResponseState();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsResponse#getOperationsRequest <em>Operations Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operations Request</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsResponse#getOperationsRequest()
	 * @see #getOperationsResponse()
	 * @generated
	 */
	EReference getOperationsResponse_OperationsRequest();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsResponse#getHierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsResponse#getHierarchyScope()
	 * @see #getOperationsResponse()
	 * @generated
	 */
	EReference getOperationsResponse_HierarchyScope();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsResponse#getOperationsDefinition <em>Operations Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operations Definition</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsResponse#getOperationsDefinition()
	 * @see #getOperationsResponse()
	 * @generated
	 */
	EReference getOperationsResponse_OperationsDefinition();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance <em>Operations Performance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Performance</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformance
	 * @generated
	 */
	EClass getOperationsPerformance();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getOperationsResponses <em>Operations Responses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations Responses</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getOperationsResponses()
	 * @see #getOperationsPerformance()
	 * @generated
	 */
	EReference getOperationsPerformance_OperationsResponses();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getId()
	 * @see #getOperationsPerformance()
	 * @generated
	 */
	EAttribute getOperationsPerformance_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getDescription()
	 * @see #getOperationsPerformance()
	 * @generated
	 */
	EAttribute getOperationsPerformance_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getOperationsType <em>Operations Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operations Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getOperationsType()
	 * @see #getOperationsPerformance()
	 * @generated
	 */
	EAttribute getOperationsPerformance_OperationsType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getStartTime()
	 * @see #getOperationsPerformance()
	 * @generated
	 */
	EAttribute getOperationsPerformance_StartTime();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getOperationsSchedule <em>Operations Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operations Schedule</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getOperationsSchedule()
	 * @see #getOperationsPerformance()
	 * @generated
	 */
	EReference getOperationsPerformance_OperationsSchedule();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getEndTime()
	 * @see #getOperationsPerformance()
	 * @generated
	 */
	EAttribute getOperationsPerformance_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getPerformanceState <em>Performance State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Performance State</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getPerformanceState()
	 * @see #getOperationsPerformance()
	 * @generated
	 */
	EAttribute getOperationsPerformance_PerformanceState();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getHierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getHierarchyScope()
	 * @see #getOperationsPerformance()
	 * @generated
	 */
	EReference getOperationsPerformance_HierarchyScope();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getPublishedDate <em>Published Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Published Date</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsPerformance#getPublishedDate()
	 * @see #getOperationsPerformance()
	 * @generated
	 */
	EAttribute getOperationsPerformance_PublishedDate();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability <em>Operations Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operations Capability</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability
	 * @generated
	 */
	EClass getOperationsCapability();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getProcessSegmentCapabilities <em>Process Segment Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Process Segment Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getProcessSegmentCapabilities()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EReference getOperationsCapability_ProcessSegmentCapabilities();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getPersonnelCapabilities <em>Personnel Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Personnel Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getPersonnelCapabilities()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EReference getOperationsCapability_PersonnelCapabilities();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getEquipmentCapabilities <em>Equipment Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Equipment Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getEquipmentCapabilities()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EReference getOperationsCapability_EquipmentCapabilities();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getPhysicalAssetCapabilities <em>Physical Asset Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Physical Asset Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getPhysicalAssetCapabilities()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EReference getOperationsCapability_PhysicalAssetCapabilities();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getMaterialCapabilities <em>Material Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Material Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getMaterialCapabilities()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EReference getOperationsCapability_MaterialCapabilities();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getId()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EAttribute getOperationsCapability_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getDescription()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EAttribute getOperationsCapability_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getCapacityType <em>Capacity Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Capacity Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getCapacityType()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EAttribute getOperationsCapability_CapacityType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getReason <em>Reason</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reason</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getReason()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EAttribute getOperationsCapability_Reason();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getConfidenceFactor <em>Confidence Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Confidence Factor</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getConfidenceFactor()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EAttribute getOperationsCapability_ConfidenceFactor();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getStartTime()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EAttribute getOperationsCapability_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getEndTime()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EAttribute getOperationsCapability_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getPublishedDate <em>Published Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Published Date</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getPublishedDate()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EAttribute getOperationsCapability_PublishedDate();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.OperationsCapability#getHierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsCapability#getHierarchyScope()
	 * @see #getOperationsCapability()
	 * @generated
	 */
	EReference getOperationsCapability_HierarchyScope();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapabilityProperty <em>Material Capability Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Capability Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapabilityProperty
	 * @generated
	 */
	EClass getMaterialCapabilityProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapabilityProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapabilityProperty#getSubProperties()
	 * @see #getMaterialCapabilityProperty()
	 * @generated
	 */
	EReference getMaterialCapabilityProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityProperty <em>Physical Asset Capability Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Capability Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityProperty
	 * @generated
	 */
	EClass getPhysicalAssetCapabilityProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapabilityProperty#getSubProperties()
	 * @see #getPhysicalAssetCapabilityProperty()
	 * @generated
	 */
	EReference getPhysicalAssetCapabilityProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityProperty <em>Equipment Capability Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Capability Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityProperty
	 * @generated
	 */
	EClass getEquipmentCapabilityProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityProperty#getSubProperties()
	 * @see #getEquipmentCapabilityProperty()
	 * @generated
	 */
	EReference getEquipmentCapabilityProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapabilityProperty <em>Personnel Capability Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Capability Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapabilityProperty
	 * @generated
	 */
	EClass getPersonnelCapabilityProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapabilityProperty#getSubProperties <em>Sub Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Properties</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapabilityProperty#getSubProperties()
	 * @see #getPersonnelCapabilityProperty()
	 * @generated
	 */
	EReference getPersonnelCapabilityProperty_SubProperties();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability <em>Material Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Capability</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability
	 * @generated
	 */
	EClass getMaterialCapability();

	/**
	 * Returns the meta object for the reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getAssembledFromCapabilities <em>Assembled From Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Assembled From Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getAssembledFromCapabilities()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EReference getMaterialCapability_AssembledFromCapabilities();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getMaterialCapabilityProperty <em>Material Capability Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Capability Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getMaterialCapabilityProperty()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EReference getMaterialCapability_MaterialCapabilityProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getMaterialClass <em>Material Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Material Class</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getMaterialClass()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EReference getMaterialCapability_MaterialClass();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getMaterialDefinition <em>Material Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Material Definition</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getMaterialDefinition()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EReference getMaterialCapability_MaterialDefinition();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getMaterialLot <em>Material Lot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Material Lot</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getMaterialLot()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EReference getMaterialCapability_MaterialLot();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getMaterialSublot <em>Material Sublot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Material Sublot</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getMaterialSublot()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EReference getMaterialCapability_MaterialSublot();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getDescription()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EAttribute getMaterialCapability_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getCapabilityType <em>Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Capability Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getCapabilityType()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EAttribute getMaterialCapability_CapabilityType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getReason <em>Reason</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reason</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getReason()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EAttribute getMaterialCapability_Reason();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getConfidenceFactor <em>Confidence Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Confidence Factor</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getConfidenceFactor()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EAttribute getMaterialCapability_ConfidenceFactor();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getMaterialUse <em>Material Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Material Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getMaterialUse()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EAttribute getMaterialCapability_MaterialUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getStartTime()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EAttribute getMaterialCapability_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getEndTime()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EAttribute getMaterialCapability_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getQuantity()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EAttribute getMaterialCapability_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getQuantityUnitOfMeasure()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EAttribute getMaterialCapability_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getHierarchyscope <em>Hierarchyscope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchyscope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getHierarchyscope()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EReference getMaterialCapability_Hierarchyscope();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getAssemblyType <em>Assembly Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getAssemblyType()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EAttribute getMaterialCapability_AssemblyType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.MaterialCapability#getAssemblyRelationship <em>Assembly Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assembly Relationship</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialCapability#getAssemblyRelationship()
	 * @see #getMaterialCapability()
	 * @generated
	 */
	EAttribute getMaterialCapability_AssemblyRelationship();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability <em>Physical Asset Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Asset Capability</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability
	 * @generated
	 */
	EClass getPhysicalAssetCapability();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAssetCapabilityProperty <em>Physical Asset Capability Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Asset Capability Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAssetCapabilityProperty()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EReference getPhysicalAssetCapability_PhysicalAssetCapabilityProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAssetClass <em>Physical Asset Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Physical Asset Class</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAssetClass()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EReference getPhysicalAssetCapability_PhysicalAssetClass();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAsset <em>Physical Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Physical Asset</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAsset()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EReference getPhysicalAssetCapability_PhysicalAsset();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getDescription()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapability_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getCapabilityType <em>Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Capability Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getCapabilityType()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapability_CapabilityType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getReason <em>Reason</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reason</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getReason()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapability_Reason();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getConfidenceFactor <em>Confidence Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Confidence Factor</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getConfidenceFactor()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapability_ConfidenceFactor();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAssetUse <em>Physical Asset Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Physical Asset Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getPhysicalAssetUse()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapability_PhysicalAssetUse();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getHierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getHierarchyScope()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EReference getPhysicalAssetCapability_HierarchyScope();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getStartTime()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapability_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getEndTime()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapability_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getQuantity()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapability_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PhysicalAssetCapability#getQuantityUnitOfMeasure()
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 */
	EAttribute getPhysicalAssetCapability_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability <em>Equipment Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equipment Capability</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability
	 * @generated
	 */
	EClass getEquipmentCapability();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getEquipmentCapabilityProperty <em>Equipment Capability Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equipment Capability Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getEquipmentCapabilityProperty()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EReference getEquipmentCapability_EquipmentCapabilityProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getEquipmentClass <em>Equipment Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Equipment Class</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getEquipmentClass()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EReference getEquipmentCapability_EquipmentClass();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getEquipment <em>Equipment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Equipment</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getEquipment()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EReference getEquipmentCapability_Equipment();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getDescription()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EAttribute getEquipmentCapability_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getCapabilityType <em>Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Capability Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getCapabilityType()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EAttribute getEquipmentCapability_CapabilityType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getReason <em>Reason</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reason</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getReason()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EAttribute getEquipmentCapability_Reason();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getConfidenceFactor <em>Confidence Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Confidence Factor</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getConfidenceFactor()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EAttribute getEquipmentCapability_ConfidenceFactor();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getEquipmentUse <em>Equipment Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Equipment Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getEquipmentUse()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EAttribute getEquipmentCapability_EquipmentUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getStartTime()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EAttribute getEquipmentCapability_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getEndTime()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EAttribute getEquipmentCapability_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getQuantity()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EAttribute getEquipmentCapability_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getQuantityUnitOfMeasure()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EAttribute getEquipmentCapability_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getHierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.EquipmentCapability#getHierarchyScope()
	 * @see #getEquipmentCapability()
	 * @generated
	 */
	EReference getEquipmentCapability_HierarchyScope();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability <em>Personnel Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Personnel Capability</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability
	 * @generated
	 */
	EClass getPersonnelCapability();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getPersonnelCapabilityProperty <em>Personnel Capability Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Personnel Capability Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getPersonnelCapabilityProperty()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EReference getPersonnelCapability_PersonnelCapabilityProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getPersonnelClass <em>Personnel Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Personnel Class</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getPersonnelClass()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EReference getPersonnelCapability_PersonnelClass();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getPerson <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Person</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getPerson()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EReference getPersonnelCapability_Person();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getDescription()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EAttribute getPersonnelCapability_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getCapabilityType <em>Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Capability Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getCapabilityType()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EAttribute getPersonnelCapability_CapabilityType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getReason <em>Reason</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reason</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getReason()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EAttribute getPersonnelCapability_Reason();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getConfidenceFactor <em>Confidence Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Confidence Factor</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getConfidenceFactor()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EAttribute getPersonnelCapability_ConfidenceFactor();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getHierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getHierarchyScope()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EReference getPersonnelCapability_HierarchyScope();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getPersonnelUse <em>Personnel Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Personnel Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getPersonnelUse()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EAttribute getPersonnelCapability_PersonnelUse();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getStartTime()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EAttribute getPersonnelCapability_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getEndTime()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EAttribute getPersonnelCapability_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getQuantity()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EAttribute getPersonnelCapability_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PersonnelCapability#getQuantityUnitOfMeasure()
	 * @see #getPersonnelCapability()
	 * @generated
	 */
	EAttribute getPersonnelCapability_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability <em>Process Segment Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process Segment Capability</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability
	 * @generated
	 */
	EClass getProcessSegmentCapability();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getSubCapabilities <em>Sub Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getSubCapabilities()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EReference getProcessSegmentCapability_SubCapabilities();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getPersonnelCapabilities <em>Personnel Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Personnel Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getPersonnelCapabilities()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EReference getProcessSegmentCapability_PersonnelCapabilities();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getEquipmentCapabilities <em>Equipment Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Equipment Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getEquipmentCapabilities()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EReference getProcessSegmentCapability_EquipmentCapabilities();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getPhysicalAssetCapabilities <em>Physical Asset Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Asset Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getPhysicalAssetCapabilities()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EReference getProcessSegmentCapability_PhysicalAssetCapabilities();

	/**
	 * Returns the meta object for the containment reference list '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getMaterialCapabilities <em>Material Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Material Capabilities</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getMaterialCapabilities()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EReference getProcessSegmentCapability_MaterialCapabilities();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getProcessSegment <em>Process Segment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Process Segment</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getProcessSegment()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EReference getProcessSegmentCapability_ProcessSegment();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getId()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EAttribute getProcessSegmentCapability_Id();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getDescription()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EAttribute getProcessSegmentCapability_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getCapacityType <em>Capacity Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Capacity Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getCapacityType()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EAttribute getProcessSegmentCapability_CapacityType();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getReason <em>Reason</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reason</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getReason()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EAttribute getProcessSegmentCapability_Reason();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getStartTime()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EAttribute getProcessSegmentCapability_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getEndTime()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EAttribute getProcessSegmentCapability_EndTime();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getHierarchyScope <em>Hierarchy Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hierarchy Scope</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ProcessSegmentCapability#getHierarchyScope()
	 * @see #getProcessSegmentCapability()
	 * @generated
	 */
	EReference getProcessSegmentCapability_HierarchyScope();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty <em>Referencial Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Referencial Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferencialProperty
	 * @generated
	 */
	EClass getReferencialProperty();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getDescription()
	 * @see #getReferencialProperty()
	 * @generated
	 */
	EAttribute getReferencialProperty_Description();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getValue()
	 * @see #getReferencialProperty()
	 * @generated
	 */
	EAttribute getReferencialProperty_Value();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getValueUnitOfMeasure()
	 * @see #getReferencialProperty()
	 * @generated
	 */
	EAttribute getReferencialProperty_ValueUnitOfMeasure();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getQuantity()
	 * @see #getReferencialProperty()
	 * @generated
	 */
	EAttribute getReferencialProperty_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity Unit Of Measure</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferencialProperty#getQuantityUnitOfMeasure()
	 * @see #getReferencialProperty()
	 * @generated
	 */
	EAttribute getReferencialProperty_QuantityUnitOfMeasure();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty <em>Referential Personnel Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Referential Personnel Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty
	 * @generated
	 */
	EClass getReferentialPersonnelProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty#getPersonnelClassProperty <em>Personnel Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Personnel Class Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty#getPersonnelClassProperty()
	 * @see #getReferentialPersonnelProperty()
	 * @generated
	 */
	EReference getReferentialPersonnelProperty_PersonnelClassProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty#getPersonProperty <em>Person Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Person Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialPersonnelProperty#getPersonProperty()
	 * @see #getReferentialPersonnelProperty()
	 * @generated
	 */
	EReference getReferentialPersonnelProperty_PersonProperty();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty <em>Referential Equipment Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Referential Equipment Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty
	 * @generated
	 */
	EClass getReferentialEquipmentProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty#getEquipmentClassProperty <em>Equipment Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Equipment Class Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty#getEquipmentClassProperty()
	 * @see #getReferentialEquipmentProperty()
	 * @generated
	 */
	EReference getReferentialEquipmentProperty_EquipmentClassProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty#getEquipmentProperty <em>Equipment Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Equipment Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialEquipmentProperty#getEquipmentProperty()
	 * @see #getReferentialEquipmentProperty()
	 * @generated
	 */
	EReference getReferentialEquipmentProperty_EquipmentProperty();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty <em>Referential Physical Asset Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Referential Physical Asset Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty
	 * @generated
	 */
	EClass getReferentialPhysicalAssetProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty#getPhysicalAssetClassProperty <em>Physical Asset Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Physical Asset Class Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty#getPhysicalAssetClassProperty()
	 * @see #getReferentialPhysicalAssetProperty()
	 * @generated
	 */
	EReference getReferentialPhysicalAssetProperty_PhysicalAssetClassProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty#getPhysicalAssetProperty <em>Physical Asset Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Physical Asset Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialPhysicalAssetProperty#getPhysicalAssetProperty()
	 * @see #getReferentialPhysicalAssetProperty()
	 * @generated
	 */
	EReference getReferentialPhysicalAssetProperty_PhysicalAssetProperty();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty <em>Referential Material Type Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Referential Material Type Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty
	 * @generated
	 */
	EClass getReferentialMaterialTypeProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty#getMaterialClassProperty <em>Material Class Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Material Class Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty#getMaterialClassProperty()
	 * @see #getReferentialMaterialTypeProperty()
	 * @generated
	 */
	EReference getReferentialMaterialTypeProperty_MaterialClassProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty#getMaterialDefinitionProperty <em>Material Definition Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Material Definition Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty#getMaterialDefinitionProperty()
	 * @see #getReferentialMaterialTypeProperty()
	 * @generated
	 */
	EReference getReferentialMaterialTypeProperty_MaterialDefinitionProperty();

	/**
	 * Returns the meta object for class '{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialProperty <em>Referential Material Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Referential Material Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialMaterialProperty
	 * @generated
	 */
	EClass getReferentialMaterialProperty();

	/**
	 * Returns the meta object for the reference '{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialProperty#getMaterialLotProperty <em>Material Lot Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Material Lot Property</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ReferentialMaterialProperty#getMaterialLotProperty()
	 * @see #getReferentialMaterialProperty()
	 * @generated
	 */
	EReference getReferentialMaterialProperty_MaterialLotProperty();

	/**
	 * Returns the meta object for enum '{@link at.ac.tuwien.big.ame.iec62264.AssemblyType <em>Assembly Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Assembly Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyType
	 * @generated
	 */
	EEnum getAssemblyType();

	/**
	 * Returns the meta object for enum '{@link at.ac.tuwien.big.ame.iec62264.AssemblyRelationship <em>Assembly Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Assembly Relationship</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.AssemblyRelationship
	 * @generated
	 */
	EEnum getAssemblyRelationship();

	/**
	 * Returns the meta object for enum '{@link at.ac.tuwien.big.ame.iec62264.OperationsType <em>Operations Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Operations Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
	 * @generated
	 */
	EEnum getOperationsType();

	/**
	 * Returns the meta object for enum '{@link at.ac.tuwien.big.ame.iec62264.MaterialUse <em>Material Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Material Use</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.MaterialUse
	 * @generated
	 */
	EEnum getMaterialUse();

	/**
	 * Returns the meta object for enum '{@link at.ac.tuwien.big.ame.iec62264.UseType <em>Use Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Use Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.UseType
	 * @generated
	 */
	EEnum getUseType();

	/**
	 * Returns the meta object for enum '{@link at.ac.tuwien.big.ame.iec62264.ScheduleState <em>Schedule State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Schedule State</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.ScheduleState
	 * @generated
	 */
	EEnum getScheduleState();

	/**
	 * Returns the meta object for enum '{@link at.ac.tuwien.big.ame.iec62264.PerformanceState <em>Performance State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Performance State</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.PerformanceState
	 * @generated
	 */
	EEnum getPerformanceState();

	/**
	 * Returns the meta object for enum '{@link at.ac.tuwien.big.ame.iec62264.CapabilityType <em>Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Capability Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.CapabilityType
	 * @generated
	 */
	EEnum getCapabilityType();

	/**
	 * Returns the meta object for enum '{@link at.ac.tuwien.big.ame.iec62264.CapacityType <em>Capacity Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Capacity Type</em>'.
	 * @see at.ac.tuwien.big.ame.iec62264.CapacityType
	 * @generated
	 */
	EEnum getCapacityType();

	/**
	 * Returns the meta object for data type '{@link java.time.ZonedDateTime <em>Zoned Date Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Zoned Date Time</em>'.
	 * @see java.time.ZonedDateTime
	 * @model instanceClass="java.time.ZonedDateTime"
	 * @generated
	 */
	EDataType getZonedDateTime();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Iec62264Factory getIec62264Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonImpl <em>Person</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPerson()
		 * @generated
		 */
		EClass PERSON = eINSTANCE.getPerson();

		/**
		 * The meta object literal for the '<em><b>Personnel Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON__PERSONNEL_CLASSES = eINSTANCE.getPerson_PersonnelClasses();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON__PROPERTIES = eINSTANCE.getPerson_Properties();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON__TEST_SPECIFICATIONS = eINSTANCE.getPerson_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__ID = eINSTANCE.getPerson_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__DESCRIPTION = eINSTANCE.getPerson_Description();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__NAME = eINSTANCE.getPerson_Name();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassImpl <em>Personnel Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelClass()
		 * @generated
		 */
		EClass PERSONNEL_CLASS = eINSTANCE.getPersonnelClass();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_CLASS__PROPERTIES = eINSTANCE.getPersonnelClass_Properties();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_CLASS__TEST_SPECIFICATIONS = eINSTANCE.getPersonnelClass_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CLASS__ID = eINSTANCE.getPersonnelClass_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CLASS__DESCRIPTION = eINSTANCE.getPersonnelClass_Description();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.QualificationTestSpecificationImpl <em>Qualification Test Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.QualificationTestSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getQualificationTestSpecification()
		 * @generated
		 */
		EClass QUALIFICATION_TEST_SPECIFICATION = eINSTANCE.getQualificationTestSpecification();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUALIFICATION_TEST_SPECIFICATION__ID = eINSTANCE.getQualificationTestSpecification_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUALIFICATION_TEST_SPECIFICATION__DESCRIPTION = eINSTANCE
				.getQualificationTestSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUALIFICATION_TEST_SPECIFICATION__VERSION = eINSTANCE.getQualificationTestSpecification_Version();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassPropertyImpl <em>Personnel Class Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelClassPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelClassProperty()
		 * @generated
		 */
		EClass PERSONNEL_CLASS_PROPERTY = eINSTANCE.getPersonnelClassProperty();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_CLASS_PROPERTY__TEST_SPECIFICATIONS = eINSTANCE
				.getPersonnelClassProperty_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_CLASS_PROPERTY__SUB_PROPERTIES = eINSTANCE.getPersonnelClassProperty_SubProperties();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CLASS_PROPERTY__ID = eINSTANCE.getPersonnelClassProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CLASS_PROPERTY__DESCRIPTION = eINSTANCE.getPersonnelClassProperty_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CLASS_PROPERTY__VALUE = eINSTANCE.getPersonnelClassProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE = eINSTANCE
				.getPersonnelClassProperty_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonPropertyImpl <em>Person Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonProperty()
		 * @generated
		 */
		EClass PERSON_PROPERTY = eINSTANCE.getPersonProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON_PROPERTY__SUB_PROPERTIES = eINSTANCE.getPersonProperty_SubProperties();

		/**
		 * The meta object literal for the '<em><b>Personnel Class Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON_PROPERTY__PERSONNEL_CLASS_PROPERTY = eINSTANCE.getPersonProperty_PersonnelClassProperty();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON_PROPERTY__ID = eINSTANCE.getPersonProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON_PROPERTY__DESCRIPTION = eINSTANCE.getPersonProperty_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON_PROPERTY__VALUE = eINSTANCE.getPersonProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON_PROPERTY__VALUE_UNIT_OF_MEASURE = eINSTANCE.getPersonProperty_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.QualificationTestResultImpl <em>Qualification Test Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.QualificationTestResultImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getQualificationTestResult()
		 * @generated
		 */
		EClass QUALIFICATION_TEST_RESULT = eINSTANCE.getQualificationTestResult();

		/**
		 * The meta object literal for the '<em><b>Test Specification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUALIFICATION_TEST_RESULT__TEST_SPECIFICATION = eINSTANCE
				.getQualificationTestResult_TestSpecification();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUALIFICATION_TEST_RESULT__PROPERTY = eINSTANCE.getQualificationTestResult_Property();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUALIFICATION_TEST_RESULT__ID = eINSTANCE.getQualificationTestResult_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUALIFICATION_TEST_RESULT__DESCRIPTION = eINSTANCE.getQualificationTestResult_Description();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUALIFICATION_TEST_RESULT__DATE = eINSTANCE.getQualificationTestResult_Date();

		/**
		 * The meta object literal for the '<em><b>Result</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUALIFICATION_TEST_RESULT__RESULT = eINSTANCE.getQualificationTestResult_Result();

		/**
		 * The meta object literal for the '<em><b>Result Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUALIFICATION_TEST_RESULT__RESULT_UNIT_OF_MEASURE = eINSTANCE
				.getQualificationTestResult_ResultUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Expiration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUALIFICATION_TEST_RESULT__EXPIRATION = eINSTANCE.getQualificationTestResult_Expiration();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelModelImpl <em>Personnel Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelModelImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelModel()
		 * @generated
		 */
		EClass PERSONNEL_MODEL = eINSTANCE.getPersonnelModel();

		/**
		 * The meta object literal for the '<em><b>Personnel Classes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_MODEL__PERSONNEL_CLASSES = eINSTANCE.getPersonnelModel_PersonnelClasses();

		/**
		 * The meta object literal for the '<em><b>Persons</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_MODEL__PERSONS = eINSTANCE.getPersonnelModel_Persons();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_MODEL__TEST_SPECIFICATIONS = eINSTANCE.getPersonnelModel_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Test Results</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_MODEL__TEST_RESULTS = eINSTANCE.getPersonnelModel_TestResults();

		/**
		 * The meta object literal for the '<em><b>Operations Capability Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_MODEL__OPERATIONS_CAPABILITY_MODEL = eINSTANCE
				.getPersonnelModel_OperationsCapabilityModel();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentModelImpl <em>Equipment Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentModelImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentModel()
		 * @generated
		 */
		EClass EQUIPMENT_MODEL = eINSTANCE.getEquipmentModel();

		/**
		 * The meta object literal for the '<em><b>Equipment Classes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_MODEL__EQUIPMENT_CLASSES = eINSTANCE.getEquipmentModel_EquipmentClasses();

		/**
		 * The meta object literal for the '<em><b>Equipments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_MODEL__EQUIPMENTS = eINSTANCE.getEquipmentModel_Equipments();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_MODEL__TEST_SPECIFICATIONS = eINSTANCE.getEquipmentModel_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Test Results</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_MODEL__TEST_RESULTS = eINSTANCE.getEquipmentModel_TestResults();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentClassImpl <em>Equipment Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentClassImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentClass()
		 * @generated
		 */
		EClass EQUIPMENT_CLASS = eINSTANCE.getEquipmentClass();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_CLASS__PROPERTIES = eINSTANCE.getEquipmentClass_Properties();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_CLASS__TEST_SPECIFICATIONS = eINSTANCE.getEquipmentClass_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CLASS__ID = eINSTANCE.getEquipmentClass_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CLASS__DESCRIPTION = eINSTANCE.getEquipmentClass_Description();

		/**
		 * The meta object literal for the '<em><b>Equipment Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CLASS__EQUIPMENT_LEVEL = eINSTANCE.getEquipmentClass_EquipmentLevel();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityTestResultImpl <em>Equipment Capability Test Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityTestResultImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentCapabilityTestResult()
		 * @generated
		 */
		EClass EQUIPMENT_CAPABILITY_TEST_RESULT = eINSTANCE.getEquipmentCapabilityTestResult();

		/**
		 * The meta object literal for the '<em><b>Test Specification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_CAPABILITY_TEST_RESULT__TEST_SPECIFICATION = eINSTANCE
				.getEquipmentCapabilityTestResult_TestSpecification();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_CAPABILITY_TEST_RESULT__PROPERTY = eINSTANCE.getEquipmentCapabilityTestResult_Property();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY_TEST_RESULT__ID = eINSTANCE.getEquipmentCapabilityTestResult_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY_TEST_RESULT__DESCRIPTION = eINSTANCE
				.getEquipmentCapabilityTestResult_Description();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY_TEST_RESULT__DATE = eINSTANCE.getEquipmentCapabilityTestResult_Date();

		/**
		 * The meta object literal for the '<em><b>Result</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY_TEST_RESULT__RESULT = eINSTANCE.getEquipmentCapabilityTestResult_Result();

		/**
		 * The meta object literal for the '<em><b>Result Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY_TEST_RESULT__RESULT_UNIT_OF_MEASURE = eINSTANCE
				.getEquipmentCapabilityTestResult_ResultUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Expiration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY_TEST_RESULT__EXPIRATION = eINSTANCE
				.getEquipmentCapabilityTestResult_Expiration();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityTestSpecificationImpl <em>Equipment Capability Test Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityTestSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentCapabilityTestSpecification()
		 * @generated
		 */
		EClass EQUIPMENT_CAPABILITY_TEST_SPECIFICATION = eINSTANCE.getEquipmentCapabilityTestSpecification();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY_TEST_SPECIFICATION__ID = eINSTANCE.getEquipmentCapabilityTestSpecification_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY_TEST_SPECIFICATION__DESCRIPTION = eINSTANCE
				.getEquipmentCapabilityTestSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY_TEST_SPECIFICATION__VERSION = eINSTANCE
				.getEquipmentCapabilityTestSpecification_Version();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentClassPropertyImpl <em>Equipment Class Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentClassPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentClassProperty()
		 * @generated
		 */
		EClass EQUIPMENT_CLASS_PROPERTY = eINSTANCE.getEquipmentClassProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_CLASS_PROPERTY__SUB_PROPERTIES = eINSTANCE.getEquipmentClassProperty_SubProperties();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_CLASS_PROPERTY__TEST_SPECIFICATIONS = eINSTANCE
				.getEquipmentClassProperty_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CLASS_PROPERTY__ID = eINSTANCE.getEquipmentClassProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CLASS_PROPERTY__DESCRIPTION = eINSTANCE.getEquipmentClassProperty_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CLASS_PROPERTY__VALUE = eINSTANCE.getEquipmentClassProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE = eINSTANCE
				.getEquipmentClassProperty_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentPropertyImpl <em>Equipment Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentProperty()
		 * @generated
		 */
		EClass EQUIPMENT_PROPERTY = eINSTANCE.getEquipmentProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_PROPERTY__SUB_PROPERTIES = eINSTANCE.getEquipmentProperty_SubProperties();

		/**
		 * The meta object literal for the '<em><b>Equipment Class Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY = eINSTANCE
				.getEquipmentProperty_EquipmentClassProperty();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_PROPERTY__ID = eINSTANCE.getEquipmentProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_PROPERTY__DESCRIPTION = eINSTANCE.getEquipmentProperty_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_PROPERTY__VALUE = eINSTANCE.getEquipmentProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_PROPERTY__VALUE_UNIT_OF_MEASURE = eINSTANCE.getEquipmentProperty_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentImpl <em>Equipment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipment()
		 * @generated
		 */
		EClass EQUIPMENT = eINSTANCE.getEquipment();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT__CHILDREN = eINSTANCE.getEquipment_Children();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT__PROPERTIES = eINSTANCE.getEquipment_Properties();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT__TEST_SPECIFICATIONS = eINSTANCE.getEquipment_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Equipment Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT__EQUIPMENT_CLASS = eINSTANCE.getEquipment_EquipmentClass();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT__ID = eINSTANCE.getEquipment_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT__DESCRIPTION = eINSTANCE.getEquipment_Description();

		/**
		 * The meta object literal for the '<em><b>Equipment Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT__EQUIPMENT_LEVEL = eINSTANCE.getEquipment_EquipmentLevel();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetModelImpl <em>Physical Asset Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetModelImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetModel()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_MODEL = eINSTANCE.getPhysicalAssetModel();

		/**
		 * The meta object literal for the '<em><b>Physical Assets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_MODEL__PHYSICAL_ASSETS = eINSTANCE.getPhysicalAssetModel_PhysicalAssets();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Classes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_MODEL__PHYSICAL_ASSET_CLASSES = eINSTANCE
				.getPhysicalAssetModel_PhysicalAssetClasses();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_MODEL__TEST_SPECIFICATIONS = eINSTANCE.getPhysicalAssetModel_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Equipment Asset Mappings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_MODEL__EQUIPMENT_ASSET_MAPPINGS = eINSTANCE
				.getPhysicalAssetModel_EquipmentAssetMappings();

		/**
		 * The meta object literal for the '<em><b>Test Results</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_MODEL__TEST_RESULTS = eINSTANCE.getPhysicalAssetModel_TestResults();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetClassImpl <em>Physical Asset Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetClassImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetClass()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_CLASS = eINSTANCE.getPhysicalAssetClass();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_CLASS__PROPERTIES = eINSTANCE.getPhysicalAssetClass_Properties();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_CLASS__TEST_SPECIFICATIONS = eINSTANCE.getPhysicalAssetClass_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CLASS__ID = eINSTANCE.getPhysicalAssetClass_Id();

		/**
		 * The meta object literal for the '<em><b>Manufacturer</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CLASS__MANUFACTURER = eINSTANCE.getPhysicalAssetClass_Manufacturer();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CLASS__DESCRIPTION = eINSTANCE.getPhysicalAssetClass_Description();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestResultImpl <em>Physical Asset Capability Test Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestResultImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetCapabilityTestResult()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_CAPABILITY_TEST_RESULT = eINSTANCE.getPhysicalAssetCapabilityTestResult();

		/**
		 * The meta object literal for the '<em><b>Test Specification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__TEST_SPECIFICATION = eINSTANCE
				.getPhysicalAssetCapabilityTestResult_TestSpecification();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__PROPERTY = eINSTANCE
				.getPhysicalAssetCapabilityTestResult_Property();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__ID = eINSTANCE.getPhysicalAssetCapabilityTestResult_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DESCRIPTION = eINSTANCE
				.getPhysicalAssetCapabilityTestResult_Description();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__DATE = eINSTANCE.getPhysicalAssetCapabilityTestResult_Date();

		/**
		 * The meta object literal for the '<em><b>Result</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT = eINSTANCE
				.getPhysicalAssetCapabilityTestResult_Result();

		/**
		 * The meta object literal for the '<em><b>Result Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__RESULT_UNIT_OF_MEASURE = eINSTANCE
				.getPhysicalAssetCapabilityTestResult_ResultUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Expiration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY_TEST_RESULT__EXPIRATION = eINSTANCE
				.getPhysicalAssetCapabilityTestResult_Expiration();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetPropertyImpl <em>Physical Asset Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetProperty()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_PROPERTY = eINSTANCE.getPhysicalAssetProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_PROPERTY__SUB_PROPERTIES = eINSTANCE.getPhysicalAssetProperty_SubProperties();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_PROPERTY__ID = eINSTANCE.getPhysicalAssetProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_PROPERTY__DESCRIPTION = eINSTANCE.getPhysicalAssetProperty_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_PROPERTY__VALUE = eINSTANCE.getPhysicalAssetProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_PROPERTY__VALUE_UNIT_OF_MEASURE = eINSTANCE
				.getPhysicalAssetProperty_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Class Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY = eINSTANCE
				.getPhysicalAssetProperty_PhysicalAssetClassProperty();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl <em>Physical Asset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAsset()
		 * @generated
		 */
		EClass PHYSICAL_ASSET = eINSTANCE.getPhysicalAsset();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET__CHILDREN = eINSTANCE.getPhysicalAsset_Children();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET__PROPERTIES = eINSTANCE.getPhysicalAsset_Properties();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET__PHYSICAL_ASSET_CLASS = eINSTANCE.getPhysicalAsset_PhysicalAssetClass();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET__TEST_SPECIFICATIONS = eINSTANCE.getPhysicalAsset_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET__ID = eINSTANCE.getPhysicalAsset_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET__DESCRIPTION = eINSTANCE.getPhysicalAsset_Description();

		/**
		 * The meta object literal for the '<em><b>Physical Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET__PHYSICAL_LOCATION = eINSTANCE.getPhysicalAsset_PhysicalLocation();

		/**
		 * The meta object literal for the '<em><b>Fixed Asset Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET__FIXED_ASSET_ID = eINSTANCE.getPhysicalAsset_FixedAssetId();

		/**
		 * The meta object literal for the '<em><b>Vendor Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET__VENDOR_ID = eINSTANCE.getPhysicalAsset_VendorId();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestSpecificationImpl <em>Physical Asset Capability Test Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityTestSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetCapabilityTestSpecification()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION = eINSTANCE.getPhysicalAssetCapabilityTestSpecification();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION__ID = eINSTANCE
				.getPhysicalAssetCapabilityTestSpecification_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION__DESCRIPTION = eINSTANCE
				.getPhysicalAssetCapabilityTestSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION__VERSION = eINSTANCE
				.getPhysicalAssetCapabilityTestSpecification_Version();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetClassPropertyImpl <em>Physical Asset Class Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetClassPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetClassProperty()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_CLASS_PROPERTY = eINSTANCE.getPhysicalAssetClassProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_CLASS_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getPhysicalAssetClassProperty_SubProperties();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_CLASS_PROPERTY__TEST_SPECIFICATIONS = eINSTANCE
				.getPhysicalAssetClassProperty_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CLASS_PROPERTY__ID = eINSTANCE.getPhysicalAssetClassProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CLASS_PROPERTY__DESCRIPTION = eINSTANCE.getPhysicalAssetClassProperty_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CLASS_PROPERTY__VALUE = eINSTANCE.getPhysicalAssetClassProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE = eINSTANCE
				.getPhysicalAssetClassProperty_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentAssetMappingImpl <em>Equipment Asset Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentAssetMappingImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentAssetMapping()
		 * @generated
		 */
		EClass EQUIPMENT_ASSET_MAPPING = eINSTANCE.getEquipmentAssetMapping();

		/**
		 * The meta object literal for the '<em><b>Physical Asset</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_ASSET_MAPPING__PHYSICAL_ASSET = eINSTANCE.getEquipmentAssetMapping_PhysicalAsset();

		/**
		 * The meta object literal for the '<em><b>Equipment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_ASSET_MAPPING__EQUIPMENT = eINSTANCE.getEquipmentAssetMapping_Equipment();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_ASSET_MAPPING__ID = eINSTANCE.getEquipmentAssetMapping_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_ASSET_MAPPING__DESCRIPTION = eINSTANCE.getEquipmentAssetMapping_Description();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_ASSET_MAPPING__START_TIME = eINSTANCE.getEquipmentAssetMapping_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_ASSET_MAPPING__END_TIME = eINSTANCE.getEquipmentAssetMapping_EndTime();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialModelImpl <em>Material Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialModelImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialModel()
		 * @generated
		 */
		EClass MATERIAL_MODEL = eINSTANCE.getMaterialModel();

		/**
		 * The meta object literal for the '<em><b>Material Classes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_MODEL__MATERIAL_CLASSES = eINSTANCE.getMaterialModel_MaterialClasses();

		/**
		 * The meta object literal for the '<em><b>Material Definitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_MODEL__MATERIAL_DEFINITIONS = eINSTANCE.getMaterialModel_MaterialDefinitions();

		/**
		 * The meta object literal for the '<em><b>Material Lots</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_MODEL__MATERIAL_LOTS = eINSTANCE.getMaterialModel_MaterialLots();

		/**
		 * The meta object literal for the '<em><b>Material Sublots</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_MODEL__MATERIAL_SUBLOTS = eINSTANCE.getMaterialModel_MaterialSublots();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_MODEL__TEST_SPECIFICATIONS = eINSTANCE.getMaterialModel_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Test Results</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_MODEL__TEST_RESULTS = eINSTANCE.getMaterialModel_TestResults();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialClassImpl <em>Material Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialClassImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialClass()
		 * @generated
		 */
		EClass MATERIAL_CLASS = eINSTANCE.getMaterialClass();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CLASS__PROPERTIES = eINSTANCE.getMaterialClass_Properties();

		/**
		 * The meta object literal for the '<em><b>Assembled From Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CLASS__ASSEMBLED_FROM_CLASSES = eINSTANCE.getMaterialClass_AssembledFromClasses();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CLASS__TEST_SPECIFICATIONS = eINSTANCE.getMaterialClass_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CLASS__ID = eINSTANCE.getMaterialClass_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CLASS__DESCRIPTION = eINSTANCE.getMaterialClass_Description();

		/**
		 * The meta object literal for the '<em><b>Assembly Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CLASS__ASSEMBLY_TYPE = eINSTANCE.getMaterialClass_AssemblyType();

		/**
		 * The meta object literal for the '<em><b>Assembly Relationship</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CLASS__ASSEMBLY_RELATIONSHIP = eINSTANCE.getMaterialClass_AssemblyRelationship();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialTestResultImpl <em>Material Test Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialTestResultImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialTestResult()
		 * @generated
		 */
		EClass MATERIAL_TEST_RESULT = eINSTANCE.getMaterialTestResult();

		/**
		 * The meta object literal for the '<em><b>Test Specification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_TEST_RESULT__TEST_SPECIFICATION = eINSTANCE.getMaterialTestResult_TestSpecification();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_TEST_RESULT__PROPERTY = eINSTANCE.getMaterialTestResult_Property();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_TEST_RESULT__ID = eINSTANCE.getMaterialTestResult_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_TEST_RESULT__DESCRIPTION = eINSTANCE.getMaterialTestResult_Description();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_TEST_RESULT__DATE = eINSTANCE.getMaterialTestResult_Date();

		/**
		 * The meta object literal for the '<em><b>Result</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_TEST_RESULT__RESULT = eINSTANCE.getMaterialTestResult_Result();

		/**
		 * The meta object literal for the '<em><b>Result Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_TEST_RESULT__RESULT_UNIT_OF_MEASURE = eINSTANCE.getMaterialTestResult_ResultUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Expiration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_TEST_RESULT__EXPIRATION = eINSTANCE.getMaterialTestResult_Expiration();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialTestSpecificationImpl <em>Material Test Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialTestSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialTestSpecification()
		 * @generated
		 */
		EClass MATERIAL_TEST_SPECIFICATION = eINSTANCE.getMaterialTestSpecification();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_TEST_SPECIFICATION__ID = eINSTANCE.getMaterialTestSpecification_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_TEST_SPECIFICATION__DESCRIPTION = eINSTANCE.getMaterialTestSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_TEST_SPECIFICATION__VERSION = eINSTANCE.getMaterialTestSpecification_Version();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl <em>Material Sublot</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialSublotImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialSublot()
		 * @generated
		 */
		EClass MATERIAL_SUBLOT = eINSTANCE.getMaterialSublot();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SUBLOT__PROPERTIES = eINSTANCE.getMaterialSublot_Properties();

		/**
		 * The meta object literal for the '<em><b>Assembled From Lots</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SUBLOT__ASSEMBLED_FROM_LOTS = eINSTANCE.getMaterialSublot_AssembledFromLots();

		/**
		 * The meta object literal for the '<em><b>Assembled From Sublots</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SUBLOT__ASSEMBLED_FROM_SUBLOTS = eINSTANCE.getMaterialSublot_AssembledFromSublots();

		/**
		 * The meta object literal for the '<em><b>Sublots</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SUBLOT__SUBLOTS = eINSTANCE.getMaterialSublot_Sublots();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SUBLOT__ID = eINSTANCE.getMaterialSublot_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SUBLOT__DESCRIPTION = eINSTANCE.getMaterialSublot_Description();

		/**
		 * The meta object literal for the '<em><b>Assembly Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SUBLOT__ASSEMBLY_TYPE = eINSTANCE.getMaterialSublot_AssemblyType();

		/**
		 * The meta object literal for the '<em><b>Assembly Relationship</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SUBLOT__ASSEMBLY_RELATIONSHIP = eINSTANCE.getMaterialSublot_AssemblyRelationship();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SUBLOT__STATUS = eINSTANCE.getMaterialSublot_Status();

		/**
		 * The meta object literal for the '<em><b>Storage Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SUBLOT__STORAGE_LOCATION = eINSTANCE.getMaterialSublot_StorageLocation();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SUBLOT__QUANTITY = eINSTANCE.getMaterialSublot_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SUBLOT__QUANTITY_UNIT_OF_MEASURE = eINSTANCE.getMaterialSublot_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialLotImpl <em>Material Lot</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialLotImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialLot()
		 * @generated
		 */
		EClass MATERIAL_LOT = eINSTANCE.getMaterialLot();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_LOT__PROPERTIES = eINSTANCE.getMaterialLot_Properties();

		/**
		 * The meta object literal for the '<em><b>Assembled From Lots</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_LOT__ASSEMBLED_FROM_LOTS = eINSTANCE.getMaterialLot_AssembledFromLots();

		/**
		 * The meta object literal for the '<em><b>Assembled From Sublots</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_LOT__ASSEMBLED_FROM_SUBLOTS = eINSTANCE.getMaterialLot_AssembledFromSublots();

		/**
		 * The meta object literal for the '<em><b>Sublots</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_LOT__SUBLOTS = eINSTANCE.getMaterialLot_Sublots();

		/**
		 * The meta object literal for the '<em><b>Material Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_LOT__MATERIAL_DEFINITION = eINSTANCE.getMaterialLot_MaterialDefinition();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_LOT__TEST_SPECIFICATIONS = eINSTANCE.getMaterialLot_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_LOT__ID = eINSTANCE.getMaterialLot_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_LOT__DESCRIPTION = eINSTANCE.getMaterialLot_Description();

		/**
		 * The meta object literal for the '<em><b>Assembly Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_LOT__ASSEMBLY_TYPE = eINSTANCE.getMaterialLot_AssemblyType();

		/**
		 * The meta object literal for the '<em><b>Assembly Relationship</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_LOT__ASSEMBLY_RELATIONSHIP = eINSTANCE.getMaterialLot_AssemblyRelationship();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_LOT__STATUS = eINSTANCE.getMaterialLot_Status();

		/**
		 * The meta object literal for the '<em><b>Storage Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_LOT__STORAGE_LOCATION = eINSTANCE.getMaterialLot_StorageLocation();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_LOT__QUANTITY = eINSTANCE.getMaterialLot_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_LOT__QUANTITY_UNIT_OF_MEASURE = eINSTANCE.getMaterialLot_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialLotPropertyImpl <em>Material Lot Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialLotPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialLotProperty()
		 * @generated
		 */
		EClass MATERIAL_LOT_PROPERTY = eINSTANCE.getMaterialLotProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_LOT_PROPERTY__SUB_PROPERTIES = eINSTANCE.getMaterialLotProperty_SubProperties();

		/**
		 * The meta object literal for the '<em><b>Material Definition Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_LOT_PROPERTY__MATERIAL_DEFINITION_PROPERTY = eINSTANCE
				.getMaterialLotProperty_MaterialDefinitionProperty();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_LOT_PROPERTY__ID = eINSTANCE.getMaterialLotProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_LOT_PROPERTY__DESCRIPTION = eINSTANCE.getMaterialLotProperty_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_LOT_PROPERTY__VALUE = eINSTANCE.getMaterialLotProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_LOT_PROPERTY__VALUE_UNIT_OF_MEASURE = eINSTANCE.getMaterialLotProperty_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialDefinitionImpl <em>Material Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialDefinitionImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialDefinition()
		 * @generated
		 */
		EClass MATERIAL_DEFINITION = eINSTANCE.getMaterialDefinition();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_DEFINITION__PROPERTIES = eINSTANCE.getMaterialDefinition_Properties();

		/**
		 * The meta object literal for the '<em><b>Assembled From Definitions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_DEFINITION__ASSEMBLED_FROM_DEFINITIONS = eINSTANCE
				.getMaterialDefinition_AssembledFromDefinitions();

		/**
		 * The meta object literal for the '<em><b>Material Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_DEFINITION__MATERIAL_CLASSES = eINSTANCE.getMaterialDefinition_MaterialClasses();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_DEFINITION__TEST_SPECIFICATIONS = eINSTANCE.getMaterialDefinition_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_DEFINITION__ID = eINSTANCE.getMaterialDefinition_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_DEFINITION__DESCRIPTION = eINSTANCE.getMaterialDefinition_Description();

		/**
		 * The meta object literal for the '<em><b>Assembly Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_DEFINITION__ASSEMBLY_TYPE = eINSTANCE.getMaterialDefinition_AssemblyType();

		/**
		 * The meta object literal for the '<em><b>Assembly Relationship</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_DEFINITION__ASSEMBLY_RELATIONSHIP = eINSTANCE.getMaterialDefinition_AssemblyRelationship();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialDefinitionPropertyImpl <em>Material Definition Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialDefinitionPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialDefinitionProperty()
		 * @generated
		 */
		EClass MATERIAL_DEFINITION_PROPERTY = eINSTANCE.getMaterialDefinitionProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_DEFINITION_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getMaterialDefinitionProperty_SubProperties();

		/**
		 * The meta object literal for the '<em><b>Material Class Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_DEFINITION_PROPERTY__MATERIAL_CLASS_PROPERTY = eINSTANCE
				.getMaterialDefinitionProperty_MaterialClassProperty();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_DEFINITION_PROPERTY__TEST_SPECIFICATIONS = eINSTANCE
				.getMaterialDefinitionProperty_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_DEFINITION_PROPERTY__ID = eINSTANCE.getMaterialDefinitionProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_DEFINITION_PROPERTY__DESCRIPTION = eINSTANCE.getMaterialDefinitionProperty_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_DEFINITION_PROPERTY__VALUE = eINSTANCE.getMaterialDefinitionProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_DEFINITION_PROPERTY__VALUE_UNIT_OF_MEASURE = eINSTANCE
				.getMaterialDefinitionProperty_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialClassPropertyImpl <em>Material Class Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialClassPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialClassProperty()
		 * @generated
		 */
		EClass MATERIAL_CLASS_PROPERTY = eINSTANCE.getMaterialClassProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CLASS_PROPERTY__SUB_PROPERTIES = eINSTANCE.getMaterialClassProperty_SubProperties();

		/**
		 * The meta object literal for the '<em><b>Test Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CLASS_PROPERTY__TEST_SPECIFICATIONS = eINSTANCE
				.getMaterialClassProperty_TestSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CLASS_PROPERTY__ID = eINSTANCE.getMaterialClassProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CLASS_PROPERTY__DESCRIPTION = eINSTANCE.getMaterialClassProperty_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CLASS_PROPERTY__VALUE = eINSTANCE.getMaterialClassProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CLASS_PROPERTY__VALUE_UNIT_OF_MEASURE = eINSTANCE
				.getMaterialClassProperty_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentModelImpl <em>Process Segment Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentModelImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getProcessSegmentModel()
		 * @generated
		 */
		EClass PROCESS_SEGMENT_MODEL = eINSTANCE.getProcessSegmentModel();

		/**
		 * The meta object literal for the '<em><b>Process Segments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_MODEL__PROCESS_SEGMENTS = eINSTANCE.getProcessSegmentModel_ProcessSegments();

		/**
		 * The meta object literal for the '<em><b>Process Segment Dependencies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_MODEL__PROCESS_SEGMENT_DEPENDENCIES = eINSTANCE
				.getProcessSegmentModel_ProcessSegmentDependencies();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl <em>Process Segment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getProcessSegment()
		 * @generated
		 */
		EClass PROCESS_SEGMENT = eINSTANCE.getProcessSegment();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT__CHILDREN = eINSTANCE.getProcessSegment_Children();

		/**
		 * The meta object literal for the '<em><b>Process Segment Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT__PROCESS_SEGMENT_PARAMETERS = eINSTANCE.getProcessSegment_ProcessSegmentParameters();

		/**
		 * The meta object literal for the '<em><b>Personnel Segment Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT__PERSONNEL_SEGMENT_SPECIFICATIONS = eINSTANCE
				.getProcessSegment_PersonnelSegmentSpecifications();

		/**
		 * The meta object literal for the '<em><b>Equipment Segment Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT__EQUIPMENT_SEGMENT_SPECIFICATIONS = eINSTANCE
				.getProcessSegment_EquipmentSegmentSpecifications();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Segment Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT__PHYSICAL_ASSET_SEGMENT_SPECIFICATIONS = eINSTANCE
				.getProcessSegment_PhysicalAssetSegmentSpecifications();

		/**
		 * The meta object literal for the '<em><b>Material Segment Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT__MATERIAL_SEGMENT_SPECIFICATIONS = eINSTANCE
				.getProcessSegment_MaterialSegmentSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT__ID = eINSTANCE.getProcessSegment_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT__DESCRIPTION = eINSTANCE.getProcessSegment_Description();

		/**
		 * The meta object literal for the '<em><b>Operations Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT__OPERATIONS_TYPE = eINSTANCE.getProcessSegment_OperationsType();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT__DURATION = eINSTANCE.getProcessSegment_Duration();

		/**
		 * The meta object literal for the '<em><b>Duration Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT__DURATION_UNIT_OF_MEASURE = eINSTANCE.getProcessSegment_DurationUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT__HIERARCHY_SCOPE = eINSTANCE.getProcessSegment_HierarchyScope();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSegmentSpecificationPropertyImpl <em>Personnel Segment Specification Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelSegmentSpecificationPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelSegmentSpecificationProperty()
		 * @generated
		 */
		EClass PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY = eINSTANCE.getPersonnelSegmentSpecificationProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getPersonnelSegmentSpecificationProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentSegmentSpecificationPropertyImpl <em>Equipment Segment Specification Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentSegmentSpecificationPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentSegmentSpecificationProperty()
		 * @generated
		 */
		EClass EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY = eINSTANCE.getEquipmentSegmentSpecificationProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getEquipmentSegmentSpecificationProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationPropertyImpl <em>Physical Asset Segment Specification Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetSegmentSpecificationProperty()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY = eINSTANCE.getPhysicalAssetSegmentSpecificationProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getPhysicalAssetSegmentSpecificationProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSegmentSpecificationPropertyImpl <em>Material Segment Specification Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialSegmentSpecificationPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialSegmentSpecificationProperty()
		 * @generated
		 */
		EClass MATERIAL_SEGMENT_SPECIFICATION_PROPERTY = eINSTANCE.getMaterialSegmentSpecificationProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SEGMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getMaterialSegmentSpecificationProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSegmentSpecificationImpl <em>Material Segment Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialSegmentSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialSegmentSpecification()
		 * @generated
		 */
		EClass MATERIAL_SEGMENT_SPECIFICATION = eINSTANCE.getMaterialSegmentSpecification();

		/**
		 * The meta object literal for the '<em><b>Assembled From Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SEGMENT_SPECIFICATION__ASSEMBLED_FROM_SPECIFICATIONS = eINSTANCE
				.getMaterialSegmentSpecification_AssembledFromSpecifications();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SEGMENT_SPECIFICATION__PROPERTIES = eINSTANCE.getMaterialSegmentSpecification_Properties();

		/**
		 * The meta object literal for the '<em><b>Material Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SEGMENT_SPECIFICATION__MATERIAL_CLASSES = eINSTANCE
				.getMaterialSegmentSpecification_MaterialClasses();

		/**
		 * The meta object literal for the '<em><b>Material Definitions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SEGMENT_SPECIFICATION__MATERIAL_DEFINITIONS = eINSTANCE
				.getMaterialSegmentSpecification_MaterialDefinitions();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SEGMENT_SPECIFICATION__DESCRIPTION = eINSTANCE
				.getMaterialSegmentSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Assembly Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SEGMENT_SPECIFICATION__ASSEMBLY_TYPE = eINSTANCE
				.getMaterialSegmentSpecification_AssemblyType();

		/**
		 * The meta object literal for the '<em><b>Assembly Relationship</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SEGMENT_SPECIFICATION__ASSEMBLY_RELATIONSHIP = eINSTANCE
				.getMaterialSegmentSpecification_AssemblyRelationship();

		/**
		 * The meta object literal for the '<em><b>Material Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SEGMENT_SPECIFICATION__MATERIAL_USE = eINSTANCE
				.getMaterialSegmentSpecification_MaterialUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SEGMENT_SPECIFICATION__QUANTITY = eINSTANCE.getMaterialSegmentSpecification_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measurement</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASUREMENT = eINSTANCE
				.getMaterialSegmentSpecification_QuantityUnitOfMeasurement();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationImpl <em>Physical Asset Segment Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetSegmentSpecification()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_SEGMENT_SPECIFICATION = eINSTANCE.getPhysicalAssetSegmentSpecification();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PROPERTIES = eINSTANCE
				.getPhysicalAssetSegmentSpecification_Properties();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_CLASSES = eINSTANCE
				.getPhysicalAssetSegmentSpecification_PhysicalAssetClasses();

		/**
		 * The meta object literal for the '<em><b>Physical Assets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSETS = eINSTANCE
				.getPhysicalAssetSegmentSpecification_PhysicalAssets();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_SEGMENT_SPECIFICATION__DESCRIPTION = eINSTANCE
				.getPhysicalAssetSegmentSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_USE = eINSTANCE
				.getPhysicalAssetSegmentSpecification_PhysicalAssetUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY = eINSTANCE
				.getPhysicalAssetSegmentSpecification_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getPhysicalAssetSegmentSpecification_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentSegmentSpecificationImpl <em>Equipment Segment Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentSegmentSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentSegmentSpecification()
		 * @generated
		 */
		EClass EQUIPMENT_SEGMENT_SPECIFICATION = eINSTANCE.getEquipmentSegmentSpecification();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_SEGMENT_SPECIFICATION__PROPERTIES = eINSTANCE
				.getEquipmentSegmentSpecification_Properties();

		/**
		 * The meta object literal for the '<em><b>Equipment Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_SEGMENT_SPECIFICATION__EQUIPMENT_CLASSES = eINSTANCE
				.getEquipmentSegmentSpecification_EquipmentClasses();

		/**
		 * The meta object literal for the '<em><b>Equipments</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_SEGMENT_SPECIFICATION__EQUIPMENTS = eINSTANCE
				.getEquipmentSegmentSpecification_Equipments();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_SEGMENT_SPECIFICATION__DESCRIPTION = eINSTANCE
				.getEquipmentSegmentSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Equipment Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_SEGMENT_SPECIFICATION__EQUIPMENT_USE = eINSTANCE
				.getEquipmentSegmentSpecification_EquipmentUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_SEGMENT_SPECIFICATION__QUANTITY = eINSTANCE.getEquipmentSegmentSpecification_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measurement</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASUREMENT = eINSTANCE
				.getEquipmentSegmentSpecification_QuantityUnitOfMeasurement();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSegmentSpecificationImpl <em>Personnel Segment Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelSegmentSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelSegmentSpecification()
		 * @generated
		 */
		EClass PERSONNEL_SEGMENT_SPECIFICATION = eINSTANCE.getPersonnelSegmentSpecification();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_SEGMENT_SPECIFICATION__PROPERTIES = eINSTANCE
				.getPersonnelSegmentSpecification_Properties();

		/**
		 * The meta object literal for the '<em><b>Personnel Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_SEGMENT_SPECIFICATION__PERSONNEL_CLASSES = eINSTANCE
				.getPersonnelSegmentSpecification_PersonnelClasses();

		/**
		 * The meta object literal for the '<em><b>Persons</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_SEGMENT_SPECIFICATION__PERSONS = eINSTANCE.getPersonnelSegmentSpecification_Persons();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_SEGMENT_SPECIFICATION__DESCRIPTION = eINSTANCE
				.getPersonnelSegmentSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Personnel Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_SEGMENT_SPECIFICATION__PERSONNEL_USE = eINSTANCE
				.getPersonnelSegmentSpecification_PersonnelUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_SEGMENT_SPECIFICATION__QUANTITY = eINSTANCE.getPersonnelSegmentSpecification_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getPersonnelSegmentSpecification_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentParameterImpl <em>Process Segment Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentParameterImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getProcessSegmentParameter()
		 * @generated
		 */
		EClass PROCESS_SEGMENT_PARAMETER = eINSTANCE.getProcessSegmentParameter();

		/**
		 * The meta object literal for the '<em><b>Sub Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_PARAMETER__SUB_PARAMETERS = eINSTANCE.getProcessSegmentParameter_SubParameters();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_PARAMETER__ID = eINSTANCE.getProcessSegmentParameter_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_PARAMETER__DESCRIPTION = eINSTANCE.getProcessSegmentParameter_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_PARAMETER__VALUE = eINSTANCE.getProcessSegmentParameter_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_PARAMETER__VALUE_UNIT_OF_MEASURE = eINSTANCE
				.getProcessSegmentParameter_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentDependencyImpl <em>Process Segment Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentDependencyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getProcessSegmentDependency()
		 * @generated
		 */
		EClass PROCESS_SEGMENT_DEPENDENCY = eINSTANCE.getProcessSegmentDependency();

		/**
		 * The meta object literal for the '<em><b>Subject</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_DEPENDENCY__SUBJECT = eINSTANCE.getProcessSegmentDependency_Subject();

		/**
		 * The meta object literal for the '<em><b>Dependency</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY = eINSTANCE.getProcessSegmentDependency_Dependency();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_DEPENDENCY__ID = eINSTANCE.getProcessSegmentDependency_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_DEPENDENCY__DESCRIPTION = eINSTANCE.getProcessSegmentDependency_Description();

		/**
		 * The meta object literal for the '<em><b>Dependency Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE = eINSTANCE.getProcessSegmentDependency_DependencyType();

		/**
		 * The meta object literal for the '<em><b>Dependency Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR = eINSTANCE
				.getProcessSegmentDependency_DependencyFactor();

		/**
		 * The meta object literal for the '<em><b>Factor Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_DEPENDENCY__FACTOR_UNIT_OF_MEASURE = eINSTANCE
				.getProcessSegmentDependency_FactorUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264ModelImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getIec62264Model()
		 * @generated
		 */
		EClass IEC62264_MODEL = eINSTANCE.getIec62264Model();

		/**
		 * The meta object literal for the '<em><b>Personnel Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IEC62264_MODEL__PERSONNEL_MODEL = eINSTANCE.getIec62264Model_PersonnelModel();

		/**
		 * The meta object literal for the '<em><b>Equipment Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IEC62264_MODEL__EQUIPMENT_MODEL = eINSTANCE.getIec62264Model_EquipmentModel();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IEC62264_MODEL__PHYSICAL_ASSET_MODEL = eINSTANCE.getIec62264Model_PhysicalAssetModel();

		/**
		 * The meta object literal for the '<em><b>Material Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IEC62264_MODEL__MATERIAL_MODEL = eINSTANCE.getIec62264Model_MaterialModel();

		/**
		 * The meta object literal for the '<em><b>Process Segment Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IEC62264_MODEL__PROCESS_SEGMENT_MODEL = eINSTANCE.getIec62264Model_ProcessSegmentModel();

		/**
		 * The meta object literal for the '<em><b>Operations Definition Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IEC62264_MODEL__OPERATIONS_DEFINITION_MODEL = eINSTANCE.getIec62264Model_OperationsDefinitionModel();

		/**
		 * The meta object literal for the '<em><b>Operations Schedule Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IEC62264_MODEL__OPERATIONS_SCHEDULE_MODEL = eINSTANCE.getIec62264Model_OperationsScheduleModel();

		/**
		 * The meta object literal for the '<em><b>Operations Performance Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IEC62264_MODEL__OPERATIONS_PERFORMANCE_MODEL = eINSTANCE
				.getIec62264Model_OperationsPerformanceModel();

		/**
		 * The meta object literal for the '<em><b>Process Segment Capability Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IEC62264_MODEL__PROCESS_SEGMENT_CAPABILITY_MODEL = eINSTANCE
				.getIec62264Model_ProcessSegmentCapabilityModel();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scopes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IEC62264_MODEL__HIERARCHY_SCOPES = eINSTANCE.getIec62264Model_HierarchyScopes();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IEC62264_MODEL__NAME = eINSTANCE.getIec62264Model_Name();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionModelImpl <em>Operations Definition Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionModelImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsDefinitionModel()
		 * @generated
		 */
		EClass OPERATIONS_DEFINITION_MODEL = eINSTANCE.getOperationsDefinitionModel();

		/**
		 * The meta object literal for the '<em><b>Operations Definitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_DEFINITION_MODEL__OPERATIONS_DEFINITIONS = eINSTANCE
				.getOperationsDefinitionModel_OperationsDefinitions();

		/**
		 * The meta object literal for the '<em><b>Operations Segments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENTS = eINSTANCE
				.getOperationsDefinitionModel_OperationsSegments();

		/**
		 * The meta object literal for the '<em><b>Operations Segment Dependencies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_DEFINITION_MODEL__OPERATIONS_SEGMENT_DEPENDENCIES = eINSTANCE
				.getOperationsDefinitionModel_OperationsSegmentDependencies();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsScheduleModelImpl <em>Operations Schedule Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsScheduleModelImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsScheduleModel()
		 * @generated
		 */
		EClass OPERATIONS_SCHEDULE_MODEL = eINSTANCE.getOperationsScheduleModel();

		/**
		 * The meta object literal for the '<em><b>Operations Schedules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_SCHEDULE_MODEL__OPERATIONS_SCHEDULES = eINSTANCE
				.getOperationsScheduleModel_OperationsSchedules();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceModelImpl <em>Operations Performance Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceModelImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsPerformanceModel()
		 * @generated
		 */
		EClass OPERATIONS_PERFORMANCE_MODEL = eINSTANCE.getOperationsPerformanceModel();

		/**
		 * The meta object literal for the '<em><b>Operations Performances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_PERFORMANCE_MODEL__OPERATIONS_PERFORMANCES = eINSTANCE
				.getOperationsPerformanceModel_OperationsPerformances();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityModelImpl <em>Operations Capability Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityModelImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsCapabilityModel()
		 * @generated
		 */
		EClass OPERATIONS_CAPABILITY_MODEL = eINSTANCE.getOperationsCapabilityModel();

		/**
		 * The meta object literal for the '<em><b>Operations Capabilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_CAPABILITY_MODEL__OPERATIONS_CAPABILITIES = eINSTANCE
				.getOperationsCapabilityModel_OperationsCapabilities();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentCapabilityModelImpl <em>Process Segment Capability Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentCapabilityModelImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getProcessSegmentCapabilityModel()
		 * @generated
		 */
		EClass PROCESS_SEGMENT_CAPABILITY_MODEL = eINSTANCE.getProcessSegmentCapabilityModel();

		/**
		 * The meta object literal for the '<em><b>Process Segment Capabilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_CAPABILITY_MODEL__PROCESS_SEGMENT_CAPABILITIES = eINSTANCE
				.getProcessSegmentCapabilityModel_ProcessSegmentCapabilities();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.HierarchyScopeImpl <em>Hierarchy Scope</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.HierarchyScopeImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getHierarchyScope()
		 * @generated
		 */
		EClass HIERARCHY_SCOPE = eINSTANCE.getHierarchyScope();

		/**
		 * The meta object literal for the '<em><b>Sub Scope</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HIERARCHY_SCOPE__SUB_SCOPE = eINSTANCE.getHierarchyScope_SubScope();

		/**
		 * The meta object literal for the '<em><b>Equipment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HIERARCHY_SCOPE__EQUIPMENT = eINSTANCE.getHierarchyScope_Equipment();

		/**
		 * The meta object literal for the '<em><b>Equipment Element Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HIERARCHY_SCOPE__EQUIPMENT_ELEMENT_LEVEL = eINSTANCE.getHierarchyScope_EquipmentElementLevel();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationPropertyImpl <em>Material Specification Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialSpecificationProperty()
		 * @generated
		 */
		EClass MATERIAL_SPECIFICATION_PROPERTY = eINSTANCE.getMaterialSpecificationProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SPECIFICATION_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getMaterialSpecificationProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationPropertyImpl <em>Physical Asset Specification Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetSpecificationProperty()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_SPECIFICATION_PROPERTY = eINSTANCE.getPhysicalAssetSpecificationProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_SPECIFICATION_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getPhysicalAssetSpecificationProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentSpecificationPropertyImpl <em>Equipment Specification Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentSpecificationPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentSpecificationProperty()
		 * @generated
		 */
		EClass EQUIPMENT_SPECIFICATION_PROPERTY = eINSTANCE.getEquipmentSpecificationProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_SPECIFICATION_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getEquipmentSpecificationProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationPropertyImpl <em>Personnel Specification Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelSpecificationProperty()
		 * @generated
		 */
		EClass PERSONNEL_SPECIFICATION_PROPERTY = eINSTANCE.getPersonnelSpecificationProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_SPECIFICATION_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getPersonnelSpecificationProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl <em>Material Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialSpecification()
		 * @generated
		 */
		EClass MATERIAL_SPECIFICATION = eINSTANCE.getMaterialSpecification();

		/**
		 * The meta object literal for the '<em><b>Assembled From Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SPECIFICATION__ASSEMBLED_FROM_SPECIFICATIONS = eINSTANCE
				.getMaterialSpecification_AssembledFromSpecifications();

		/**
		 * The meta object literal for the '<em><b>Material Specification Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SPECIFICATION__MATERIAL_SPECIFICATION_PROPERTIES = eINSTANCE
				.getMaterialSpecification_MaterialSpecificationProperties();

		/**
		 * The meta object literal for the '<em><b>Material Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SPECIFICATION__MATERIAL_CLASSES = eINSTANCE.getMaterialSpecification_MaterialClasses();

		/**
		 * The meta object literal for the '<em><b>Material Definitions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_SPECIFICATION__MATERIAL_DEFINITIONS = eINSTANCE
				.getMaterialSpecification_MaterialDefinitions();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SPECIFICATION__DESCRIPTION = eINSTANCE.getMaterialSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Material Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SPECIFICATION__MATERIAL_USE = eINSTANCE.getMaterialSpecification_MaterialUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SPECIFICATION__QUANTITY = eINSTANCE.getMaterialSpecification_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getMaterialSpecification_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Assembly Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SPECIFICATION__ASSEMBLY_TYPE = eINSTANCE.getMaterialSpecification_AssemblyType();

		/**
		 * The meta object literal for the '<em><b>Assembly Relationship</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_SPECIFICATION__ASSEMBLY_RELATIONSHIP = eINSTANCE
				.getMaterialSpecification_AssemblyRelationship();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationImpl <em>Physical Asset Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetSpecification()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_SPECIFICATION = eINSTANCE.getPhysicalAssetSpecification();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Specification Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_SPECIFICATION_PROPERTIES = eINSTANCE
				.getPhysicalAssetSpecification_PhysicalAssetSpecificationProperties();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_SPECIFICATION__DESCRIPTION = eINSTANCE.getPhysicalAssetSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Physic Alasset Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_SPECIFICATION__PHYSIC_ALASSET_CLASSES = eINSTANCE
				.getPhysicalAssetSpecification_PhysicAlassetClasses();

		/**
		 * The meta object literal for the '<em><b>Physical Assets</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSETS = eINSTANCE
				.getPhysicalAssetSpecification_PhysicalAssets();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_SPECIFICATION__PHYSICAL_ASSET_USE = eINSTANCE
				.getPhysicalAssetSpecification_PhysicalAssetUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_SPECIFICATION__QUANTITY = eINSTANCE.getPhysicalAssetSpecification_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getPhysicalAssetSpecification_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentSpecificationImpl <em>Equipment Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentSpecification()
		 * @generated
		 */
		EClass EQUIPMENT_SPECIFICATION = eINSTANCE.getEquipmentSpecification();

		/**
		 * The meta object literal for the '<em><b>Equipment Specification Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_SPECIFICATION__EQUIPMENT_SPECIFICATION_PROPERTIES = eINSTANCE
				.getEquipmentSpecification_EquipmentSpecificationProperties();

		/**
		 * The meta object literal for the '<em><b>Equipment Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_SPECIFICATION__EQUIPMENT_CLASSES = eINSTANCE.getEquipmentSpecification_EquipmentClasses();

		/**
		 * The meta object literal for the '<em><b>Equipments</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_SPECIFICATION__EQUIPMENTS = eINSTANCE.getEquipmentSpecification_Equipments();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_SPECIFICATION__DESCRIPTION = eINSTANCE.getEquipmentSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Equipment Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_SPECIFICATION__EQUIPMENT_USE = eINSTANCE.getEquipmentSpecification_EquipmentUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_SPECIFICATION__QUANTITY = eINSTANCE.getEquipmentSpecification_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getEquipmentSpecification_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationImpl <em>Personnel Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelSpecification()
		 * @generated
		 */
		EClass PERSONNEL_SPECIFICATION = eINSTANCE.getPersonnelSpecification();

		/**
		 * The meta object literal for the '<em><b>Personnel Specification Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_SPECIFICATION__PERSONNEL_SPECIFICATION_PROPERTIES = eINSTANCE
				.getPersonnelSpecification_PersonnelSpecificationProperties();

		/**
		 * The meta object literal for the '<em><b>Personnel Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_SPECIFICATION__PERSONNEL_CLASSES = eINSTANCE.getPersonnelSpecification_PersonnelClasses();

		/**
		 * The meta object literal for the '<em><b>Persons</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_SPECIFICATION__PERSONS = eINSTANCE.getPersonnelSpecification_Persons();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_SPECIFICATION__DESCRIPTION = eINSTANCE.getPersonnelSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Personnel Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_SPECIFICATION__PERSONNEL_USE = eINSTANCE.getPersonnelSpecification_PersonnelUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_SPECIFICATION__QUANTITY = eINSTANCE.getPersonnelSpecification_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getPersonnelSpecification_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ParameterSpecificationImpl <em>Parameter Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ParameterSpecificationImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getParameterSpecification()
		 * @generated
		 */
		EClass PARAMETER_SPECIFICATION = eINSTANCE.getParameterSpecification();

		/**
		 * The meta object literal for the '<em><b>Sub Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_SPECIFICATION__SUB_PARAMETERS = eINSTANCE.getParameterSpecification_SubParameters();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SPECIFICATION__ID = eINSTANCE.getParameterSpecification_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SPECIFICATION__DESCRIPTION = eINSTANCE.getParameterSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SPECIFICATION__VALUE = eINSTANCE.getParameterSpecification_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measurement</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SPECIFICATION__VALUE_UNIT_OF_MEASUREMENT = eINSTANCE
				.getParameterSpecification_ValueUnitOfMeasurement();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentDependencyImpl <em>Operations Segment Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentDependencyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsSegmentDependency()
		 * @generated
		 */
		EClass OPERATIONS_SEGMENT_DEPENDENCY = eINSTANCE.getOperationsSegmentDependency();

		/**
		 * The meta object literal for the '<em><b>Subject</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_SEGMENT_DEPENDENCY__SUBJECT = eINSTANCE.getOperationsSegmentDependency_Subject();

		/**
		 * The meta object literal for the '<em><b>Dependency</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY = eINSTANCE.getOperationsSegmentDependency_Dependency();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SEGMENT_DEPENDENCY__ID = eINSTANCE.getOperationsSegmentDependency_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SEGMENT_DEPENDENCY__DESCRIPTION = eINSTANCE.getOperationsSegmentDependency_Description();

		/**
		 * The meta object literal for the '<em><b>Dependency Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_TYPE = eINSTANCE
				.getOperationsSegmentDependency_DependencyType();

		/**
		 * The meta object literal for the '<em><b>Dependency Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SEGMENT_DEPENDENCY__DEPENDENCY_FACTOR = eINSTANCE
				.getOperationsSegmentDependency_DependencyFactor();

		/**
		 * The meta object literal for the '<em><b>Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SEGMENT_DEPENDENCY__UNIT_OF_MEASURE = eINSTANCE
				.getOperationsSegmentDependency_UnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl <em>Operations Material Bill Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillItemImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsMaterialBillItem()
		 * @generated
		 */
		EClass OPERATIONS_MATERIAL_BILL_ITEM = eINSTANCE.getOperationsMaterialBillItem();

		/**
		 * The meta object literal for the '<em><b>Material Specifications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_SPECIFICATIONS = eINSTANCE
				.getOperationsMaterialBillItem_MaterialSpecifications();

		/**
		 * The meta object literal for the '<em><b>Assembled From Items</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLED_FROM_ITEMS = eINSTANCE
				.getOperationsMaterialBillItem_AssembledFromItems();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_MATERIAL_BILL_ITEM__ID = eINSTANCE.getOperationsMaterialBillItem_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_MATERIAL_BILL_ITEM__DESCRIPTION = eINSTANCE.getOperationsMaterialBillItem_Description();

		/**
		 * The meta object literal for the '<em><b>Material Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_CLASSES = eINSTANCE
				.getOperationsMaterialBillItem_MaterialClasses();

		/**
		 * The meta object literal for the '<em><b>Material Definitions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_MATERIAL_BILL_ITEM__MATERIAL_DEFINITIONS = eINSTANCE
				.getOperationsMaterialBillItem_MaterialDefinitions();

		/**
		 * The meta object literal for the '<em><b>Use Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_MATERIAL_BILL_ITEM__USE_TYPE = eINSTANCE.getOperationsMaterialBillItem_UseType();

		/**
		 * The meta object literal for the '<em><b>Assembly Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_TYPE = eINSTANCE
				.getOperationsMaterialBillItem_AssemblyType();

		/**
		 * The meta object literal for the '<em><b>Assembly Relationship</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_MATERIAL_BILL_ITEM__ASSEMBLY_RELATIONSHIP = eINSTANCE
				.getOperationsMaterialBillItem_AssemblyRelationship();

		/**
		 * The meta object literal for the '<em><b>Quantities</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_MATERIAL_BILL_ITEM__QUANTITIES = eINSTANCE.getOperationsMaterialBillItem_Quantities();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measures</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_MATERIAL_BILL_ITEM__QUANTITY_UNIT_OF_MEASURES = eINSTANCE
				.getOperationsMaterialBillItem_QuantityUnitOfMeasures();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillImpl <em>Operations Material Bill</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsMaterialBillImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsMaterialBill()
		 * @generated
		 */
		EClass OPERATIONS_MATERIAL_BILL = eINSTANCE.getOperationsMaterialBill();

		/**
		 * The meta object literal for the '<em><b>Operations Material Bill Items</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_MATERIAL_BILL__OPERATIONS_MATERIAL_BILL_ITEMS = eINSTANCE
				.getOperationsMaterialBill_OperationsMaterialBillItems();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_MATERIAL_BILL__ID = eINSTANCE.getOperationsMaterialBill_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_MATERIAL_BILL__DESCRIPTION = eINSTANCE.getOperationsMaterialBill_Description();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl <em>Operations Segment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsSegmentImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsSegment()
		 * @generated
		 */
		EClass OPERATIONS_SEGMENT = eINSTANCE.getOperationsSegment();

		/**
		 * The meta object literal for the '<em><b>Process Segments</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_SEGMENT__PROCESS_SEGMENTS = eINSTANCE.getOperationsSegment_ProcessSegments();

		/**
		 * The meta object literal for the '<em><b>Sub Segments</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_SEGMENT__SUB_SEGMENTS = eINSTANCE.getOperationsSegment_SubSegments();

		/**
		 * The meta object literal for the '<em><b>Parameter Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_SEGMENT__PARAMETER_SPECIFICATIONS = eINSTANCE
				.getOperationsSegment_ParameterSpecifications();

		/**
		 * The meta object literal for the '<em><b>Personnel Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_SEGMENT__PERSONNEL_SPECIFICATIONS = eINSTANCE
				.getOperationsSegment_PersonnelSpecifications();

		/**
		 * The meta object literal for the '<em><b>Equipment Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_SEGMENT__EQUIPMENT_SPECIFICATIONS = eINSTANCE
				.getOperationsSegment_EquipmentSpecifications();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_SEGMENT__PHYSICAL_ASSET_SPECIFICATIONS = eINSTANCE
				.getOperationsSegment_PhysicalAssetSpecifications();

		/**
		 * The meta object literal for the '<em><b>Material Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_SEGMENT__MATERIAL_SPECIFICATIONS = eINSTANCE
				.getOperationsSegment_MaterialSpecifications();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SEGMENT__ID = eINSTANCE.getOperationsSegment_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SEGMENT__DESCRIPTION = eINSTANCE.getOperationsSegment_Description();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SEGMENT__DURATION = eINSTANCE.getOperationsSegment_Duration();

		/**
		 * The meta object literal for the '<em><b>Duration Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SEGMENT__DURATION_UNIT_OF_MEASURE = eINSTANCE
				.getOperationsSegment_DurationUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Operations Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SEGMENT__OPERATIONS_TYPE = eINSTANCE.getOperationsSegment_OperationsType();

		/**
		 * The meta object literal for the '<em><b>Work Definition Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SEGMENT__WORK_DEFINITION_ID = eINSTANCE.getOperationsSegment_WorkDefinitionId();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl <em>Operations Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsDefinitionImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsDefinition()
		 * @generated
		 */
		EClass OPERATIONS_DEFINITION = eINSTANCE.getOperationsDefinition();

		/**
		 * The meta object literal for the '<em><b>Operations Segments</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_DEFINITION__OPERATIONS_SEGMENTS = eINSTANCE.getOperationsDefinition_OperationsSegments();

		/**
		 * The meta object literal for the '<em><b>Operations Material Bills</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_DEFINITION__OPERATIONS_MATERIAL_BILLS = eINSTANCE
				.getOperationsDefinition_OperationsMaterialBills();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_DEFINITION__ID = eINSTANCE.getOperationsDefinition_Id();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_DEFINITION__VERSION = eINSTANCE.getOperationsDefinition_Version();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_DEFINITION__DESCRIPTION = eINSTANCE.getOperationsDefinition_Description();

		/**
		 * The meta object literal for the '<em><b>Operations Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_DEFINITION__OPERATIONS_TYPE = eINSTANCE.getOperationsDefinition_OperationsType();

		/**
		 * The meta object literal for the '<em><b>Bill Of Material Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_DEFINITION__BILL_OF_MATERIAL_ID = eINSTANCE.getOperationsDefinition_BillOfMaterialId();

		/**
		 * The meta object literal for the '<em><b>Work Definition Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_DEFINITION__WORK_DEFINITION_ID = eINSTANCE.getOperationsDefinition_WorkDefinitionId();

		/**
		 * The meta object literal for the '<em><b>Bill Of Resource Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_DEFINITION__BILL_OF_RESOURCE_ID = eINSTANCE.getOperationsDefinition_BillOfResourceId();

		/**
		 * The meta object literal for the '<em><b>Hierarchys Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_DEFINITION__HIERARCHYS_SCOPE = eINSTANCE.getOperationsDefinition_HierarchysScope();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialRequirementImpl <em>Material Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialRequirementImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialRequirement()
		 * @generated
		 */
		EClass MATERIAL_REQUIREMENT = eINSTANCE.getMaterialRequirement();

		/**
		 * The meta object literal for the '<em><b>Material Requirement Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_REQUIREMENT__MATERIAL_REQUIREMENT_PROPERTIES = eINSTANCE
				.getMaterialRequirement_MaterialRequirementProperties();

		/**
		 * The meta object literal for the '<em><b>Assembled From Requirements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_REQUIREMENT__ASSEMBLED_FROM_REQUIREMENTS = eINSTANCE
				.getMaterialRequirement_AssembledFromRequirements();

		/**
		 * The meta object literal for the '<em><b>Material Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_REQUIREMENT__MATERIAL_CLASSES = eINSTANCE.getMaterialRequirement_MaterialClasses();

		/**
		 * The meta object literal for the '<em><b>Material Definitions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_REQUIREMENT__MATERIAL_DEFINITIONS = eINSTANCE.getMaterialRequirement_MaterialDefinitions();

		/**
		 * The meta object literal for the '<em><b>Material Lots</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_REQUIREMENT__MATERIAL_LOTS = eINSTANCE.getMaterialRequirement_MaterialLots();

		/**
		 * The meta object literal for the '<em><b>Material Sublot</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_REQUIREMENT__MATERIAL_SUBLOT = eINSTANCE.getMaterialRequirement_MaterialSublot();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_REQUIREMENT__DESCRIPTION = eINSTANCE.getMaterialRequirement_Description();

		/**
		 * The meta object literal for the '<em><b>Material Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_REQUIREMENT__MATERIAL_USE = eINSTANCE.getMaterialRequirement_MaterialUse();

		/**
		 * The meta object literal for the '<em><b>Storage Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_REQUIREMENT__STORAGE_LOCATION = eINSTANCE.getMaterialRequirement_StorageLocation();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_REQUIREMENT__QUANTITY = eINSTANCE.getMaterialRequirement_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_REQUIREMENT__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getMaterialRequirement_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Assembly Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_REQUIREMENT__ASSEMBLY_TYPE = eINSTANCE.getMaterialRequirement_AssemblyType();

		/**
		 * The meta object literal for the '<em><b>Assembly Relationship</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_REQUIREMENT__ASSEMBLY_RELATIONSHIP = eINSTANCE
				.getMaterialRequirement_AssemblyRelationship();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialRequirementPropertyImpl <em>Material Requirement Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialRequirementPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialRequirementProperty()
		 * @generated
		 */
		EClass MATERIAL_REQUIREMENT_PROPERTY = eINSTANCE.getMaterialRequirementProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_REQUIREMENT_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getMaterialRequirementProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetRequirementPropertyImpl <em>Physical Asset Requirement Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetRequirementPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetRequirementProperty()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_REQUIREMENT_PROPERTY = eINSTANCE.getPhysicalAssetRequirementProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_REQUIREMENT_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getPhysicalAssetRequirementProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentRequirementPropertyImpl <em>Equipment Requirement Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentRequirementPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentRequirementProperty()
		 * @generated
		 */
		EClass EQUIPMENT_REQUIREMENT_PROPERTY = eINSTANCE.getEquipmentRequirementProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_REQUIREMENT_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getEquipmentRequirementProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelRequirementPropertyImpl <em>Personnel Requirement Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelRequirementPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelRequirementProperty()
		 * @generated
		 */
		EClass PERSONNEL_REQUIREMENT_PROPERTY = eINSTANCE.getPersonnelRequirementProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_REQUIREMENT_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getPersonnelRequirementProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsScheduleImpl <em>Operations Schedule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsScheduleImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsSchedule()
		 * @generated
		 */
		EClass OPERATIONS_SCHEDULE = eINSTANCE.getOperationsSchedule();

		/**
		 * The meta object literal for the '<em><b>Operations Requests</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_SCHEDULE__OPERATIONS_REQUESTS = eINSTANCE.getOperationsSchedule_OperationsRequests();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SCHEDULE__ID = eINSTANCE.getOperationsSchedule_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SCHEDULE__DESCRIPTION = eINSTANCE.getOperationsSchedule_Description();

		/**
		 * The meta object literal for the '<em><b>Operations Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SCHEDULE__OPERATIONS_TYPE = eINSTANCE.getOperationsSchedule_OperationsType();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SCHEDULE__START_TIME = eINSTANCE.getOperationsSchedule_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SCHEDULE__END_TIME = eINSTANCE.getOperationsSchedule_EndTime();

		/**
		 * The meta object literal for the '<em><b>Published Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SCHEDULE__PUBLISHED_DATE = eINSTANCE.getOperationsSchedule_PublishedDate();

		/**
		 * The meta object literal for the '<em><b>Scheduled State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_SCHEDULE__SCHEDULED_STATE = eINSTANCE.getOperationsSchedule_ScheduledState();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_SCHEDULE__HIERARCHY_SCOPE = eINSTANCE.getOperationsSchedule_HierarchyScope();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsRequestImpl <em>Operations Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsRequestImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsRequest()
		 * @generated
		 */
		EClass OPERATIONS_REQUEST = eINSTANCE.getOperationsRequest();

		/**
		 * The meta object literal for the '<em><b>Segment Requirements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_REQUEST__SEGMENT_REQUIREMENTS = eINSTANCE.getOperationsRequest_SegmentRequirements();

		/**
		 * The meta object literal for the '<em><b>Requested Segment Responses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_REQUEST__REQUESTED_SEGMENT_RESPONSES = eINSTANCE
				.getOperationsRequest_RequestedSegmentResponses();

		/**
		 * The meta object literal for the '<em><b>Operations Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_REQUEST__OPERATIONS_DEFINITION = eINSTANCE.getOperationsRequest_OperationsDefinition();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_REQUEST__ID = eINSTANCE.getOperationsRequest_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_REQUEST__DESCRIPTION = eINSTANCE.getOperationsRequest_Description();

		/**
		 * The meta object literal for the '<em><b>Operations Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_REQUEST__OPERATIONS_TYPE = eINSTANCE.getOperationsRequest_OperationsType();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_REQUEST__START_TIME = eINSTANCE.getOperationsRequest_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_REQUEST__END_TIME = eINSTANCE.getOperationsRequest_EndTime();

		/**
		 * The meta object literal for the '<em><b>Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_REQUEST__PRIORITY = eINSTANCE.getOperationsRequest_Priority();

		/**
		 * The meta object literal for the '<em><b>Request State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_REQUEST__REQUEST_STATE = eINSTANCE.getOperationsRequest_RequestState();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_REQUEST__HIERARCHY_SCOPE = eINSTANCE.getOperationsRequest_HierarchyScope();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.RequestedSegmentResponseImpl <em>Requested Segment Response</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.RequestedSegmentResponseImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getRequestedSegmentResponse()
		 * @generated
		 */
		EClass REQUESTED_SEGMENT_RESPONSE = eINSTANCE.getRequestedSegmentResponse();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl <em>Segment Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getSegmentRequirement()
		 * @generated
		 */
		EClass SEGMENT_REQUIREMENT = eINSTANCE.getSegmentRequirement();

		/**
		 * The meta object literal for the '<em><b>Sub Requirements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_REQUIREMENT__SUB_REQUIREMENTS = eINSTANCE.getSegmentRequirement_SubRequirements();

		/**
		 * The meta object literal for the '<em><b>Process Segment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_REQUIREMENT__PROCESS_SEGMENT = eINSTANCE.getSegmentRequirement_ProcessSegment();

		/**
		 * The meta object literal for the '<em><b>Operations Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_REQUIREMENT__OPERATIONS_DEFINITION = eINSTANCE.getSegmentRequirement_OperationsDefinition();

		/**
		 * The meta object literal for the '<em><b>Segment Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_REQUIREMENT__SEGMENT_PARAMETERS = eINSTANCE.getSegmentRequirement_SegmentParameters();

		/**
		 * The meta object literal for the '<em><b>Personnel Requirements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_REQUIREMENT__PERSONNEL_REQUIREMENTS = eINSTANCE
				.getSegmentRequirement_PersonnelRequirements();

		/**
		 * The meta object literal for the '<em><b>Equipment Requirements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_REQUIREMENT__EQUIPMENT_REQUIREMENTS = eINSTANCE
				.getSegmentRequirement_EquipmentRequirements();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Requirements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_REQUIREMENT__PHYSICAL_ASSET_REQUIREMENTS = eINSTANCE
				.getSegmentRequirement_PhysicalAssetRequirements();

		/**
		 * The meta object literal for the '<em><b>Material Requirements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_REQUIREMENT__MATERIAL_REQUIREMENTS = eINSTANCE.getSegmentRequirement_MaterialRequirements();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_REQUIREMENT__ID = eINSTANCE.getSegmentRequirement_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_REQUIREMENT__DESCRIPTION = eINSTANCE.getSegmentRequirement_Description();

		/**
		 * The meta object literal for the '<em><b>Operations Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_REQUIREMENT__OPERATIONS_TYPE = eINSTANCE.getSegmentRequirement_OperationsType();

		/**
		 * The meta object literal for the '<em><b>Earliest Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_REQUIREMENT__EARLIEST_START_TIME = eINSTANCE.getSegmentRequirement_EarliestStartTime();

		/**
		 * The meta object literal for the '<em><b>Latest Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_REQUIREMENT__LATEST_START_TIME = eINSTANCE.getSegmentRequirement_LatestStartTime();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_REQUIREMENT__DURATION = eINSTANCE.getSegmentRequirement_Duration();

		/**
		 * The meta object literal for the '<em><b>Duration Unit Of Measurement</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_REQUIREMENT__DURATION_UNIT_OF_MEASUREMENT = eINSTANCE
				.getSegmentRequirement_DurationUnitOfMeasurement();

		/**
		 * The meta object literal for the '<em><b>Segment State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_REQUIREMENT__SEGMENT_STATE = eINSTANCE.getSegmentRequirement_SegmentState();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_REQUIREMENT__HIERARCHY_SCOPE = eINSTANCE.getSegmentRequirement_HierarchyScope();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentRequirementImpl <em>Equipment Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentRequirementImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentRequirement()
		 * @generated
		 */
		EClass EQUIPMENT_REQUIREMENT = eINSTANCE.getEquipmentRequirement();

		/**
		 * The meta object literal for the '<em><b>Equipment Requirement Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_REQUIREMENT__EQUIPMENT_REQUIREMENT_PROPERTIES = eINSTANCE
				.getEquipmentRequirement_EquipmentRequirementProperties();

		/**
		 * The meta object literal for the '<em><b>Equipment Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_REQUIREMENT__EQUIPMENT_CLASSES = eINSTANCE.getEquipmentRequirement_EquipmentClasses();

		/**
		 * The meta object literal for the '<em><b>Equipments</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_REQUIREMENT__EQUIPMENTS = eINSTANCE.getEquipmentRequirement_Equipments();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_REQUIREMENT__DESCRIPTION = eINSTANCE.getEquipmentRequirement_Description();

		/**
		 * The meta object literal for the '<em><b>Equipment Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_REQUIREMENT__EQUIPMENT_USE = eINSTANCE.getEquipmentRequirement_EquipmentUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_REQUIREMENT__QUANTITY = eINSTANCE.getEquipmentRequirement_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_REQUIREMENT__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getEquipmentRequirement_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Equipment Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_REQUIREMENT__EQUIPMENT_LEVEL = eINSTANCE.getEquipmentRequirement_EquipmentLevel();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetRequirementImpl <em>Physical Asset Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetRequirementImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetRequirement()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_REQUIREMENT = eINSTANCE.getPhysicalAssetRequirement();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Requirement Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_REQUIREMENT__PHYSICAL_ASSET_REQUIREMENT_PROPERTIES = eINSTANCE
				.getPhysicalAssetRequirement_PhysicalAssetRequirementProperties();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_REQUIREMENT__PHYSICAL_ASSET_CLASSES = eINSTANCE
				.getPhysicalAssetRequirement_PhysicalAssetClasses();

		/**
		 * The meta object literal for the '<em><b>Physical Assets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_REQUIREMENT__PHYSICAL_ASSETS = eINSTANCE.getPhysicalAssetRequirement_PhysicalAssets();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_REQUIREMENT__DESCRIPTION = eINSTANCE.getPhysicalAssetRequirement_Description();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_REQUIREMENT__PHYSICAL_ASSET_USE = eINSTANCE
				.getPhysicalAssetRequirement_PhysicalAssetUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_REQUIREMENT__QUANTITY = eINSTANCE.getPhysicalAssetRequirement_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_REQUIREMENT__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getPhysicalAssetRequirement_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Equipment Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_REQUIREMENT__EQUIPMENT_LEVEL = eINSTANCE.getPhysicalAssetRequirement_EquipmentLevel();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelRequirementImpl <em>Personnel Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelRequirementImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelRequirement()
		 * @generated
		 */
		EClass PERSONNEL_REQUIREMENT = eINSTANCE.getPersonnelRequirement();

		/**
		 * The meta object literal for the '<em><b>Personnel Requirement Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_REQUIREMENT__PERSONNEL_REQUIREMENT_PROPERTIES = eINSTANCE
				.getPersonnelRequirement_PersonnelRequirementProperties();

		/**
		 * The meta object literal for the '<em><b>Personnel Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_REQUIREMENT__PERSONNEL_CLASSES = eINSTANCE.getPersonnelRequirement_PersonnelClasses();

		/**
		 * The meta object literal for the '<em><b>Persons</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_REQUIREMENT__PERSONS = eINSTANCE.getPersonnelRequirement_Persons();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_REQUIREMENT__DESCRIPTION = eINSTANCE.getPersonnelRequirement_Description();

		/**
		 * The meta object literal for the '<em><b>Personnel Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_REQUIREMENT__PERSONNEL_USE = eINSTANCE.getPersonnelRequirement_PersonnelUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_REQUIREMENT__QUANTITY = eINSTANCE.getPersonnelRequirement_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_REQUIREMENT__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getPersonnelRequirement_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentParameterImpl <em>Segment Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.SegmentParameterImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getSegmentParameter()
		 * @generated
		 */
		EClass SEGMENT_PARAMETER = eINSTANCE.getSegmentParameter();

		/**
		 * The meta object literal for the '<em><b>Sub Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_PARAMETER__SUB_PARAMETERS = eINSTANCE.getSegmentParameter_SubParameters();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_PARAMETER__ID = eINSTANCE.getSegmentParameter_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_PARAMETER__DESCRIPTION = eINSTANCE.getSegmentParameter_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_PARAMETER__VALUE = eINSTANCE.getSegmentParameter_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_PARAMETER__VALUE_UNIT_OF_MEASURE = eINSTANCE.getSegmentParameter_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualPropertyImpl <em>Material Actual Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialActualPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialActualProperty()
		 * @generated
		 */
		EClass MATERIAL_ACTUAL_PROPERTY = eINSTANCE.getMaterialActualProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_ACTUAL_PROPERTY__SUB_PROPERTIES = eINSTANCE.getMaterialActualProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetActualPropertyImpl <em>Physical Asset Actual Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetActualPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetActualProperty()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_ACTUAL_PROPERTY = eINSTANCE.getPhysicalAssetActualProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_ACTUAL_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getPhysicalAssetActualProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualPropertyImpl <em>Equipment Actual Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentActualProperty()
		 * @generated
		 */
		EClass EQUIPMENT_ACTUAL_PROPERTY = eINSTANCE.getEquipmentActualProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_ACTUAL_PROPERTY__SUB_PROPERTIES = eINSTANCE.getEquipmentActualProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelActualPropertyImpl <em>Personnel Actual Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelActualPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelActualProperty()
		 * @generated
		 */
		EClass PERSONNEL_ACTUAL_PROPERTY = eINSTANCE.getPersonnelActualProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_ACTUAL_PROPERTY__SUB_PROPERTIES = eINSTANCE.getPersonnelActualProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl <em>Material Actual</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialActual()
		 * @generated
		 */
		EClass MATERIAL_ACTUAL = eINSTANCE.getMaterialActual();

		/**
		 * The meta object literal for the '<em><b>Assembled From Actuals</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_ACTUAL__ASSEMBLED_FROM_ACTUALS = eINSTANCE.getMaterialActual_AssembledFromActuals();

		/**
		 * The meta object literal for the '<em><b>Material Actual Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_ACTUAL__MATERIAL_ACTUAL_PROPERTIES = eINSTANCE.getMaterialActual_MaterialActualProperties();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_ACTUAL__DESCRIPTION = eINSTANCE.getMaterialActual_Description();

		/**
		 * The meta object literal for the '<em><b>Material Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_ACTUAL__MATERIAL_USE = eINSTANCE.getMaterialActual_MaterialUse();

		/**
		 * The meta object literal for the '<em><b>Storage Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_ACTUAL__STORAGE_LOCATION = eINSTANCE.getMaterialActual_StorageLocation();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_ACTUAL__QUANTITY = eINSTANCE.getMaterialActual_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_ACTUAL__QUANTITY_UNIT_OF_MEASURE = eINSTANCE.getMaterialActual_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Assembly Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_ACTUAL__ASSEMBLY_TYPE = eINSTANCE.getMaterialActual_AssemblyType();

		/**
		 * The meta object literal for the '<em><b>Assembly Relationship</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_ACTUAL__ASSEMBLY_RELATIONSHIP = eINSTANCE.getMaterialActual_AssemblyRelationship();

		/**
		 * The meta object literal for the '<em><b>Material Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_ACTUAL__MATERIAL_CLASSES = eINSTANCE.getMaterialActual_MaterialClasses();

		/**
		 * The meta object literal for the '<em><b>Material Definitions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_ACTUAL__MATERIAL_DEFINITIONS = eINSTANCE.getMaterialActual_MaterialDefinitions();

		/**
		 * The meta object literal for the '<em><b>Material Lots</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_ACTUAL__MATERIAL_LOTS = eINSTANCE.getMaterialActual_MaterialLots();

		/**
		 * The meta object literal for the '<em><b>Material Sublots</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_ACTUAL__MATERIAL_SUBLOTS = eINSTANCE.getMaterialActual_MaterialSublots();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetActualImpl <em>Physical Asset Actual</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetActualImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetActual()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_ACTUAL = eINSTANCE.getPhysicalAssetActual();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Actual Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_ACTUAL__PHYSICAL_ASSET_ACTUAL_PROPERTIES = eINSTANCE
				.getPhysicalAssetActual_PhysicalAssetActualProperties();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_ACTUAL__DESCRIPTION = eINSTANCE.getPhysicalAssetActual_Description();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_ACTUAL__PHYSICAL_ASSET_USE = eINSTANCE.getPhysicalAssetActual_PhysicalAssetUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_ACTUAL__QUANTITY = eINSTANCE.getPhysicalAssetActual_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_ACTUAL__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getPhysicalAssetActual_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_ACTUAL__PHYSICAL_ASSET_CLASSES = eINSTANCE
				.getPhysicalAssetActual_PhysicalAssetClasses();

		/**
		 * The meta object literal for the '<em><b>Physical Assets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_ACTUAL__PHYSICAL_ASSETS = eINSTANCE.getPhysicalAssetActual_PhysicalAssets();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualImpl <em>Equipment Actual</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentActualImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentActual()
		 * @generated
		 */
		EClass EQUIPMENT_ACTUAL = eINSTANCE.getEquipmentActual();

		/**
		 * The meta object literal for the '<em><b>Equipment Actual Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_ACTUAL__EQUIPMENT_ACTUAL_PROPERTIES = eINSTANCE
				.getEquipmentActual_EquipmentActualProperties();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_ACTUAL__DESCRIPTION = eINSTANCE.getEquipmentActual_Description();

		/**
		 * The meta object literal for the '<em><b>Equipment Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_ACTUAL__EQUIPMENT_USE = eINSTANCE.getEquipmentActual_EquipmentUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_ACTUAL__QUANTITY = eINSTANCE.getEquipmentActual_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_ACTUAL__QUANTITY_UNIT_OF_MEASURE = eINSTANCE.getEquipmentActual_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Equipment Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_ACTUAL__EQUIPMENT_CLASSES = eINSTANCE.getEquipmentActual_EquipmentClasses();

		/**
		 * The meta object literal for the '<em><b>Equipments</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_ACTUAL__EQUIPMENTS = eINSTANCE.getEquipmentActual_Equipments();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelActualImpl <em>Personnel Actual</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelActualImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelActual()
		 * @generated
		 */
		EClass PERSONNEL_ACTUAL = eINSTANCE.getPersonnelActual();

		/**
		 * The meta object literal for the '<em><b>Personnel Actual Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_ACTUAL__PERSONNEL_ACTUAL_PROPERTIES = eINSTANCE
				.getPersonnelActual_PersonnelActualProperties();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_ACTUAL__DESCRIPTION = eINSTANCE.getPersonnelActual_Description();

		/**
		 * The meta object literal for the '<em><b>Personnel Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_ACTUAL__PERSONNEL_USE = eINSTANCE.getPersonnelActual_PersonnelUse();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_ACTUAL__QUANTITY = eINSTANCE.getPersonnelActual_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_ACTUAL__QUANTITY_UNIT_OF_MEASURE = eINSTANCE.getPersonnelActual_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Personnel Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_ACTUAL__PERSONNEL_CLASSES = eINSTANCE.getPersonnelActual_PersonnelClasses();

		/**
		 * The meta object literal for the '<em><b>Persons</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_ACTUAL__PERSONS = eINSTANCE.getPersonnelActual_Persons();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentDataImpl <em>Segment Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.SegmentDataImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getSegmentData()
		 * @generated
		 */
		EClass SEGMENT_DATA = eINSTANCE.getSegmentData();

		/**
		 * The meta object literal for the '<em><b>Sub Data</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_DATA__SUB_DATA = eINSTANCE.getSegmentData_SubData();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_DATA__ID = eINSTANCE.getSegmentData_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_DATA__DESCRIPTION = eINSTANCE.getSegmentData_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_DATA__VALUE = eINSTANCE.getSegmentData_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_DATA__VALUE_UNIT_OF_MEASURE = eINSTANCE.getSegmentData_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl <em>Segment Response</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.SegmentResponseImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getSegmentResponse()
		 * @generated
		 */
		EClass SEGMENT_RESPONSE = eINSTANCE.getSegmentResponse();

		/**
		 * The meta object literal for the '<em><b>Sub Responses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_RESPONSE__SUB_RESPONSES = eINSTANCE.getSegmentResponse_SubResponses();

		/**
		 * The meta object literal for the '<em><b>Process Segment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_RESPONSE__PROCESS_SEGMENT = eINSTANCE.getSegmentResponse_ProcessSegment();

		/**
		 * The meta object literal for the '<em><b>Operations Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_RESPONSE__OPERATIONS_DEFINITION = eINSTANCE.getSegmentResponse_OperationsDefinition();

		/**
		 * The meta object literal for the '<em><b>Segment Data</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_RESPONSE__SEGMENT_DATA = eINSTANCE.getSegmentResponse_SegmentData();

		/**
		 * The meta object literal for the '<em><b>Personnel Actuals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_RESPONSE__PERSONNEL_ACTUALS = eINSTANCE.getSegmentResponse_PersonnelActuals();

		/**
		 * The meta object literal for the '<em><b>Equipment Actuals</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_RESPONSE__EQUIPMENT_ACTUALS = eINSTANCE.getSegmentResponse_EquipmentActuals();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Actuals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_RESPONSE__PHYSICAL_ASSET_ACTUALS = eINSTANCE.getSegmentResponse_PhysicalAssetActuals();

		/**
		 * The meta object literal for the '<em><b>Material Actuals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_RESPONSE__MATERIAL_ACTUALS = eINSTANCE.getSegmentResponse_MaterialActuals();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_RESPONSE__ID = eINSTANCE.getSegmentResponse_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_RESPONSE__DESCRIPTION = eINSTANCE.getSegmentResponse_Description();

		/**
		 * The meta object literal for the '<em><b>Operations Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_RESPONSE__OPERATIONS_TYPE = eINSTANCE.getSegmentResponse_OperationsType();

		/**
		 * The meta object literal for the '<em><b>Actual Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_RESPONSE__ACTUAL_START_TIME = eINSTANCE.getSegmentResponse_ActualStartTime();

		/**
		 * The meta object literal for the '<em><b>Actual End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_RESPONSE__ACTUAL_END_TIME = eINSTANCE.getSegmentResponse_ActualEndTime();

		/**
		 * The meta object literal for the '<em><b>Segment State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEGMENT_RESPONSE__SEGMENT_STATE = eINSTANCE.getSegmentResponse_SegmentState();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEGMENT_RESPONSE__HIERARCHY_SCOPE = eINSTANCE.getSegmentResponse_HierarchyScope();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl <em>Operations Response</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsResponseImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsResponse()
		 * @generated
		 */
		EClass OPERATIONS_RESPONSE = eINSTANCE.getOperationsResponse();

		/**
		 * The meta object literal for the '<em><b>Segment Responses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_RESPONSE__SEGMENT_RESPONSES = eINSTANCE.getOperationsResponse_SegmentResponses();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_RESPONSE__ID = eINSTANCE.getOperationsResponse_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_RESPONSE__DESCRIPTION = eINSTANCE.getOperationsResponse_Description();

		/**
		 * The meta object literal for the '<em><b>Operations Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_RESPONSE__OPERATIONS_TYPE = eINSTANCE.getOperationsResponse_OperationsType();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_RESPONSE__START_TIME = eINSTANCE.getOperationsResponse_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_RESPONSE__END_TIME = eINSTANCE.getOperationsResponse_EndTime();

		/**
		 * The meta object literal for the '<em><b>Response State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_RESPONSE__RESPONSE_STATE = eINSTANCE.getOperationsResponse_ResponseState();

		/**
		 * The meta object literal for the '<em><b>Operations Request</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_RESPONSE__OPERATIONS_REQUEST = eINSTANCE.getOperationsResponse_OperationsRequest();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_RESPONSE__HIERARCHY_SCOPE = eINSTANCE.getOperationsResponse_HierarchyScope();

		/**
		 * The meta object literal for the '<em><b>Operations Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_RESPONSE__OPERATIONS_DEFINITION = eINSTANCE.getOperationsResponse_OperationsDefinition();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl <em>Operations Performance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsPerformanceImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsPerformance()
		 * @generated
		 */
		EClass OPERATIONS_PERFORMANCE = eINSTANCE.getOperationsPerformance();

		/**
		 * The meta object literal for the '<em><b>Operations Responses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_PERFORMANCE__OPERATIONS_RESPONSES = eINSTANCE
				.getOperationsPerformance_OperationsResponses();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_PERFORMANCE__ID = eINSTANCE.getOperationsPerformance_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_PERFORMANCE__DESCRIPTION = eINSTANCE.getOperationsPerformance_Description();

		/**
		 * The meta object literal for the '<em><b>Operations Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_PERFORMANCE__OPERATIONS_TYPE = eINSTANCE.getOperationsPerformance_OperationsType();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_PERFORMANCE__START_TIME = eINSTANCE.getOperationsPerformance_StartTime();

		/**
		 * The meta object literal for the '<em><b>Operations Schedule</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_PERFORMANCE__OPERATIONS_SCHEDULE = eINSTANCE
				.getOperationsPerformance_OperationsSchedule();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_PERFORMANCE__END_TIME = eINSTANCE.getOperationsPerformance_EndTime();

		/**
		 * The meta object literal for the '<em><b>Performance State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_PERFORMANCE__PERFORMANCE_STATE = eINSTANCE.getOperationsPerformance_PerformanceState();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_PERFORMANCE__HIERARCHY_SCOPE = eINSTANCE.getOperationsPerformance_HierarchyScope();

		/**
		 * The meta object literal for the '<em><b>Published Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_PERFORMANCE__PUBLISHED_DATE = eINSTANCE.getOperationsPerformance_PublishedDate();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl <em>Operations Capability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.OperationsCapabilityImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsCapability()
		 * @generated
		 */
		EClass OPERATIONS_CAPABILITY = eINSTANCE.getOperationsCapability();

		/**
		 * The meta object literal for the '<em><b>Process Segment Capabilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_CAPABILITY__PROCESS_SEGMENT_CAPABILITIES = eINSTANCE
				.getOperationsCapability_ProcessSegmentCapabilities();

		/**
		 * The meta object literal for the '<em><b>Personnel Capabilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_CAPABILITY__PERSONNEL_CAPABILITIES = eINSTANCE
				.getOperationsCapability_PersonnelCapabilities();

		/**
		 * The meta object literal for the '<em><b>Equipment Capabilities</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_CAPABILITY__EQUIPMENT_CAPABILITIES = eINSTANCE
				.getOperationsCapability_EquipmentCapabilities();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Capabilities</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_CAPABILITY__PHYSICAL_ASSET_CAPABILITIES = eINSTANCE
				.getOperationsCapability_PhysicalAssetCapabilities();

		/**
		 * The meta object literal for the '<em><b>Material Capabilities</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_CAPABILITY__MATERIAL_CAPABILITIES = eINSTANCE
				.getOperationsCapability_MaterialCapabilities();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_CAPABILITY__ID = eINSTANCE.getOperationsCapability_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_CAPABILITY__DESCRIPTION = eINSTANCE.getOperationsCapability_Description();

		/**
		 * The meta object literal for the '<em><b>Capacity Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_CAPABILITY__CAPACITY_TYPE = eINSTANCE.getOperationsCapability_CapacityType();

		/**
		 * The meta object literal for the '<em><b>Reason</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_CAPABILITY__REASON = eINSTANCE.getOperationsCapability_Reason();

		/**
		 * The meta object literal for the '<em><b>Confidence Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_CAPABILITY__CONFIDENCE_FACTOR = eINSTANCE.getOperationsCapability_ConfidenceFactor();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_CAPABILITY__START_TIME = eINSTANCE.getOperationsCapability_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_CAPABILITY__END_TIME = eINSTANCE.getOperationsCapability_EndTime();

		/**
		 * The meta object literal for the '<em><b>Published Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONS_CAPABILITY__PUBLISHED_DATE = eINSTANCE.getOperationsCapability_PublishedDate();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONS_CAPABILITY__HIERARCHY_SCOPE = eINSTANCE.getOperationsCapability_HierarchyScope();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityPropertyImpl <em>Material Capability Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialCapabilityProperty()
		 * @generated
		 */
		EClass MATERIAL_CAPABILITY_PROPERTY = eINSTANCE.getMaterialCapabilityProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CAPABILITY_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getMaterialCapabilityProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityPropertyImpl <em>Physical Asset Capability Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetCapabilityProperty()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_CAPABILITY_PROPERTY = eINSTANCE.getPhysicalAssetCapabilityProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_CAPABILITY_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getPhysicalAssetCapabilityProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityPropertyImpl <em>Equipment Capability Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentCapabilityProperty()
		 * @generated
		 */
		EClass EQUIPMENT_CAPABILITY_PROPERTY = eINSTANCE.getEquipmentCapabilityProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_CAPABILITY_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getEquipmentCapabilityProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelCapabilityPropertyImpl <em>Personnel Capability Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelCapabilityPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelCapabilityProperty()
		 * @generated
		 */
		EClass PERSONNEL_CAPABILITY_PROPERTY = eINSTANCE.getPersonnelCapabilityProperty();

		/**
		 * The meta object literal for the '<em><b>Sub Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_CAPABILITY_PROPERTY__SUB_PROPERTIES = eINSTANCE
				.getPersonnelCapabilityProperty_SubProperties();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl <em>Material Capability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.MaterialCapabilityImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialCapability()
		 * @generated
		 */
		EClass MATERIAL_CAPABILITY = eINSTANCE.getMaterialCapability();

		/**
		 * The meta object literal for the '<em><b>Assembled From Capabilities</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CAPABILITY__ASSEMBLED_FROM_CAPABILITIES = eINSTANCE
				.getMaterialCapability_AssembledFromCapabilities();

		/**
		 * The meta object literal for the '<em><b>Material Capability Property</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CAPABILITY__MATERIAL_CAPABILITY_PROPERTY = eINSTANCE
				.getMaterialCapability_MaterialCapabilityProperty();

		/**
		 * The meta object literal for the '<em><b>Material Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CAPABILITY__MATERIAL_CLASS = eINSTANCE.getMaterialCapability_MaterialClass();

		/**
		 * The meta object literal for the '<em><b>Material Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CAPABILITY__MATERIAL_DEFINITION = eINSTANCE.getMaterialCapability_MaterialDefinition();

		/**
		 * The meta object literal for the '<em><b>Material Lot</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CAPABILITY__MATERIAL_LOT = eINSTANCE.getMaterialCapability_MaterialLot();

		/**
		 * The meta object literal for the '<em><b>Material Sublot</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CAPABILITY__MATERIAL_SUBLOT = eINSTANCE.getMaterialCapability_MaterialSublot();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CAPABILITY__DESCRIPTION = eINSTANCE.getMaterialCapability_Description();

		/**
		 * The meta object literal for the '<em><b>Capability Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CAPABILITY__CAPABILITY_TYPE = eINSTANCE.getMaterialCapability_CapabilityType();

		/**
		 * The meta object literal for the '<em><b>Reason</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CAPABILITY__REASON = eINSTANCE.getMaterialCapability_Reason();

		/**
		 * The meta object literal for the '<em><b>Confidence Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CAPABILITY__CONFIDENCE_FACTOR = eINSTANCE.getMaterialCapability_ConfidenceFactor();

		/**
		 * The meta object literal for the '<em><b>Material Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CAPABILITY__MATERIAL_USE = eINSTANCE.getMaterialCapability_MaterialUse();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CAPABILITY__START_TIME = eINSTANCE.getMaterialCapability_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CAPABILITY__END_TIME = eINSTANCE.getMaterialCapability_EndTime();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CAPABILITY__QUANTITY = eINSTANCE.getMaterialCapability_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CAPABILITY__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getMaterialCapability_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Hierarchyscope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_CAPABILITY__HIERARCHYSCOPE = eINSTANCE.getMaterialCapability_Hierarchyscope();

		/**
		 * The meta object literal for the '<em><b>Assembly Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CAPABILITY__ASSEMBLY_TYPE = eINSTANCE.getMaterialCapability_AssemblyType();

		/**
		 * The meta object literal for the '<em><b>Assembly Relationship</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_CAPABILITY__ASSEMBLY_RELATIONSHIP = eINSTANCE.getMaterialCapability_AssemblyRelationship();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl <em>Physical Asset Capability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetCapabilityImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPhysicalAssetCapability()
		 * @generated
		 */
		EClass PHYSICAL_ASSET_CAPABILITY = eINSTANCE.getPhysicalAssetCapability();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Capability Property</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CAPABILITY_PROPERTY = eINSTANCE
				.getPhysicalAssetCapability_PhysicalAssetCapabilityProperty();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_CLASS = eINSTANCE
				.getPhysicalAssetCapability_PhysicalAssetClass();

		/**
		 * The meta object literal for the '<em><b>Physical Asset</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET = eINSTANCE.getPhysicalAssetCapability_PhysicalAsset();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY__DESCRIPTION = eINSTANCE.getPhysicalAssetCapability_Description();

		/**
		 * The meta object literal for the '<em><b>Capability Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY__CAPABILITY_TYPE = eINSTANCE.getPhysicalAssetCapability_CapabilityType();

		/**
		 * The meta object literal for the '<em><b>Reason</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY__REASON = eINSTANCE.getPhysicalAssetCapability_Reason();

		/**
		 * The meta object literal for the '<em><b>Confidence Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY__CONFIDENCE_FACTOR = eINSTANCE
				.getPhysicalAssetCapability_ConfidenceFactor();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY__PHYSICAL_ASSET_USE = eINSTANCE
				.getPhysicalAssetCapability_PhysicalAssetUse();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_ASSET_CAPABILITY__HIERARCHY_SCOPE = eINSTANCE.getPhysicalAssetCapability_HierarchyScope();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY__START_TIME = eINSTANCE.getPhysicalAssetCapability_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY__END_TIME = eINSTANCE.getPhysicalAssetCapability_EndTime();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY__QUANTITY = eINSTANCE.getPhysicalAssetCapability_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_ASSET_CAPABILITY__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getPhysicalAssetCapability_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl <em>Equipment Capability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.EquipmentCapabilityImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getEquipmentCapability()
		 * @generated
		 */
		EClass EQUIPMENT_CAPABILITY = eINSTANCE.getEquipmentCapability();

		/**
		 * The meta object literal for the '<em><b>Equipment Capability Property</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_CAPABILITY__EQUIPMENT_CAPABILITY_PROPERTY = eINSTANCE
				.getEquipmentCapability_EquipmentCapabilityProperty();

		/**
		 * The meta object literal for the '<em><b>Equipment Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_CAPABILITY__EQUIPMENT_CLASS = eINSTANCE.getEquipmentCapability_EquipmentClass();

		/**
		 * The meta object literal for the '<em><b>Equipment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_CAPABILITY__EQUIPMENT = eINSTANCE.getEquipmentCapability_Equipment();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY__DESCRIPTION = eINSTANCE.getEquipmentCapability_Description();

		/**
		 * The meta object literal for the '<em><b>Capability Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY__CAPABILITY_TYPE = eINSTANCE.getEquipmentCapability_CapabilityType();

		/**
		 * The meta object literal for the '<em><b>Reason</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY__REASON = eINSTANCE.getEquipmentCapability_Reason();

		/**
		 * The meta object literal for the '<em><b>Confidence Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY__CONFIDENCE_FACTOR = eINSTANCE.getEquipmentCapability_ConfidenceFactor();

		/**
		 * The meta object literal for the '<em><b>Equipment Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY__EQUIPMENT_USE = eINSTANCE.getEquipmentCapability_EquipmentUse();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY__START_TIME = eINSTANCE.getEquipmentCapability_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY__END_TIME = eINSTANCE.getEquipmentCapability_EndTime();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY__QUANTITY = eINSTANCE.getEquipmentCapability_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EQUIPMENT_CAPABILITY__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getEquipmentCapability_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQUIPMENT_CAPABILITY__HIERARCHY_SCOPE = eINSTANCE.getEquipmentCapability_HierarchyScope();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.PersonnelCapabilityImpl <em>Personnel Capability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.PersonnelCapabilityImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPersonnelCapability()
		 * @generated
		 */
		EClass PERSONNEL_CAPABILITY = eINSTANCE.getPersonnelCapability();

		/**
		 * The meta object literal for the '<em><b>Personnel Capability Property</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_CAPABILITY__PERSONNEL_CAPABILITY_PROPERTY = eINSTANCE
				.getPersonnelCapability_PersonnelCapabilityProperty();

		/**
		 * The meta object literal for the '<em><b>Personnel Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_CAPABILITY__PERSONNEL_CLASS = eINSTANCE.getPersonnelCapability_PersonnelClass();

		/**
		 * The meta object literal for the '<em><b>Person</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_CAPABILITY__PERSON = eINSTANCE.getPersonnelCapability_Person();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CAPABILITY__DESCRIPTION = eINSTANCE.getPersonnelCapability_Description();

		/**
		 * The meta object literal for the '<em><b>Capability Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CAPABILITY__CAPABILITY_TYPE = eINSTANCE.getPersonnelCapability_CapabilityType();

		/**
		 * The meta object literal for the '<em><b>Reason</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CAPABILITY__REASON = eINSTANCE.getPersonnelCapability_Reason();

		/**
		 * The meta object literal for the '<em><b>Confidence Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CAPABILITY__CONFIDENCE_FACTOR = eINSTANCE.getPersonnelCapability_ConfidenceFactor();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSONNEL_CAPABILITY__HIERARCHY_SCOPE = eINSTANCE.getPersonnelCapability_HierarchyScope();

		/**
		 * The meta object literal for the '<em><b>Personnel Use</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CAPABILITY__PERSONNEL_USE = eINSTANCE.getPersonnelCapability_PersonnelUse();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CAPABILITY__START_TIME = eINSTANCE.getPersonnelCapability_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CAPABILITY__END_TIME = eINSTANCE.getPersonnelCapability_EndTime();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CAPABILITY__QUANTITY = eINSTANCE.getPersonnelCapability_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSONNEL_CAPABILITY__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getPersonnelCapability_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentCapabilityImpl <em>Process Segment Capability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentCapabilityImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getProcessSegmentCapability()
		 * @generated
		 */
		EClass PROCESS_SEGMENT_CAPABILITY = eINSTANCE.getProcessSegmentCapability();

		/**
		 * The meta object literal for the '<em><b>Sub Capabilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_CAPABILITY__SUB_CAPABILITIES = eINSTANCE
				.getProcessSegmentCapability_SubCapabilities();

		/**
		 * The meta object literal for the '<em><b>Personnel Capabilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_CAPABILITY__PERSONNEL_CAPABILITIES = eINSTANCE
				.getProcessSegmentCapability_PersonnelCapabilities();

		/**
		 * The meta object literal for the '<em><b>Equipment Capabilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_CAPABILITY__EQUIPMENT_CAPABILITIES = eINSTANCE
				.getProcessSegmentCapability_EquipmentCapabilities();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Capabilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_CAPABILITY__PHYSICAL_ASSET_CAPABILITIES = eINSTANCE
				.getProcessSegmentCapability_PhysicalAssetCapabilities();

		/**
		 * The meta object literal for the '<em><b>Material Capabilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_CAPABILITY__MATERIAL_CAPABILITIES = eINSTANCE
				.getProcessSegmentCapability_MaterialCapabilities();

		/**
		 * The meta object literal for the '<em><b>Process Segment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_CAPABILITY__PROCESS_SEGMENT = eINSTANCE.getProcessSegmentCapability_ProcessSegment();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_CAPABILITY__ID = eINSTANCE.getProcessSegmentCapability_Id();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_CAPABILITY__DESCRIPTION = eINSTANCE.getProcessSegmentCapability_Description();

		/**
		 * The meta object literal for the '<em><b>Capacity Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_CAPABILITY__CAPACITY_TYPE = eINSTANCE.getProcessSegmentCapability_CapacityType();

		/**
		 * The meta object literal for the '<em><b>Reason</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_CAPABILITY__REASON = eINSTANCE.getProcessSegmentCapability_Reason();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_CAPABILITY__START_TIME = eINSTANCE.getProcessSegmentCapability_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_SEGMENT_CAPABILITY__END_TIME = eINSTANCE.getProcessSegmentCapability_EndTime();

		/**
		 * The meta object literal for the '<em><b>Hierarchy Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS_SEGMENT_CAPABILITY__HIERARCHY_SCOPE = eINSTANCE.getProcessSegmentCapability_HierarchyScope();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ReferencialPropertyImpl <em>Referencial Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ReferencialPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getReferencialProperty()
		 * @generated
		 */
		EClass REFERENCIAL_PROPERTY = eINSTANCE.getReferencialProperty();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERENCIAL_PROPERTY__DESCRIPTION = eINSTANCE.getReferencialProperty_Description();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERENCIAL_PROPERTY__VALUE = eINSTANCE.getReferencialProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Value Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERENCIAL_PROPERTY__VALUE_UNIT_OF_MEASURE = eINSTANCE.getReferencialProperty_ValueUnitOfMeasure();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERENCIAL_PROPERTY__QUANTITY = eINSTANCE.getReferencialProperty_Quantity();

		/**
		 * The meta object literal for the '<em><b>Quantity Unit Of Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERENCIAL_PROPERTY__QUANTITY_UNIT_OF_MEASURE = eINSTANCE
				.getReferencialProperty_QuantityUnitOfMeasure();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialPersonnelPropertyImpl <em>Referential Personnel Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ReferentialPersonnelPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getReferentialPersonnelProperty()
		 * @generated
		 */
		EClass REFERENTIAL_PERSONNEL_PROPERTY = eINSTANCE.getReferentialPersonnelProperty();

		/**
		 * The meta object literal for the '<em><b>Personnel Class Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENTIAL_PERSONNEL_PROPERTY__PERSONNEL_CLASS_PROPERTY = eINSTANCE
				.getReferentialPersonnelProperty_PersonnelClassProperty();

		/**
		 * The meta object literal for the '<em><b>Person Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENTIAL_PERSONNEL_PROPERTY__PERSON_PROPERTY = eINSTANCE
				.getReferentialPersonnelProperty_PersonProperty();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialEquipmentPropertyImpl <em>Referential Equipment Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ReferentialEquipmentPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getReferentialEquipmentProperty()
		 * @generated
		 */
		EClass REFERENTIAL_EQUIPMENT_PROPERTY = eINSTANCE.getReferentialEquipmentProperty();

		/**
		 * The meta object literal for the '<em><b>Equipment Class Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_CLASS_PROPERTY = eINSTANCE
				.getReferentialEquipmentProperty_EquipmentClassProperty();

		/**
		 * The meta object literal for the '<em><b>Equipment Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENTIAL_EQUIPMENT_PROPERTY__EQUIPMENT_PROPERTY = eINSTANCE
				.getReferentialEquipmentProperty_EquipmentProperty();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialPhysicalAssetPropertyImpl <em>Referential Physical Asset Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ReferentialPhysicalAssetPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getReferentialPhysicalAssetProperty()
		 * @generated
		 */
		EClass REFERENTIAL_PHYSICAL_ASSET_PROPERTY = eINSTANCE.getReferentialPhysicalAssetProperty();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Class Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_CLASS_PROPERTY = eINSTANCE
				.getReferentialPhysicalAssetProperty_PhysicalAssetClassProperty();

		/**
		 * The meta object literal for the '<em><b>Physical Asset Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENTIAL_PHYSICAL_ASSET_PROPERTY__PHYSICAL_ASSET_PROPERTY = eINSTANCE
				.getReferentialPhysicalAssetProperty_PhysicalAssetProperty();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialMaterialTypePropertyImpl <em>Referential Material Type Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ReferentialMaterialTypePropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getReferentialMaterialTypeProperty()
		 * @generated
		 */
		EClass REFERENTIAL_MATERIAL_TYPE_PROPERTY = eINSTANCE.getReferentialMaterialTypeProperty();

		/**
		 * The meta object literal for the '<em><b>Material Class Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_CLASS_PROPERTY = eINSTANCE
				.getReferentialMaterialTypeProperty_MaterialClassProperty();

		/**
		 * The meta object literal for the '<em><b>Material Definition Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_DEFINITION_PROPERTY = eINSTANCE
				.getReferentialMaterialTypeProperty_MaterialDefinitionProperty();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialMaterialPropertyImpl <em>Referential Material Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.impl.ReferentialMaterialPropertyImpl
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getReferentialMaterialProperty()
		 * @generated
		 */
		EClass REFERENTIAL_MATERIAL_PROPERTY = eINSTANCE.getReferentialMaterialProperty();

		/**
		 * The meta object literal for the '<em><b>Material Lot Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENTIAL_MATERIAL_PROPERTY__MATERIAL_LOT_PROPERTY = eINSTANCE
				.getReferentialMaterialProperty_MaterialLotProperty();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.AssemblyType <em>Assembly Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.AssemblyType
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getAssemblyType()
		 * @generated
		 */
		EEnum ASSEMBLY_TYPE = eINSTANCE.getAssemblyType();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.AssemblyRelationship <em>Assembly Relationship</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.AssemblyRelationship
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getAssemblyRelationship()
		 * @generated
		 */
		EEnum ASSEMBLY_RELATIONSHIP = eINSTANCE.getAssemblyRelationship();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.OperationsType <em>Operations Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.OperationsType
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getOperationsType()
		 * @generated
		 */
		EEnum OPERATIONS_TYPE = eINSTANCE.getOperationsType();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.MaterialUse <em>Material Use</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.MaterialUse
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getMaterialUse()
		 * @generated
		 */
		EEnum MATERIAL_USE = eINSTANCE.getMaterialUse();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.UseType <em>Use Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.UseType
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getUseType()
		 * @generated
		 */
		EEnum USE_TYPE = eINSTANCE.getUseType();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.ScheduleState <em>Schedule State</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.ScheduleState
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getScheduleState()
		 * @generated
		 */
		EEnum SCHEDULE_STATE = eINSTANCE.getScheduleState();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.PerformanceState <em>Performance State</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.PerformanceState
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getPerformanceState()
		 * @generated
		 */
		EEnum PERFORMANCE_STATE = eINSTANCE.getPerformanceState();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.CapabilityType <em>Capability Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.CapabilityType
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getCapabilityType()
		 * @generated
		 */
		EEnum CAPABILITY_TYPE = eINSTANCE.getCapabilityType();

		/**
		 * The meta object literal for the '{@link at.ac.tuwien.big.ame.iec62264.CapacityType <em>Capacity Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see at.ac.tuwien.big.ame.iec62264.CapacityType
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getCapacityType()
		 * @generated
		 */
		EEnum CAPACITY_TYPE = eINSTANCE.getCapacityType();

		/**
		 * The meta object literal for the '<em>Zoned Date Time</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.time.ZonedDateTime
		 * @see at.ac.tuwien.big.ame.iec62264.impl.Iec62264PackageImpl#getZonedDateTime()
		 * @generated
		 */
		EDataType ZONED_DATE_TIME = eINSTANCE.getZonedDateTime();

	}

} //Iec62264Package
