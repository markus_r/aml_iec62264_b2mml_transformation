/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getPersonnelModel <em>Personnel Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getEquipmentModel <em>Equipment Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getPhysicalAssetModel <em>Physical Asset Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getMaterialModel <em>Material Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getProcessSegmentModel <em>Process Segment Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getOperationsDefinitionModel <em>Operations Definition Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getOperationsScheduleModel <em>Operations Schedule Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getOperationsPerformanceModel <em>Operations Performance Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getProcessSegmentCapabilityModel <em>Process Segment Capability Model</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getHierarchyScopes <em>Hierarchy Scopes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getIec62264Model()
 * @model
 * @generated
 */
public interface Iec62264Model extends EObject {
	/**
	 * Returns the value of the '<em><b>Personnel Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Model</em>' containment reference.
	 * @see #setPersonnelModel(PersonnelModel)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getIec62264Model_PersonnelModel()
	 * @model containment="true"
	 * @generated
	 */
	PersonnelModel getPersonnelModel();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getPersonnelModel <em>Personnel Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Personnel Model</em>' containment reference.
	 * @see #getPersonnelModel()
	 * @generated
	 */
	void setPersonnelModel(PersonnelModel value);

	/**
	 * Returns the value of the '<em><b>Equipment Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Model</em>' containment reference.
	 * @see #setEquipmentModel(EquipmentModel)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getIec62264Model_EquipmentModel()
	 * @model containment="true"
	 * @generated
	 */
	EquipmentModel getEquipmentModel();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getEquipmentModel <em>Equipment Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Model</em>' containment reference.
	 * @see #getEquipmentModel()
	 * @generated
	 */
	void setEquipmentModel(EquipmentModel value);

	/**
	 * Returns the value of the '<em><b>Physical Asset Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Model</em>' containment reference.
	 * @see #setPhysicalAssetModel(PhysicalAssetModel)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getIec62264Model_PhysicalAssetModel()
	 * @model containment="true"
	 * @generated
	 */
	PhysicalAssetModel getPhysicalAssetModel();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getPhysicalAssetModel <em>Physical Asset Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Model</em>' containment reference.
	 * @see #getPhysicalAssetModel()
	 * @generated
	 */
	void setPhysicalAssetModel(PhysicalAssetModel value);

	/**
	 * Returns the value of the '<em><b>Material Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Model</em>' containment reference.
	 * @see #setMaterialModel(MaterialModel)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getIec62264Model_MaterialModel()
	 * @model containment="true"
	 * @generated
	 */
	MaterialModel getMaterialModel();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getMaterialModel <em>Material Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Model</em>' containment reference.
	 * @see #getMaterialModel()
	 * @generated
	 */
	void setMaterialModel(MaterialModel value);

	/**
	 * Returns the value of the '<em><b>Process Segment Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment Model</em>' containment reference.
	 * @see #setProcessSegmentModel(ProcessSegmentModel)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getIec62264Model_ProcessSegmentModel()
	 * @model containment="true"
	 * @generated
	 */
	ProcessSegmentModel getProcessSegmentModel();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getProcessSegmentModel <em>Process Segment Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Segment Model</em>' containment reference.
	 * @see #getProcessSegmentModel()
	 * @generated
	 */
	void setProcessSegmentModel(ProcessSegmentModel value);

	/**
	 * Returns the value of the '<em><b>Operations Definition Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition Model</em>' containment reference.
	 * @see #setOperationsDefinitionModel(OperationsDefinitionModel)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getIec62264Model_OperationsDefinitionModel()
	 * @model containment="true"
	 * @generated
	 */
	OperationsDefinitionModel getOperationsDefinitionModel();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getOperationsDefinitionModel <em>Operations Definition Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Definition Model</em>' containment reference.
	 * @see #getOperationsDefinitionModel()
	 * @generated
	 */
	void setOperationsDefinitionModel(OperationsDefinitionModel value);

	/**
	 * Returns the value of the '<em><b>Operations Schedule Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Schedule Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Schedule Model</em>' containment reference.
	 * @see #setOperationsScheduleModel(OperationsScheduleModel)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getIec62264Model_OperationsScheduleModel()
	 * @model containment="true"
	 * @generated
	 */
	OperationsScheduleModel getOperationsScheduleModel();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getOperationsScheduleModel <em>Operations Schedule Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Schedule Model</em>' containment reference.
	 * @see #getOperationsScheduleModel()
	 * @generated
	 */
	void setOperationsScheduleModel(OperationsScheduleModel value);

	/**
	 * Returns the value of the '<em><b>Operations Performance Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Performance Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Performance Model</em>' containment reference.
	 * @see #setOperationsPerformanceModel(OperationsPerformanceModel)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getIec62264Model_OperationsPerformanceModel()
	 * @model containment="true"
	 * @generated
	 */
	OperationsPerformanceModel getOperationsPerformanceModel();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getOperationsPerformanceModel <em>Operations Performance Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Performance Model</em>' containment reference.
	 * @see #getOperationsPerformanceModel()
	 * @generated
	 */
	void setOperationsPerformanceModel(OperationsPerformanceModel value);

	/**
	 * Returns the value of the '<em><b>Process Segment Capability Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment Capability Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment Capability Model</em>' containment reference.
	 * @see #setProcessSegmentCapabilityModel(ProcessSegmentCapabilityModel)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getIec62264Model_ProcessSegmentCapabilityModel()
	 * @model containment="true"
	 * @generated
	 */
	ProcessSegmentCapabilityModel getProcessSegmentCapabilityModel();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getProcessSegmentCapabilityModel <em>Process Segment Capability Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Segment Capability Model</em>' containment reference.
	 * @see #getProcessSegmentCapabilityModel()
	 * @generated
	 */
	void setProcessSegmentCapabilityModel(ProcessSegmentCapabilityModel value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scopes</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.HierarchyScope}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scopes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scopes</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getIec62264Model_HierarchyScopes()
	 * @model containment="true"
	 * @generated
	 */
	EList<HierarchyScope> getHierarchyScopes();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getIec62264Model_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.Iec62264Model#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Iec62264Model
