/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.PhysicalAsset;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetClass;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecificationProperty;

import java.math.BigDecimal;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Segment Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationImpl#getPhysicalAssetClasses <em>Physical Asset Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationImpl#getPhysicalAssets <em>Physical Assets</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationImpl#getPhysicalAssetUse <em>Physical Asset Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PhysicalAssetSegmentSpecificationImpl#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalAssetSegmentSpecificationImpl extends MinimalEObjectImpl.Container
		implements PhysicalAssetSegmentSpecification {
	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetSegmentSpecificationProperty> properties;

	/**
	 * The cached value of the '{@link #getPhysicalAssetClasses() <em>Physical Asset Classes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetClass> physicalAssetClasses;

	/**
	 * The cached value of the '{@link #getPhysicalAssets() <em>Physical Assets</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssets()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAsset> physicalAssets;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPhysicalAssetUse() <em>Physical Asset Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetUse()
	 * @generated
	 * @ordered
	 */
	protected static final String PHYSICAL_ASSET_USE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPhysicalAssetUse() <em>Physical Asset Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetUse()
	 * @generated
	 * @ordered
	 */
	protected String physicalAssetUse = PHYSICAL_ASSET_USE_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal QUANTITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal quantity = QUANTITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String QUANTITY_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String quantityUnitOfMeasure = QUANTITY_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetSegmentSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PHYSICAL_ASSET_SEGMENT_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetSegmentSpecificationProperty> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<PhysicalAssetSegmentSpecificationProperty>(
					PhysicalAssetSegmentSpecificationProperty.class, this,
					Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetClass> getPhysicalAssetClasses() {
		if (physicalAssetClasses == null) {
			physicalAssetClasses = new EObjectResolvingEList<PhysicalAssetClass>(PhysicalAssetClass.class, this,
					Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_CLASSES);
		}
		return physicalAssetClasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAsset> getPhysicalAssets() {
		if (physicalAssets == null) {
			physicalAssets = new EObjectResolvingEList<PhysicalAsset>(PhysicalAsset.class, this,
					Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSETS);
		}
		return physicalAssets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPhysicalAssetUse() {
		return physicalAssetUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetUse(String newPhysicalAssetUse) {
		String oldPhysicalAssetUse = physicalAssetUse;
		physicalAssetUse = newPhysicalAssetUse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_USE, oldPhysicalAssetUse,
					physicalAssetUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(BigDecimal newQuantity) {
		BigDecimal oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY, oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQuantityUnitOfMeasure() {
		return quantityUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantityUnitOfMeasure(String newQuantityUnitOfMeasure) {
		String oldQuantityUnitOfMeasure = quantityUnitOfMeasure;
		quantityUnitOfMeasure = newQuantityUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE,
					oldQuantityUnitOfMeasure, quantityUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PROPERTIES:
			return ((InternalEList<?>) getProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PROPERTIES:
			return getProperties();
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_CLASSES:
			return getPhysicalAssetClasses();
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSETS:
			return getPhysicalAssets();
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__DESCRIPTION:
			return getDescription();
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_USE:
			return getPhysicalAssetUse();
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY:
			return getQuantity();
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			return getQuantityUnitOfMeasure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PROPERTIES:
			getProperties().clear();
			getProperties().addAll((Collection<? extends PhysicalAssetSegmentSpecificationProperty>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_CLASSES:
			getPhysicalAssetClasses().clear();
			getPhysicalAssetClasses().addAll((Collection<? extends PhysicalAssetClass>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSETS:
			getPhysicalAssets().clear();
			getPhysicalAssets().addAll((Collection<? extends PhysicalAsset>) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_USE:
			setPhysicalAssetUse((String) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY:
			setQuantity((BigDecimal) newValue);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PROPERTIES:
			getProperties().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_CLASSES:
			getPhysicalAssetClasses().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSETS:
			getPhysicalAssets().clear();
			return;
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_USE:
			setPhysicalAssetUse(PHYSICAL_ASSET_USE_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure(QUANTITY_UNIT_OF_MEASURE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PROPERTIES:
			return properties != null && !properties.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_CLASSES:
			return physicalAssetClasses != null && !physicalAssetClasses.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSETS:
			return physicalAssets != null && !physicalAssets.isEmpty();
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__PHYSICAL_ASSET_USE:
			return PHYSICAL_ASSET_USE_EDEFAULT == null ? physicalAssetUse != null
					: !PHYSICAL_ASSET_USE_EDEFAULT.equals(physicalAssetUse);
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY:
			return QUANTITY_EDEFAULT == null ? quantity != null : !QUANTITY_EDEFAULT.equals(quantity);
		case Iec62264Package.PHYSICAL_ASSET_SEGMENT_SPECIFICATION__QUANTITY_UNIT_OF_MEASURE:
			return QUANTITY_UNIT_OF_MEASURE_EDEFAULT == null ? quantityUnitOfMeasure != null
					: !QUANTITY_UNIT_OF_MEASURE_EDEFAULT.equals(quantityUnitOfMeasure);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", physicalAssetUse: ");
		result.append(physicalAssetUse);
		result.append(", quantity: ");
		result.append(quantity);
		result.append(", quantityUnitOfMeasure: ");
		result.append(quantityUnitOfMeasure);
		result.append(')');
		return result.toString();
	}

} //PhysicalAssetSegmentSpecificationImpl
