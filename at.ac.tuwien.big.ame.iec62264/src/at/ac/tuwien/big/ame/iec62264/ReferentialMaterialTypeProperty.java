/**
 */
package at.ac.tuwien.big.ame.iec62264;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Referential Material Type Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty#getMaterialClassProperty <em>Material Class Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty#getMaterialDefinitionProperty <em>Material Definition Property</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialMaterialTypeProperty()
 * @model abstract="true"
 * @generated
 */
public interface ReferentialMaterialTypeProperty extends ReferencialProperty {
	/**
	 * Returns the value of the '<em><b>Material Class Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Class Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Class Property</em>' reference.
	 * @see #setMaterialClassProperty(MaterialClassProperty)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialMaterialTypeProperty_MaterialClassProperty()
	 * @model
	 * @generated
	 */
	MaterialClassProperty getMaterialClassProperty();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty#getMaterialClassProperty <em>Material Class Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Class Property</em>' reference.
	 * @see #getMaterialClassProperty()
	 * @generated
	 */
	void setMaterialClassProperty(MaterialClassProperty value);

	/**
	 * Returns the value of the '<em><b>Material Definition Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Definition Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Definition Property</em>' reference.
	 * @see #setMaterialDefinitionProperty(MaterialDefinitionProperty)
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getReferentialMaterialTypeProperty_MaterialDefinitionProperty()
	 * @model
	 * @generated
	 */
	MaterialDefinitionProperty getMaterialDefinitionProperty();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty#getMaterialDefinitionProperty <em>Material Definition Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Definition Property</em>' reference.
	 * @see #getMaterialDefinitionProperty()
	 * @generated
	 */
	void setMaterialDefinitionProperty(MaterialDefinitionProperty value);

} // ReferentialMaterialTypeProperty
