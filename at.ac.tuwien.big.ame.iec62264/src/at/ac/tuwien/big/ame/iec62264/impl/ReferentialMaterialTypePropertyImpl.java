/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialClassProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialDefinitionProperty;
import at.ac.tuwien.big.ame.iec62264.ReferentialMaterialTypeProperty;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Referential Material Type Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialMaterialTypePropertyImpl#getMaterialClassProperty <em>Material Class Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ReferentialMaterialTypePropertyImpl#getMaterialDefinitionProperty <em>Material Definition Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ReferentialMaterialTypePropertyImpl extends ReferencialPropertyImpl
		implements ReferentialMaterialTypeProperty {
	/**
	 * The cached value of the '{@link #getMaterialClassProperty() <em>Material Class Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClassProperty()
	 * @generated
	 * @ordered
	 */
	protected MaterialClassProperty materialClassProperty;

	/**
	 * The cached value of the '{@link #getMaterialDefinitionProperty() <em>Material Definition Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinitionProperty()
	 * @generated
	 * @ordered
	 */
	protected MaterialDefinitionProperty materialDefinitionProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferentialMaterialTypePropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.REFERENTIAL_MATERIAL_TYPE_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialClassProperty getMaterialClassProperty() {
		if (materialClassProperty != null && materialClassProperty.eIsProxy()) {
			InternalEObject oldMaterialClassProperty = (InternalEObject) materialClassProperty;
			materialClassProperty = (MaterialClassProperty) eResolveProxy(oldMaterialClassProperty);
			if (materialClassProperty != oldMaterialClassProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_CLASS_PROPERTY,
							oldMaterialClassProperty, materialClassProperty));
			}
		}
		return materialClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialClassProperty basicGetMaterialClassProperty() {
		return materialClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialClassProperty(MaterialClassProperty newMaterialClassProperty) {
		MaterialClassProperty oldMaterialClassProperty = materialClassProperty;
		materialClassProperty = newMaterialClassProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_CLASS_PROPERTY,
					oldMaterialClassProperty, materialClassProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialDefinitionProperty getMaterialDefinitionProperty() {
		if (materialDefinitionProperty != null && materialDefinitionProperty.eIsProxy()) {
			InternalEObject oldMaterialDefinitionProperty = (InternalEObject) materialDefinitionProperty;
			materialDefinitionProperty = (MaterialDefinitionProperty) eResolveProxy(oldMaterialDefinitionProperty);
			if (materialDefinitionProperty != oldMaterialDefinitionProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_DEFINITION_PROPERTY,
							oldMaterialDefinitionProperty, materialDefinitionProperty));
			}
		}
		return materialDefinitionProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialDefinitionProperty basicGetMaterialDefinitionProperty() {
		return materialDefinitionProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialDefinitionProperty(MaterialDefinitionProperty newMaterialDefinitionProperty) {
		MaterialDefinitionProperty oldMaterialDefinitionProperty = materialDefinitionProperty;
		materialDefinitionProperty = newMaterialDefinitionProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_DEFINITION_PROPERTY,
					oldMaterialDefinitionProperty, materialDefinitionProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_CLASS_PROPERTY:
			if (resolve)
				return getMaterialClassProperty();
			return basicGetMaterialClassProperty();
		case Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_DEFINITION_PROPERTY:
			if (resolve)
				return getMaterialDefinitionProperty();
			return basicGetMaterialDefinitionProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_CLASS_PROPERTY:
			setMaterialClassProperty((MaterialClassProperty) newValue);
			return;
		case Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_DEFINITION_PROPERTY:
			setMaterialDefinitionProperty((MaterialDefinitionProperty) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_CLASS_PROPERTY:
			setMaterialClassProperty((MaterialClassProperty) null);
			return;
		case Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_DEFINITION_PROPERTY:
			setMaterialDefinitionProperty((MaterialDefinitionProperty) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_CLASS_PROPERTY:
			return materialClassProperty != null;
		case Iec62264Package.REFERENTIAL_MATERIAL_TYPE_PROPERTY__MATERIAL_DEFINITION_PROPERTY:
			return materialDefinitionProperty != null;
		}
		return super.eIsSet(featureID);
	}

} //ReferentialMaterialTypePropertyImpl
