/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.RequestedSegmentResponse;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requested Segment Response</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequestedSegmentResponseImpl extends SegmentResponseImpl implements RequestedSegmentResponse {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequestedSegmentResponseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.REQUESTED_SEGMENT_RESPONSE;
	}

} //RequestedSegmentResponseImpl
