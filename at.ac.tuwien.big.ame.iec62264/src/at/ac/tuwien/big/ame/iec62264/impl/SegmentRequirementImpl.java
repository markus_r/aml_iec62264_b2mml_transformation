/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.EquipmentRequirement;
import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialRequirement;
import at.ac.tuwien.big.ame.iec62264.OperationsDefinition;
import at.ac.tuwien.big.ame.iec62264.OperationsType;
import at.ac.tuwien.big.ame.iec62264.PersonnelRequirement;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetRequirement;
import at.ac.tuwien.big.ame.iec62264.ProcessSegment;
import at.ac.tuwien.big.ame.iec62264.ScheduleState;
import at.ac.tuwien.big.ame.iec62264.SegmentParameter;
import at.ac.tuwien.big.ame.iec62264.SegmentRequirement;

import java.math.BigDecimal;

import java.time.ZonedDateTime;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Segment Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getSubRequirements <em>Sub Requirements</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getProcessSegment <em>Process Segment</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getOperationsDefinition <em>Operations Definition</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getSegmentParameters <em>Segment Parameters</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getPersonnelRequirements <em>Personnel Requirements</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getEquipmentRequirements <em>Equipment Requirements</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getPhysicalAssetRequirements <em>Physical Asset Requirements</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getMaterialRequirements <em>Material Requirements</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getEarliestStartTime <em>Earliest Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getLatestStartTime <em>Latest Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getDuration <em>Duration</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getDurationUnitOfMeasurement <em>Duration Unit Of Measurement</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getSegmentState <em>Segment State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.SegmentRequirementImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SegmentRequirementImpl extends MinimalEObjectImpl.Container implements SegmentRequirement {
	/**
	 * The cached value of the '{@link #getSubRequirements() <em>Sub Requirements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubRequirements()
	 * @generated
	 * @ordered
	 */
	protected EList<SegmentRequirement> subRequirements;

	/**
	 * The cached value of the '{@link #getProcessSegment() <em>Process Segment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegment()
	 * @generated
	 * @ordered
	 */
	protected ProcessSegment processSegment;

	/**
	 * The cached value of the '{@link #getOperationsDefinition() <em>Operations Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsDefinition()
	 * @generated
	 * @ordered
	 */
	protected OperationsDefinition operationsDefinition;

	/**
	 * The cached value of the '{@link #getSegmentParameters() <em>Segment Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<SegmentParameter> segmentParameters;

	/**
	 * The cached value of the '{@link #getPersonnelRequirements() <em>Personnel Requirements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelRequirements()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelRequirement> personnelRequirements;

	/**
	 * The cached value of the '{@link #getEquipmentRequirements() <em>Equipment Requirements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentRequirements()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentRequirement> equipmentRequirements;

	/**
	 * The cached value of the '{@link #getPhysicalAssetRequirements() <em>Physical Asset Requirements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetRequirements()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetRequirement> physicalAssetRequirements;

	/**
	 * The cached value of the '{@link #getMaterialRequirements() <em>Material Requirements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialRequirements()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialRequirement> materialRequirements;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected static final OperationsType OPERATIONS_TYPE_EDEFAULT = OperationsType.PRODUCTION;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsType operationsType = OPERATIONS_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getEarliestStartTime() <em>Earliest Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEarliestStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime EARLIEST_START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEarliestStartTime() <em>Earliest Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEarliestStartTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime earliestStartTime = EARLIEST_START_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLatestStartTime() <em>Latest Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLatestStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final ZonedDateTime LATEST_START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLatestStartTime() <em>Latest Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLatestStartTime()
	 * @generated
	 * @ordered
	 */
	protected ZonedDateTime latestStartTime = LATEST_START_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal DURATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal duration = DURATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDurationUnitOfMeasurement() <em>Duration Unit Of Measurement</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDurationUnitOfMeasurement()
	 * @generated
	 * @ordered
	 */
	protected static final String DURATION_UNIT_OF_MEASUREMENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDurationUnitOfMeasurement() <em>Duration Unit Of Measurement</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDurationUnitOfMeasurement()
	 * @generated
	 * @ordered
	 */
	protected String durationUnitOfMeasurement = DURATION_UNIT_OF_MEASUREMENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getSegmentState() <em>Segment State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentState()
	 * @generated
	 * @ordered
	 */
	protected static final ScheduleState SEGMENT_STATE_EDEFAULT = ScheduleState.FORECAST;

	/**
	 * The cached value of the '{@link #getSegmentState() <em>Segment State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentState()
	 * @generated
	 * @ordered
	 */
	protected ScheduleState segmentState = SEGMENT_STATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScope hierarchyScope;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SegmentRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.SEGMENT_REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SegmentRequirement> getSubRequirements() {
		if (subRequirements == null) {
			subRequirements = new EObjectResolvingEList<SegmentRequirement>(SegmentRequirement.class, this,
					Iec62264Package.SEGMENT_REQUIREMENT__SUB_REQUIREMENTS);
		}
		return subRequirements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegment getProcessSegment() {
		if (processSegment != null && processSegment.eIsProxy()) {
			InternalEObject oldProcessSegment = (InternalEObject) processSegment;
			processSegment = (ProcessSegment) eResolveProxy(oldProcessSegment);
			if (processSegment != oldProcessSegment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.SEGMENT_REQUIREMENT__PROCESS_SEGMENT, oldProcessSegment, processSegment));
			}
		}
		return processSegment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegment basicGetProcessSegment() {
		return processSegment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessSegment(ProcessSegment newProcessSegment) {
		ProcessSegment oldProcessSegment = processSegment;
		processSegment = newProcessSegment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_REQUIREMENT__PROCESS_SEGMENT,
					oldProcessSegment, processSegment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinition getOperationsDefinition() {
		if (operationsDefinition != null && operationsDefinition.eIsProxy()) {
			InternalEObject oldOperationsDefinition = (InternalEObject) operationsDefinition;
			operationsDefinition = (OperationsDefinition) eResolveProxy(oldOperationsDefinition);
			if (operationsDefinition != oldOperationsDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.SEGMENT_REQUIREMENT__OPERATIONS_DEFINITION, oldOperationsDefinition,
							operationsDefinition));
			}
		}
		return operationsDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinition basicGetOperationsDefinition() {
		return operationsDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsDefinition(OperationsDefinition newOperationsDefinition) {
		OperationsDefinition oldOperationsDefinition = operationsDefinition;
		operationsDefinition = newOperationsDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.SEGMENT_REQUIREMENT__OPERATIONS_DEFINITION, oldOperationsDefinition,
					operationsDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SegmentParameter> getSegmentParameters() {
		if (segmentParameters == null) {
			segmentParameters = new EObjectContainmentEList<SegmentParameter>(SegmentParameter.class, this,
					Iec62264Package.SEGMENT_REQUIREMENT__SEGMENT_PARAMETERS);
		}
		return segmentParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelRequirement> getPersonnelRequirements() {
		if (personnelRequirements == null) {
			personnelRequirements = new EObjectContainmentEList<PersonnelRequirement>(PersonnelRequirement.class, this,
					Iec62264Package.SEGMENT_REQUIREMENT__PERSONNEL_REQUIREMENTS);
		}
		return personnelRequirements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentRequirement> getEquipmentRequirements() {
		if (equipmentRequirements == null) {
			equipmentRequirements = new EObjectContainmentEList<EquipmentRequirement>(EquipmentRequirement.class, this,
					Iec62264Package.SEGMENT_REQUIREMENT__EQUIPMENT_REQUIREMENTS);
		}
		return equipmentRequirements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetRequirement> getPhysicalAssetRequirements() {
		if (physicalAssetRequirements == null) {
			physicalAssetRequirements = new EObjectContainmentEList<PhysicalAssetRequirement>(
					PhysicalAssetRequirement.class, this,
					Iec62264Package.SEGMENT_REQUIREMENT__PHYSICAL_ASSET_REQUIREMENTS);
		}
		return physicalAssetRequirements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialRequirement> getMaterialRequirements() {
		if (materialRequirements == null) {
			materialRequirements = new EObjectContainmentEList<MaterialRequirement>(MaterialRequirement.class, this,
					Iec62264Package.SEGMENT_REQUIREMENT__MATERIAL_REQUIREMENTS);
		}
		return materialRequirements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_REQUIREMENT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_REQUIREMENT__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsType newOperationsType) {
		OperationsType oldOperationsType = operationsType;
		operationsType = newOperationsType == null ? OPERATIONS_TYPE_EDEFAULT : newOperationsType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_REQUIREMENT__OPERATIONS_TYPE,
					oldOperationsType, operationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getEarliestStartTime() {
		return earliestStartTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEarliestStartTime(ZonedDateTime newEarliestStartTime) {
		ZonedDateTime oldEarliestStartTime = earliestStartTime;
		earliestStartTime = newEarliestStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.SEGMENT_REQUIREMENT__EARLIEST_START_TIME, oldEarliestStartTime, earliestStartTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZonedDateTime getLatestStartTime() {
		return latestStartTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLatestStartTime(ZonedDateTime newLatestStartTime) {
		ZonedDateTime oldLatestStartTime = latestStartTime;
		latestStartTime = newLatestStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.SEGMENT_REQUIREMENT__LATEST_START_TIME, oldLatestStartTime, latestStartTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getDuration() {
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDuration(BigDecimal newDuration) {
		BigDecimal oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_REQUIREMENT__DURATION,
					oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDurationUnitOfMeasurement() {
		return durationUnitOfMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDurationUnitOfMeasurement(String newDurationUnitOfMeasurement) {
		String oldDurationUnitOfMeasurement = durationUnitOfMeasurement;
		durationUnitOfMeasurement = newDurationUnitOfMeasurement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.SEGMENT_REQUIREMENT__DURATION_UNIT_OF_MEASUREMENT, oldDurationUnitOfMeasurement,
					durationUnitOfMeasurement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScheduleState getSegmentState() {
		return segmentState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSegmentState(ScheduleState newSegmentState) {
		ScheduleState oldSegmentState = segmentState;
		segmentState = newSegmentState == null ? SEGMENT_STATE_EDEFAULT : newSegmentState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_REQUIREMENT__SEGMENT_STATE,
					oldSegmentState, segmentState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope getHierarchyScope() {
		if (hierarchyScope != null && hierarchyScope.eIsProxy()) {
			InternalEObject oldHierarchyScope = (InternalEObject) hierarchyScope;
			hierarchyScope = (HierarchyScope) eResolveProxy(oldHierarchyScope);
			if (hierarchyScope != oldHierarchyScope) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.SEGMENT_REQUIREMENT__HIERARCHY_SCOPE, oldHierarchyScope, hierarchyScope));
			}
		}
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope basicGetHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScope newHierarchyScope) {
		HierarchyScope oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.SEGMENT_REQUIREMENT__HIERARCHY_SCOPE,
					oldHierarchyScope, hierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.SEGMENT_REQUIREMENT__SEGMENT_PARAMETERS:
			return ((InternalEList<?>) getSegmentParameters()).basicRemove(otherEnd, msgs);
		case Iec62264Package.SEGMENT_REQUIREMENT__PERSONNEL_REQUIREMENTS:
			return ((InternalEList<?>) getPersonnelRequirements()).basicRemove(otherEnd, msgs);
		case Iec62264Package.SEGMENT_REQUIREMENT__EQUIPMENT_REQUIREMENTS:
			return ((InternalEList<?>) getEquipmentRequirements()).basicRemove(otherEnd, msgs);
		case Iec62264Package.SEGMENT_REQUIREMENT__PHYSICAL_ASSET_REQUIREMENTS:
			return ((InternalEList<?>) getPhysicalAssetRequirements()).basicRemove(otherEnd, msgs);
		case Iec62264Package.SEGMENT_REQUIREMENT__MATERIAL_REQUIREMENTS:
			return ((InternalEList<?>) getMaterialRequirements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.SEGMENT_REQUIREMENT__SUB_REQUIREMENTS:
			return getSubRequirements();
		case Iec62264Package.SEGMENT_REQUIREMENT__PROCESS_SEGMENT:
			if (resolve)
				return getProcessSegment();
			return basicGetProcessSegment();
		case Iec62264Package.SEGMENT_REQUIREMENT__OPERATIONS_DEFINITION:
			if (resolve)
				return getOperationsDefinition();
			return basicGetOperationsDefinition();
		case Iec62264Package.SEGMENT_REQUIREMENT__SEGMENT_PARAMETERS:
			return getSegmentParameters();
		case Iec62264Package.SEGMENT_REQUIREMENT__PERSONNEL_REQUIREMENTS:
			return getPersonnelRequirements();
		case Iec62264Package.SEGMENT_REQUIREMENT__EQUIPMENT_REQUIREMENTS:
			return getEquipmentRequirements();
		case Iec62264Package.SEGMENT_REQUIREMENT__PHYSICAL_ASSET_REQUIREMENTS:
			return getPhysicalAssetRequirements();
		case Iec62264Package.SEGMENT_REQUIREMENT__MATERIAL_REQUIREMENTS:
			return getMaterialRequirements();
		case Iec62264Package.SEGMENT_REQUIREMENT__ID:
			return getId();
		case Iec62264Package.SEGMENT_REQUIREMENT__DESCRIPTION:
			return getDescription();
		case Iec62264Package.SEGMENT_REQUIREMENT__OPERATIONS_TYPE:
			return getOperationsType();
		case Iec62264Package.SEGMENT_REQUIREMENT__EARLIEST_START_TIME:
			return getEarliestStartTime();
		case Iec62264Package.SEGMENT_REQUIREMENT__LATEST_START_TIME:
			return getLatestStartTime();
		case Iec62264Package.SEGMENT_REQUIREMENT__DURATION:
			return getDuration();
		case Iec62264Package.SEGMENT_REQUIREMENT__DURATION_UNIT_OF_MEASUREMENT:
			return getDurationUnitOfMeasurement();
		case Iec62264Package.SEGMENT_REQUIREMENT__SEGMENT_STATE:
			return getSegmentState();
		case Iec62264Package.SEGMENT_REQUIREMENT__HIERARCHY_SCOPE:
			if (resolve)
				return getHierarchyScope();
			return basicGetHierarchyScope();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.SEGMENT_REQUIREMENT__SUB_REQUIREMENTS:
			getSubRequirements().clear();
			getSubRequirements().addAll((Collection<? extends SegmentRequirement>) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__PROCESS_SEGMENT:
			setProcessSegment((ProcessSegment) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__OPERATIONS_DEFINITION:
			setOperationsDefinition((OperationsDefinition) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__SEGMENT_PARAMETERS:
			getSegmentParameters().clear();
			getSegmentParameters().addAll((Collection<? extends SegmentParameter>) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__PERSONNEL_REQUIREMENTS:
			getPersonnelRequirements().clear();
			getPersonnelRequirements().addAll((Collection<? extends PersonnelRequirement>) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__EQUIPMENT_REQUIREMENTS:
			getEquipmentRequirements().clear();
			getEquipmentRequirements().addAll((Collection<? extends EquipmentRequirement>) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__PHYSICAL_ASSET_REQUIREMENTS:
			getPhysicalAssetRequirements().clear();
			getPhysicalAssetRequirements().addAll((Collection<? extends PhysicalAssetRequirement>) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__MATERIAL_REQUIREMENTS:
			getMaterialRequirements().clear();
			getMaterialRequirements().addAll((Collection<? extends MaterialRequirement>) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__OPERATIONS_TYPE:
			setOperationsType((OperationsType) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__EARLIEST_START_TIME:
			setEarliestStartTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__LATEST_START_TIME:
			setLatestStartTime((ZonedDateTime) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__DURATION:
			setDuration((BigDecimal) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__DURATION_UNIT_OF_MEASUREMENT:
			setDurationUnitOfMeasurement((String) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__SEGMENT_STATE:
			setSegmentState((ScheduleState) newValue);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.SEGMENT_REQUIREMENT__SUB_REQUIREMENTS:
			getSubRequirements().clear();
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__PROCESS_SEGMENT:
			setProcessSegment((ProcessSegment) null);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__OPERATIONS_DEFINITION:
			setOperationsDefinition((OperationsDefinition) null);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__SEGMENT_PARAMETERS:
			getSegmentParameters().clear();
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__PERSONNEL_REQUIREMENTS:
			getPersonnelRequirements().clear();
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__EQUIPMENT_REQUIREMENTS:
			getEquipmentRequirements().clear();
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__PHYSICAL_ASSET_REQUIREMENTS:
			getPhysicalAssetRequirements().clear();
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__MATERIAL_REQUIREMENTS:
			getMaterialRequirements().clear();
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__OPERATIONS_TYPE:
			setOperationsType(OPERATIONS_TYPE_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__EARLIEST_START_TIME:
			setEarliestStartTime(EARLIEST_START_TIME_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__LATEST_START_TIME:
			setLatestStartTime(LATEST_START_TIME_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__DURATION:
			setDuration(DURATION_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__DURATION_UNIT_OF_MEASUREMENT:
			setDurationUnitOfMeasurement(DURATION_UNIT_OF_MEASUREMENT_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__SEGMENT_STATE:
			setSegmentState(SEGMENT_STATE_EDEFAULT);
			return;
		case Iec62264Package.SEGMENT_REQUIREMENT__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.SEGMENT_REQUIREMENT__SUB_REQUIREMENTS:
			return subRequirements != null && !subRequirements.isEmpty();
		case Iec62264Package.SEGMENT_REQUIREMENT__PROCESS_SEGMENT:
			return processSegment != null;
		case Iec62264Package.SEGMENT_REQUIREMENT__OPERATIONS_DEFINITION:
			return operationsDefinition != null;
		case Iec62264Package.SEGMENT_REQUIREMENT__SEGMENT_PARAMETERS:
			return segmentParameters != null && !segmentParameters.isEmpty();
		case Iec62264Package.SEGMENT_REQUIREMENT__PERSONNEL_REQUIREMENTS:
			return personnelRequirements != null && !personnelRequirements.isEmpty();
		case Iec62264Package.SEGMENT_REQUIREMENT__EQUIPMENT_REQUIREMENTS:
			return equipmentRequirements != null && !equipmentRequirements.isEmpty();
		case Iec62264Package.SEGMENT_REQUIREMENT__PHYSICAL_ASSET_REQUIREMENTS:
			return physicalAssetRequirements != null && !physicalAssetRequirements.isEmpty();
		case Iec62264Package.SEGMENT_REQUIREMENT__MATERIAL_REQUIREMENTS:
			return materialRequirements != null && !materialRequirements.isEmpty();
		case Iec62264Package.SEGMENT_REQUIREMENT__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.SEGMENT_REQUIREMENT__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.SEGMENT_REQUIREMENT__OPERATIONS_TYPE:
			return operationsType != OPERATIONS_TYPE_EDEFAULT;
		case Iec62264Package.SEGMENT_REQUIREMENT__EARLIEST_START_TIME:
			return EARLIEST_START_TIME_EDEFAULT == null ? earliestStartTime != null
					: !EARLIEST_START_TIME_EDEFAULT.equals(earliestStartTime);
		case Iec62264Package.SEGMENT_REQUIREMENT__LATEST_START_TIME:
			return LATEST_START_TIME_EDEFAULT == null ? latestStartTime != null
					: !LATEST_START_TIME_EDEFAULT.equals(latestStartTime);
		case Iec62264Package.SEGMENT_REQUIREMENT__DURATION:
			return DURATION_EDEFAULT == null ? duration != null : !DURATION_EDEFAULT.equals(duration);
		case Iec62264Package.SEGMENT_REQUIREMENT__DURATION_UNIT_OF_MEASUREMENT:
			return DURATION_UNIT_OF_MEASUREMENT_EDEFAULT == null ? durationUnitOfMeasurement != null
					: !DURATION_UNIT_OF_MEASUREMENT_EDEFAULT.equals(durationUnitOfMeasurement);
		case Iec62264Package.SEGMENT_REQUIREMENT__SEGMENT_STATE:
			return segmentState != SEGMENT_STATE_EDEFAULT;
		case Iec62264Package.SEGMENT_REQUIREMENT__HIERARCHY_SCOPE:
			return hierarchyScope != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", operationsType: ");
		result.append(operationsType);
		result.append(", earliestStartTime: ");
		result.append(earliestStartTime);
		result.append(", latestStartTime: ");
		result.append(latestStartTime);
		result.append(", duration: ");
		result.append(duration);
		result.append(", durationUnitOfMeasurement: ");
		result.append(durationUnitOfMeasurement);
		result.append(", segmentState: ");
		result.append(segmentState);
		result.append(')');
		return result.toString();
	}

} //SegmentRequirementImpl
