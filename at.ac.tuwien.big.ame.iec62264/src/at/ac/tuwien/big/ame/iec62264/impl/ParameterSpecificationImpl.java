/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.ParameterSpecification;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ParameterSpecificationImpl#getSubParameters <em>Sub Parameters</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ParameterSpecificationImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ParameterSpecificationImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ParameterSpecificationImpl#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ParameterSpecificationImpl#getValueUnitOfMeasurement <em>Value Unit Of Measurement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParameterSpecificationImpl extends MinimalEObjectImpl.Container implements ParameterSpecification {
	/**
	 * The cached value of the '{@link #getSubParameters() <em>Sub Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterSpecification> subParameters;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getValueUnitOfMeasurement() <em>Value Unit Of Measurement</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueUnitOfMeasurement()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_UNIT_OF_MEASUREMENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValueUnitOfMeasurement() <em>Value Unit Of Measurement</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueUnitOfMeasurement()
	 * @generated
	 * @ordered
	 */
	protected String valueUnitOfMeasurement = VALUE_UNIT_OF_MEASUREMENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PARAMETER_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterSpecification> getSubParameters() {
		if (subParameters == null) {
			subParameters = new EObjectContainmentEList<ParameterSpecification>(ParameterSpecification.class, this,
					Iec62264Package.PARAMETER_SPECIFICATION__SUB_PARAMETERS);
		}
		return subParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PARAMETER_SPECIFICATION__ID, oldId,
					id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PARAMETER_SPECIFICATION__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PARAMETER_SPECIFICATION__VALUE,
					oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValueUnitOfMeasurement() {
		return valueUnitOfMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueUnitOfMeasurement(String newValueUnitOfMeasurement) {
		String oldValueUnitOfMeasurement = valueUnitOfMeasurement;
		valueUnitOfMeasurement = newValueUnitOfMeasurement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PARAMETER_SPECIFICATION__VALUE_UNIT_OF_MEASUREMENT, oldValueUnitOfMeasurement,
					valueUnitOfMeasurement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PARAMETER_SPECIFICATION__SUB_PARAMETERS:
			return ((InternalEList<?>) getSubParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PARAMETER_SPECIFICATION__SUB_PARAMETERS:
			return getSubParameters();
		case Iec62264Package.PARAMETER_SPECIFICATION__ID:
			return getId();
		case Iec62264Package.PARAMETER_SPECIFICATION__DESCRIPTION:
			return getDescription();
		case Iec62264Package.PARAMETER_SPECIFICATION__VALUE:
			return getValue();
		case Iec62264Package.PARAMETER_SPECIFICATION__VALUE_UNIT_OF_MEASUREMENT:
			return getValueUnitOfMeasurement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PARAMETER_SPECIFICATION__SUB_PARAMETERS:
			getSubParameters().clear();
			getSubParameters().addAll((Collection<? extends ParameterSpecification>) newValue);
			return;
		case Iec62264Package.PARAMETER_SPECIFICATION__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.PARAMETER_SPECIFICATION__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.PARAMETER_SPECIFICATION__VALUE:
			setValue((String) newValue);
			return;
		case Iec62264Package.PARAMETER_SPECIFICATION__VALUE_UNIT_OF_MEASUREMENT:
			setValueUnitOfMeasurement((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PARAMETER_SPECIFICATION__SUB_PARAMETERS:
			getSubParameters().clear();
			return;
		case Iec62264Package.PARAMETER_SPECIFICATION__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.PARAMETER_SPECIFICATION__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.PARAMETER_SPECIFICATION__VALUE:
			setValue(VALUE_EDEFAULT);
			return;
		case Iec62264Package.PARAMETER_SPECIFICATION__VALUE_UNIT_OF_MEASUREMENT:
			setValueUnitOfMeasurement(VALUE_UNIT_OF_MEASUREMENT_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PARAMETER_SPECIFICATION__SUB_PARAMETERS:
			return subParameters != null && !subParameters.isEmpty();
		case Iec62264Package.PARAMETER_SPECIFICATION__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.PARAMETER_SPECIFICATION__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.PARAMETER_SPECIFICATION__VALUE:
			return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
		case Iec62264Package.PARAMETER_SPECIFICATION__VALUE_UNIT_OF_MEASUREMENT:
			return VALUE_UNIT_OF_MEASUREMENT_EDEFAULT == null ? valueUnitOfMeasurement != null
					: !VALUE_UNIT_OF_MEASUREMENT_EDEFAULT.equals(valueUnitOfMeasurement);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", value: ");
		result.append(value);
		result.append(", valueUnitOfMeasurement: ");
		result.append(valueUnitOfMeasurement);
		result.append(')');
		return result.toString();
	}

} //ParameterSpecificationImpl
