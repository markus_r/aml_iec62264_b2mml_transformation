/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.PersonProperty;
import at.ac.tuwien.big.ame.iec62264.PersonnelClassProperty;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Person Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonPropertyImpl#getSubProperties <em>Sub Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonPropertyImpl#getPersonnelClassProperty <em>Personnel Class Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonPropertyImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonPropertyImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonPropertyImpl#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.PersonPropertyImpl#getValueUnitOfMeasure <em>Value Unit Of Measure</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonPropertyImpl extends MinimalEObjectImpl.Container implements PersonProperty {
	/**
	 * The cached value of the '{@link #getSubProperties() <em>Sub Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonProperty> subProperties;

	/**
	 * The cached value of the '{@link #getPersonnelClassProperty() <em>Personnel Class Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelClassProperty()
	 * @generated
	 * @ordered
	 */
	protected PersonnelClassProperty personnelClassProperty;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getValueUnitOfMeasure() <em>Value Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValueUnitOfMeasure() <em>Value Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String valueUnitOfMeasure = VALUE_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PERSON_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonProperty> getSubProperties() {
		if (subProperties == null) {
			subProperties = new EObjectContainmentEList<PersonProperty>(PersonProperty.class, this,
					Iec62264Package.PERSON_PROPERTY__SUB_PROPERTIES);
		}
		return subProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelClassProperty getPersonnelClassProperty() {
		if (personnelClassProperty != null && personnelClassProperty.eIsProxy()) {
			InternalEObject oldPersonnelClassProperty = (InternalEObject) personnelClassProperty;
			personnelClassProperty = (PersonnelClassProperty) eResolveProxy(oldPersonnelClassProperty);
			if (personnelClassProperty != oldPersonnelClassProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.PERSON_PROPERTY__PERSONNEL_CLASS_PROPERTY, oldPersonnelClassProperty,
							personnelClassProperty));
			}
		}
		return personnelClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelClassProperty basicGetPersonnelClassProperty() {
		return personnelClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonnelClassProperty(PersonnelClassProperty newPersonnelClassProperty) {
		PersonnelClassProperty oldPersonnelClassProperty = personnelClassProperty;
		personnelClassProperty = newPersonnelClassProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PERSON_PROPERTY__PERSONNEL_CLASS_PROPERTY, oldPersonnelClassProperty,
					personnelClassProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PERSON_PROPERTY__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PERSON_PROPERTY__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PERSON_PROPERTY__VALUE, oldValue,
					value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValueUnitOfMeasure() {
		return valueUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueUnitOfMeasure(String newValueUnitOfMeasure) {
		String oldValueUnitOfMeasure = valueUnitOfMeasure;
		valueUnitOfMeasure = newValueUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PERSON_PROPERTY__VALUE_UNIT_OF_MEASURE, oldValueUnitOfMeasure, valueUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PERSON_PROPERTY__SUB_PROPERTIES:
			return ((InternalEList<?>) getSubProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PERSON_PROPERTY__SUB_PROPERTIES:
			return getSubProperties();
		case Iec62264Package.PERSON_PROPERTY__PERSONNEL_CLASS_PROPERTY:
			if (resolve)
				return getPersonnelClassProperty();
			return basicGetPersonnelClassProperty();
		case Iec62264Package.PERSON_PROPERTY__ID:
			return getId();
		case Iec62264Package.PERSON_PROPERTY__DESCRIPTION:
			return getDescription();
		case Iec62264Package.PERSON_PROPERTY__VALUE:
			return getValue();
		case Iec62264Package.PERSON_PROPERTY__VALUE_UNIT_OF_MEASURE:
			return getValueUnitOfMeasure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PERSON_PROPERTY__SUB_PROPERTIES:
			getSubProperties().clear();
			getSubProperties().addAll((Collection<? extends PersonProperty>) newValue);
			return;
		case Iec62264Package.PERSON_PROPERTY__PERSONNEL_CLASS_PROPERTY:
			setPersonnelClassProperty((PersonnelClassProperty) newValue);
			return;
		case Iec62264Package.PERSON_PROPERTY__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.PERSON_PROPERTY__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.PERSON_PROPERTY__VALUE:
			setValue((String) newValue);
			return;
		case Iec62264Package.PERSON_PROPERTY__VALUE_UNIT_OF_MEASURE:
			setValueUnitOfMeasure((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PERSON_PROPERTY__SUB_PROPERTIES:
			getSubProperties().clear();
			return;
		case Iec62264Package.PERSON_PROPERTY__PERSONNEL_CLASS_PROPERTY:
			setPersonnelClassProperty((PersonnelClassProperty) null);
			return;
		case Iec62264Package.PERSON_PROPERTY__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.PERSON_PROPERTY__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.PERSON_PROPERTY__VALUE:
			setValue(VALUE_EDEFAULT);
			return;
		case Iec62264Package.PERSON_PROPERTY__VALUE_UNIT_OF_MEASURE:
			setValueUnitOfMeasure(VALUE_UNIT_OF_MEASURE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PERSON_PROPERTY__SUB_PROPERTIES:
			return subProperties != null && !subProperties.isEmpty();
		case Iec62264Package.PERSON_PROPERTY__PERSONNEL_CLASS_PROPERTY:
			return personnelClassProperty != null;
		case Iec62264Package.PERSON_PROPERTY__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.PERSON_PROPERTY__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.PERSON_PROPERTY__VALUE:
			return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
		case Iec62264Package.PERSON_PROPERTY__VALUE_UNIT_OF_MEASURE:
			return VALUE_UNIT_OF_MEASURE_EDEFAULT == null ? valueUnitOfMeasure != null
					: !VALUE_UNIT_OF_MEASURE_EDEFAULT.equals(valueUnitOfMeasure);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", value: ");
		result.append(value);
		result.append(", valueUnitOfMeasure: ");
		result.append(valueUnitOfMeasure);
		result.append(')');
		return result.toString();
	}

} //PersonPropertyImpl
