/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.AssemblyRelationship;
import at.ac.tuwien.big.ame.iec62264.AssemblyType;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialActual;
import at.ac.tuwien.big.ame.iec62264.MaterialActualProperty;
import at.ac.tuwien.big.ame.iec62264.MaterialClass;
import at.ac.tuwien.big.ame.iec62264.MaterialDefinition;
import at.ac.tuwien.big.ame.iec62264.MaterialLot;
import at.ac.tuwien.big.ame.iec62264.MaterialSublot;

import java.math.BigDecimal;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Actual</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getAssembledFromActuals <em>Assembled From Actuals</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getMaterialActualProperties <em>Material Actual Properties</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getStorageLocation <em>Storage Location</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getQuantityUnitOfMeasure <em>Quantity Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getMaterialClasses <em>Material Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getMaterialDefinitions <em>Material Definitions</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getMaterialLots <em>Material Lots</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.MaterialActualImpl#getMaterialSublots <em>Material Sublots</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MaterialActualImpl extends MinimalEObjectImpl.Container implements MaterialActual {
	/**
	 * The cached value of the '{@link #getAssembledFromActuals() <em>Assembled From Actuals</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssembledFromActuals()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialActual> assembledFromActuals;

	/**
	 * The cached value of the '{@link #getMaterialActualProperties() <em>Material Actual Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialActualProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialActualProperty> materialActualProperties;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaterialUse() <em>Material Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialUse()
	 * @generated
	 * @ordered
	 */
	protected static final String MATERIAL_USE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMaterialUse() <em>Material Use</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialUse()
	 * @generated
	 * @ordered
	 */
	protected String materialUse = MATERIAL_USE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStorageLocation() <em>Storage Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String STORAGE_LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStorageLocation() <em>Storage Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageLocation()
	 * @generated
	 * @ordered
	 */
	protected String storageLocation = STORAGE_LOCATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal QUANTITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal quantity = QUANTITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String QUANTITY_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuantityUnitOfMeasure() <em>Quantity Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String quantityUnitOfMeasure = QUANTITY_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected static final AssemblyType ASSEMBLY_TYPE_EDEFAULT = AssemblyType.PHYSICAL;

	/**
	 * The cached value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected AssemblyType assemblyType = ASSEMBLY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected static final AssemblyRelationship ASSEMBLY_RELATIONSHIP_EDEFAULT = AssemblyRelationship.PERMANENT;

	/**
	 * The cached value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected AssemblyRelationship assemblyRelationship = ASSEMBLY_RELATIONSHIP_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMaterialClasses() <em>Material Classes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialClass> materialClasses;

	/**
	 * The cached value of the '{@link #getMaterialDefinitions() <em>Material Definitions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinitions()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialDefinition> materialDefinitions;

	/**
	 * The cached value of the '{@link #getMaterialLots() <em>Material Lots</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialLots()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialLot> materialLots;

	/**
	 * The cached value of the '{@link #getMaterialSublots() <em>Material Sublots</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSublots()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSublot> materialSublots;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialActualImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.MATERIAL_ACTUAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialActual> getAssembledFromActuals() {
		if (assembledFromActuals == null) {
			assembledFromActuals = new EObjectResolvingEList<MaterialActual>(MaterialActual.class, this,
					Iec62264Package.MATERIAL_ACTUAL__ASSEMBLED_FROM_ACTUALS);
		}
		return assembledFromActuals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialActualProperty> getMaterialActualProperties() {
		if (materialActualProperties == null) {
			materialActualProperties = new EObjectContainmentEList<MaterialActualProperty>(MaterialActualProperty.class,
					this, Iec62264Package.MATERIAL_ACTUAL__MATERIAL_ACTUAL_PROPERTIES);
		}
		return materialActualProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_ACTUAL__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMaterialUse() {
		return materialUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialUse(String newMaterialUse) {
		String oldMaterialUse = materialUse;
		materialUse = newMaterialUse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_ACTUAL__MATERIAL_USE,
					oldMaterialUse, materialUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStorageLocation() {
		return storageLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageLocation(String newStorageLocation) {
		String oldStorageLocation = storageLocation;
		storageLocation = newStorageLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_ACTUAL__STORAGE_LOCATION,
					oldStorageLocation, storageLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity(BigDecimal newQuantity) {
		BigDecimal oldQuantity = quantity;
		quantity = newQuantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_ACTUAL__QUANTITY,
					oldQuantity, quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQuantityUnitOfMeasure() {
		return quantityUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantityUnitOfMeasure(String newQuantityUnitOfMeasure) {
		String oldQuantityUnitOfMeasure = quantityUnitOfMeasure;
		quantityUnitOfMeasure = newQuantityUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.MATERIAL_ACTUAL__QUANTITY_UNIT_OF_MEASURE, oldQuantityUnitOfMeasure,
					quantityUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyType getAssemblyType() {
		return assemblyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyType(AssemblyType newAssemblyType) {
		AssemblyType oldAssemblyType = assemblyType;
		assemblyType = newAssemblyType == null ? ASSEMBLY_TYPE_EDEFAULT : newAssemblyType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.MATERIAL_ACTUAL__ASSEMBLY_TYPE,
					oldAssemblyType, assemblyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationship getAssemblyRelationship() {
		return assemblyRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyRelationship(AssemblyRelationship newAssemblyRelationship) {
		AssemblyRelationship oldAssemblyRelationship = assemblyRelationship;
		assemblyRelationship = newAssemblyRelationship == null ? ASSEMBLY_RELATIONSHIP_EDEFAULT
				: newAssemblyRelationship;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.MATERIAL_ACTUAL__ASSEMBLY_RELATIONSHIP, oldAssemblyRelationship,
					assemblyRelationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialClass> getMaterialClasses() {
		if (materialClasses == null) {
			materialClasses = new EObjectResolvingEList<MaterialClass>(MaterialClass.class, this,
					Iec62264Package.MATERIAL_ACTUAL__MATERIAL_CLASSES);
		}
		return materialClasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialDefinition> getMaterialDefinitions() {
		if (materialDefinitions == null) {
			materialDefinitions = new EObjectResolvingEList<MaterialDefinition>(MaterialDefinition.class, this,
					Iec62264Package.MATERIAL_ACTUAL__MATERIAL_DEFINITIONS);
		}
		return materialDefinitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialLot> getMaterialLots() {
		if (materialLots == null) {
			materialLots = new EObjectResolvingEList<MaterialLot>(MaterialLot.class, this,
					Iec62264Package.MATERIAL_ACTUAL__MATERIAL_LOTS);
		}
		return materialLots;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSublot> getMaterialSublots() {
		if (materialSublots == null) {
			materialSublots = new EObjectResolvingEList<MaterialSublot>(MaterialSublot.class, this,
					Iec62264Package.MATERIAL_ACTUAL__MATERIAL_SUBLOTS);
		}
		return materialSublots;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_ACTUAL_PROPERTIES:
			return ((InternalEList<?>) getMaterialActualProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_ACTUAL__ASSEMBLED_FROM_ACTUALS:
			return getAssembledFromActuals();
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_ACTUAL_PROPERTIES:
			return getMaterialActualProperties();
		case Iec62264Package.MATERIAL_ACTUAL__DESCRIPTION:
			return getDescription();
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_USE:
			return getMaterialUse();
		case Iec62264Package.MATERIAL_ACTUAL__STORAGE_LOCATION:
			return getStorageLocation();
		case Iec62264Package.MATERIAL_ACTUAL__QUANTITY:
			return getQuantity();
		case Iec62264Package.MATERIAL_ACTUAL__QUANTITY_UNIT_OF_MEASURE:
			return getQuantityUnitOfMeasure();
		case Iec62264Package.MATERIAL_ACTUAL__ASSEMBLY_TYPE:
			return getAssemblyType();
		case Iec62264Package.MATERIAL_ACTUAL__ASSEMBLY_RELATIONSHIP:
			return getAssemblyRelationship();
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_CLASSES:
			return getMaterialClasses();
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_DEFINITIONS:
			return getMaterialDefinitions();
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_LOTS:
			return getMaterialLots();
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_SUBLOTS:
			return getMaterialSublots();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_ACTUAL__ASSEMBLED_FROM_ACTUALS:
			getAssembledFromActuals().clear();
			getAssembledFromActuals().addAll((Collection<? extends MaterialActual>) newValue);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_ACTUAL_PROPERTIES:
			getMaterialActualProperties().clear();
			getMaterialActualProperties().addAll((Collection<? extends MaterialActualProperty>) newValue);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_USE:
			setMaterialUse((String) newValue);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__STORAGE_LOCATION:
			setStorageLocation((String) newValue);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__QUANTITY:
			setQuantity((BigDecimal) newValue);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure((String) newValue);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__ASSEMBLY_TYPE:
			setAssemblyType((AssemblyType) newValue);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__ASSEMBLY_RELATIONSHIP:
			setAssemblyRelationship((AssemblyRelationship) newValue);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_CLASSES:
			getMaterialClasses().clear();
			getMaterialClasses().addAll((Collection<? extends MaterialClass>) newValue);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_DEFINITIONS:
			getMaterialDefinitions().clear();
			getMaterialDefinitions().addAll((Collection<? extends MaterialDefinition>) newValue);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_LOTS:
			getMaterialLots().clear();
			getMaterialLots().addAll((Collection<? extends MaterialLot>) newValue);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_SUBLOTS:
			getMaterialSublots().clear();
			getMaterialSublots().addAll((Collection<? extends MaterialSublot>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_ACTUAL__ASSEMBLED_FROM_ACTUALS:
			getAssembledFromActuals().clear();
			return;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_ACTUAL_PROPERTIES:
			getMaterialActualProperties().clear();
			return;
		case Iec62264Package.MATERIAL_ACTUAL__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_USE:
			setMaterialUse(MATERIAL_USE_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__STORAGE_LOCATION:
			setStorageLocation(STORAGE_LOCATION_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__QUANTITY:
			setQuantity(QUANTITY_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__QUANTITY_UNIT_OF_MEASURE:
			setQuantityUnitOfMeasure(QUANTITY_UNIT_OF_MEASURE_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__ASSEMBLY_TYPE:
			setAssemblyType(ASSEMBLY_TYPE_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__ASSEMBLY_RELATIONSHIP:
			setAssemblyRelationship(ASSEMBLY_RELATIONSHIP_EDEFAULT);
			return;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_CLASSES:
			getMaterialClasses().clear();
			return;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_DEFINITIONS:
			getMaterialDefinitions().clear();
			return;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_LOTS:
			getMaterialLots().clear();
			return;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_SUBLOTS:
			getMaterialSublots().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.MATERIAL_ACTUAL__ASSEMBLED_FROM_ACTUALS:
			return assembledFromActuals != null && !assembledFromActuals.isEmpty();
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_ACTUAL_PROPERTIES:
			return materialActualProperties != null && !materialActualProperties.isEmpty();
		case Iec62264Package.MATERIAL_ACTUAL__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_USE:
			return MATERIAL_USE_EDEFAULT == null ? materialUse != null : !MATERIAL_USE_EDEFAULT.equals(materialUse);
		case Iec62264Package.MATERIAL_ACTUAL__STORAGE_LOCATION:
			return STORAGE_LOCATION_EDEFAULT == null ? storageLocation != null
					: !STORAGE_LOCATION_EDEFAULT.equals(storageLocation);
		case Iec62264Package.MATERIAL_ACTUAL__QUANTITY:
			return QUANTITY_EDEFAULT == null ? quantity != null : !QUANTITY_EDEFAULT.equals(quantity);
		case Iec62264Package.MATERIAL_ACTUAL__QUANTITY_UNIT_OF_MEASURE:
			return QUANTITY_UNIT_OF_MEASURE_EDEFAULT == null ? quantityUnitOfMeasure != null
					: !QUANTITY_UNIT_OF_MEASURE_EDEFAULT.equals(quantityUnitOfMeasure);
		case Iec62264Package.MATERIAL_ACTUAL__ASSEMBLY_TYPE:
			return assemblyType != ASSEMBLY_TYPE_EDEFAULT;
		case Iec62264Package.MATERIAL_ACTUAL__ASSEMBLY_RELATIONSHIP:
			return assemblyRelationship != ASSEMBLY_RELATIONSHIP_EDEFAULT;
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_CLASSES:
			return materialClasses != null && !materialClasses.isEmpty();
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_DEFINITIONS:
			return materialDefinitions != null && !materialDefinitions.isEmpty();
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_LOTS:
			return materialLots != null && !materialLots.isEmpty();
		case Iec62264Package.MATERIAL_ACTUAL__MATERIAL_SUBLOTS:
			return materialSublots != null && !materialSublots.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", materialUse: ");
		result.append(materialUse);
		result.append(", storageLocation: ");
		result.append(storageLocation);
		result.append(", quantity: ");
		result.append(quantity);
		result.append(", quantityUnitOfMeasure: ");
		result.append(quantityUnitOfMeasure);
		result.append(", assemblyType: ");
		result.append(assemblyType);
		result.append(", assemblyRelationship: ");
		result.append(assemblyRelationship);
		result.append(')');
		return result.toString();
	}

} //MaterialActualImpl
