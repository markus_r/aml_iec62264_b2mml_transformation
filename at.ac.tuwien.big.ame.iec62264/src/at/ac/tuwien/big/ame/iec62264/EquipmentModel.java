/**
 */
package at.ac.tuwien.big.ame.iec62264;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentModel#getEquipmentClasses <em>Equipment Classes</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentModel#getEquipments <em>Equipments</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentModel#getTestSpecifications <em>Test Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.EquipmentModel#getTestResults <em>Test Results</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentModel()
 * @model
 * @generated
 */
public interface EquipmentModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Equipment Classes</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.EquipmentClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Classes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Classes</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentModel_EquipmentClasses()
	 * @model containment="true"
	 * @generated
	 */
	EList<EquipmentClass> getEquipmentClasses();

	/**
	 * Returns the value of the '<em><b>Equipments</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.Equipment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipments</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentModel_Equipments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Equipment> getEquipments();

	/**
	 * Returns the value of the '<em><b>Test Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Specifications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Specifications</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentModel_TestSpecifications()
	 * @model containment="true"
	 * @generated
	 */
	EList<EquipmentCapabilityTestSpecification> getTestSpecifications();

	/**
	 * Returns the value of the '<em><b>Test Results</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.iec62264.EquipmentCapabilityTestResult}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Results</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Results</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.iec62264.Iec62264Package#getEquipmentModel_TestResults()
	 * @model containment="true"
	 * @generated
	 */
	EList<EquipmentCapabilityTestResult> getTestResults();

} // EquipmentModel
