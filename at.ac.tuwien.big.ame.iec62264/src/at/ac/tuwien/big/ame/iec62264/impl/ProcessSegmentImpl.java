/**
 */
package at.ac.tuwien.big.ame.iec62264.impl;

import at.ac.tuwien.big.ame.iec62264.EquipmentSegmentSpecification;
import at.ac.tuwien.big.ame.iec62264.HierarchyScope;
import at.ac.tuwien.big.ame.iec62264.Iec62264Package;
import at.ac.tuwien.big.ame.iec62264.MaterialSegmentSpecification;
import at.ac.tuwien.big.ame.iec62264.OperationsType;
import at.ac.tuwien.big.ame.iec62264.PersonnelSegmentSpecification;
import at.ac.tuwien.big.ame.iec62264.PhysicalAssetSegmentSpecification;
import at.ac.tuwien.big.ame.iec62264.ProcessSegment;
import at.ac.tuwien.big.ame.iec62264.ProcessSegmentParameter;

import java.math.BigDecimal;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process Segment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl#getChildren <em>Children</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl#getProcessSegmentParameters <em>Process Segment Parameters</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl#getPersonnelSegmentSpecifications <em>Personnel Segment Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl#getEquipmentSegmentSpecifications <em>Equipment Segment Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl#getPhysicalAssetSegmentSpecifications <em>Physical Asset Segment Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl#getMaterialSegmentSpecifications <em>Material Segment Specifications</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl#getId <em>Id</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl#getDuration <em>Duration</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl#getDurationUnitOfMeasure <em>Duration Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.iec62264.impl.ProcessSegmentImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcessSegmentImpl extends MinimalEObjectImpl.Container implements ProcessSegment {
	/**
	 * The cached value of the '{@link #getChildren() <em>Children</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildren()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessSegment> children;

	/**
	 * The cached value of the '{@link #getProcessSegmentParameters() <em>Process Segment Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegmentParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessSegmentParameter> processSegmentParameters;

	/**
	 * The cached value of the '{@link #getPersonnelSegmentSpecifications() <em>Personnel Segment Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelSegmentSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelSegmentSpecification> personnelSegmentSpecifications;

	/**
	 * The cached value of the '{@link #getEquipmentSegmentSpecifications() <em>Equipment Segment Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentSegmentSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentSegmentSpecification> equipmentSegmentSpecifications;

	/**
	 * The cached value of the '{@link #getPhysicalAssetSegmentSpecifications() <em>Physical Asset Segment Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetSegmentSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetSegmentSpecification> physicalAssetSegmentSpecifications;

	/**
	 * The cached value of the '{@link #getMaterialSegmentSpecifications() <em>Material Segment Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSegmentSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSegmentSpecification> materialSegmentSpecifications;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected static final OperationsType OPERATIONS_TYPE_EDEFAULT = OperationsType.PRODUCTION;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsType operationsType = OPERATIONS_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal DURATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal duration = DURATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDurationUnitOfMeasure() <em>Duration Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDurationUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final String DURATION_UNIT_OF_MEASURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDurationUnitOfMeasure() <em>Duration Unit Of Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDurationUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected String durationUnitOfMeasure = DURATION_UNIT_OF_MEASURE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScope hierarchyScope;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessSegmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Iec62264Package.Literals.PROCESS_SEGMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessSegment> getChildren() {
		if (children == null) {
			children = new EObjectResolvingEList<ProcessSegment>(ProcessSegment.class, this,
					Iec62264Package.PROCESS_SEGMENT__CHILDREN);
		}
		return children;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessSegmentParameter> getProcessSegmentParameters() {
		if (processSegmentParameters == null) {
			processSegmentParameters = new EObjectContainmentEList<ProcessSegmentParameter>(
					ProcessSegmentParameter.class, this, Iec62264Package.PROCESS_SEGMENT__PROCESS_SEGMENT_PARAMETERS);
		}
		return processSegmentParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelSegmentSpecification> getPersonnelSegmentSpecifications() {
		if (personnelSegmentSpecifications == null) {
			personnelSegmentSpecifications = new EObjectContainmentEList<PersonnelSegmentSpecification>(
					PersonnelSegmentSpecification.class, this,
					Iec62264Package.PROCESS_SEGMENT__PERSONNEL_SEGMENT_SPECIFICATIONS);
		}
		return personnelSegmentSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentSegmentSpecification> getEquipmentSegmentSpecifications() {
		if (equipmentSegmentSpecifications == null) {
			equipmentSegmentSpecifications = new EObjectContainmentEList<EquipmentSegmentSpecification>(
					EquipmentSegmentSpecification.class, this,
					Iec62264Package.PROCESS_SEGMENT__EQUIPMENT_SEGMENT_SPECIFICATIONS);
		}
		return equipmentSegmentSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetSegmentSpecification> getPhysicalAssetSegmentSpecifications() {
		if (physicalAssetSegmentSpecifications == null) {
			physicalAssetSegmentSpecifications = new EObjectContainmentEList<PhysicalAssetSegmentSpecification>(
					PhysicalAssetSegmentSpecification.class, this,
					Iec62264Package.PROCESS_SEGMENT__PHYSICAL_ASSET_SEGMENT_SPECIFICATIONS);
		}
		return physicalAssetSegmentSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSegmentSpecification> getMaterialSegmentSpecifications() {
		if (materialSegmentSpecifications == null) {
			materialSegmentSpecifications = new EObjectContainmentEList<MaterialSegmentSpecification>(
					MaterialSegmentSpecification.class, this,
					Iec62264Package.PROCESS_SEGMENT__MATERIAL_SEGMENT_SPECIFICATIONS);
		}
		return materialSegmentSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PROCESS_SEGMENT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PROCESS_SEGMENT__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsType newOperationsType) {
		OperationsType oldOperationsType = operationsType;
		operationsType = newOperationsType == null ? OPERATIONS_TYPE_EDEFAULT : newOperationsType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PROCESS_SEGMENT__OPERATIONS_TYPE,
					oldOperationsType, operationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getDuration() {
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDuration(BigDecimal newDuration) {
		BigDecimal oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PROCESS_SEGMENT__DURATION,
					oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDurationUnitOfMeasure() {
		return durationUnitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDurationUnitOfMeasure(String newDurationUnitOfMeasure) {
		String oldDurationUnitOfMeasure = durationUnitOfMeasure;
		durationUnitOfMeasure = newDurationUnitOfMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Iec62264Package.PROCESS_SEGMENT__DURATION_UNIT_OF_MEASURE, oldDurationUnitOfMeasure,
					durationUnitOfMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope getHierarchyScope() {
		if (hierarchyScope != null && hierarchyScope.eIsProxy()) {
			InternalEObject oldHierarchyScope = (InternalEObject) hierarchyScope;
			hierarchyScope = (HierarchyScope) eResolveProxy(oldHierarchyScope);
			if (hierarchyScope != oldHierarchyScope) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Iec62264Package.PROCESS_SEGMENT__HIERARCHY_SCOPE, oldHierarchyScope, hierarchyScope));
			}
		}
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScope basicGetHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScope newHierarchyScope) {
		HierarchyScope oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Iec62264Package.PROCESS_SEGMENT__HIERARCHY_SCOPE,
					oldHierarchyScope, hierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT__PROCESS_SEGMENT_PARAMETERS:
			return ((InternalEList<?>) getProcessSegmentParameters()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PROCESS_SEGMENT__PERSONNEL_SEGMENT_SPECIFICATIONS:
			return ((InternalEList<?>) getPersonnelSegmentSpecifications()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PROCESS_SEGMENT__EQUIPMENT_SEGMENT_SPECIFICATIONS:
			return ((InternalEList<?>) getEquipmentSegmentSpecifications()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PROCESS_SEGMENT__PHYSICAL_ASSET_SEGMENT_SPECIFICATIONS:
			return ((InternalEList<?>) getPhysicalAssetSegmentSpecifications()).basicRemove(otherEnd, msgs);
		case Iec62264Package.PROCESS_SEGMENT__MATERIAL_SEGMENT_SPECIFICATIONS:
			return ((InternalEList<?>) getMaterialSegmentSpecifications()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT__CHILDREN:
			return getChildren();
		case Iec62264Package.PROCESS_SEGMENT__PROCESS_SEGMENT_PARAMETERS:
			return getProcessSegmentParameters();
		case Iec62264Package.PROCESS_SEGMENT__PERSONNEL_SEGMENT_SPECIFICATIONS:
			return getPersonnelSegmentSpecifications();
		case Iec62264Package.PROCESS_SEGMENT__EQUIPMENT_SEGMENT_SPECIFICATIONS:
			return getEquipmentSegmentSpecifications();
		case Iec62264Package.PROCESS_SEGMENT__PHYSICAL_ASSET_SEGMENT_SPECIFICATIONS:
			return getPhysicalAssetSegmentSpecifications();
		case Iec62264Package.PROCESS_SEGMENT__MATERIAL_SEGMENT_SPECIFICATIONS:
			return getMaterialSegmentSpecifications();
		case Iec62264Package.PROCESS_SEGMENT__ID:
			return getId();
		case Iec62264Package.PROCESS_SEGMENT__DESCRIPTION:
			return getDescription();
		case Iec62264Package.PROCESS_SEGMENT__OPERATIONS_TYPE:
			return getOperationsType();
		case Iec62264Package.PROCESS_SEGMENT__DURATION:
			return getDuration();
		case Iec62264Package.PROCESS_SEGMENT__DURATION_UNIT_OF_MEASURE:
			return getDurationUnitOfMeasure();
		case Iec62264Package.PROCESS_SEGMENT__HIERARCHY_SCOPE:
			if (resolve)
				return getHierarchyScope();
			return basicGetHierarchyScope();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT__CHILDREN:
			getChildren().clear();
			getChildren().addAll((Collection<? extends ProcessSegment>) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT__PROCESS_SEGMENT_PARAMETERS:
			getProcessSegmentParameters().clear();
			getProcessSegmentParameters().addAll((Collection<? extends ProcessSegmentParameter>) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT__PERSONNEL_SEGMENT_SPECIFICATIONS:
			getPersonnelSegmentSpecifications().clear();
			getPersonnelSegmentSpecifications().addAll((Collection<? extends PersonnelSegmentSpecification>) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT__EQUIPMENT_SEGMENT_SPECIFICATIONS:
			getEquipmentSegmentSpecifications().clear();
			getEquipmentSegmentSpecifications().addAll((Collection<? extends EquipmentSegmentSpecification>) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT__PHYSICAL_ASSET_SEGMENT_SPECIFICATIONS:
			getPhysicalAssetSegmentSpecifications().clear();
			getPhysicalAssetSegmentSpecifications()
					.addAll((Collection<? extends PhysicalAssetSegmentSpecification>) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT__MATERIAL_SEGMENT_SPECIFICATIONS:
			getMaterialSegmentSpecifications().clear();
			getMaterialSegmentSpecifications().addAll((Collection<? extends MaterialSegmentSpecification>) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT__ID:
			setId((String) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT__OPERATIONS_TYPE:
			setOperationsType((OperationsType) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT__DURATION:
			setDuration((BigDecimal) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT__DURATION_UNIT_OF_MEASURE:
			setDurationUnitOfMeasure((String) newValue);
			return;
		case Iec62264Package.PROCESS_SEGMENT__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT__CHILDREN:
			getChildren().clear();
			return;
		case Iec62264Package.PROCESS_SEGMENT__PROCESS_SEGMENT_PARAMETERS:
			getProcessSegmentParameters().clear();
			return;
		case Iec62264Package.PROCESS_SEGMENT__PERSONNEL_SEGMENT_SPECIFICATIONS:
			getPersonnelSegmentSpecifications().clear();
			return;
		case Iec62264Package.PROCESS_SEGMENT__EQUIPMENT_SEGMENT_SPECIFICATIONS:
			getEquipmentSegmentSpecifications().clear();
			return;
		case Iec62264Package.PROCESS_SEGMENT__PHYSICAL_ASSET_SEGMENT_SPECIFICATIONS:
			getPhysicalAssetSegmentSpecifications().clear();
			return;
		case Iec62264Package.PROCESS_SEGMENT__MATERIAL_SEGMENT_SPECIFICATIONS:
			getMaterialSegmentSpecifications().clear();
			return;
		case Iec62264Package.PROCESS_SEGMENT__ID:
			setId(ID_EDEFAULT);
			return;
		case Iec62264Package.PROCESS_SEGMENT__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case Iec62264Package.PROCESS_SEGMENT__OPERATIONS_TYPE:
			setOperationsType(OPERATIONS_TYPE_EDEFAULT);
			return;
		case Iec62264Package.PROCESS_SEGMENT__DURATION:
			setDuration(DURATION_EDEFAULT);
			return;
		case Iec62264Package.PROCESS_SEGMENT__DURATION_UNIT_OF_MEASURE:
			setDurationUnitOfMeasure(DURATION_UNIT_OF_MEASURE_EDEFAULT);
			return;
		case Iec62264Package.PROCESS_SEGMENT__HIERARCHY_SCOPE:
			setHierarchyScope((HierarchyScope) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Iec62264Package.PROCESS_SEGMENT__CHILDREN:
			return children != null && !children.isEmpty();
		case Iec62264Package.PROCESS_SEGMENT__PROCESS_SEGMENT_PARAMETERS:
			return processSegmentParameters != null && !processSegmentParameters.isEmpty();
		case Iec62264Package.PROCESS_SEGMENT__PERSONNEL_SEGMENT_SPECIFICATIONS:
			return personnelSegmentSpecifications != null && !personnelSegmentSpecifications.isEmpty();
		case Iec62264Package.PROCESS_SEGMENT__EQUIPMENT_SEGMENT_SPECIFICATIONS:
			return equipmentSegmentSpecifications != null && !equipmentSegmentSpecifications.isEmpty();
		case Iec62264Package.PROCESS_SEGMENT__PHYSICAL_ASSET_SEGMENT_SPECIFICATIONS:
			return physicalAssetSegmentSpecifications != null && !physicalAssetSegmentSpecifications.isEmpty();
		case Iec62264Package.PROCESS_SEGMENT__MATERIAL_SEGMENT_SPECIFICATIONS:
			return materialSegmentSpecifications != null && !materialSegmentSpecifications.isEmpty();
		case Iec62264Package.PROCESS_SEGMENT__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		case Iec62264Package.PROCESS_SEGMENT__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case Iec62264Package.PROCESS_SEGMENT__OPERATIONS_TYPE:
			return operationsType != OPERATIONS_TYPE_EDEFAULT;
		case Iec62264Package.PROCESS_SEGMENT__DURATION:
			return DURATION_EDEFAULT == null ? duration != null : !DURATION_EDEFAULT.equals(duration);
		case Iec62264Package.PROCESS_SEGMENT__DURATION_UNIT_OF_MEASURE:
			return DURATION_UNIT_OF_MEASURE_EDEFAULT == null ? durationUnitOfMeasure != null
					: !DURATION_UNIT_OF_MEASURE_EDEFAULT.equals(durationUnitOfMeasure);
		case Iec62264Package.PROCESS_SEGMENT__HIERARCHY_SCOPE:
			return hierarchyScope != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", operationsType: ");
		result.append(operationsType);
		result.append(", duration: ");
		result.append(duration);
		result.append(", durationUnitOfMeasure: ");
		result.append(durationUnitOfMeasure);
		result.append(')');
		return result.toString();
	}

} //ProcessSegmentImpl
