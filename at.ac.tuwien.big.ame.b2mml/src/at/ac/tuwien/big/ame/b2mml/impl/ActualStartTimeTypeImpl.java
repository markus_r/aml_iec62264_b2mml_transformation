/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.ActualStartTimeType;
import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actual Start Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ActualStartTimeTypeImpl extends DateTimeTypeImpl implements ActualStartTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActualStartTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getActualStartTimeType();
	}

} //ActualStartTimeTypeImpl
