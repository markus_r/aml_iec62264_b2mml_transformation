/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type130</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType130#getAcknowledge <em>Acknowledge</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType130#getEquipmentCapabilityTestSpec <em>Equipment Capability Test Spec</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType130()
 * @model extendedMetaData="name='DataArea_._131_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType130 extends EObject {
	/**
	 * Returns the value of the '<em><b>Acknowledge</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge</em>' containment reference.
	 * @see #setAcknowledge(TransAcknowledgeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType130_Acknowledge()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Acknowledge' namespace='##targetNamespace'"
	 * @generated
	 */
	TransAcknowledgeType getAcknowledge();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType130#getAcknowledge <em>Acknowledge</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge</em>' containment reference.
	 * @see #getAcknowledge()
	 * @generated
	 */
	void setAcknowledge(TransAcknowledgeType value);

	/**
	 * Returns the value of the '<em><b>Equipment Capability Test Spec</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.EquipmentCapabilityTestSpecificationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Capability Test Spec</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Capability Test Spec</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType130_EquipmentCapabilityTestSpec()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='EquipmentCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentCapabilityTestSpecificationType> getEquipmentCapabilityTestSpec();

} // DataAreaType130
