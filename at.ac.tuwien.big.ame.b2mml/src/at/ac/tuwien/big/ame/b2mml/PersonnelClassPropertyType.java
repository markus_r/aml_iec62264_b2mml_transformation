/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Personnel Class Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PersonnelClassPropertyType#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PersonnelClassPropertyType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PersonnelClassPropertyType#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PersonnelClassPropertyType#getPersonnelClassProperty <em>Personnel Class Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PersonnelClassPropertyType#getQualificationTestSpecificationID <em>Qualification Test Specification ID</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelClassPropertyType()
 * @model extendedMetaData="name='PersonnelClassPropertyType' kind='elementOnly'"
 * @generated
 */
public interface PersonnelClassPropertyType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelClassPropertyType_ID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.PersonnelClassPropertyType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelClassPropertyType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.ValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelClassPropertyType_Value()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Value' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ValueType> getValue();

	/**
	 * Returns the value of the '<em><b>Personnel Class Property</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.PersonnelClassPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Class Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Class Property</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelClassPropertyType_PersonnelClassProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonnelClassProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PersonnelClassPropertyType> getPersonnelClassProperty();

	/**
	 * Returns the value of the '<em><b>Qualification Test Specification ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.QualificationTestSpecificationIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualification Test Specification ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualification Test Specification ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelClassPropertyType_QualificationTestSpecificationID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='QualificationTestSpecificationID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QualificationTestSpecificationIDType> getQualificationTestSpecificationID();

} // PersonnelClassPropertyType
