/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.TransReceiverType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trans Receiver Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransReceiverTypeImpl#getLogicalID <em>Logical ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransReceiverTypeImpl#getComponentID <em>Component ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransReceiverTypeImpl#getID <em>ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransReceiverTypeImpl extends MinimalEObjectImpl.Container implements TransReceiverType {
	/**
	 * The cached value of the '{@link #getLogicalID() <em>Logical ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogicalID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType logicalID;

	/**
	 * The cached value of the '{@link #getComponentID() <em>Component ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType componentID;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected EList<IdentifierType> iD;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransReceiverTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTransReceiverType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getLogicalID() {
		return logicalID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLogicalID(IdentifierType newLogicalID, NotificationChain msgs) {
		IdentifierType oldLogicalID = logicalID;
		logicalID = newLogicalID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_RECEIVER_TYPE__LOGICAL_ID, oldLogicalID, newLogicalID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLogicalID(IdentifierType newLogicalID) {
		if (newLogicalID != logicalID) {
			NotificationChain msgs = null;
			if (logicalID != null)
				msgs = ((InternalEObject)logicalID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_RECEIVER_TYPE__LOGICAL_ID, null, msgs);
			if (newLogicalID != null)
				msgs = ((InternalEObject)newLogicalID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_RECEIVER_TYPE__LOGICAL_ID, null, msgs);
			msgs = basicSetLogicalID(newLogicalID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_RECEIVER_TYPE__LOGICAL_ID, newLogicalID, newLogicalID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getComponentID() {
		return componentID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentID(IdentifierType newComponentID, NotificationChain msgs) {
		IdentifierType oldComponentID = componentID;
		componentID = newComponentID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_RECEIVER_TYPE__COMPONENT_ID, oldComponentID, newComponentID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentID(IdentifierType newComponentID) {
		if (newComponentID != componentID) {
			NotificationChain msgs = null;
			if (componentID != null)
				msgs = ((InternalEObject)componentID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_RECEIVER_TYPE__COMPONENT_ID, null, msgs);
			if (newComponentID != null)
				msgs = ((InternalEObject)newComponentID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_RECEIVER_TYPE__COMPONENT_ID, null, msgs);
			msgs = basicSetComponentID(newComponentID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_RECEIVER_TYPE__COMPONENT_ID, newComponentID, newComponentID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IdentifierType> getID() {
		if (iD == null) {
			iD = new EObjectContainmentEList<IdentifierType>(IdentifierType.class, this, B2MMLPackage.TRANS_RECEIVER_TYPE__ID);
		}
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TRANS_RECEIVER_TYPE__LOGICAL_ID:
				return basicSetLogicalID(null, msgs);
			case B2MMLPackage.TRANS_RECEIVER_TYPE__COMPONENT_ID:
				return basicSetComponentID(null, msgs);
			case B2MMLPackage.TRANS_RECEIVER_TYPE__ID:
				return ((InternalEList<?>)getID()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TRANS_RECEIVER_TYPE__LOGICAL_ID:
				return getLogicalID();
			case B2MMLPackage.TRANS_RECEIVER_TYPE__COMPONENT_ID:
				return getComponentID();
			case B2MMLPackage.TRANS_RECEIVER_TYPE__ID:
				return getID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TRANS_RECEIVER_TYPE__LOGICAL_ID:
				setLogicalID((IdentifierType)newValue);
				return;
			case B2MMLPackage.TRANS_RECEIVER_TYPE__COMPONENT_ID:
				setComponentID((IdentifierType)newValue);
				return;
			case B2MMLPackage.TRANS_RECEIVER_TYPE__ID:
				getID().clear();
				getID().addAll((Collection<? extends IdentifierType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_RECEIVER_TYPE__LOGICAL_ID:
				setLogicalID((IdentifierType)null);
				return;
			case B2MMLPackage.TRANS_RECEIVER_TYPE__COMPONENT_ID:
				setComponentID((IdentifierType)null);
				return;
			case B2MMLPackage.TRANS_RECEIVER_TYPE__ID:
				getID().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_RECEIVER_TYPE__LOGICAL_ID:
				return logicalID != null;
			case B2MMLPackage.TRANS_RECEIVER_TYPE__COMPONENT_ID:
				return componentID != null;
			case B2MMLPackage.TRANS_RECEIVER_TYPE__ID:
				return iD != null && !iD.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TransReceiverTypeImpl
