/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DataAreaType92;
import at.ac.tuwien.big.ame.b2mml.RespondOperationsScheduleType;
import at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Respond Operations Schedule Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.RespondOperationsScheduleTypeImpl#getApplicationArea <em>Application Area</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.RespondOperationsScheduleTypeImpl#getDataArea <em>Data Area</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.RespondOperationsScheduleTypeImpl#getReleaseID <em>Release ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.RespondOperationsScheduleTypeImpl#getVersionID <em>Version ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RespondOperationsScheduleTypeImpl extends MinimalEObjectImpl.Container implements RespondOperationsScheduleType {
	/**
	 * The cached value of the '{@link #getApplicationArea() <em>Application Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplicationArea()
	 * @generated
	 * @ordered
	 */
	protected TransApplicationAreaType applicationArea;

	/**
	 * The cached value of the '{@link #getDataArea() <em>Data Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataArea()
	 * @generated
	 * @ordered
	 */
	protected DataAreaType92 dataArea;

	/**
	 * The default value of the '{@link #getReleaseID() <em>Release ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReleaseID()
	 * @generated
	 * @ordered
	 */
	protected static final String RELEASE_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReleaseID() <em>Release ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReleaseID()
	 * @generated
	 * @ordered
	 */
	protected String releaseID = RELEASE_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersionID() <em>Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersionID()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersionID() <em>Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersionID()
	 * @generated
	 * @ordered
	 */
	protected String versionID = VERSION_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RespondOperationsScheduleTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getRespondOperationsScheduleType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransApplicationAreaType getApplicationArea() {
		return applicationArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetApplicationArea(TransApplicationAreaType newApplicationArea, NotificationChain msgs) {
		TransApplicationAreaType oldApplicationArea = applicationArea;
		applicationArea = newApplicationArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__APPLICATION_AREA, oldApplicationArea, newApplicationArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicationArea(TransApplicationAreaType newApplicationArea) {
		if (newApplicationArea != applicationArea) {
			NotificationChain msgs = null;
			if (applicationArea != null)
				msgs = ((InternalEObject)applicationArea).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__APPLICATION_AREA, null, msgs);
			if (newApplicationArea != null)
				msgs = ((InternalEObject)newApplicationArea).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__APPLICATION_AREA, null, msgs);
			msgs = basicSetApplicationArea(newApplicationArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__APPLICATION_AREA, newApplicationArea, newApplicationArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType92 getDataArea() {
		return dataArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataArea(DataAreaType92 newDataArea, NotificationChain msgs) {
		DataAreaType92 oldDataArea = dataArea;
		dataArea = newDataArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__DATA_AREA, oldDataArea, newDataArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataArea(DataAreaType92 newDataArea) {
		if (newDataArea != dataArea) {
			NotificationChain msgs = null;
			if (dataArea != null)
				msgs = ((InternalEObject)dataArea).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__DATA_AREA, null, msgs);
			if (newDataArea != null)
				msgs = ((InternalEObject)newDataArea).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__DATA_AREA, null, msgs);
			msgs = basicSetDataArea(newDataArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__DATA_AREA, newDataArea, newDataArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReleaseID() {
		return releaseID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReleaseID(String newReleaseID) {
		String oldReleaseID = releaseID;
		releaseID = newReleaseID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__RELEASE_ID, oldReleaseID, releaseID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersionID() {
		return versionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersionID(String newVersionID) {
		String oldVersionID = versionID;
		versionID = newVersionID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__VERSION_ID, oldVersionID, versionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__APPLICATION_AREA:
				return basicSetApplicationArea(null, msgs);
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__DATA_AREA:
				return basicSetDataArea(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__APPLICATION_AREA:
				return getApplicationArea();
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__DATA_AREA:
				return getDataArea();
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__RELEASE_ID:
				return getReleaseID();
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__VERSION_ID:
				return getVersionID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__APPLICATION_AREA:
				setApplicationArea((TransApplicationAreaType)newValue);
				return;
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__DATA_AREA:
				setDataArea((DataAreaType92)newValue);
				return;
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__RELEASE_ID:
				setReleaseID((String)newValue);
				return;
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__VERSION_ID:
				setVersionID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__APPLICATION_AREA:
				setApplicationArea((TransApplicationAreaType)null);
				return;
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__DATA_AREA:
				setDataArea((DataAreaType92)null);
				return;
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__RELEASE_ID:
				setReleaseID(RELEASE_ID_EDEFAULT);
				return;
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__VERSION_ID:
				setVersionID(VERSION_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__APPLICATION_AREA:
				return applicationArea != null;
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__DATA_AREA:
				return dataArea != null;
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__RELEASE_ID:
				return RELEASE_ID_EDEFAULT == null ? releaseID != null : !RELEASE_ID_EDEFAULT.equals(releaseID);
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE__VERSION_ID:
				return VERSION_ID_EDEFAULT == null ? versionID != null : !VERSION_ID_EDEFAULT.equals(versionID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (releaseID: ");
		result.append(releaseID);
		result.append(", versionID: ");
		result.append(versionID);
		result.append(')');
		return result.toString();
	}

} //RespondOperationsScheduleTypeImpl
