/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.LocationType;
import at.ac.tuwien.big.ame.b2mml.MaterialClassType;
import at.ac.tuwien.big.ame.b2mml.MaterialDefinitionType;
import at.ac.tuwien.big.ame.b2mml.MaterialInformationType;
import at.ac.tuwien.big.ame.b2mml.MaterialLotType;
import at.ac.tuwien.big.ame.b2mml.MaterialSubLotType;
import at.ac.tuwien.big.ame.b2mml.MaterialTestSpecificationType;
import at.ac.tuwien.big.ame.b2mml.PublishedDateType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Information Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MaterialInformationTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MaterialInformationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MaterialInformationTypeImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MaterialInformationTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MaterialInformationTypeImpl#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MaterialInformationTypeImpl#getMaterialClass <em>Material Class</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MaterialInformationTypeImpl#getMaterialDefinition <em>Material Definition</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MaterialInformationTypeImpl#getMaterialLot <em>Material Lot</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MaterialInformationTypeImpl#getMaterialSubLot <em>Material Sub Lot</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MaterialInformationTypeImpl#getMaterialTestSpecification <em>Material Test Specification</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MaterialInformationTypeImpl extends MinimalEObjectImpl.Container implements MaterialInformationType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * This is true if the ID containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iDESet;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected LocationType location;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * This is true if the Hierarchy Scope containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean hierarchyScopeESet;

	/**
	 * The cached value of the '{@link #getPublishedDate() <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublishedDate()
	 * @generated
	 * @ordered
	 */
	protected PublishedDateType publishedDate;

	/**
	 * This is true if the Published Date containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean publishedDateESet;

	/**
	 * The cached value of the '{@link #getMaterialClass() <em>Material Class</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClass()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialClassType> materialClass;

	/**
	 * The cached value of the '{@link #getMaterialDefinition() <em>Material Definition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialDefinitionType> materialDefinition;

	/**
	 * The cached value of the '{@link #getMaterialLot() <em>Material Lot</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialLot()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialLotType> materialLot;

	/**
	 * The cached value of the '{@link #getMaterialSubLot() <em>Material Sub Lot</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSubLot()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSubLotType> materialSubLot;

	/**
	 * The cached value of the '{@link #getMaterialTestSpecification() <em>Material Test Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialTestSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialTestSpecificationType> materialTestSpecification;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialInformationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getMaterialInformationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		boolean oldIDESet = iDESet;
		iDESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__ID, oldID, newID, !oldIDESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_INFORMATION_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_INFORMATION_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldIDESet = iDESet;
			iDESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__ID, newID, newID, !oldIDESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetID(NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = null;
		boolean oldIDESet = iDESet;
		iDESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__ID, oldID, null, oldIDESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetID() {
		if (iD != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_INFORMATION_TYPE__ID, null, msgs);
			msgs = basicUnsetID(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldIDESet = iDESet;
			iDESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__ID, null, null, oldIDESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetID() {
		return iDESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.MATERIAL_INFORMATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationType getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocation(LocationType newLocation, NotificationChain msgs) {
		LocationType oldLocation = location;
		location = newLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__LOCATION, oldLocation, newLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(LocationType newLocation) {
		if (newLocation != location) {
			NotificationChain msgs = null;
			if (location != null)
				msgs = ((InternalEObject)location).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_INFORMATION_TYPE__LOCATION, null, msgs);
			if (newLocation != null)
				msgs = ((InternalEObject)newLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_INFORMATION_TYPE__LOCATION, null, msgs);
			msgs = basicSetLocation(newLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__LOCATION, newLocation, newLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		boolean oldHierarchyScopeESet = hierarchyScopeESet;
		hierarchyScopeESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope, !oldHierarchyScopeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_INFORMATION_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_INFORMATION_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldHierarchyScopeESet = hierarchyScopeESet;
			hierarchyScopeESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope, !oldHierarchyScopeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetHierarchyScope(NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = null;
		boolean oldHierarchyScopeESet = hierarchyScopeESet;
		hierarchyScopeESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, null, oldHierarchyScopeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHierarchyScope() {
		if (hierarchyScope != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_INFORMATION_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicUnsetHierarchyScope(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldHierarchyScopeESet = hierarchyScopeESet;
			hierarchyScopeESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__HIERARCHY_SCOPE, null, null, oldHierarchyScopeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHierarchyScope() {
		return hierarchyScopeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PublishedDateType getPublishedDate() {
		return publishedDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPublishedDate(PublishedDateType newPublishedDate, NotificationChain msgs) {
		PublishedDateType oldPublishedDate = publishedDate;
		publishedDate = newPublishedDate;
		boolean oldPublishedDateESet = publishedDateESet;
		publishedDateESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__PUBLISHED_DATE, oldPublishedDate, newPublishedDate, !oldPublishedDateESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublishedDate(PublishedDateType newPublishedDate) {
		if (newPublishedDate != publishedDate) {
			NotificationChain msgs = null;
			if (publishedDate != null)
				msgs = ((InternalEObject)publishedDate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_INFORMATION_TYPE__PUBLISHED_DATE, null, msgs);
			if (newPublishedDate != null)
				msgs = ((InternalEObject)newPublishedDate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_INFORMATION_TYPE__PUBLISHED_DATE, null, msgs);
			msgs = basicSetPublishedDate(newPublishedDate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldPublishedDateESet = publishedDateESet;
			publishedDateESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__PUBLISHED_DATE, newPublishedDate, newPublishedDate, !oldPublishedDateESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetPublishedDate(NotificationChain msgs) {
		PublishedDateType oldPublishedDate = publishedDate;
		publishedDate = null;
		boolean oldPublishedDateESet = publishedDateESet;
		publishedDateESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__PUBLISHED_DATE, oldPublishedDate, null, oldPublishedDateESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPublishedDate() {
		if (publishedDate != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)publishedDate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_INFORMATION_TYPE__PUBLISHED_DATE, null, msgs);
			msgs = basicUnsetPublishedDate(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldPublishedDateESet = publishedDateESet;
			publishedDateESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.MATERIAL_INFORMATION_TYPE__PUBLISHED_DATE, null, null, oldPublishedDateESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPublishedDate() {
		return publishedDateESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialClassType> getMaterialClass() {
		if (materialClass == null) {
			materialClass = new EObjectContainmentEList<MaterialClassType>(MaterialClassType.class, this, B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_CLASS);
		}
		return materialClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialDefinitionType> getMaterialDefinition() {
		if (materialDefinition == null) {
			materialDefinition = new EObjectContainmentEList<MaterialDefinitionType>(MaterialDefinitionType.class, this, B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_DEFINITION);
		}
		return materialDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialLotType> getMaterialLot() {
		if (materialLot == null) {
			materialLot = new EObjectContainmentEList<MaterialLotType>(MaterialLotType.class, this, B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_LOT);
		}
		return materialLot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSubLotType> getMaterialSubLot() {
		if (materialSubLot == null) {
			materialSubLot = new EObjectContainmentEList<MaterialSubLotType>(MaterialSubLotType.class, this, B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_SUB_LOT);
		}
		return materialSubLot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialTestSpecificationType> getMaterialTestSpecification() {
		if (materialTestSpecification == null) {
			materialTestSpecification = new EObjectContainmentEList<MaterialTestSpecificationType>(MaterialTestSpecificationType.class, this, B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_TEST_SPECIFICATION);
		}
		return materialTestSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__ID:
				return basicUnsetID(msgs);
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__LOCATION:
				return basicSetLocation(null, msgs);
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__HIERARCHY_SCOPE:
				return basicUnsetHierarchyScope(msgs);
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__PUBLISHED_DATE:
				return basicUnsetPublishedDate(msgs);
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_CLASS:
				return ((InternalEList<?>)getMaterialClass()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_DEFINITION:
				return ((InternalEList<?>)getMaterialDefinition()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_LOT:
				return ((InternalEList<?>)getMaterialLot()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_SUB_LOT:
				return ((InternalEList<?>)getMaterialSubLot()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_TEST_SPECIFICATION:
				return ((InternalEList<?>)getMaterialTestSpecification()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__ID:
				return getID();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__LOCATION:
				return getLocation();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__PUBLISHED_DATE:
				return getPublishedDate();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_CLASS:
				return getMaterialClass();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_DEFINITION:
				return getMaterialDefinition();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_LOT:
				return getMaterialLot();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_SUB_LOT:
				return getMaterialSubLot();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_TEST_SPECIFICATION:
				return getMaterialTestSpecification();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__LOCATION:
				setLocation((LocationType)newValue);
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__PUBLISHED_DATE:
				setPublishedDate((PublishedDateType)newValue);
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_CLASS:
				getMaterialClass().clear();
				getMaterialClass().addAll((Collection<? extends MaterialClassType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_DEFINITION:
				getMaterialDefinition().clear();
				getMaterialDefinition().addAll((Collection<? extends MaterialDefinitionType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_LOT:
				getMaterialLot().clear();
				getMaterialLot().addAll((Collection<? extends MaterialLotType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_SUB_LOT:
				getMaterialSubLot().clear();
				getMaterialSubLot().addAll((Collection<? extends MaterialSubLotType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_TEST_SPECIFICATION:
				getMaterialTestSpecification().clear();
				getMaterialTestSpecification().addAll((Collection<? extends MaterialTestSpecificationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__ID:
				unsetID();
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__LOCATION:
				setLocation((LocationType)null);
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__HIERARCHY_SCOPE:
				unsetHierarchyScope();
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__PUBLISHED_DATE:
				unsetPublishedDate();
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_CLASS:
				getMaterialClass().clear();
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_DEFINITION:
				getMaterialDefinition().clear();
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_LOT:
				getMaterialLot().clear();
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_SUB_LOT:
				getMaterialSubLot().clear();
				return;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_TEST_SPECIFICATION:
				getMaterialTestSpecification().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__ID:
				return isSetID();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__LOCATION:
				return location != null;
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__HIERARCHY_SCOPE:
				return isSetHierarchyScope();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__PUBLISHED_DATE:
				return isSetPublishedDate();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_CLASS:
				return materialClass != null && !materialClass.isEmpty();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_DEFINITION:
				return materialDefinition != null && !materialDefinition.isEmpty();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_LOT:
				return materialLot != null && !materialLot.isEmpty();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_SUB_LOT:
				return materialSubLot != null && !materialSubLot.isEmpty();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE__MATERIAL_TEST_SPECIFICATION:
				return materialTestSpecification != null && !materialTestSpecification.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MaterialInformationTypeImpl
