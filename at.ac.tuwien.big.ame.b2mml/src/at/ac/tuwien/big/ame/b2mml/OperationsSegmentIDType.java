/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Segment ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsSegmentIDType()
 * @model extendedMetaData="name='OperationsSegmentIDType' kind='simple'"
 * @generated
 */
public interface OperationsSegmentIDType extends IdentifierType {
} // OperationsSegmentIDType
