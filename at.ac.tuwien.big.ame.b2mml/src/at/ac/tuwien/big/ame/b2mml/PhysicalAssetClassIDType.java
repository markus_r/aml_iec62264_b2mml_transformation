/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Class ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetClassIDType()
 * @model extendedMetaData="name='PhysicalAssetClassIDType' kind='simple'"
 * @generated
 */
public interface PhysicalAssetClassIDType extends IdentifierType {
} // PhysicalAssetClassIDType
