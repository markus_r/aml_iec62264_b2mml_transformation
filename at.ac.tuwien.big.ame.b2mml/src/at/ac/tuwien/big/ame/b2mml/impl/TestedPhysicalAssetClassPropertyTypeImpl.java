/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassIDType;
import at.ac.tuwien.big.ame.b2mml.PropertyIDType;
import at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetClassPropertyType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tested Physical Asset Class Property Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TestedPhysicalAssetClassPropertyTypeImpl#getPhysicalAssetClassID <em>Physical Asset Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TestedPhysicalAssetClassPropertyTypeImpl#getPropertyID <em>Property ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestedPhysicalAssetClassPropertyTypeImpl extends MinimalEObjectImpl.Container implements TestedPhysicalAssetClassPropertyType {
	/**
	 * The cached value of the '{@link #getPhysicalAssetClassID() <em>Physical Asset Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClassID()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetClassIDType physicalAssetClassID;

	/**
	 * The cached value of the '{@link #getPropertyID() <em>Property ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyID()
	 * @generated
	 * @ordered
	 */
	protected PropertyIDType propertyID;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestedPhysicalAssetClassPropertyTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTestedPhysicalAssetClassPropertyType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClassIDType getPhysicalAssetClassID() {
		return physicalAssetClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPhysicalAssetClassID(PhysicalAssetClassIDType newPhysicalAssetClassID, NotificationChain msgs) {
		PhysicalAssetClassIDType oldPhysicalAssetClassID = physicalAssetClassID;
		physicalAssetClassID = newPhysicalAssetClassID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_ID, oldPhysicalAssetClassID, newPhysicalAssetClassID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetClassID(PhysicalAssetClassIDType newPhysicalAssetClassID) {
		if (newPhysicalAssetClassID != physicalAssetClassID) {
			NotificationChain msgs = null;
			if (physicalAssetClassID != null)
				msgs = ((InternalEObject)physicalAssetClassID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_ID, null, msgs);
			if (newPhysicalAssetClassID != null)
				msgs = ((InternalEObject)newPhysicalAssetClassID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_ID, null, msgs);
			msgs = basicSetPhysicalAssetClassID(newPhysicalAssetClassID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_ID, newPhysicalAssetClassID, newPhysicalAssetClassID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyIDType getPropertyID() {
		return propertyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPropertyID(PropertyIDType newPropertyID, NotificationChain msgs) {
		PropertyIDType oldPropertyID = propertyID;
		propertyID = newPropertyID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PROPERTY_ID, oldPropertyID, newPropertyID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyID(PropertyIDType newPropertyID) {
		if (newPropertyID != propertyID) {
			NotificationChain msgs = null;
			if (propertyID != null)
				msgs = ((InternalEObject)propertyID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PROPERTY_ID, null, msgs);
			if (newPropertyID != null)
				msgs = ((InternalEObject)newPropertyID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PROPERTY_ID, null, msgs);
			msgs = basicSetPropertyID(newPropertyID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PROPERTY_ID, newPropertyID, newPropertyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return basicSetPhysicalAssetClassID(null, msgs);
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				return basicSetPropertyID(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return getPhysicalAssetClassID();
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				return getPropertyID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_ID:
				setPhysicalAssetClassID((PhysicalAssetClassIDType)newValue);
				return;
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				setPropertyID((PropertyIDType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_ID:
				setPhysicalAssetClassID((PhysicalAssetClassIDType)null);
				return;
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				setPropertyID((PropertyIDType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return physicalAssetClassID != null;
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				return propertyID != null;
		}
		return super.eIsSet(featureID);
	}

} //TestedPhysicalAssetClassPropertyTypeImpl
