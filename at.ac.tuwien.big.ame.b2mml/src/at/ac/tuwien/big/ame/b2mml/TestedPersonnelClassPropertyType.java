/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tested Personnel Class Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestedPersonnelClassPropertyType#getPersonnelClassID <em>Personnel Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestedPersonnelClassPropertyType#getPropertyID <em>Property ID</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedPersonnelClassPropertyType()
 * @model extendedMetaData="name='TestedPersonnelClassPropertyType' kind='elementOnly'"
 * @generated
 */
public interface TestedPersonnelClassPropertyType extends EObject {
	/**
	 * Returns the value of the '<em><b>Personnel Class ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Class ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Class ID</em>' containment reference.
	 * @see #setPersonnelClassID(PersonnelClassIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedPersonnelClassPropertyType_PersonnelClassID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PersonnelClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	PersonnelClassIDType getPersonnelClassID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestedPersonnelClassPropertyType#getPersonnelClassID <em>Personnel Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Personnel Class ID</em>' containment reference.
	 * @see #getPersonnelClassID()
	 * @generated
	 */
	void setPersonnelClassID(PersonnelClassIDType value);

	/**
	 * Returns the value of the '<em><b>Property ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property ID</em>' containment reference.
	 * @see #setPropertyID(PropertyIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedPersonnelClassPropertyType_PropertyID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PropertyID' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyIDType getPropertyID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestedPersonnelClassPropertyType#getPropertyID <em>Property ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property ID</em>' containment reference.
	 * @see #getPropertyID()
	 * @generated
	 */
	void setPropertyID(PropertyIDType value);

} // TestedPersonnelClassPropertyType
