/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Definition ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsDefinitionIDType()
 * @model extendedMetaData="name='OperationsDefinitionIDType' kind='simple'"
 * @generated
 */
public interface OperationsDefinitionIDType extends IdentifierType {
} // OperationsDefinitionIDType
