/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Request Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getPriority <em>Priority</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getOperationsDefinitionID <em>Operations Definition ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getRequestState <em>Request State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getSegmentRequirement <em>Segment Requirement</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getSegmentResponse <em>Segment Response</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestType()
 * @model extendedMetaData="name='OperationsRequestType' kind='elementOnly'"
 * @generated
 */
public interface OperationsRequestType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestType_ID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' containment reference.
	 * @see #setOperationsType(OperationsTypeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestType_OperationsType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsType' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsTypeType getOperationsType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getOperationsType <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' containment reference.
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsTypeType value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' containment reference.
	 * @see #setStartTime(StartTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestType_StartTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='StartTime' namespace='##targetNamespace'"
	 * @generated
	 */
	StartTimeType getStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getStartTime <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' containment reference.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(StartTimeType value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' containment reference.
	 * @see #setEndTime(EndTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestType_EndTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EndTime' namespace='##targetNamespace'"
	 * @generated
	 */
	EndTimeType getEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getEndTime <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' containment reference.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(EndTimeType value);

	/**
	 * Returns the value of the '<em><b>Priority</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority</em>' containment reference.
	 * @see #setPriority(PriorityType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestType_Priority()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Priority' namespace='##targetNamespace'"
	 * @generated
	 */
	PriorityType getPriority();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getPriority <em>Priority</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority</em>' containment reference.
	 * @see #getPriority()
	 * @generated
	 */
	void setPriority(PriorityType value);

	/**
	 * Returns the value of the '<em><b>Operations Definition ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition ID</em>' containment reference.
	 * @see #setOperationsDefinitionID(OperationsDefinitionIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestType_OperationsDefinitionID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsDefinitionID' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsDefinitionIDType getOperationsDefinitionID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getOperationsDefinitionID <em>Operations Definition ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Definition ID</em>' containment reference.
	 * @see #getOperationsDefinitionID()
	 * @generated
	 */
	void setOperationsDefinitionID(OperationsDefinitionIDType value);

	/**
	 * Returns the value of the '<em><b>Request State</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Request State</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Request State</em>' containment reference.
	 * @see #setRequestState(RequestStateType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestType_RequestState()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='RequestState' namespace='##targetNamespace'"
	 * @generated
	 */
	RequestStateType getRequestState();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType#getRequestState <em>Request State</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Request State</em>' containment reference.
	 * @see #getRequestState()
	 * @generated
	 */
	void setRequestState(RequestStateType value);

	/**
	 * Returns the value of the '<em><b>Segment Requirement</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpSegmentRequirementType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment Requirement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment Requirement</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestType_SegmentRequirement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SegmentRequirement' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpSegmentRequirementType> getSegmentRequirement();

	/**
	 * Returns the value of the '<em><b>Segment Response</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment Response</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment Response</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestType_SegmentResponse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SegmentResponse' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpSegmentResponseType> getSegmentResponse();

} // OperationsRequestType
