/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dependency1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDependency1Type()
 * @model extendedMetaData="name='Dependency1Type' kind='simple'"
 * @generated
 */
public interface Dependency1Type extends CodeType {
} // Dependency1Type
