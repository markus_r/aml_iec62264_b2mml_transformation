/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type81</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType81#getShow <em>Show</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType81#getOperationsCapabilityInformation <em>Operations Capability Information</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType81()
 * @model extendedMetaData="name='DataArea_._82_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType81 extends EObject {
	/**
	 * Returns the value of the '<em><b>Show</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show</em>' containment reference.
	 * @see #setShow(TransShowType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType81_Show()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Show' namespace='##targetNamespace'"
	 * @generated
	 */
	TransShowType getShow();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType81#getShow <em>Show</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show</em>' containment reference.
	 * @see #getShow()
	 * @generated
	 */
	void setShow(TransShowType value);

	/**
	 * Returns the value of the '<em><b>Operations Capability Information</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Capability Information</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Capability Information</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType81_OperationsCapabilityInformation()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OperationsCapabilityInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsCapabilityInformationType> getOperationsCapabilityInformation();

} // DataAreaType81
