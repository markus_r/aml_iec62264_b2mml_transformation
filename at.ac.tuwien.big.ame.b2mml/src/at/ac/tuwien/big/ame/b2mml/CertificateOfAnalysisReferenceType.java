/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Certificate Of Analysis Reference Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCertificateOfAnalysisReferenceType()
 * @model extendedMetaData="name='CertificateOfAnalysisReferenceType' kind='simple'"
 * @generated
 */
public interface CertificateOfAnalysisReferenceType extends IdentifierType {
} // CertificateOfAnalysisReferenceType
