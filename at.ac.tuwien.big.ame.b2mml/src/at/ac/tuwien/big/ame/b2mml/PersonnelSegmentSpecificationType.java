/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Personnel Segment Specification Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType#getPersonnelClassID <em>Personnel Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType#getPersonID <em>Person ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType#getPersonnelUse <em>Personnel Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType#getPersonnelSegmentSpecificationProperty <em>Personnel Segment Specification Property</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelSegmentSpecificationType()
 * @model extendedMetaData="name='PersonnelSegmentSpecificationType' kind='elementOnly'"
 * @generated
 */
public interface PersonnelSegmentSpecificationType extends EObject {
	/**
	 * Returns the value of the '<em><b>Personnel Class ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Class ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Class ID</em>' containment reference.
	 * @see #setPersonnelClassID(PersonnelClassIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelSegmentSpecificationType_PersonnelClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonnelClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	PersonnelClassIDType getPersonnelClassID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType#getPersonnelClassID <em>Personnel Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Personnel Class ID</em>' containment reference.
	 * @see #getPersonnelClassID()
	 * @generated
	 */
	void setPersonnelClassID(PersonnelClassIDType value);

	/**
	 * Returns the value of the '<em><b>Person ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person ID</em>' containment reference.
	 * @see #setPersonID(PersonIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelSegmentSpecificationType_PersonID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonID' namespace='##targetNamespace'"
	 * @generated
	 */
	PersonIDType getPersonID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType#getPersonID <em>Person ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Person ID</em>' containment reference.
	 * @see #getPersonID()
	 * @generated
	 */
	void setPersonID(PersonIDType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelSegmentSpecificationType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Personnel Use</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Use</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Use</em>' containment reference.
	 * @see #setPersonnelUse(CodeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelSegmentSpecificationType_PersonnelUse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonnelUse' namespace='##targetNamespace'"
	 * @generated
	 */
	CodeType getPersonnelUse();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType#getPersonnelUse <em>Personnel Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Personnel Use</em>' containment reference.
	 * @see #getPersonnelUse()
	 * @generated
	 */
	void setPersonnelUse(CodeType value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelSegmentSpecificationType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

	/**
	 * Returns the value of the '<em><b>Personnel Segment Specification Property</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Segment Specification Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Segment Specification Property</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelSegmentSpecificationType_PersonnelSegmentSpecificationProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonnelSegmentSpecificationProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PersonnelSegmentSpecificationPropertyType> getPersonnelSegmentSpecificationProperty();

} // PersonnelSegmentSpecificationType
