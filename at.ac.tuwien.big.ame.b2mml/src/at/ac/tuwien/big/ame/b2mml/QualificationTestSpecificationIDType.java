/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Qualification Test Specification ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQualificationTestSpecificationIDType()
 * @model extendedMetaData="name='QualificationTestSpecificationIDType' kind='simple'"
 * @generated
 */
public interface QualificationTestSpecificationIDType extends IdentifierType {
} // QualificationTestSpecificationIDType
