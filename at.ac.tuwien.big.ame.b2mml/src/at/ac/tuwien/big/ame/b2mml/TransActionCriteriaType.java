/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Action Criteria Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransActionCriteriaType#getActionExpression <em>Action Expression</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransActionCriteriaType#getChangeStatus <em>Change Status</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransActionCriteriaType()
 * @model extendedMetaData="name='TransActionCriteriaType' kind='elementOnly'"
 * @generated
 */
public interface TransActionCriteriaType extends EObject {
	/**
	 * Returns the value of the '<em><b>Action Expression</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.TransExpressionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Expression</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransActionCriteriaType_ActionExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ActionExpression' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TransExpressionType> getActionExpression();

	/**
	 * Returns the value of the '<em><b>Change Status</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Status</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Status</em>' containment reference.
	 * @see #setChangeStatus(TransChangeStatusType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransActionCriteriaType_ChangeStatus()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ChangeStatus' namespace='##targetNamespace'"
	 * @generated
	 */
	TransChangeStatusType getChangeStatus();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransActionCriteriaType#getChangeStatus <em>Change Status</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Status</em>' containment reference.
	 * @see #getChangeStatus()
	 * @generated
	 */
	void setChangeStatus(TransChangeStatusType value);

} // TransActionCriteriaType
