/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Identifier Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.IdentifierTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.IdentifierTypeImpl#getSchemeAgencyID <em>Scheme Agency ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.IdentifierTypeImpl#getSchemeAgencyName <em>Scheme Agency Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.IdentifierTypeImpl#getSchemeDataURI <em>Scheme Data URI</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.IdentifierTypeImpl#getSchemeID <em>Scheme ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.IdentifierTypeImpl#getSchemeName <em>Scheme Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.IdentifierTypeImpl#getSchemeURI <em>Scheme URI</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.IdentifierTypeImpl#getSchemeVersionID <em>Scheme Version ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IdentifierTypeImpl extends MinimalEObjectImpl.Container implements IdentifierType {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemeAgencyID() <em>Scheme Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeAgencyID()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEME_AGENCY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemeAgencyID() <em>Scheme Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeAgencyID()
	 * @generated
	 * @ordered
	 */
	protected String schemeAgencyID = SCHEME_AGENCY_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemeAgencyName() <em>Scheme Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeAgencyName()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEME_AGENCY_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemeAgencyName() <em>Scheme Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeAgencyName()
	 * @generated
	 * @ordered
	 */
	protected String schemeAgencyName = SCHEME_AGENCY_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemeDataURI() <em>Scheme Data URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeDataURI()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEME_DATA_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemeDataURI() <em>Scheme Data URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeDataURI()
	 * @generated
	 * @ordered
	 */
	protected String schemeDataURI = SCHEME_DATA_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemeID() <em>Scheme ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeID()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEME_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemeID() <em>Scheme ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeID()
	 * @generated
	 * @ordered
	 */
	protected String schemeID = SCHEME_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemeName() <em>Scheme Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeName()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEME_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemeName() <em>Scheme Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeName()
	 * @generated
	 * @ordered
	 */
	protected String schemeName = SCHEME_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemeURI() <em>Scheme URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeURI()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEME_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemeURI() <em>Scheme URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeURI()
	 * @generated
	 * @ordered
	 */
	protected String schemeURI = SCHEME_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemeVersionID() <em>Scheme Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeVersionID()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEME_VERSION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemeVersionID() <em>Scheme Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemeVersionID()
	 * @generated
	 * @ordered
	 */
	protected String schemeVersionID = SCHEME_VERSION_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IdentifierTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getIdentifierType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.IDENTIFIER_TYPE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemeAgencyID() {
		return schemeAgencyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemeAgencyID(String newSchemeAgencyID) {
		String oldSchemeAgencyID = schemeAgencyID;
		schemeAgencyID = newSchemeAgencyID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.IDENTIFIER_TYPE__SCHEME_AGENCY_ID, oldSchemeAgencyID, schemeAgencyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemeAgencyName() {
		return schemeAgencyName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemeAgencyName(String newSchemeAgencyName) {
		String oldSchemeAgencyName = schemeAgencyName;
		schemeAgencyName = newSchemeAgencyName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.IDENTIFIER_TYPE__SCHEME_AGENCY_NAME, oldSchemeAgencyName, schemeAgencyName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemeDataURI() {
		return schemeDataURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemeDataURI(String newSchemeDataURI) {
		String oldSchemeDataURI = schemeDataURI;
		schemeDataURI = newSchemeDataURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.IDENTIFIER_TYPE__SCHEME_DATA_URI, oldSchemeDataURI, schemeDataURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemeID() {
		return schemeID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemeID(String newSchemeID) {
		String oldSchemeID = schemeID;
		schemeID = newSchemeID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.IDENTIFIER_TYPE__SCHEME_ID, oldSchemeID, schemeID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemeName() {
		return schemeName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemeName(String newSchemeName) {
		String oldSchemeName = schemeName;
		schemeName = newSchemeName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.IDENTIFIER_TYPE__SCHEME_NAME, oldSchemeName, schemeName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemeURI() {
		return schemeURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemeURI(String newSchemeURI) {
		String oldSchemeURI = schemeURI;
		schemeURI = newSchemeURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.IDENTIFIER_TYPE__SCHEME_URI, oldSchemeURI, schemeURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemeVersionID() {
		return schemeVersionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemeVersionID(String newSchemeVersionID) {
		String oldSchemeVersionID = schemeVersionID;
		schemeVersionID = newSchemeVersionID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.IDENTIFIER_TYPE__SCHEME_VERSION_ID, oldSchemeVersionID, schemeVersionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.IDENTIFIER_TYPE__VALUE:
				return getValue();
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_AGENCY_ID:
				return getSchemeAgencyID();
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_AGENCY_NAME:
				return getSchemeAgencyName();
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_DATA_URI:
				return getSchemeDataURI();
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_ID:
				return getSchemeID();
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_NAME:
				return getSchemeName();
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_URI:
				return getSchemeURI();
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_VERSION_ID:
				return getSchemeVersionID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.IDENTIFIER_TYPE__VALUE:
				setValue((String)newValue);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_AGENCY_ID:
				setSchemeAgencyID((String)newValue);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_AGENCY_NAME:
				setSchemeAgencyName((String)newValue);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_DATA_URI:
				setSchemeDataURI((String)newValue);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_ID:
				setSchemeID((String)newValue);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_NAME:
				setSchemeName((String)newValue);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_URI:
				setSchemeURI((String)newValue);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_VERSION_ID:
				setSchemeVersionID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.IDENTIFIER_TYPE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_AGENCY_ID:
				setSchemeAgencyID(SCHEME_AGENCY_ID_EDEFAULT);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_AGENCY_NAME:
				setSchemeAgencyName(SCHEME_AGENCY_NAME_EDEFAULT);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_DATA_URI:
				setSchemeDataURI(SCHEME_DATA_URI_EDEFAULT);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_ID:
				setSchemeID(SCHEME_ID_EDEFAULT);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_NAME:
				setSchemeName(SCHEME_NAME_EDEFAULT);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_URI:
				setSchemeURI(SCHEME_URI_EDEFAULT);
				return;
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_VERSION_ID:
				setSchemeVersionID(SCHEME_VERSION_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.IDENTIFIER_TYPE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_AGENCY_ID:
				return SCHEME_AGENCY_ID_EDEFAULT == null ? schemeAgencyID != null : !SCHEME_AGENCY_ID_EDEFAULT.equals(schemeAgencyID);
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_AGENCY_NAME:
				return SCHEME_AGENCY_NAME_EDEFAULT == null ? schemeAgencyName != null : !SCHEME_AGENCY_NAME_EDEFAULT.equals(schemeAgencyName);
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_DATA_URI:
				return SCHEME_DATA_URI_EDEFAULT == null ? schemeDataURI != null : !SCHEME_DATA_URI_EDEFAULT.equals(schemeDataURI);
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_ID:
				return SCHEME_ID_EDEFAULT == null ? schemeID != null : !SCHEME_ID_EDEFAULT.equals(schemeID);
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_NAME:
				return SCHEME_NAME_EDEFAULT == null ? schemeName != null : !SCHEME_NAME_EDEFAULT.equals(schemeName);
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_URI:
				return SCHEME_URI_EDEFAULT == null ? schemeURI != null : !SCHEME_URI_EDEFAULT.equals(schemeURI);
			case B2MMLPackage.IDENTIFIER_TYPE__SCHEME_VERSION_ID:
				return SCHEME_VERSION_ID_EDEFAULT == null ? schemeVersionID != null : !SCHEME_VERSION_ID_EDEFAULT.equals(schemeVersionID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", schemeAgencyID: ");
		result.append(schemeAgencyID);
		result.append(", schemeAgencyName: ");
		result.append(schemeAgencyName);
		result.append(", schemeDataURI: ");
		result.append(schemeDataURI);
		result.append(", schemeID: ");
		result.append(schemeID);
		result.append(", schemeName: ");
		result.append(schemeName);
		result.append(", schemeURI: ");
		result.append(schemeURI);
		result.append(", schemeVersionID: ");
		result.append(schemeVersionID);
		result.append(')');
		return result.toString();
	}

} //IdentifierTypeImpl
