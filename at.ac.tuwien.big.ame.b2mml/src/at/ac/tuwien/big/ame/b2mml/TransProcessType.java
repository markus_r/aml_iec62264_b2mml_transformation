/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Process Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransProcessType#getActionCriteria <em>Action Criteria</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransProcessType#getAcknowledgeCode <em>Acknowledge Code</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransProcessType()
 * @model extendedMetaData="name='TransProcessType' kind='elementOnly'"
 * @generated
 */
public interface TransProcessType extends EObject {
	/**
	 * Returns the value of the '<em><b>Action Criteria</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.TransActionCriteriaType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action Criteria</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Criteria</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransProcessType_ActionCriteria()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ActionCriteria' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TransActionCriteriaType> getActionCriteria();

	/**
	 * Returns the value of the '<em><b>Acknowledge Code</b></em>' attribute.
	 * The literals are from the enumeration {@link at.ac.tuwien.big.ame.b2mml.TransResponseCodeType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Code</em>' attribute.
	 * @see at.ac.tuwien.big.ame.b2mml.TransResponseCodeType
	 * @see #isSetAcknowledgeCode()
	 * @see #unsetAcknowledgeCode()
	 * @see #setAcknowledgeCode(TransResponseCodeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransProcessType_AcknowledgeCode()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='acknowledgeCode'"
	 * @generated
	 */
	TransResponseCodeType getAcknowledgeCode();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransProcessType#getAcknowledgeCode <em>Acknowledge Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Code</em>' attribute.
	 * @see at.ac.tuwien.big.ame.b2mml.TransResponseCodeType
	 * @see #isSetAcknowledgeCode()
	 * @see #unsetAcknowledgeCode()
	 * @see #getAcknowledgeCode()
	 * @generated
	 */
	void setAcknowledgeCode(TransResponseCodeType value);

	/**
	 * Unsets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransProcessType#getAcknowledgeCode <em>Acknowledge Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAcknowledgeCode()
	 * @see #getAcknowledgeCode()
	 * @see #setAcknowledgeCode(TransResponseCodeType)
	 * @generated
	 */
	void unsetAcknowledgeCode();

	/**
	 * Returns whether the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransProcessType#getAcknowledgeCode <em>Acknowledge Code</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Acknowledge Code</em>' attribute is set.
	 * @see #unsetAcknowledgeCode()
	 * @see #getAcknowledgeCode()
	 * @see #setAcknowledgeCode(TransResponseCodeType)
	 * @generated
	 */
	boolean isSetAcknowledgeCode();

} // TransProcessType
