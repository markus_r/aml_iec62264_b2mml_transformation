/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.JobOrderCommandRuleType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Job Order Command Rule Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class JobOrderCommandRuleTypeImpl extends TextTypeImpl implements JobOrderCommandRuleType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JobOrderCommandRuleTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getJobOrderCommandRuleType();
	}

} //JobOrderCommandRuleTypeImpl
