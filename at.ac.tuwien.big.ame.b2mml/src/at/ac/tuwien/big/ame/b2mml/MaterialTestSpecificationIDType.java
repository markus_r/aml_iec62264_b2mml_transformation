/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Test Specification ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialTestSpecificationIDType()
 * @model extendedMetaData="name='MaterialTestSpecificationIDType' kind='simple'"
 * @generated
 */
public interface MaterialTestSpecificationIDType extends IdentifierType {
} // MaterialTestSpecificationIDType
