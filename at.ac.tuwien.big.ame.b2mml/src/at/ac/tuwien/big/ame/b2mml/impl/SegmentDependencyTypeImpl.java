/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DependencyType;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.ProcessSegmentIDType;
import at.ac.tuwien.big.ame.b2mml.ProductSegmentIDType;
import at.ac.tuwien.big.ame.b2mml.SegmentDependencyType;
import at.ac.tuwien.big.ame.b2mml.ValueType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Segment Dependency Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.SegmentDependencyTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.SegmentDependencyTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.SegmentDependencyTypeImpl#getDependency <em>Dependency</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.SegmentDependencyTypeImpl#getTimingFactor <em>Timing Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.SegmentDependencyTypeImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.SegmentDependencyTypeImpl#getProductSegmentID <em>Product Segment ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.SegmentDependencyTypeImpl#getProcessSegmentID <em>Process Segment ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.SegmentDependencyTypeImpl#getSegmentID <em>Segment ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SegmentDependencyTypeImpl extends MinimalEObjectImpl.Container implements SegmentDependencyType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getDependency() <em>Dependency</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependency()
	 * @generated
	 * @ordered
	 */
	protected DependencyType dependency;

	/**
	 * The cached value of the '{@link #getTimingFactor() <em>Timing Factor</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimingFactor()
	 * @generated
	 * @ordered
	 */
	protected EList<ValueType> timingFactor;

	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SegmentDependencyTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getSegmentDependencyType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DependencyType getDependency() {
		return dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDependency(DependencyType newDependency, NotificationChain msgs) {
		DependencyType oldDependency = dependency;
		dependency = newDependency;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DEPENDENCY, oldDependency, newDependency);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependency(DependencyType newDependency) {
		if (newDependency != dependency) {
			NotificationChain msgs = null;
			if (dependency != null)
				msgs = ((InternalEObject)dependency).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DEPENDENCY, null, msgs);
			if (newDependency != null)
				msgs = ((InternalEObject)newDependency).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DEPENDENCY, null, msgs);
			msgs = basicSetDependency(newDependency, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DEPENDENCY, newDependency, newDependency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueType> getTimingFactor() {
		if (timingFactor == null) {
			timingFactor = new EObjectContainmentEList<ValueType>(ValueType.class, this, B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__TIMING_FACTOR);
		}
		return timingFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProductSegmentIDType> getProductSegmentID() {
		return getGroup().list(B2MMLPackage.eINSTANCE.getSegmentDependencyType_ProductSegmentID());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessSegmentIDType> getProcessSegmentID() {
		return getGroup().list(B2MMLPackage.eINSTANCE.getSegmentDependencyType_ProcessSegmentID());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IdentifierType> getSegmentID() {
		return getGroup().list(B2MMLPackage.eINSTANCE.getSegmentDependencyType_SegmentID());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DEPENDENCY:
				return basicSetDependency(null, msgs);
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__TIMING_FACTOR:
				return ((InternalEList<?>)getTimingFactor()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__PRODUCT_SEGMENT_ID:
				return ((InternalEList<?>)getProductSegmentID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__PROCESS_SEGMENT_ID:
				return ((InternalEList<?>)getProcessSegmentID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__SEGMENT_ID:
				return ((InternalEList<?>)getSegmentID()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__ID:
				return getID();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DEPENDENCY:
				return getDependency();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__TIMING_FACTOR:
				return getTimingFactor();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__PRODUCT_SEGMENT_ID:
				return getProductSegmentID();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__PROCESS_SEGMENT_ID:
				return getProcessSegmentID();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__SEGMENT_ID:
				return getSegmentID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DEPENDENCY:
				setDependency((DependencyType)newValue);
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__TIMING_FACTOR:
				getTimingFactor().clear();
				getTimingFactor().addAll((Collection<? extends ValueType>)newValue);
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__PRODUCT_SEGMENT_ID:
				getProductSegmentID().clear();
				getProductSegmentID().addAll((Collection<? extends ProductSegmentIDType>)newValue);
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__PROCESS_SEGMENT_ID:
				getProcessSegmentID().clear();
				getProcessSegmentID().addAll((Collection<? extends ProcessSegmentIDType>)newValue);
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__SEGMENT_ID:
				getSegmentID().clear();
				getSegmentID().addAll((Collection<? extends IdentifierType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DEPENDENCY:
				setDependency((DependencyType)null);
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__TIMING_FACTOR:
				getTimingFactor().clear();
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__GROUP:
				getGroup().clear();
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__PRODUCT_SEGMENT_ID:
				getProductSegmentID().clear();
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__PROCESS_SEGMENT_ID:
				getProcessSegmentID().clear();
				return;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__SEGMENT_ID:
				getSegmentID().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__ID:
				return iD != null;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__DEPENDENCY:
				return dependency != null;
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__TIMING_FACTOR:
				return timingFactor != null && !timingFactor.isEmpty();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__GROUP:
				return group != null && !group.isEmpty();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__PRODUCT_SEGMENT_ID:
				return !getProductSegmentID().isEmpty();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__PROCESS_SEGMENT_ID:
				return !getProcessSegmentID().isEmpty();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE__SEGMENT_ID:
				return !getSegmentID().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //SegmentDependencyTypeImpl
