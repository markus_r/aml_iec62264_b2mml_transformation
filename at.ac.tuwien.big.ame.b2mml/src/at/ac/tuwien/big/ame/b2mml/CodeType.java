/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Code Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.CodeType#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.CodeType#getLanguageID <em>Language ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListAgencyID <em>List Agency ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListAgencyName <em>List Agency Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListID <em>List ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListName <em>List Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListSchemeURI <em>List Scheme URI</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListURI <em>List URI</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListVersionID <em>List Version ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.CodeType#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCodeType()
 * @model extendedMetaData="name='CodeType' kind='simple'"
 * @generated
 */
public interface CodeType extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCodeType_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.CodeType#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Language ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Language ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language ID</em>' attribute.
	 * @see #setLanguageID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCodeType_LanguageID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Language"
	 *        extendedMetaData="kind='attribute' name='languageID'"
	 * @generated
	 */
	String getLanguageID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.CodeType#getLanguageID <em>Language ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language ID</em>' attribute.
	 * @see #getLanguageID()
	 * @generated
	 */
	void setLanguageID(String value);

	/**
	 * Returns the value of the '<em><b>List Agency ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Agency ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Agency ID</em>' attribute.
	 * @see #setListAgencyID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCodeType_ListAgencyID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='listAgencyID'"
	 * @generated
	 */
	String getListAgencyID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListAgencyID <em>List Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Agency ID</em>' attribute.
	 * @see #getListAgencyID()
	 * @generated
	 */
	void setListAgencyID(String value);

	/**
	 * Returns the value of the '<em><b>List Agency Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Agency Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Agency Name</em>' attribute.
	 * @see #setListAgencyName(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCodeType_ListAgencyName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='listAgencyName'"
	 * @generated
	 */
	String getListAgencyName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListAgencyName <em>List Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Agency Name</em>' attribute.
	 * @see #getListAgencyName()
	 * @generated
	 */
	void setListAgencyName(String value);

	/**
	 * Returns the value of the '<em><b>List ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List ID</em>' attribute.
	 * @see #setListID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCodeType_ListID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='listID'"
	 * @generated
	 */
	String getListID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListID <em>List ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List ID</em>' attribute.
	 * @see #getListID()
	 * @generated
	 */
	void setListID(String value);

	/**
	 * Returns the value of the '<em><b>List Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Name</em>' attribute.
	 * @see #setListName(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCodeType_ListName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='listName'"
	 * @generated
	 */
	String getListName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListName <em>List Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Name</em>' attribute.
	 * @see #getListName()
	 * @generated
	 */
	void setListName(String value);

	/**
	 * Returns the value of the '<em><b>List Scheme URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Scheme URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Scheme URI</em>' attribute.
	 * @see #setListSchemeURI(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCodeType_ListSchemeURI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='listSchemeURI'"
	 * @generated
	 */
	String getListSchemeURI();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListSchemeURI <em>List Scheme URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Scheme URI</em>' attribute.
	 * @see #getListSchemeURI()
	 * @generated
	 */
	void setListSchemeURI(String value);

	/**
	 * Returns the value of the '<em><b>List URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List URI</em>' attribute.
	 * @see #setListURI(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCodeType_ListURI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='listURI'"
	 * @generated
	 */
	String getListURI();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListURI <em>List URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List URI</em>' attribute.
	 * @see #getListURI()
	 * @generated
	 */
	void setListURI(String value);

	/**
	 * Returns the value of the '<em><b>List Version ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Version ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Version ID</em>' attribute.
	 * @see #setListVersionID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCodeType_ListVersionID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='listVersionID'"
	 * @generated
	 */
	String getListVersionID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.CodeType#getListVersionID <em>List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Version ID</em>' attribute.
	 * @see #getListVersionID()
	 * @generated
	 */
	void setListVersionID(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCodeType_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.CodeType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // CodeType
