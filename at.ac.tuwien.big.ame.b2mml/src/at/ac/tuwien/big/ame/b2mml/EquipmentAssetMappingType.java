/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Asset Mapping Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.EquipmentAssetMappingType#getEquipmentID <em>Equipment ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.EquipmentAssetMappingType#getPhysicalAssetID <em>Physical Asset ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.EquipmentAssetMappingType#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.EquipmentAssetMappingType#getEndTime <em>End Time</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentAssetMappingType()
 * @model extendedMetaData="name='EquipmentAssetMappingType' kind='elementOnly'"
 * @generated
 */
public interface EquipmentAssetMappingType extends EObject {
	/**
	 * Returns the value of the '<em><b>Equipment ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment ID</em>' containment reference.
	 * @see #setEquipmentID(EquipmentIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentAssetMappingType_EquipmentID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='EquipmentID' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentIDType getEquipmentID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.EquipmentAssetMappingType#getEquipmentID <em>Equipment ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment ID</em>' containment reference.
	 * @see #getEquipmentID()
	 * @generated
	 */
	void setEquipmentID(EquipmentIDType value);

	/**
	 * Returns the value of the '<em><b>Physical Asset ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset ID</em>' containment reference.
	 * @see #setPhysicalAssetID(PhysicalAssetIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentAssetMappingType_PhysicalAssetID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetID' namespace='##targetNamespace'"
	 * @generated
	 */
	PhysicalAssetIDType getPhysicalAssetID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.EquipmentAssetMappingType#getPhysicalAssetID <em>Physical Asset ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset ID</em>' containment reference.
	 * @see #getPhysicalAssetID()
	 * @generated
	 */
	void setPhysicalAssetID(PhysicalAssetIDType value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' containment reference.
	 * @see #setStartTime(DateTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentAssetMappingType_StartTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='StartTime' namespace='##targetNamespace'"
	 * @generated
	 */
	DateTimeType getStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.EquipmentAssetMappingType#getStartTime <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' containment reference.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(DateTimeType value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' containment reference.
	 * @see #setEndTime(DateTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentAssetMappingType_EndTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EndTime' namespace='##targetNamespace'"
	 * @generated
	 */
	DateTimeType getEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.EquipmentAssetMappingType#getEndTime <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' containment reference.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(DateTimeType value);

} // EquipmentAssetMappingType
