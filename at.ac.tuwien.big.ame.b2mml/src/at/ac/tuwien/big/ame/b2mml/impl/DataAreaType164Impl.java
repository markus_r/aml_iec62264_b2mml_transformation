/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.BODType;
import at.ac.tuwien.big.ame.b2mml.DataAreaType164;
import at.ac.tuwien.big.ame.b2mml.TransConfirmType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type164</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType164Impl#getConfirm <em>Confirm</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType164Impl#getBOD <em>BOD</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType164Impl extends MinimalEObjectImpl.Container implements DataAreaType164 {
	/**
	 * The cached value of the '{@link #getConfirm() <em>Confirm</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfirm()
	 * @generated
	 * @ordered
	 */
	protected TransConfirmType confirm;

	/**
	 * The cached value of the '{@link #getBOD() <em>BOD</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBOD()
	 * @generated
	 * @ordered
	 */
	protected EList<BODType> bOD;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType164Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType164();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransConfirmType getConfirm() {
		return confirm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConfirm(TransConfirmType newConfirm, NotificationChain msgs) {
		TransConfirmType oldConfirm = confirm;
		confirm = newConfirm;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE164__CONFIRM, oldConfirm, newConfirm);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfirm(TransConfirmType newConfirm) {
		if (newConfirm != confirm) {
			NotificationChain msgs = null;
			if (confirm != null)
				msgs = ((InternalEObject)confirm).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE164__CONFIRM, null, msgs);
			if (newConfirm != null)
				msgs = ((InternalEObject)newConfirm).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE164__CONFIRM, null, msgs);
			msgs = basicSetConfirm(newConfirm, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE164__CONFIRM, newConfirm, newConfirm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BODType> getBOD() {
		if (bOD == null) {
			bOD = new EObjectContainmentEList<BODType>(BODType.class, this, B2MMLPackage.DATA_AREA_TYPE164__BOD);
		}
		return bOD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE164__CONFIRM:
				return basicSetConfirm(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE164__BOD:
				return ((InternalEList<?>)getBOD()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE164__CONFIRM:
				return getConfirm();
			case B2MMLPackage.DATA_AREA_TYPE164__BOD:
				return getBOD();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE164__CONFIRM:
				setConfirm((TransConfirmType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE164__BOD:
				getBOD().clear();
				getBOD().addAll((Collection<? extends BODType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE164__CONFIRM:
				setConfirm((TransConfirmType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE164__BOD:
				getBOD().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE164__CONFIRM:
				return confirm != null;
			case B2MMLPackage.DATA_AREA_TYPE164__BOD:
				return bOD != null && !bOD.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType164Impl
