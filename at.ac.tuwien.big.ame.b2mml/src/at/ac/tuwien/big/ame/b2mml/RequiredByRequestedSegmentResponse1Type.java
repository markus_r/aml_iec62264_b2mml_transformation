/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Required By Requested Segment Response1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getRequiredByRequestedSegmentResponse1Type()
 * @model extendedMetaData="name='RequiredByRequestedSegmentResponse1Type' kind='simple'"
 * @generated
 */
public interface RequiredByRequestedSegmentResponse1Type extends CodeType {
} // RequiredByRequestedSegmentResponse1Type
