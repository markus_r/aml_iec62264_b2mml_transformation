/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Expression Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransExpressionType#getActionCode <em>Action Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransExpressionType#getExpressionLanguage <em>Expression Language</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransExpressionType()
 * @model extendedMetaData="name='TransExpressionType' kind='simple'"
 * @generated
 */
public interface TransExpressionType extends TransExpression1Type {
	/**
	 * Returns the value of the '<em><b>Action Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Code</em>' attribute.
	 * @see #setActionCode(Object)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransExpressionType_ActionCode()
	 * @model dataType="at.ac.tuwien.big.ame.b2mml.TransActionCodeType" required="true"
	 *        extendedMetaData="kind='attribute' name='actionCode'"
	 * @generated
	 */
	Object getActionCode();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransExpressionType#getActionCode <em>Action Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action Code</em>' attribute.
	 * @see #getActionCode()
	 * @generated
	 */
	void setActionCode(Object value);

	/**
	 * Returns the value of the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression Language</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression Language</em>' attribute.
	 * @see #setExpressionLanguage(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransExpressionType_ExpressionLanguage()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Token"
	 *        extendedMetaData="kind='attribute' name='expressionLanguage'"
	 * @generated
	 */
	String getExpressionLanguage();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransExpressionType#getExpressionLanguage <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression Language</em>' attribute.
	 * @see #getExpressionLanguage()
	 * @generated
	 */
	void setExpressionLanguage(String value);

} // TransExpressionType
