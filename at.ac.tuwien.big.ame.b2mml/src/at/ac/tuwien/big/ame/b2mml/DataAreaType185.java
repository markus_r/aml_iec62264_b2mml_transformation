/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type185</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType185#getCancel <em>Cancel</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType185#getOperationsDefinition <em>Operations Definition</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType185()
 * @model extendedMetaData="name='DataArea_._186_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType185 extends EObject {
	/**
	 * Returns the value of the '<em><b>Cancel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel</em>' containment reference.
	 * @see #setCancel(TransCancelType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType185_Cancel()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Cancel' namespace='##targetNamespace'"
	 * @generated
	 */
	TransCancelType getCancel();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType185#getCancel <em>Cancel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel</em>' containment reference.
	 * @see #getCancel()
	 * @generated
	 */
	void setCancel(TransCancelType value);

	/**
	 * Returns the value of the '<em><b>Operations Definition</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OperationsDefinitionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType185_OperationsDefinition()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OperationsDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsDefinitionType> getOperationsDefinition();

} // DataAreaType185
