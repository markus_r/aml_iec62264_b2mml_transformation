/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tested Equipment Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestedEquipmentPropertyType#getEquipmentID <em>Equipment ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestedEquipmentPropertyType#getPropertyID <em>Property ID</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedEquipmentPropertyType()
 * @model extendedMetaData="name='TestedEquipmentPropertyType' kind='elementOnly'"
 * @generated
 */
public interface TestedEquipmentPropertyType extends EObject {
	/**
	 * Returns the value of the '<em><b>Equipment ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment ID</em>' containment reference.
	 * @see #setEquipmentID(EquipmentIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedEquipmentPropertyType_EquipmentID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='EquipmentID' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentIDType getEquipmentID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestedEquipmentPropertyType#getEquipmentID <em>Equipment ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment ID</em>' containment reference.
	 * @see #getEquipmentID()
	 * @generated
	 */
	void setEquipmentID(EquipmentIDType value);

	/**
	 * Returns the value of the '<em><b>Property ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property ID</em>' containment reference.
	 * @see #setPropertyID(PropertyIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedEquipmentPropertyType_PropertyID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PropertyID' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyIDType getPropertyID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestedEquipmentPropertyType#getPropertyID <em>Property ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property ID</em>' containment reference.
	 * @see #getPropertyID()
	 * @generated
	 */
	void setPropertyID(PropertyIDType value);

} // TestedEquipmentPropertyType
