/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Capability Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCapabilityType1Type()
 * @model extendedMetaData="name='CapabilityType1Type' kind='simple'"
 * @generated
 */
public interface CapabilityType1Type extends CodeType {
} // CapabilityType1Type
