/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.PlannedFinishTimeType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Planned Finish Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PlannedFinishTimeTypeImpl extends DateTimeTypeImpl implements PlannedFinishTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PlannedFinishTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPlannedFinishTimeType();
	}

} //PlannedFinishTimeTypeImpl
