/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DataAreaType61;
import at.ac.tuwien.big.ame.b2mml.QualificationTestSpecificationType;
import at.ac.tuwien.big.ame.b2mml.TransSyncType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type61</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType61Impl#getSync <em>Sync</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType61Impl#getQualificationTestSpecification <em>Qualification Test Specification</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType61Impl extends MinimalEObjectImpl.Container implements DataAreaType61 {
	/**
	 * The cached value of the '{@link #getSync() <em>Sync</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSync()
	 * @generated
	 * @ordered
	 */
	protected TransSyncType sync;

	/**
	 * The cached value of the '{@link #getQualificationTestSpecification() <em>Qualification Test Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualificationTestSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<QualificationTestSpecificationType> qualificationTestSpecification;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType61Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType61();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransSyncType getSync() {
		return sync;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSync(TransSyncType newSync, NotificationChain msgs) {
		TransSyncType oldSync = sync;
		sync = newSync;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE61__SYNC, oldSync, newSync);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSync(TransSyncType newSync) {
		if (newSync != sync) {
			NotificationChain msgs = null;
			if (sync != null)
				msgs = ((InternalEObject)sync).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE61__SYNC, null, msgs);
			if (newSync != null)
				msgs = ((InternalEObject)newSync).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE61__SYNC, null, msgs);
			msgs = basicSetSync(newSync, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE61__SYNC, newSync, newSync));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QualificationTestSpecificationType> getQualificationTestSpecification() {
		if (qualificationTestSpecification == null) {
			qualificationTestSpecification = new EObjectContainmentEList<QualificationTestSpecificationType>(QualificationTestSpecificationType.class, this, B2MMLPackage.DATA_AREA_TYPE61__QUALIFICATION_TEST_SPECIFICATION);
		}
		return qualificationTestSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE61__SYNC:
				return basicSetSync(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE61__QUALIFICATION_TEST_SPECIFICATION:
				return ((InternalEList<?>)getQualificationTestSpecification()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE61__SYNC:
				return getSync();
			case B2MMLPackage.DATA_AREA_TYPE61__QUALIFICATION_TEST_SPECIFICATION:
				return getQualificationTestSpecification();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE61__SYNC:
				setSync((TransSyncType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE61__QUALIFICATION_TEST_SPECIFICATION:
				getQualificationTestSpecification().clear();
				getQualificationTestSpecification().addAll((Collection<? extends QualificationTestSpecificationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE61__SYNC:
				setSync((TransSyncType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE61__QUALIFICATION_TEST_SPECIFICATION:
				getQualificationTestSpecification().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE61__SYNC:
				return sync != null;
			case B2MMLPackage.DATA_AREA_TYPE61__QUALIFICATION_TEST_SPECIFICATION:
				return qualificationTestSpecification != null && !qualificationTestSpecification.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType61Impl
