/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.MeasureType;

import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Measure Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MeasureTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MeasureTypeImpl#getUnitCode <em>Unit Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.MeasureTypeImpl#getUnitCodeListVersionID <em>Unit Code List Version ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MeasureTypeImpl extends MinimalEObjectImpl.Container implements MeasureType {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitCode() <em>Unit Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCode()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitCode() <em>Unit Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCode()
	 * @generated
	 * @ordered
	 */
	protected String unitCode = UNIT_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitCodeListVersionID() <em>Unit Code List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListVersionID()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_CODE_LIST_VERSION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitCodeListVersionID() <em>Unit Code List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListVersionID()
	 * @generated
	 * @ordered
	 */
	protected String unitCodeListVersionID = UNIT_CODE_LIST_VERSION_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MeasureTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getMeasureType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(BigDecimal newValue) {
		BigDecimal oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MEASURE_TYPE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitCode() {
		return unitCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitCode(String newUnitCode) {
		String oldUnitCode = unitCode;
		unitCode = newUnitCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MEASURE_TYPE__UNIT_CODE, oldUnitCode, unitCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitCodeListVersionID() {
		return unitCodeListVersionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitCodeListVersionID(String newUnitCodeListVersionID) {
		String oldUnitCodeListVersionID = unitCodeListVersionID;
		unitCodeListVersionID = newUnitCodeListVersionID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MEASURE_TYPE__UNIT_CODE_LIST_VERSION_ID, oldUnitCodeListVersionID, unitCodeListVersionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.MEASURE_TYPE__VALUE:
				return getValue();
			case B2MMLPackage.MEASURE_TYPE__UNIT_CODE:
				return getUnitCode();
			case B2MMLPackage.MEASURE_TYPE__UNIT_CODE_LIST_VERSION_ID:
				return getUnitCodeListVersionID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.MEASURE_TYPE__VALUE:
				setValue((BigDecimal)newValue);
				return;
			case B2MMLPackage.MEASURE_TYPE__UNIT_CODE:
				setUnitCode((String)newValue);
				return;
			case B2MMLPackage.MEASURE_TYPE__UNIT_CODE_LIST_VERSION_ID:
				setUnitCodeListVersionID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.MEASURE_TYPE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case B2MMLPackage.MEASURE_TYPE__UNIT_CODE:
				setUnitCode(UNIT_CODE_EDEFAULT);
				return;
			case B2MMLPackage.MEASURE_TYPE__UNIT_CODE_LIST_VERSION_ID:
				setUnitCodeListVersionID(UNIT_CODE_LIST_VERSION_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.MEASURE_TYPE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case B2MMLPackage.MEASURE_TYPE__UNIT_CODE:
				return UNIT_CODE_EDEFAULT == null ? unitCode != null : !UNIT_CODE_EDEFAULT.equals(unitCode);
			case B2MMLPackage.MEASURE_TYPE__UNIT_CODE_LIST_VERSION_ID:
				return UNIT_CODE_LIST_VERSION_ID_EDEFAULT == null ? unitCodeListVersionID != null : !UNIT_CODE_LIST_VERSION_ID_EDEFAULT.equals(unitCodeListVersionID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", unitCode: ");
		result.append(unitCode);
		result.append(", unitCodeListVersionID: ");
		result.append(unitCodeListVersionID);
		result.append(')');
		return result.toString();
	}

} //MeasureTypeImpl
