/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Sub Lot ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotIDType()
 * @model extendedMetaData="name='MaterialSubLotIDType' kind='simple'"
 * @generated
 */
public interface MaterialSubLotIDType extends IdentifierType {
} // MaterialSubLotIDType
