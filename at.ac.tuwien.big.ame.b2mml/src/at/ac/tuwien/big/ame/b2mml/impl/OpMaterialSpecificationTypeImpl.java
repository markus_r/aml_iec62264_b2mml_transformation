/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.AssemblyRelationshipType;
import at.ac.tuwien.big.ame.b2mml.AssemblyTypeType;
import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.MaterialClassIDType;
import at.ac.tuwien.big.ame.b2mml.MaterialDefinitionIDType;
import at.ac.tuwien.big.ame.b2mml.MaterialUseType;
import at.ac.tuwien.big.ame.b2mml.OpMaterialSpecificationPropertyType;
import at.ac.tuwien.big.ame.b2mml.OpMaterialSpecificationType;
import at.ac.tuwien.big.ame.b2mml.QuantityValueType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Material Specification Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialSpecificationTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialSpecificationTypeImpl#getMaterialClassID <em>Material Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialSpecificationTypeImpl#getMaterialDefinitionID <em>Material Definition ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialSpecificationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialSpecificationTypeImpl#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialSpecificationTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialSpecificationTypeImpl#getAssemblySpecification <em>Assembly Specification</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialSpecificationTypeImpl#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialSpecificationTypeImpl#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialSpecificationTypeImpl#getMaterialSpecificationProperty <em>Material Specification Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpMaterialSpecificationTypeImpl extends MinimalEObjectImpl.Container implements OpMaterialSpecificationType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getMaterialClassID() <em>Material Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialClassIDType> materialClassID;

	/**
	 * The cached value of the '{@link #getMaterialDefinitionID() <em>Material Definition ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinitionID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialDefinitionIDType> materialDefinitionID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getMaterialUse() <em>Material Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialUse()
	 * @generated
	 * @ordered
	 */
	protected MaterialUseType materialUse;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getAssemblySpecification() <em>Assembly Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblySpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<OpMaterialSpecificationType> assemblySpecification;

	/**
	 * The cached value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected AssemblyTypeType assemblyType;

	/**
	 * The cached value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected AssemblyRelationshipType assemblyRelationship;

	/**
	 * The cached value of the '{@link #getMaterialSpecificationProperty() <em>Material Specification Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSpecificationProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<OpMaterialSpecificationPropertyType> materialSpecificationProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpMaterialSpecificationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpMaterialSpecificationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialClassIDType> getMaterialClassID() {
		if (materialClassID == null) {
			materialClassID = new EObjectContainmentEList<MaterialClassIDType>(MaterialClassIDType.class, this, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_CLASS_ID);
		}
		return materialClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialDefinitionIDType> getMaterialDefinitionID() {
		if (materialDefinitionID == null) {
			materialDefinitionID = new EObjectContainmentEList<MaterialDefinitionIDType>(MaterialDefinitionIDType.class, this, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID);
		}
		return materialDefinitionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialUseType getMaterialUse() {
		return materialUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialUse(MaterialUseType newMaterialUse, NotificationChain msgs) {
		MaterialUseType oldMaterialUse = materialUse;
		materialUse = newMaterialUse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_USE, oldMaterialUse, newMaterialUse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialUse(MaterialUseType newMaterialUse) {
		if (newMaterialUse != materialUse) {
			NotificationChain msgs = null;
			if (materialUse != null)
				msgs = ((InternalEObject)materialUse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_USE, null, msgs);
			if (newMaterialUse != null)
				msgs = ((InternalEObject)newMaterialUse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_USE, null, msgs);
			msgs = basicSetMaterialUse(newMaterialUse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_USE, newMaterialUse, newMaterialUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpMaterialSpecificationType> getAssemblySpecification() {
		if (assemblySpecification == null) {
			assemblySpecification = new EObjectContainmentEList<OpMaterialSpecificationType>(OpMaterialSpecificationType.class, this, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_SPECIFICATION);
		}
		return assemblySpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyTypeType getAssemblyType() {
		return assemblyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyType(AssemblyTypeType newAssemblyType, NotificationChain msgs) {
		AssemblyTypeType oldAssemblyType = assemblyType;
		assemblyType = newAssemblyType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_TYPE, oldAssemblyType, newAssemblyType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyType(AssemblyTypeType newAssemblyType) {
		if (newAssemblyType != assemblyType) {
			NotificationChain msgs = null;
			if (assemblyType != null)
				msgs = ((InternalEObject)assemblyType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_TYPE, null, msgs);
			if (newAssemblyType != null)
				msgs = ((InternalEObject)newAssemblyType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_TYPE, null, msgs);
			msgs = basicSetAssemblyType(newAssemblyType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_TYPE, newAssemblyType, newAssemblyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationshipType getAssemblyRelationship() {
		return assemblyRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship, NotificationChain msgs) {
		AssemblyRelationshipType oldAssemblyRelationship = assemblyRelationship;
		assemblyRelationship = newAssemblyRelationship;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP, oldAssemblyRelationship, newAssemblyRelationship);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship) {
		if (newAssemblyRelationship != assemblyRelationship) {
			NotificationChain msgs = null;
			if (assemblyRelationship != null)
				msgs = ((InternalEObject)assemblyRelationship).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			if (newAssemblyRelationship != null)
				msgs = ((InternalEObject)newAssemblyRelationship).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			msgs = basicSetAssemblyRelationship(newAssemblyRelationship, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP, newAssemblyRelationship, newAssemblyRelationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpMaterialSpecificationPropertyType> getMaterialSpecificationProperty() {
		if (materialSpecificationProperty == null) {
			materialSpecificationProperty = new EObjectContainmentEList<OpMaterialSpecificationPropertyType>(OpMaterialSpecificationPropertyType.class, this, B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_SPECIFICATION_PROPERTY);
		}
		return materialSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_CLASS_ID:
				return ((InternalEList<?>)getMaterialClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID:
				return ((InternalEList<?>)getMaterialDefinitionID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_USE:
				return basicSetMaterialUse(null, msgs);
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_SPECIFICATION:
				return ((InternalEList<?>)getAssemblySpecification()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_TYPE:
				return basicSetAssemblyType(null, msgs);
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP:
				return basicSetAssemblyRelationship(null, msgs);
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_SPECIFICATION_PROPERTY:
				return ((InternalEList<?>)getMaterialSpecificationProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ID:
				return getID();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_CLASS_ID:
				return getMaterialClassID();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID:
				return getMaterialDefinitionID();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_USE:
				return getMaterialUse();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_SPECIFICATION:
				return getAssemblySpecification();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_TYPE:
				return getAssemblyType();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP:
				return getAssemblyRelationship();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_SPECIFICATION_PROPERTY:
				return getMaterialSpecificationProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_CLASS_ID:
				getMaterialClassID().clear();
				getMaterialClassID().addAll((Collection<? extends MaterialClassIDType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID:
				getMaterialDefinitionID().clear();
				getMaterialDefinitionID().addAll((Collection<? extends MaterialDefinitionIDType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_USE:
				setMaterialUse((MaterialUseType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_SPECIFICATION:
				getAssemblySpecification().clear();
				getAssemblySpecification().addAll((Collection<? extends OpMaterialSpecificationType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_SPECIFICATION_PROPERTY:
				getMaterialSpecificationProperty().clear();
				getMaterialSpecificationProperty().addAll((Collection<? extends OpMaterialSpecificationPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_CLASS_ID:
				getMaterialClassID().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID:
				getMaterialDefinitionID().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_USE:
				setMaterialUse((MaterialUseType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_SPECIFICATION:
				getAssemblySpecification().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_SPECIFICATION_PROPERTY:
				getMaterialSpecificationProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ID:
				return iD != null;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_CLASS_ID:
				return materialClassID != null && !materialClassID.isEmpty();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID:
				return materialDefinitionID != null && !materialDefinitionID.isEmpty();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_USE:
				return materialUse != null;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_SPECIFICATION:
				return assemblySpecification != null && !assemblySpecification.isEmpty();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_TYPE:
				return assemblyType != null;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP:
				return assemblyRelationship != null;
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE__MATERIAL_SPECIFICATION_PROPERTY:
				return materialSpecificationProperty != null && !materialSpecificationProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OpMaterialSpecificationTypeImpl
