/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type197</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType197#getCancel <em>Cancel</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType197#getMaterialClass <em>Material Class</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType197()
 * @model extendedMetaData="name='DataArea_._198_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType197 extends EObject {
	/**
	 * Returns the value of the '<em><b>Cancel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel</em>' containment reference.
	 * @see #setCancel(TransCancelType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType197_Cancel()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Cancel' namespace='##targetNamespace'"
	 * @generated
	 */
	TransCancelType getCancel();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType197#getCancel <em>Cancel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel</em>' containment reference.
	 * @see #getCancel()
	 * @generated
	 */
	void setCancel(TransCancelType value);

	/**
	 * Returns the value of the '<em><b>Material Class</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialClassType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Class</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Class</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType197_MaterialClass()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MaterialClass' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialClassType> getMaterialClass();

} // DataAreaType197
