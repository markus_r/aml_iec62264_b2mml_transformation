/**
 */
package at.ac.tuwien.big.ame.b2mml;

import java.math.BigDecimal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Measure Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MeasureType#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MeasureType#getUnitCode <em>Unit Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MeasureType#getUnitCodeListVersionID <em>Unit Code List Version ID</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMeasureType()
 * @model extendedMetaData="name='MeasureType' kind='simple'"
 * @generated
 */
public interface MeasureType extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(BigDecimal)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMeasureType_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	BigDecimal getValue();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MeasureType#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Unit Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Code</em>' attribute.
	 * @see #setUnitCode(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMeasureType_UnitCode()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='unitCode'"
	 * @generated
	 */
	String getUnitCode();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MeasureType#getUnitCode <em>Unit Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Code</em>' attribute.
	 * @see #getUnitCode()
	 * @generated
	 */
	void setUnitCode(String value);

	/**
	 * Returns the value of the '<em><b>Unit Code List Version ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Code List Version ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Code List Version ID</em>' attribute.
	 * @see #setUnitCodeListVersionID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMeasureType_UnitCodeListVersionID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='unitCodeListVersionID'"
	 * @generated
	 */
	String getUnitCodeListVersionID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MeasureType#getUnitCodeListVersionID <em>Unit Code List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Code List Version ID</em>' attribute.
	 * @see #getUnitCodeListVersionID()
	 * @generated
	 */
	void setUnitCodeListVersionID(String value);

} // MeasureType
