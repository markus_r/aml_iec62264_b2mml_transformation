/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.OperationsDefinitionInformationType;
import at.ac.tuwien.big.ame.b2mml.OperationsDefinitionType;
import at.ac.tuwien.big.ame.b2mml.PublishedDateType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Definition Information Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsDefinitionInformationTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsDefinitionInformationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsDefinitionInformationTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsDefinitionInformationTypeImpl#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsDefinitionInformationTypeImpl#getOperationsDefinition <em>Operations Definition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsDefinitionInformationTypeImpl extends MinimalEObjectImpl.Container implements OperationsDefinitionInformationType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getPublishedDate() <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublishedDate()
	 * @generated
	 * @ordered
	 */
	protected PublishedDateType publishedDate;

	/**
	 * The cached value of the '{@link #getOperationsDefinition() <em>Operations Definition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsDefinitionType> operationsDefinition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsDefinitionInformationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOperationsDefinitionInformationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PublishedDateType getPublishedDate() {
		return publishedDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPublishedDate(PublishedDateType newPublishedDate, NotificationChain msgs) {
		PublishedDateType oldPublishedDate = publishedDate;
		publishedDate = newPublishedDate;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__PUBLISHED_DATE, oldPublishedDate, newPublishedDate);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublishedDate(PublishedDateType newPublishedDate) {
		if (newPublishedDate != publishedDate) {
			NotificationChain msgs = null;
			if (publishedDate != null)
				msgs = ((InternalEObject)publishedDate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__PUBLISHED_DATE, null, msgs);
			if (newPublishedDate != null)
				msgs = ((InternalEObject)newPublishedDate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__PUBLISHED_DATE, null, msgs);
			msgs = basicSetPublishedDate(newPublishedDate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__PUBLISHED_DATE, newPublishedDate, newPublishedDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsDefinitionType> getOperationsDefinition() {
		if (operationsDefinition == null) {
			operationsDefinition = new EObjectContainmentEList<OperationsDefinitionType>(OperationsDefinitionType.class, this, B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__OPERATIONS_DEFINITION);
		}
		return operationsDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__PUBLISHED_DATE:
				return basicSetPublishedDate(null, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__OPERATIONS_DEFINITION:
				return ((InternalEList<?>)getOperationsDefinition()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__ID:
				return getID();
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__PUBLISHED_DATE:
				return getPublishedDate();
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__OPERATIONS_DEFINITION:
				return getOperationsDefinition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__PUBLISHED_DATE:
				setPublishedDate((PublishedDateType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__OPERATIONS_DEFINITION:
				getOperationsDefinition().clear();
				getOperationsDefinition().addAll((Collection<? extends OperationsDefinitionType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__PUBLISHED_DATE:
				setPublishedDate((PublishedDateType)null);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__OPERATIONS_DEFINITION:
				getOperationsDefinition().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__ID:
				return iD != null;
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__PUBLISHED_DATE:
				return publishedDate != null;
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE__OPERATIONS_DEFINITION:
				return operationsDefinition != null && !operationsDefinition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OperationsDefinitionInformationTypeImpl
