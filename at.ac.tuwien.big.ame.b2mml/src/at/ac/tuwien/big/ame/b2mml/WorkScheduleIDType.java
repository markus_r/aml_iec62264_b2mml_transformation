/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Work Schedule ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getWorkScheduleIDType()
 * @model extendedMetaData="name='WorkScheduleIDType' kind='simple'"
 * @generated
 */
public interface WorkScheduleIDType extends IdentifierType {
} // WorkScheduleIDType
