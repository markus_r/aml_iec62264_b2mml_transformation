/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type88</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType88#getRespond <em>Respond</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType88#getProcessSegment <em>Process Segment</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType88()
 * @model extendedMetaData="name='DataArea_._89_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType88 extends EObject {
	/**
	 * Returns the value of the '<em><b>Respond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond</em>' containment reference.
	 * @see #setRespond(TransRespondType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType88_Respond()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Respond' namespace='##targetNamespace'"
	 * @generated
	 */
	TransRespondType getRespond();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType88#getRespond <em>Respond</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond</em>' containment reference.
	 * @see #getRespond()
	 * @generated
	 */
	void setRespond(TransRespondType value);

	/**
	 * Returns the value of the '<em><b>Process Segment</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.ProcessSegmentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType88_ProcessSegment()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ProcessSegment' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ProcessSegmentType> getProcessSegment();

} // DataAreaType88
