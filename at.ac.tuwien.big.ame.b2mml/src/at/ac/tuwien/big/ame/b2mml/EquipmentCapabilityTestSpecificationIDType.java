/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Capability Test Specification ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentCapabilityTestSpecificationIDType()
 * @model extendedMetaData="name='EquipmentCapabilityTestSpecificationIDType' kind='simple'"
 * @generated
 */
public interface EquipmentCapabilityTestSpecificationIDType extends IdentifierType {
} // EquipmentCapabilityTestSpecificationIDType
