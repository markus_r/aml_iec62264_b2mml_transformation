/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Result Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestResultType#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestResultType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestResultType#getTestDateTime <em>Test Date Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestResultType#getResult <em>Result</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestResultType#getExpirationTime <em>Expiration Time</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestResultType()
 * @model extendedMetaData="name='TestResultType' kind='elementOnly'"
 * @generated
 */
public interface TestResultType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestResultType_ID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestResultType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestResultType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Test Date Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Date Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Date Time</em>' containment reference.
	 * @see #setTestDateTime(TestDateTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestResultType_TestDateTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TestDateTime' namespace='##targetNamespace'"
	 * @generated
	 */
	TestDateTimeType getTestDateTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestResultType#getTestDateTime <em>Test Date Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Date Time</em>' containment reference.
	 * @see #getTestDateTime()
	 * @generated
	 */
	void setTestDateTime(TestDateTimeType value);

	/**
	 * Returns the value of the '<em><b>Result</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.ResultType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestResultType_Result()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Result' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ResultType> getResult();

	/**
	 * Returns the value of the '<em><b>Expiration Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expiration Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expiration Time</em>' containment reference.
	 * @see #setExpirationTime(ExpirationTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestResultType_ExpirationTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ExpirationTime' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpirationTimeType getExpirationTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestResultType#getExpirationTime <em>Expiration Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expiration Time</em>' containment reference.
	 * @see #getExpirationTime()
	 * @generated
	 */
	void setExpirationTime(ExpirationTimeType value);

} // TestResultType
