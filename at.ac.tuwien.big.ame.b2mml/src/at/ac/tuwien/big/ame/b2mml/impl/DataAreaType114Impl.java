/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DataAreaType114;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetInformationType;
import at.ac.tuwien.big.ame.b2mml.TransGetType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type114</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType114Impl#getGet <em>Get</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType114Impl#getPhysicalAssetInformation <em>Physical Asset Information</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType114Impl extends MinimalEObjectImpl.Container implements DataAreaType114 {
	/**
	 * The cached value of the '{@link #getGet() <em>Get</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGet()
	 * @generated
	 * @ordered
	 */
	protected TransGetType get;

	/**
	 * The cached value of the '{@link #getPhysicalAssetInformation() <em>Physical Asset Information</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetInformation()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetInformationType> physicalAssetInformation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType114Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType114();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransGetType getGet() {
		return get;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGet(TransGetType newGet, NotificationChain msgs) {
		TransGetType oldGet = get;
		get = newGet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE114__GET, oldGet, newGet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGet(TransGetType newGet) {
		if (newGet != get) {
			NotificationChain msgs = null;
			if (get != null)
				msgs = ((InternalEObject)get).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE114__GET, null, msgs);
			if (newGet != null)
				msgs = ((InternalEObject)newGet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE114__GET, null, msgs);
			msgs = basicSetGet(newGet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE114__GET, newGet, newGet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetInformationType> getPhysicalAssetInformation() {
		if (physicalAssetInformation == null) {
			physicalAssetInformation = new EObjectContainmentEList<PhysicalAssetInformationType>(PhysicalAssetInformationType.class, this, B2MMLPackage.DATA_AREA_TYPE114__PHYSICAL_ASSET_INFORMATION);
		}
		return physicalAssetInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE114__GET:
				return basicSetGet(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE114__PHYSICAL_ASSET_INFORMATION:
				return ((InternalEList<?>)getPhysicalAssetInformation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE114__GET:
				return getGet();
			case B2MMLPackage.DATA_AREA_TYPE114__PHYSICAL_ASSET_INFORMATION:
				return getPhysicalAssetInformation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE114__GET:
				setGet((TransGetType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE114__PHYSICAL_ASSET_INFORMATION:
				getPhysicalAssetInformation().clear();
				getPhysicalAssetInformation().addAll((Collection<? extends PhysicalAssetInformationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE114__GET:
				setGet((TransGetType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE114__PHYSICAL_ASSET_INFORMATION:
				getPhysicalAssetInformation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE114__GET:
				return get != null;
			case B2MMLPackage.DATA_AREA_TYPE114__PHYSICAL_ASSET_INFORMATION:
				return physicalAssetInformation != null && !physicalAssetInformation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType114Impl
