/**
 */
package at.ac.tuwien.big.ame.b2mml.util;

import at.ac.tuwien.big.ame.b2mml.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage
 * @generated
 */
public class B2MMLAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static B2MMLPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public B2MMLAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = B2MMLPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected B2MMLSwitch<Adapter> modelSwitch =
		new B2MMLSwitch<Adapter>() {
			@Override
			public Adapter caseAcknowledgeEquipmentCapabilityTestSpecType(AcknowledgeEquipmentCapabilityTestSpecType object) {
				return createAcknowledgeEquipmentCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeEquipmentClassType(AcknowledgeEquipmentClassType object) {
				return createAcknowledgeEquipmentClassTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeEquipmentInformationType(AcknowledgeEquipmentInformationType object) {
				return createAcknowledgeEquipmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeEquipmentType(AcknowledgeEquipmentType object) {
				return createAcknowledgeEquipmentTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeMaterialClassType(AcknowledgeMaterialClassType object) {
				return createAcknowledgeMaterialClassTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeMaterialDefinitionType(AcknowledgeMaterialDefinitionType object) {
				return createAcknowledgeMaterialDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeMaterialInformationType(AcknowledgeMaterialInformationType object) {
				return createAcknowledgeMaterialInformationTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeMaterialLotType(AcknowledgeMaterialLotType object) {
				return createAcknowledgeMaterialLotTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeMaterialSubLotType(AcknowledgeMaterialSubLotType object) {
				return createAcknowledgeMaterialSubLotTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeMaterialTestSpecType(AcknowledgeMaterialTestSpecType object) {
				return createAcknowledgeMaterialTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeOperationsCapabilityInformationType(AcknowledgeOperationsCapabilityInformationType object) {
				return createAcknowledgeOperationsCapabilityInformationTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeOperationsCapabilityType(AcknowledgeOperationsCapabilityType object) {
				return createAcknowledgeOperationsCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeOperationsDefinitionInformationType(AcknowledgeOperationsDefinitionInformationType object) {
				return createAcknowledgeOperationsDefinitionInformationTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeOperationsDefinitionType(AcknowledgeOperationsDefinitionType object) {
				return createAcknowledgeOperationsDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeOperationsPerformanceType(AcknowledgeOperationsPerformanceType object) {
				return createAcknowledgeOperationsPerformanceTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeOperationsScheduleType(AcknowledgeOperationsScheduleType object) {
				return createAcknowledgeOperationsScheduleTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgePersonnelClassType(AcknowledgePersonnelClassType object) {
				return createAcknowledgePersonnelClassTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgePersonnelInformationType(AcknowledgePersonnelInformationType object) {
				return createAcknowledgePersonnelInformationTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgePersonType(AcknowledgePersonType object) {
				return createAcknowledgePersonTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgePhysicalAssetCapabilityTestSpecType(AcknowledgePhysicalAssetCapabilityTestSpecType object) {
				return createAcknowledgePhysicalAssetCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgePhysicalAssetClassType(AcknowledgePhysicalAssetClassType object) {
				return createAcknowledgePhysicalAssetClassTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgePhysicalAssetInformationType(AcknowledgePhysicalAssetInformationType object) {
				return createAcknowledgePhysicalAssetInformationTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgePhysicalAssetType(AcknowledgePhysicalAssetType object) {
				return createAcknowledgePhysicalAssetTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeProcessSegmentInformationType(AcknowledgeProcessSegmentInformationType object) {
				return createAcknowledgeProcessSegmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeProcessSegmentType(AcknowledgeProcessSegmentType object) {
				return createAcknowledgeProcessSegmentTypeAdapter();
			}
			@Override
			public Adapter caseAcknowledgeQualificationTestSpecificationType(AcknowledgeQualificationTestSpecificationType object) {
				return createAcknowledgeQualificationTestSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseActualEndTimeType(ActualEndTimeType object) {
				return createActualEndTimeTypeAdapter();
			}
			@Override
			public Adapter caseActualFinishTimeType(ActualFinishTimeType object) {
				return createActualFinishTimeTypeAdapter();
			}
			@Override
			public Adapter caseActualStartTimeType(ActualStartTimeType object) {
				return createActualStartTimeTypeAdapter();
			}
			@Override
			public Adapter caseAmountType(AmountType object) {
				return createAmountTypeAdapter();
			}
			@Override
			public Adapter caseAnyGenericValueType(AnyGenericValueType object) {
				return createAnyGenericValueTypeAdapter();
			}
			@Override
			public Adapter caseAssemblyRelationship1Type(AssemblyRelationship1Type object) {
				return createAssemblyRelationship1TypeAdapter();
			}
			@Override
			public Adapter caseAssemblyRelationshipType(AssemblyRelationshipType object) {
				return createAssemblyRelationshipTypeAdapter();
			}
			@Override
			public Adapter caseAssemblyType1Type(AssemblyType1Type object) {
				return createAssemblyType1TypeAdapter();
			}
			@Override
			public Adapter caseAssemblyTypeType(AssemblyTypeType object) {
				return createAssemblyTypeTypeAdapter();
			}
			@Override
			public Adapter caseBillOfMaterialIDType(BillOfMaterialIDType object) {
				return createBillOfMaterialIDTypeAdapter();
			}
			@Override
			public Adapter caseBillOfMaterialsIDType(BillOfMaterialsIDType object) {
				return createBillOfMaterialsIDTypeAdapter();
			}
			@Override
			public Adapter caseBillOfResourcesIDType(BillOfResourcesIDType object) {
				return createBillOfResourcesIDTypeAdapter();
			}
			@Override
			public Adapter caseBinaryObjectType(BinaryObjectType object) {
				return createBinaryObjectTypeAdapter();
			}
			@Override
			public Adapter caseBODType(BODType object) {
				return createBODTypeAdapter();
			}
			@Override
			public Adapter caseCancelEquipmentCapabilityTestSpecType(CancelEquipmentCapabilityTestSpecType object) {
				return createCancelEquipmentCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseCancelEquipmentClassType(CancelEquipmentClassType object) {
				return createCancelEquipmentClassTypeAdapter();
			}
			@Override
			public Adapter caseCancelEquipmentInformationType(CancelEquipmentInformationType object) {
				return createCancelEquipmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseCancelEquipmentType(CancelEquipmentType object) {
				return createCancelEquipmentTypeAdapter();
			}
			@Override
			public Adapter caseCancelMaterialClassType(CancelMaterialClassType object) {
				return createCancelMaterialClassTypeAdapter();
			}
			@Override
			public Adapter caseCancelMaterialDefinitionType(CancelMaterialDefinitionType object) {
				return createCancelMaterialDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseCancelMaterialInformationType(CancelMaterialInformationType object) {
				return createCancelMaterialInformationTypeAdapter();
			}
			@Override
			public Adapter caseCancelMaterialLotType(CancelMaterialLotType object) {
				return createCancelMaterialLotTypeAdapter();
			}
			@Override
			public Adapter caseCancelMaterialSubLotType(CancelMaterialSubLotType object) {
				return createCancelMaterialSubLotTypeAdapter();
			}
			@Override
			public Adapter caseCancelMaterialTestSpecType(CancelMaterialTestSpecType object) {
				return createCancelMaterialTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseCancelOperationsCapabilityInformationType(CancelOperationsCapabilityInformationType object) {
				return createCancelOperationsCapabilityInformationTypeAdapter();
			}
			@Override
			public Adapter caseCancelOperationsCapabilityType(CancelOperationsCapabilityType object) {
				return createCancelOperationsCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseCancelOperationsDefinitionInformationType(CancelOperationsDefinitionInformationType object) {
				return createCancelOperationsDefinitionInformationTypeAdapter();
			}
			@Override
			public Adapter caseCancelOperationsDefinitionType(CancelOperationsDefinitionType object) {
				return createCancelOperationsDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseCancelOperationsPerformanceType(CancelOperationsPerformanceType object) {
				return createCancelOperationsPerformanceTypeAdapter();
			}
			@Override
			public Adapter caseCancelOperationsScheduleType(CancelOperationsScheduleType object) {
				return createCancelOperationsScheduleTypeAdapter();
			}
			@Override
			public Adapter caseCancelPersonnelClassType(CancelPersonnelClassType object) {
				return createCancelPersonnelClassTypeAdapter();
			}
			@Override
			public Adapter caseCancelPersonnelInformationType(CancelPersonnelInformationType object) {
				return createCancelPersonnelInformationTypeAdapter();
			}
			@Override
			public Adapter caseCancelPersonType(CancelPersonType object) {
				return createCancelPersonTypeAdapter();
			}
			@Override
			public Adapter caseCancelPhysicalAssetCapabilityTestSpecType(CancelPhysicalAssetCapabilityTestSpecType object) {
				return createCancelPhysicalAssetCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseCancelPhysicalAssetClassType(CancelPhysicalAssetClassType object) {
				return createCancelPhysicalAssetClassTypeAdapter();
			}
			@Override
			public Adapter caseCancelPhysicalAssetInformationType(CancelPhysicalAssetInformationType object) {
				return createCancelPhysicalAssetInformationTypeAdapter();
			}
			@Override
			public Adapter caseCancelPhysicalAssetType(CancelPhysicalAssetType object) {
				return createCancelPhysicalAssetTypeAdapter();
			}
			@Override
			public Adapter caseCancelProcessSegmentInformationType(CancelProcessSegmentInformationType object) {
				return createCancelProcessSegmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseCancelProcessSegmentType(CancelProcessSegmentType object) {
				return createCancelProcessSegmentTypeAdapter();
			}
			@Override
			public Adapter caseCancelQualificationTestSpecificationType(CancelQualificationTestSpecificationType object) {
				return createCancelQualificationTestSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseCapabilityType1Type(CapabilityType1Type object) {
				return createCapabilityType1TypeAdapter();
			}
			@Override
			public Adapter caseCapabilityTypeType(CapabilityTypeType object) {
				return createCapabilityTypeTypeAdapter();
			}
			@Override
			public Adapter caseCauseType(CauseType object) {
				return createCauseTypeAdapter();
			}
			@Override
			public Adapter caseCertificateOfAnalysisReferenceType(CertificateOfAnalysisReferenceType object) {
				return createCertificateOfAnalysisReferenceTypeAdapter();
			}
			@Override
			public Adapter caseChangeEquipmentCapabilityTestSpecType(ChangeEquipmentCapabilityTestSpecType object) {
				return createChangeEquipmentCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseChangeEquipmentClassType(ChangeEquipmentClassType object) {
				return createChangeEquipmentClassTypeAdapter();
			}
			@Override
			public Adapter caseChangeEquipmentInformationType(ChangeEquipmentInformationType object) {
				return createChangeEquipmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseChangeEquipmentType(ChangeEquipmentType object) {
				return createChangeEquipmentTypeAdapter();
			}
			@Override
			public Adapter caseChangeMaterialClassType(ChangeMaterialClassType object) {
				return createChangeMaterialClassTypeAdapter();
			}
			@Override
			public Adapter caseChangeMaterialDefinitionType(ChangeMaterialDefinitionType object) {
				return createChangeMaterialDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseChangeMaterialInformationType(ChangeMaterialInformationType object) {
				return createChangeMaterialInformationTypeAdapter();
			}
			@Override
			public Adapter caseChangeMaterialLotType(ChangeMaterialLotType object) {
				return createChangeMaterialLotTypeAdapter();
			}
			@Override
			public Adapter caseChangeMaterialSubLotType(ChangeMaterialSubLotType object) {
				return createChangeMaterialSubLotTypeAdapter();
			}
			@Override
			public Adapter caseChangeMaterialTestSpecType(ChangeMaterialTestSpecType object) {
				return createChangeMaterialTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseChangeOperationsCapabilityInformationType(ChangeOperationsCapabilityInformationType object) {
				return createChangeOperationsCapabilityInformationTypeAdapter();
			}
			@Override
			public Adapter caseChangeOperationsCapabilityType(ChangeOperationsCapabilityType object) {
				return createChangeOperationsCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseChangeOperationsDefinitionInformationType(ChangeOperationsDefinitionInformationType object) {
				return createChangeOperationsDefinitionInformationTypeAdapter();
			}
			@Override
			public Adapter caseChangeOperationsDefinitionType(ChangeOperationsDefinitionType object) {
				return createChangeOperationsDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseChangeOperationsPerformanceType(ChangeOperationsPerformanceType object) {
				return createChangeOperationsPerformanceTypeAdapter();
			}
			@Override
			public Adapter caseChangeOperationsScheduleType(ChangeOperationsScheduleType object) {
				return createChangeOperationsScheduleTypeAdapter();
			}
			@Override
			public Adapter caseChangePersonnelClassType(ChangePersonnelClassType object) {
				return createChangePersonnelClassTypeAdapter();
			}
			@Override
			public Adapter caseChangePersonnelInformationType(ChangePersonnelInformationType object) {
				return createChangePersonnelInformationTypeAdapter();
			}
			@Override
			public Adapter caseChangePersonType(ChangePersonType object) {
				return createChangePersonTypeAdapter();
			}
			@Override
			public Adapter caseChangePhysicalAssetCapabilityTestSpecType(ChangePhysicalAssetCapabilityTestSpecType object) {
				return createChangePhysicalAssetCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseChangePhysicalAssetClassType(ChangePhysicalAssetClassType object) {
				return createChangePhysicalAssetClassTypeAdapter();
			}
			@Override
			public Adapter caseChangePhysicalAssetInformationType(ChangePhysicalAssetInformationType object) {
				return createChangePhysicalAssetInformationTypeAdapter();
			}
			@Override
			public Adapter caseChangePhysicalAssetType(ChangePhysicalAssetType object) {
				return createChangePhysicalAssetTypeAdapter();
			}
			@Override
			public Adapter caseChangeProcessSegmentInformationType(ChangeProcessSegmentInformationType object) {
				return createChangeProcessSegmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseChangeProcessSegmentType(ChangeProcessSegmentType object) {
				return createChangeProcessSegmentTypeAdapter();
			}
			@Override
			public Adapter caseChangeQualificationTestSpecificationType(ChangeQualificationTestSpecificationType object) {
				return createChangeQualificationTestSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseCodeType(CodeType object) {
				return createCodeTypeAdapter();
			}
			@Override
			public Adapter caseConfidenceFactorType(ConfidenceFactorType object) {
				return createConfidenceFactorTypeAdapter();
			}
			@Override
			public Adapter caseConfirmBODType(ConfirmBODType object) {
				return createConfirmBODTypeAdapter();
			}
			@Override
			public Adapter caseCorrectionType(CorrectionType object) {
				return createCorrectionTypeAdapter();
			}
			@Override
			public Adapter caseDataAreaType(DataAreaType object) {
				return createDataAreaTypeAdapter();
			}
			@Override
			public Adapter caseDataAreaType1(DataAreaType1 object) {
				return createDataAreaType1Adapter();
			}
			@Override
			public Adapter caseDataAreaType2(DataAreaType2 object) {
				return createDataAreaType2Adapter();
			}
			@Override
			public Adapter caseDataAreaType3(DataAreaType3 object) {
				return createDataAreaType3Adapter();
			}
			@Override
			public Adapter caseDataAreaType4(DataAreaType4 object) {
				return createDataAreaType4Adapter();
			}
			@Override
			public Adapter caseDataAreaType5(DataAreaType5 object) {
				return createDataAreaType5Adapter();
			}
			@Override
			public Adapter caseDataAreaType6(DataAreaType6 object) {
				return createDataAreaType6Adapter();
			}
			@Override
			public Adapter caseDataAreaType7(DataAreaType7 object) {
				return createDataAreaType7Adapter();
			}
			@Override
			public Adapter caseDataAreaType8(DataAreaType8 object) {
				return createDataAreaType8Adapter();
			}
			@Override
			public Adapter caseDataAreaType9(DataAreaType9 object) {
				return createDataAreaType9Adapter();
			}
			@Override
			public Adapter caseDataAreaType10(DataAreaType10 object) {
				return createDataAreaType10Adapter();
			}
			@Override
			public Adapter caseDataAreaType11(DataAreaType11 object) {
				return createDataAreaType11Adapter();
			}
			@Override
			public Adapter caseDataAreaType12(DataAreaType12 object) {
				return createDataAreaType12Adapter();
			}
			@Override
			public Adapter caseDataAreaType13(DataAreaType13 object) {
				return createDataAreaType13Adapter();
			}
			@Override
			public Adapter caseDataAreaType14(DataAreaType14 object) {
				return createDataAreaType14Adapter();
			}
			@Override
			public Adapter caseDataAreaType15(DataAreaType15 object) {
				return createDataAreaType15Adapter();
			}
			@Override
			public Adapter caseDataAreaType16(DataAreaType16 object) {
				return createDataAreaType16Adapter();
			}
			@Override
			public Adapter caseDataAreaType17(DataAreaType17 object) {
				return createDataAreaType17Adapter();
			}
			@Override
			public Adapter caseDataAreaType18(DataAreaType18 object) {
				return createDataAreaType18Adapter();
			}
			@Override
			public Adapter caseDataAreaType19(DataAreaType19 object) {
				return createDataAreaType19Adapter();
			}
			@Override
			public Adapter caseDataAreaType20(DataAreaType20 object) {
				return createDataAreaType20Adapter();
			}
			@Override
			public Adapter caseDataAreaType21(DataAreaType21 object) {
				return createDataAreaType21Adapter();
			}
			@Override
			public Adapter caseDataAreaType22(DataAreaType22 object) {
				return createDataAreaType22Adapter();
			}
			@Override
			public Adapter caseDataAreaType23(DataAreaType23 object) {
				return createDataAreaType23Adapter();
			}
			@Override
			public Adapter caseDataAreaType24(DataAreaType24 object) {
				return createDataAreaType24Adapter();
			}
			@Override
			public Adapter caseDataAreaType25(DataAreaType25 object) {
				return createDataAreaType25Adapter();
			}
			@Override
			public Adapter caseDataAreaType26(DataAreaType26 object) {
				return createDataAreaType26Adapter();
			}
			@Override
			public Adapter caseDataAreaType27(DataAreaType27 object) {
				return createDataAreaType27Adapter();
			}
			@Override
			public Adapter caseDataAreaType28(DataAreaType28 object) {
				return createDataAreaType28Adapter();
			}
			@Override
			public Adapter caseDataAreaType29(DataAreaType29 object) {
				return createDataAreaType29Adapter();
			}
			@Override
			public Adapter caseDataAreaType30(DataAreaType30 object) {
				return createDataAreaType30Adapter();
			}
			@Override
			public Adapter caseDataAreaType31(DataAreaType31 object) {
				return createDataAreaType31Adapter();
			}
			@Override
			public Adapter caseDataAreaType32(DataAreaType32 object) {
				return createDataAreaType32Adapter();
			}
			@Override
			public Adapter caseDataAreaType33(DataAreaType33 object) {
				return createDataAreaType33Adapter();
			}
			@Override
			public Adapter caseDataAreaType34(DataAreaType34 object) {
				return createDataAreaType34Adapter();
			}
			@Override
			public Adapter caseDataAreaType35(DataAreaType35 object) {
				return createDataAreaType35Adapter();
			}
			@Override
			public Adapter caseDataAreaType36(DataAreaType36 object) {
				return createDataAreaType36Adapter();
			}
			@Override
			public Adapter caseDataAreaType37(DataAreaType37 object) {
				return createDataAreaType37Adapter();
			}
			@Override
			public Adapter caseDataAreaType38(DataAreaType38 object) {
				return createDataAreaType38Adapter();
			}
			@Override
			public Adapter caseDataAreaType39(DataAreaType39 object) {
				return createDataAreaType39Adapter();
			}
			@Override
			public Adapter caseDataAreaType40(DataAreaType40 object) {
				return createDataAreaType40Adapter();
			}
			@Override
			public Adapter caseDataAreaType41(DataAreaType41 object) {
				return createDataAreaType41Adapter();
			}
			@Override
			public Adapter caseDataAreaType42(DataAreaType42 object) {
				return createDataAreaType42Adapter();
			}
			@Override
			public Adapter caseDataAreaType43(DataAreaType43 object) {
				return createDataAreaType43Adapter();
			}
			@Override
			public Adapter caseDataAreaType44(DataAreaType44 object) {
				return createDataAreaType44Adapter();
			}
			@Override
			public Adapter caseDataAreaType45(DataAreaType45 object) {
				return createDataAreaType45Adapter();
			}
			@Override
			public Adapter caseDataAreaType46(DataAreaType46 object) {
				return createDataAreaType46Adapter();
			}
			@Override
			public Adapter caseDataAreaType47(DataAreaType47 object) {
				return createDataAreaType47Adapter();
			}
			@Override
			public Adapter caseDataAreaType48(DataAreaType48 object) {
				return createDataAreaType48Adapter();
			}
			@Override
			public Adapter caseDataAreaType49(DataAreaType49 object) {
				return createDataAreaType49Adapter();
			}
			@Override
			public Adapter caseDataAreaType50(DataAreaType50 object) {
				return createDataAreaType50Adapter();
			}
			@Override
			public Adapter caseDataAreaType51(DataAreaType51 object) {
				return createDataAreaType51Adapter();
			}
			@Override
			public Adapter caseDataAreaType52(DataAreaType52 object) {
				return createDataAreaType52Adapter();
			}
			@Override
			public Adapter caseDataAreaType53(DataAreaType53 object) {
				return createDataAreaType53Adapter();
			}
			@Override
			public Adapter caseDataAreaType54(DataAreaType54 object) {
				return createDataAreaType54Adapter();
			}
			@Override
			public Adapter caseDataAreaType55(DataAreaType55 object) {
				return createDataAreaType55Adapter();
			}
			@Override
			public Adapter caseDataAreaType56(DataAreaType56 object) {
				return createDataAreaType56Adapter();
			}
			@Override
			public Adapter caseDataAreaType57(DataAreaType57 object) {
				return createDataAreaType57Adapter();
			}
			@Override
			public Adapter caseDataAreaType58(DataAreaType58 object) {
				return createDataAreaType58Adapter();
			}
			@Override
			public Adapter caseDataAreaType59(DataAreaType59 object) {
				return createDataAreaType59Adapter();
			}
			@Override
			public Adapter caseDataAreaType60(DataAreaType60 object) {
				return createDataAreaType60Adapter();
			}
			@Override
			public Adapter caseDataAreaType61(DataAreaType61 object) {
				return createDataAreaType61Adapter();
			}
			@Override
			public Adapter caseDataAreaType62(DataAreaType62 object) {
				return createDataAreaType62Adapter();
			}
			@Override
			public Adapter caseDataAreaType63(DataAreaType63 object) {
				return createDataAreaType63Adapter();
			}
			@Override
			public Adapter caseDataAreaType64(DataAreaType64 object) {
				return createDataAreaType64Adapter();
			}
			@Override
			public Adapter caseDataAreaType65(DataAreaType65 object) {
				return createDataAreaType65Adapter();
			}
			@Override
			public Adapter caseDataAreaType66(DataAreaType66 object) {
				return createDataAreaType66Adapter();
			}
			@Override
			public Adapter caseDataAreaType67(DataAreaType67 object) {
				return createDataAreaType67Adapter();
			}
			@Override
			public Adapter caseDataAreaType68(DataAreaType68 object) {
				return createDataAreaType68Adapter();
			}
			@Override
			public Adapter caseDataAreaType69(DataAreaType69 object) {
				return createDataAreaType69Adapter();
			}
			@Override
			public Adapter caseDataAreaType70(DataAreaType70 object) {
				return createDataAreaType70Adapter();
			}
			@Override
			public Adapter caseDataAreaType71(DataAreaType71 object) {
				return createDataAreaType71Adapter();
			}
			@Override
			public Adapter caseDataAreaType72(DataAreaType72 object) {
				return createDataAreaType72Adapter();
			}
			@Override
			public Adapter caseDataAreaType73(DataAreaType73 object) {
				return createDataAreaType73Adapter();
			}
			@Override
			public Adapter caseDataAreaType74(DataAreaType74 object) {
				return createDataAreaType74Adapter();
			}
			@Override
			public Adapter caseDataAreaType75(DataAreaType75 object) {
				return createDataAreaType75Adapter();
			}
			@Override
			public Adapter caseDataAreaType76(DataAreaType76 object) {
				return createDataAreaType76Adapter();
			}
			@Override
			public Adapter caseDataAreaType77(DataAreaType77 object) {
				return createDataAreaType77Adapter();
			}
			@Override
			public Adapter caseDataAreaType78(DataAreaType78 object) {
				return createDataAreaType78Adapter();
			}
			@Override
			public Adapter caseDataAreaType79(DataAreaType79 object) {
				return createDataAreaType79Adapter();
			}
			@Override
			public Adapter caseDataAreaType80(DataAreaType80 object) {
				return createDataAreaType80Adapter();
			}
			@Override
			public Adapter caseDataAreaType81(DataAreaType81 object) {
				return createDataAreaType81Adapter();
			}
			@Override
			public Adapter caseDataAreaType82(DataAreaType82 object) {
				return createDataAreaType82Adapter();
			}
			@Override
			public Adapter caseDataAreaType83(DataAreaType83 object) {
				return createDataAreaType83Adapter();
			}
			@Override
			public Adapter caseDataAreaType84(DataAreaType84 object) {
				return createDataAreaType84Adapter();
			}
			@Override
			public Adapter caseDataAreaType85(DataAreaType85 object) {
				return createDataAreaType85Adapter();
			}
			@Override
			public Adapter caseDataAreaType86(DataAreaType86 object) {
				return createDataAreaType86Adapter();
			}
			@Override
			public Adapter caseDataAreaType87(DataAreaType87 object) {
				return createDataAreaType87Adapter();
			}
			@Override
			public Adapter caseDataAreaType88(DataAreaType88 object) {
				return createDataAreaType88Adapter();
			}
			@Override
			public Adapter caseDataAreaType89(DataAreaType89 object) {
				return createDataAreaType89Adapter();
			}
			@Override
			public Adapter caseDataAreaType90(DataAreaType90 object) {
				return createDataAreaType90Adapter();
			}
			@Override
			public Adapter caseDataAreaType91(DataAreaType91 object) {
				return createDataAreaType91Adapter();
			}
			@Override
			public Adapter caseDataAreaType92(DataAreaType92 object) {
				return createDataAreaType92Adapter();
			}
			@Override
			public Adapter caseDataAreaType93(DataAreaType93 object) {
				return createDataAreaType93Adapter();
			}
			@Override
			public Adapter caseDataAreaType94(DataAreaType94 object) {
				return createDataAreaType94Adapter();
			}
			@Override
			public Adapter caseDataAreaType95(DataAreaType95 object) {
				return createDataAreaType95Adapter();
			}
			@Override
			public Adapter caseDataAreaType96(DataAreaType96 object) {
				return createDataAreaType96Adapter();
			}
			@Override
			public Adapter caseDataAreaType97(DataAreaType97 object) {
				return createDataAreaType97Adapter();
			}
			@Override
			public Adapter caseDataAreaType98(DataAreaType98 object) {
				return createDataAreaType98Adapter();
			}
			@Override
			public Adapter caseDataAreaType99(DataAreaType99 object) {
				return createDataAreaType99Adapter();
			}
			@Override
			public Adapter caseDataAreaType100(DataAreaType100 object) {
				return createDataAreaType100Adapter();
			}
			@Override
			public Adapter caseDataAreaType101(DataAreaType101 object) {
				return createDataAreaType101Adapter();
			}
			@Override
			public Adapter caseDataAreaType102(DataAreaType102 object) {
				return createDataAreaType102Adapter();
			}
			@Override
			public Adapter caseDataAreaType103(DataAreaType103 object) {
				return createDataAreaType103Adapter();
			}
			@Override
			public Adapter caseDataAreaType104(DataAreaType104 object) {
				return createDataAreaType104Adapter();
			}
			@Override
			public Adapter caseDataAreaType105(DataAreaType105 object) {
				return createDataAreaType105Adapter();
			}
			@Override
			public Adapter caseDataAreaType106(DataAreaType106 object) {
				return createDataAreaType106Adapter();
			}
			@Override
			public Adapter caseDataAreaType107(DataAreaType107 object) {
				return createDataAreaType107Adapter();
			}
			@Override
			public Adapter caseDataAreaType108(DataAreaType108 object) {
				return createDataAreaType108Adapter();
			}
			@Override
			public Adapter caseDataAreaType109(DataAreaType109 object) {
				return createDataAreaType109Adapter();
			}
			@Override
			public Adapter caseDataAreaType110(DataAreaType110 object) {
				return createDataAreaType110Adapter();
			}
			@Override
			public Adapter caseDataAreaType111(DataAreaType111 object) {
				return createDataAreaType111Adapter();
			}
			@Override
			public Adapter caseDataAreaType112(DataAreaType112 object) {
				return createDataAreaType112Adapter();
			}
			@Override
			public Adapter caseDataAreaType113(DataAreaType113 object) {
				return createDataAreaType113Adapter();
			}
			@Override
			public Adapter caseDataAreaType114(DataAreaType114 object) {
				return createDataAreaType114Adapter();
			}
			@Override
			public Adapter caseDataAreaType115(DataAreaType115 object) {
				return createDataAreaType115Adapter();
			}
			@Override
			public Adapter caseDataAreaType116(DataAreaType116 object) {
				return createDataAreaType116Adapter();
			}
			@Override
			public Adapter caseDataAreaType117(DataAreaType117 object) {
				return createDataAreaType117Adapter();
			}
			@Override
			public Adapter caseDataAreaType118(DataAreaType118 object) {
				return createDataAreaType118Adapter();
			}
			@Override
			public Adapter caseDataAreaType119(DataAreaType119 object) {
				return createDataAreaType119Adapter();
			}
			@Override
			public Adapter caseDataAreaType120(DataAreaType120 object) {
				return createDataAreaType120Adapter();
			}
			@Override
			public Adapter caseDataAreaType121(DataAreaType121 object) {
				return createDataAreaType121Adapter();
			}
			@Override
			public Adapter caseDataAreaType122(DataAreaType122 object) {
				return createDataAreaType122Adapter();
			}
			@Override
			public Adapter caseDataAreaType123(DataAreaType123 object) {
				return createDataAreaType123Adapter();
			}
			@Override
			public Adapter caseDataAreaType124(DataAreaType124 object) {
				return createDataAreaType124Adapter();
			}
			@Override
			public Adapter caseDataAreaType125(DataAreaType125 object) {
				return createDataAreaType125Adapter();
			}
			@Override
			public Adapter caseDataAreaType126(DataAreaType126 object) {
				return createDataAreaType126Adapter();
			}
			@Override
			public Adapter caseDataAreaType127(DataAreaType127 object) {
				return createDataAreaType127Adapter();
			}
			@Override
			public Adapter caseDataAreaType128(DataAreaType128 object) {
				return createDataAreaType128Adapter();
			}
			@Override
			public Adapter caseDataAreaType129(DataAreaType129 object) {
				return createDataAreaType129Adapter();
			}
			@Override
			public Adapter caseDataAreaType130(DataAreaType130 object) {
				return createDataAreaType130Adapter();
			}
			@Override
			public Adapter caseDataAreaType131(DataAreaType131 object) {
				return createDataAreaType131Adapter();
			}
			@Override
			public Adapter caseDataAreaType132(DataAreaType132 object) {
				return createDataAreaType132Adapter();
			}
			@Override
			public Adapter caseDataAreaType133(DataAreaType133 object) {
				return createDataAreaType133Adapter();
			}
			@Override
			public Adapter caseDataAreaType134(DataAreaType134 object) {
				return createDataAreaType134Adapter();
			}
			@Override
			public Adapter caseDataAreaType135(DataAreaType135 object) {
				return createDataAreaType135Adapter();
			}
			@Override
			public Adapter caseDataAreaType136(DataAreaType136 object) {
				return createDataAreaType136Adapter();
			}
			@Override
			public Adapter caseDataAreaType137(DataAreaType137 object) {
				return createDataAreaType137Adapter();
			}
			@Override
			public Adapter caseDataAreaType138(DataAreaType138 object) {
				return createDataAreaType138Adapter();
			}
			@Override
			public Adapter caseDataAreaType139(DataAreaType139 object) {
				return createDataAreaType139Adapter();
			}
			@Override
			public Adapter caseDataAreaType140(DataAreaType140 object) {
				return createDataAreaType140Adapter();
			}
			@Override
			public Adapter caseDataAreaType141(DataAreaType141 object) {
				return createDataAreaType141Adapter();
			}
			@Override
			public Adapter caseDataAreaType142(DataAreaType142 object) {
				return createDataAreaType142Adapter();
			}
			@Override
			public Adapter caseDataAreaType143(DataAreaType143 object) {
				return createDataAreaType143Adapter();
			}
			@Override
			public Adapter caseDataAreaType144(DataAreaType144 object) {
				return createDataAreaType144Adapter();
			}
			@Override
			public Adapter caseDataAreaType145(DataAreaType145 object) {
				return createDataAreaType145Adapter();
			}
			@Override
			public Adapter caseDataAreaType146(DataAreaType146 object) {
				return createDataAreaType146Adapter();
			}
			@Override
			public Adapter caseDataAreaType147(DataAreaType147 object) {
				return createDataAreaType147Adapter();
			}
			@Override
			public Adapter caseDataAreaType148(DataAreaType148 object) {
				return createDataAreaType148Adapter();
			}
			@Override
			public Adapter caseDataAreaType149(DataAreaType149 object) {
				return createDataAreaType149Adapter();
			}
			@Override
			public Adapter caseDataAreaType150(DataAreaType150 object) {
				return createDataAreaType150Adapter();
			}
			@Override
			public Adapter caseDataAreaType151(DataAreaType151 object) {
				return createDataAreaType151Adapter();
			}
			@Override
			public Adapter caseDataAreaType152(DataAreaType152 object) {
				return createDataAreaType152Adapter();
			}
			@Override
			public Adapter caseDataAreaType153(DataAreaType153 object) {
				return createDataAreaType153Adapter();
			}
			@Override
			public Adapter caseDataAreaType154(DataAreaType154 object) {
				return createDataAreaType154Adapter();
			}
			@Override
			public Adapter caseDataAreaType155(DataAreaType155 object) {
				return createDataAreaType155Adapter();
			}
			@Override
			public Adapter caseDataAreaType156(DataAreaType156 object) {
				return createDataAreaType156Adapter();
			}
			@Override
			public Adapter caseDataAreaType157(DataAreaType157 object) {
				return createDataAreaType157Adapter();
			}
			@Override
			public Adapter caseDataAreaType158(DataAreaType158 object) {
				return createDataAreaType158Adapter();
			}
			@Override
			public Adapter caseDataAreaType159(DataAreaType159 object) {
				return createDataAreaType159Adapter();
			}
			@Override
			public Adapter caseDataAreaType160(DataAreaType160 object) {
				return createDataAreaType160Adapter();
			}
			@Override
			public Adapter caseDataAreaType161(DataAreaType161 object) {
				return createDataAreaType161Adapter();
			}
			@Override
			public Adapter caseDataAreaType162(DataAreaType162 object) {
				return createDataAreaType162Adapter();
			}
			@Override
			public Adapter caseDataAreaType163(DataAreaType163 object) {
				return createDataAreaType163Adapter();
			}
			@Override
			public Adapter caseDataAreaType164(DataAreaType164 object) {
				return createDataAreaType164Adapter();
			}
			@Override
			public Adapter caseDataAreaType165(DataAreaType165 object) {
				return createDataAreaType165Adapter();
			}
			@Override
			public Adapter caseDataAreaType166(DataAreaType166 object) {
				return createDataAreaType166Adapter();
			}
			@Override
			public Adapter caseDataAreaType167(DataAreaType167 object) {
				return createDataAreaType167Adapter();
			}
			@Override
			public Adapter caseDataAreaType168(DataAreaType168 object) {
				return createDataAreaType168Adapter();
			}
			@Override
			public Adapter caseDataAreaType169(DataAreaType169 object) {
				return createDataAreaType169Adapter();
			}
			@Override
			public Adapter caseDataAreaType170(DataAreaType170 object) {
				return createDataAreaType170Adapter();
			}
			@Override
			public Adapter caseDataAreaType171(DataAreaType171 object) {
				return createDataAreaType171Adapter();
			}
			@Override
			public Adapter caseDataAreaType172(DataAreaType172 object) {
				return createDataAreaType172Adapter();
			}
			@Override
			public Adapter caseDataAreaType173(DataAreaType173 object) {
				return createDataAreaType173Adapter();
			}
			@Override
			public Adapter caseDataAreaType174(DataAreaType174 object) {
				return createDataAreaType174Adapter();
			}
			@Override
			public Adapter caseDataAreaType175(DataAreaType175 object) {
				return createDataAreaType175Adapter();
			}
			@Override
			public Adapter caseDataAreaType176(DataAreaType176 object) {
				return createDataAreaType176Adapter();
			}
			@Override
			public Adapter caseDataAreaType177(DataAreaType177 object) {
				return createDataAreaType177Adapter();
			}
			@Override
			public Adapter caseDataAreaType178(DataAreaType178 object) {
				return createDataAreaType178Adapter();
			}
			@Override
			public Adapter caseDataAreaType179(DataAreaType179 object) {
				return createDataAreaType179Adapter();
			}
			@Override
			public Adapter caseDataAreaType180(DataAreaType180 object) {
				return createDataAreaType180Adapter();
			}
			@Override
			public Adapter caseDataAreaType181(DataAreaType181 object) {
				return createDataAreaType181Adapter();
			}
			@Override
			public Adapter caseDataAreaType182(DataAreaType182 object) {
				return createDataAreaType182Adapter();
			}
			@Override
			public Adapter caseDataAreaType183(DataAreaType183 object) {
				return createDataAreaType183Adapter();
			}
			@Override
			public Adapter caseDataAreaType184(DataAreaType184 object) {
				return createDataAreaType184Adapter();
			}
			@Override
			public Adapter caseDataAreaType185(DataAreaType185 object) {
				return createDataAreaType185Adapter();
			}
			@Override
			public Adapter caseDataAreaType186(DataAreaType186 object) {
				return createDataAreaType186Adapter();
			}
			@Override
			public Adapter caseDataAreaType187(DataAreaType187 object) {
				return createDataAreaType187Adapter();
			}
			@Override
			public Adapter caseDataAreaType188(DataAreaType188 object) {
				return createDataAreaType188Adapter();
			}
			@Override
			public Adapter caseDataAreaType189(DataAreaType189 object) {
				return createDataAreaType189Adapter();
			}
			@Override
			public Adapter caseDataAreaType190(DataAreaType190 object) {
				return createDataAreaType190Adapter();
			}
			@Override
			public Adapter caseDataAreaType191(DataAreaType191 object) {
				return createDataAreaType191Adapter();
			}
			@Override
			public Adapter caseDataAreaType192(DataAreaType192 object) {
				return createDataAreaType192Adapter();
			}
			@Override
			public Adapter caseDataAreaType193(DataAreaType193 object) {
				return createDataAreaType193Adapter();
			}
			@Override
			public Adapter caseDataAreaType194(DataAreaType194 object) {
				return createDataAreaType194Adapter();
			}
			@Override
			public Adapter caseDataAreaType195(DataAreaType195 object) {
				return createDataAreaType195Adapter();
			}
			@Override
			public Adapter caseDataAreaType196(DataAreaType196 object) {
				return createDataAreaType196Adapter();
			}
			@Override
			public Adapter caseDataAreaType197(DataAreaType197 object) {
				return createDataAreaType197Adapter();
			}
			@Override
			public Adapter caseDataAreaType198(DataAreaType198 object) {
				return createDataAreaType198Adapter();
			}
			@Override
			public Adapter caseDataAreaType199(DataAreaType199 object) {
				return createDataAreaType199Adapter();
			}
			@Override
			public Adapter caseDataAreaType200(DataAreaType200 object) {
				return createDataAreaType200Adapter();
			}
			@Override
			public Adapter caseDataAreaType201(DataAreaType201 object) {
				return createDataAreaType201Adapter();
			}
			@Override
			public Adapter caseDataAreaType202(DataAreaType202 object) {
				return createDataAreaType202Adapter();
			}
			@Override
			public Adapter caseDataAreaType203(DataAreaType203 object) {
				return createDataAreaType203Adapter();
			}
			@Override
			public Adapter caseDataAreaType204(DataAreaType204 object) {
				return createDataAreaType204Adapter();
			}
			@Override
			public Adapter caseDataAreaType205(DataAreaType205 object) {
				return createDataAreaType205Adapter();
			}
			@Override
			public Adapter caseDataAreaType206(DataAreaType206 object) {
				return createDataAreaType206Adapter();
			}
			@Override
			public Adapter caseDataAreaType207(DataAreaType207 object) {
				return createDataAreaType207Adapter();
			}
			@Override
			public Adapter caseDataAreaType208(DataAreaType208 object) {
				return createDataAreaType208Adapter();
			}
			@Override
			public Adapter caseDataType1Type(DataType1Type object) {
				return createDataType1TypeAdapter();
			}
			@Override
			public Adapter caseDataTypeType(DataTypeType object) {
				return createDataTypeTypeAdapter();
			}
			@Override
			public Adapter caseDateTimeType(DateTimeType object) {
				return createDateTimeTypeAdapter();
			}
			@Override
			public Adapter caseDependency1Type(Dependency1Type object) {
				return createDependency1TypeAdapter();
			}
			@Override
			public Adapter caseDependencyType(DependencyType object) {
				return createDependencyTypeAdapter();
			}
			@Override
			public Adapter caseDescriptionType(DescriptionType object) {
				return createDescriptionTypeAdapter();
			}
			@Override
			public Adapter caseDocumentRoot(DocumentRoot object) {
				return createDocumentRootAdapter();
			}
			@Override
			public Adapter caseEarliestStartTimeType(EarliestStartTimeType object) {
				return createEarliestStartTimeTypeAdapter();
			}
			@Override
			public Adapter caseEndTimeType(EndTimeType object) {
				return createEndTimeTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentAssetMappingType(EquipmentAssetMappingType object) {
				return createEquipmentAssetMappingTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentCapabilityTestSpecificationIDType(EquipmentCapabilityTestSpecificationIDType object) {
				return createEquipmentCapabilityTestSpecificationIDTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentCapabilityTestSpecificationType(EquipmentCapabilityTestSpecificationType object) {
				return createEquipmentCapabilityTestSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentClassIDType(EquipmentClassIDType object) {
				return createEquipmentClassIDTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentClassPropertyType(EquipmentClassPropertyType object) {
				return createEquipmentClassPropertyTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentClassType(EquipmentClassType object) {
				return createEquipmentClassTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentElementLevel1Type(EquipmentElementLevel1Type object) {
				return createEquipmentElementLevel1TypeAdapter();
			}
			@Override
			public Adapter caseEquipmentElementLevelType(EquipmentElementLevelType object) {
				return createEquipmentElementLevelTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentIDType(EquipmentIDType object) {
				return createEquipmentIDTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentInformationType(EquipmentInformationType object) {
				return createEquipmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentPropertyType(EquipmentPropertyType object) {
				return createEquipmentPropertyTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentSegmentSpecificationPropertyType(EquipmentSegmentSpecificationPropertyType object) {
				return createEquipmentSegmentSpecificationPropertyTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentSegmentSpecificationType(EquipmentSegmentSpecificationType object) {
				return createEquipmentSegmentSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentType(EquipmentType object) {
				return createEquipmentTypeAdapter();
			}
			@Override
			public Adapter caseEquipmentUseType(EquipmentUseType object) {
				return createEquipmentUseTypeAdapter();
			}
			@Override
			public Adapter caseExpirationTimeType(ExpirationTimeType object) {
				return createExpirationTimeTypeAdapter();
			}
			@Override
			public Adapter caseGetEquipmentCapabilityTestSpecType(GetEquipmentCapabilityTestSpecType object) {
				return createGetEquipmentCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseGetEquipmentClassType(GetEquipmentClassType object) {
				return createGetEquipmentClassTypeAdapter();
			}
			@Override
			public Adapter caseGetEquipmentInformationType(GetEquipmentInformationType object) {
				return createGetEquipmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseGetEquipmentType(GetEquipmentType object) {
				return createGetEquipmentTypeAdapter();
			}
			@Override
			public Adapter caseGetMaterialClassType(GetMaterialClassType object) {
				return createGetMaterialClassTypeAdapter();
			}
			@Override
			public Adapter caseGetMaterialDefinitionType(GetMaterialDefinitionType object) {
				return createGetMaterialDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseGetMaterialInformationType(GetMaterialInformationType object) {
				return createGetMaterialInformationTypeAdapter();
			}
			@Override
			public Adapter caseGetMaterialLotType(GetMaterialLotType object) {
				return createGetMaterialLotTypeAdapter();
			}
			@Override
			public Adapter caseGetMaterialSubLotType(GetMaterialSubLotType object) {
				return createGetMaterialSubLotTypeAdapter();
			}
			@Override
			public Adapter caseGetMaterialTestSpecType(GetMaterialTestSpecType object) {
				return createGetMaterialTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseGetOperationsCapabilityInformationType(GetOperationsCapabilityInformationType object) {
				return createGetOperationsCapabilityInformationTypeAdapter();
			}
			@Override
			public Adapter caseGetOperationsCapabilityType(GetOperationsCapabilityType object) {
				return createGetOperationsCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseGetOperationsDefinitionInformationType(GetOperationsDefinitionInformationType object) {
				return createGetOperationsDefinitionInformationTypeAdapter();
			}
			@Override
			public Adapter caseGetOperationsDefinitionType(GetOperationsDefinitionType object) {
				return createGetOperationsDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseGetOperationsPerformanceType(GetOperationsPerformanceType object) {
				return createGetOperationsPerformanceTypeAdapter();
			}
			@Override
			public Adapter caseGetOperationsScheduleType(GetOperationsScheduleType object) {
				return createGetOperationsScheduleTypeAdapter();
			}
			@Override
			public Adapter caseGetPersonnelClassType(GetPersonnelClassType object) {
				return createGetPersonnelClassTypeAdapter();
			}
			@Override
			public Adapter caseGetPersonnelInformationType(GetPersonnelInformationType object) {
				return createGetPersonnelInformationTypeAdapter();
			}
			@Override
			public Adapter caseGetPersonType(GetPersonType object) {
				return createGetPersonTypeAdapter();
			}
			@Override
			public Adapter caseGetPhysicalAssetCapabilityTestSpecType(GetPhysicalAssetCapabilityTestSpecType object) {
				return createGetPhysicalAssetCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseGetPhysicalAssetClassType(GetPhysicalAssetClassType object) {
				return createGetPhysicalAssetClassTypeAdapter();
			}
			@Override
			public Adapter caseGetPhysicalAssetInformationType(GetPhysicalAssetInformationType object) {
				return createGetPhysicalAssetInformationTypeAdapter();
			}
			@Override
			public Adapter caseGetPhysicalAssetType(GetPhysicalAssetType object) {
				return createGetPhysicalAssetTypeAdapter();
			}
			@Override
			public Adapter caseGetProcessSegmentInformationType(GetProcessSegmentInformationType object) {
				return createGetProcessSegmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseGetProcessSegmentType(GetProcessSegmentType object) {
				return createGetProcessSegmentTypeAdapter();
			}
			@Override
			public Adapter caseGetQualificationTestSpecificationType(GetQualificationTestSpecificationType object) {
				return createGetQualificationTestSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseHierarchyScopeType(HierarchyScopeType object) {
				return createHierarchyScopeTypeAdapter();
			}
			@Override
			public Adapter caseIdentifierType(IdentifierType object) {
				return createIdentifierTypeAdapter();
			}
			@Override
			public Adapter caseJobOrderCommand1Type(JobOrderCommand1Type object) {
				return createJobOrderCommand1TypeAdapter();
			}
			@Override
			public Adapter caseJobOrderCommandRuleType(JobOrderCommandRuleType object) {
				return createJobOrderCommandRuleTypeAdapter();
			}
			@Override
			public Adapter caseJobOrderCommandType(JobOrderCommandType object) {
				return createJobOrderCommandTypeAdapter();
			}
			@Override
			public Adapter caseJobOrderDispatchStatusType(JobOrderDispatchStatusType object) {
				return createJobOrderDispatchStatusTypeAdapter();
			}
			@Override
			public Adapter caseLatestEndTimeType(LatestEndTimeType object) {
				return createLatestEndTimeTypeAdapter();
			}
			@Override
			public Adapter caseLocationType(LocationType object) {
				return createLocationTypeAdapter();
			}
			@Override
			public Adapter caseManufacturingBillIDType(ManufacturingBillIDType object) {
				return createManufacturingBillIDTypeAdapter();
			}
			@Override
			public Adapter caseMaterialActualIDType(MaterialActualIDType object) {
				return createMaterialActualIDTypeAdapter();
			}
			@Override
			public Adapter caseMaterialCapabilityIDType(MaterialCapabilityIDType object) {
				return createMaterialCapabilityIDTypeAdapter();
			}
			@Override
			public Adapter caseMaterialClassIDType(MaterialClassIDType object) {
				return createMaterialClassIDTypeAdapter();
			}
			@Override
			public Adapter caseMaterialClassPropertyType(MaterialClassPropertyType object) {
				return createMaterialClassPropertyTypeAdapter();
			}
			@Override
			public Adapter caseMaterialClassType(MaterialClassType object) {
				return createMaterialClassTypeAdapter();
			}
			@Override
			public Adapter caseMaterialDefinitionIDType(MaterialDefinitionIDType object) {
				return createMaterialDefinitionIDTypeAdapter();
			}
			@Override
			public Adapter caseMaterialDefinitionPropertyType(MaterialDefinitionPropertyType object) {
				return createMaterialDefinitionPropertyTypeAdapter();
			}
			@Override
			public Adapter caseMaterialDefinitionType(MaterialDefinitionType object) {
				return createMaterialDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseMaterialInformationType(MaterialInformationType object) {
				return createMaterialInformationTypeAdapter();
			}
			@Override
			public Adapter caseMaterialLotIDType(MaterialLotIDType object) {
				return createMaterialLotIDTypeAdapter();
			}
			@Override
			public Adapter caseMaterialLotPropertyType(MaterialLotPropertyType object) {
				return createMaterialLotPropertyTypeAdapter();
			}
			@Override
			public Adapter caseMaterialLotType(MaterialLotType object) {
				return createMaterialLotTypeAdapter();
			}
			@Override
			public Adapter caseMaterialRequirementIDType(MaterialRequirementIDType object) {
				return createMaterialRequirementIDTypeAdapter();
			}
			@Override
			public Adapter caseMaterialSegmentSpecificationPropertyType(MaterialSegmentSpecificationPropertyType object) {
				return createMaterialSegmentSpecificationPropertyTypeAdapter();
			}
			@Override
			public Adapter caseMaterialSegmentSpecificationType(MaterialSegmentSpecificationType object) {
				return createMaterialSegmentSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseMaterialSpecificationIDType(MaterialSpecificationIDType object) {
				return createMaterialSpecificationIDTypeAdapter();
			}
			@Override
			public Adapter caseMaterialSubLotIDType(MaterialSubLotIDType object) {
				return createMaterialSubLotIDTypeAdapter();
			}
			@Override
			public Adapter caseMaterialSubLotType(MaterialSubLotType object) {
				return createMaterialSubLotTypeAdapter();
			}
			@Override
			public Adapter caseMaterialTestSpecificationIDType(MaterialTestSpecificationIDType object) {
				return createMaterialTestSpecificationIDTypeAdapter();
			}
			@Override
			public Adapter caseMaterialTestSpecificationType(MaterialTestSpecificationType object) {
				return createMaterialTestSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseMaterialUse1Type(MaterialUse1Type object) {
				return createMaterialUse1TypeAdapter();
			}
			@Override
			public Adapter caseMaterialUseType(MaterialUseType object) {
				return createMaterialUseTypeAdapter();
			}
			@Override
			public Adapter caseMeasureType(MeasureType object) {
				return createMeasureTypeAdapter();
			}
			@Override
			public Adapter caseNameType(NameType object) {
				return createNameTypeAdapter();
			}
			@Override
			public Adapter caseNumericType(NumericType object) {
				return createNumericTypeAdapter();
			}
			@Override
			public Adapter caseOpEquipmentActualPropertyType(OpEquipmentActualPropertyType object) {
				return createOpEquipmentActualPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpEquipmentActualType(OpEquipmentActualType object) {
				return createOpEquipmentActualTypeAdapter();
			}
			@Override
			public Adapter caseOpEquipmentCapabilityPropertyType(OpEquipmentCapabilityPropertyType object) {
				return createOpEquipmentCapabilityPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpEquipmentCapabilityType(OpEquipmentCapabilityType object) {
				return createOpEquipmentCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseOpEquipmentRequirementPropertyType(OpEquipmentRequirementPropertyType object) {
				return createOpEquipmentRequirementPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpEquipmentRequirementType(OpEquipmentRequirementType object) {
				return createOpEquipmentRequirementTypeAdapter();
			}
			@Override
			public Adapter caseOpEquipmentSpecificationPropertyType(OpEquipmentSpecificationPropertyType object) {
				return createOpEquipmentSpecificationPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpEquipmentSpecificationType(OpEquipmentSpecificationType object) {
				return createOpEquipmentSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseOperationsCapabilityInformationType(OperationsCapabilityInformationType object) {
				return createOperationsCapabilityInformationTypeAdapter();
			}
			@Override
			public Adapter caseOperationsCapabilityType(OperationsCapabilityType object) {
				return createOperationsCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseOperationsDefinitionIDType(OperationsDefinitionIDType object) {
				return createOperationsDefinitionIDTypeAdapter();
			}
			@Override
			public Adapter caseOperationsDefinitionInformationType(OperationsDefinitionInformationType object) {
				return createOperationsDefinitionInformationTypeAdapter();
			}
			@Override
			public Adapter caseOperationsDefinitionType(OperationsDefinitionType object) {
				return createOperationsDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseOperationsMaterialBillItemType(OperationsMaterialBillItemType object) {
				return createOperationsMaterialBillItemTypeAdapter();
			}
			@Override
			public Adapter caseOperationsMaterialBillType(OperationsMaterialBillType object) {
				return createOperationsMaterialBillTypeAdapter();
			}
			@Override
			public Adapter caseOperationsPerformanceType(OperationsPerformanceType object) {
				return createOperationsPerformanceTypeAdapter();
			}
			@Override
			public Adapter caseOperationsRequestIDType(OperationsRequestIDType object) {
				return createOperationsRequestIDTypeAdapter();
			}
			@Override
			public Adapter caseOperationsRequestType(OperationsRequestType object) {
				return createOperationsRequestTypeAdapter();
			}
			@Override
			public Adapter caseOperationsResponseType(OperationsResponseType object) {
				return createOperationsResponseTypeAdapter();
			}
			@Override
			public Adapter caseOperationsScheduleIDType(OperationsScheduleIDType object) {
				return createOperationsScheduleIDTypeAdapter();
			}
			@Override
			public Adapter caseOperationsScheduleType(OperationsScheduleType object) {
				return createOperationsScheduleTypeAdapter();
			}
			@Override
			public Adapter caseOperationsSegmentIDType(OperationsSegmentIDType object) {
				return createOperationsSegmentIDTypeAdapter();
			}
			@Override
			public Adapter caseOperationsSegmentType(OperationsSegmentType object) {
				return createOperationsSegmentTypeAdapter();
			}
			@Override
			public Adapter caseOperationsType1Type(OperationsType1Type object) {
				return createOperationsType1TypeAdapter();
			}
			@Override
			public Adapter caseOperationsTypeType(OperationsTypeType object) {
				return createOperationsTypeTypeAdapter();
			}
			@Override
			public Adapter caseOpMaterialActualPropertyType(OpMaterialActualPropertyType object) {
				return createOpMaterialActualPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpMaterialActualType(OpMaterialActualType object) {
				return createOpMaterialActualTypeAdapter();
			}
			@Override
			public Adapter caseOpMaterialCapabilityPropertyType(OpMaterialCapabilityPropertyType object) {
				return createOpMaterialCapabilityPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpMaterialCapabilityType(OpMaterialCapabilityType object) {
				return createOpMaterialCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseOpMaterialRequirementPropertyType(OpMaterialRequirementPropertyType object) {
				return createOpMaterialRequirementPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpMaterialRequirementType(OpMaterialRequirementType object) {
				return createOpMaterialRequirementTypeAdapter();
			}
			@Override
			public Adapter caseOpMaterialSpecificationPropertyType(OpMaterialSpecificationPropertyType object) {
				return createOpMaterialSpecificationPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpMaterialSpecificationType(OpMaterialSpecificationType object) {
				return createOpMaterialSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseOpPersonnelActualPropertyType(OpPersonnelActualPropertyType object) {
				return createOpPersonnelActualPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpPersonnelActualType(OpPersonnelActualType object) {
				return createOpPersonnelActualTypeAdapter();
			}
			@Override
			public Adapter caseOpPersonnelCapabilityPropertyType(OpPersonnelCapabilityPropertyType object) {
				return createOpPersonnelCapabilityPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpPersonnelCapabilityType(OpPersonnelCapabilityType object) {
				return createOpPersonnelCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseOpPersonnelRequirementPropertyType(OpPersonnelRequirementPropertyType object) {
				return createOpPersonnelRequirementPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpPersonnelRequirementType(OpPersonnelRequirementType object) {
				return createOpPersonnelRequirementTypeAdapter();
			}
			@Override
			public Adapter caseOpPersonnelSpecificationPropertyType(OpPersonnelSpecificationPropertyType object) {
				return createOpPersonnelSpecificationPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpPersonnelSpecificationType(OpPersonnelSpecificationType object) {
				return createOpPersonnelSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseOpPhysicalAssetActualPropertyType(OpPhysicalAssetActualPropertyType object) {
				return createOpPhysicalAssetActualPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpPhysicalAssetActualType(OpPhysicalAssetActualType object) {
				return createOpPhysicalAssetActualTypeAdapter();
			}
			@Override
			public Adapter caseOpPhysicalAssetCapabilityPropertyType(OpPhysicalAssetCapabilityPropertyType object) {
				return createOpPhysicalAssetCapabilityPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpPhysicalAssetCapabilityType(OpPhysicalAssetCapabilityType object) {
				return createOpPhysicalAssetCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseOpPhysicalAssetRequirementPropertyType(OpPhysicalAssetRequirementPropertyType object) {
				return createOpPhysicalAssetRequirementPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpPhysicalAssetRequirementType(OpPhysicalAssetRequirementType object) {
				return createOpPhysicalAssetRequirementTypeAdapter();
			}
			@Override
			public Adapter caseOpPhysicalAssetSpecificationPropertyType(OpPhysicalAssetSpecificationPropertyType object) {
				return createOpPhysicalAssetSpecificationPropertyTypeAdapter();
			}
			@Override
			public Adapter caseOpPhysicalAssetSpecificationType(OpPhysicalAssetSpecificationType object) {
				return createOpPhysicalAssetSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseOpProcessSegmentCapabilityType(OpProcessSegmentCapabilityType object) {
				return createOpProcessSegmentCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseOpSegmentDataType(OpSegmentDataType object) {
				return createOpSegmentDataTypeAdapter();
			}
			@Override
			public Adapter caseOpSegmentRequirementType(OpSegmentRequirementType object) {
				return createOpSegmentRequirementTypeAdapter();
			}
			@Override
			public Adapter caseOpSegmentResponseType(OpSegmentResponseType object) {
				return createOpSegmentResponseTypeAdapter();
			}
			@Override
			public Adapter caseOtherDependencyType(OtherDependencyType object) {
				return createOtherDependencyTypeAdapter();
			}
			@Override
			public Adapter caseParameterIDType(ParameterIDType object) {
				return createParameterIDTypeAdapter();
			}
			@Override
			public Adapter caseParameterType(ParameterType object) {
				return createParameterTypeAdapter();
			}
			@Override
			public Adapter casePersonIDType(PersonIDType object) {
				return createPersonIDTypeAdapter();
			}
			@Override
			public Adapter casePersonNameType(PersonNameType object) {
				return createPersonNameTypeAdapter();
			}
			@Override
			public Adapter casePersonnelClassIDType(PersonnelClassIDType object) {
				return createPersonnelClassIDTypeAdapter();
			}
			@Override
			public Adapter casePersonnelClassPropertyType(PersonnelClassPropertyType object) {
				return createPersonnelClassPropertyTypeAdapter();
			}
			@Override
			public Adapter casePersonnelClassType(PersonnelClassType object) {
				return createPersonnelClassTypeAdapter();
			}
			@Override
			public Adapter casePersonnelInformationType(PersonnelInformationType object) {
				return createPersonnelInformationTypeAdapter();
			}
			@Override
			public Adapter casePersonnelSegmentSpecificationPropertyType(PersonnelSegmentSpecificationPropertyType object) {
				return createPersonnelSegmentSpecificationPropertyTypeAdapter();
			}
			@Override
			public Adapter casePersonnelSegmentSpecificationType(PersonnelSegmentSpecificationType object) {
				return createPersonnelSegmentSpecificationTypeAdapter();
			}
			@Override
			public Adapter casePersonnelUseType(PersonnelUseType object) {
				return createPersonnelUseTypeAdapter();
			}
			@Override
			public Adapter casePersonPropertyType(PersonPropertyType object) {
				return createPersonPropertyTypeAdapter();
			}
			@Override
			public Adapter casePersonType(PersonType object) {
				return createPersonTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetActualIDType(PhysicalAssetActualIDType object) {
				return createPhysicalAssetActualIDTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetCapabilityTestSpecificationIDType(PhysicalAssetCapabilityTestSpecificationIDType object) {
				return createPhysicalAssetCapabilityTestSpecificationIDTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetCapabilityTestSpecificationType(PhysicalAssetCapabilityTestSpecificationType object) {
				return createPhysicalAssetCapabilityTestSpecificationTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetClassIDType(PhysicalAssetClassIDType object) {
				return createPhysicalAssetClassIDTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetClassPropertyType(PhysicalAssetClassPropertyType object) {
				return createPhysicalAssetClassPropertyTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetClassType(PhysicalAssetClassType object) {
				return createPhysicalAssetClassTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetIDType(PhysicalAssetIDType object) {
				return createPhysicalAssetIDTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetInformationType(PhysicalAssetInformationType object) {
				return createPhysicalAssetInformationTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetPropertyType(PhysicalAssetPropertyType object) {
				return createPhysicalAssetPropertyTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetSegmentSpecificationPropertyType(PhysicalAssetSegmentSpecificationPropertyType object) {
				return createPhysicalAssetSegmentSpecificationPropertyTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetSegmentSpecificationType(PhysicalAssetSegmentSpecificationType object) {
				return createPhysicalAssetSegmentSpecificationTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetType(PhysicalAssetType object) {
				return createPhysicalAssetTypeAdapter();
			}
			@Override
			public Adapter casePhysicalAssetUseType(PhysicalAssetUseType object) {
				return createPhysicalAssetUseTypeAdapter();
			}
			@Override
			public Adapter casePlannedFinishTimeType(PlannedFinishTimeType object) {
				return createPlannedFinishTimeTypeAdapter();
			}
			@Override
			public Adapter casePlannedStartTimeType(PlannedStartTimeType object) {
				return createPlannedStartTimeTypeAdapter();
			}
			@Override
			public Adapter casePriorityType(PriorityType object) {
				return createPriorityTypeAdapter();
			}
			@Override
			public Adapter caseProblemType(ProblemType object) {
				return createProblemTypeAdapter();
			}
			@Override
			public Adapter caseProcessEquipmentCapabilityTestSpecType(ProcessEquipmentCapabilityTestSpecType object) {
				return createProcessEquipmentCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseProcessEquipmentClassType(ProcessEquipmentClassType object) {
				return createProcessEquipmentClassTypeAdapter();
			}
			@Override
			public Adapter caseProcessEquipmentInformationType(ProcessEquipmentInformationType object) {
				return createProcessEquipmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseProcessEquipmentType(ProcessEquipmentType object) {
				return createProcessEquipmentTypeAdapter();
			}
			@Override
			public Adapter caseProcessMaterialClassType(ProcessMaterialClassType object) {
				return createProcessMaterialClassTypeAdapter();
			}
			@Override
			public Adapter caseProcessMaterialDefinitionType(ProcessMaterialDefinitionType object) {
				return createProcessMaterialDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseProcessMaterialInformationType(ProcessMaterialInformationType object) {
				return createProcessMaterialInformationTypeAdapter();
			}
			@Override
			public Adapter caseProcessMaterialLotType(ProcessMaterialLotType object) {
				return createProcessMaterialLotTypeAdapter();
			}
			@Override
			public Adapter caseProcessMaterialSubLotType(ProcessMaterialSubLotType object) {
				return createProcessMaterialSubLotTypeAdapter();
			}
			@Override
			public Adapter caseProcessMaterialTestSpecType(ProcessMaterialTestSpecType object) {
				return createProcessMaterialTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseProcessOperationsCapabilityInformationType(ProcessOperationsCapabilityInformationType object) {
				return createProcessOperationsCapabilityInformationTypeAdapter();
			}
			@Override
			public Adapter caseProcessOperationsCapabilityType(ProcessOperationsCapabilityType object) {
				return createProcessOperationsCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseProcessOperationsDefinitionInformationType(ProcessOperationsDefinitionInformationType object) {
				return createProcessOperationsDefinitionInformationTypeAdapter();
			}
			@Override
			public Adapter caseProcessOperationsDefinitionType(ProcessOperationsDefinitionType object) {
				return createProcessOperationsDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseProcessOperationsPerformanceType(ProcessOperationsPerformanceType object) {
				return createProcessOperationsPerformanceTypeAdapter();
			}
			@Override
			public Adapter caseProcessOperationsScheduleType(ProcessOperationsScheduleType object) {
				return createProcessOperationsScheduleTypeAdapter();
			}
			@Override
			public Adapter caseProcessPersonnelClassType(ProcessPersonnelClassType object) {
				return createProcessPersonnelClassTypeAdapter();
			}
			@Override
			public Adapter caseProcessPersonnelInformationType(ProcessPersonnelInformationType object) {
				return createProcessPersonnelInformationTypeAdapter();
			}
			@Override
			public Adapter caseProcessPersonType(ProcessPersonType object) {
				return createProcessPersonTypeAdapter();
			}
			@Override
			public Adapter caseProcessPhysicalAssetCapabilityTestSpecType(ProcessPhysicalAssetCapabilityTestSpecType object) {
				return createProcessPhysicalAssetCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseProcessPhysicalAssetClassType(ProcessPhysicalAssetClassType object) {
				return createProcessPhysicalAssetClassTypeAdapter();
			}
			@Override
			public Adapter caseProcessPhysicalAssetInformationType(ProcessPhysicalAssetInformationType object) {
				return createProcessPhysicalAssetInformationTypeAdapter();
			}
			@Override
			public Adapter caseProcessPhysicalAssetType(ProcessPhysicalAssetType object) {
				return createProcessPhysicalAssetTypeAdapter();
			}
			@Override
			public Adapter caseProcessProcessSegmentInformationType(ProcessProcessSegmentInformationType object) {
				return createProcessProcessSegmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseProcessProcessSegmentType(ProcessProcessSegmentType object) {
				return createProcessProcessSegmentTypeAdapter();
			}
			@Override
			public Adapter caseProcessQualificationTestSpecificationType(ProcessQualificationTestSpecificationType object) {
				return createProcessQualificationTestSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseProcessSegmentIDType(ProcessSegmentIDType object) {
				return createProcessSegmentIDTypeAdapter();
			}
			@Override
			public Adapter caseProcessSegmentInformationType(ProcessSegmentInformationType object) {
				return createProcessSegmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseProcessSegmentType(ProcessSegmentType object) {
				return createProcessSegmentTypeAdapter();
			}
			@Override
			public Adapter caseProductionRequestIDType(ProductionRequestIDType object) {
				return createProductionRequestIDTypeAdapter();
			}
			@Override
			public Adapter caseProductionScheduleIDType(ProductionScheduleIDType object) {
				return createProductionScheduleIDTypeAdapter();
			}
			@Override
			public Adapter caseProductProductionRuleIDType(ProductProductionRuleIDType object) {
				return createProductProductionRuleIDTypeAdapter();
			}
			@Override
			public Adapter caseProductProductionRuleType(ProductProductionRuleType object) {
				return createProductProductionRuleTypeAdapter();
			}
			@Override
			public Adapter caseProductSegmentIDType(ProductSegmentIDType object) {
				return createProductSegmentIDTypeAdapter();
			}
			@Override
			public Adapter casePropertyIDType(PropertyIDType object) {
				return createPropertyIDTypeAdapter();
			}
			@Override
			public Adapter casePublishedDateType(PublishedDateType object) {
				return createPublishedDateTypeAdapter();
			}
			@Override
			public Adapter caseQualificationTestSpecificationIDType(QualificationTestSpecificationIDType object) {
				return createQualificationTestSpecificationIDTypeAdapter();
			}
			@Override
			public Adapter caseQualificationTestSpecificationType(QualificationTestSpecificationType object) {
				return createQualificationTestSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseQuantityStringType(QuantityStringType object) {
				return createQuantityStringTypeAdapter();
			}
			@Override
			public Adapter caseQuantityType(QuantityType object) {
				return createQuantityTypeAdapter();
			}
			@Override
			public Adapter caseQuantityValueType(QuantityValueType object) {
				return createQuantityValueTypeAdapter();
			}
			@Override
			public Adapter caseReasonType(ReasonType object) {
				return createReasonTypeAdapter();
			}
			@Override
			public Adapter caseRelationshipForm1Type(RelationshipForm1Type object) {
				return createRelationshipForm1TypeAdapter();
			}
			@Override
			public Adapter caseRelationshipFormType(RelationshipFormType object) {
				return createRelationshipFormTypeAdapter();
			}
			@Override
			public Adapter caseRelationshipType1Type(RelationshipType1Type object) {
				return createRelationshipType1TypeAdapter();
			}
			@Override
			public Adapter caseRelationshipTypeType(RelationshipTypeType object) {
				return createRelationshipTypeTypeAdapter();
			}
			@Override
			public Adapter caseRequestedCompletionDateType(RequestedCompletionDateType object) {
				return createRequestedCompletionDateTypeAdapter();
			}
			@Override
			public Adapter caseRequestedPriorityType(RequestedPriorityType object) {
				return createRequestedPriorityTypeAdapter();
			}
			@Override
			public Adapter caseRequestState1Type(RequestState1Type object) {
				return createRequestState1TypeAdapter();
			}
			@Override
			public Adapter caseRequestStateType(RequestStateType object) {
				return createRequestStateTypeAdapter();
			}
			@Override
			public Adapter caseRequiredByRequestedSegmentResponse1Type(RequiredByRequestedSegmentResponse1Type object) {
				return createRequiredByRequestedSegmentResponse1TypeAdapter();
			}
			@Override
			public Adapter caseRequiredByRequestedSegmentResponseType(RequiredByRequestedSegmentResponseType object) {
				return createRequiredByRequestedSegmentResponseTypeAdapter();
			}
			@Override
			public Adapter caseResourceIDType(ResourceIDType object) {
				return createResourceIDTypeAdapter();
			}
			@Override
			public Adapter caseResourceNetworkConnectionIDType(ResourceNetworkConnectionIDType object) {
				return createResourceNetworkConnectionIDTypeAdapter();
			}
			@Override
			public Adapter caseResourceReferenceType1Type(ResourceReferenceType1Type object) {
				return createResourceReferenceType1TypeAdapter();
			}
			@Override
			public Adapter caseResourceReferenceTypeType(ResourceReferenceTypeType object) {
				return createResourceReferenceTypeTypeAdapter();
			}
			@Override
			public Adapter caseResourcesType(ResourcesType object) {
				return createResourcesTypeAdapter();
			}
			@Override
			public Adapter caseRespondEquipmentCapabilityTestSpecType(RespondEquipmentCapabilityTestSpecType object) {
				return createRespondEquipmentCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseRespondEquipmentClassType(RespondEquipmentClassType object) {
				return createRespondEquipmentClassTypeAdapter();
			}
			@Override
			public Adapter caseRespondEquipmentInformationType(RespondEquipmentInformationType object) {
				return createRespondEquipmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseRespondEquipmentType(RespondEquipmentType object) {
				return createRespondEquipmentTypeAdapter();
			}
			@Override
			public Adapter caseRespondMaterialClassType(RespondMaterialClassType object) {
				return createRespondMaterialClassTypeAdapter();
			}
			@Override
			public Adapter caseRespondMaterialDefinitionType(RespondMaterialDefinitionType object) {
				return createRespondMaterialDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseRespondMaterialInformationType(RespondMaterialInformationType object) {
				return createRespondMaterialInformationTypeAdapter();
			}
			@Override
			public Adapter caseRespondMaterialLotType(RespondMaterialLotType object) {
				return createRespondMaterialLotTypeAdapter();
			}
			@Override
			public Adapter caseRespondMaterialSubLotType(RespondMaterialSubLotType object) {
				return createRespondMaterialSubLotTypeAdapter();
			}
			@Override
			public Adapter caseRespondMaterialTestSpecType(RespondMaterialTestSpecType object) {
				return createRespondMaterialTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseRespondOperationsCapabilityInformationType(RespondOperationsCapabilityInformationType object) {
				return createRespondOperationsCapabilityInformationTypeAdapter();
			}
			@Override
			public Adapter caseRespondOperationsCapabilityType(RespondOperationsCapabilityType object) {
				return createRespondOperationsCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseRespondOperationsDefinitionInformationType(RespondOperationsDefinitionInformationType object) {
				return createRespondOperationsDefinitionInformationTypeAdapter();
			}
			@Override
			public Adapter caseRespondOperationsDefinitionType(RespondOperationsDefinitionType object) {
				return createRespondOperationsDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseRespondOperationsPerformanceType(RespondOperationsPerformanceType object) {
				return createRespondOperationsPerformanceTypeAdapter();
			}
			@Override
			public Adapter caseRespondOperationsScheduleType(RespondOperationsScheduleType object) {
				return createRespondOperationsScheduleTypeAdapter();
			}
			@Override
			public Adapter caseRespondPersonnelClassType(RespondPersonnelClassType object) {
				return createRespondPersonnelClassTypeAdapter();
			}
			@Override
			public Adapter caseRespondPersonnelInformationType(RespondPersonnelInformationType object) {
				return createRespondPersonnelInformationTypeAdapter();
			}
			@Override
			public Adapter caseRespondPersonType(RespondPersonType object) {
				return createRespondPersonTypeAdapter();
			}
			@Override
			public Adapter caseRespondPhysicalAssetCapabilityTestSpecType(RespondPhysicalAssetCapabilityTestSpecType object) {
				return createRespondPhysicalAssetCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseRespondPhysicalAssetClassType(RespondPhysicalAssetClassType object) {
				return createRespondPhysicalAssetClassTypeAdapter();
			}
			@Override
			public Adapter caseRespondPhysicalAssetInformationType(RespondPhysicalAssetInformationType object) {
				return createRespondPhysicalAssetInformationTypeAdapter();
			}
			@Override
			public Adapter caseRespondPhysicalAssetType(RespondPhysicalAssetType object) {
				return createRespondPhysicalAssetTypeAdapter();
			}
			@Override
			public Adapter caseRespondProcessSegmentInformationType(RespondProcessSegmentInformationType object) {
				return createRespondProcessSegmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseRespondProcessSegmentType(RespondProcessSegmentType object) {
				return createRespondProcessSegmentTypeAdapter();
			}
			@Override
			public Adapter caseRespondQualificationTestSpecificationType(RespondQualificationTestSpecificationType object) {
				return createRespondQualificationTestSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseResponseState1Type(ResponseState1Type object) {
				return createResponseState1TypeAdapter();
			}
			@Override
			public Adapter caseResponseStateType(ResponseStateType object) {
				return createResponseStateTypeAdapter();
			}
			@Override
			public Adapter caseResultType(ResultType object) {
				return createResultTypeAdapter();
			}
			@Override
			public Adapter caseSegmentDependencyType(SegmentDependencyType object) {
				return createSegmentDependencyTypeAdapter();
			}
			@Override
			public Adapter caseShowEquipmentCapabilityTestSpecType(ShowEquipmentCapabilityTestSpecType object) {
				return createShowEquipmentCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseShowEquipmentClassType(ShowEquipmentClassType object) {
				return createShowEquipmentClassTypeAdapter();
			}
			@Override
			public Adapter caseShowEquipmentInformationType(ShowEquipmentInformationType object) {
				return createShowEquipmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseShowEquipmentType(ShowEquipmentType object) {
				return createShowEquipmentTypeAdapter();
			}
			@Override
			public Adapter caseShowMaterialClassType(ShowMaterialClassType object) {
				return createShowMaterialClassTypeAdapter();
			}
			@Override
			public Adapter caseShowMaterialDefinitionType(ShowMaterialDefinitionType object) {
				return createShowMaterialDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseShowMaterialInformationType(ShowMaterialInformationType object) {
				return createShowMaterialInformationTypeAdapter();
			}
			@Override
			public Adapter caseShowMaterialLotType(ShowMaterialLotType object) {
				return createShowMaterialLotTypeAdapter();
			}
			@Override
			public Adapter caseShowMaterialSubLotType(ShowMaterialSubLotType object) {
				return createShowMaterialSubLotTypeAdapter();
			}
			@Override
			public Adapter caseShowMaterialTestSpecType(ShowMaterialTestSpecType object) {
				return createShowMaterialTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseShowOperationsCapabilityInformationType(ShowOperationsCapabilityInformationType object) {
				return createShowOperationsCapabilityInformationTypeAdapter();
			}
			@Override
			public Adapter caseShowOperationsCapabilityType(ShowOperationsCapabilityType object) {
				return createShowOperationsCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseShowOperationsDefinitionInformationType(ShowOperationsDefinitionInformationType object) {
				return createShowOperationsDefinitionInformationTypeAdapter();
			}
			@Override
			public Adapter caseShowOperationsDefinitionType(ShowOperationsDefinitionType object) {
				return createShowOperationsDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseShowOperationsPerformanceType(ShowOperationsPerformanceType object) {
				return createShowOperationsPerformanceTypeAdapter();
			}
			@Override
			public Adapter caseShowOperationsScheduleType(ShowOperationsScheduleType object) {
				return createShowOperationsScheduleTypeAdapter();
			}
			@Override
			public Adapter caseShowPersonnelClassType(ShowPersonnelClassType object) {
				return createShowPersonnelClassTypeAdapter();
			}
			@Override
			public Adapter caseShowPersonnelInformationType(ShowPersonnelInformationType object) {
				return createShowPersonnelInformationTypeAdapter();
			}
			@Override
			public Adapter caseShowPersonType(ShowPersonType object) {
				return createShowPersonTypeAdapter();
			}
			@Override
			public Adapter caseShowPhysicalAssetCapabilityTestSpecType(ShowPhysicalAssetCapabilityTestSpecType object) {
				return createShowPhysicalAssetCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseShowPhysicalAssetClassType(ShowPhysicalAssetClassType object) {
				return createShowPhysicalAssetClassTypeAdapter();
			}
			@Override
			public Adapter caseShowPhysicalAssetInformationType(ShowPhysicalAssetInformationType object) {
				return createShowPhysicalAssetInformationTypeAdapter();
			}
			@Override
			public Adapter caseShowPhysicalAssetType(ShowPhysicalAssetType object) {
				return createShowPhysicalAssetTypeAdapter();
			}
			@Override
			public Adapter caseShowProcessSegmentInformationType(ShowProcessSegmentInformationType object) {
				return createShowProcessSegmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseShowProcessSegmentType(ShowProcessSegmentType object) {
				return createShowProcessSegmentTypeAdapter();
			}
			@Override
			public Adapter caseShowQualificationTestSpecificationType(ShowQualificationTestSpecificationType object) {
				return createShowQualificationTestSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseStartTimeType(StartTimeType object) {
				return createStartTimeTypeAdapter();
			}
			@Override
			public Adapter caseStatusTimeType(StatusTimeType object) {
				return createStatusTimeTypeAdapter();
			}
			@Override
			public Adapter caseStatusType(StatusType object) {
				return createStatusTypeAdapter();
			}
			@Override
			public Adapter caseStorageHierarchyScopeType(StorageHierarchyScopeType object) {
				return createStorageHierarchyScopeTypeAdapter();
			}
			@Override
			public Adapter caseStorageLocationType(StorageLocationType object) {
				return createStorageLocationTypeAdapter();
			}
			@Override
			public Adapter caseSyncEquipmentCapabilityTestSpecType(SyncEquipmentCapabilityTestSpecType object) {
				return createSyncEquipmentCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseSyncEquipmentClassType(SyncEquipmentClassType object) {
				return createSyncEquipmentClassTypeAdapter();
			}
			@Override
			public Adapter caseSyncEquipmentInformationType(SyncEquipmentInformationType object) {
				return createSyncEquipmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseSyncEquipmentType(SyncEquipmentType object) {
				return createSyncEquipmentTypeAdapter();
			}
			@Override
			public Adapter caseSyncMaterialClassType(SyncMaterialClassType object) {
				return createSyncMaterialClassTypeAdapter();
			}
			@Override
			public Adapter caseSyncMaterialDefinitionType(SyncMaterialDefinitionType object) {
				return createSyncMaterialDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseSyncMaterialInformationType(SyncMaterialInformationType object) {
				return createSyncMaterialInformationTypeAdapter();
			}
			@Override
			public Adapter caseSyncMaterialLotType(SyncMaterialLotType object) {
				return createSyncMaterialLotTypeAdapter();
			}
			@Override
			public Adapter caseSyncMaterialSubLotType(SyncMaterialSubLotType object) {
				return createSyncMaterialSubLotTypeAdapter();
			}
			@Override
			public Adapter caseSyncMaterialTestSpecType(SyncMaterialTestSpecType object) {
				return createSyncMaterialTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseSyncOperationsCapabilityInformationType(SyncOperationsCapabilityInformationType object) {
				return createSyncOperationsCapabilityInformationTypeAdapter();
			}
			@Override
			public Adapter caseSyncOperationsCapabilityType(SyncOperationsCapabilityType object) {
				return createSyncOperationsCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseSyncOperationsDefinitionInformationType(SyncOperationsDefinitionInformationType object) {
				return createSyncOperationsDefinitionInformationTypeAdapter();
			}
			@Override
			public Adapter caseSyncOperationsDefinitionType(SyncOperationsDefinitionType object) {
				return createSyncOperationsDefinitionTypeAdapter();
			}
			@Override
			public Adapter caseSyncOperationsPerformanceType(SyncOperationsPerformanceType object) {
				return createSyncOperationsPerformanceTypeAdapter();
			}
			@Override
			public Adapter caseSyncOperationsScheduleType(SyncOperationsScheduleType object) {
				return createSyncOperationsScheduleTypeAdapter();
			}
			@Override
			public Adapter caseSyncPersonnelClassType(SyncPersonnelClassType object) {
				return createSyncPersonnelClassTypeAdapter();
			}
			@Override
			public Adapter caseSyncPersonnelInformationType(SyncPersonnelInformationType object) {
				return createSyncPersonnelInformationTypeAdapter();
			}
			@Override
			public Adapter caseSyncPersonType(SyncPersonType object) {
				return createSyncPersonTypeAdapter();
			}
			@Override
			public Adapter caseSyncPhysicalAssetCapabilityTestSpecType(SyncPhysicalAssetCapabilityTestSpecType object) {
				return createSyncPhysicalAssetCapabilityTestSpecTypeAdapter();
			}
			@Override
			public Adapter caseSyncPhysicalAssetClassType(SyncPhysicalAssetClassType object) {
				return createSyncPhysicalAssetClassTypeAdapter();
			}
			@Override
			public Adapter caseSyncPhysicalAssetInformationType(SyncPhysicalAssetInformationType object) {
				return createSyncPhysicalAssetInformationTypeAdapter();
			}
			@Override
			public Adapter caseSyncPhysicalAssetType(SyncPhysicalAssetType object) {
				return createSyncPhysicalAssetTypeAdapter();
			}
			@Override
			public Adapter caseSyncProcessSegmentInformationType(SyncProcessSegmentInformationType object) {
				return createSyncProcessSegmentInformationTypeAdapter();
			}
			@Override
			public Adapter caseSyncProcessSegmentType(SyncProcessSegmentType object) {
				return createSyncProcessSegmentTypeAdapter();
			}
			@Override
			public Adapter caseSyncQualificationTestSpecificationType(SyncQualificationTestSpecificationType object) {
				return createSyncQualificationTestSpecificationTypeAdapter();
			}
			@Override
			public Adapter caseTestDateTimeType(TestDateTimeType object) {
				return createTestDateTimeTypeAdapter();
			}
			@Override
			public Adapter caseTestedEquipmentClassPropertyType(TestedEquipmentClassPropertyType object) {
				return createTestedEquipmentClassPropertyTypeAdapter();
			}
			@Override
			public Adapter caseTestedEquipmentPropertyType(TestedEquipmentPropertyType object) {
				return createTestedEquipmentPropertyTypeAdapter();
			}
			@Override
			public Adapter caseTestedMaterialClassPropertyType(TestedMaterialClassPropertyType object) {
				return createTestedMaterialClassPropertyTypeAdapter();
			}
			@Override
			public Adapter caseTestedMaterialDefinitionPropertyType(TestedMaterialDefinitionPropertyType object) {
				return createTestedMaterialDefinitionPropertyTypeAdapter();
			}
			@Override
			public Adapter caseTestedMaterialLotPropertyType(TestedMaterialLotPropertyType object) {
				return createTestedMaterialLotPropertyTypeAdapter();
			}
			@Override
			public Adapter caseTestedPersonnelClassPropertyType(TestedPersonnelClassPropertyType object) {
				return createTestedPersonnelClassPropertyTypeAdapter();
			}
			@Override
			public Adapter caseTestedPersonPropertyType(TestedPersonPropertyType object) {
				return createTestedPersonPropertyTypeAdapter();
			}
			@Override
			public Adapter caseTestedPhysicalAssetClassPropertyType(TestedPhysicalAssetClassPropertyType object) {
				return createTestedPhysicalAssetClassPropertyTypeAdapter();
			}
			@Override
			public Adapter caseTestedPhysicalAssetPropertyType(TestedPhysicalAssetPropertyType object) {
				return createTestedPhysicalAssetPropertyTypeAdapter();
			}
			@Override
			public Adapter caseTestResultType(TestResultType object) {
				return createTestResultTypeAdapter();
			}
			@Override
			public Adapter caseTextType(TextType object) {
				return createTextTypeAdapter();
			}
			@Override
			public Adapter caseTransAcknowledgeType(TransAcknowledgeType object) {
				return createTransAcknowledgeTypeAdapter();
			}
			@Override
			public Adapter caseTransActionCriteriaType(TransActionCriteriaType object) {
				return createTransActionCriteriaTypeAdapter();
			}
			@Override
			public Adapter caseTransApplicationAreaType(TransApplicationAreaType object) {
				return createTransApplicationAreaTypeAdapter();
			}
			@Override
			public Adapter caseTransCancelType(TransCancelType object) {
				return createTransCancelTypeAdapter();
			}
			@Override
			public Adapter caseTransChangeStatusType(TransChangeStatusType object) {
				return createTransChangeStatusTypeAdapter();
			}
			@Override
			public Adapter caseTransChangeType(TransChangeType object) {
				return createTransChangeTypeAdapter();
			}
			@Override
			public Adapter caseTransConfirmationCodeType(TransConfirmationCodeType object) {
				return createTransConfirmationCodeTypeAdapter();
			}
			@Override
			public Adapter caseTransConfirmType(TransConfirmType object) {
				return createTransConfirmTypeAdapter();
			}
			@Override
			public Adapter caseTransExpression1Type(TransExpression1Type object) {
				return createTransExpression1TypeAdapter();
			}
			@Override
			public Adapter caseTransExpressionType(TransExpressionType object) {
				return createTransExpressionTypeAdapter();
			}
			@Override
			public Adapter caseTransGetType(TransGetType object) {
				return createTransGetTypeAdapter();
			}
			@Override
			public Adapter caseTransProcessType(TransProcessType object) {
				return createTransProcessTypeAdapter();
			}
			@Override
			public Adapter caseTransReceiverType(TransReceiverType object) {
				return createTransReceiverTypeAdapter();
			}
			@Override
			public Adapter caseTransRespondType(TransRespondType object) {
				return createTransRespondTypeAdapter();
			}
			@Override
			public Adapter caseTransResponseCriteriaType(TransResponseCriteriaType object) {
				return createTransResponseCriteriaTypeAdapter();
			}
			@Override
			public Adapter caseTransSenderType(TransSenderType object) {
				return createTransSenderTypeAdapter();
			}
			@Override
			public Adapter caseTransShowType(TransShowType object) {
				return createTransShowTypeAdapter();
			}
			@Override
			public Adapter caseTransSignatureType(TransSignatureType object) {
				return createTransSignatureTypeAdapter();
			}
			@Override
			public Adapter caseTransStateChangeType(TransStateChangeType object) {
				return createTransStateChangeTypeAdapter();
			}
			@Override
			public Adapter caseTransSyncType(TransSyncType object) {
				return createTransSyncTypeAdapter();
			}
			@Override
			public Adapter caseTransUserAreaType(TransUserAreaType object) {
				return createTransUserAreaTypeAdapter();
			}
			@Override
			public Adapter caseUnitOfMeasureType(UnitOfMeasureType object) {
				return createUnitOfMeasureTypeAdapter();
			}
			@Override
			public Adapter caseValueStringType(ValueStringType object) {
				return createValueStringTypeAdapter();
			}
			@Override
			public Adapter caseValueType(ValueType object) {
				return createValueTypeAdapter();
			}
			@Override
			public Adapter caseVersionType(VersionType object) {
				return createVersionTypeAdapter();
			}
			@Override
			public Adapter caseWorkRequestIDType(WorkRequestIDType object) {
				return createWorkRequestIDTypeAdapter();
			}
			@Override
			public Adapter caseWorkScheduleIDType(WorkScheduleIDType object) {
				return createWorkScheduleIDTypeAdapter();
			}
			@Override
			public Adapter caseWorkType1Type(WorkType1Type object) {
				return createWorkType1TypeAdapter();
			}
			@Override
			public Adapter caseWorkTypeType(WorkTypeType object) {
				return createWorkTypeTypeAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeEquipmentCapabilityTestSpecType <em>Acknowledge Equipment Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeEquipmentCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createAcknowledgeEquipmentCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeEquipmentClassType <em>Acknowledge Equipment Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeEquipmentClassType
	 * @generated
	 */
	public Adapter createAcknowledgeEquipmentClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeEquipmentInformationType <em>Acknowledge Equipment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeEquipmentInformationType
	 * @generated
	 */
	public Adapter createAcknowledgeEquipmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeEquipmentType <em>Acknowledge Equipment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeEquipmentType
	 * @generated
	 */
	public Adapter createAcknowledgeEquipmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeMaterialClassType <em>Acknowledge Material Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeMaterialClassType
	 * @generated
	 */
	public Adapter createAcknowledgeMaterialClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeMaterialDefinitionType <em>Acknowledge Material Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeMaterialDefinitionType
	 * @generated
	 */
	public Adapter createAcknowledgeMaterialDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeMaterialInformationType <em>Acknowledge Material Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeMaterialInformationType
	 * @generated
	 */
	public Adapter createAcknowledgeMaterialInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeMaterialLotType <em>Acknowledge Material Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeMaterialLotType
	 * @generated
	 */
	public Adapter createAcknowledgeMaterialLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeMaterialSubLotType <em>Acknowledge Material Sub Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeMaterialSubLotType
	 * @generated
	 */
	public Adapter createAcknowledgeMaterialSubLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeMaterialTestSpecType <em>Acknowledge Material Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeMaterialTestSpecType
	 * @generated
	 */
	public Adapter createAcknowledgeMaterialTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeOperationsCapabilityInformationType <em>Acknowledge Operations Capability Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeOperationsCapabilityInformationType
	 * @generated
	 */
	public Adapter createAcknowledgeOperationsCapabilityInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeOperationsCapabilityType <em>Acknowledge Operations Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeOperationsCapabilityType
	 * @generated
	 */
	public Adapter createAcknowledgeOperationsCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeOperationsDefinitionInformationType <em>Acknowledge Operations Definition Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeOperationsDefinitionInformationType
	 * @generated
	 */
	public Adapter createAcknowledgeOperationsDefinitionInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeOperationsDefinitionType <em>Acknowledge Operations Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeOperationsDefinitionType
	 * @generated
	 */
	public Adapter createAcknowledgeOperationsDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeOperationsPerformanceType <em>Acknowledge Operations Performance Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeOperationsPerformanceType
	 * @generated
	 */
	public Adapter createAcknowledgeOperationsPerformanceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeOperationsScheduleType <em>Acknowledge Operations Schedule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeOperationsScheduleType
	 * @generated
	 */
	public Adapter createAcknowledgeOperationsScheduleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgePersonnelClassType <em>Acknowledge Personnel Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgePersonnelClassType
	 * @generated
	 */
	public Adapter createAcknowledgePersonnelClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgePersonnelInformationType <em>Acknowledge Personnel Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgePersonnelInformationType
	 * @generated
	 */
	public Adapter createAcknowledgePersonnelInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgePersonType <em>Acknowledge Person Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgePersonType
	 * @generated
	 */
	public Adapter createAcknowledgePersonTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgePhysicalAssetCapabilityTestSpecType <em>Acknowledge Physical Asset Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgePhysicalAssetCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createAcknowledgePhysicalAssetCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgePhysicalAssetClassType <em>Acknowledge Physical Asset Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgePhysicalAssetClassType
	 * @generated
	 */
	public Adapter createAcknowledgePhysicalAssetClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgePhysicalAssetInformationType <em>Acknowledge Physical Asset Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgePhysicalAssetInformationType
	 * @generated
	 */
	public Adapter createAcknowledgePhysicalAssetInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgePhysicalAssetType <em>Acknowledge Physical Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgePhysicalAssetType
	 * @generated
	 */
	public Adapter createAcknowledgePhysicalAssetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeProcessSegmentInformationType <em>Acknowledge Process Segment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeProcessSegmentInformationType
	 * @generated
	 */
	public Adapter createAcknowledgeProcessSegmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeProcessSegmentType <em>Acknowledge Process Segment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeProcessSegmentType
	 * @generated
	 */
	public Adapter createAcknowledgeProcessSegmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AcknowledgeQualificationTestSpecificationType <em>Acknowledge Qualification Test Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AcknowledgeQualificationTestSpecificationType
	 * @generated
	 */
	public Adapter createAcknowledgeQualificationTestSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ActualEndTimeType <em>Actual End Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ActualEndTimeType
	 * @generated
	 */
	public Adapter createActualEndTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ActualFinishTimeType <em>Actual Finish Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ActualFinishTimeType
	 * @generated
	 */
	public Adapter createActualFinishTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ActualStartTimeType <em>Actual Start Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ActualStartTimeType
	 * @generated
	 */
	public Adapter createActualStartTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AmountType <em>Amount Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AmountType
	 * @generated
	 */
	public Adapter createAmountTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType <em>Any Generic Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AnyGenericValueType
	 * @generated
	 */
	public Adapter createAnyGenericValueTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AssemblyRelationship1Type <em>Assembly Relationship1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AssemblyRelationship1Type
	 * @generated
	 */
	public Adapter createAssemblyRelationship1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AssemblyRelationshipType <em>Assembly Relationship Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AssemblyRelationshipType
	 * @generated
	 */
	public Adapter createAssemblyRelationshipTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AssemblyType1Type <em>Assembly Type1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AssemblyType1Type
	 * @generated
	 */
	public Adapter createAssemblyType1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.AssemblyTypeType <em>Assembly Type Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.AssemblyTypeType
	 * @generated
	 */
	public Adapter createAssemblyTypeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.BillOfMaterialIDType <em>Bill Of Material ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.BillOfMaterialIDType
	 * @generated
	 */
	public Adapter createBillOfMaterialIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.BillOfMaterialsIDType <em>Bill Of Materials ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.BillOfMaterialsIDType
	 * @generated
	 */
	public Adapter createBillOfMaterialsIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.BillOfResourcesIDType <em>Bill Of Resources ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.BillOfResourcesIDType
	 * @generated
	 */
	public Adapter createBillOfResourcesIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.BinaryObjectType <em>Binary Object Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.BinaryObjectType
	 * @generated
	 */
	public Adapter createBinaryObjectTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.BODType <em>BOD Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.BODType
	 * @generated
	 */
	public Adapter createBODTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelEquipmentCapabilityTestSpecType <em>Cancel Equipment Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelEquipmentCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createCancelEquipmentCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelEquipmentClassType <em>Cancel Equipment Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelEquipmentClassType
	 * @generated
	 */
	public Adapter createCancelEquipmentClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelEquipmentInformationType <em>Cancel Equipment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelEquipmentInformationType
	 * @generated
	 */
	public Adapter createCancelEquipmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelEquipmentType <em>Cancel Equipment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelEquipmentType
	 * @generated
	 */
	public Adapter createCancelEquipmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelMaterialClassType <em>Cancel Material Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelMaterialClassType
	 * @generated
	 */
	public Adapter createCancelMaterialClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelMaterialDefinitionType <em>Cancel Material Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelMaterialDefinitionType
	 * @generated
	 */
	public Adapter createCancelMaterialDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelMaterialInformationType <em>Cancel Material Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelMaterialInformationType
	 * @generated
	 */
	public Adapter createCancelMaterialInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelMaterialLotType <em>Cancel Material Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelMaterialLotType
	 * @generated
	 */
	public Adapter createCancelMaterialLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelMaterialSubLotType <em>Cancel Material Sub Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelMaterialSubLotType
	 * @generated
	 */
	public Adapter createCancelMaterialSubLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelMaterialTestSpecType <em>Cancel Material Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelMaterialTestSpecType
	 * @generated
	 */
	public Adapter createCancelMaterialTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelOperationsCapabilityInformationType <em>Cancel Operations Capability Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelOperationsCapabilityInformationType
	 * @generated
	 */
	public Adapter createCancelOperationsCapabilityInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelOperationsCapabilityType <em>Cancel Operations Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelOperationsCapabilityType
	 * @generated
	 */
	public Adapter createCancelOperationsCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelOperationsDefinitionInformationType <em>Cancel Operations Definition Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelOperationsDefinitionInformationType
	 * @generated
	 */
	public Adapter createCancelOperationsDefinitionInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelOperationsDefinitionType <em>Cancel Operations Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelOperationsDefinitionType
	 * @generated
	 */
	public Adapter createCancelOperationsDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelOperationsPerformanceType <em>Cancel Operations Performance Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelOperationsPerformanceType
	 * @generated
	 */
	public Adapter createCancelOperationsPerformanceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelOperationsScheduleType <em>Cancel Operations Schedule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelOperationsScheduleType
	 * @generated
	 */
	public Adapter createCancelOperationsScheduleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelPersonnelClassType <em>Cancel Personnel Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelPersonnelClassType
	 * @generated
	 */
	public Adapter createCancelPersonnelClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelPersonnelInformationType <em>Cancel Personnel Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelPersonnelInformationType
	 * @generated
	 */
	public Adapter createCancelPersonnelInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelPersonType <em>Cancel Person Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelPersonType
	 * @generated
	 */
	public Adapter createCancelPersonTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelPhysicalAssetCapabilityTestSpecType <em>Cancel Physical Asset Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelPhysicalAssetCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createCancelPhysicalAssetCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelPhysicalAssetClassType <em>Cancel Physical Asset Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelPhysicalAssetClassType
	 * @generated
	 */
	public Adapter createCancelPhysicalAssetClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelPhysicalAssetInformationType <em>Cancel Physical Asset Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelPhysicalAssetInformationType
	 * @generated
	 */
	public Adapter createCancelPhysicalAssetInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelPhysicalAssetType <em>Cancel Physical Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelPhysicalAssetType
	 * @generated
	 */
	public Adapter createCancelPhysicalAssetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelProcessSegmentInformationType <em>Cancel Process Segment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelProcessSegmentInformationType
	 * @generated
	 */
	public Adapter createCancelProcessSegmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelProcessSegmentType <em>Cancel Process Segment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelProcessSegmentType
	 * @generated
	 */
	public Adapter createCancelProcessSegmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CancelQualificationTestSpecificationType <em>Cancel Qualification Test Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CancelQualificationTestSpecificationType
	 * @generated
	 */
	public Adapter createCancelQualificationTestSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CapabilityType1Type <em>Capability Type1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CapabilityType1Type
	 * @generated
	 */
	public Adapter createCapabilityType1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CapabilityTypeType <em>Capability Type Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CapabilityTypeType
	 * @generated
	 */
	public Adapter createCapabilityTypeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CauseType <em>Cause Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CauseType
	 * @generated
	 */
	public Adapter createCauseTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CertificateOfAnalysisReferenceType <em>Certificate Of Analysis Reference Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CertificateOfAnalysisReferenceType
	 * @generated
	 */
	public Adapter createCertificateOfAnalysisReferenceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeEquipmentCapabilityTestSpecType <em>Change Equipment Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeEquipmentCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createChangeEquipmentCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeEquipmentClassType <em>Change Equipment Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeEquipmentClassType
	 * @generated
	 */
	public Adapter createChangeEquipmentClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeEquipmentInformationType <em>Change Equipment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeEquipmentInformationType
	 * @generated
	 */
	public Adapter createChangeEquipmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeEquipmentType <em>Change Equipment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeEquipmentType
	 * @generated
	 */
	public Adapter createChangeEquipmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeMaterialClassType <em>Change Material Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeMaterialClassType
	 * @generated
	 */
	public Adapter createChangeMaterialClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeMaterialDefinitionType <em>Change Material Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeMaterialDefinitionType
	 * @generated
	 */
	public Adapter createChangeMaterialDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeMaterialInformationType <em>Change Material Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeMaterialInformationType
	 * @generated
	 */
	public Adapter createChangeMaterialInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeMaterialLotType <em>Change Material Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeMaterialLotType
	 * @generated
	 */
	public Adapter createChangeMaterialLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeMaterialSubLotType <em>Change Material Sub Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeMaterialSubLotType
	 * @generated
	 */
	public Adapter createChangeMaterialSubLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeMaterialTestSpecType <em>Change Material Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeMaterialTestSpecType
	 * @generated
	 */
	public Adapter createChangeMaterialTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeOperationsCapabilityInformationType <em>Change Operations Capability Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeOperationsCapabilityInformationType
	 * @generated
	 */
	public Adapter createChangeOperationsCapabilityInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeOperationsCapabilityType <em>Change Operations Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeOperationsCapabilityType
	 * @generated
	 */
	public Adapter createChangeOperationsCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeOperationsDefinitionInformationType <em>Change Operations Definition Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeOperationsDefinitionInformationType
	 * @generated
	 */
	public Adapter createChangeOperationsDefinitionInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeOperationsDefinitionType <em>Change Operations Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeOperationsDefinitionType
	 * @generated
	 */
	public Adapter createChangeOperationsDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeOperationsPerformanceType <em>Change Operations Performance Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeOperationsPerformanceType
	 * @generated
	 */
	public Adapter createChangeOperationsPerformanceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeOperationsScheduleType <em>Change Operations Schedule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeOperationsScheduleType
	 * @generated
	 */
	public Adapter createChangeOperationsScheduleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangePersonnelClassType <em>Change Personnel Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangePersonnelClassType
	 * @generated
	 */
	public Adapter createChangePersonnelClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangePersonnelInformationType <em>Change Personnel Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangePersonnelInformationType
	 * @generated
	 */
	public Adapter createChangePersonnelInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangePersonType <em>Change Person Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangePersonType
	 * @generated
	 */
	public Adapter createChangePersonTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangePhysicalAssetCapabilityTestSpecType <em>Change Physical Asset Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangePhysicalAssetCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createChangePhysicalAssetCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangePhysicalAssetClassType <em>Change Physical Asset Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangePhysicalAssetClassType
	 * @generated
	 */
	public Adapter createChangePhysicalAssetClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangePhysicalAssetInformationType <em>Change Physical Asset Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangePhysicalAssetInformationType
	 * @generated
	 */
	public Adapter createChangePhysicalAssetInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangePhysicalAssetType <em>Change Physical Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangePhysicalAssetType
	 * @generated
	 */
	public Adapter createChangePhysicalAssetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeProcessSegmentInformationType <em>Change Process Segment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeProcessSegmentInformationType
	 * @generated
	 */
	public Adapter createChangeProcessSegmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeProcessSegmentType <em>Change Process Segment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeProcessSegmentType
	 * @generated
	 */
	public Adapter createChangeProcessSegmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ChangeQualificationTestSpecificationType <em>Change Qualification Test Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ChangeQualificationTestSpecificationType
	 * @generated
	 */
	public Adapter createChangeQualificationTestSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CodeType <em>Code Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CodeType
	 * @generated
	 */
	public Adapter createCodeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ConfidenceFactorType <em>Confidence Factor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ConfidenceFactorType
	 * @generated
	 */
	public Adapter createConfidenceFactorTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ConfirmBODType <em>Confirm BOD Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ConfirmBODType
	 * @generated
	 */
	public Adapter createConfirmBODTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.CorrectionType <em>Correction Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.CorrectionType
	 * @generated
	 */
	public Adapter createCorrectionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType <em>Data Area Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType
	 * @generated
	 */
	public Adapter createDataAreaTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType1 <em>Data Area Type1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType1
	 * @generated
	 */
	public Adapter createDataAreaType1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType2 <em>Data Area Type2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType2
	 * @generated
	 */
	public Adapter createDataAreaType2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType3 <em>Data Area Type3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType3
	 * @generated
	 */
	public Adapter createDataAreaType3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType4 <em>Data Area Type4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType4
	 * @generated
	 */
	public Adapter createDataAreaType4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType5 <em>Data Area Type5</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType5
	 * @generated
	 */
	public Adapter createDataAreaType5Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType6 <em>Data Area Type6</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType6
	 * @generated
	 */
	public Adapter createDataAreaType6Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType7 <em>Data Area Type7</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType7
	 * @generated
	 */
	public Adapter createDataAreaType7Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType8 <em>Data Area Type8</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType8
	 * @generated
	 */
	public Adapter createDataAreaType8Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType9 <em>Data Area Type9</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType9
	 * @generated
	 */
	public Adapter createDataAreaType9Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType10 <em>Data Area Type10</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType10
	 * @generated
	 */
	public Adapter createDataAreaType10Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType11 <em>Data Area Type11</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType11
	 * @generated
	 */
	public Adapter createDataAreaType11Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType12 <em>Data Area Type12</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType12
	 * @generated
	 */
	public Adapter createDataAreaType12Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType13 <em>Data Area Type13</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType13
	 * @generated
	 */
	public Adapter createDataAreaType13Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType14 <em>Data Area Type14</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType14
	 * @generated
	 */
	public Adapter createDataAreaType14Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType15 <em>Data Area Type15</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType15
	 * @generated
	 */
	public Adapter createDataAreaType15Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType16 <em>Data Area Type16</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType16
	 * @generated
	 */
	public Adapter createDataAreaType16Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType17 <em>Data Area Type17</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType17
	 * @generated
	 */
	public Adapter createDataAreaType17Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType18 <em>Data Area Type18</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType18
	 * @generated
	 */
	public Adapter createDataAreaType18Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType19 <em>Data Area Type19</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType19
	 * @generated
	 */
	public Adapter createDataAreaType19Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType20 <em>Data Area Type20</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType20
	 * @generated
	 */
	public Adapter createDataAreaType20Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType21 <em>Data Area Type21</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType21
	 * @generated
	 */
	public Adapter createDataAreaType21Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType22 <em>Data Area Type22</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType22
	 * @generated
	 */
	public Adapter createDataAreaType22Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType23 <em>Data Area Type23</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType23
	 * @generated
	 */
	public Adapter createDataAreaType23Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType24 <em>Data Area Type24</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType24
	 * @generated
	 */
	public Adapter createDataAreaType24Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType25 <em>Data Area Type25</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType25
	 * @generated
	 */
	public Adapter createDataAreaType25Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType26 <em>Data Area Type26</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType26
	 * @generated
	 */
	public Adapter createDataAreaType26Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType27 <em>Data Area Type27</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType27
	 * @generated
	 */
	public Adapter createDataAreaType27Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType28 <em>Data Area Type28</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType28
	 * @generated
	 */
	public Adapter createDataAreaType28Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType29 <em>Data Area Type29</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType29
	 * @generated
	 */
	public Adapter createDataAreaType29Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType30 <em>Data Area Type30</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType30
	 * @generated
	 */
	public Adapter createDataAreaType30Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType31 <em>Data Area Type31</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType31
	 * @generated
	 */
	public Adapter createDataAreaType31Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType32 <em>Data Area Type32</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType32
	 * @generated
	 */
	public Adapter createDataAreaType32Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType33 <em>Data Area Type33</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType33
	 * @generated
	 */
	public Adapter createDataAreaType33Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType34 <em>Data Area Type34</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType34
	 * @generated
	 */
	public Adapter createDataAreaType34Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType35 <em>Data Area Type35</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType35
	 * @generated
	 */
	public Adapter createDataAreaType35Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType36 <em>Data Area Type36</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType36
	 * @generated
	 */
	public Adapter createDataAreaType36Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType37 <em>Data Area Type37</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType37
	 * @generated
	 */
	public Adapter createDataAreaType37Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType38 <em>Data Area Type38</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType38
	 * @generated
	 */
	public Adapter createDataAreaType38Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType39 <em>Data Area Type39</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType39
	 * @generated
	 */
	public Adapter createDataAreaType39Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType40 <em>Data Area Type40</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType40
	 * @generated
	 */
	public Adapter createDataAreaType40Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType41 <em>Data Area Type41</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType41
	 * @generated
	 */
	public Adapter createDataAreaType41Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType42 <em>Data Area Type42</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType42
	 * @generated
	 */
	public Adapter createDataAreaType42Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType43 <em>Data Area Type43</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType43
	 * @generated
	 */
	public Adapter createDataAreaType43Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType44 <em>Data Area Type44</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType44
	 * @generated
	 */
	public Adapter createDataAreaType44Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType45 <em>Data Area Type45</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType45
	 * @generated
	 */
	public Adapter createDataAreaType45Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType46 <em>Data Area Type46</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType46
	 * @generated
	 */
	public Adapter createDataAreaType46Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType47 <em>Data Area Type47</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType47
	 * @generated
	 */
	public Adapter createDataAreaType47Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType48 <em>Data Area Type48</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType48
	 * @generated
	 */
	public Adapter createDataAreaType48Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType49 <em>Data Area Type49</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType49
	 * @generated
	 */
	public Adapter createDataAreaType49Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType50 <em>Data Area Type50</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType50
	 * @generated
	 */
	public Adapter createDataAreaType50Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType51 <em>Data Area Type51</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType51
	 * @generated
	 */
	public Adapter createDataAreaType51Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType52 <em>Data Area Type52</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType52
	 * @generated
	 */
	public Adapter createDataAreaType52Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType53 <em>Data Area Type53</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType53
	 * @generated
	 */
	public Adapter createDataAreaType53Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType54 <em>Data Area Type54</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType54
	 * @generated
	 */
	public Adapter createDataAreaType54Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType55 <em>Data Area Type55</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType55
	 * @generated
	 */
	public Adapter createDataAreaType55Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType56 <em>Data Area Type56</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType56
	 * @generated
	 */
	public Adapter createDataAreaType56Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType57 <em>Data Area Type57</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType57
	 * @generated
	 */
	public Adapter createDataAreaType57Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType58 <em>Data Area Type58</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType58
	 * @generated
	 */
	public Adapter createDataAreaType58Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType59 <em>Data Area Type59</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType59
	 * @generated
	 */
	public Adapter createDataAreaType59Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType60 <em>Data Area Type60</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType60
	 * @generated
	 */
	public Adapter createDataAreaType60Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType61 <em>Data Area Type61</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType61
	 * @generated
	 */
	public Adapter createDataAreaType61Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType62 <em>Data Area Type62</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType62
	 * @generated
	 */
	public Adapter createDataAreaType62Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType63 <em>Data Area Type63</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType63
	 * @generated
	 */
	public Adapter createDataAreaType63Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType64 <em>Data Area Type64</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType64
	 * @generated
	 */
	public Adapter createDataAreaType64Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType65 <em>Data Area Type65</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType65
	 * @generated
	 */
	public Adapter createDataAreaType65Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType66 <em>Data Area Type66</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType66
	 * @generated
	 */
	public Adapter createDataAreaType66Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType67 <em>Data Area Type67</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType67
	 * @generated
	 */
	public Adapter createDataAreaType67Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType68 <em>Data Area Type68</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType68
	 * @generated
	 */
	public Adapter createDataAreaType68Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType69 <em>Data Area Type69</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType69
	 * @generated
	 */
	public Adapter createDataAreaType69Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType70 <em>Data Area Type70</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType70
	 * @generated
	 */
	public Adapter createDataAreaType70Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType71 <em>Data Area Type71</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType71
	 * @generated
	 */
	public Adapter createDataAreaType71Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType72 <em>Data Area Type72</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType72
	 * @generated
	 */
	public Adapter createDataAreaType72Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType73 <em>Data Area Type73</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType73
	 * @generated
	 */
	public Adapter createDataAreaType73Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType74 <em>Data Area Type74</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType74
	 * @generated
	 */
	public Adapter createDataAreaType74Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType75 <em>Data Area Type75</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType75
	 * @generated
	 */
	public Adapter createDataAreaType75Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType76 <em>Data Area Type76</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType76
	 * @generated
	 */
	public Adapter createDataAreaType76Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType77 <em>Data Area Type77</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType77
	 * @generated
	 */
	public Adapter createDataAreaType77Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType78 <em>Data Area Type78</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType78
	 * @generated
	 */
	public Adapter createDataAreaType78Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType79 <em>Data Area Type79</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType79
	 * @generated
	 */
	public Adapter createDataAreaType79Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType80 <em>Data Area Type80</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType80
	 * @generated
	 */
	public Adapter createDataAreaType80Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType81 <em>Data Area Type81</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType81
	 * @generated
	 */
	public Adapter createDataAreaType81Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType82 <em>Data Area Type82</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType82
	 * @generated
	 */
	public Adapter createDataAreaType82Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType83 <em>Data Area Type83</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType83
	 * @generated
	 */
	public Adapter createDataAreaType83Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType84 <em>Data Area Type84</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType84
	 * @generated
	 */
	public Adapter createDataAreaType84Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType85 <em>Data Area Type85</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType85
	 * @generated
	 */
	public Adapter createDataAreaType85Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType86 <em>Data Area Type86</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType86
	 * @generated
	 */
	public Adapter createDataAreaType86Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType87 <em>Data Area Type87</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType87
	 * @generated
	 */
	public Adapter createDataAreaType87Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType88 <em>Data Area Type88</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType88
	 * @generated
	 */
	public Adapter createDataAreaType88Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType89 <em>Data Area Type89</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType89
	 * @generated
	 */
	public Adapter createDataAreaType89Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType90 <em>Data Area Type90</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType90
	 * @generated
	 */
	public Adapter createDataAreaType90Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType91 <em>Data Area Type91</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType91
	 * @generated
	 */
	public Adapter createDataAreaType91Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType92 <em>Data Area Type92</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType92
	 * @generated
	 */
	public Adapter createDataAreaType92Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType93 <em>Data Area Type93</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType93
	 * @generated
	 */
	public Adapter createDataAreaType93Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType94 <em>Data Area Type94</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType94
	 * @generated
	 */
	public Adapter createDataAreaType94Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType95 <em>Data Area Type95</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType95
	 * @generated
	 */
	public Adapter createDataAreaType95Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType96 <em>Data Area Type96</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType96
	 * @generated
	 */
	public Adapter createDataAreaType96Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType97 <em>Data Area Type97</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType97
	 * @generated
	 */
	public Adapter createDataAreaType97Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType98 <em>Data Area Type98</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType98
	 * @generated
	 */
	public Adapter createDataAreaType98Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType99 <em>Data Area Type99</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType99
	 * @generated
	 */
	public Adapter createDataAreaType99Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType100 <em>Data Area Type100</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType100
	 * @generated
	 */
	public Adapter createDataAreaType100Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType101 <em>Data Area Type101</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType101
	 * @generated
	 */
	public Adapter createDataAreaType101Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType102 <em>Data Area Type102</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType102
	 * @generated
	 */
	public Adapter createDataAreaType102Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType103 <em>Data Area Type103</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType103
	 * @generated
	 */
	public Adapter createDataAreaType103Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType104 <em>Data Area Type104</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType104
	 * @generated
	 */
	public Adapter createDataAreaType104Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType105 <em>Data Area Type105</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType105
	 * @generated
	 */
	public Adapter createDataAreaType105Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType106 <em>Data Area Type106</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType106
	 * @generated
	 */
	public Adapter createDataAreaType106Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType107 <em>Data Area Type107</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType107
	 * @generated
	 */
	public Adapter createDataAreaType107Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType108 <em>Data Area Type108</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType108
	 * @generated
	 */
	public Adapter createDataAreaType108Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType109 <em>Data Area Type109</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType109
	 * @generated
	 */
	public Adapter createDataAreaType109Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType110 <em>Data Area Type110</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType110
	 * @generated
	 */
	public Adapter createDataAreaType110Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType111 <em>Data Area Type111</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType111
	 * @generated
	 */
	public Adapter createDataAreaType111Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType112 <em>Data Area Type112</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType112
	 * @generated
	 */
	public Adapter createDataAreaType112Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType113 <em>Data Area Type113</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType113
	 * @generated
	 */
	public Adapter createDataAreaType113Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType114 <em>Data Area Type114</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType114
	 * @generated
	 */
	public Adapter createDataAreaType114Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType115 <em>Data Area Type115</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType115
	 * @generated
	 */
	public Adapter createDataAreaType115Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType116 <em>Data Area Type116</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType116
	 * @generated
	 */
	public Adapter createDataAreaType116Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType117 <em>Data Area Type117</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType117
	 * @generated
	 */
	public Adapter createDataAreaType117Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType118 <em>Data Area Type118</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType118
	 * @generated
	 */
	public Adapter createDataAreaType118Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType119 <em>Data Area Type119</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType119
	 * @generated
	 */
	public Adapter createDataAreaType119Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType120 <em>Data Area Type120</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType120
	 * @generated
	 */
	public Adapter createDataAreaType120Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType121 <em>Data Area Type121</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType121
	 * @generated
	 */
	public Adapter createDataAreaType121Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType122 <em>Data Area Type122</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType122
	 * @generated
	 */
	public Adapter createDataAreaType122Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType123 <em>Data Area Type123</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType123
	 * @generated
	 */
	public Adapter createDataAreaType123Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType124 <em>Data Area Type124</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType124
	 * @generated
	 */
	public Adapter createDataAreaType124Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType125 <em>Data Area Type125</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType125
	 * @generated
	 */
	public Adapter createDataAreaType125Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType126 <em>Data Area Type126</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType126
	 * @generated
	 */
	public Adapter createDataAreaType126Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType127 <em>Data Area Type127</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType127
	 * @generated
	 */
	public Adapter createDataAreaType127Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType128 <em>Data Area Type128</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType128
	 * @generated
	 */
	public Adapter createDataAreaType128Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType129 <em>Data Area Type129</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType129
	 * @generated
	 */
	public Adapter createDataAreaType129Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType130 <em>Data Area Type130</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType130
	 * @generated
	 */
	public Adapter createDataAreaType130Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType131 <em>Data Area Type131</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType131
	 * @generated
	 */
	public Adapter createDataAreaType131Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType132 <em>Data Area Type132</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType132
	 * @generated
	 */
	public Adapter createDataAreaType132Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType133 <em>Data Area Type133</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType133
	 * @generated
	 */
	public Adapter createDataAreaType133Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType134 <em>Data Area Type134</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType134
	 * @generated
	 */
	public Adapter createDataAreaType134Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType135 <em>Data Area Type135</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType135
	 * @generated
	 */
	public Adapter createDataAreaType135Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType136 <em>Data Area Type136</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType136
	 * @generated
	 */
	public Adapter createDataAreaType136Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType137 <em>Data Area Type137</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType137
	 * @generated
	 */
	public Adapter createDataAreaType137Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType138 <em>Data Area Type138</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType138
	 * @generated
	 */
	public Adapter createDataAreaType138Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType139 <em>Data Area Type139</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType139
	 * @generated
	 */
	public Adapter createDataAreaType139Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType140 <em>Data Area Type140</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType140
	 * @generated
	 */
	public Adapter createDataAreaType140Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType141 <em>Data Area Type141</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType141
	 * @generated
	 */
	public Adapter createDataAreaType141Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType142 <em>Data Area Type142</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType142
	 * @generated
	 */
	public Adapter createDataAreaType142Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType143 <em>Data Area Type143</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType143
	 * @generated
	 */
	public Adapter createDataAreaType143Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType144 <em>Data Area Type144</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType144
	 * @generated
	 */
	public Adapter createDataAreaType144Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType145 <em>Data Area Type145</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType145
	 * @generated
	 */
	public Adapter createDataAreaType145Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType146 <em>Data Area Type146</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType146
	 * @generated
	 */
	public Adapter createDataAreaType146Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType147 <em>Data Area Type147</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType147
	 * @generated
	 */
	public Adapter createDataAreaType147Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType148 <em>Data Area Type148</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType148
	 * @generated
	 */
	public Adapter createDataAreaType148Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType149 <em>Data Area Type149</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType149
	 * @generated
	 */
	public Adapter createDataAreaType149Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType150 <em>Data Area Type150</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType150
	 * @generated
	 */
	public Adapter createDataAreaType150Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType151 <em>Data Area Type151</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType151
	 * @generated
	 */
	public Adapter createDataAreaType151Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType152 <em>Data Area Type152</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType152
	 * @generated
	 */
	public Adapter createDataAreaType152Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType153 <em>Data Area Type153</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType153
	 * @generated
	 */
	public Adapter createDataAreaType153Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType154 <em>Data Area Type154</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType154
	 * @generated
	 */
	public Adapter createDataAreaType154Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType155 <em>Data Area Type155</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType155
	 * @generated
	 */
	public Adapter createDataAreaType155Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType156 <em>Data Area Type156</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType156
	 * @generated
	 */
	public Adapter createDataAreaType156Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType157 <em>Data Area Type157</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType157
	 * @generated
	 */
	public Adapter createDataAreaType157Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType158 <em>Data Area Type158</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType158
	 * @generated
	 */
	public Adapter createDataAreaType158Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType159 <em>Data Area Type159</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType159
	 * @generated
	 */
	public Adapter createDataAreaType159Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType160 <em>Data Area Type160</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType160
	 * @generated
	 */
	public Adapter createDataAreaType160Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType161 <em>Data Area Type161</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType161
	 * @generated
	 */
	public Adapter createDataAreaType161Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType162 <em>Data Area Type162</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType162
	 * @generated
	 */
	public Adapter createDataAreaType162Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType163 <em>Data Area Type163</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType163
	 * @generated
	 */
	public Adapter createDataAreaType163Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType164 <em>Data Area Type164</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType164
	 * @generated
	 */
	public Adapter createDataAreaType164Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType165 <em>Data Area Type165</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType165
	 * @generated
	 */
	public Adapter createDataAreaType165Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType166 <em>Data Area Type166</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType166
	 * @generated
	 */
	public Adapter createDataAreaType166Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType167 <em>Data Area Type167</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType167
	 * @generated
	 */
	public Adapter createDataAreaType167Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType168 <em>Data Area Type168</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType168
	 * @generated
	 */
	public Adapter createDataAreaType168Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType169 <em>Data Area Type169</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType169
	 * @generated
	 */
	public Adapter createDataAreaType169Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType170 <em>Data Area Type170</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType170
	 * @generated
	 */
	public Adapter createDataAreaType170Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType171 <em>Data Area Type171</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType171
	 * @generated
	 */
	public Adapter createDataAreaType171Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType172 <em>Data Area Type172</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType172
	 * @generated
	 */
	public Adapter createDataAreaType172Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType173 <em>Data Area Type173</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType173
	 * @generated
	 */
	public Adapter createDataAreaType173Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType174 <em>Data Area Type174</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType174
	 * @generated
	 */
	public Adapter createDataAreaType174Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType175 <em>Data Area Type175</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType175
	 * @generated
	 */
	public Adapter createDataAreaType175Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType176 <em>Data Area Type176</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType176
	 * @generated
	 */
	public Adapter createDataAreaType176Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType177 <em>Data Area Type177</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType177
	 * @generated
	 */
	public Adapter createDataAreaType177Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType178 <em>Data Area Type178</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType178
	 * @generated
	 */
	public Adapter createDataAreaType178Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType179 <em>Data Area Type179</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType179
	 * @generated
	 */
	public Adapter createDataAreaType179Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType180 <em>Data Area Type180</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType180
	 * @generated
	 */
	public Adapter createDataAreaType180Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType181 <em>Data Area Type181</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType181
	 * @generated
	 */
	public Adapter createDataAreaType181Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType182 <em>Data Area Type182</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType182
	 * @generated
	 */
	public Adapter createDataAreaType182Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType183 <em>Data Area Type183</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType183
	 * @generated
	 */
	public Adapter createDataAreaType183Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType184 <em>Data Area Type184</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType184
	 * @generated
	 */
	public Adapter createDataAreaType184Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType185 <em>Data Area Type185</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType185
	 * @generated
	 */
	public Adapter createDataAreaType185Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType186 <em>Data Area Type186</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType186
	 * @generated
	 */
	public Adapter createDataAreaType186Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType187 <em>Data Area Type187</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType187
	 * @generated
	 */
	public Adapter createDataAreaType187Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType188 <em>Data Area Type188</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType188
	 * @generated
	 */
	public Adapter createDataAreaType188Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType189 <em>Data Area Type189</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType189
	 * @generated
	 */
	public Adapter createDataAreaType189Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType190 <em>Data Area Type190</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType190
	 * @generated
	 */
	public Adapter createDataAreaType190Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType191 <em>Data Area Type191</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType191
	 * @generated
	 */
	public Adapter createDataAreaType191Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType192 <em>Data Area Type192</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType192
	 * @generated
	 */
	public Adapter createDataAreaType192Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType193 <em>Data Area Type193</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType193
	 * @generated
	 */
	public Adapter createDataAreaType193Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType194 <em>Data Area Type194</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType194
	 * @generated
	 */
	public Adapter createDataAreaType194Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType195 <em>Data Area Type195</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType195
	 * @generated
	 */
	public Adapter createDataAreaType195Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType196 <em>Data Area Type196</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType196
	 * @generated
	 */
	public Adapter createDataAreaType196Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType197 <em>Data Area Type197</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType197
	 * @generated
	 */
	public Adapter createDataAreaType197Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType198 <em>Data Area Type198</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType198
	 * @generated
	 */
	public Adapter createDataAreaType198Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType199 <em>Data Area Type199</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType199
	 * @generated
	 */
	public Adapter createDataAreaType199Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType200 <em>Data Area Type200</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType200
	 * @generated
	 */
	public Adapter createDataAreaType200Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType201 <em>Data Area Type201</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType201
	 * @generated
	 */
	public Adapter createDataAreaType201Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType202 <em>Data Area Type202</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType202
	 * @generated
	 */
	public Adapter createDataAreaType202Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType203 <em>Data Area Type203</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType203
	 * @generated
	 */
	public Adapter createDataAreaType203Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType204 <em>Data Area Type204</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType204
	 * @generated
	 */
	public Adapter createDataAreaType204Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType205 <em>Data Area Type205</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType205
	 * @generated
	 */
	public Adapter createDataAreaType205Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType206 <em>Data Area Type206</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType206
	 * @generated
	 */
	public Adapter createDataAreaType206Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType207 <em>Data Area Type207</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType207
	 * @generated
	 */
	public Adapter createDataAreaType207Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType208 <em>Data Area Type208</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataAreaType208
	 * @generated
	 */
	public Adapter createDataAreaType208Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataType1Type <em>Data Type1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataType1Type
	 * @generated
	 */
	public Adapter createDataType1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DataTypeType <em>Data Type Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DataTypeType
	 * @generated
	 */
	public Adapter createDataTypeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DateTimeType <em>Date Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DateTimeType
	 * @generated
	 */
	public Adapter createDateTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.Dependency1Type <em>Dependency1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.Dependency1Type
	 * @generated
	 */
	public Adapter createDependency1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DependencyType <em>Dependency Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DependencyType
	 * @generated
	 */
	public Adapter createDependencyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DescriptionType <em>Description Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DescriptionType
	 * @generated
	 */
	public Adapter createDescriptionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.DocumentRoot
	 * @generated
	 */
	public Adapter createDocumentRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EarliestStartTimeType <em>Earliest Start Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EarliestStartTimeType
	 * @generated
	 */
	public Adapter createEarliestStartTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EndTimeType <em>End Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EndTimeType
	 * @generated
	 */
	public Adapter createEndTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentAssetMappingType <em>Equipment Asset Mapping Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentAssetMappingType
	 * @generated
	 */
	public Adapter createEquipmentAssetMappingTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentCapabilityTestSpecificationIDType <em>Equipment Capability Test Specification ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentCapabilityTestSpecificationIDType
	 * @generated
	 */
	public Adapter createEquipmentCapabilityTestSpecificationIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentCapabilityTestSpecificationType <em>Equipment Capability Test Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentCapabilityTestSpecificationType
	 * @generated
	 */
	public Adapter createEquipmentCapabilityTestSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentClassIDType <em>Equipment Class ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentClassIDType
	 * @generated
	 */
	public Adapter createEquipmentClassIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentClassPropertyType <em>Equipment Class Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentClassPropertyType
	 * @generated
	 */
	public Adapter createEquipmentClassPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentClassType <em>Equipment Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentClassType
	 * @generated
	 */
	public Adapter createEquipmentClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentElementLevel1Type <em>Equipment Element Level1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentElementLevel1Type
	 * @generated
	 */
	public Adapter createEquipmentElementLevel1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentElementLevelType <em>Equipment Element Level Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentElementLevelType
	 * @generated
	 */
	public Adapter createEquipmentElementLevelTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentIDType <em>Equipment ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentIDType
	 * @generated
	 */
	public Adapter createEquipmentIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentInformationType <em>Equipment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentInformationType
	 * @generated
	 */
	public Adapter createEquipmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentPropertyType <em>Equipment Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentPropertyType
	 * @generated
	 */
	public Adapter createEquipmentPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationPropertyType <em>Equipment Segment Specification Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationPropertyType
	 * @generated
	 */
	public Adapter createEquipmentSegmentSpecificationPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType <em>Equipment Segment Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType
	 * @generated
	 */
	public Adapter createEquipmentSegmentSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentType <em>Equipment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentType
	 * @generated
	 */
	public Adapter createEquipmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.EquipmentUseType <em>Equipment Use Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.EquipmentUseType
	 * @generated
	 */
	public Adapter createEquipmentUseTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ExpirationTimeType <em>Expiration Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ExpirationTimeType
	 * @generated
	 */
	public Adapter createExpirationTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetEquipmentCapabilityTestSpecType <em>Get Equipment Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetEquipmentCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createGetEquipmentCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetEquipmentClassType <em>Get Equipment Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetEquipmentClassType
	 * @generated
	 */
	public Adapter createGetEquipmentClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetEquipmentInformationType <em>Get Equipment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetEquipmentInformationType
	 * @generated
	 */
	public Adapter createGetEquipmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetEquipmentType <em>Get Equipment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetEquipmentType
	 * @generated
	 */
	public Adapter createGetEquipmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetMaterialClassType <em>Get Material Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetMaterialClassType
	 * @generated
	 */
	public Adapter createGetMaterialClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetMaterialDefinitionType <em>Get Material Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetMaterialDefinitionType
	 * @generated
	 */
	public Adapter createGetMaterialDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetMaterialInformationType <em>Get Material Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetMaterialInformationType
	 * @generated
	 */
	public Adapter createGetMaterialInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetMaterialLotType <em>Get Material Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetMaterialLotType
	 * @generated
	 */
	public Adapter createGetMaterialLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetMaterialSubLotType <em>Get Material Sub Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetMaterialSubLotType
	 * @generated
	 */
	public Adapter createGetMaterialSubLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetMaterialTestSpecType <em>Get Material Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetMaterialTestSpecType
	 * @generated
	 */
	public Adapter createGetMaterialTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetOperationsCapabilityInformationType <em>Get Operations Capability Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetOperationsCapabilityInformationType
	 * @generated
	 */
	public Adapter createGetOperationsCapabilityInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetOperationsCapabilityType <em>Get Operations Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetOperationsCapabilityType
	 * @generated
	 */
	public Adapter createGetOperationsCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetOperationsDefinitionInformationType <em>Get Operations Definition Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetOperationsDefinitionInformationType
	 * @generated
	 */
	public Adapter createGetOperationsDefinitionInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetOperationsDefinitionType <em>Get Operations Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetOperationsDefinitionType
	 * @generated
	 */
	public Adapter createGetOperationsDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetOperationsPerformanceType <em>Get Operations Performance Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetOperationsPerformanceType
	 * @generated
	 */
	public Adapter createGetOperationsPerformanceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetOperationsScheduleType <em>Get Operations Schedule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetOperationsScheduleType
	 * @generated
	 */
	public Adapter createGetOperationsScheduleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetPersonnelClassType <em>Get Personnel Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetPersonnelClassType
	 * @generated
	 */
	public Adapter createGetPersonnelClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetPersonnelInformationType <em>Get Personnel Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetPersonnelInformationType
	 * @generated
	 */
	public Adapter createGetPersonnelInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetPersonType <em>Get Person Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetPersonType
	 * @generated
	 */
	public Adapter createGetPersonTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetPhysicalAssetCapabilityTestSpecType <em>Get Physical Asset Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetPhysicalAssetCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createGetPhysicalAssetCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetPhysicalAssetClassType <em>Get Physical Asset Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetPhysicalAssetClassType
	 * @generated
	 */
	public Adapter createGetPhysicalAssetClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetPhysicalAssetInformationType <em>Get Physical Asset Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetPhysicalAssetInformationType
	 * @generated
	 */
	public Adapter createGetPhysicalAssetInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetPhysicalAssetType <em>Get Physical Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetPhysicalAssetType
	 * @generated
	 */
	public Adapter createGetPhysicalAssetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetProcessSegmentInformationType <em>Get Process Segment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetProcessSegmentInformationType
	 * @generated
	 */
	public Adapter createGetProcessSegmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetProcessSegmentType <em>Get Process Segment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetProcessSegmentType
	 * @generated
	 */
	public Adapter createGetProcessSegmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.GetQualificationTestSpecificationType <em>Get Qualification Test Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.GetQualificationTestSpecificationType
	 * @generated
	 */
	public Adapter createGetQualificationTestSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.HierarchyScopeType <em>Hierarchy Scope Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.HierarchyScopeType
	 * @generated
	 */
	public Adapter createHierarchyScopeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.IdentifierType <em>Identifier Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.IdentifierType
	 * @generated
	 */
	public Adapter createIdentifierTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.JobOrderCommand1Type <em>Job Order Command1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.JobOrderCommand1Type
	 * @generated
	 */
	public Adapter createJobOrderCommand1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.JobOrderCommandRuleType <em>Job Order Command Rule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.JobOrderCommandRuleType
	 * @generated
	 */
	public Adapter createJobOrderCommandRuleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.JobOrderCommandType <em>Job Order Command Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.JobOrderCommandType
	 * @generated
	 */
	public Adapter createJobOrderCommandTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.JobOrderDispatchStatusType <em>Job Order Dispatch Status Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.JobOrderDispatchStatusType
	 * @generated
	 */
	public Adapter createJobOrderDispatchStatusTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.LatestEndTimeType <em>Latest End Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.LatestEndTimeType
	 * @generated
	 */
	public Adapter createLatestEndTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.LocationType <em>Location Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.LocationType
	 * @generated
	 */
	public Adapter createLocationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ManufacturingBillIDType <em>Manufacturing Bill ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ManufacturingBillIDType
	 * @generated
	 */
	public Adapter createManufacturingBillIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialActualIDType <em>Material Actual ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialActualIDType
	 * @generated
	 */
	public Adapter createMaterialActualIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialCapabilityIDType <em>Material Capability ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialCapabilityIDType
	 * @generated
	 */
	public Adapter createMaterialCapabilityIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialClassIDType <em>Material Class ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialClassIDType
	 * @generated
	 */
	public Adapter createMaterialClassIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialClassPropertyType <em>Material Class Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialClassPropertyType
	 * @generated
	 */
	public Adapter createMaterialClassPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialClassType <em>Material Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialClassType
	 * @generated
	 */
	public Adapter createMaterialClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialDefinitionIDType <em>Material Definition ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialDefinitionIDType
	 * @generated
	 */
	public Adapter createMaterialDefinitionIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialDefinitionPropertyType <em>Material Definition Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialDefinitionPropertyType
	 * @generated
	 */
	public Adapter createMaterialDefinitionPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialDefinitionType <em>Material Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialDefinitionType
	 * @generated
	 */
	public Adapter createMaterialDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType <em>Material Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialInformationType
	 * @generated
	 */
	public Adapter createMaterialInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialLotIDType <em>Material Lot ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialLotIDType
	 * @generated
	 */
	public Adapter createMaterialLotIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialLotPropertyType <em>Material Lot Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialLotPropertyType
	 * @generated
	 */
	public Adapter createMaterialLotPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialLotType <em>Material Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialLotType
	 * @generated
	 */
	public Adapter createMaterialLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialRequirementIDType <em>Material Requirement ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialRequirementIDType
	 * @generated
	 */
	public Adapter createMaterialRequirementIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialSegmentSpecificationPropertyType <em>Material Segment Specification Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialSegmentSpecificationPropertyType
	 * @generated
	 */
	public Adapter createMaterialSegmentSpecificationPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialSegmentSpecificationType <em>Material Segment Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialSegmentSpecificationType
	 * @generated
	 */
	public Adapter createMaterialSegmentSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialSpecificationIDType <em>Material Specification ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialSpecificationIDType
	 * @generated
	 */
	public Adapter createMaterialSpecificationIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotIDType <em>Material Sub Lot ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialSubLotIDType
	 * @generated
	 */
	public Adapter createMaterialSubLotIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType <em>Material Sub Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialSubLotType
	 * @generated
	 */
	public Adapter createMaterialSubLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialTestSpecificationIDType <em>Material Test Specification ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialTestSpecificationIDType
	 * @generated
	 */
	public Adapter createMaterialTestSpecificationIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialTestSpecificationType <em>Material Test Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialTestSpecificationType
	 * @generated
	 */
	public Adapter createMaterialTestSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialUse1Type <em>Material Use1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialUse1Type
	 * @generated
	 */
	public Adapter createMaterialUse1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MaterialUseType <em>Material Use Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MaterialUseType
	 * @generated
	 */
	public Adapter createMaterialUseTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.MeasureType <em>Measure Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.MeasureType
	 * @generated
	 */
	public Adapter createMeasureTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.NameType <em>Name Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.NameType
	 * @generated
	 */
	public Adapter createNameTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.NumericType <em>Numeric Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.NumericType
	 * @generated
	 */
	public Adapter createNumericTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentActualPropertyType <em>Op Equipment Actual Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpEquipmentActualPropertyType
	 * @generated
	 */
	public Adapter createOpEquipmentActualPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentActualType <em>Op Equipment Actual Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpEquipmentActualType
	 * @generated
	 */
	public Adapter createOpEquipmentActualTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentCapabilityPropertyType <em>Op Equipment Capability Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpEquipmentCapabilityPropertyType
	 * @generated
	 */
	public Adapter createOpEquipmentCapabilityPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentCapabilityType <em>Op Equipment Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpEquipmentCapabilityType
	 * @generated
	 */
	public Adapter createOpEquipmentCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentRequirementPropertyType <em>Op Equipment Requirement Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpEquipmentRequirementPropertyType
	 * @generated
	 */
	public Adapter createOpEquipmentRequirementPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentRequirementType <em>Op Equipment Requirement Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpEquipmentRequirementType
	 * @generated
	 */
	public Adapter createOpEquipmentRequirementTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationPropertyType <em>Op Equipment Specification Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationPropertyType
	 * @generated
	 */
	public Adapter createOpEquipmentSpecificationPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationType <em>Op Equipment Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationType
	 * @generated
	 */
	public Adapter createOpEquipmentSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType <em>Operations Capability Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType
	 * @generated
	 */
	public Adapter createOperationsCapabilityInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType <em>Operations Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType
	 * @generated
	 */
	public Adapter createOperationsCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsDefinitionIDType <em>Operations Definition ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsDefinitionIDType
	 * @generated
	 */
	public Adapter createOperationsDefinitionIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsDefinitionInformationType <em>Operations Definition Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsDefinitionInformationType
	 * @generated
	 */
	public Adapter createOperationsDefinitionInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsDefinitionType <em>Operations Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsDefinitionType
	 * @generated
	 */
	public Adapter createOperationsDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType <em>Operations Material Bill Item Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType
	 * @generated
	 */
	public Adapter createOperationsMaterialBillItemTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillType <em>Operations Material Bill Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillType
	 * @generated
	 */
	public Adapter createOperationsMaterialBillTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsPerformanceType <em>Operations Performance Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsPerformanceType
	 * @generated
	 */
	public Adapter createOperationsPerformanceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestIDType <em>Operations Request ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsRequestIDType
	 * @generated
	 */
	public Adapter createOperationsRequestIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsRequestType <em>Operations Request Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsRequestType
	 * @generated
	 */
	public Adapter createOperationsRequestTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsResponseType <em>Operations Response Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsResponseType
	 * @generated
	 */
	public Adapter createOperationsResponseTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsScheduleIDType <em>Operations Schedule ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsScheduleIDType
	 * @generated
	 */
	public Adapter createOperationsScheduleIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsScheduleType <em>Operations Schedule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsScheduleType
	 * @generated
	 */
	public Adapter createOperationsScheduleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsSegmentIDType <em>Operations Segment ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsSegmentIDType
	 * @generated
	 */
	public Adapter createOperationsSegmentIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsSegmentType <em>Operations Segment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsSegmentType
	 * @generated
	 */
	public Adapter createOperationsSegmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsType1Type <em>Operations Type1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsType1Type
	 * @generated
	 */
	public Adapter createOperationsType1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OperationsTypeType <em>Operations Type Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OperationsTypeType
	 * @generated
	 */
	public Adapter createOperationsTypeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpMaterialActualPropertyType <em>Op Material Actual Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpMaterialActualPropertyType
	 * @generated
	 */
	public Adapter createOpMaterialActualPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpMaterialActualType <em>Op Material Actual Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpMaterialActualType
	 * @generated
	 */
	public Adapter createOpMaterialActualTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpMaterialCapabilityPropertyType <em>Op Material Capability Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpMaterialCapabilityPropertyType
	 * @generated
	 */
	public Adapter createOpMaterialCapabilityPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpMaterialCapabilityType <em>Op Material Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpMaterialCapabilityType
	 * @generated
	 */
	public Adapter createOpMaterialCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpMaterialRequirementPropertyType <em>Op Material Requirement Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpMaterialRequirementPropertyType
	 * @generated
	 */
	public Adapter createOpMaterialRequirementPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpMaterialRequirementType <em>Op Material Requirement Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpMaterialRequirementType
	 * @generated
	 */
	public Adapter createOpMaterialRequirementTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpMaterialSpecificationPropertyType <em>Op Material Specification Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpMaterialSpecificationPropertyType
	 * @generated
	 */
	public Adapter createOpMaterialSpecificationPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpMaterialSpecificationType <em>Op Material Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpMaterialSpecificationType
	 * @generated
	 */
	public Adapter createOpMaterialSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPersonnelActualPropertyType <em>Op Personnel Actual Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPersonnelActualPropertyType
	 * @generated
	 */
	public Adapter createOpPersonnelActualPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPersonnelActualType <em>Op Personnel Actual Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPersonnelActualType
	 * @generated
	 */
	public Adapter createOpPersonnelActualTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPersonnelCapabilityPropertyType <em>Op Personnel Capability Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPersonnelCapabilityPropertyType
	 * @generated
	 */
	public Adapter createOpPersonnelCapabilityPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPersonnelCapabilityType <em>Op Personnel Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPersonnelCapabilityType
	 * @generated
	 */
	public Adapter createOpPersonnelCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPersonnelRequirementPropertyType <em>Op Personnel Requirement Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPersonnelRequirementPropertyType
	 * @generated
	 */
	public Adapter createOpPersonnelRequirementPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPersonnelRequirementType <em>Op Personnel Requirement Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPersonnelRequirementType
	 * @generated
	 */
	public Adapter createOpPersonnelRequirementTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPersonnelSpecificationPropertyType <em>Op Personnel Specification Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPersonnelSpecificationPropertyType
	 * @generated
	 */
	public Adapter createOpPersonnelSpecificationPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPersonnelSpecificationType <em>Op Personnel Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPersonnelSpecificationType
	 * @generated
	 */
	public Adapter createOpPersonnelSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetActualPropertyType <em>Op Physical Asset Actual Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetActualPropertyType
	 * @generated
	 */
	public Adapter createOpPhysicalAssetActualPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetActualType <em>Op Physical Asset Actual Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetActualType
	 * @generated
	 */
	public Adapter createOpPhysicalAssetActualTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityPropertyType <em>Op Physical Asset Capability Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityPropertyType
	 * @generated
	 */
	public Adapter createOpPhysicalAssetCapabilityPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType <em>Op Physical Asset Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType
	 * @generated
	 */
	public Adapter createOpPhysicalAssetCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetRequirementPropertyType <em>Op Physical Asset Requirement Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetRequirementPropertyType
	 * @generated
	 */
	public Adapter createOpPhysicalAssetRequirementPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetRequirementType <em>Op Physical Asset Requirement Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetRequirementType
	 * @generated
	 */
	public Adapter createOpPhysicalAssetRequirementTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetSpecificationPropertyType <em>Op Physical Asset Specification Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetSpecificationPropertyType
	 * @generated
	 */
	public Adapter createOpPhysicalAssetSpecificationPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetSpecificationType <em>Op Physical Asset Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetSpecificationType
	 * @generated
	 */
	public Adapter createOpPhysicalAssetSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpProcessSegmentCapabilityType <em>Op Process Segment Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpProcessSegmentCapabilityType
	 * @generated
	 */
	public Adapter createOpProcessSegmentCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpSegmentDataType <em>Op Segment Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpSegmentDataType
	 * @generated
	 */
	public Adapter createOpSegmentDataTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpSegmentRequirementType <em>Op Segment Requirement Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpSegmentRequirementType
	 * @generated
	 */
	public Adapter createOpSegmentRequirementTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType <em>Op Segment Response Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType
	 * @generated
	 */
	public Adapter createOpSegmentResponseTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.OtherDependencyType <em>Other Dependency Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.OtherDependencyType
	 * @generated
	 */
	public Adapter createOtherDependencyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ParameterIDType <em>Parameter ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ParameterIDType
	 * @generated
	 */
	public Adapter createParameterIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ParameterType <em>Parameter Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ParameterType
	 * @generated
	 */
	public Adapter createParameterTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PersonIDType <em>Person ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PersonIDType
	 * @generated
	 */
	public Adapter createPersonIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PersonNameType <em>Person Name Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PersonNameType
	 * @generated
	 */
	public Adapter createPersonNameTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PersonnelClassIDType <em>Personnel Class ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PersonnelClassIDType
	 * @generated
	 */
	public Adapter createPersonnelClassIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PersonnelClassPropertyType <em>Personnel Class Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PersonnelClassPropertyType
	 * @generated
	 */
	public Adapter createPersonnelClassPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PersonnelClassType <em>Personnel Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PersonnelClassType
	 * @generated
	 */
	public Adapter createPersonnelClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PersonnelInformationType <em>Personnel Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PersonnelInformationType
	 * @generated
	 */
	public Adapter createPersonnelInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationPropertyType <em>Personnel Segment Specification Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationPropertyType
	 * @generated
	 */
	public Adapter createPersonnelSegmentSpecificationPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType <em>Personnel Segment Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType
	 * @generated
	 */
	public Adapter createPersonnelSegmentSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PersonnelUseType <em>Personnel Use Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PersonnelUseType
	 * @generated
	 */
	public Adapter createPersonnelUseTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PersonPropertyType <em>Person Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PersonPropertyType
	 * @generated
	 */
	public Adapter createPersonPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PersonType <em>Person Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PersonType
	 * @generated
	 */
	public Adapter createPersonTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetActualIDType <em>Physical Asset Actual ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetActualIDType
	 * @generated
	 */
	public Adapter createPhysicalAssetActualIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationIDType <em>Physical Asset Capability Test Specification ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationIDType
	 * @generated
	 */
	public Adapter createPhysicalAssetCapabilityTestSpecificationIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationType <em>Physical Asset Capability Test Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationType
	 * @generated
	 */
	public Adapter createPhysicalAssetCapabilityTestSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassIDType <em>Physical Asset Class ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassIDType
	 * @generated
	 */
	public Adapter createPhysicalAssetClassIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassPropertyType <em>Physical Asset Class Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassPropertyType
	 * @generated
	 */
	public Adapter createPhysicalAssetClassPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassType <em>Physical Asset Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassType
	 * @generated
	 */
	public Adapter createPhysicalAssetClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetIDType <em>Physical Asset ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetIDType
	 * @generated
	 */
	public Adapter createPhysicalAssetIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetInformationType <em>Physical Asset Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetInformationType
	 * @generated
	 */
	public Adapter createPhysicalAssetInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetPropertyType <em>Physical Asset Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetPropertyType
	 * @generated
	 */
	public Adapter createPhysicalAssetPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetSegmentSpecificationPropertyType <em>Physical Asset Segment Specification Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetSegmentSpecificationPropertyType
	 * @generated
	 */
	public Adapter createPhysicalAssetSegmentSpecificationPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetSegmentSpecificationType <em>Physical Asset Segment Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetSegmentSpecificationType
	 * @generated
	 */
	public Adapter createPhysicalAssetSegmentSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetType <em>Physical Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetType
	 * @generated
	 */
	public Adapter createPhysicalAssetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetUseType <em>Physical Asset Use Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PhysicalAssetUseType
	 * @generated
	 */
	public Adapter createPhysicalAssetUseTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PlannedFinishTimeType <em>Planned Finish Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PlannedFinishTimeType
	 * @generated
	 */
	public Adapter createPlannedFinishTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PlannedStartTimeType <em>Planned Start Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PlannedStartTimeType
	 * @generated
	 */
	public Adapter createPlannedStartTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PriorityType <em>Priority Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PriorityType
	 * @generated
	 */
	public Adapter createPriorityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProblemType <em>Problem Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProblemType
	 * @generated
	 */
	public Adapter createProblemTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessEquipmentCapabilityTestSpecType <em>Process Equipment Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessEquipmentCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createProcessEquipmentCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessEquipmentClassType <em>Process Equipment Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessEquipmentClassType
	 * @generated
	 */
	public Adapter createProcessEquipmentClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessEquipmentInformationType <em>Process Equipment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessEquipmentInformationType
	 * @generated
	 */
	public Adapter createProcessEquipmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessEquipmentType <em>Process Equipment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessEquipmentType
	 * @generated
	 */
	public Adapter createProcessEquipmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessMaterialClassType <em>Process Material Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessMaterialClassType
	 * @generated
	 */
	public Adapter createProcessMaterialClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessMaterialDefinitionType <em>Process Material Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessMaterialDefinitionType
	 * @generated
	 */
	public Adapter createProcessMaterialDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessMaterialInformationType <em>Process Material Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessMaterialInformationType
	 * @generated
	 */
	public Adapter createProcessMaterialInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessMaterialLotType <em>Process Material Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessMaterialLotType
	 * @generated
	 */
	public Adapter createProcessMaterialLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessMaterialSubLotType <em>Process Material Sub Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessMaterialSubLotType
	 * @generated
	 */
	public Adapter createProcessMaterialSubLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessMaterialTestSpecType <em>Process Material Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessMaterialTestSpecType
	 * @generated
	 */
	public Adapter createProcessMaterialTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessOperationsCapabilityInformationType <em>Process Operations Capability Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessOperationsCapabilityInformationType
	 * @generated
	 */
	public Adapter createProcessOperationsCapabilityInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessOperationsCapabilityType <em>Process Operations Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessOperationsCapabilityType
	 * @generated
	 */
	public Adapter createProcessOperationsCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessOperationsDefinitionInformationType <em>Process Operations Definition Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessOperationsDefinitionInformationType
	 * @generated
	 */
	public Adapter createProcessOperationsDefinitionInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessOperationsDefinitionType <em>Process Operations Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessOperationsDefinitionType
	 * @generated
	 */
	public Adapter createProcessOperationsDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessOperationsPerformanceType <em>Process Operations Performance Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessOperationsPerformanceType
	 * @generated
	 */
	public Adapter createProcessOperationsPerformanceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessOperationsScheduleType <em>Process Operations Schedule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessOperationsScheduleType
	 * @generated
	 */
	public Adapter createProcessOperationsScheduleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessPersonnelClassType <em>Process Personnel Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessPersonnelClassType
	 * @generated
	 */
	public Adapter createProcessPersonnelClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessPersonnelInformationType <em>Process Personnel Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessPersonnelInformationType
	 * @generated
	 */
	public Adapter createProcessPersonnelInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessPersonType <em>Process Person Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessPersonType
	 * @generated
	 */
	public Adapter createProcessPersonTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessPhysicalAssetCapabilityTestSpecType <em>Process Physical Asset Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessPhysicalAssetCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createProcessPhysicalAssetCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessPhysicalAssetClassType <em>Process Physical Asset Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessPhysicalAssetClassType
	 * @generated
	 */
	public Adapter createProcessPhysicalAssetClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessPhysicalAssetInformationType <em>Process Physical Asset Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessPhysicalAssetInformationType
	 * @generated
	 */
	public Adapter createProcessPhysicalAssetInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessPhysicalAssetType <em>Process Physical Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessPhysicalAssetType
	 * @generated
	 */
	public Adapter createProcessPhysicalAssetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessProcessSegmentInformationType <em>Process Process Segment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessProcessSegmentInformationType
	 * @generated
	 */
	public Adapter createProcessProcessSegmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessProcessSegmentType <em>Process Process Segment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessProcessSegmentType
	 * @generated
	 */
	public Adapter createProcessProcessSegmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessQualificationTestSpecificationType <em>Process Qualification Test Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessQualificationTestSpecificationType
	 * @generated
	 */
	public Adapter createProcessQualificationTestSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessSegmentIDType <em>Process Segment ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessSegmentIDType
	 * @generated
	 */
	public Adapter createProcessSegmentIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessSegmentInformationType <em>Process Segment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessSegmentInformationType
	 * @generated
	 */
	public Adapter createProcessSegmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProcessSegmentType <em>Process Segment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProcessSegmentType
	 * @generated
	 */
	public Adapter createProcessSegmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProductionRequestIDType <em>Production Request ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProductionRequestIDType
	 * @generated
	 */
	public Adapter createProductionRequestIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProductionScheduleIDType <em>Production Schedule ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProductionScheduleIDType
	 * @generated
	 */
	public Adapter createProductionScheduleIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProductProductionRuleIDType <em>Product Production Rule ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProductProductionRuleIDType
	 * @generated
	 */
	public Adapter createProductProductionRuleIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProductProductionRuleType <em>Product Production Rule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProductProductionRuleType
	 * @generated
	 */
	public Adapter createProductProductionRuleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ProductSegmentIDType <em>Product Segment ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ProductSegmentIDType
	 * @generated
	 */
	public Adapter createProductSegmentIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PropertyIDType <em>Property ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PropertyIDType
	 * @generated
	 */
	public Adapter createPropertyIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.PublishedDateType <em>Published Date Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.PublishedDateType
	 * @generated
	 */
	public Adapter createPublishedDateTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.QualificationTestSpecificationIDType <em>Qualification Test Specification ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.QualificationTestSpecificationIDType
	 * @generated
	 */
	public Adapter createQualificationTestSpecificationIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.QualificationTestSpecificationType <em>Qualification Test Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.QualificationTestSpecificationType
	 * @generated
	 */
	public Adapter createQualificationTestSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.QuantityStringType <em>Quantity String Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.QuantityStringType
	 * @generated
	 */
	public Adapter createQuantityStringTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.QuantityType <em>Quantity Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.QuantityType
	 * @generated
	 */
	public Adapter createQuantityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType <em>Quantity Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.QuantityValueType
	 * @generated
	 */
	public Adapter createQuantityValueTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ReasonType <em>Reason Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ReasonType
	 * @generated
	 */
	public Adapter createReasonTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RelationshipForm1Type <em>Relationship Form1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RelationshipForm1Type
	 * @generated
	 */
	public Adapter createRelationshipForm1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RelationshipFormType <em>Relationship Form Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RelationshipFormType
	 * @generated
	 */
	public Adapter createRelationshipFormTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RelationshipType1Type <em>Relationship Type1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RelationshipType1Type
	 * @generated
	 */
	public Adapter createRelationshipType1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RelationshipTypeType <em>Relationship Type Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RelationshipTypeType
	 * @generated
	 */
	public Adapter createRelationshipTypeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RequestedCompletionDateType <em>Requested Completion Date Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RequestedCompletionDateType
	 * @generated
	 */
	public Adapter createRequestedCompletionDateTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RequestedPriorityType <em>Requested Priority Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RequestedPriorityType
	 * @generated
	 */
	public Adapter createRequestedPriorityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RequestState1Type <em>Request State1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RequestState1Type
	 * @generated
	 */
	public Adapter createRequestState1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RequestStateType <em>Request State Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RequestStateType
	 * @generated
	 */
	public Adapter createRequestStateTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RequiredByRequestedSegmentResponse1Type <em>Required By Requested Segment Response1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RequiredByRequestedSegmentResponse1Type
	 * @generated
	 */
	public Adapter createRequiredByRequestedSegmentResponse1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RequiredByRequestedSegmentResponseType <em>Required By Requested Segment Response Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RequiredByRequestedSegmentResponseType
	 * @generated
	 */
	public Adapter createRequiredByRequestedSegmentResponseTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ResourceIDType <em>Resource ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ResourceIDType
	 * @generated
	 */
	public Adapter createResourceIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ResourceNetworkConnectionIDType <em>Resource Network Connection ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ResourceNetworkConnectionIDType
	 * @generated
	 */
	public Adapter createResourceNetworkConnectionIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ResourceReferenceType1Type <em>Resource Reference Type1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ResourceReferenceType1Type
	 * @generated
	 */
	public Adapter createResourceReferenceType1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ResourceReferenceTypeType <em>Resource Reference Type Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ResourceReferenceTypeType
	 * @generated
	 */
	public Adapter createResourceReferenceTypeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ResourcesType <em>Resources Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ResourcesType
	 * @generated
	 */
	public Adapter createResourcesTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondEquipmentCapabilityTestSpecType <em>Respond Equipment Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondEquipmentCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createRespondEquipmentCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondEquipmentClassType <em>Respond Equipment Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondEquipmentClassType
	 * @generated
	 */
	public Adapter createRespondEquipmentClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondEquipmentInformationType <em>Respond Equipment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondEquipmentInformationType
	 * @generated
	 */
	public Adapter createRespondEquipmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondEquipmentType <em>Respond Equipment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondEquipmentType
	 * @generated
	 */
	public Adapter createRespondEquipmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialClassType <em>Respond Material Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondMaterialClassType
	 * @generated
	 */
	public Adapter createRespondMaterialClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialDefinitionType <em>Respond Material Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondMaterialDefinitionType
	 * @generated
	 */
	public Adapter createRespondMaterialDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialInformationType <em>Respond Material Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondMaterialInformationType
	 * @generated
	 */
	public Adapter createRespondMaterialInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialLotType <em>Respond Material Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondMaterialLotType
	 * @generated
	 */
	public Adapter createRespondMaterialLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialSubLotType <em>Respond Material Sub Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondMaterialSubLotType
	 * @generated
	 */
	public Adapter createRespondMaterialSubLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialTestSpecType <em>Respond Material Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondMaterialTestSpecType
	 * @generated
	 */
	public Adapter createRespondMaterialTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondOperationsCapabilityInformationType <em>Respond Operations Capability Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondOperationsCapabilityInformationType
	 * @generated
	 */
	public Adapter createRespondOperationsCapabilityInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondOperationsCapabilityType <em>Respond Operations Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondOperationsCapabilityType
	 * @generated
	 */
	public Adapter createRespondOperationsCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondOperationsDefinitionInformationType <em>Respond Operations Definition Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondOperationsDefinitionInformationType
	 * @generated
	 */
	public Adapter createRespondOperationsDefinitionInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondOperationsDefinitionType <em>Respond Operations Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondOperationsDefinitionType
	 * @generated
	 */
	public Adapter createRespondOperationsDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondOperationsPerformanceType <em>Respond Operations Performance Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondOperationsPerformanceType
	 * @generated
	 */
	public Adapter createRespondOperationsPerformanceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondOperationsScheduleType <em>Respond Operations Schedule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondOperationsScheduleType
	 * @generated
	 */
	public Adapter createRespondOperationsScheduleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondPersonnelClassType <em>Respond Personnel Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondPersonnelClassType
	 * @generated
	 */
	public Adapter createRespondPersonnelClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondPersonnelInformationType <em>Respond Personnel Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondPersonnelInformationType
	 * @generated
	 */
	public Adapter createRespondPersonnelInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondPersonType <em>Respond Person Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondPersonType
	 * @generated
	 */
	public Adapter createRespondPersonTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondPhysicalAssetCapabilityTestSpecType <em>Respond Physical Asset Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondPhysicalAssetCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createRespondPhysicalAssetCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondPhysicalAssetClassType <em>Respond Physical Asset Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondPhysicalAssetClassType
	 * @generated
	 */
	public Adapter createRespondPhysicalAssetClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondPhysicalAssetInformationType <em>Respond Physical Asset Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondPhysicalAssetInformationType
	 * @generated
	 */
	public Adapter createRespondPhysicalAssetInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondPhysicalAssetType <em>Respond Physical Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondPhysicalAssetType
	 * @generated
	 */
	public Adapter createRespondPhysicalAssetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondProcessSegmentInformationType <em>Respond Process Segment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondProcessSegmentInformationType
	 * @generated
	 */
	public Adapter createRespondProcessSegmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondProcessSegmentType <em>Respond Process Segment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondProcessSegmentType
	 * @generated
	 */
	public Adapter createRespondProcessSegmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.RespondQualificationTestSpecificationType <em>Respond Qualification Test Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.RespondQualificationTestSpecificationType
	 * @generated
	 */
	public Adapter createRespondQualificationTestSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ResponseState1Type <em>Response State1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ResponseState1Type
	 * @generated
	 */
	public Adapter createResponseState1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ResponseStateType <em>Response State Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ResponseStateType
	 * @generated
	 */
	public Adapter createResponseStateTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ResultType <em>Result Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ResultType
	 * @generated
	 */
	public Adapter createResultTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SegmentDependencyType <em>Segment Dependency Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SegmentDependencyType
	 * @generated
	 */
	public Adapter createSegmentDependencyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowEquipmentCapabilityTestSpecType <em>Show Equipment Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowEquipmentCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createShowEquipmentCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowEquipmentClassType <em>Show Equipment Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowEquipmentClassType
	 * @generated
	 */
	public Adapter createShowEquipmentClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowEquipmentInformationType <em>Show Equipment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowEquipmentInformationType
	 * @generated
	 */
	public Adapter createShowEquipmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowEquipmentType <em>Show Equipment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowEquipmentType
	 * @generated
	 */
	public Adapter createShowEquipmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowMaterialClassType <em>Show Material Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowMaterialClassType
	 * @generated
	 */
	public Adapter createShowMaterialClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowMaterialDefinitionType <em>Show Material Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowMaterialDefinitionType
	 * @generated
	 */
	public Adapter createShowMaterialDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowMaterialInformationType <em>Show Material Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowMaterialInformationType
	 * @generated
	 */
	public Adapter createShowMaterialInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowMaterialLotType <em>Show Material Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowMaterialLotType
	 * @generated
	 */
	public Adapter createShowMaterialLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowMaterialSubLotType <em>Show Material Sub Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowMaterialSubLotType
	 * @generated
	 */
	public Adapter createShowMaterialSubLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowMaterialTestSpecType <em>Show Material Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowMaterialTestSpecType
	 * @generated
	 */
	public Adapter createShowMaterialTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowOperationsCapabilityInformationType <em>Show Operations Capability Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowOperationsCapabilityInformationType
	 * @generated
	 */
	public Adapter createShowOperationsCapabilityInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowOperationsCapabilityType <em>Show Operations Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowOperationsCapabilityType
	 * @generated
	 */
	public Adapter createShowOperationsCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowOperationsDefinitionInformationType <em>Show Operations Definition Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowOperationsDefinitionInformationType
	 * @generated
	 */
	public Adapter createShowOperationsDefinitionInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowOperationsDefinitionType <em>Show Operations Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowOperationsDefinitionType
	 * @generated
	 */
	public Adapter createShowOperationsDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowOperationsPerformanceType <em>Show Operations Performance Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowOperationsPerformanceType
	 * @generated
	 */
	public Adapter createShowOperationsPerformanceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowOperationsScheduleType <em>Show Operations Schedule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowOperationsScheduleType
	 * @generated
	 */
	public Adapter createShowOperationsScheduleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowPersonnelClassType <em>Show Personnel Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowPersonnelClassType
	 * @generated
	 */
	public Adapter createShowPersonnelClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowPersonnelInformationType <em>Show Personnel Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowPersonnelInformationType
	 * @generated
	 */
	public Adapter createShowPersonnelInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowPersonType <em>Show Person Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowPersonType
	 * @generated
	 */
	public Adapter createShowPersonTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowPhysicalAssetCapabilityTestSpecType <em>Show Physical Asset Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowPhysicalAssetCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createShowPhysicalAssetCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowPhysicalAssetClassType <em>Show Physical Asset Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowPhysicalAssetClassType
	 * @generated
	 */
	public Adapter createShowPhysicalAssetClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowPhysicalAssetInformationType <em>Show Physical Asset Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowPhysicalAssetInformationType
	 * @generated
	 */
	public Adapter createShowPhysicalAssetInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowPhysicalAssetType <em>Show Physical Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowPhysicalAssetType
	 * @generated
	 */
	public Adapter createShowPhysicalAssetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowProcessSegmentInformationType <em>Show Process Segment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowProcessSegmentInformationType
	 * @generated
	 */
	public Adapter createShowProcessSegmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowProcessSegmentType <em>Show Process Segment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowProcessSegmentType
	 * @generated
	 */
	public Adapter createShowProcessSegmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ShowQualificationTestSpecificationType <em>Show Qualification Test Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ShowQualificationTestSpecificationType
	 * @generated
	 */
	public Adapter createShowQualificationTestSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.StartTimeType <em>Start Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.StartTimeType
	 * @generated
	 */
	public Adapter createStartTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.StatusTimeType <em>Status Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.StatusTimeType
	 * @generated
	 */
	public Adapter createStatusTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.StatusType <em>Status Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.StatusType
	 * @generated
	 */
	public Adapter createStatusTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.StorageHierarchyScopeType <em>Storage Hierarchy Scope Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.StorageHierarchyScopeType
	 * @generated
	 */
	public Adapter createStorageHierarchyScopeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.StorageLocationType <em>Storage Location Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.StorageLocationType
	 * @generated
	 */
	public Adapter createStorageLocationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncEquipmentCapabilityTestSpecType <em>Sync Equipment Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncEquipmentCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createSyncEquipmentCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncEquipmentClassType <em>Sync Equipment Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncEquipmentClassType
	 * @generated
	 */
	public Adapter createSyncEquipmentClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncEquipmentInformationType <em>Sync Equipment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncEquipmentInformationType
	 * @generated
	 */
	public Adapter createSyncEquipmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncEquipmentType <em>Sync Equipment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncEquipmentType
	 * @generated
	 */
	public Adapter createSyncEquipmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncMaterialClassType <em>Sync Material Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncMaterialClassType
	 * @generated
	 */
	public Adapter createSyncMaterialClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncMaterialDefinitionType <em>Sync Material Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncMaterialDefinitionType
	 * @generated
	 */
	public Adapter createSyncMaterialDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncMaterialInformationType <em>Sync Material Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncMaterialInformationType
	 * @generated
	 */
	public Adapter createSyncMaterialInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncMaterialLotType <em>Sync Material Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncMaterialLotType
	 * @generated
	 */
	public Adapter createSyncMaterialLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncMaterialSubLotType <em>Sync Material Sub Lot Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncMaterialSubLotType
	 * @generated
	 */
	public Adapter createSyncMaterialSubLotTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncMaterialTestSpecType <em>Sync Material Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncMaterialTestSpecType
	 * @generated
	 */
	public Adapter createSyncMaterialTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncOperationsCapabilityInformationType <em>Sync Operations Capability Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncOperationsCapabilityInformationType
	 * @generated
	 */
	public Adapter createSyncOperationsCapabilityInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncOperationsCapabilityType <em>Sync Operations Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncOperationsCapabilityType
	 * @generated
	 */
	public Adapter createSyncOperationsCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncOperationsDefinitionInformationType <em>Sync Operations Definition Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncOperationsDefinitionInformationType
	 * @generated
	 */
	public Adapter createSyncOperationsDefinitionInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncOperationsDefinitionType <em>Sync Operations Definition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncOperationsDefinitionType
	 * @generated
	 */
	public Adapter createSyncOperationsDefinitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncOperationsPerformanceType <em>Sync Operations Performance Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncOperationsPerformanceType
	 * @generated
	 */
	public Adapter createSyncOperationsPerformanceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncOperationsScheduleType <em>Sync Operations Schedule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncOperationsScheduleType
	 * @generated
	 */
	public Adapter createSyncOperationsScheduleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncPersonnelClassType <em>Sync Personnel Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncPersonnelClassType
	 * @generated
	 */
	public Adapter createSyncPersonnelClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncPersonnelInformationType <em>Sync Personnel Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncPersonnelInformationType
	 * @generated
	 */
	public Adapter createSyncPersonnelInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncPersonType <em>Sync Person Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncPersonType
	 * @generated
	 */
	public Adapter createSyncPersonTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncPhysicalAssetCapabilityTestSpecType <em>Sync Physical Asset Capability Test Spec Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncPhysicalAssetCapabilityTestSpecType
	 * @generated
	 */
	public Adapter createSyncPhysicalAssetCapabilityTestSpecTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncPhysicalAssetClassType <em>Sync Physical Asset Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncPhysicalAssetClassType
	 * @generated
	 */
	public Adapter createSyncPhysicalAssetClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncPhysicalAssetInformationType <em>Sync Physical Asset Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncPhysicalAssetInformationType
	 * @generated
	 */
	public Adapter createSyncPhysicalAssetInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncPhysicalAssetType <em>Sync Physical Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncPhysicalAssetType
	 * @generated
	 */
	public Adapter createSyncPhysicalAssetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncProcessSegmentInformationType <em>Sync Process Segment Information Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncProcessSegmentInformationType
	 * @generated
	 */
	public Adapter createSyncProcessSegmentInformationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncProcessSegmentType <em>Sync Process Segment Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncProcessSegmentType
	 * @generated
	 */
	public Adapter createSyncProcessSegmentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.SyncQualificationTestSpecificationType <em>Sync Qualification Test Specification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.SyncQualificationTestSpecificationType
	 * @generated
	 */
	public Adapter createSyncQualificationTestSpecificationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TestDateTimeType <em>Test Date Time Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TestDateTimeType
	 * @generated
	 */
	public Adapter createTestDateTimeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TestedEquipmentClassPropertyType <em>Tested Equipment Class Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TestedEquipmentClassPropertyType
	 * @generated
	 */
	public Adapter createTestedEquipmentClassPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TestedEquipmentPropertyType <em>Tested Equipment Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TestedEquipmentPropertyType
	 * @generated
	 */
	public Adapter createTestedEquipmentPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TestedMaterialClassPropertyType <em>Tested Material Class Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TestedMaterialClassPropertyType
	 * @generated
	 */
	public Adapter createTestedMaterialClassPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TestedMaterialDefinitionPropertyType <em>Tested Material Definition Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TestedMaterialDefinitionPropertyType
	 * @generated
	 */
	public Adapter createTestedMaterialDefinitionPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TestedMaterialLotPropertyType <em>Tested Material Lot Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TestedMaterialLotPropertyType
	 * @generated
	 */
	public Adapter createTestedMaterialLotPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TestedPersonnelClassPropertyType <em>Tested Personnel Class Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TestedPersonnelClassPropertyType
	 * @generated
	 */
	public Adapter createTestedPersonnelClassPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TestedPersonPropertyType <em>Tested Person Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TestedPersonPropertyType
	 * @generated
	 */
	public Adapter createTestedPersonPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetClassPropertyType <em>Tested Physical Asset Class Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetClassPropertyType
	 * @generated
	 */
	public Adapter createTestedPhysicalAssetClassPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetPropertyType <em>Tested Physical Asset Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetPropertyType
	 * @generated
	 */
	public Adapter createTestedPhysicalAssetPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TestResultType <em>Test Result Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TestResultType
	 * @generated
	 */
	public Adapter createTestResultTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TextType <em>Text Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TextType
	 * @generated
	 */
	public Adapter createTextTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransAcknowledgeType <em>Trans Acknowledge Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransAcknowledgeType
	 * @generated
	 */
	public Adapter createTransAcknowledgeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransActionCriteriaType <em>Trans Action Criteria Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransActionCriteriaType
	 * @generated
	 */
	public Adapter createTransActionCriteriaTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType <em>Trans Application Area Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType
	 * @generated
	 */
	public Adapter createTransApplicationAreaTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransCancelType <em>Trans Cancel Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransCancelType
	 * @generated
	 */
	public Adapter createTransCancelTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransChangeStatusType <em>Trans Change Status Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransChangeStatusType
	 * @generated
	 */
	public Adapter createTransChangeStatusTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransChangeType <em>Trans Change Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransChangeType
	 * @generated
	 */
	public Adapter createTransChangeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransConfirmationCodeType <em>Trans Confirmation Code Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransConfirmationCodeType
	 * @generated
	 */
	public Adapter createTransConfirmationCodeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransConfirmType <em>Trans Confirm Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransConfirmType
	 * @generated
	 */
	public Adapter createTransConfirmTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransExpression1Type <em>Trans Expression1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransExpression1Type
	 * @generated
	 */
	public Adapter createTransExpression1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransExpressionType <em>Trans Expression Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransExpressionType
	 * @generated
	 */
	public Adapter createTransExpressionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransGetType <em>Trans Get Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransGetType
	 * @generated
	 */
	public Adapter createTransGetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransProcessType <em>Trans Process Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransProcessType
	 * @generated
	 */
	public Adapter createTransProcessTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransReceiverType <em>Trans Receiver Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransReceiverType
	 * @generated
	 */
	public Adapter createTransReceiverTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransRespondType <em>Trans Respond Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransRespondType
	 * @generated
	 */
	public Adapter createTransRespondTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransResponseCriteriaType <em>Trans Response Criteria Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransResponseCriteriaType
	 * @generated
	 */
	public Adapter createTransResponseCriteriaTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransSenderType <em>Trans Sender Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransSenderType
	 * @generated
	 */
	public Adapter createTransSenderTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransShowType <em>Trans Show Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransShowType
	 * @generated
	 */
	public Adapter createTransShowTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransSignatureType <em>Trans Signature Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransSignatureType
	 * @generated
	 */
	public Adapter createTransSignatureTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransStateChangeType <em>Trans State Change Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransStateChangeType
	 * @generated
	 */
	public Adapter createTransStateChangeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransSyncType <em>Trans Sync Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransSyncType
	 * @generated
	 */
	public Adapter createTransSyncTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.TransUserAreaType <em>Trans User Area Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.TransUserAreaType
	 * @generated
	 */
	public Adapter createTransUserAreaTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.UnitOfMeasureType <em>Unit Of Measure Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.UnitOfMeasureType
	 * @generated
	 */
	public Adapter createUnitOfMeasureTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ValueStringType <em>Value String Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ValueStringType
	 * @generated
	 */
	public Adapter createValueStringTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.ValueType <em>Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.ValueType
	 * @generated
	 */
	public Adapter createValueTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.VersionType <em>Version Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.VersionType
	 * @generated
	 */
	public Adapter createVersionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.WorkRequestIDType <em>Work Request ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.WorkRequestIDType
	 * @generated
	 */
	public Adapter createWorkRequestIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.WorkScheduleIDType <em>Work Schedule ID Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.WorkScheduleIDType
	 * @generated
	 */
	public Adapter createWorkScheduleIDTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.WorkType1Type <em>Work Type1 Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.WorkType1Type
	 * @generated
	 */
	public Adapter createWorkType1TypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link at.ac.tuwien.big.ame.b2mml.WorkTypeType <em>Work Type Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see at.ac.tuwien.big.ame.b2mml.WorkTypeType
	 * @generated
	 */
	public Adapter createWorkTypeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //B2MMLAdapterFactory
