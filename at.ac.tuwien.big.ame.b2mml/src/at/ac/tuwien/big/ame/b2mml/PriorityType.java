/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Priority Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPriorityType()
 * @model extendedMetaData="name='PriorityType' kind='simple'"
 * @generated
 */
public interface PriorityType extends NumericType {
} // PriorityType
