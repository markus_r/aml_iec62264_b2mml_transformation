/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.LocationType;
import at.ac.tuwien.big.ame.b2mml.MaterialSegmentSpecificationType;
import at.ac.tuwien.big.ame.b2mml.OperationsTypeType;
import at.ac.tuwien.big.ame.b2mml.ParameterType;
import at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetSegmentSpecificationType;
import at.ac.tuwien.big.ame.b2mml.ProcessSegmentType;
import at.ac.tuwien.big.ame.b2mml.PublishedDateType;
import at.ac.tuwien.big.ame.b2mml.SegmentDependencyType;

import java.util.Collection;

import javax.xml.datatype.Duration;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process Segment Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getDuration <em>Duration</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getPersonnelSegmentSpecification <em>Personnel Segment Specification</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getEquipmentSegmentSpecification <em>Equipment Segment Specification</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getPhysicalAssetSegmentSpecification <em>Physical Asset Segment Specification</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getMaterialSegmentSpecification <em>Material Segment Specification</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getParameter <em>Parameter</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getSegmentDependency <em>Segment Dependency</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ProcessSegmentTypeImpl#getProcessSegment <em>Process Segment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcessSegmentTypeImpl extends MinimalEObjectImpl.Container implements ProcessSegmentType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsTypeType operationsType;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected LocationType location;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getPublishedDate() <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublishedDate()
	 * @generated
	 * @ordered
	 */
	protected PublishedDateType publishedDate;

	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final Duration DURATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected Duration duration = DURATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPersonnelSegmentSpecification() <em>Personnel Segment Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelSegmentSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelSegmentSpecificationType> personnelSegmentSpecification;

	/**
	 * The cached value of the '{@link #getEquipmentSegmentSpecification() <em>Equipment Segment Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentSegmentSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentSegmentSpecificationType> equipmentSegmentSpecification;

	/**
	 * The cached value of the '{@link #getPhysicalAssetSegmentSpecification() <em>Physical Asset Segment Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetSegmentSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetSegmentSpecificationType> physicalAssetSegmentSpecification;

	/**
	 * The cached value of the '{@link #getMaterialSegmentSpecification() <em>Material Segment Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSegmentSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSegmentSpecificationType> materialSegmentSpecification;

	/**
	 * The cached value of the '{@link #getParameter() <em>Parameter</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterType> parameter;

	/**
	 * The cached value of the '{@link #getSegmentDependency() <em>Segment Dependency</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentDependency()
	 * @generated
	 * @ordered
	 */
	protected EList<SegmentDependencyType> segmentDependency;

	/**
	 * The cached value of the '{@link #getProcessSegment() <em>Process Segment</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegment()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessSegmentType> processSegment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessSegmentTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getProcessSegmentType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PROCESS_SEGMENT_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PROCESS_SEGMENT_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PROCESS_SEGMENT_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PROCESS_SEGMENT_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.PROCESS_SEGMENT_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsTypeType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsType(OperationsTypeType newOperationsType, NotificationChain msgs) {
		OperationsTypeType oldOperationsType = operationsType;
		operationsType = newOperationsType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PROCESS_SEGMENT_TYPE__OPERATIONS_TYPE, oldOperationsType, newOperationsType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsTypeType newOperationsType) {
		if (newOperationsType != operationsType) {
			NotificationChain msgs = null;
			if (operationsType != null)
				msgs = ((InternalEObject)operationsType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PROCESS_SEGMENT_TYPE__OPERATIONS_TYPE, null, msgs);
			if (newOperationsType != null)
				msgs = ((InternalEObject)newOperationsType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PROCESS_SEGMENT_TYPE__OPERATIONS_TYPE, null, msgs);
			msgs = basicSetOperationsType(newOperationsType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PROCESS_SEGMENT_TYPE__OPERATIONS_TYPE, newOperationsType, newOperationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationType getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocation(LocationType newLocation, NotificationChain msgs) {
		LocationType oldLocation = location;
		location = newLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PROCESS_SEGMENT_TYPE__LOCATION, oldLocation, newLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(LocationType newLocation) {
		if (newLocation != location) {
			NotificationChain msgs = null;
			if (location != null)
				msgs = ((InternalEObject)location).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PROCESS_SEGMENT_TYPE__LOCATION, null, msgs);
			if (newLocation != null)
				msgs = ((InternalEObject)newLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PROCESS_SEGMENT_TYPE__LOCATION, null, msgs);
			msgs = basicSetLocation(newLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PROCESS_SEGMENT_TYPE__LOCATION, newLocation, newLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PROCESS_SEGMENT_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PROCESS_SEGMENT_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PROCESS_SEGMENT_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PROCESS_SEGMENT_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PublishedDateType getPublishedDate() {
		return publishedDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPublishedDate(PublishedDateType newPublishedDate, NotificationChain msgs) {
		PublishedDateType oldPublishedDate = publishedDate;
		publishedDate = newPublishedDate;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PROCESS_SEGMENT_TYPE__PUBLISHED_DATE, oldPublishedDate, newPublishedDate);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublishedDate(PublishedDateType newPublishedDate) {
		if (newPublishedDate != publishedDate) {
			NotificationChain msgs = null;
			if (publishedDate != null)
				msgs = ((InternalEObject)publishedDate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PROCESS_SEGMENT_TYPE__PUBLISHED_DATE, null, msgs);
			if (newPublishedDate != null)
				msgs = ((InternalEObject)newPublishedDate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PROCESS_SEGMENT_TYPE__PUBLISHED_DATE, null, msgs);
			msgs = basicSetPublishedDate(newPublishedDate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PROCESS_SEGMENT_TYPE__PUBLISHED_DATE, newPublishedDate, newPublishedDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Duration getDuration() {
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDuration(Duration newDuration) {
		Duration oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PROCESS_SEGMENT_TYPE__DURATION, oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelSegmentSpecificationType> getPersonnelSegmentSpecification() {
		if (personnelSegmentSpecification == null) {
			personnelSegmentSpecification = new EObjectContainmentEList<PersonnelSegmentSpecificationType>(PersonnelSegmentSpecificationType.class, this, B2MMLPackage.PROCESS_SEGMENT_TYPE__PERSONNEL_SEGMENT_SPECIFICATION);
		}
		return personnelSegmentSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentSegmentSpecificationType> getEquipmentSegmentSpecification() {
		if (equipmentSegmentSpecification == null) {
			equipmentSegmentSpecification = new EObjectContainmentEList<EquipmentSegmentSpecificationType>(EquipmentSegmentSpecificationType.class, this, B2MMLPackage.PROCESS_SEGMENT_TYPE__EQUIPMENT_SEGMENT_SPECIFICATION);
		}
		return equipmentSegmentSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetSegmentSpecificationType> getPhysicalAssetSegmentSpecification() {
		if (physicalAssetSegmentSpecification == null) {
			physicalAssetSegmentSpecification = new EObjectContainmentEList<PhysicalAssetSegmentSpecificationType>(PhysicalAssetSegmentSpecificationType.class, this, B2MMLPackage.PROCESS_SEGMENT_TYPE__PHYSICAL_ASSET_SEGMENT_SPECIFICATION);
		}
		return physicalAssetSegmentSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSegmentSpecificationType> getMaterialSegmentSpecification() {
		if (materialSegmentSpecification == null) {
			materialSegmentSpecification = new EObjectContainmentEList<MaterialSegmentSpecificationType>(MaterialSegmentSpecificationType.class, this, B2MMLPackage.PROCESS_SEGMENT_TYPE__MATERIAL_SEGMENT_SPECIFICATION);
		}
		return materialSegmentSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterType> getParameter() {
		if (parameter == null) {
			parameter = new EObjectContainmentEList<ParameterType>(ParameterType.class, this, B2MMLPackage.PROCESS_SEGMENT_TYPE__PARAMETER);
		}
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SegmentDependencyType> getSegmentDependency() {
		if (segmentDependency == null) {
			segmentDependency = new EObjectContainmentEList<SegmentDependencyType>(SegmentDependencyType.class, this, B2MMLPackage.PROCESS_SEGMENT_TYPE__SEGMENT_DEPENDENCY);
		}
		return segmentDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessSegmentType> getProcessSegment() {
		if (processSegment == null) {
			processSegment = new EObjectContainmentEList<ProcessSegmentType>(ProcessSegmentType.class, this, B2MMLPackage.PROCESS_SEGMENT_TYPE__PROCESS_SEGMENT);
		}
		return processSegment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__OPERATIONS_TYPE:
				return basicSetOperationsType(null, msgs);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__LOCATION:
				return basicSetLocation(null, msgs);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PUBLISHED_DATE:
				return basicSetPublishedDate(null, msgs);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PERSONNEL_SEGMENT_SPECIFICATION:
				return ((InternalEList<?>)getPersonnelSegmentSpecification()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__EQUIPMENT_SEGMENT_SPECIFICATION:
				return ((InternalEList<?>)getEquipmentSegmentSpecification()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PHYSICAL_ASSET_SEGMENT_SPECIFICATION:
				return ((InternalEList<?>)getPhysicalAssetSegmentSpecification()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__MATERIAL_SEGMENT_SPECIFICATION:
				return ((InternalEList<?>)getMaterialSegmentSpecification()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PARAMETER:
				return ((InternalEList<?>)getParameter()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__SEGMENT_DEPENDENCY:
				return ((InternalEList<?>)getSegmentDependency()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PROCESS_SEGMENT:
				return ((InternalEList<?>)getProcessSegment()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__ID:
				return getID();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__OPERATIONS_TYPE:
				return getOperationsType();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__LOCATION:
				return getLocation();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PUBLISHED_DATE:
				return getPublishedDate();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__DURATION:
				return getDuration();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PERSONNEL_SEGMENT_SPECIFICATION:
				return getPersonnelSegmentSpecification();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__EQUIPMENT_SEGMENT_SPECIFICATION:
				return getEquipmentSegmentSpecification();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PHYSICAL_ASSET_SEGMENT_SPECIFICATION:
				return getPhysicalAssetSegmentSpecification();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__MATERIAL_SEGMENT_SPECIFICATION:
				return getMaterialSegmentSpecification();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PARAMETER:
				return getParameter();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__SEGMENT_DEPENDENCY:
				return getSegmentDependency();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PROCESS_SEGMENT:
				return getProcessSegment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__LOCATION:
				setLocation((LocationType)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PUBLISHED_DATE:
				setPublishedDate((PublishedDateType)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__DURATION:
				setDuration((Duration)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PERSONNEL_SEGMENT_SPECIFICATION:
				getPersonnelSegmentSpecification().clear();
				getPersonnelSegmentSpecification().addAll((Collection<? extends PersonnelSegmentSpecificationType>)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__EQUIPMENT_SEGMENT_SPECIFICATION:
				getEquipmentSegmentSpecification().clear();
				getEquipmentSegmentSpecification().addAll((Collection<? extends EquipmentSegmentSpecificationType>)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PHYSICAL_ASSET_SEGMENT_SPECIFICATION:
				getPhysicalAssetSegmentSpecification().clear();
				getPhysicalAssetSegmentSpecification().addAll((Collection<? extends PhysicalAssetSegmentSpecificationType>)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__MATERIAL_SEGMENT_SPECIFICATION:
				getMaterialSegmentSpecification().clear();
				getMaterialSegmentSpecification().addAll((Collection<? extends MaterialSegmentSpecificationType>)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PARAMETER:
				getParameter().clear();
				getParameter().addAll((Collection<? extends ParameterType>)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__SEGMENT_DEPENDENCY:
				getSegmentDependency().clear();
				getSegmentDependency().addAll((Collection<? extends SegmentDependencyType>)newValue);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PROCESS_SEGMENT:
				getProcessSegment().clear();
				getProcessSegment().addAll((Collection<? extends ProcessSegmentType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)null);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__LOCATION:
				setLocation((LocationType)null);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PUBLISHED_DATE:
				setPublishedDate((PublishedDateType)null);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__DURATION:
				setDuration(DURATION_EDEFAULT);
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PERSONNEL_SEGMENT_SPECIFICATION:
				getPersonnelSegmentSpecification().clear();
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__EQUIPMENT_SEGMENT_SPECIFICATION:
				getEquipmentSegmentSpecification().clear();
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PHYSICAL_ASSET_SEGMENT_SPECIFICATION:
				getPhysicalAssetSegmentSpecification().clear();
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__MATERIAL_SEGMENT_SPECIFICATION:
				getMaterialSegmentSpecification().clear();
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PARAMETER:
				getParameter().clear();
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__SEGMENT_DEPENDENCY:
				getSegmentDependency().clear();
				return;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PROCESS_SEGMENT:
				getProcessSegment().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__ID:
				return iD != null;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__OPERATIONS_TYPE:
				return operationsType != null;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__LOCATION:
				return location != null;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PUBLISHED_DATE:
				return publishedDate != null;
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__DURATION:
				return DURATION_EDEFAULT == null ? duration != null : !DURATION_EDEFAULT.equals(duration);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PERSONNEL_SEGMENT_SPECIFICATION:
				return personnelSegmentSpecification != null && !personnelSegmentSpecification.isEmpty();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__EQUIPMENT_SEGMENT_SPECIFICATION:
				return equipmentSegmentSpecification != null && !equipmentSegmentSpecification.isEmpty();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PHYSICAL_ASSET_SEGMENT_SPECIFICATION:
				return physicalAssetSegmentSpecification != null && !physicalAssetSegmentSpecification.isEmpty();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__MATERIAL_SEGMENT_SPECIFICATION:
				return materialSegmentSpecification != null && !materialSegmentSpecification.isEmpty();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PARAMETER:
				return parameter != null && !parameter.isEmpty();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__SEGMENT_DEPENDENCY:
				return segmentDependency != null && !segmentDependency.isEmpty();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE__PROCESS_SEGMENT:
				return processSegment != null && !processSegment.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (duration: ");
		result.append(duration);
		result.append(')');
		return result.toString();
	}

} //ProcessSegmentTypeImpl
