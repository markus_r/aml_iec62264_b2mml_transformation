/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Actual ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetActualIDType()
 * @model extendedMetaData="name='PhysicalAssetActualIDType' kind='simple'"
 * @generated
 */
public interface PhysicalAssetActualIDType extends IdentifierType {
} // PhysicalAssetActualIDType
