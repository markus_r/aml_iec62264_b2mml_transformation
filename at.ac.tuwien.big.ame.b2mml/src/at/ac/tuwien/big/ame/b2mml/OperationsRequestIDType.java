/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Request ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsRequestIDType()
 * @model extendedMetaData="name='OperationsRequestIDType' kind='simple'"
 * @generated
 */
public interface OperationsRequestIDType extends IdentifierType {
} // OperationsRequestIDType
