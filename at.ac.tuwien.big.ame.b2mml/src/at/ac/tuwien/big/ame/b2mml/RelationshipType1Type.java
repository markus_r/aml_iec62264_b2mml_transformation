/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relationship Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getRelationshipType1Type()
 * @model extendedMetaData="name='RelationshipType1Type' kind='simple'"
 * @generated
 */
public interface RelationshipType1Type extends CodeType {
} // RelationshipType1Type
