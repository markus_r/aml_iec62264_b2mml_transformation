/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.CertificateOfAnalysisReferenceType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Certificate Of Analysis Reference Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CertificateOfAnalysisReferenceTypeImpl extends IdentifierTypeImpl implements CertificateOfAnalysisReferenceType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CertificateOfAnalysisReferenceTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getCertificateOfAnalysisReferenceType();
	}

} //CertificateOfAnalysisReferenceTypeImpl
