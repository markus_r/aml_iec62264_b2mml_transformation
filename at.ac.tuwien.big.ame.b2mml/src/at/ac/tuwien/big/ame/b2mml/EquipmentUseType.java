/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Use Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentUseType()
 * @model extendedMetaData="name='EquipmentUseType' kind='simple'"
 * @generated
 */
public interface EquipmentUseType extends CodeType {
} // EquipmentUseType
