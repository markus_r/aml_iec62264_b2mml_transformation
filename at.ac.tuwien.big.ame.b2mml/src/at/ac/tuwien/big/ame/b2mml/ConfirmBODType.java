/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Confirm BOD Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.ConfirmBODType#getApplicationArea <em>Application Area</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.ConfirmBODType#getDataArea <em>Data Area</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getConfirmBODType()
 * @model extendedMetaData="name='ConfirmBODType' kind='elementOnly'"
 * @generated
 */
public interface ConfirmBODType extends EObject {
	/**
	 * Returns the value of the '<em><b>Application Area</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application Area</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application Area</em>' containment reference.
	 * @see #setApplicationArea(TransApplicationAreaType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getConfirmBODType_ApplicationArea()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ApplicationArea' namespace='##targetNamespace'"
	 * @generated
	 */
	TransApplicationAreaType getApplicationArea();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.ConfirmBODType#getApplicationArea <em>Application Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Application Area</em>' containment reference.
	 * @see #getApplicationArea()
	 * @generated
	 */
	void setApplicationArea(TransApplicationAreaType value);

	/**
	 * Returns the value of the '<em><b>Data Area</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Area</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Area</em>' containment reference.
	 * @see #setDataArea(DataAreaType164)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getConfirmBODType_DataArea()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DataArea' namespace='##targetNamespace'"
	 * @generated
	 */
	DataAreaType164 getDataArea();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.ConfirmBODType#getDataArea <em>Data Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Area</em>' containment reference.
	 * @see #getDataArea()
	 * @generated
	 */
	void setDataArea(DataAreaType164 value);

} // ConfirmBODType
