/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Specification ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSpecificationIDType()
 * @model extendedMetaData="name='MaterialSpecificationIDType' kind='simple'"
 * @generated
 */
public interface MaterialSpecificationIDType extends IdentifierType {
} // MaterialSpecificationIDType
