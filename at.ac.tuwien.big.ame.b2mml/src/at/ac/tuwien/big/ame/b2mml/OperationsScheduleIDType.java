/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Schedule ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsScheduleIDType()
 * @model extendedMetaData="name='OperationsScheduleIDType' kind='simple'"
 * @generated
 */
public interface OperationsScheduleIDType extends IdentifierType {
} // OperationsScheduleIDType
