/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Op Segment Response Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getProcessSegmentID <em>Process Segment ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getActualStartTime <em>Actual Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getActualEndTime <em>Actual End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getOperationsDefinitionID <em>Operations Definition ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getSegmentState <em>Segment State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getSegmentData <em>Segment Data</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getPersonnelActual <em>Personnel Actual</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getEquipmentActual <em>Equipment Actual</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getPhysicalAssetActual <em>Physical Asset Actual</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getMaterialActual <em>Material Actual</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getSegmentResponse <em>Segment Response</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType()
 * @model extendedMetaData="name='OpSegmentResponseType' kind='elementOnly'"
 * @generated
 */
public interface OpSegmentResponseType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_ID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' containment reference.
	 * @see #setOperationsType(OperationsTypeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_OperationsType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsType' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsTypeType getOperationsType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getOperationsType <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' containment reference.
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsTypeType value);

	/**
	 * Returns the value of the '<em><b>Process Segment ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.ProcessSegmentIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_ProcessSegmentID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ProcessSegmentID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ProcessSegmentIDType> getProcessSegmentID();

	/**
	 * Returns the value of the '<em><b>Actual Start Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual Start Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual Start Time</em>' containment reference.
	 * @see #setActualStartTime(ActualStartTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_ActualStartTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ActualStartTime' namespace='##targetNamespace'"
	 * @generated
	 */
	ActualStartTimeType getActualStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getActualStartTime <em>Actual Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actual Start Time</em>' containment reference.
	 * @see #getActualStartTime()
	 * @generated
	 */
	void setActualStartTime(ActualStartTimeType value);

	/**
	 * Returns the value of the '<em><b>Actual End Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual End Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual End Time</em>' containment reference.
	 * @see #setActualEndTime(ActualEndTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_ActualEndTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ActualEndTime' namespace='##targetNamespace'"
	 * @generated
	 */
	ActualEndTimeType getActualEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getActualEndTime <em>Actual End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actual End Time</em>' containment reference.
	 * @see #getActualEndTime()
	 * @generated
	 */
	void setActualEndTime(ActualEndTimeType value);

	/**
	 * Returns the value of the '<em><b>Operations Definition ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OperationsDefinitionIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_OperationsDefinitionID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsDefinitionID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsDefinitionIDType> getOperationsDefinitionID();

	/**
	 * Returns the value of the '<em><b>Segment State</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment State</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment State</em>' containment reference.
	 * @see #setSegmentState(ResponseStateType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_SegmentState()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SegmentState' namespace='##targetNamespace'"
	 * @generated
	 */
	ResponseStateType getSegmentState();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getSegmentState <em>Segment State</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Segment State</em>' containment reference.
	 * @see #getSegmentState()
	 * @generated
	 */
	void setSegmentState(ResponseStateType value);

	/**
	 * Returns the value of the '<em><b>Segment Data</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpSegmentDataType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment Data</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment Data</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_SegmentData()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SegmentData' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpSegmentDataType> getSegmentData();

	/**
	 * Returns the value of the '<em><b>Personnel Actual</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpPersonnelActualType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Actual</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Actual</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_PersonnelActual()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonnelActual' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPersonnelActualType> getPersonnelActual();

	/**
	 * Returns the value of the '<em><b>Equipment Actual</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpEquipmentActualType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Actual</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Actual</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_EquipmentActual()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentActual' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpEquipmentActualType> getEquipmentActual();

	/**
	 * Returns the value of the '<em><b>Physical Asset Actual</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetActualType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Actual</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Actual</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_PhysicalAssetActual()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetActual' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPhysicalAssetActualType> getPhysicalAssetActual();

	/**
	 * Returns the value of the '<em><b>Material Actual</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpMaterialActualType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Actual</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Actual</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_MaterialActual()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialActual' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpMaterialActualType> getMaterialActual();

	/**
	 * Returns the value of the '<em><b>Segment Response</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment Response</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment Response</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_SegmentResponse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SegmentResponse' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpSegmentResponseType> getSegmentResponse();

	/**
	 * Returns the value of the '<em><b>Required By Requested Segment Response</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required By Requested Segment Response</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required By Requested Segment Response</em>' containment reference.
	 * @see #setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpSegmentResponseType_RequiredByRequestedSegmentResponse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='RequiredByRequestedSegmentResponse' namespace='##targetNamespace'"
	 * @generated
	 */
	RequiredByRequestedSegmentResponseType getRequiredByRequestedSegmentResponse();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpSegmentResponseType#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required By Requested Segment Response</em>' containment reference.
	 * @see #getRequiredByRequestedSegmentResponse()
	 * @generated
	 */
	void setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType value);

} // OpSegmentResponseType
