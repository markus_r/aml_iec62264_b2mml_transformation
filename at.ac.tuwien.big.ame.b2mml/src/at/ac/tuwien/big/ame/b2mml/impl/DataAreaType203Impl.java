/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DataAreaType203;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetInformationType;
import at.ac.tuwien.big.ame.b2mml.TransAcknowledgeType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type203</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType203Impl#getAcknowledge <em>Acknowledge</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType203Impl#getPhysicalAssetInformation <em>Physical Asset Information</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType203Impl extends MinimalEObjectImpl.Container implements DataAreaType203 {
	/**
	 * The cached value of the '{@link #getAcknowledge() <em>Acknowledge</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcknowledge()
	 * @generated
	 * @ordered
	 */
	protected TransAcknowledgeType acknowledge;

	/**
	 * The cached value of the '{@link #getPhysicalAssetInformation() <em>Physical Asset Information</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetInformation()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetInformationType> physicalAssetInformation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType203Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType203();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransAcknowledgeType getAcknowledge() {
		return acknowledge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledge(TransAcknowledgeType newAcknowledge, NotificationChain msgs) {
		TransAcknowledgeType oldAcknowledge = acknowledge;
		acknowledge = newAcknowledge;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE203__ACKNOWLEDGE, oldAcknowledge, newAcknowledge);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledge(TransAcknowledgeType newAcknowledge) {
		if (newAcknowledge != acknowledge) {
			NotificationChain msgs = null;
			if (acknowledge != null)
				msgs = ((InternalEObject)acknowledge).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE203__ACKNOWLEDGE, null, msgs);
			if (newAcknowledge != null)
				msgs = ((InternalEObject)newAcknowledge).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE203__ACKNOWLEDGE, null, msgs);
			msgs = basicSetAcknowledge(newAcknowledge, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE203__ACKNOWLEDGE, newAcknowledge, newAcknowledge));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetInformationType> getPhysicalAssetInformation() {
		if (physicalAssetInformation == null) {
			physicalAssetInformation = new EObjectContainmentEList<PhysicalAssetInformationType>(PhysicalAssetInformationType.class, this, B2MMLPackage.DATA_AREA_TYPE203__PHYSICAL_ASSET_INFORMATION);
		}
		return physicalAssetInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE203__ACKNOWLEDGE:
				return basicSetAcknowledge(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE203__PHYSICAL_ASSET_INFORMATION:
				return ((InternalEList<?>)getPhysicalAssetInformation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE203__ACKNOWLEDGE:
				return getAcknowledge();
			case B2MMLPackage.DATA_AREA_TYPE203__PHYSICAL_ASSET_INFORMATION:
				return getPhysicalAssetInformation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE203__ACKNOWLEDGE:
				setAcknowledge((TransAcknowledgeType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE203__PHYSICAL_ASSET_INFORMATION:
				getPhysicalAssetInformation().clear();
				getPhysicalAssetInformation().addAll((Collection<? extends PhysicalAssetInformationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE203__ACKNOWLEDGE:
				setAcknowledge((TransAcknowledgeType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE203__PHYSICAL_ASSET_INFORMATION:
				getPhysicalAssetInformation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE203__ACKNOWLEDGE:
				return acknowledge != null;
			case B2MMLPackage.DATA_AREA_TYPE203__PHYSICAL_ASSET_INFORMATION:
				return physicalAssetInformation != null && !physicalAssetInformation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType203Impl
