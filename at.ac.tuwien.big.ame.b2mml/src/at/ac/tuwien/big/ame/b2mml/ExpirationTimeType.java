/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expiration Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getExpirationTimeType()
 * @model extendedMetaData="name='ExpirationTimeType' kind='simple'"
 * @generated
 */
public interface ExpirationTimeType extends DateTimeType {
} // ExpirationTimeType
