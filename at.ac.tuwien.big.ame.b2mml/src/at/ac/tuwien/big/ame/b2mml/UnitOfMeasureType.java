/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unit Of Measure Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getUnitOfMeasureType()
 * @model extendedMetaData="name='UnitOfMeasureType' kind='simple'"
 * @generated
 */
public interface UnitOfMeasureType extends CodeType {
} // UnitOfMeasureType
