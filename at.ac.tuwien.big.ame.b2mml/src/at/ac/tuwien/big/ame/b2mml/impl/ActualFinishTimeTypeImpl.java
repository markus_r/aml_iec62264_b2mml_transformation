/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.ActualFinishTimeType;
import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actual Finish Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ActualFinishTimeTypeImpl extends DateTimeTypeImpl implements ActualFinishTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActualFinishTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getActualFinishTimeType();
	}

} //ActualFinishTimeTypeImpl
