/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Use1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialUse1Type()
 * @model extendedMetaData="name='MaterialUse1Type' kind='simple'"
 * @generated
 */
public interface MaterialUse1Type extends CodeType {
} // MaterialUse1Type
