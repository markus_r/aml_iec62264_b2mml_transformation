/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Status Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getStatusType()
 * @model extendedMetaData="name='StatusType' kind='simple'"
 * @generated
 */
public interface StatusType extends CodeType {
} // StatusType
