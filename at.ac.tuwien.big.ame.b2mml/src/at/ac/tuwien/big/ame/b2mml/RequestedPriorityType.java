/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requested Priority Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getRequestedPriorityType()
 * @model extendedMetaData="name='RequestedPriorityType' kind='simple'"
 * @generated
 */
public interface RequestedPriorityType extends NumericType {
} // RequestedPriorityType
