/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Other Dependency Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOtherDependencyType()
 * @model extendedMetaData="name='OtherDependencyType' kind='simple'"
 * @generated
 */
public interface OtherDependencyType extends CodeType {
} // OtherDependencyType
