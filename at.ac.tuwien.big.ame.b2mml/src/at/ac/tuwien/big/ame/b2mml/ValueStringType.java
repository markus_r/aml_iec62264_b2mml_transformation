/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value String Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getValueStringType()
 * @model extendedMetaData="name='ValueStringType' kind='simple'"
 * @generated
 */
public interface ValueStringType extends AnyGenericValueType {
} // ValueStringType
