/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resources Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getResourcesType()
 * @model extendedMetaData="name='ResourcesType' kind='simple'"
 * @generated
 */
public interface ResourcesType extends CodeType {
} // ResourcesType
