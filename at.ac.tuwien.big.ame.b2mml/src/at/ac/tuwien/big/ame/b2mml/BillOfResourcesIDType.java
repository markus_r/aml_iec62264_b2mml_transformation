/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bill Of Resources ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getBillOfResourcesIDType()
 * @model extendedMetaData="name='BillOfResourcesIDType' kind='simple'"
 * @generated
 */
public interface BillOfResourcesIDType extends IdentifierType {
} // BillOfResourcesIDType
