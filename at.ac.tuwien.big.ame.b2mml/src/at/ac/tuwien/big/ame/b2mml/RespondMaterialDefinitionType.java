/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Respond Material Definition Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialDefinitionType#getApplicationArea <em>Application Area</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialDefinitionType#getDataArea <em>Data Area</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialDefinitionType#getReleaseID <em>Release ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialDefinitionType#getVersionID <em>Version ID</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getRespondMaterialDefinitionType()
 * @model extendedMetaData="name='RespondMaterialDefinitionType' kind='elementOnly'"
 * @generated
 */
public interface RespondMaterialDefinitionType extends EObject {
	/**
	 * Returns the value of the '<em><b>Application Area</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application Area</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application Area</em>' containment reference.
	 * @see #setApplicationArea(TransApplicationAreaType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getRespondMaterialDefinitionType_ApplicationArea()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ApplicationArea' namespace='##targetNamespace'"
	 * @generated
	 */
	TransApplicationAreaType getApplicationArea();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialDefinitionType#getApplicationArea <em>Application Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Application Area</em>' containment reference.
	 * @see #getApplicationArea()
	 * @generated
	 */
	void setApplicationArea(TransApplicationAreaType value);

	/**
	 * Returns the value of the '<em><b>Data Area</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Area</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Area</em>' containment reference.
	 * @see #setDataArea(DataAreaType97)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getRespondMaterialDefinitionType_DataArea()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DataArea' namespace='##targetNamespace'"
	 * @generated
	 */
	DataAreaType97 getDataArea();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialDefinitionType#getDataArea <em>Data Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Area</em>' containment reference.
	 * @see #getDataArea()
	 * @generated
	 */
	void setDataArea(DataAreaType97 value);

	/**
	 * Returns the value of the '<em><b>Release ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Release ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Release ID</em>' attribute.
	 * @see #setReleaseID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getRespondMaterialDefinitionType_ReleaseID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString" required="true"
	 *        extendedMetaData="kind='attribute' name='releaseID'"
	 * @generated
	 */
	String getReleaseID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialDefinitionType#getReleaseID <em>Release ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Release ID</em>' attribute.
	 * @see #getReleaseID()
	 * @generated
	 */
	void setReleaseID(String value);

	/**
	 * Returns the value of the '<em><b>Version ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version ID</em>' attribute.
	 * @see #setVersionID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getRespondMaterialDefinitionType_VersionID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='versionID'"
	 * @generated
	 */
	String getVersionID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.RespondMaterialDefinitionType#getVersionID <em>Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version ID</em>' attribute.
	 * @see #getVersionID()
	 * @generated
	 */
	void setVersionID(String value);

} // RespondMaterialDefinitionType
