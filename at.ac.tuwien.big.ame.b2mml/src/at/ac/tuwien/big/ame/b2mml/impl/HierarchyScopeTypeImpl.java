/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.EquipmentElementLevelType;
import at.ac.tuwien.big.ame.b2mml.EquipmentIDType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hierarchy Scope Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.HierarchyScopeTypeImpl#getEquipmentID <em>Equipment ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.HierarchyScopeTypeImpl#getEquipmentElementLevel <em>Equipment Element Level</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.HierarchyScopeTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HierarchyScopeTypeImpl extends MinimalEObjectImpl.Container implements HierarchyScopeType {
	/**
	 * The cached value of the '{@link #getEquipmentID() <em>Equipment ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentID()
	 * @generated
	 * @ordered
	 */
	protected EquipmentIDType equipmentID;

	/**
	 * The cached value of the '{@link #getEquipmentElementLevel() <em>Equipment Element Level</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentElementLevel()
	 * @generated
	 * @ordered
	 */
	protected EquipmentElementLevelType equipmentElementLevel;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HierarchyScopeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getHierarchyScopeType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentIDType getEquipmentID() {
		return equipmentID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentID(EquipmentIDType newEquipmentID, NotificationChain msgs) {
		EquipmentIDType oldEquipmentID = equipmentID;
		equipmentID = newEquipmentID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ID, oldEquipmentID, newEquipmentID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentID(EquipmentIDType newEquipmentID) {
		if (newEquipmentID != equipmentID) {
			NotificationChain msgs = null;
			if (equipmentID != null)
				msgs = ((InternalEObject)equipmentID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ID, null, msgs);
			if (newEquipmentID != null)
				msgs = ((InternalEObject)newEquipmentID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ID, null, msgs);
			msgs = basicSetEquipmentID(newEquipmentID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ID, newEquipmentID, newEquipmentID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentElementLevelType getEquipmentElementLevel() {
		return equipmentElementLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentElementLevel(EquipmentElementLevelType newEquipmentElementLevel, NotificationChain msgs) {
		EquipmentElementLevelType oldEquipmentElementLevel = equipmentElementLevel;
		equipmentElementLevel = newEquipmentElementLevel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ELEMENT_LEVEL, oldEquipmentElementLevel, newEquipmentElementLevel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentElementLevel(EquipmentElementLevelType newEquipmentElementLevel) {
		if (newEquipmentElementLevel != equipmentElementLevel) {
			NotificationChain msgs = null;
			if (equipmentElementLevel != null)
				msgs = ((InternalEObject)equipmentElementLevel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ELEMENT_LEVEL, null, msgs);
			if (newEquipmentElementLevel != null)
				msgs = ((InternalEObject)newEquipmentElementLevel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ELEMENT_LEVEL, null, msgs);
			msgs = basicSetEquipmentElementLevel(newEquipmentElementLevel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ELEMENT_LEVEL, newEquipmentElementLevel, newEquipmentElementLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.HIERARCHY_SCOPE_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.HIERARCHY_SCOPE_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.HIERARCHY_SCOPE_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.HIERARCHY_SCOPE_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ID:
				return basicSetEquipmentID(null, msgs);
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				return basicSetEquipmentElementLevel(null, msgs);
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ID:
				return getEquipmentID();
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				return getEquipmentElementLevel();
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ID:
				setEquipmentID((EquipmentIDType)newValue);
				return;
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				setEquipmentElementLevel((EquipmentElementLevelType)newValue);
				return;
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ID:
				setEquipmentID((EquipmentIDType)null);
				return;
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				setEquipmentElementLevel((EquipmentElementLevelType)null);
				return;
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ID:
				return equipmentID != null;
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				return equipmentElementLevel != null;
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
		}
		return super.eIsSet(featureID);
	}

} //HierarchyScopeTypeImpl
