/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Job Order Command Rule Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getJobOrderCommandRuleType()
 * @model extendedMetaData="name='JobOrderCommandRuleType' kind='simple'"
 * @generated
 */
public interface JobOrderCommandRuleType extends TextType {
} // JobOrderCommandRuleType
