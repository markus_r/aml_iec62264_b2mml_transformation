/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DataAreaType26;
import at.ac.tuwien.big.ame.b2mml.MaterialSubLotType;
import at.ac.tuwien.big.ame.b2mml.TransRespondType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type26</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType26Impl#getRespond <em>Respond</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType26Impl#getMaterialSubLot <em>Material Sub Lot</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType26Impl extends MinimalEObjectImpl.Container implements DataAreaType26 {
	/**
	 * The cached value of the '{@link #getRespond() <em>Respond</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRespond()
	 * @generated
	 * @ordered
	 */
	protected TransRespondType respond;

	/**
	 * The cached value of the '{@link #getMaterialSubLot() <em>Material Sub Lot</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSubLot()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSubLotType> materialSubLot;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType26Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType26();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransRespondType getRespond() {
		return respond;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespond(TransRespondType newRespond, NotificationChain msgs) {
		TransRespondType oldRespond = respond;
		respond = newRespond;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE26__RESPOND, oldRespond, newRespond);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespond(TransRespondType newRespond) {
		if (newRespond != respond) {
			NotificationChain msgs = null;
			if (respond != null)
				msgs = ((InternalEObject)respond).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE26__RESPOND, null, msgs);
			if (newRespond != null)
				msgs = ((InternalEObject)newRespond).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE26__RESPOND, null, msgs);
			msgs = basicSetRespond(newRespond, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE26__RESPOND, newRespond, newRespond));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSubLotType> getMaterialSubLot() {
		if (materialSubLot == null) {
			materialSubLot = new EObjectContainmentEList<MaterialSubLotType>(MaterialSubLotType.class, this, B2MMLPackage.DATA_AREA_TYPE26__MATERIAL_SUB_LOT);
		}
		return materialSubLot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE26__RESPOND:
				return basicSetRespond(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE26__MATERIAL_SUB_LOT:
				return ((InternalEList<?>)getMaterialSubLot()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE26__RESPOND:
				return getRespond();
			case B2MMLPackage.DATA_AREA_TYPE26__MATERIAL_SUB_LOT:
				return getMaterialSubLot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE26__RESPOND:
				setRespond((TransRespondType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE26__MATERIAL_SUB_LOT:
				getMaterialSubLot().clear();
				getMaterialSubLot().addAll((Collection<? extends MaterialSubLotType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE26__RESPOND:
				setRespond((TransRespondType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE26__MATERIAL_SUB_LOT:
				getMaterialSubLot().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE26__RESPOND:
				return respond != null;
			case B2MMLPackage.DATA_AREA_TYPE26__MATERIAL_SUB_LOT:
				return materialSubLot != null && !materialSubLot.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType26Impl
