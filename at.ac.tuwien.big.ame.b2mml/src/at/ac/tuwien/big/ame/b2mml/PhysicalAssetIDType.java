/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetIDType()
 * @model extendedMetaData="name='PhysicalAssetIDType' kind='simple'"
 * @generated
 */
public interface PhysicalAssetIDType extends IdentifierType {
} // PhysicalAssetIDType
