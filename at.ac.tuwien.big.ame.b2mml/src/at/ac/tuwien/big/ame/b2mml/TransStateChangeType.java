/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans State Change Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransStateChangeType#getFromStateCode <em>From State Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransStateChangeType#getToStateCode <em>To State Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransStateChangeType#getChangeDateTime <em>Change Date Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransStateChangeType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransStateChangeType#getNote <em>Note</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransStateChangeType#getUserArea <em>User Area</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransStateChangeType()
 * @model extendedMetaData="name='TransStateChangeType' kind='elementOnly'"
 * @generated
 */
public interface TransStateChangeType extends EObject {
	/**
	 * Returns the value of the '<em><b>From State Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From State Code</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From State Code</em>' containment reference.
	 * @see #setFromStateCode(CodeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransStateChangeType_FromStateCode()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FromStateCode' namespace='##targetNamespace'"
	 * @generated
	 */
	CodeType getFromStateCode();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransStateChangeType#getFromStateCode <em>From State Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From State Code</em>' containment reference.
	 * @see #getFromStateCode()
	 * @generated
	 */
	void setFromStateCode(CodeType value);

	/**
	 * Returns the value of the '<em><b>To State Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To State Code</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To State Code</em>' containment reference.
	 * @see #setToStateCode(CodeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransStateChangeType_ToStateCode()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ToStateCode' namespace='##targetNamespace'"
	 * @generated
	 */
	CodeType getToStateCode();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransStateChangeType#getToStateCode <em>To State Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To State Code</em>' containment reference.
	 * @see #getToStateCode()
	 * @generated
	 */
	void setToStateCode(CodeType value);

	/**
	 * Returns the value of the '<em><b>Change Date Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Date Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Date Time</em>' containment reference.
	 * @see #setChangeDateTime(DateTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransStateChangeType_ChangeDateTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ChangeDateTime' namespace='##targetNamespace'"
	 * @generated
	 */
	DateTimeType getChangeDateTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransStateChangeType#getChangeDateTime <em>Change Date Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Date Time</em>' containment reference.
	 * @see #getChangeDateTime()
	 * @generated
	 */
	void setChangeDateTime(DateTimeType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransStateChangeType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Note</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.TextType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Note</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Note</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransStateChangeType_Note()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Note' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TextType> getNote();

	/**
	 * Returns the value of the '<em><b>User Area</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Area</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Area</em>' containment reference.
	 * @see #setUserArea(TransUserAreaType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransStateChangeType_UserArea()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UserArea' namespace='##targetNamespace'"
	 * @generated
	 */
	TransUserAreaType getUserArea();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransStateChangeType#getUserArea <em>User Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Area</em>' containment reference.
	 * @see #getUserArea()
	 * @generated
	 */
	void setUserArea(TransUserAreaType value);

} // TransStateChangeType
