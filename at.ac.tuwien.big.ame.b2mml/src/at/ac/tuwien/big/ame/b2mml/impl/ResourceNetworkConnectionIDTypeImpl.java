/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.ResourceNetworkConnectionIDType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource Network Connection ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ResourceNetworkConnectionIDTypeImpl extends IdentifierTypeImpl implements ResourceNetworkConnectionIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceNetworkConnectionIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getResourceNetworkConnectionIDType();
	}

} //ResourceNetworkConnectionIDTypeImpl
