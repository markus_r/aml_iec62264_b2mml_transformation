/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Latest End Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getLatestEndTimeType()
 * @model extendedMetaData="name='LatestEndTimeType' kind='simple'"
 * @generated
 */
public interface LatestEndTimeType extends DateTimeType {
} // LatestEndTimeType
