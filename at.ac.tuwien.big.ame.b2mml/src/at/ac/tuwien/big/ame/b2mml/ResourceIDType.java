/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getResourceIDType()
 * @model extendedMetaData="name='ResourceIDType' kind='simple'"
 * @generated
 */
public interface ResourceIDType extends IdentifierType {
} // ResourceIDType
