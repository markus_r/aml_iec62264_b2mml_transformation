/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.MaterialTestSpecificationIDType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Test Specification ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MaterialTestSpecificationIDTypeImpl extends IdentifierTypeImpl implements MaterialTestSpecificationIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialTestSpecificationIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getMaterialTestSpecificationIDType();
	}

} //MaterialTestSpecificationIDTypeImpl
