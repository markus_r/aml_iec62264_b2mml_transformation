/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Information Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getLocation <em>Location</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getMaterialClass <em>Material Class</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getMaterialDefinition <em>Material Definition</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getMaterialLot <em>Material Lot</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getMaterialSubLot <em>Material Sub Lot</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getMaterialTestSpecification <em>Material Test Specification</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialInformationType()
 * @model extendedMetaData="name='MaterialInformationType' kind='elementOnly'"
 * @generated
 */
public interface MaterialInformationType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #isSetID()
	 * @see #unsetID()
	 * @see #setID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialInformationType_ID()
	 * @model containment="true" unsettable="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #isSetID()
	 * @see #unsetID()
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Unsets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetID()
	 * @see #getID()
	 * @see #setID(IdentifierType)
	 * @generated
	 */
	void unsetID();

	/**
	 * Returns whether the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getID <em>ID</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ID</em>' containment reference is set.
	 * @see #unsetID()
	 * @see #getID()
	 * @see #setID(IdentifierType)
	 * @generated
	 */
	boolean isSetID();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialInformationType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Location</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' containment reference.
	 * @see #setLocation(LocationType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialInformationType_Location()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Location' namespace='##targetNamespace'"
	 * @generated
	 */
	LocationType getLocation();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getLocation <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' containment reference.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(LocationType value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #isSetHierarchyScope()
	 * @see #unsetHierarchyScope()
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialInformationType_HierarchyScope()
	 * @model containment="true" unsettable="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #isSetHierarchyScope()
	 * @see #unsetHierarchyScope()
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Unsets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHierarchyScope()
	 * @see #getHierarchyScope()
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @generated
	 */
	void unsetHierarchyScope();

	/**
	 * Returns whether the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Hierarchy Scope</em>' containment reference is set.
	 * @see #unsetHierarchyScope()
	 * @see #getHierarchyScope()
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @generated
	 */
	boolean isSetHierarchyScope();

	/**
	 * Returns the value of the '<em><b>Published Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Published Date</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Published Date</em>' containment reference.
	 * @see #isSetPublishedDate()
	 * @see #unsetPublishedDate()
	 * @see #setPublishedDate(PublishedDateType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialInformationType_PublishedDate()
	 * @model containment="true" unsettable="true"
	 *        extendedMetaData="kind='element' name='PublishedDate' namespace='##targetNamespace'"
	 * @generated
	 */
	PublishedDateType getPublishedDate();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getPublishedDate <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Published Date</em>' containment reference.
	 * @see #isSetPublishedDate()
	 * @see #unsetPublishedDate()
	 * @see #getPublishedDate()
	 * @generated
	 */
	void setPublishedDate(PublishedDateType value);

	/**
	 * Unsets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getPublishedDate <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPublishedDate()
	 * @see #getPublishedDate()
	 * @see #setPublishedDate(PublishedDateType)
	 * @generated
	 */
	void unsetPublishedDate();

	/**
	 * Returns whether the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType#getPublishedDate <em>Published Date</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Published Date</em>' containment reference is set.
	 * @see #unsetPublishedDate()
	 * @see #getPublishedDate()
	 * @see #setPublishedDate(PublishedDateType)
	 * @generated
	 */
	boolean isSetPublishedDate();

	/**
	 * Returns the value of the '<em><b>Material Class</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialClassType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Class</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Class</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialInformationType_MaterialClass()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialClass' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialClassType> getMaterialClass();

	/**
	 * Returns the value of the '<em><b>Material Definition</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialDefinitionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Definition</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialInformationType_MaterialDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialDefinitionType> getMaterialDefinition();

	/**
	 * Returns the value of the '<em><b>Material Lot</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialLotType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Lot</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Lot</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialInformationType_MaterialLot()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialLot' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialLotType> getMaterialLot();

	/**
	 * Returns the value of the '<em><b>Material Sub Lot</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Sub Lot</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Sub Lot</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialInformationType_MaterialSubLot()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialSubLot' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialSubLotType> getMaterialSubLot();

	/**
	 * Returns the value of the '<em><b>Material Test Specification</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialTestSpecificationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Test Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Test Specification</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialInformationType_MaterialTestSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialTestSpecificationType> getMaterialTestSpecification();

} // MaterialInformationType
