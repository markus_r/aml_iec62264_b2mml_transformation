/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Any Generic Value Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getCharacterSetCode <em>Character Set Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getCurrencyCodeListVersionID <em>Currency Code List Version ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getCurrencyID <em>Currency ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getEncodingCode <em>Encoding Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getFilename <em>Filename</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getFormat <em>Format</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getLanguageID <em>Language ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getLanguageLocaleID <em>Language Locale ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListAgencyID <em>List Agency ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListAgencyName <em>List Agency Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListID <em>List ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListName <em>List Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListSchemaURI <em>List Schema URI</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListURI <em>List URI</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListVersionID <em>List Version ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getMimeCode <em>Mime Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getName <em>Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaAgencyID <em>Schema Agency ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaAgencyName <em>Schema Agency Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaDataURI <em>Schema Data URI</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaID <em>Schema ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaName <em>Schema Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaURI <em>Schema URI</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaVersionID <em>Schema Version ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getUnitCode <em>Unit Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getUnitCodeListAgencyID <em>Unit Code List Agency ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getUnitCodeListAgencyName <em>Unit Code List Agency Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getUnitCodeListID <em>Unit Code List ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getUnitCodeListVersionID <em>Unit Code List Version ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getUri <em>Uri</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType()
 * @model extendedMetaData="name='AnyGenericValueType' kind='simple'"
 * @generated
 */
public interface AnyGenericValueType extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Character Set Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Character Set Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Character Set Code</em>' attribute.
	 * @see #setCharacterSetCode(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_CharacterSetCode()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='characterSetCode'"
	 * @generated
	 */
	String getCharacterSetCode();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getCharacterSetCode <em>Character Set Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Character Set Code</em>' attribute.
	 * @see #getCharacterSetCode()
	 * @generated
	 */
	void setCharacterSetCode(String value);

	/**
	 * Returns the value of the '<em><b>Currency Code List Version ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Currency Code List Version ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Currency Code List Version ID</em>' attribute.
	 * @see #setCurrencyCodeListVersionID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_CurrencyCodeListVersionID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='currencyCodeListVersionID'"
	 * @generated
	 */
	String getCurrencyCodeListVersionID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getCurrencyCodeListVersionID <em>Currency Code List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Currency Code List Version ID</em>' attribute.
	 * @see #getCurrencyCodeListVersionID()
	 * @generated
	 */
	void setCurrencyCodeListVersionID(String value);

	/**
	 * Returns the value of the '<em><b>Currency ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Currency ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Currency ID</em>' attribute.
	 * @see #setCurrencyID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_CurrencyID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='currencyID'"
	 * @generated
	 */
	String getCurrencyID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getCurrencyID <em>Currency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Currency ID</em>' attribute.
	 * @see #getCurrencyID()
	 * @generated
	 */
	void setCurrencyID(String value);

	/**
	 * Returns the value of the '<em><b>Encoding Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Encoding Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Encoding Code</em>' attribute.
	 * @see #setEncodingCode(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_EncodingCode()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='encodingCode'"
	 * @generated
	 */
	String getEncodingCode();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getEncodingCode <em>Encoding Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Encoding Code</em>' attribute.
	 * @see #getEncodingCode()
	 * @generated
	 */
	void setEncodingCode(String value);

	/**
	 * Returns the value of the '<em><b>Filename</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filename</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filename</em>' attribute.
	 * @see #setFilename(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_Filename()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='filename'"
	 * @generated
	 */
	String getFilename();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getFilename <em>Filename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filename</em>' attribute.
	 * @see #getFilename()
	 * @generated
	 */
	void setFilename(String value);

	/**
	 * Returns the value of the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Format</em>' attribute.
	 * @see #setFormat(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_Format()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='format'"
	 * @generated
	 */
	String getFormat();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getFormat <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Format</em>' attribute.
	 * @see #getFormat()
	 * @generated
	 */
	void setFormat(String value);

	/**
	 * Returns the value of the '<em><b>Language ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Language ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language ID</em>' attribute.
	 * @see #setLanguageID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_LanguageID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Language"
	 *        extendedMetaData="kind='attribute' name='languageID'"
	 * @generated
	 */
	String getLanguageID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getLanguageID <em>Language ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language ID</em>' attribute.
	 * @see #getLanguageID()
	 * @generated
	 */
	void setLanguageID(String value);

	/**
	 * Returns the value of the '<em><b>Language Locale ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Language Locale ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language Locale ID</em>' attribute.
	 * @see #setLanguageLocaleID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_LanguageLocaleID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='languageLocaleID'"
	 * @generated
	 */
	String getLanguageLocaleID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getLanguageLocaleID <em>Language Locale ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language Locale ID</em>' attribute.
	 * @see #getLanguageLocaleID()
	 * @generated
	 */
	void setLanguageLocaleID(String value);

	/**
	 * Returns the value of the '<em><b>List Agency ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Agency ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Agency ID</em>' attribute.
	 * @see #setListAgencyID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_ListAgencyID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='listAgencyID'"
	 * @generated
	 */
	String getListAgencyID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListAgencyID <em>List Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Agency ID</em>' attribute.
	 * @see #getListAgencyID()
	 * @generated
	 */
	void setListAgencyID(String value);

	/**
	 * Returns the value of the '<em><b>List Agency Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Agency Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Agency Name</em>' attribute.
	 * @see #setListAgencyName(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_ListAgencyName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='listAgencyName'"
	 * @generated
	 */
	String getListAgencyName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListAgencyName <em>List Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Agency Name</em>' attribute.
	 * @see #getListAgencyName()
	 * @generated
	 */
	void setListAgencyName(String value);

	/**
	 * Returns the value of the '<em><b>List ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List ID</em>' attribute.
	 * @see #setListID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_ListID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='listID'"
	 * @generated
	 */
	String getListID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListID <em>List ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List ID</em>' attribute.
	 * @see #getListID()
	 * @generated
	 */
	void setListID(String value);

	/**
	 * Returns the value of the '<em><b>List Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Name</em>' attribute.
	 * @see #setListName(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_ListName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='listName'"
	 * @generated
	 */
	String getListName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListName <em>List Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Name</em>' attribute.
	 * @see #getListName()
	 * @generated
	 */
	void setListName(String value);

	/**
	 * Returns the value of the '<em><b>List Schema URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Schema URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Schema URI</em>' attribute.
	 * @see #setListSchemaURI(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_ListSchemaURI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='listSchemaURI'"
	 * @generated
	 */
	String getListSchemaURI();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListSchemaURI <em>List Schema URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Schema URI</em>' attribute.
	 * @see #getListSchemaURI()
	 * @generated
	 */
	void setListSchemaURI(String value);

	/**
	 * Returns the value of the '<em><b>List URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List URI</em>' attribute.
	 * @see #setListURI(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_ListURI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='listURI'"
	 * @generated
	 */
	String getListURI();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListURI <em>List URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List URI</em>' attribute.
	 * @see #getListURI()
	 * @generated
	 */
	void setListURI(String value);

	/**
	 * Returns the value of the '<em><b>List Version ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Version ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Version ID</em>' attribute.
	 * @see #setListVersionID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_ListVersionID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='listVersionID'"
	 * @generated
	 */
	String getListVersionID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getListVersionID <em>List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Version ID</em>' attribute.
	 * @see #getListVersionID()
	 * @generated
	 */
	void setListVersionID(String value);

	/**
	 * Returns the value of the '<em><b>Mime Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mime Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mime Code</em>' attribute.
	 * @see #setMimeCode(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_MimeCode()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='mimeCode'"
	 * @generated
	 */
	String getMimeCode();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getMimeCode <em>Mime Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mime Code</em>' attribute.
	 * @see #getMimeCode()
	 * @generated
	 */
	void setMimeCode(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Schema Agency ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema Agency ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema Agency ID</em>' attribute.
	 * @see #setSchemaAgencyID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_SchemaAgencyID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='schemaAgencyID'"
	 * @generated
	 */
	String getSchemaAgencyID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaAgencyID <em>Schema Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema Agency ID</em>' attribute.
	 * @see #getSchemaAgencyID()
	 * @generated
	 */
	void setSchemaAgencyID(String value);

	/**
	 * Returns the value of the '<em><b>Schema Agency Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema Agency Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema Agency Name</em>' attribute.
	 * @see #setSchemaAgencyName(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_SchemaAgencyName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='schemaAgencyName'"
	 * @generated
	 */
	String getSchemaAgencyName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaAgencyName <em>Schema Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema Agency Name</em>' attribute.
	 * @see #getSchemaAgencyName()
	 * @generated
	 */
	void setSchemaAgencyName(String value);

	/**
	 * Returns the value of the '<em><b>Schema Data URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema Data URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema Data URI</em>' attribute.
	 * @see #setSchemaDataURI(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_SchemaDataURI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='schemaDataURI'"
	 * @generated
	 */
	String getSchemaDataURI();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaDataURI <em>Schema Data URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema Data URI</em>' attribute.
	 * @see #getSchemaDataURI()
	 * @generated
	 */
	void setSchemaDataURI(String value);

	/**
	 * Returns the value of the '<em><b>Schema ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema ID</em>' attribute.
	 * @see #setSchemaID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_SchemaID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='schemaID'"
	 * @generated
	 */
	String getSchemaID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaID <em>Schema ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema ID</em>' attribute.
	 * @see #getSchemaID()
	 * @generated
	 */
	void setSchemaID(String value);

	/**
	 * Returns the value of the '<em><b>Schema Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema Name</em>' attribute.
	 * @see #setSchemaName(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_SchemaName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='schemaName'"
	 * @generated
	 */
	String getSchemaName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaName <em>Schema Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema Name</em>' attribute.
	 * @see #getSchemaName()
	 * @generated
	 */
	void setSchemaName(String value);

	/**
	 * Returns the value of the '<em><b>Schema URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema URI</em>' attribute.
	 * @see #setSchemaURI(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_SchemaURI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='schemaURI'"
	 * @generated
	 */
	String getSchemaURI();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaURI <em>Schema URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema URI</em>' attribute.
	 * @see #getSchemaURI()
	 * @generated
	 */
	void setSchemaURI(String value);

	/**
	 * Returns the value of the '<em><b>Schema Version ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema Version ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema Version ID</em>' attribute.
	 * @see #setSchemaVersionID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_SchemaVersionID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='schemaVersionID'"
	 * @generated
	 */
	String getSchemaVersionID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getSchemaVersionID <em>Schema Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema Version ID</em>' attribute.
	 * @see #getSchemaVersionID()
	 * @generated
	 */
	void setSchemaVersionID(String value);

	/**
	 * Returns the value of the '<em><b>Unit Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Code</em>' attribute.
	 * @see #setUnitCode(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_UnitCode()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='unitCode'"
	 * @generated
	 */
	String getUnitCode();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getUnitCode <em>Unit Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Code</em>' attribute.
	 * @see #getUnitCode()
	 * @generated
	 */
	void setUnitCode(String value);

	/**
	 * Returns the value of the '<em><b>Unit Code List Agency ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Code List Agency ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Code List Agency ID</em>' attribute.
	 * @see #setUnitCodeListAgencyID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_UnitCodeListAgencyID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='unitCodeListAgencyID'"
	 * @generated
	 */
	String getUnitCodeListAgencyID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getUnitCodeListAgencyID <em>Unit Code List Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Code List Agency ID</em>' attribute.
	 * @see #getUnitCodeListAgencyID()
	 * @generated
	 */
	void setUnitCodeListAgencyID(String value);

	/**
	 * Returns the value of the '<em><b>Unit Code List Agency Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Code List Agency Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Code List Agency Name</em>' attribute.
	 * @see #setUnitCodeListAgencyName(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_UnitCodeListAgencyName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='unitCodeListAgencyName'"
	 * @generated
	 */
	String getUnitCodeListAgencyName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getUnitCodeListAgencyName <em>Unit Code List Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Code List Agency Name</em>' attribute.
	 * @see #getUnitCodeListAgencyName()
	 * @generated
	 */
	void setUnitCodeListAgencyName(String value);

	/**
	 * Returns the value of the '<em><b>Unit Code List ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Code List ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Code List ID</em>' attribute.
	 * @see #setUnitCodeListID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_UnitCodeListID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='unitCodeListID'"
	 * @generated
	 */
	String getUnitCodeListID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getUnitCodeListID <em>Unit Code List ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Code List ID</em>' attribute.
	 * @see #getUnitCodeListID()
	 * @generated
	 */
	void setUnitCodeListID(String value);

	/**
	 * Returns the value of the '<em><b>Unit Code List Version ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Code List Version ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Code List Version ID</em>' attribute.
	 * @see #setUnitCodeListVersionID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_UnitCodeListVersionID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='unitCodeListVersionID'"
	 * @generated
	 */
	String getUnitCodeListVersionID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getUnitCodeListVersionID <em>Unit Code List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Code List Version ID</em>' attribute.
	 * @see #getUnitCodeListVersionID()
	 * @generated
	 */
	void setUnitCodeListVersionID(String value);

	/**
	 * Returns the value of the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uri</em>' attribute.
	 * @see #setUri(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAnyGenericValueType_Uri()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='uri'"
	 * @generated
	 */
	String getUri();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.AnyGenericValueType#getUri <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uri</em>' attribute.
	 * @see #getUri()
	 * @generated
	 */
	void setUri(String value);

} // AnyGenericValueType
