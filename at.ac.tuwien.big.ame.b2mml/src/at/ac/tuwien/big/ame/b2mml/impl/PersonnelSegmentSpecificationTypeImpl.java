/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.CodeType;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.PersonIDType;
import at.ac.tuwien.big.ame.b2mml.PersonnelClassIDType;
import at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationPropertyType;
import at.ac.tuwien.big.ame.b2mml.PersonnelSegmentSpecificationType;
import at.ac.tuwien.big.ame.b2mml.QuantityValueType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Personnel Segment Specification Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PersonnelSegmentSpecificationTypeImpl#getPersonnelClassID <em>Personnel Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PersonnelSegmentSpecificationTypeImpl#getPersonID <em>Person ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PersonnelSegmentSpecificationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PersonnelSegmentSpecificationTypeImpl#getPersonnelUse <em>Personnel Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PersonnelSegmentSpecificationTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PersonnelSegmentSpecificationTypeImpl#getPersonnelSegmentSpecificationProperty <em>Personnel Segment Specification Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonnelSegmentSpecificationTypeImpl extends MinimalEObjectImpl.Container implements PersonnelSegmentSpecificationType {
	/**
	 * The cached value of the '{@link #getPersonnelClassID() <em>Personnel Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelClassID()
	 * @generated
	 * @ordered
	 */
	protected PersonnelClassIDType personnelClassID;

	/**
	 * The cached value of the '{@link #getPersonID() <em>Person ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonID()
	 * @generated
	 * @ordered
	 */
	protected PersonIDType personID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getPersonnelUse() <em>Personnel Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelUse()
	 * @generated
	 * @ordered
	 */
	protected CodeType personnelUse;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getPersonnelSegmentSpecificationProperty() <em>Personnel Segment Specification Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelSegmentSpecificationProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelSegmentSpecificationPropertyType> personnelSegmentSpecificationProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonnelSegmentSpecificationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPersonnelSegmentSpecificationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelClassIDType getPersonnelClassID() {
		return personnelClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPersonnelClassID(PersonnelClassIDType newPersonnelClassID, NotificationChain msgs) {
		PersonnelClassIDType oldPersonnelClassID = personnelClassID;
		personnelClassID = newPersonnelClassID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID, oldPersonnelClassID, newPersonnelClassID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonnelClassID(PersonnelClassIDType newPersonnelClassID) {
		if (newPersonnelClassID != personnelClassID) {
			NotificationChain msgs = null;
			if (personnelClassID != null)
				msgs = ((InternalEObject)personnelClassID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID, null, msgs);
			if (newPersonnelClassID != null)
				msgs = ((InternalEObject)newPersonnelClassID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID, null, msgs);
			msgs = basicSetPersonnelClassID(newPersonnelClassID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID, newPersonnelClassID, newPersonnelClassID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonIDType getPersonID() {
		return personID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPersonID(PersonIDType newPersonID, NotificationChain msgs) {
		PersonIDType oldPersonID = personID;
		personID = newPersonID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSON_ID, oldPersonID, newPersonID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonID(PersonIDType newPersonID) {
		if (newPersonID != personID) {
			NotificationChain msgs = null;
			if (personID != null)
				msgs = ((InternalEObject)personID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSON_ID, null, msgs);
			if (newPersonID != null)
				msgs = ((InternalEObject)newPersonID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSON_ID, null, msgs);
			msgs = basicSetPersonID(newPersonID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSON_ID, newPersonID, newPersonID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeType getPersonnelUse() {
		return personnelUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPersonnelUse(CodeType newPersonnelUse, NotificationChain msgs) {
		CodeType oldPersonnelUse = personnelUse;
		personnelUse = newPersonnelUse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_USE, oldPersonnelUse, newPersonnelUse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonnelUse(CodeType newPersonnelUse) {
		if (newPersonnelUse != personnelUse) {
			NotificationChain msgs = null;
			if (personnelUse != null)
				msgs = ((InternalEObject)personnelUse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_USE, null, msgs);
			if (newPersonnelUse != null)
				msgs = ((InternalEObject)newPersonnelUse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_USE, null, msgs);
			msgs = basicSetPersonnelUse(newPersonnelUse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_USE, newPersonnelUse, newPersonnelUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelSegmentSpecificationPropertyType> getPersonnelSegmentSpecificationProperty() {
		if (personnelSegmentSpecificationProperty == null) {
			personnelSegmentSpecificationProperty = new EObjectContainmentEList<PersonnelSegmentSpecificationPropertyType>(PersonnelSegmentSpecificationPropertyType.class, this, B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY);
		}
		return personnelSegmentSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID:
				return basicSetPersonnelClassID(null, msgs);
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSON_ID:
				return basicSetPersonID(null, msgs);
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_USE:
				return basicSetPersonnelUse(null, msgs);
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY:
				return ((InternalEList<?>)getPersonnelSegmentSpecificationProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID:
				return getPersonnelClassID();
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSON_ID:
				return getPersonID();
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_USE:
				return getPersonnelUse();
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY:
				return getPersonnelSegmentSpecificationProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID:
				setPersonnelClassID((PersonnelClassIDType)newValue);
				return;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSON_ID:
				setPersonID((PersonIDType)newValue);
				return;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_USE:
				setPersonnelUse((CodeType)newValue);
				return;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY:
				getPersonnelSegmentSpecificationProperty().clear();
				getPersonnelSegmentSpecificationProperty().addAll((Collection<? extends PersonnelSegmentSpecificationPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID:
				setPersonnelClassID((PersonnelClassIDType)null);
				return;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSON_ID:
				setPersonID((PersonIDType)null);
				return;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_USE:
				setPersonnelUse((CodeType)null);
				return;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY:
				getPersonnelSegmentSpecificationProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID:
				return personnelClassID != null;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSON_ID:
				return personID != null;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_USE:
				return personnelUse != null;
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE__PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY:
				return personnelSegmentSpecificationProperty != null && !personnelSegmentSpecificationProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PersonnelSegmentSpecificationTypeImpl
