/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Class Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassType#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassType#getManufacturer <em>Manufacturer</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassType#getPhysicalAssetClassProperty <em>Physical Asset Class Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassType#getPhysicalAssetID <em>Physical Asset ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassType#getPhysicalAssetCapabilityTestSpecificationID <em>Physical Asset Capability Test Specification ID</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetClassType()
 * @model extendedMetaData="name='PhysicalAssetClassType' kind='elementOnly'"
 * @generated
 */
public interface PhysicalAssetClassType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetClassType_ID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetClassType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetClassType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Manufacturer</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.NameType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Manufacturer</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Manufacturer</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetClassType_Manufacturer()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Manufacturer' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<NameType> getManufacturer();

	/**
	 * Returns the value of the '<em><b>Physical Asset Class Property</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Class Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Class Property</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetClassType_PhysicalAssetClassProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetClassProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetClassPropertyType> getPhysicalAssetClassProperty();

	/**
	 * Returns the value of the '<em><b>Physical Asset ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetClassType_PhysicalAssetID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetIDType> getPhysicalAssetID();

	/**
	 * Returns the value of the '<em><b>Physical Asset Capability Test Specification ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Capability Test Specification ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Capability Test Specification ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetClassType_PhysicalAssetCapabilityTestSpecificationID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetCapabilityTestSpecificationID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetCapabilityTestSpecificationIDType> getPhysicalAssetCapabilityTestSpecificationID();

} // PhysicalAssetClassType
