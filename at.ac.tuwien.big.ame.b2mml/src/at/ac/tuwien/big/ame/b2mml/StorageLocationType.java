/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Storage Location Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getStorageLocationType()
 * @model extendedMetaData="name='StorageLocationType' kind='simple'"
 * @generated
 */
public interface StorageLocationType extends IdentifierType {
} // StorageLocationType
