/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getParameterIDType()
 * @model extendedMetaData="name='ParameterIDType' kind='simple'"
 * @generated
 */
public interface ParameterIDType extends IdentifierType {
} // ParameterIDType
