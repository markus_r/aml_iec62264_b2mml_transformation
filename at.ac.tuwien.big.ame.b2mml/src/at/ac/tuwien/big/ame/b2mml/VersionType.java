/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Version Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getVersionType()
 * @model extendedMetaData="name='VersionType' kind='simple'"
 * @generated
 */
public interface VersionType extends IdentifierType {
} // VersionType
