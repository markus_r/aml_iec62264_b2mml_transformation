/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type2</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType2#getGet <em>Get</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType2#getOperationsDefinitionInformation <em>Operations Definition Information</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType2()
 * @model extendedMetaData="name='DataArea_._3_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType2 extends EObject {
	/**
	 * Returns the value of the '<em><b>Get</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get</em>' containment reference.
	 * @see #setGet(TransGetType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType2_Get()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Get' namespace='##targetNamespace'"
	 * @generated
	 */
	TransGetType getGet();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType2#getGet <em>Get</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get</em>' containment reference.
	 * @see #getGet()
	 * @generated
	 */
	void setGet(TransGetType value);

	/**
	 * Returns the value of the '<em><b>Operations Definition Information</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OperationsDefinitionInformationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition Information</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition Information</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType2_OperationsDefinitionInformation()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OperationsDefinitionInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsDefinitionInformationType> getOperationsDefinitionInformation();

} // DataAreaType2
