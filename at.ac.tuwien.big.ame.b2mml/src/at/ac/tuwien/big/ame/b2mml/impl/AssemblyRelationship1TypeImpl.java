/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.AssemblyRelationship1Type;
import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assembly Relationship1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AssemblyRelationship1TypeImpl extends CodeTypeImpl implements AssemblyRelationship1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssemblyRelationship1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getAssemblyRelationship1Type();
	}

} //AssemblyRelationship1TypeImpl
