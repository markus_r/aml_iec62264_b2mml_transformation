/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type67</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType67#getSync <em>Sync</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType67#getOperationsPerformance <em>Operations Performance</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType67()
 * @model extendedMetaData="name='DataArea_._68_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType67 extends EObject {
	/**
	 * Returns the value of the '<em><b>Sync</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync</em>' containment reference.
	 * @see #setSync(TransSyncType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType67_Sync()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Sync' namespace='##targetNamespace'"
	 * @generated
	 */
	TransSyncType getSync();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType67#getSync <em>Sync</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync</em>' containment reference.
	 * @see #getSync()
	 * @generated
	 */
	void setSync(TransSyncType value);

	/**
	 * Returns the value of the '<em><b>Operations Performance</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OperationsPerformanceType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Performance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Performance</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType67_OperationsPerformance()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OperationsPerformance' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsPerformanceType> getOperationsPerformance();

} // DataAreaType67
