/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Network Connection ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getResourceNetworkConnectionIDType()
 * @model extendedMetaData="name='ResourceNetworkConnectionIDType' kind='simple'"
 * @generated
 */
public interface ResourceNetworkConnectionIDType extends IdentifierType {
} // ResourceNetworkConnectionIDType
