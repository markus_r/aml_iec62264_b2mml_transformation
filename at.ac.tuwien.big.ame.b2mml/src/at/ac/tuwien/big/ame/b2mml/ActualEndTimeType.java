/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actual End Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getActualEndTimeType()
 * @model extendedMetaData="name='ActualEndTimeType' kind='simple'"
 * @generated
 */
public interface ActualEndTimeType extends DateTimeType {
} // ActualEndTimeType
