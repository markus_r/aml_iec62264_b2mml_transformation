/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonIDType()
 * @model extendedMetaData="name='PersonIDType' kind='simple'"
 * @generated
 */
public interface PersonIDType extends IdentifierType {
} // PersonIDType
