/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Use Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetUseType()
 * @model extendedMetaData="name='PhysicalAssetUseType' kind='simple'"
 * @generated
 */
public interface PhysicalAssetUseType extends CodeType {
} // PhysicalAssetUseType
