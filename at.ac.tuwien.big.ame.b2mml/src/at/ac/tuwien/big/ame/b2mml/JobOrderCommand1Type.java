/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Job Order Command1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getJobOrderCommand1Type()
 * @model extendedMetaData="name='JobOrderCommand1Type' kind='simple'"
 * @generated
 */
public interface JobOrderCommand1Type extends CodeType {
} // JobOrderCommand1Type
