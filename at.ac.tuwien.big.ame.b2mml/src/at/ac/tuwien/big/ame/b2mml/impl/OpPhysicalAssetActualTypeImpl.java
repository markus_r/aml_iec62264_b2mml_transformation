/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetActualPropertyType;
import at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetActualType;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassIDType;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetIDType;
import at.ac.tuwien.big.ame.b2mml.QuantityValueType;
import at.ac.tuwien.big.ame.b2mml.RequiredByRequestedSegmentResponseType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Physical Asset Actual Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPhysicalAssetActualTypeImpl#getPhysicalAssetClassID <em>Physical Asset Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPhysicalAssetActualTypeImpl#getPhysicalAssetID <em>Physical Asset ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPhysicalAssetActualTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPhysicalAssetActualTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPhysicalAssetActualTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPhysicalAssetActualTypeImpl#getPhysicalAssetActualProperty <em>Physical Asset Actual Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPhysicalAssetActualTypeImpl#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpPhysicalAssetActualTypeImpl extends MinimalEObjectImpl.Container implements OpPhysicalAssetActualType {
	/**
	 * The cached value of the '{@link #getPhysicalAssetClassID() <em>Physical Asset Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetClassIDType> physicalAssetClassID;

	/**
	 * The cached value of the '{@link #getPhysicalAssetID() <em>Physical Asset ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetID()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetIDType> physicalAssetID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getPhysicalAssetActualProperty() <em>Physical Asset Actual Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetActualProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPhysicalAssetActualPropertyType> physicalAssetActualProperty;

	/**
	 * The cached value of the '{@link #getRequiredByRequestedSegmentResponse() <em>Required By Requested Segment Response</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredByRequestedSegmentResponse()
	 * @generated
	 * @ordered
	 */
	protected RequiredByRequestedSegmentResponseType requiredByRequestedSegmentResponse;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpPhysicalAssetActualTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpPhysicalAssetActualType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetClassIDType> getPhysicalAssetClassID() {
		if (physicalAssetClassID == null) {
			physicalAssetClassID = new EObjectContainmentEList<PhysicalAssetClassIDType>(PhysicalAssetClassIDType.class, this, B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_CLASS_ID);
		}
		return physicalAssetClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetIDType> getPhysicalAssetID() {
		if (physicalAssetID == null) {
			physicalAssetID = new EObjectContainmentEList<PhysicalAssetIDType>(PhysicalAssetIDType.class, this, B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_ID);
		}
		return physicalAssetID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPhysicalAssetActualPropertyType> getPhysicalAssetActualProperty() {
		if (physicalAssetActualProperty == null) {
			physicalAssetActualProperty = new EObjectContainmentEList<OpPhysicalAssetActualPropertyType>(OpPhysicalAssetActualPropertyType.class, this, B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_ACTUAL_PROPERTY);
		}
		return physicalAssetActualProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredByRequestedSegmentResponseType getRequiredByRequestedSegmentResponse() {
		return requiredByRequestedSegmentResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType newRequiredByRequestedSegmentResponse, NotificationChain msgs) {
		RequiredByRequestedSegmentResponseType oldRequiredByRequestedSegmentResponse = requiredByRequestedSegmentResponse;
		requiredByRequestedSegmentResponse = newRequiredByRequestedSegmentResponse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, oldRequiredByRequestedSegmentResponse, newRequiredByRequestedSegmentResponse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType newRequiredByRequestedSegmentResponse) {
		if (newRequiredByRequestedSegmentResponse != requiredByRequestedSegmentResponse) {
			NotificationChain msgs = null;
			if (requiredByRequestedSegmentResponse != null)
				msgs = ((InternalEObject)requiredByRequestedSegmentResponse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, null, msgs);
			if (newRequiredByRequestedSegmentResponse != null)
				msgs = ((InternalEObject)newRequiredByRequestedSegmentResponse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, null, msgs);
			msgs = basicSetRequiredByRequestedSegmentResponse(newRequiredByRequestedSegmentResponse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, newRequiredByRequestedSegmentResponse, newRequiredByRequestedSegmentResponse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return ((InternalEList<?>)getPhysicalAssetClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_ID:
				return ((InternalEList<?>)getPhysicalAssetID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_ACTUAL_PROPERTY:
				return ((InternalEList<?>)getPhysicalAssetActualProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return basicSetRequiredByRequestedSegmentResponse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return getPhysicalAssetClassID();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_ID:
				return getPhysicalAssetID();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_ACTUAL_PROPERTY:
				return getPhysicalAssetActualProperty();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return getRequiredByRequestedSegmentResponse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_CLASS_ID:
				getPhysicalAssetClassID().clear();
				getPhysicalAssetClassID().addAll((Collection<? extends PhysicalAssetClassIDType>)newValue);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_ID:
				getPhysicalAssetID().clear();
				getPhysicalAssetID().addAll((Collection<? extends PhysicalAssetIDType>)newValue);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_ACTUAL_PROPERTY:
				getPhysicalAssetActualProperty().clear();
				getPhysicalAssetActualProperty().addAll((Collection<? extends OpPhysicalAssetActualPropertyType>)newValue);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				setRequiredByRequestedSegmentResponse((RequiredByRequestedSegmentResponseType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_CLASS_ID:
				getPhysicalAssetClassID().clear();
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_ID:
				getPhysicalAssetID().clear();
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_ACTUAL_PROPERTY:
				getPhysicalAssetActualProperty().clear();
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				setRequiredByRequestedSegmentResponse((RequiredByRequestedSegmentResponseType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return physicalAssetClassID != null && !physicalAssetClassID.isEmpty();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_ID:
				return physicalAssetID != null && !physicalAssetID.isEmpty();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__PHYSICAL_ASSET_ACTUAL_PROPERTY:
				return physicalAssetActualProperty != null && !physicalAssetActualProperty.isEmpty();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return requiredByRequestedSegmentResponse != null;
		}
		return super.eIsSet(featureID);
	}

} //OpPhysicalAssetActualTypeImpl
