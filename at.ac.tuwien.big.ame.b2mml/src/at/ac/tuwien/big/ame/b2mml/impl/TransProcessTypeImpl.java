/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.TransActionCriteriaType;
import at.ac.tuwien.big.ame.b2mml.TransProcessType;
import at.ac.tuwien.big.ame.b2mml.TransResponseCodeType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trans Process Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransProcessTypeImpl#getActionCriteria <em>Action Criteria</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransProcessTypeImpl#getAcknowledgeCode <em>Acknowledge Code</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransProcessTypeImpl extends MinimalEObjectImpl.Container implements TransProcessType {
	/**
	 * The cached value of the '{@link #getActionCriteria() <em>Action Criteria</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionCriteria()
	 * @generated
	 * @ordered
	 */
	protected EList<TransActionCriteriaType> actionCriteria;

	/**
	 * The default value of the '{@link #getAcknowledgeCode() <em>Acknowledge Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcknowledgeCode()
	 * @generated
	 * @ordered
	 */
	protected static final TransResponseCodeType ACKNOWLEDGE_CODE_EDEFAULT = TransResponseCodeType.ALWAYS;

	/**
	 * The cached value of the '{@link #getAcknowledgeCode() <em>Acknowledge Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcknowledgeCode()
	 * @generated
	 * @ordered
	 */
	protected TransResponseCodeType acknowledgeCode = ACKNOWLEDGE_CODE_EDEFAULT;

	/**
	 * This is true if the Acknowledge Code attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean acknowledgeCodeESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransProcessTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTransProcessType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TransActionCriteriaType> getActionCriteria() {
		if (actionCriteria == null) {
			actionCriteria = new EObjectContainmentEList<TransActionCriteriaType>(TransActionCriteriaType.class, this, B2MMLPackage.TRANS_PROCESS_TYPE__ACTION_CRITERIA);
		}
		return actionCriteria;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransResponseCodeType getAcknowledgeCode() {
		return acknowledgeCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeCode(TransResponseCodeType newAcknowledgeCode) {
		TransResponseCodeType oldAcknowledgeCode = acknowledgeCode;
		acknowledgeCode = newAcknowledgeCode == null ? ACKNOWLEDGE_CODE_EDEFAULT : newAcknowledgeCode;
		boolean oldAcknowledgeCodeESet = acknowledgeCodeESet;
		acknowledgeCodeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_PROCESS_TYPE__ACKNOWLEDGE_CODE, oldAcknowledgeCode, acknowledgeCode, !oldAcknowledgeCodeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAcknowledgeCode() {
		TransResponseCodeType oldAcknowledgeCode = acknowledgeCode;
		boolean oldAcknowledgeCodeESet = acknowledgeCodeESet;
		acknowledgeCode = ACKNOWLEDGE_CODE_EDEFAULT;
		acknowledgeCodeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.TRANS_PROCESS_TYPE__ACKNOWLEDGE_CODE, oldAcknowledgeCode, ACKNOWLEDGE_CODE_EDEFAULT, oldAcknowledgeCodeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAcknowledgeCode() {
		return acknowledgeCodeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TRANS_PROCESS_TYPE__ACTION_CRITERIA:
				return ((InternalEList<?>)getActionCriteria()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TRANS_PROCESS_TYPE__ACTION_CRITERIA:
				return getActionCriteria();
			case B2MMLPackage.TRANS_PROCESS_TYPE__ACKNOWLEDGE_CODE:
				return getAcknowledgeCode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TRANS_PROCESS_TYPE__ACTION_CRITERIA:
				getActionCriteria().clear();
				getActionCriteria().addAll((Collection<? extends TransActionCriteriaType>)newValue);
				return;
			case B2MMLPackage.TRANS_PROCESS_TYPE__ACKNOWLEDGE_CODE:
				setAcknowledgeCode((TransResponseCodeType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_PROCESS_TYPE__ACTION_CRITERIA:
				getActionCriteria().clear();
				return;
			case B2MMLPackage.TRANS_PROCESS_TYPE__ACKNOWLEDGE_CODE:
				unsetAcknowledgeCode();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_PROCESS_TYPE__ACTION_CRITERIA:
				return actionCriteria != null && !actionCriteria.isEmpty();
			case B2MMLPackage.TRANS_PROCESS_TYPE__ACKNOWLEDGE_CODE:
				return isSetAcknowledgeCode();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (acknowledgeCode: ");
		if (acknowledgeCodeESet) result.append(acknowledgeCode); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //TransProcessTypeImpl
