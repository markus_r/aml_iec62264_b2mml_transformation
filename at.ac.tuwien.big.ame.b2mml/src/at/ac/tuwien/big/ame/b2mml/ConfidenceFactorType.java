/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Confidence Factor Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getConfidenceFactorType()
 * @model extendedMetaData="name='ConfidenceFactorType' kind='simple'"
 * @generated
 */
public interface ConfidenceFactorType extends IdentifierType {
} // ConfidenceFactorType
