/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.EarliestStartTimeType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.LatestEndTimeType;
import at.ac.tuwien.big.ame.b2mml.OpEquipmentRequirementType;
import at.ac.tuwien.big.ame.b2mml.OpMaterialRequirementType;
import at.ac.tuwien.big.ame.b2mml.OpPersonnelRequirementType;
import at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetRequirementType;
import at.ac.tuwien.big.ame.b2mml.OpSegmentRequirementType;
import at.ac.tuwien.big.ame.b2mml.OperationsDefinitionIDType;
import at.ac.tuwien.big.ame.b2mml.OperationsTypeType;
import at.ac.tuwien.big.ame.b2mml.ParameterType;
import at.ac.tuwien.big.ame.b2mml.ProcessSegmentIDType;
import at.ac.tuwien.big.ame.b2mml.RequestStateType;
import at.ac.tuwien.big.ame.b2mml.RequiredByRequestedSegmentResponseType;

import java.util.Collection;

import javax.xml.datatype.Duration;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Segment Requirement Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getProcessSegmentID <em>Process Segment ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getEarliestStartTime <em>Earliest Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getLatestEndTime <em>Latest End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getDuration <em>Duration</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getOperationsDefinitionID <em>Operations Definition ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getSegmentState <em>Segment State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getSegmentParameter <em>Segment Parameter</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getPersonnelRequirement <em>Personnel Requirement</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getEquipmentRequirement <em>Equipment Requirement</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getPhysicalAssetRequirement <em>Physical Asset Requirement</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getMaterialRequirement <em>Material Requirement</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getSegmentRequirement <em>Segment Requirement</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpSegmentRequirementTypeImpl#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpSegmentRequirementTypeImpl extends MinimalEObjectImpl.Container implements OpSegmentRequirementType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsTypeType operationsType;

	/**
	 * The cached value of the '{@link #getProcessSegmentID() <em>Process Segment ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegmentID()
	 * @generated
	 * @ordered
	 */
	protected ProcessSegmentIDType processSegmentID;

	/**
	 * The cached value of the '{@link #getEarliestStartTime() <em>Earliest Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEarliestStartTime()
	 * @generated
	 * @ordered
	 */
	protected EarliestStartTimeType earliestStartTime;

	/**
	 * The cached value of the '{@link #getLatestEndTime() <em>Latest End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLatestEndTime()
	 * @generated
	 * @ordered
	 */
	protected LatestEndTimeType latestEndTime;

	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final Duration DURATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected Duration duration = DURATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOperationsDefinitionID() <em>Operations Definition ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsDefinitionID()
	 * @generated
	 * @ordered
	 */
	protected OperationsDefinitionIDType operationsDefinitionID;

	/**
	 * The cached value of the '{@link #getSegmentState() <em>Segment State</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentState()
	 * @generated
	 * @ordered
	 */
	protected RequestStateType segmentState;

	/**
	 * The cached value of the '{@link #getSegmentParameter() <em>Segment Parameter</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentParameter()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterType> segmentParameter;

	/**
	 * The cached value of the '{@link #getPersonnelRequirement() <em>Personnel Requirement</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelRequirement()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPersonnelRequirementType> personnelRequirement;

	/**
	 * The cached value of the '{@link #getEquipmentRequirement() <em>Equipment Requirement</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentRequirement()
	 * @generated
	 * @ordered
	 */
	protected EList<OpEquipmentRequirementType> equipmentRequirement;

	/**
	 * The cached value of the '{@link #getPhysicalAssetRequirement() <em>Physical Asset Requirement</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetRequirement()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPhysicalAssetRequirementType> physicalAssetRequirement;

	/**
	 * The cached value of the '{@link #getMaterialRequirement() <em>Material Requirement</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialRequirement()
	 * @generated
	 * @ordered
	 */
	protected EList<OpMaterialRequirementType> materialRequirement;

	/**
	 * The cached value of the '{@link #getSegmentRequirement() <em>Segment Requirement</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentRequirement()
	 * @generated
	 * @ordered
	 */
	protected EList<OpSegmentRequirementType> segmentRequirement;

	/**
	 * The cached value of the '{@link #getRequiredByRequestedSegmentResponse() <em>Required By Requested Segment Response</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredByRequestedSegmentResponse()
	 * @generated
	 * @ordered
	 */
	protected RequiredByRequestedSegmentResponseType requiredByRequestedSegmentResponse;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpSegmentRequirementTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpSegmentRequirementType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsTypeType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsType(OperationsTypeType newOperationsType, NotificationChain msgs) {
		OperationsTypeType oldOperationsType = operationsType;
		operationsType = newOperationsType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_TYPE, oldOperationsType, newOperationsType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsTypeType newOperationsType) {
		if (newOperationsType != operationsType) {
			NotificationChain msgs = null;
			if (operationsType != null)
				msgs = ((InternalEObject)operationsType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_TYPE, null, msgs);
			if (newOperationsType != null)
				msgs = ((InternalEObject)newOperationsType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_TYPE, null, msgs);
			msgs = basicSetOperationsType(newOperationsType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_TYPE, newOperationsType, newOperationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentIDType getProcessSegmentID() {
		return processSegmentID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessSegmentID(ProcessSegmentIDType newProcessSegmentID, NotificationChain msgs) {
		ProcessSegmentIDType oldProcessSegmentID = processSegmentID;
		processSegmentID = newProcessSegmentID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PROCESS_SEGMENT_ID, oldProcessSegmentID, newProcessSegmentID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessSegmentID(ProcessSegmentIDType newProcessSegmentID) {
		if (newProcessSegmentID != processSegmentID) {
			NotificationChain msgs = null;
			if (processSegmentID != null)
				msgs = ((InternalEObject)processSegmentID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PROCESS_SEGMENT_ID, null, msgs);
			if (newProcessSegmentID != null)
				msgs = ((InternalEObject)newProcessSegmentID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PROCESS_SEGMENT_ID, null, msgs);
			msgs = basicSetProcessSegmentID(newProcessSegmentID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PROCESS_SEGMENT_ID, newProcessSegmentID, newProcessSegmentID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarliestStartTimeType getEarliestStartTime() {
		return earliestStartTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEarliestStartTime(EarliestStartTimeType newEarliestStartTime, NotificationChain msgs) {
		EarliestStartTimeType oldEarliestStartTime = earliestStartTime;
		earliestStartTime = newEarliestStartTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EARLIEST_START_TIME, oldEarliestStartTime, newEarliestStartTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEarliestStartTime(EarliestStartTimeType newEarliestStartTime) {
		if (newEarliestStartTime != earliestStartTime) {
			NotificationChain msgs = null;
			if (earliestStartTime != null)
				msgs = ((InternalEObject)earliestStartTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EARLIEST_START_TIME, null, msgs);
			if (newEarliestStartTime != null)
				msgs = ((InternalEObject)newEarliestStartTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EARLIEST_START_TIME, null, msgs);
			msgs = basicSetEarliestStartTime(newEarliestStartTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EARLIEST_START_TIME, newEarliestStartTime, newEarliestStartTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LatestEndTimeType getLatestEndTime() {
		return latestEndTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLatestEndTime(LatestEndTimeType newLatestEndTime, NotificationChain msgs) {
		LatestEndTimeType oldLatestEndTime = latestEndTime;
		latestEndTime = newLatestEndTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__LATEST_END_TIME, oldLatestEndTime, newLatestEndTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLatestEndTime(LatestEndTimeType newLatestEndTime) {
		if (newLatestEndTime != latestEndTime) {
			NotificationChain msgs = null;
			if (latestEndTime != null)
				msgs = ((InternalEObject)latestEndTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__LATEST_END_TIME, null, msgs);
			if (newLatestEndTime != null)
				msgs = ((InternalEObject)newLatestEndTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__LATEST_END_TIME, null, msgs);
			msgs = basicSetLatestEndTime(newLatestEndTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__LATEST_END_TIME, newLatestEndTime, newLatestEndTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Duration getDuration() {
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDuration(Duration newDuration) {
		Duration oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__DURATION, oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinitionIDType getOperationsDefinitionID() {
		return operationsDefinitionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsDefinitionID(OperationsDefinitionIDType newOperationsDefinitionID, NotificationChain msgs) {
		OperationsDefinitionIDType oldOperationsDefinitionID = operationsDefinitionID;
		operationsDefinitionID = newOperationsDefinitionID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_DEFINITION_ID, oldOperationsDefinitionID, newOperationsDefinitionID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsDefinitionID(OperationsDefinitionIDType newOperationsDefinitionID) {
		if (newOperationsDefinitionID != operationsDefinitionID) {
			NotificationChain msgs = null;
			if (operationsDefinitionID != null)
				msgs = ((InternalEObject)operationsDefinitionID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_DEFINITION_ID, null, msgs);
			if (newOperationsDefinitionID != null)
				msgs = ((InternalEObject)newOperationsDefinitionID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_DEFINITION_ID, null, msgs);
			msgs = basicSetOperationsDefinitionID(newOperationsDefinitionID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_DEFINITION_ID, newOperationsDefinitionID, newOperationsDefinitionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequestStateType getSegmentState() {
		return segmentState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSegmentState(RequestStateType newSegmentState, NotificationChain msgs) {
		RequestStateType oldSegmentState = segmentState;
		segmentState = newSegmentState;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_STATE, oldSegmentState, newSegmentState);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSegmentState(RequestStateType newSegmentState) {
		if (newSegmentState != segmentState) {
			NotificationChain msgs = null;
			if (segmentState != null)
				msgs = ((InternalEObject)segmentState).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_STATE, null, msgs);
			if (newSegmentState != null)
				msgs = ((InternalEObject)newSegmentState).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_STATE, null, msgs);
			msgs = basicSetSegmentState(newSegmentState, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_STATE, newSegmentState, newSegmentState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterType> getSegmentParameter() {
		if (segmentParameter == null) {
			segmentParameter = new EObjectContainmentEList<ParameterType>(ParameterType.class, this, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_PARAMETER);
		}
		return segmentParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPersonnelRequirementType> getPersonnelRequirement() {
		if (personnelRequirement == null) {
			personnelRequirement = new EObjectContainmentEList<OpPersonnelRequirementType>(OpPersonnelRequirementType.class, this, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PERSONNEL_REQUIREMENT);
		}
		return personnelRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpEquipmentRequirementType> getEquipmentRequirement() {
		if (equipmentRequirement == null) {
			equipmentRequirement = new EObjectContainmentEList<OpEquipmentRequirementType>(OpEquipmentRequirementType.class, this, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EQUIPMENT_REQUIREMENT);
		}
		return equipmentRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPhysicalAssetRequirementType> getPhysicalAssetRequirement() {
		if (physicalAssetRequirement == null) {
			physicalAssetRequirement = new EObjectContainmentEList<OpPhysicalAssetRequirementType>(OpPhysicalAssetRequirementType.class, this, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PHYSICAL_ASSET_REQUIREMENT);
		}
		return physicalAssetRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpMaterialRequirementType> getMaterialRequirement() {
		if (materialRequirement == null) {
			materialRequirement = new EObjectContainmentEList<OpMaterialRequirementType>(OpMaterialRequirementType.class, this, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__MATERIAL_REQUIREMENT);
		}
		return materialRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpSegmentRequirementType> getSegmentRequirement() {
		if (segmentRequirement == null) {
			segmentRequirement = new EObjectContainmentEList<OpSegmentRequirementType>(OpSegmentRequirementType.class, this, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_REQUIREMENT);
		}
		return segmentRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredByRequestedSegmentResponseType getRequiredByRequestedSegmentResponse() {
		return requiredByRequestedSegmentResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType newRequiredByRequestedSegmentResponse, NotificationChain msgs) {
		RequiredByRequestedSegmentResponseType oldRequiredByRequestedSegmentResponse = requiredByRequestedSegmentResponse;
		requiredByRequestedSegmentResponse = newRequiredByRequestedSegmentResponse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, oldRequiredByRequestedSegmentResponse, newRequiredByRequestedSegmentResponse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType newRequiredByRequestedSegmentResponse) {
		if (newRequiredByRequestedSegmentResponse != requiredByRequestedSegmentResponse) {
			NotificationChain msgs = null;
			if (requiredByRequestedSegmentResponse != null)
				msgs = ((InternalEObject)requiredByRequestedSegmentResponse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, null, msgs);
			if (newRequiredByRequestedSegmentResponse != null)
				msgs = ((InternalEObject)newRequiredByRequestedSegmentResponse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, null, msgs);
			msgs = basicSetRequiredByRequestedSegmentResponse(newRequiredByRequestedSegmentResponse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, newRequiredByRequestedSegmentResponse, newRequiredByRequestedSegmentResponse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_TYPE:
				return basicSetOperationsType(null, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PROCESS_SEGMENT_ID:
				return basicSetProcessSegmentID(null, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EARLIEST_START_TIME:
				return basicSetEarliestStartTime(null, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__LATEST_END_TIME:
				return basicSetLatestEndTime(null, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_DEFINITION_ID:
				return basicSetOperationsDefinitionID(null, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_STATE:
				return basicSetSegmentState(null, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_PARAMETER:
				return ((InternalEList<?>)getSegmentParameter()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PERSONNEL_REQUIREMENT:
				return ((InternalEList<?>)getPersonnelRequirement()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EQUIPMENT_REQUIREMENT:
				return ((InternalEList<?>)getEquipmentRequirement()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PHYSICAL_ASSET_REQUIREMENT:
				return ((InternalEList<?>)getPhysicalAssetRequirement()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__MATERIAL_REQUIREMENT:
				return ((InternalEList<?>)getMaterialRequirement()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_REQUIREMENT:
				return ((InternalEList<?>)getSegmentRequirement()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return basicSetRequiredByRequestedSegmentResponse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__ID:
				return getID();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_TYPE:
				return getOperationsType();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PROCESS_SEGMENT_ID:
				return getProcessSegmentID();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EARLIEST_START_TIME:
				return getEarliestStartTime();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__LATEST_END_TIME:
				return getLatestEndTime();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__DURATION:
				return getDuration();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_DEFINITION_ID:
				return getOperationsDefinitionID();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_STATE:
				return getSegmentState();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_PARAMETER:
				return getSegmentParameter();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PERSONNEL_REQUIREMENT:
				return getPersonnelRequirement();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EQUIPMENT_REQUIREMENT:
				return getEquipmentRequirement();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PHYSICAL_ASSET_REQUIREMENT:
				return getPhysicalAssetRequirement();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__MATERIAL_REQUIREMENT:
				return getMaterialRequirement();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_REQUIREMENT:
				return getSegmentRequirement();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return getRequiredByRequestedSegmentResponse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PROCESS_SEGMENT_ID:
				setProcessSegmentID((ProcessSegmentIDType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EARLIEST_START_TIME:
				setEarliestStartTime((EarliestStartTimeType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__LATEST_END_TIME:
				setLatestEndTime((LatestEndTimeType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__DURATION:
				setDuration((Duration)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_DEFINITION_ID:
				setOperationsDefinitionID((OperationsDefinitionIDType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_STATE:
				setSegmentState((RequestStateType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_PARAMETER:
				getSegmentParameter().clear();
				getSegmentParameter().addAll((Collection<? extends ParameterType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PERSONNEL_REQUIREMENT:
				getPersonnelRequirement().clear();
				getPersonnelRequirement().addAll((Collection<? extends OpPersonnelRequirementType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EQUIPMENT_REQUIREMENT:
				getEquipmentRequirement().clear();
				getEquipmentRequirement().addAll((Collection<? extends OpEquipmentRequirementType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PHYSICAL_ASSET_REQUIREMENT:
				getPhysicalAssetRequirement().clear();
				getPhysicalAssetRequirement().addAll((Collection<? extends OpPhysicalAssetRequirementType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__MATERIAL_REQUIREMENT:
				getMaterialRequirement().clear();
				getMaterialRequirement().addAll((Collection<? extends OpMaterialRequirementType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_REQUIREMENT:
				getSegmentRequirement().clear();
				getSegmentRequirement().addAll((Collection<? extends OpSegmentRequirementType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				setRequiredByRequestedSegmentResponse((RequiredByRequestedSegmentResponseType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PROCESS_SEGMENT_ID:
				setProcessSegmentID((ProcessSegmentIDType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EARLIEST_START_TIME:
				setEarliestStartTime((EarliestStartTimeType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__LATEST_END_TIME:
				setLatestEndTime((LatestEndTimeType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__DURATION:
				setDuration(DURATION_EDEFAULT);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_DEFINITION_ID:
				setOperationsDefinitionID((OperationsDefinitionIDType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_STATE:
				setSegmentState((RequestStateType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_PARAMETER:
				getSegmentParameter().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PERSONNEL_REQUIREMENT:
				getPersonnelRequirement().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EQUIPMENT_REQUIREMENT:
				getEquipmentRequirement().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PHYSICAL_ASSET_REQUIREMENT:
				getPhysicalAssetRequirement().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__MATERIAL_REQUIREMENT:
				getMaterialRequirement().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_REQUIREMENT:
				getSegmentRequirement().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				setRequiredByRequestedSegmentResponse((RequiredByRequestedSegmentResponseType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__ID:
				return iD != null;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_TYPE:
				return operationsType != null;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PROCESS_SEGMENT_ID:
				return processSegmentID != null;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EARLIEST_START_TIME:
				return earliestStartTime != null;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__LATEST_END_TIME:
				return latestEndTime != null;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__DURATION:
				return DURATION_EDEFAULT == null ? duration != null : !DURATION_EDEFAULT.equals(duration);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__OPERATIONS_DEFINITION_ID:
				return operationsDefinitionID != null;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_STATE:
				return segmentState != null;
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_PARAMETER:
				return segmentParameter != null && !segmentParameter.isEmpty();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PERSONNEL_REQUIREMENT:
				return personnelRequirement != null && !personnelRequirement.isEmpty();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__EQUIPMENT_REQUIREMENT:
				return equipmentRequirement != null && !equipmentRequirement.isEmpty();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__PHYSICAL_ASSET_REQUIREMENT:
				return physicalAssetRequirement != null && !physicalAssetRequirement.isEmpty();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__MATERIAL_REQUIREMENT:
				return materialRequirement != null && !materialRequirement.isEmpty();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__SEGMENT_REQUIREMENT:
				return segmentRequirement != null && !segmentRequirement.isEmpty();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return requiredByRequestedSegmentResponse != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (duration: ");
		result.append(duration);
		result.append(')');
		return result.toString();
	}

} //OpSegmentRequirementTypeImpl
