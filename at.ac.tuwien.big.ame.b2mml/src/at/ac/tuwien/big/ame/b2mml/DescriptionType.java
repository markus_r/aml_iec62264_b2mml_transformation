/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Description Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDescriptionType()
 * @model extendedMetaData="name='DescriptionType' kind='simple'"
 * @generated
 */
public interface DescriptionType extends TextType {
} // DescriptionType
