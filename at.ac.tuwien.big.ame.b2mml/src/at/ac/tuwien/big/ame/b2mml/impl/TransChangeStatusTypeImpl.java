/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.CodeType;
import at.ac.tuwien.big.ame.b2mml.DateTimeType;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.TransChangeStatusType;
import at.ac.tuwien.big.ame.b2mml.TransStateChangeType;
import at.ac.tuwien.big.ame.b2mml.TransUserAreaType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trans Change Status Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransChangeStatusTypeImpl#getCode <em>Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransChangeStatusTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransChangeStatusTypeImpl#getEffectiveDateTime <em>Effective Date Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransChangeStatusTypeImpl#getReasonCode <em>Reason Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransChangeStatusTypeImpl#getReason <em>Reason</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransChangeStatusTypeImpl#getStateChange <em>State Change</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransChangeStatusTypeImpl#getUserArea <em>User Area</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransChangeStatusTypeImpl extends MinimalEObjectImpl.Container implements TransChangeStatusType {
	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected CodeType code;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected DescriptionType description;

	/**
	 * The cached value of the '{@link #getEffectiveDateTime() <em>Effective Date Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEffectiveDateTime()
	 * @generated
	 * @ordered
	 */
	protected DateTimeType effectiveDateTime;

	/**
	 * The cached value of the '{@link #getReasonCode() <em>Reason Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReasonCode()
	 * @generated
	 * @ordered
	 */
	protected CodeType reasonCode;

	/**
	 * The cached value of the '{@link #getReason() <em>Reason</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected EList<CodeType> reason;

	/**
	 * The cached value of the '{@link #getStateChange() <em>State Change</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateChange()
	 * @generated
	 * @ordered
	 */
	protected EList<TransStateChangeType> stateChange;

	/**
	 * The cached value of the '{@link #getUserArea() <em>User Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserArea()
	 * @generated
	 * @ordered
	 */
	protected TransUserAreaType userArea;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransChangeStatusTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTransChangeStatusType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeType getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCode(CodeType newCode, NotificationChain msgs) {
		CodeType oldCode = code;
		code = newCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__CODE, oldCode, newCode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(CodeType newCode) {
		if (newCode != code) {
			NotificationChain msgs = null;
			if (code != null)
				msgs = ((InternalEObject)code).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__CODE, null, msgs);
			if (newCode != null)
				msgs = ((InternalEObject)newCode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__CODE, null, msgs);
			msgs = basicSetCode(newCode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__CODE, newCode, newCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DescriptionType getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescription(DescriptionType newDescription, NotificationChain msgs) {
		DescriptionType oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__DESCRIPTION, oldDescription, newDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(DescriptionType newDescription) {
		if (newDescription != description) {
			NotificationChain msgs = null;
			if (description != null)
				msgs = ((InternalEObject)description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__DESCRIPTION, null, msgs);
			if (newDescription != null)
				msgs = ((InternalEObject)newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__DESCRIPTION, null, msgs);
			msgs = basicSetDescription(newDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__DESCRIPTION, newDescription, newDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTimeType getEffectiveDateTime() {
		return effectiveDateTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEffectiveDateTime(DateTimeType newEffectiveDateTime, NotificationChain msgs) {
		DateTimeType oldEffectiveDateTime = effectiveDateTime;
		effectiveDateTime = newEffectiveDateTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__EFFECTIVE_DATE_TIME, oldEffectiveDateTime, newEffectiveDateTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEffectiveDateTime(DateTimeType newEffectiveDateTime) {
		if (newEffectiveDateTime != effectiveDateTime) {
			NotificationChain msgs = null;
			if (effectiveDateTime != null)
				msgs = ((InternalEObject)effectiveDateTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__EFFECTIVE_DATE_TIME, null, msgs);
			if (newEffectiveDateTime != null)
				msgs = ((InternalEObject)newEffectiveDateTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__EFFECTIVE_DATE_TIME, null, msgs);
			msgs = basicSetEffectiveDateTime(newEffectiveDateTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__EFFECTIVE_DATE_TIME, newEffectiveDateTime, newEffectiveDateTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeType getReasonCode() {
		return reasonCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReasonCode(CodeType newReasonCode, NotificationChain msgs) {
		CodeType oldReasonCode = reasonCode;
		reasonCode = newReasonCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON_CODE, oldReasonCode, newReasonCode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReasonCode(CodeType newReasonCode) {
		if (newReasonCode != reasonCode) {
			NotificationChain msgs = null;
			if (reasonCode != null)
				msgs = ((InternalEObject)reasonCode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON_CODE, null, msgs);
			if (newReasonCode != null)
				msgs = ((InternalEObject)newReasonCode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON_CODE, null, msgs);
			msgs = basicSetReasonCode(newReasonCode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON_CODE, newReasonCode, newReasonCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CodeType> getReason() {
		if (reason == null) {
			reason = new EObjectContainmentEList<CodeType>(CodeType.class, this, B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON);
		}
		return reason;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TransStateChangeType> getStateChange() {
		if (stateChange == null) {
			stateChange = new EObjectContainmentEList<TransStateChangeType>(TransStateChangeType.class, this, B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__STATE_CHANGE);
		}
		return stateChange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransUserAreaType getUserArea() {
		return userArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUserArea(TransUserAreaType newUserArea, NotificationChain msgs) {
		TransUserAreaType oldUserArea = userArea;
		userArea = newUserArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__USER_AREA, oldUserArea, newUserArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUserArea(TransUserAreaType newUserArea) {
		if (newUserArea != userArea) {
			NotificationChain msgs = null;
			if (userArea != null)
				msgs = ((InternalEObject)userArea).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__USER_AREA, null, msgs);
			if (newUserArea != null)
				msgs = ((InternalEObject)newUserArea).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__USER_AREA, null, msgs);
			msgs = basicSetUserArea(newUserArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__USER_AREA, newUserArea, newUserArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__CODE:
				return basicSetCode(null, msgs);
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__DESCRIPTION:
				return basicSetDescription(null, msgs);
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__EFFECTIVE_DATE_TIME:
				return basicSetEffectiveDateTime(null, msgs);
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON_CODE:
				return basicSetReasonCode(null, msgs);
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON:
				return ((InternalEList<?>)getReason()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__STATE_CHANGE:
				return ((InternalEList<?>)getStateChange()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__USER_AREA:
				return basicSetUserArea(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__CODE:
				return getCode();
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__EFFECTIVE_DATE_TIME:
				return getEffectiveDateTime();
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON_CODE:
				return getReasonCode();
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON:
				return getReason();
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__STATE_CHANGE:
				return getStateChange();
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__USER_AREA:
				return getUserArea();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__CODE:
				setCode((CodeType)newValue);
				return;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__DESCRIPTION:
				setDescription((DescriptionType)newValue);
				return;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__EFFECTIVE_DATE_TIME:
				setEffectiveDateTime((DateTimeType)newValue);
				return;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON_CODE:
				setReasonCode((CodeType)newValue);
				return;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON:
				getReason().clear();
				getReason().addAll((Collection<? extends CodeType>)newValue);
				return;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__STATE_CHANGE:
				getStateChange().clear();
				getStateChange().addAll((Collection<? extends TransStateChangeType>)newValue);
				return;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__USER_AREA:
				setUserArea((TransUserAreaType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__CODE:
				setCode((CodeType)null);
				return;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__DESCRIPTION:
				setDescription((DescriptionType)null);
				return;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__EFFECTIVE_DATE_TIME:
				setEffectiveDateTime((DateTimeType)null);
				return;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON_CODE:
				setReasonCode((CodeType)null);
				return;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON:
				getReason().clear();
				return;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__STATE_CHANGE:
				getStateChange().clear();
				return;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__USER_AREA:
				setUserArea((TransUserAreaType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__CODE:
				return code != null;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__DESCRIPTION:
				return description != null;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__EFFECTIVE_DATE_TIME:
				return effectiveDateTime != null;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON_CODE:
				return reasonCode != null;
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__REASON:
				return reason != null && !reason.isEmpty();
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__STATE_CHANGE:
				return stateChange != null && !stateChange.isEmpty();
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE__USER_AREA:
				return userArea != null;
		}
		return super.eIsSet(featureID);
	}

} //TransChangeStatusTypeImpl
