/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetActualIDType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Actual ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PhysicalAssetActualIDTypeImpl extends IdentifierTypeImpl implements PhysicalAssetActualIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetActualIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPhysicalAssetActualIDType();
	}

} //PhysicalAssetActualIDTypeImpl
