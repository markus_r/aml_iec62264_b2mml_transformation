/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationType;
import at.ac.tuwien.big.ame.b2mml.OpMaterialSpecificationType;
import at.ac.tuwien.big.ame.b2mml.OpPersonnelSpecificationType;
import at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetSpecificationType;
import at.ac.tuwien.big.ame.b2mml.OperationsSegmentType;
import at.ac.tuwien.big.ame.b2mml.OperationsTypeType;
import at.ac.tuwien.big.ame.b2mml.ParameterType;
import at.ac.tuwien.big.ame.b2mml.ProcessSegmentIDType;
import at.ac.tuwien.big.ame.b2mml.SegmentDependencyType;

import java.util.Collection;

import javax.xml.datatype.Duration;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Segment Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getDuration <em>Duration</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getProcessSegmentID <em>Process Segment ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getParameter <em>Parameter</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getPersonnelSpecification <em>Personnel Specification</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getEquipmentSpecification <em>Equipment Specification</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getPhysicalAssetSpecification <em>Physical Asset Specification</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getMaterialSpecification <em>Material Specification</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getSegmentDependency <em>Segment Dependency</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsSegmentTypeImpl#getOperationsSegment <em>Operations Segment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsSegmentTypeImpl extends MinimalEObjectImpl.Container implements OperationsSegmentType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected DescriptionType description;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsTypeType operationsType;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final Duration DURATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected Duration duration = DURATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProcessSegmentID() <em>Process Segment ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegmentID()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessSegmentIDType> processSegmentID;

	/**
	 * The cached value of the '{@link #getParameter() <em>Parameter</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterType> parameter;

	/**
	 * The cached value of the '{@link #getPersonnelSpecification() <em>Personnel Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPersonnelSpecificationType> personnelSpecification;

	/**
	 * The cached value of the '{@link #getEquipmentSpecification() <em>Equipment Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<OpEquipmentSpecificationType> equipmentSpecification;

	/**
	 * The cached value of the '{@link #getPhysicalAssetSpecification() <em>Physical Asset Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPhysicalAssetSpecificationType> physicalAssetSpecification;

	/**
	 * The cached value of the '{@link #getMaterialSpecification() <em>Material Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<OpMaterialSpecificationType> materialSpecification;

	/**
	 * The cached value of the '{@link #getSegmentDependency() <em>Segment Dependency</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentDependency()
	 * @generated
	 * @ordered
	 */
	protected EList<SegmentDependencyType> segmentDependency;

	/**
	 * The cached value of the '{@link #getOperationsSegment() <em>Operations Segment</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsSegment()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsSegmentType> operationsSegment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsSegmentTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOperationsSegmentType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SEGMENT_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SEGMENT_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DescriptionType getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescription(DescriptionType newDescription, NotificationChain msgs) {
		DescriptionType oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DESCRIPTION, oldDescription, newDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(DescriptionType newDescription) {
		if (newDescription != description) {
			NotificationChain msgs = null;
			if (description != null)
				msgs = ((InternalEObject)description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DESCRIPTION, null, msgs);
			if (newDescription != null)
				msgs = ((InternalEObject)newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DESCRIPTION, null, msgs);
			msgs = basicSetDescription(newDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DESCRIPTION, newDescription, newDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsTypeType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsType(OperationsTypeType newOperationsType, NotificationChain msgs) {
		OperationsTypeType oldOperationsType = operationsType;
		operationsType = newOperationsType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_TYPE, oldOperationsType, newOperationsType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsTypeType newOperationsType) {
		if (newOperationsType != operationsType) {
			NotificationChain msgs = null;
			if (operationsType != null)
				msgs = ((InternalEObject)operationsType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_TYPE, null, msgs);
			if (newOperationsType != null)
				msgs = ((InternalEObject)newOperationsType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_TYPE, null, msgs);
			msgs = basicSetOperationsType(newOperationsType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_TYPE, newOperationsType, newOperationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SEGMENT_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SEGMENT_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Duration getDuration() {
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDuration(Duration newDuration) {
		Duration oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DURATION, oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessSegmentIDType> getProcessSegmentID() {
		if (processSegmentID == null) {
			processSegmentID = new EObjectContainmentEList<ProcessSegmentIDType>(ProcessSegmentIDType.class, this, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PROCESS_SEGMENT_ID);
		}
		return processSegmentID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterType> getParameter() {
		if (parameter == null) {
			parameter = new EObjectContainmentEList<ParameterType>(ParameterType.class, this, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PARAMETER);
		}
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPersonnelSpecificationType> getPersonnelSpecification() {
		if (personnelSpecification == null) {
			personnelSpecification = new EObjectContainmentEList<OpPersonnelSpecificationType>(OpPersonnelSpecificationType.class, this, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PERSONNEL_SPECIFICATION);
		}
		return personnelSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpEquipmentSpecificationType> getEquipmentSpecification() {
		if (equipmentSpecification == null) {
			equipmentSpecification = new EObjectContainmentEList<OpEquipmentSpecificationType>(OpEquipmentSpecificationType.class, this, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__EQUIPMENT_SPECIFICATION);
		}
		return equipmentSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPhysicalAssetSpecificationType> getPhysicalAssetSpecification() {
		if (physicalAssetSpecification == null) {
			physicalAssetSpecification = new EObjectContainmentEList<OpPhysicalAssetSpecificationType>(OpPhysicalAssetSpecificationType.class, this, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PHYSICAL_ASSET_SPECIFICATION);
		}
		return physicalAssetSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpMaterialSpecificationType> getMaterialSpecification() {
		if (materialSpecification == null) {
			materialSpecification = new EObjectContainmentEList<OpMaterialSpecificationType>(OpMaterialSpecificationType.class, this, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__MATERIAL_SPECIFICATION);
		}
		return materialSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SegmentDependencyType> getSegmentDependency() {
		if (segmentDependency == null) {
			segmentDependency = new EObjectContainmentEList<SegmentDependencyType>(SegmentDependencyType.class, this, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__SEGMENT_DEPENDENCY);
		}
		return segmentDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsSegmentType> getOperationsSegment() {
		if (operationsSegment == null) {
			operationsSegment = new EObjectContainmentEList<OperationsSegmentType>(OperationsSegmentType.class, this, B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_SEGMENT);
		}
		return operationsSegment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DESCRIPTION:
				return basicSetDescription(null, msgs);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_TYPE:
				return basicSetOperationsType(null, msgs);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PROCESS_SEGMENT_ID:
				return ((InternalEList<?>)getProcessSegmentID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PARAMETER:
				return ((InternalEList<?>)getParameter()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PERSONNEL_SPECIFICATION:
				return ((InternalEList<?>)getPersonnelSpecification()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__EQUIPMENT_SPECIFICATION:
				return ((InternalEList<?>)getEquipmentSpecification()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PHYSICAL_ASSET_SPECIFICATION:
				return ((InternalEList<?>)getPhysicalAssetSpecification()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__MATERIAL_SPECIFICATION:
				return ((InternalEList<?>)getMaterialSpecification()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__SEGMENT_DEPENDENCY:
				return ((InternalEList<?>)getSegmentDependency()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_SEGMENT:
				return ((InternalEList<?>)getOperationsSegment()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__ID:
				return getID();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_TYPE:
				return getOperationsType();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DURATION:
				return getDuration();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PROCESS_SEGMENT_ID:
				return getProcessSegmentID();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PARAMETER:
				return getParameter();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PERSONNEL_SPECIFICATION:
				return getPersonnelSpecification();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__EQUIPMENT_SPECIFICATION:
				return getEquipmentSpecification();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PHYSICAL_ASSET_SPECIFICATION:
				return getPhysicalAssetSpecification();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__MATERIAL_SPECIFICATION:
				return getMaterialSpecification();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__SEGMENT_DEPENDENCY:
				return getSegmentDependency();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_SEGMENT:
				return getOperationsSegment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DESCRIPTION:
				setDescription((DescriptionType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DURATION:
				setDuration((Duration)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PROCESS_SEGMENT_ID:
				getProcessSegmentID().clear();
				getProcessSegmentID().addAll((Collection<? extends ProcessSegmentIDType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PARAMETER:
				getParameter().clear();
				getParameter().addAll((Collection<? extends ParameterType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PERSONNEL_SPECIFICATION:
				getPersonnelSpecification().clear();
				getPersonnelSpecification().addAll((Collection<? extends OpPersonnelSpecificationType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__EQUIPMENT_SPECIFICATION:
				getEquipmentSpecification().clear();
				getEquipmentSpecification().addAll((Collection<? extends OpEquipmentSpecificationType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PHYSICAL_ASSET_SPECIFICATION:
				getPhysicalAssetSpecification().clear();
				getPhysicalAssetSpecification().addAll((Collection<? extends OpPhysicalAssetSpecificationType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__MATERIAL_SPECIFICATION:
				getMaterialSpecification().clear();
				getMaterialSpecification().addAll((Collection<? extends OpMaterialSpecificationType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__SEGMENT_DEPENDENCY:
				getSegmentDependency().clear();
				getSegmentDependency().addAll((Collection<? extends SegmentDependencyType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_SEGMENT:
				getOperationsSegment().clear();
				getOperationsSegment().addAll((Collection<? extends OperationsSegmentType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DESCRIPTION:
				setDescription((DescriptionType)null);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)null);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DURATION:
				setDuration(DURATION_EDEFAULT);
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PROCESS_SEGMENT_ID:
				getProcessSegmentID().clear();
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PARAMETER:
				getParameter().clear();
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PERSONNEL_SPECIFICATION:
				getPersonnelSpecification().clear();
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__EQUIPMENT_SPECIFICATION:
				getEquipmentSpecification().clear();
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PHYSICAL_ASSET_SPECIFICATION:
				getPhysicalAssetSpecification().clear();
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__MATERIAL_SPECIFICATION:
				getMaterialSpecification().clear();
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__SEGMENT_DEPENDENCY:
				getSegmentDependency().clear();
				return;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_SEGMENT:
				getOperationsSegment().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__ID:
				return iD != null;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DESCRIPTION:
				return description != null;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_TYPE:
				return operationsType != null;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__DURATION:
				return DURATION_EDEFAULT == null ? duration != null : !DURATION_EDEFAULT.equals(duration);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PROCESS_SEGMENT_ID:
				return processSegmentID != null && !processSegmentID.isEmpty();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PARAMETER:
				return parameter != null && !parameter.isEmpty();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PERSONNEL_SPECIFICATION:
				return personnelSpecification != null && !personnelSpecification.isEmpty();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__EQUIPMENT_SPECIFICATION:
				return equipmentSpecification != null && !equipmentSpecification.isEmpty();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__PHYSICAL_ASSET_SPECIFICATION:
				return physicalAssetSpecification != null && !physicalAssetSpecification.isEmpty();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__MATERIAL_SPECIFICATION:
				return materialSpecification != null && !materialSpecification.isEmpty();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__SEGMENT_DEPENDENCY:
				return segmentDependency != null && !segmentDependency.isEmpty();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE__OPERATIONS_SEGMENT:
				return operationsSegment != null && !operationsSegment.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (duration: ");
		result.append(duration);
		result.append(')');
		return result.toString();
	}

} //OperationsSegmentTypeImpl
