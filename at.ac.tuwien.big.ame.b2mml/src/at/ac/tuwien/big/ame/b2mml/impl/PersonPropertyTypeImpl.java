/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.PersonPropertyType;
import at.ac.tuwien.big.ame.b2mml.QualificationTestSpecificationIDType;
import at.ac.tuwien.big.ame.b2mml.TestResultType;
import at.ac.tuwien.big.ame.b2mml.ValueType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Person Property Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PersonPropertyTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PersonPropertyTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PersonPropertyTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PersonPropertyTypeImpl#getPersonProperty <em>Person Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PersonPropertyTypeImpl#getQualificationTestSpecificationID <em>Qualification Test Specification ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PersonPropertyTypeImpl#getTestResult <em>Test Result</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonPropertyTypeImpl extends MinimalEObjectImpl.Container implements PersonPropertyType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected EList<ValueType> value;

	/**
	 * The cached value of the '{@link #getPersonProperty() <em>Person Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonPropertyType> personProperty;

	/**
	 * The cached value of the '{@link #getQualificationTestSpecificationID() <em>Qualification Test Specification ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualificationTestSpecificationID()
	 * @generated
	 * @ordered
	 */
	protected EList<QualificationTestSpecificationIDType> qualificationTestSpecificationID;

	/**
	 * The cached value of the '{@link #getTestResult() <em>Test Result</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestResult()
	 * @generated
	 * @ordered
	 */
	protected EList<TestResultType> testResult;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonPropertyTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPersonPropertyType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSON_PROPERTY_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSON_PROPERTY_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSON_PROPERTY_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSON_PROPERTY_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.PERSON_PROPERTY_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueType> getValue() {
		if (value == null) {
			value = new EObjectContainmentEList<ValueType>(ValueType.class, this, B2MMLPackage.PERSON_PROPERTY_TYPE__VALUE);
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonPropertyType> getPersonProperty() {
		if (personProperty == null) {
			personProperty = new EObjectContainmentEList<PersonPropertyType>(PersonPropertyType.class, this, B2MMLPackage.PERSON_PROPERTY_TYPE__PERSON_PROPERTY);
		}
		return personProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QualificationTestSpecificationIDType> getQualificationTestSpecificationID() {
		if (qualificationTestSpecificationID == null) {
			qualificationTestSpecificationID = new EObjectContainmentEList<QualificationTestSpecificationIDType>(QualificationTestSpecificationIDType.class, this, B2MMLPackage.PERSON_PROPERTY_TYPE__QUALIFICATION_TEST_SPECIFICATION_ID);
		}
		return qualificationTestSpecificationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestResultType> getTestResult() {
		if (testResult == null) {
			testResult = new EObjectContainmentEList<TestResultType>(TestResultType.class, this, B2MMLPackage.PERSON_PROPERTY_TYPE__TEST_RESULT);
		}
		return testResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.PERSON_PROPERTY_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.PERSON_PROPERTY_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PERSON_PROPERTY_TYPE__VALUE:
				return ((InternalEList<?>)getValue()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PERSON_PROPERTY_TYPE__PERSON_PROPERTY:
				return ((InternalEList<?>)getPersonProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PERSON_PROPERTY_TYPE__QUALIFICATION_TEST_SPECIFICATION_ID:
				return ((InternalEList<?>)getQualificationTestSpecificationID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PERSON_PROPERTY_TYPE__TEST_RESULT:
				return ((InternalEList<?>)getTestResult()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.PERSON_PROPERTY_TYPE__ID:
				return getID();
			case B2MMLPackage.PERSON_PROPERTY_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.PERSON_PROPERTY_TYPE__VALUE:
				return getValue();
			case B2MMLPackage.PERSON_PROPERTY_TYPE__PERSON_PROPERTY:
				return getPersonProperty();
			case B2MMLPackage.PERSON_PROPERTY_TYPE__QUALIFICATION_TEST_SPECIFICATION_ID:
				return getQualificationTestSpecificationID();
			case B2MMLPackage.PERSON_PROPERTY_TYPE__TEST_RESULT:
				return getTestResult();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.PERSON_PROPERTY_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.PERSON_PROPERTY_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.PERSON_PROPERTY_TYPE__VALUE:
				getValue().clear();
				getValue().addAll((Collection<? extends ValueType>)newValue);
				return;
			case B2MMLPackage.PERSON_PROPERTY_TYPE__PERSON_PROPERTY:
				getPersonProperty().clear();
				getPersonProperty().addAll((Collection<? extends PersonPropertyType>)newValue);
				return;
			case B2MMLPackage.PERSON_PROPERTY_TYPE__QUALIFICATION_TEST_SPECIFICATION_ID:
				getQualificationTestSpecificationID().clear();
				getQualificationTestSpecificationID().addAll((Collection<? extends QualificationTestSpecificationIDType>)newValue);
				return;
			case B2MMLPackage.PERSON_PROPERTY_TYPE__TEST_RESULT:
				getTestResult().clear();
				getTestResult().addAll((Collection<? extends TestResultType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PERSON_PROPERTY_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.PERSON_PROPERTY_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.PERSON_PROPERTY_TYPE__VALUE:
				getValue().clear();
				return;
			case B2MMLPackage.PERSON_PROPERTY_TYPE__PERSON_PROPERTY:
				getPersonProperty().clear();
				return;
			case B2MMLPackage.PERSON_PROPERTY_TYPE__QUALIFICATION_TEST_SPECIFICATION_ID:
				getQualificationTestSpecificationID().clear();
				return;
			case B2MMLPackage.PERSON_PROPERTY_TYPE__TEST_RESULT:
				getTestResult().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PERSON_PROPERTY_TYPE__ID:
				return iD != null;
			case B2MMLPackage.PERSON_PROPERTY_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.PERSON_PROPERTY_TYPE__VALUE:
				return value != null && !value.isEmpty();
			case B2MMLPackage.PERSON_PROPERTY_TYPE__PERSON_PROPERTY:
				return personProperty != null && !personProperty.isEmpty();
			case B2MMLPackage.PERSON_PROPERTY_TYPE__QUALIFICATION_TEST_SPECIFICATION_ID:
				return qualificationTestSpecificationID != null && !qualificationTestSpecificationID.isEmpty();
			case B2MMLPackage.PERSON_PROPERTY_TYPE__TEST_RESULT:
				return testResult != null && !testResult.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PersonPropertyTypeImpl
