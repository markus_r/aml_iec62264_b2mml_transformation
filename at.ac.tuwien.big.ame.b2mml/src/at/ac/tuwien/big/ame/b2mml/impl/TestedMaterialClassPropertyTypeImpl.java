/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.MaterialClassIDType;
import at.ac.tuwien.big.ame.b2mml.PropertyIDType;
import at.ac.tuwien.big.ame.b2mml.TestedMaterialClassPropertyType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tested Material Class Property Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TestedMaterialClassPropertyTypeImpl#getMaterialClassID <em>Material Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TestedMaterialClassPropertyTypeImpl#getPropertyID <em>Property ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestedMaterialClassPropertyTypeImpl extends MinimalEObjectImpl.Container implements TestedMaterialClassPropertyType {
	/**
	 * The cached value of the '{@link #getMaterialClassID() <em>Material Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClassID()
	 * @generated
	 * @ordered
	 */
	protected MaterialClassIDType materialClassID;

	/**
	 * The cached value of the '{@link #getPropertyID() <em>Property ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyID()
	 * @generated
	 * @ordered
	 */
	protected PropertyIDType propertyID;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestedMaterialClassPropertyTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTestedMaterialClassPropertyType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialClassIDType getMaterialClassID() {
		return materialClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialClassID(MaterialClassIDType newMaterialClassID, NotificationChain msgs) {
		MaterialClassIDType oldMaterialClassID = materialClassID;
		materialClassID = newMaterialClassID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__MATERIAL_CLASS_ID, oldMaterialClassID, newMaterialClassID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialClassID(MaterialClassIDType newMaterialClassID) {
		if (newMaterialClassID != materialClassID) {
			NotificationChain msgs = null;
			if (materialClassID != null)
				msgs = ((InternalEObject)materialClassID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__MATERIAL_CLASS_ID, null, msgs);
			if (newMaterialClassID != null)
				msgs = ((InternalEObject)newMaterialClassID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__MATERIAL_CLASS_ID, null, msgs);
			msgs = basicSetMaterialClassID(newMaterialClassID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__MATERIAL_CLASS_ID, newMaterialClassID, newMaterialClassID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyIDType getPropertyID() {
		return propertyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPropertyID(PropertyIDType newPropertyID, NotificationChain msgs) {
		PropertyIDType oldPropertyID = propertyID;
		propertyID = newPropertyID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__PROPERTY_ID, oldPropertyID, newPropertyID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyID(PropertyIDType newPropertyID) {
		if (newPropertyID != propertyID) {
			NotificationChain msgs = null;
			if (propertyID != null)
				msgs = ((InternalEObject)propertyID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__PROPERTY_ID, null, msgs);
			if (newPropertyID != null)
				msgs = ((InternalEObject)newPropertyID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__PROPERTY_ID, null, msgs);
			msgs = basicSetPropertyID(newPropertyID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__PROPERTY_ID, newPropertyID, newPropertyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__MATERIAL_CLASS_ID:
				return basicSetMaterialClassID(null, msgs);
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				return basicSetPropertyID(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__MATERIAL_CLASS_ID:
				return getMaterialClassID();
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				return getPropertyID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__MATERIAL_CLASS_ID:
				setMaterialClassID((MaterialClassIDType)newValue);
				return;
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				setPropertyID((PropertyIDType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__MATERIAL_CLASS_ID:
				setMaterialClassID((MaterialClassIDType)null);
				return;
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				setPropertyID((PropertyIDType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__MATERIAL_CLASS_ID:
				return materialClassID != null;
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				return propertyID != null;
		}
		return super.eIsSet(featureID);
	}

} //TestedMaterialClassPropertyTypeImpl
