/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Work Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getWorkType1Type()
 * @model extendedMetaData="name='WorkType1Type' kind='simple'"
 * @generated
 */
public interface WorkType1Type extends CodeType {
} // WorkType1Type
