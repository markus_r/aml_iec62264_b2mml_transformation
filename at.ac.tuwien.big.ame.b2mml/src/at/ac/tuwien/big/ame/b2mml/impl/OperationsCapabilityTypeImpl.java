/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.CapabilityTypeType;
import at.ac.tuwien.big.ame.b2mml.ConfidenceFactorType;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.EndTimeType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.OpEquipmentCapabilityType;
import at.ac.tuwien.big.ame.b2mml.OpMaterialCapabilityType;
import at.ac.tuwien.big.ame.b2mml.OpPersonnelCapabilityType;
import at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType;
import at.ac.tuwien.big.ame.b2mml.OpProcessSegmentCapabilityType;
import at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType;
import at.ac.tuwien.big.ame.b2mml.PublishedDateType;
import at.ac.tuwien.big.ame.b2mml.ReasonType;
import at.ac.tuwien.big.ame.b2mml.StartTimeType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Capability Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getReason <em>Reason</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getPersonnelCapability <em>Personnel Capability</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getEquipmentCapability <em>Equipment Capability</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getPhysicalAssetCapability <em>Physical Asset Capability</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getMaterialCapability <em>Material Capability</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsCapabilityTypeImpl#getProcessSegmentCapability <em>Process Segment Capability</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsCapabilityTypeImpl extends MinimalEObjectImpl.Container implements OperationsCapabilityType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getCapabilityType() <em>Capability Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilityType()
	 * @generated
	 * @ordered
	 */
	protected CapabilityTypeType capabilityType;

	/**
	 * The cached value of the '{@link #getReason() <em>Reason</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected ReasonType reason;

	/**
	 * The cached value of the '{@link #getConfidenceFactor() <em>Confidence Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceFactor()
	 * @generated
	 * @ordered
	 */
	protected ConfidenceFactorType confidenceFactor;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected StartTimeType startTime;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected EndTimeType endTime;

	/**
	 * The cached value of the '{@link #getPublishedDate() <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublishedDate()
	 * @generated
	 * @ordered
	 */
	protected PublishedDateType publishedDate;

	/**
	 * The cached value of the '{@link #getPersonnelCapability() <em>Personnel Capability</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelCapability()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPersonnelCapabilityType> personnelCapability;

	/**
	 * The cached value of the '{@link #getEquipmentCapability() <em>Equipment Capability</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentCapability()
	 * @generated
	 * @ordered
	 */
	protected EList<OpEquipmentCapabilityType> equipmentCapability;

	/**
	 * The cached value of the '{@link #getPhysicalAssetCapability() <em>Physical Asset Capability</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPhysicalAssetCapabilityType> physicalAssetCapability;

	/**
	 * The cached value of the '{@link #getMaterialCapability() <em>Material Capability</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialCapability()
	 * @generated
	 * @ordered
	 */
	protected EList<OpMaterialCapabilityType> materialCapability;

	/**
	 * The cached value of the '{@link #getProcessSegmentCapability() <em>Process Segment Capability</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegmentCapability()
	 * @generated
	 * @ordered
	 */
	protected EList<OpProcessSegmentCapabilityType> processSegmentCapability;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsCapabilityTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOperationsCapabilityType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityTypeType getCapabilityType() {
		return capabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCapabilityType(CapabilityTypeType newCapabilityType, NotificationChain msgs) {
		CapabilityTypeType oldCapabilityType = capabilityType;
		capabilityType = newCapabilityType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CAPABILITY_TYPE, oldCapabilityType, newCapabilityType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCapabilityType(CapabilityTypeType newCapabilityType) {
		if (newCapabilityType != capabilityType) {
			NotificationChain msgs = null;
			if (capabilityType != null)
				msgs = ((InternalEObject)capabilityType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CAPABILITY_TYPE, null, msgs);
			if (newCapabilityType != null)
				msgs = ((InternalEObject)newCapabilityType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CAPABILITY_TYPE, null, msgs);
			msgs = basicSetCapabilityType(newCapabilityType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CAPABILITY_TYPE, newCapabilityType, newCapabilityType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReasonType getReason() {
		return reason;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReason(ReasonType newReason, NotificationChain msgs) {
		ReasonType oldReason = reason;
		reason = newReason;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__REASON, oldReason, newReason);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReason(ReasonType newReason) {
		if (newReason != reason) {
			NotificationChain msgs = null;
			if (reason != null)
				msgs = ((InternalEObject)reason).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__REASON, null, msgs);
			if (newReason != null)
				msgs = ((InternalEObject)newReason).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__REASON, null, msgs);
			msgs = basicSetReason(newReason, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__REASON, newReason, newReason));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfidenceFactorType getConfidenceFactor() {
		return confidenceFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConfidenceFactor(ConfidenceFactorType newConfidenceFactor, NotificationChain msgs) {
		ConfidenceFactorType oldConfidenceFactor = confidenceFactor;
		confidenceFactor = newConfidenceFactor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CONFIDENCE_FACTOR, oldConfidenceFactor, newConfidenceFactor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfidenceFactor(ConfidenceFactorType newConfidenceFactor) {
		if (newConfidenceFactor != confidenceFactor) {
			NotificationChain msgs = null;
			if (confidenceFactor != null)
				msgs = ((InternalEObject)confidenceFactor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CONFIDENCE_FACTOR, null, msgs);
			if (newConfidenceFactor != null)
				msgs = ((InternalEObject)newConfidenceFactor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CONFIDENCE_FACTOR, null, msgs);
			msgs = basicSetConfidenceFactor(newConfidenceFactor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CONFIDENCE_FACTOR, newConfidenceFactor, newConfidenceFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StartTimeType getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStartTime(StartTimeType newStartTime, NotificationChain msgs) {
		StartTimeType oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__START_TIME, oldStartTime, newStartTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(StartTimeType newStartTime) {
		if (newStartTime != startTime) {
			NotificationChain msgs = null;
			if (startTime != null)
				msgs = ((InternalEObject)startTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__START_TIME, null, msgs);
			if (newStartTime != null)
				msgs = ((InternalEObject)newStartTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__START_TIME, null, msgs);
			msgs = basicSetStartTime(newStartTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__START_TIME, newStartTime, newStartTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EndTimeType getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEndTime(EndTimeType newEndTime, NotificationChain msgs) {
		EndTimeType oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__END_TIME, oldEndTime, newEndTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(EndTimeType newEndTime) {
		if (newEndTime != endTime) {
			NotificationChain msgs = null;
			if (endTime != null)
				msgs = ((InternalEObject)endTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__END_TIME, null, msgs);
			if (newEndTime != null)
				msgs = ((InternalEObject)newEndTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__END_TIME, null, msgs);
			msgs = basicSetEndTime(newEndTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__END_TIME, newEndTime, newEndTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PublishedDateType getPublishedDate() {
		return publishedDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPublishedDate(PublishedDateType newPublishedDate, NotificationChain msgs) {
		PublishedDateType oldPublishedDate = publishedDate;
		publishedDate = newPublishedDate;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PUBLISHED_DATE, oldPublishedDate, newPublishedDate);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublishedDate(PublishedDateType newPublishedDate) {
		if (newPublishedDate != publishedDate) {
			NotificationChain msgs = null;
			if (publishedDate != null)
				msgs = ((InternalEObject)publishedDate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PUBLISHED_DATE, null, msgs);
			if (newPublishedDate != null)
				msgs = ((InternalEObject)newPublishedDate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PUBLISHED_DATE, null, msgs);
			msgs = basicSetPublishedDate(newPublishedDate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PUBLISHED_DATE, newPublishedDate, newPublishedDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPersonnelCapabilityType> getPersonnelCapability() {
		if (personnelCapability == null) {
			personnelCapability = new EObjectContainmentEList<OpPersonnelCapabilityType>(OpPersonnelCapabilityType.class, this, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PERSONNEL_CAPABILITY);
		}
		return personnelCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpEquipmentCapabilityType> getEquipmentCapability() {
		if (equipmentCapability == null) {
			equipmentCapability = new EObjectContainmentEList<OpEquipmentCapabilityType>(OpEquipmentCapabilityType.class, this, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__EQUIPMENT_CAPABILITY);
		}
		return equipmentCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPhysicalAssetCapabilityType> getPhysicalAssetCapability() {
		if (physicalAssetCapability == null) {
			physicalAssetCapability = new EObjectContainmentEList<OpPhysicalAssetCapabilityType>(OpPhysicalAssetCapabilityType.class, this, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PHYSICAL_ASSET_CAPABILITY);
		}
		return physicalAssetCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpMaterialCapabilityType> getMaterialCapability() {
		if (materialCapability == null) {
			materialCapability = new EObjectContainmentEList<OpMaterialCapabilityType>(OpMaterialCapabilityType.class, this, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__MATERIAL_CAPABILITY);
		}
		return materialCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpProcessSegmentCapabilityType> getProcessSegmentCapability() {
		if (processSegmentCapability == null) {
			processSegmentCapability = new EObjectContainmentEList<OpProcessSegmentCapabilityType>(OpProcessSegmentCapabilityType.class, this, B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PROCESS_SEGMENT_CAPABILITY);
		}
		return processSegmentCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CAPABILITY_TYPE:
				return basicSetCapabilityType(null, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__REASON:
				return basicSetReason(null, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				return basicSetConfidenceFactor(null, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__START_TIME:
				return basicSetStartTime(null, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__END_TIME:
				return basicSetEndTime(null, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PUBLISHED_DATE:
				return basicSetPublishedDate(null, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PERSONNEL_CAPABILITY:
				return ((InternalEList<?>)getPersonnelCapability()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__EQUIPMENT_CAPABILITY:
				return ((InternalEList<?>)getEquipmentCapability()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PHYSICAL_ASSET_CAPABILITY:
				return ((InternalEList<?>)getPhysicalAssetCapability()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__MATERIAL_CAPABILITY:
				return ((InternalEList<?>)getMaterialCapability()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PROCESS_SEGMENT_CAPABILITY:
				return ((InternalEList<?>)getProcessSegmentCapability()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__ID:
				return getID();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CAPABILITY_TYPE:
				return getCapabilityType();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__REASON:
				return getReason();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				return getConfidenceFactor();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__START_TIME:
				return getStartTime();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__END_TIME:
				return getEndTime();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PUBLISHED_DATE:
				return getPublishedDate();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PERSONNEL_CAPABILITY:
				return getPersonnelCapability();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__EQUIPMENT_CAPABILITY:
				return getEquipmentCapability();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PHYSICAL_ASSET_CAPABILITY:
				return getPhysicalAssetCapability();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__MATERIAL_CAPABILITY:
				return getMaterialCapability();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PROCESS_SEGMENT_CAPABILITY:
				return getProcessSegmentCapability();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CAPABILITY_TYPE:
				setCapabilityType((CapabilityTypeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__REASON:
				setReason((ReasonType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				setConfidenceFactor((ConfidenceFactorType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__START_TIME:
				setStartTime((StartTimeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__END_TIME:
				setEndTime((EndTimeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PUBLISHED_DATE:
				setPublishedDate((PublishedDateType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PERSONNEL_CAPABILITY:
				getPersonnelCapability().clear();
				getPersonnelCapability().addAll((Collection<? extends OpPersonnelCapabilityType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__EQUIPMENT_CAPABILITY:
				getEquipmentCapability().clear();
				getEquipmentCapability().addAll((Collection<? extends OpEquipmentCapabilityType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PHYSICAL_ASSET_CAPABILITY:
				getPhysicalAssetCapability().clear();
				getPhysicalAssetCapability().addAll((Collection<? extends OpPhysicalAssetCapabilityType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__MATERIAL_CAPABILITY:
				getMaterialCapability().clear();
				getMaterialCapability().addAll((Collection<? extends OpMaterialCapabilityType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PROCESS_SEGMENT_CAPABILITY:
				getProcessSegmentCapability().clear();
				getProcessSegmentCapability().addAll((Collection<? extends OpProcessSegmentCapabilityType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CAPABILITY_TYPE:
				setCapabilityType((CapabilityTypeType)null);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__REASON:
				setReason((ReasonType)null);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				setConfidenceFactor((ConfidenceFactorType)null);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__START_TIME:
				setStartTime((StartTimeType)null);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__END_TIME:
				setEndTime((EndTimeType)null);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PUBLISHED_DATE:
				setPublishedDate((PublishedDateType)null);
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PERSONNEL_CAPABILITY:
				getPersonnelCapability().clear();
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__EQUIPMENT_CAPABILITY:
				getEquipmentCapability().clear();
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PHYSICAL_ASSET_CAPABILITY:
				getPhysicalAssetCapability().clear();
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__MATERIAL_CAPABILITY:
				getMaterialCapability().clear();
				return;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PROCESS_SEGMENT_CAPABILITY:
				getProcessSegmentCapability().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__ID:
				return iD != null;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CAPABILITY_TYPE:
				return capabilityType != null;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__REASON:
				return reason != null;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				return confidenceFactor != null;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__START_TIME:
				return startTime != null;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__END_TIME:
				return endTime != null;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PUBLISHED_DATE:
				return publishedDate != null;
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PERSONNEL_CAPABILITY:
				return personnelCapability != null && !personnelCapability.isEmpty();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__EQUIPMENT_CAPABILITY:
				return equipmentCapability != null && !equipmentCapability.isEmpty();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PHYSICAL_ASSET_CAPABILITY:
				return physicalAssetCapability != null && !physicalAssetCapability.isEmpty();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__MATERIAL_CAPABILITY:
				return materialCapability != null && !materialCapability.isEmpty();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE__PROCESS_SEGMENT_CAPABILITY:
				return processSegmentCapability != null && !processSegmentCapability.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OperationsCapabilityTypeImpl
