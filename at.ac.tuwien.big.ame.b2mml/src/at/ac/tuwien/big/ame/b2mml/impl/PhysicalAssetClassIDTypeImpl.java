/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassIDType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Class ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PhysicalAssetClassIDTypeImpl extends IdentifierTypeImpl implements PhysicalAssetClassIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetClassIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPhysicalAssetClassIDType();
	}

} //PhysicalAssetClassIDTypeImpl
