/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Material Bill Item Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getMaterialClassID <em>Material Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getMaterialDefinitionID <em>Material Definition ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getUseType <em>Use Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getAssemblyBillOfMaterialItem <em>Assembly Bill Of Material Item</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getMaterialSpecificationID <em>Material Specification ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getQuantity <em>Quantity</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsMaterialBillItemType()
 * @model extendedMetaData="name='OperationsMaterialBillItemType' kind='elementOnly'"
 * @generated
 */
public interface OperationsMaterialBillItemType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsMaterialBillItemType_ID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(DescriptionType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsMaterialBillItemType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	DescriptionType getDescription();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(DescriptionType value);

	/**
	 * Returns the value of the '<em><b>Material Class ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialClassIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Class ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Class ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsMaterialBillItemType_MaterialClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialClassIDType> getMaterialClassID();

	/**
	 * Returns the value of the '<em><b>Material Definition ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialDefinitionIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Definition ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Definition ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsMaterialBillItemType_MaterialDefinitionID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialDefinitionID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialDefinitionIDType> getMaterialDefinitionID();

	/**
	 * Returns the value of the '<em><b>Use Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Type</em>' containment reference.
	 * @see #setUseType(CodeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsMaterialBillItemType_UseType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UseType' namespace='##targetNamespace'"
	 * @generated
	 */
	CodeType getUseType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getUseType <em>Use Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Type</em>' containment reference.
	 * @see #getUseType()
	 * @generated
	 */
	void setUseType(CodeType value);

	/**
	 * Returns the value of the '<em><b>Assembly Bill Of Material Item</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Bill Of Material Item</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Bill Of Material Item</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsMaterialBillItemType_AssemblyBillOfMaterialItem()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyBillOfMaterialItem' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsMaterialBillItemType> getAssemblyBillOfMaterialItem();

	/**
	 * Returns the value of the '<em><b>Assembly Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Type</em>' containment reference.
	 * @see #setAssemblyType(AssemblyTypeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsMaterialBillItemType_AssemblyType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyType' namespace='##targetNamespace'"
	 * @generated
	 */
	AssemblyTypeType getAssemblyType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getAssemblyType <em>Assembly Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Type</em>' containment reference.
	 * @see #getAssemblyType()
	 * @generated
	 */
	void setAssemblyType(AssemblyTypeType value);

	/**
	 * Returns the value of the '<em><b>Assembly Relationship</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Relationship</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Relationship</em>' containment reference.
	 * @see #setAssemblyRelationship(AssemblyRelationshipType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsMaterialBillItemType_AssemblyRelationship()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyRelationship' namespace='##targetNamespace'"
	 * @generated
	 */
	AssemblyRelationshipType getAssemblyRelationship();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsMaterialBillItemType#getAssemblyRelationship <em>Assembly Relationship</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Relationship</em>' containment reference.
	 * @see #getAssemblyRelationship()
	 * @generated
	 */
	void setAssemblyRelationship(AssemblyRelationshipType value);

	/**
	 * Returns the value of the '<em><b>Material Specification ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.IdentifierType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Specification ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Specification ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsMaterialBillItemType_MaterialSpecificationID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialSpecificationID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<IdentifierType> getMaterialSpecificationID();

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsMaterialBillItemType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

} // OperationsMaterialBillItemType
