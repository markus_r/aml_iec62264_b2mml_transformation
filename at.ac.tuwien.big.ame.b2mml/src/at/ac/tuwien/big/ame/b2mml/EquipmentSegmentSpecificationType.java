/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Segment Specification Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType#getEquipmentClassID <em>Equipment Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType#getEquipmentID <em>Equipment ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType#getEquipmentUse <em>Equipment Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType#getEquipmentSegmentSpecificationProperty <em>Equipment Segment Specification Property</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentSegmentSpecificationType()
 * @model extendedMetaData="name='EquipmentSegmentSpecificationType' kind='elementOnly'"
 * @generated
 */
public interface EquipmentSegmentSpecificationType extends EObject {
	/**
	 * Returns the value of the '<em><b>Equipment Class ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Class ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Class ID</em>' containment reference.
	 * @see #setEquipmentClassID(EquipmentClassIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentSegmentSpecificationType_EquipmentClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentClassIDType getEquipmentClassID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType#getEquipmentClassID <em>Equipment Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Class ID</em>' containment reference.
	 * @see #getEquipmentClassID()
	 * @generated
	 */
	void setEquipmentClassID(EquipmentClassIDType value);

	/**
	 * Returns the value of the '<em><b>Equipment ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment ID</em>' containment reference.
	 * @see #setEquipmentID(EquipmentIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentSegmentSpecificationType_EquipmentID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentID' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentIDType getEquipmentID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType#getEquipmentID <em>Equipment ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment ID</em>' containment reference.
	 * @see #getEquipmentID()
	 * @generated
	 */
	void setEquipmentID(EquipmentIDType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentSegmentSpecificationType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Equipment Use</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Use</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Use</em>' containment reference.
	 * @see #setEquipmentUse(CodeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentSegmentSpecificationType_EquipmentUse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentUse' namespace='##targetNamespace'"
	 * @generated
	 */
	CodeType getEquipmentUse();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType#getEquipmentUse <em>Equipment Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Use</em>' containment reference.
	 * @see #getEquipmentUse()
	 * @generated
	 */
	void setEquipmentUse(CodeType value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentSegmentSpecificationType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

	/**
	 * Returns the value of the '<em><b>Equipment Segment Specification Property</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Segment Specification Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Segment Specification Property</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentSegmentSpecificationType_EquipmentSegmentSpecificationProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentSegmentSpecificationProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentSegmentSpecificationPropertyType> getEquipmentSegmentSpecificationProperty();

} // EquipmentSegmentSpecificationType
