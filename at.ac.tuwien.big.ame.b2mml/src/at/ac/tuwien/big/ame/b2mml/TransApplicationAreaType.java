/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Application Area Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType#getSender <em>Sender</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType#getReceiver <em>Receiver</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType#getCreationDateTime <em>Creation Date Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType#getSignature <em>Signature</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType#getBODID <em>BODID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType#getUserArea <em>User Area</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransApplicationAreaType()
 * @model extendedMetaData="name='TransApplicationAreaType' kind='elementOnly'"
 * @generated
 */
public interface TransApplicationAreaType extends EObject {
	/**
	 * Returns the value of the '<em><b>Sender</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sender</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sender</em>' containment reference.
	 * @see #setSender(TransSenderType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransApplicationAreaType_Sender()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Sender' namespace='##targetNamespace'"
	 * @generated
	 */
	TransSenderType getSender();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType#getSender <em>Sender</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sender</em>' containment reference.
	 * @see #getSender()
	 * @generated
	 */
	void setSender(TransSenderType value);

	/**
	 * Returns the value of the '<em><b>Receiver</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.TransReceiverType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receiver</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receiver</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransApplicationAreaType_Receiver()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Receiver' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TransReceiverType> getReceiver();

	/**
	 * Returns the value of the '<em><b>Creation Date Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Creation Date Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Creation Date Time</em>' containment reference.
	 * @see #setCreationDateTime(DateTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransApplicationAreaType_CreationDateTime()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='CreationDateTime' namespace='##targetNamespace'"
	 * @generated
	 */
	DateTimeType getCreationDateTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType#getCreationDateTime <em>Creation Date Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Creation Date Time</em>' containment reference.
	 * @see #getCreationDateTime()
	 * @generated
	 */
	void setCreationDateTime(DateTimeType value);

	/**
	 * Returns the value of the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signature</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature</em>' containment reference.
	 * @see #setSignature(TransSignatureType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransApplicationAreaType_Signature()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Signature' namespace='##targetNamespace'"
	 * @generated
	 */
	TransSignatureType getSignature();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType#getSignature <em>Signature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signature</em>' containment reference.
	 * @see #getSignature()
	 * @generated
	 */
	void setSignature(TransSignatureType value);

	/**
	 * Returns the value of the '<em><b>BODID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>BODID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BODID</em>' containment reference.
	 * @see #setBODID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransApplicationAreaType_BODID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='BODID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getBODID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType#getBODID <em>BODID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BODID</em>' containment reference.
	 * @see #getBODID()
	 * @generated
	 */
	void setBODID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>User Area</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Area</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Area</em>' containment reference.
	 * @see #setUserArea(TransUserAreaType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransApplicationAreaType_UserArea()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UserArea' namespace='##targetNamespace'"
	 * @generated
	 */
	TransUserAreaType getUserArea();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType#getUserArea <em>User Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Area</em>' containment reference.
	 * @see #getUserArea()
	 * @generated
	 */
	void setUserArea(TransUserAreaType value);

} // TransApplicationAreaType
