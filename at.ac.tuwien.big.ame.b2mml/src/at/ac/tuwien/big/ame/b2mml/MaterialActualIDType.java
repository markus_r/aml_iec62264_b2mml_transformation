/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Actual ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialActualIDType()
 * @model extendedMetaData="name='MaterialActualIDType' kind='simple'"
 * @generated
 */
public interface MaterialActualIDType extends IdentifierType {
} // MaterialActualIDType
