/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.AssemblyRelationshipType;
import at.ac.tuwien.big.ame.b2mml.AssemblyTypeType;
import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.CapabilityTypeType;
import at.ac.tuwien.big.ame.b2mml.ConfidenceFactorType;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.EndTimeType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.MaterialClassIDType;
import at.ac.tuwien.big.ame.b2mml.MaterialDefinitionIDType;
import at.ac.tuwien.big.ame.b2mml.MaterialLotIDType;
import at.ac.tuwien.big.ame.b2mml.MaterialSubLotIDType;
import at.ac.tuwien.big.ame.b2mml.MaterialUseType;
import at.ac.tuwien.big.ame.b2mml.OpMaterialCapabilityPropertyType;
import at.ac.tuwien.big.ame.b2mml.OpMaterialCapabilityType;
import at.ac.tuwien.big.ame.b2mml.QuantityValueType;
import at.ac.tuwien.big.ame.b2mml.ReasonType;
import at.ac.tuwien.big.ame.b2mml.StartTimeType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Material Capability Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getMaterialClassID <em>Material Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getMaterialDefinitionID <em>Material Definition ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getMaterialLotID <em>Material Lot ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getMaterialSubLotID <em>Material Sub Lot ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getReason <em>Reason</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getAssemblyCapability <em>Assembly Capability</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpMaterialCapabilityTypeImpl#getMaterialCapabilityProperty <em>Material Capability Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpMaterialCapabilityTypeImpl extends MinimalEObjectImpl.Container implements OpMaterialCapabilityType {
	/**
	 * The cached value of the '{@link #getMaterialClassID() <em>Material Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialClassIDType> materialClassID;

	/**
	 * The cached value of the '{@link #getMaterialDefinitionID() <em>Material Definition ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinitionID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialDefinitionIDType> materialDefinitionID;

	/**
	 * The cached value of the '{@link #getMaterialLotID() <em>Material Lot ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialLotID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialLotIDType> materialLotID;

	/**
	 * The cached value of the '{@link #getMaterialSubLotID() <em>Material Sub Lot ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSubLotID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSubLotIDType> materialSubLotID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getCapabilityType() <em>Capability Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilityType()
	 * @generated
	 * @ordered
	 */
	protected CapabilityTypeType capabilityType;

	/**
	 * The cached value of the '{@link #getReason() <em>Reason</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected ReasonType reason;

	/**
	 * The cached value of the '{@link #getConfidenceFactor() <em>Confidence Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceFactor()
	 * @generated
	 * @ordered
	 */
	protected ConfidenceFactorType confidenceFactor;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getMaterialUse() <em>Material Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialUse()
	 * @generated
	 * @ordered
	 */
	protected MaterialUseType materialUse;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected StartTimeType startTime;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected EndTimeType endTime;

	/**
	 * The cached value of the '{@link #getAssemblyCapability() <em>Assembly Capability</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyCapability()
	 * @generated
	 * @ordered
	 */
	protected EList<OpMaterialCapabilityType> assemblyCapability;

	/**
	 * The cached value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected AssemblyTypeType assemblyType;

	/**
	 * The cached value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected AssemblyRelationshipType assemblyRelationship;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getMaterialCapabilityProperty() <em>Material Capability Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialCapabilityProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<OpMaterialCapabilityPropertyType> materialCapabilityProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpMaterialCapabilityTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpMaterialCapabilityType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialClassIDType> getMaterialClassID() {
		if (materialClassID == null) {
			materialClassID = new EObjectContainmentEList<MaterialClassIDType>(MaterialClassIDType.class, this, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_CLASS_ID);
		}
		return materialClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialDefinitionIDType> getMaterialDefinitionID() {
		if (materialDefinitionID == null) {
			materialDefinitionID = new EObjectContainmentEList<MaterialDefinitionIDType>(MaterialDefinitionIDType.class, this, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_DEFINITION_ID);
		}
		return materialDefinitionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialLotIDType> getMaterialLotID() {
		if (materialLotID == null) {
			materialLotID = new EObjectContainmentEList<MaterialLotIDType>(MaterialLotIDType.class, this, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_LOT_ID);
		}
		return materialLotID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSubLotIDType> getMaterialSubLotID() {
		if (materialSubLotID == null) {
			materialSubLotID = new EObjectContainmentEList<MaterialSubLotIDType>(MaterialSubLotIDType.class, this, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_SUB_LOT_ID);
		}
		return materialSubLotID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityTypeType getCapabilityType() {
		return capabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCapabilityType(CapabilityTypeType newCapabilityType, NotificationChain msgs) {
		CapabilityTypeType oldCapabilityType = capabilityType;
		capabilityType = newCapabilityType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CAPABILITY_TYPE, oldCapabilityType, newCapabilityType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCapabilityType(CapabilityTypeType newCapabilityType) {
		if (newCapabilityType != capabilityType) {
			NotificationChain msgs = null;
			if (capabilityType != null)
				msgs = ((InternalEObject)capabilityType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CAPABILITY_TYPE, null, msgs);
			if (newCapabilityType != null)
				msgs = ((InternalEObject)newCapabilityType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CAPABILITY_TYPE, null, msgs);
			msgs = basicSetCapabilityType(newCapabilityType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CAPABILITY_TYPE, newCapabilityType, newCapabilityType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReasonType getReason() {
		return reason;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReason(ReasonType newReason, NotificationChain msgs) {
		ReasonType oldReason = reason;
		reason = newReason;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__REASON, oldReason, newReason);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReason(ReasonType newReason) {
		if (newReason != reason) {
			NotificationChain msgs = null;
			if (reason != null)
				msgs = ((InternalEObject)reason).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__REASON, null, msgs);
			if (newReason != null)
				msgs = ((InternalEObject)newReason).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__REASON, null, msgs);
			msgs = basicSetReason(newReason, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__REASON, newReason, newReason));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfidenceFactorType getConfidenceFactor() {
		return confidenceFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConfidenceFactor(ConfidenceFactorType newConfidenceFactor, NotificationChain msgs) {
		ConfidenceFactorType oldConfidenceFactor = confidenceFactor;
		confidenceFactor = newConfidenceFactor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CONFIDENCE_FACTOR, oldConfidenceFactor, newConfidenceFactor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfidenceFactor(ConfidenceFactorType newConfidenceFactor) {
		if (newConfidenceFactor != confidenceFactor) {
			NotificationChain msgs = null;
			if (confidenceFactor != null)
				msgs = ((InternalEObject)confidenceFactor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CONFIDENCE_FACTOR, null, msgs);
			if (newConfidenceFactor != null)
				msgs = ((InternalEObject)newConfidenceFactor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CONFIDENCE_FACTOR, null, msgs);
			msgs = basicSetConfidenceFactor(newConfidenceFactor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CONFIDENCE_FACTOR, newConfidenceFactor, newConfidenceFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialUseType getMaterialUse() {
		return materialUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialUse(MaterialUseType newMaterialUse, NotificationChain msgs) {
		MaterialUseType oldMaterialUse = materialUse;
		materialUse = newMaterialUse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_USE, oldMaterialUse, newMaterialUse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialUse(MaterialUseType newMaterialUse) {
		if (newMaterialUse != materialUse) {
			NotificationChain msgs = null;
			if (materialUse != null)
				msgs = ((InternalEObject)materialUse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_USE, null, msgs);
			if (newMaterialUse != null)
				msgs = ((InternalEObject)newMaterialUse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_USE, null, msgs);
			msgs = basicSetMaterialUse(newMaterialUse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_USE, newMaterialUse, newMaterialUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StartTimeType getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStartTime(StartTimeType newStartTime, NotificationChain msgs) {
		StartTimeType oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__START_TIME, oldStartTime, newStartTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(StartTimeType newStartTime) {
		if (newStartTime != startTime) {
			NotificationChain msgs = null;
			if (startTime != null)
				msgs = ((InternalEObject)startTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__START_TIME, null, msgs);
			if (newStartTime != null)
				msgs = ((InternalEObject)newStartTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__START_TIME, null, msgs);
			msgs = basicSetStartTime(newStartTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__START_TIME, newStartTime, newStartTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EndTimeType getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEndTime(EndTimeType newEndTime, NotificationChain msgs) {
		EndTimeType oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__END_TIME, oldEndTime, newEndTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(EndTimeType newEndTime) {
		if (newEndTime != endTime) {
			NotificationChain msgs = null;
			if (endTime != null)
				msgs = ((InternalEObject)endTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__END_TIME, null, msgs);
			if (newEndTime != null)
				msgs = ((InternalEObject)newEndTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__END_TIME, null, msgs);
			msgs = basicSetEndTime(newEndTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__END_TIME, newEndTime, newEndTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpMaterialCapabilityType> getAssemblyCapability() {
		if (assemblyCapability == null) {
			assemblyCapability = new EObjectContainmentEList<OpMaterialCapabilityType>(OpMaterialCapabilityType.class, this, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_CAPABILITY);
		}
		return assemblyCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyTypeType getAssemblyType() {
		return assemblyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyType(AssemblyTypeType newAssemblyType, NotificationChain msgs) {
		AssemblyTypeType oldAssemblyType = assemblyType;
		assemblyType = newAssemblyType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_TYPE, oldAssemblyType, newAssemblyType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyType(AssemblyTypeType newAssemblyType) {
		if (newAssemblyType != assemblyType) {
			NotificationChain msgs = null;
			if (assemblyType != null)
				msgs = ((InternalEObject)assemblyType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_TYPE, null, msgs);
			if (newAssemblyType != null)
				msgs = ((InternalEObject)newAssemblyType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_TYPE, null, msgs);
			msgs = basicSetAssemblyType(newAssemblyType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_TYPE, newAssemblyType, newAssemblyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationshipType getAssemblyRelationship() {
		return assemblyRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship, NotificationChain msgs) {
		AssemblyRelationshipType oldAssemblyRelationship = assemblyRelationship;
		assemblyRelationship = newAssemblyRelationship;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_RELATIONSHIP, oldAssemblyRelationship, newAssemblyRelationship);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship) {
		if (newAssemblyRelationship != assemblyRelationship) {
			NotificationChain msgs = null;
			if (assemblyRelationship != null)
				msgs = ((InternalEObject)assemblyRelationship).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			if (newAssemblyRelationship != null)
				msgs = ((InternalEObject)newAssemblyRelationship).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			msgs = basicSetAssemblyRelationship(newAssemblyRelationship, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_RELATIONSHIP, newAssemblyRelationship, newAssemblyRelationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpMaterialCapabilityPropertyType> getMaterialCapabilityProperty() {
		if (materialCapabilityProperty == null) {
			materialCapabilityProperty = new EObjectContainmentEList<OpMaterialCapabilityPropertyType>(OpMaterialCapabilityPropertyType.class, this, B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_CAPABILITY_PROPERTY);
		}
		return materialCapabilityProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_CLASS_ID:
				return ((InternalEList<?>)getMaterialClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_DEFINITION_ID:
				return ((InternalEList<?>)getMaterialDefinitionID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_LOT_ID:
				return ((InternalEList<?>)getMaterialLotID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_SUB_LOT_ID:
				return ((InternalEList<?>)getMaterialSubLotID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CAPABILITY_TYPE:
				return basicSetCapabilityType(null, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__REASON:
				return basicSetReason(null, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				return basicSetConfidenceFactor(null, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_USE:
				return basicSetMaterialUse(null, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__START_TIME:
				return basicSetStartTime(null, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__END_TIME:
				return basicSetEndTime(null, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_CAPABILITY:
				return ((InternalEList<?>)getAssemblyCapability()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_TYPE:
				return basicSetAssemblyType(null, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_RELATIONSHIP:
				return basicSetAssemblyRelationship(null, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_CAPABILITY_PROPERTY:
				return ((InternalEList<?>)getMaterialCapabilityProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_CLASS_ID:
				return getMaterialClassID();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_DEFINITION_ID:
				return getMaterialDefinitionID();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_LOT_ID:
				return getMaterialLotID();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_SUB_LOT_ID:
				return getMaterialSubLotID();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CAPABILITY_TYPE:
				return getCapabilityType();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__REASON:
				return getReason();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				return getConfidenceFactor();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_USE:
				return getMaterialUse();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__START_TIME:
				return getStartTime();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__END_TIME:
				return getEndTime();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_CAPABILITY:
				return getAssemblyCapability();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_TYPE:
				return getAssemblyType();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_RELATIONSHIP:
				return getAssemblyRelationship();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_CAPABILITY_PROPERTY:
				return getMaterialCapabilityProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_CLASS_ID:
				getMaterialClassID().clear();
				getMaterialClassID().addAll((Collection<? extends MaterialClassIDType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_DEFINITION_ID:
				getMaterialDefinitionID().clear();
				getMaterialDefinitionID().addAll((Collection<? extends MaterialDefinitionIDType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_LOT_ID:
				getMaterialLotID().clear();
				getMaterialLotID().addAll((Collection<? extends MaterialLotIDType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_SUB_LOT_ID:
				getMaterialSubLotID().clear();
				getMaterialSubLotID().addAll((Collection<? extends MaterialSubLotIDType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CAPABILITY_TYPE:
				setCapabilityType((CapabilityTypeType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__REASON:
				setReason((ReasonType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				setConfidenceFactor((ConfidenceFactorType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_USE:
				setMaterialUse((MaterialUseType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__START_TIME:
				setStartTime((StartTimeType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__END_TIME:
				setEndTime((EndTimeType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_CAPABILITY:
				getAssemblyCapability().clear();
				getAssemblyCapability().addAll((Collection<? extends OpMaterialCapabilityType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_CAPABILITY_PROPERTY:
				getMaterialCapabilityProperty().clear();
				getMaterialCapabilityProperty().addAll((Collection<? extends OpMaterialCapabilityPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_CLASS_ID:
				getMaterialClassID().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_DEFINITION_ID:
				getMaterialDefinitionID().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_LOT_ID:
				getMaterialLotID().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_SUB_LOT_ID:
				getMaterialSubLotID().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CAPABILITY_TYPE:
				setCapabilityType((CapabilityTypeType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__REASON:
				setReason((ReasonType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				setConfidenceFactor((ConfidenceFactorType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_USE:
				setMaterialUse((MaterialUseType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__START_TIME:
				setStartTime((StartTimeType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__END_TIME:
				setEndTime((EndTimeType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_CAPABILITY:
				getAssemblyCapability().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_CAPABILITY_PROPERTY:
				getMaterialCapabilityProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_CLASS_ID:
				return materialClassID != null && !materialClassID.isEmpty();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_DEFINITION_ID:
				return materialDefinitionID != null && !materialDefinitionID.isEmpty();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_LOT_ID:
				return materialLotID != null && !materialLotID.isEmpty();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_SUB_LOT_ID:
				return materialSubLotID != null && !materialSubLotID.isEmpty();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CAPABILITY_TYPE:
				return capabilityType != null;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__REASON:
				return reason != null;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				return confidenceFactor != null;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_USE:
				return materialUse != null;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__START_TIME:
				return startTime != null;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__END_TIME:
				return endTime != null;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_CAPABILITY:
				return assemblyCapability != null && !assemblyCapability.isEmpty();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_TYPE:
				return assemblyType != null;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__ASSEMBLY_RELATIONSHIP:
				return assemblyRelationship != null;
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE__MATERIAL_CAPABILITY_PROPERTY:
				return materialCapabilityProperty != null && !materialCapabilityProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OpMaterialCapabilityTypeImpl
