/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type95</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType95#getRespond <em>Respond</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType95#getMaterialTestSpec <em>Material Test Spec</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType95()
 * @model extendedMetaData="name='DataArea_._96_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType95 extends EObject {
	/**
	 * Returns the value of the '<em><b>Respond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond</em>' containment reference.
	 * @see #setRespond(TransRespondType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType95_Respond()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Respond' namespace='##targetNamespace'"
	 * @generated
	 */
	TransRespondType getRespond();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType95#getRespond <em>Respond</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond</em>' containment reference.
	 * @see #getRespond()
	 * @generated
	 */
	void setRespond(TransRespondType value);

	/**
	 * Returns the value of the '<em><b>Material Test Spec</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialTestSpecificationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Test Spec</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Test Spec</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType95_MaterialTestSpec()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MaterialTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialTestSpecificationType> getMaterialTestSpec();

} // DataAreaType95
