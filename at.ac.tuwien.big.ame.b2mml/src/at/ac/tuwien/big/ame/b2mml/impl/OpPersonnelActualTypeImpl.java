/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.OpPersonnelActualPropertyType;
import at.ac.tuwien.big.ame.b2mml.OpPersonnelActualType;
import at.ac.tuwien.big.ame.b2mml.PersonIDType;
import at.ac.tuwien.big.ame.b2mml.PersonnelClassIDType;
import at.ac.tuwien.big.ame.b2mml.PersonnelUseType;
import at.ac.tuwien.big.ame.b2mml.QuantityValueType;
import at.ac.tuwien.big.ame.b2mml.RequiredByRequestedSegmentResponseType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Personnel Actual Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelActualTypeImpl#getPersonnelClassID <em>Personnel Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelActualTypeImpl#getPersonID <em>Person ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelActualTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelActualTypeImpl#getPersonnelUse <em>Personnel Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelActualTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelActualTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelActualTypeImpl#getPersonnelActualProperty <em>Personnel Actual Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelActualTypeImpl#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpPersonnelActualTypeImpl extends MinimalEObjectImpl.Container implements OpPersonnelActualType {
	/**
	 * The cached value of the '{@link #getPersonnelClassID() <em>Personnel Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelClassIDType> personnelClassID;

	/**
	 * The cached value of the '{@link #getPersonID() <em>Person ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonID()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonIDType> personID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getPersonnelUse() <em>Personnel Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelUse()
	 * @generated
	 * @ordered
	 */
	protected PersonnelUseType personnelUse;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getPersonnelActualProperty() <em>Personnel Actual Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelActualProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPersonnelActualPropertyType> personnelActualProperty;

	/**
	 * The cached value of the '{@link #getRequiredByRequestedSegmentResponse() <em>Required By Requested Segment Response</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredByRequestedSegmentResponse()
	 * @generated
	 * @ordered
	 */
	protected RequiredByRequestedSegmentResponseType requiredByRequestedSegmentResponse;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpPersonnelActualTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpPersonnelActualType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelClassIDType> getPersonnelClassID() {
		if (personnelClassID == null) {
			personnelClassID = new EObjectContainmentEList<PersonnelClassIDType>(PersonnelClassIDType.class, this, B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_CLASS_ID);
		}
		return personnelClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonIDType> getPersonID() {
		if (personID == null) {
			personID = new EObjectContainmentEList<PersonIDType>(PersonIDType.class, this, B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSON_ID);
		}
		return personID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelUseType getPersonnelUse() {
		return personnelUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPersonnelUse(PersonnelUseType newPersonnelUse, NotificationChain msgs) {
		PersonnelUseType oldPersonnelUse = personnelUse;
		personnelUse = newPersonnelUse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_USE, oldPersonnelUse, newPersonnelUse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonnelUse(PersonnelUseType newPersonnelUse) {
		if (newPersonnelUse != personnelUse) {
			NotificationChain msgs = null;
			if (personnelUse != null)
				msgs = ((InternalEObject)personnelUse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_USE, null, msgs);
			if (newPersonnelUse != null)
				msgs = ((InternalEObject)newPersonnelUse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_USE, null, msgs);
			msgs = basicSetPersonnelUse(newPersonnelUse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_USE, newPersonnelUse, newPersonnelUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPersonnelActualPropertyType> getPersonnelActualProperty() {
		if (personnelActualProperty == null) {
			personnelActualProperty = new EObjectContainmentEList<OpPersonnelActualPropertyType>(OpPersonnelActualPropertyType.class, this, B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_ACTUAL_PROPERTY);
		}
		return personnelActualProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredByRequestedSegmentResponseType getRequiredByRequestedSegmentResponse() {
		return requiredByRequestedSegmentResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType newRequiredByRequestedSegmentResponse, NotificationChain msgs) {
		RequiredByRequestedSegmentResponseType oldRequiredByRequestedSegmentResponse = requiredByRequestedSegmentResponse;
		requiredByRequestedSegmentResponse = newRequiredByRequestedSegmentResponse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, oldRequiredByRequestedSegmentResponse, newRequiredByRequestedSegmentResponse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType newRequiredByRequestedSegmentResponse) {
		if (newRequiredByRequestedSegmentResponse != requiredByRequestedSegmentResponse) {
			NotificationChain msgs = null;
			if (requiredByRequestedSegmentResponse != null)
				msgs = ((InternalEObject)requiredByRequestedSegmentResponse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, null, msgs);
			if (newRequiredByRequestedSegmentResponse != null)
				msgs = ((InternalEObject)newRequiredByRequestedSegmentResponse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, null, msgs);
			msgs = basicSetRequiredByRequestedSegmentResponse(newRequiredByRequestedSegmentResponse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, newRequiredByRequestedSegmentResponse, newRequiredByRequestedSegmentResponse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_CLASS_ID:
				return ((InternalEList<?>)getPersonnelClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSON_ID:
				return ((InternalEList<?>)getPersonID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_USE:
				return basicSetPersonnelUse(null, msgs);
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_ACTUAL_PROPERTY:
				return ((InternalEList<?>)getPersonnelActualProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return basicSetRequiredByRequestedSegmentResponse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_CLASS_ID:
				return getPersonnelClassID();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSON_ID:
				return getPersonID();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_USE:
				return getPersonnelUse();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_ACTUAL_PROPERTY:
				return getPersonnelActualProperty();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return getRequiredByRequestedSegmentResponse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_CLASS_ID:
				getPersonnelClassID().clear();
				getPersonnelClassID().addAll((Collection<? extends PersonnelClassIDType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSON_ID:
				getPersonID().clear();
				getPersonID().addAll((Collection<? extends PersonIDType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_USE:
				setPersonnelUse((PersonnelUseType)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_ACTUAL_PROPERTY:
				getPersonnelActualProperty().clear();
				getPersonnelActualProperty().addAll((Collection<? extends OpPersonnelActualPropertyType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				setRequiredByRequestedSegmentResponse((RequiredByRequestedSegmentResponseType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_CLASS_ID:
				getPersonnelClassID().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSON_ID:
				getPersonID().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_USE:
				setPersonnelUse((PersonnelUseType)null);
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_ACTUAL_PROPERTY:
				getPersonnelActualProperty().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				setRequiredByRequestedSegmentResponse((RequiredByRequestedSegmentResponseType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_CLASS_ID:
				return personnelClassID != null && !personnelClassID.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSON_ID:
				return personID != null && !personID.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_USE:
				return personnelUse != null;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__PERSONNEL_ACTUAL_PROPERTY:
				return personnelActualProperty != null && !personnelActualProperty.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return requiredByRequestedSegmentResponse != null;
		}
		return super.eIsSet(featureID);
	}

} //OpPersonnelActualTypeImpl
