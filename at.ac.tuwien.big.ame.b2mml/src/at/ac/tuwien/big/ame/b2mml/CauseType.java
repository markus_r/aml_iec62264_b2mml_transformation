/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cause Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getCauseType()
 * @model extendedMetaData="name='CauseType' kind='simple'"
 * @generated
 */
public interface CauseType extends CodeType {
} // CauseType
