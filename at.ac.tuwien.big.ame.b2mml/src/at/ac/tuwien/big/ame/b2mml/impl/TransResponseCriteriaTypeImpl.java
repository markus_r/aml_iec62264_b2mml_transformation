/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.TransChangeStatusType;
import at.ac.tuwien.big.ame.b2mml.TransExpressionType;
import at.ac.tuwien.big.ame.b2mml.TransResponseCriteriaType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trans Response Criteria Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransResponseCriteriaTypeImpl#getResponseExpression <em>Response Expression</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransResponseCriteriaTypeImpl#getChangeStatus <em>Change Status</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransResponseCriteriaTypeImpl extends MinimalEObjectImpl.Container implements TransResponseCriteriaType {
	/**
	 * The cached value of the '{@link #getResponseExpression() <em>Response Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseExpression()
	 * @generated
	 * @ordered
	 */
	protected TransExpressionType responseExpression;

	/**
	 * The cached value of the '{@link #getChangeStatus() <em>Change Status</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChangeStatus()
	 * @generated
	 * @ordered
	 */
	protected TransChangeStatusType changeStatus;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransResponseCriteriaTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTransResponseCriteriaType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransExpressionType getResponseExpression() {
		return responseExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResponseExpression(TransExpressionType newResponseExpression, NotificationChain msgs) {
		TransExpressionType oldResponseExpression = responseExpression;
		responseExpression = newResponseExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__RESPONSE_EXPRESSION, oldResponseExpression, newResponseExpression);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponseExpression(TransExpressionType newResponseExpression) {
		if (newResponseExpression != responseExpression) {
			NotificationChain msgs = null;
			if (responseExpression != null)
				msgs = ((InternalEObject)responseExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__RESPONSE_EXPRESSION, null, msgs);
			if (newResponseExpression != null)
				msgs = ((InternalEObject)newResponseExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__RESPONSE_EXPRESSION, null, msgs);
			msgs = basicSetResponseExpression(newResponseExpression, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__RESPONSE_EXPRESSION, newResponseExpression, newResponseExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransChangeStatusType getChangeStatus() {
		return changeStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeStatus(TransChangeStatusType newChangeStatus, NotificationChain msgs) {
		TransChangeStatusType oldChangeStatus = changeStatus;
		changeStatus = newChangeStatus;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__CHANGE_STATUS, oldChangeStatus, newChangeStatus);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeStatus(TransChangeStatusType newChangeStatus) {
		if (newChangeStatus != changeStatus) {
			NotificationChain msgs = null;
			if (changeStatus != null)
				msgs = ((InternalEObject)changeStatus).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__CHANGE_STATUS, null, msgs);
			if (newChangeStatus != null)
				msgs = ((InternalEObject)newChangeStatus).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__CHANGE_STATUS, null, msgs);
			msgs = basicSetChangeStatus(newChangeStatus, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__CHANGE_STATUS, newChangeStatus, newChangeStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__RESPONSE_EXPRESSION:
				return basicSetResponseExpression(null, msgs);
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__CHANGE_STATUS:
				return basicSetChangeStatus(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__RESPONSE_EXPRESSION:
				return getResponseExpression();
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__CHANGE_STATUS:
				return getChangeStatus();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__RESPONSE_EXPRESSION:
				setResponseExpression((TransExpressionType)newValue);
				return;
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__CHANGE_STATUS:
				setChangeStatus((TransChangeStatusType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__RESPONSE_EXPRESSION:
				setResponseExpression((TransExpressionType)null);
				return;
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__CHANGE_STATUS:
				setChangeStatus((TransChangeStatusType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__RESPONSE_EXPRESSION:
				return responseExpression != null;
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE__CHANGE_STATUS:
				return changeStatus != null;
		}
		return super.eIsSet(featureID);
	}

} //TransResponseCriteriaTypeImpl
