/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Production Request ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getProductionRequestIDType()
 * @model extendedMetaData="name='ProductionRequestIDType' kind='simple'"
 * @generated
 */
public interface ProductionRequestIDType extends IdentifierType {
} // ProductionRequestIDType
