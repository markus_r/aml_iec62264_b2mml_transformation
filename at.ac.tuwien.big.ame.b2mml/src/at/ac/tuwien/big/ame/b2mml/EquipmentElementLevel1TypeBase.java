/**
 */
package at.ac.tuwien.big.ame.b2mml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Equipment Element Level1 Type Base</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentElementLevel1TypeBase()
 * @model extendedMetaData="name='EquipmentElementLevel1Type_._base'"
 * @generated
 */
public enum EquipmentElementLevel1TypeBase implements Enumerator {
	/**
	 * The '<em><b>Enterprise</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENTERPRISE_VALUE
	 * @generated
	 * @ordered
	 */
	ENTERPRISE(0, "Enterprise", "Enterprise"),

	/**
	 * The '<em><b>Site</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SITE_VALUE
	 * @generated
	 * @ordered
	 */
	SITE(1, "Site", "Site"),

	/**
	 * The '<em><b>Area</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AREA_VALUE
	 * @generated
	 * @ordered
	 */
	AREA(2, "Area", "Area"),

	/**
	 * The '<em><b>Process Cell</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROCESS_CELL_VALUE
	 * @generated
	 * @ordered
	 */
	PROCESS_CELL(3, "ProcessCell", "ProcessCell"),

	/**
	 * The '<em><b>Unit</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNIT_VALUE
	 * @generated
	 * @ordered
	 */
	UNIT(4, "Unit", "Unit"),

	/**
	 * The '<em><b>Production Line</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PRODUCTION_LINE_VALUE
	 * @generated
	 * @ordered
	 */
	PRODUCTION_LINE(5, "ProductionLine", "ProductionLine"),

	/**
	 * The '<em><b>Work Cell</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WORK_CELL_VALUE
	 * @generated
	 * @ordered
	 */
	WORK_CELL(6, "WorkCell", "WorkCell"),

	/**
	 * The '<em><b>Production Unit</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PRODUCTION_UNIT_VALUE
	 * @generated
	 * @ordered
	 */
	PRODUCTION_UNIT(7, "ProductionUnit", "ProductionUnit"),

	/**
	 * The '<em><b>Storage Zone</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STORAGE_ZONE_VALUE
	 * @generated
	 * @ordered
	 */
	STORAGE_ZONE(8, "StorageZone", "StorageZone"),

	/**
	 * The '<em><b>Storage Unit</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STORAGE_UNIT_VALUE
	 * @generated
	 * @ordered
	 */
	STORAGE_UNIT(9, "StorageUnit", "StorageUnit"),

	/**
	 * The '<em><b>Work Center</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WORK_CENTER_VALUE
	 * @generated
	 * @ordered
	 */
	WORK_CENTER(10, "WorkCenter", "WorkCenter"),

	/**
	 * The '<em><b>Work Unit</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WORK_UNIT_VALUE
	 * @generated
	 * @ordered
	 */
	WORK_UNIT(11, "WorkUnit", "WorkUnit"),

	/**
	 * The '<em><b>Equipment Module</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EQUIPMENT_MODULE_VALUE
	 * @generated
	 * @ordered
	 */
	EQUIPMENT_MODULE(12, "EquipmentModule", "EquipmentModule"),

	/**
	 * The '<em><b>Control Module</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONTROL_MODULE_VALUE
	 * @generated
	 * @ordered
	 */
	CONTROL_MODULE(13, "ControlModule", "ControlModule"),

	/**
	 * The '<em><b>Other</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	OTHER(14, "Other", "Other");

	/**
	 * The '<em><b>Enterprise</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Enterprise</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ENTERPRISE
	 * @model name="Enterprise"
	 * @generated
	 * @ordered
	 */
	public static final int ENTERPRISE_VALUE = 0;

	/**
	 * The '<em><b>Site</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Site</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SITE
	 * @model name="Site"
	 * @generated
	 * @ordered
	 */
	public static final int SITE_VALUE = 1;

	/**
	 * The '<em><b>Area</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Area</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AREA
	 * @model name="Area"
	 * @generated
	 * @ordered
	 */
	public static final int AREA_VALUE = 2;

	/**
	 * The '<em><b>Process Cell</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Process Cell</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PROCESS_CELL
	 * @model name="ProcessCell"
	 * @generated
	 * @ordered
	 */
	public static final int PROCESS_CELL_VALUE = 3;

	/**
	 * The '<em><b>Unit</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Unit</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNIT
	 * @model name="Unit"
	 * @generated
	 * @ordered
	 */
	public static final int UNIT_VALUE = 4;

	/**
	 * The '<em><b>Production Line</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Production Line</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PRODUCTION_LINE
	 * @model name="ProductionLine"
	 * @generated
	 * @ordered
	 */
	public static final int PRODUCTION_LINE_VALUE = 5;

	/**
	 * The '<em><b>Work Cell</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Work Cell</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WORK_CELL
	 * @model name="WorkCell"
	 * @generated
	 * @ordered
	 */
	public static final int WORK_CELL_VALUE = 6;

	/**
	 * The '<em><b>Production Unit</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Production Unit</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PRODUCTION_UNIT
	 * @model name="ProductionUnit"
	 * @generated
	 * @ordered
	 */
	public static final int PRODUCTION_UNIT_VALUE = 7;

	/**
	 * The '<em><b>Storage Zone</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Storage Zone</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STORAGE_ZONE
	 * @model name="StorageZone"
	 * @generated
	 * @ordered
	 */
	public static final int STORAGE_ZONE_VALUE = 8;

	/**
	 * The '<em><b>Storage Unit</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Storage Unit</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STORAGE_UNIT
	 * @model name="StorageUnit"
	 * @generated
	 * @ordered
	 */
	public static final int STORAGE_UNIT_VALUE = 9;

	/**
	 * The '<em><b>Work Center</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Work Center</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WORK_CENTER
	 * @model name="WorkCenter"
	 * @generated
	 * @ordered
	 */
	public static final int WORK_CENTER_VALUE = 10;

	/**
	 * The '<em><b>Work Unit</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Work Unit</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WORK_UNIT
	 * @model name="WorkUnit"
	 * @generated
	 * @ordered
	 */
	public static final int WORK_UNIT_VALUE = 11;

	/**
	 * The '<em><b>Equipment Module</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Equipment Module</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EQUIPMENT_MODULE
	 * @model name="EquipmentModule"
	 * @generated
	 * @ordered
	 */
	public static final int EQUIPMENT_MODULE_VALUE = 12;

	/**
	 * The '<em><b>Control Module</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Control Module</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONTROL_MODULE
	 * @model name="ControlModule"
	 * @generated
	 * @ordered
	 */
	public static final int CONTROL_MODULE_VALUE = 13;

	/**
	 * The '<em><b>Other</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Other</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OTHER
	 * @model name="Other"
	 * @generated
	 * @ordered
	 */
	public static final int OTHER_VALUE = 14;

	/**
	 * An array of all the '<em><b>Equipment Element Level1 Type Base</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final EquipmentElementLevel1TypeBase[] VALUES_ARRAY =
		new EquipmentElementLevel1TypeBase[] {
			ENTERPRISE,
			SITE,
			AREA,
			PROCESS_CELL,
			UNIT,
			PRODUCTION_LINE,
			WORK_CELL,
			PRODUCTION_UNIT,
			STORAGE_ZONE,
			STORAGE_UNIT,
			WORK_CENTER,
			WORK_UNIT,
			EQUIPMENT_MODULE,
			CONTROL_MODULE,
			OTHER,
		};

	/**
	 * A public read-only list of all the '<em><b>Equipment Element Level1 Type Base</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<EquipmentElementLevel1TypeBase> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Equipment Element Level1 Type Base</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static EquipmentElementLevel1TypeBase get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			EquipmentElementLevel1TypeBase result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Equipment Element Level1 Type Base</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static EquipmentElementLevel1TypeBase getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			EquipmentElementLevel1TypeBase result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Equipment Element Level1 Type Base</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static EquipmentElementLevel1TypeBase get(int value) {
		switch (value) {
			case ENTERPRISE_VALUE: return ENTERPRISE;
			case SITE_VALUE: return SITE;
			case AREA_VALUE: return AREA;
			case PROCESS_CELL_VALUE: return PROCESS_CELL;
			case UNIT_VALUE: return UNIT;
			case PRODUCTION_LINE_VALUE: return PRODUCTION_LINE;
			case WORK_CELL_VALUE: return WORK_CELL;
			case PRODUCTION_UNIT_VALUE: return PRODUCTION_UNIT;
			case STORAGE_ZONE_VALUE: return STORAGE_ZONE;
			case STORAGE_UNIT_VALUE: return STORAGE_UNIT;
			case WORK_CENTER_VALUE: return WORK_CENTER;
			case WORK_UNIT_VALUE: return WORK_UNIT;
			case EQUIPMENT_MODULE_VALUE: return EQUIPMENT_MODULE;
			case CONTROL_MODULE_VALUE: return CONTROL_MODULE;
			case OTHER_VALUE: return OTHER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EquipmentElementLevel1TypeBase(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //EquipmentElementLevel1TypeBase
