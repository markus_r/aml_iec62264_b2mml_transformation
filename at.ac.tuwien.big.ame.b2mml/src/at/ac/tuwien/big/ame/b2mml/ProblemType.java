/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Problem Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getProblemType()
 * @model extendedMetaData="name='ProblemType' kind='simple'"
 * @generated
 */
public interface ProblemType extends CodeType {
} // ProblemType
