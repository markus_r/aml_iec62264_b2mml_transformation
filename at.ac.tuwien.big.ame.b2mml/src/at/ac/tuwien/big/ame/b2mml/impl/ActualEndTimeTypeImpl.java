/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.ActualEndTimeType;
import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actual End Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ActualEndTimeTypeImpl extends DateTimeTypeImpl implements ActualEndTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActualEndTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getActualEndTimeType();
	}

} //ActualEndTimeTypeImpl
