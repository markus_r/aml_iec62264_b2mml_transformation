/**
 */
package at.ac.tuwien.big.ame.b2mml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Dependency1 Type Base</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDependency1TypeBase()
 * @model extendedMetaData="name='Dependency1Type_._base'"
 * @generated
 */
public enum Dependency1TypeBase implements Enumerator {
	/**
	 * The '<em><b>Not Follow</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_FOLLOW_VALUE
	 * @generated
	 * @ordered
	 */
	NOT_FOLLOW(0, "NotFollow", "NotFollow"),

	/**
	 * The '<em><b>Possible Parallel</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #POSSIBLE_PARALLEL_VALUE
	 * @generated
	 * @ordered
	 */
	POSSIBLE_PARALLEL(1, "PossibleParallel", "PossibleParallel"),

	/**
	 * The '<em><b>Not In Parallel</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_IN_PARALLEL_VALUE
	 * @generated
	 * @ordered
	 */
	NOT_IN_PARALLEL(2, "NotInParallel", "NotInParallel"),

	/**
	 * The '<em><b>At Start</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AT_START_VALUE
	 * @generated
	 * @ordered
	 */
	AT_START(3, "AtStart", "AtStart"),

	/**
	 * The '<em><b>After Start</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AFTER_START_VALUE
	 * @generated
	 * @ordered
	 */
	AFTER_START(4, "AfterStart", "AfterStart"),

	/**
	 * The '<em><b>After End</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AFTER_END_VALUE
	 * @generated
	 * @ordered
	 */
	AFTER_END(5, "AfterEnd", "AfterEnd"),

	/**
	 * The '<em><b>No Later After Start</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NO_LATER_AFTER_START_VALUE
	 * @generated
	 * @ordered
	 */
	NO_LATER_AFTER_START(6, "NoLaterAfterStart", "NoLaterAfterStart"),

	/**
	 * The '<em><b>No Earlier After Start</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NO_EARLIER_AFTER_START_VALUE
	 * @generated
	 * @ordered
	 */
	NO_EARLIER_AFTER_START(7, "NoEarlierAfterStart", "NoEarlierAfterStart"),

	/**
	 * The '<em><b>No Later After End</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NO_LATER_AFTER_END_VALUE
	 * @generated
	 * @ordered
	 */
	NO_LATER_AFTER_END(8, "NoLaterAfterEnd", "NoLaterAfterEnd"),

	/**
	 * The '<em><b>No Earlier After End</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NO_EARLIER_AFTER_END_VALUE
	 * @generated
	 * @ordered
	 */
	NO_EARLIER_AFTER_END(9, "NoEarlierAfterEnd", "NoEarlierAfterEnd"),

	/**
	 * The '<em><b>Other</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	OTHER(10, "Other", "Other");

	/**
	 * The '<em><b>Not Follow</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Not Follow</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOT_FOLLOW
	 * @model name="NotFollow"
	 * @generated
	 * @ordered
	 */
	public static final int NOT_FOLLOW_VALUE = 0;

	/**
	 * The '<em><b>Possible Parallel</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Possible Parallel</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #POSSIBLE_PARALLEL
	 * @model name="PossibleParallel"
	 * @generated
	 * @ordered
	 */
	public static final int POSSIBLE_PARALLEL_VALUE = 1;

	/**
	 * The '<em><b>Not In Parallel</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Not In Parallel</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOT_IN_PARALLEL
	 * @model name="NotInParallel"
	 * @generated
	 * @ordered
	 */
	public static final int NOT_IN_PARALLEL_VALUE = 2;

	/**
	 * The '<em><b>At Start</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>At Start</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AT_START
	 * @model name="AtStart"
	 * @generated
	 * @ordered
	 */
	public static final int AT_START_VALUE = 3;

	/**
	 * The '<em><b>After Start</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>After Start</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AFTER_START
	 * @model name="AfterStart"
	 * @generated
	 * @ordered
	 */
	public static final int AFTER_START_VALUE = 4;

	/**
	 * The '<em><b>After End</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>After End</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AFTER_END
	 * @model name="AfterEnd"
	 * @generated
	 * @ordered
	 */
	public static final int AFTER_END_VALUE = 5;

	/**
	 * The '<em><b>No Later After Start</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>No Later After Start</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NO_LATER_AFTER_START
	 * @model name="NoLaterAfterStart"
	 * @generated
	 * @ordered
	 */
	public static final int NO_LATER_AFTER_START_VALUE = 6;

	/**
	 * The '<em><b>No Earlier After Start</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>No Earlier After Start</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NO_EARLIER_AFTER_START
	 * @model name="NoEarlierAfterStart"
	 * @generated
	 * @ordered
	 */
	public static final int NO_EARLIER_AFTER_START_VALUE = 7;

	/**
	 * The '<em><b>No Later After End</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>No Later After End</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NO_LATER_AFTER_END
	 * @model name="NoLaterAfterEnd"
	 * @generated
	 * @ordered
	 */
	public static final int NO_LATER_AFTER_END_VALUE = 8;

	/**
	 * The '<em><b>No Earlier After End</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>No Earlier After End</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NO_EARLIER_AFTER_END
	 * @model name="NoEarlierAfterEnd"
	 * @generated
	 * @ordered
	 */
	public static final int NO_EARLIER_AFTER_END_VALUE = 9;

	/**
	 * The '<em><b>Other</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Other</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OTHER
	 * @model name="Other"
	 * @generated
	 * @ordered
	 */
	public static final int OTHER_VALUE = 10;

	/**
	 * An array of all the '<em><b>Dependency1 Type Base</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final Dependency1TypeBase[] VALUES_ARRAY =
		new Dependency1TypeBase[] {
			NOT_FOLLOW,
			POSSIBLE_PARALLEL,
			NOT_IN_PARALLEL,
			AT_START,
			AFTER_START,
			AFTER_END,
			NO_LATER_AFTER_START,
			NO_EARLIER_AFTER_START,
			NO_LATER_AFTER_END,
			NO_EARLIER_AFTER_END,
			OTHER,
		};

	/**
	 * A public read-only list of all the '<em><b>Dependency1 Type Base</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<Dependency1TypeBase> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Dependency1 Type Base</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Dependency1TypeBase get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Dependency1TypeBase result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Dependency1 Type Base</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Dependency1TypeBase getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Dependency1TypeBase result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Dependency1 Type Base</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Dependency1TypeBase get(int value) {
		switch (value) {
			case NOT_FOLLOW_VALUE: return NOT_FOLLOW;
			case POSSIBLE_PARALLEL_VALUE: return POSSIBLE_PARALLEL;
			case NOT_IN_PARALLEL_VALUE: return NOT_IN_PARALLEL;
			case AT_START_VALUE: return AT_START;
			case AFTER_START_VALUE: return AFTER_START;
			case AFTER_END_VALUE: return AFTER_END;
			case NO_LATER_AFTER_START_VALUE: return NO_LATER_AFTER_START;
			case NO_EARLIER_AFTER_START_VALUE: return NO_EARLIER_AFTER_START;
			case NO_LATER_AFTER_END_VALUE: return NO_LATER_AFTER_END;
			case NO_EARLIER_AFTER_END_VALUE: return NO_EARLIER_AFTER_END;
			case OTHER_VALUE: return OTHER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Dependency1TypeBase(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //Dependency1TypeBase
