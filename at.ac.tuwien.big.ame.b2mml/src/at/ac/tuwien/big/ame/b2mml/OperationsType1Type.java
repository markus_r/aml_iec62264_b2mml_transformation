/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsType1Type()
 * @model extendedMetaData="name='OperationsType1Type' kind='simple'"
 * @generated
 */
public interface OperationsType1Type extends CodeType {
} // OperationsType1Type
