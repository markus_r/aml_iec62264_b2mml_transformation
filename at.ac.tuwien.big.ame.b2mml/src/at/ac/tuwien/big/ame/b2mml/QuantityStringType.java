/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quantity String Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQuantityStringType()
 * @model extendedMetaData="name='QuantityStringType' kind='simple'"
 * @generated
 */
public interface QuantityStringType extends AnyGenericValueType {
} // QuantityStringType
