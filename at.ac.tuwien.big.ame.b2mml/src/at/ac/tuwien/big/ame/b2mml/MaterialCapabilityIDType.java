/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Capability ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialCapabilityIDType()
 * @model extendedMetaData="name='MaterialCapabilityIDType' kind='simple'"
 * @generated
 */
public interface MaterialCapabilityIDType extends IdentifierType {
} // MaterialCapabilityIDType
