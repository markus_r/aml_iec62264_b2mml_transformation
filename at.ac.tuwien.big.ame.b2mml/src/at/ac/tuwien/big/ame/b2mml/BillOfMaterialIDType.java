/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bill Of Material ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getBillOfMaterialIDType()
 * @model extendedMetaData="name='BillOfMaterialIDType' kind='simple'"
 * @generated
 */
public interface BillOfMaterialIDType extends IdentifierType {
} // BillOfMaterialIDType
