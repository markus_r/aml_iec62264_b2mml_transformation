/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationIDType;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassPropertyType;
import at.ac.tuwien.big.ame.b2mml.ValueType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Class Property Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PhysicalAssetClassPropertyTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PhysicalAssetClassPropertyTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PhysicalAssetClassPropertyTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PhysicalAssetClassPropertyTypeImpl#getPhysicalAssetClassProperty <em>Physical Asset Class Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.PhysicalAssetClassPropertyTypeImpl#getPhysicalAssetCapabilityTestSpecificationID <em>Physical Asset Capability Test Specification ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalAssetClassPropertyTypeImpl extends MinimalEObjectImpl.Container implements PhysicalAssetClassPropertyType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected EList<ValueType> value;

	/**
	 * The cached value of the '{@link #getPhysicalAssetClassProperty() <em>Physical Asset Class Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClassProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetClassPropertyType> physicalAssetClassProperty;

	/**
	 * The cached value of the '{@link #getPhysicalAssetCapabilityTestSpecificationID() <em>Physical Asset Capability Test Specification ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetCapabilityTestSpecificationID()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetCapabilityTestSpecificationIDType> physicalAssetCapabilityTestSpecificationID;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetClassPropertyTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPhysicalAssetClassPropertyType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueType> getValue() {
		if (value == null) {
			value = new EObjectContainmentEList<ValueType>(ValueType.class, this, B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__VALUE);
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetClassPropertyType> getPhysicalAssetClassProperty() {
		if (physicalAssetClassProperty == null) {
			physicalAssetClassProperty = new EObjectContainmentEList<PhysicalAssetClassPropertyType>(PhysicalAssetClassPropertyType.class, this, B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_PROPERTY);
		}
		return physicalAssetClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetCapabilityTestSpecificationIDType> getPhysicalAssetCapabilityTestSpecificationID() {
		if (physicalAssetCapabilityTestSpecificationID == null) {
			physicalAssetCapabilityTestSpecificationID = new EObjectContainmentEList<PhysicalAssetCapabilityTestSpecificationIDType>(PhysicalAssetCapabilityTestSpecificationIDType.class, this, B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID);
		}
		return physicalAssetCapabilityTestSpecificationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__VALUE:
				return ((InternalEList<?>)getValue()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_PROPERTY:
				return ((InternalEList<?>)getPhysicalAssetClassProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				return ((InternalEList<?>)getPhysicalAssetCapabilityTestSpecificationID()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__ID:
				return getID();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__VALUE:
				return getValue();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_PROPERTY:
				return getPhysicalAssetClassProperty();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				return getPhysicalAssetCapabilityTestSpecificationID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__VALUE:
				getValue().clear();
				getValue().addAll((Collection<? extends ValueType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_PROPERTY:
				getPhysicalAssetClassProperty().clear();
				getPhysicalAssetClassProperty().addAll((Collection<? extends PhysicalAssetClassPropertyType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				getPhysicalAssetCapabilityTestSpecificationID().clear();
				getPhysicalAssetCapabilityTestSpecificationID().addAll((Collection<? extends PhysicalAssetCapabilityTestSpecificationIDType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__VALUE:
				getValue().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_PROPERTY:
				getPhysicalAssetClassProperty().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				getPhysicalAssetCapabilityTestSpecificationID().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__ID:
				return iD != null;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__VALUE:
				return value != null && !value.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CLASS_PROPERTY:
				return physicalAssetClassProperty != null && !physicalAssetClassProperty.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				return physicalAssetCapabilityTestSpecificationID != null && !physicalAssetCapabilityTestSpecificationID.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PhysicalAssetClassPropertyTypeImpl
