/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Date Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestDateTimeType()
 * @model extendedMetaData="name='TestDateTimeType' kind='simple'"
 * @generated
 */
public interface TestDateTimeType extends DateTimeType {
} // TestDateTimeType
