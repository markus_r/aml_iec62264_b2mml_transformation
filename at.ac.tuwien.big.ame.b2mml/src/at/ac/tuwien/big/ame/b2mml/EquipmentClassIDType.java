/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Class ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentClassIDType()
 * @model extendedMetaData="name='EquipmentClassIDType' kind='simple'"
 * @generated
 */
public interface EquipmentClassIDType extends IdentifierType {
} // EquipmentClassIDType
