/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.ProductProductionRuleIDType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Product Production Rule ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ProductProductionRuleIDTypeImpl extends IdentifierTypeImpl implements ProductProductionRuleIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProductProductionRuleIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getProductProductionRuleIDType();
	}

} //ProductProductionRuleIDTypeImpl
