/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans User Area Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransUserAreaType#getAny <em>Any</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransUserAreaType()
 * @model extendedMetaData="name='TransUserAreaType' kind='elementOnly'"
 * @generated
 */
public interface TransUserAreaType extends EObject {
	/**
	 * Returns the value of the '<em><b>Any</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any</em>' attribute list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransUserAreaType_Any()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' wildcards='##any' name=':0' processing='strict'"
	 * @generated
	 */
	FeatureMap getAny();

} // TransUserAreaType
