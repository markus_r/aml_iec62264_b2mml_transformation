/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.JobOrderDispatchStatusType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Job Order Dispatch Status Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class JobOrderDispatchStatusTypeImpl extends CodeTypeImpl implements JobOrderDispatchStatusType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JobOrderDispatchStatusTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getJobOrderDispatchStatusType();
	}

} //JobOrderDispatchStatusTypeImpl
