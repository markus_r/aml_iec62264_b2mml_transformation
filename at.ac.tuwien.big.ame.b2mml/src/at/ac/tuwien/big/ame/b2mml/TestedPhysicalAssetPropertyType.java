/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tested Physical Asset Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetPropertyType#getPhysicalAssetID <em>Physical Asset ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetPropertyType#getPropertyID <em>Property ID</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedPhysicalAssetPropertyType()
 * @model extendedMetaData="name='TestedPhysicalAssetPropertyType' kind='elementOnly'"
 * @generated
 */
public interface TestedPhysicalAssetPropertyType extends EObject {
	/**
	 * Returns the value of the '<em><b>Physical Asset ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset ID</em>' containment reference.
	 * @see #setPhysicalAssetID(PhysicalAssetIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedPhysicalAssetPropertyType_PhysicalAssetID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetID' namespace='##targetNamespace'"
	 * @generated
	 */
	PhysicalAssetIDType getPhysicalAssetID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetPropertyType#getPhysicalAssetID <em>Physical Asset ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset ID</em>' containment reference.
	 * @see #getPhysicalAssetID()
	 * @generated
	 */
	void setPhysicalAssetID(PhysicalAssetIDType value);

	/**
	 * Returns the value of the '<em><b>Property ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property ID</em>' containment reference.
	 * @see #setPropertyID(PropertyIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedPhysicalAssetPropertyType_PropertyID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PropertyID' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyIDType getPropertyID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetPropertyType#getPropertyID <em>Property ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property ID</em>' containment reference.
	 * @see #getPropertyID()
	 * @generated
	 */
	void setPropertyID(PropertyIDType value);

} // TestedPhysicalAssetPropertyType
