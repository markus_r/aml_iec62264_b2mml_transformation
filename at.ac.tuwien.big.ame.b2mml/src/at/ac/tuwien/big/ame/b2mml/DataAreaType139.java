/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type139</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType139#getAcknowledge <em>Acknowledge</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType139#getMaterialInformation <em>Material Information</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType139()
 * @model extendedMetaData="name='DataArea_._140_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType139 extends EObject {
	/**
	 * Returns the value of the '<em><b>Acknowledge</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge</em>' containment reference.
	 * @see #setAcknowledge(TransAcknowledgeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType139_Acknowledge()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Acknowledge' namespace='##targetNamespace'"
	 * @generated
	 */
	TransAcknowledgeType getAcknowledge();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType139#getAcknowledge <em>Acknowledge</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge</em>' containment reference.
	 * @see #getAcknowledge()
	 * @generated
	 */
	void setAcknowledge(TransAcknowledgeType value);

	/**
	 * Returns the value of the '<em><b>Material Information</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialInformationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Information</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Information</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType139_MaterialInformation()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MaterialInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialInformationType> getMaterialInformation();

} // DataAreaType139
