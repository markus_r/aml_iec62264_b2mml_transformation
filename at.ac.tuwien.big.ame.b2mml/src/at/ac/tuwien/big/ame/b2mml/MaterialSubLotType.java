/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Sub Lot Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getLocation <em>Location</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getStatus <em>Status</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getMaterialSublotProperty <em>Material Sublot Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getStorageLocation <em>Storage Location</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getMaterialSubLot <em>Material Sub Lot</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getMaterialLotID <em>Material Lot ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getAssemblyLotID <em>Assembly Lot ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getAssemblySubLotID <em>Assembly Sub Lot ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType()
 * @model extendedMetaData="name='MaterialSubLotType' kind='elementOnly'"
 * @generated
 */
public interface MaterialSubLotType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_ID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Location</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' containment reference.
	 * @see #setLocation(LocationType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_Location()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Location' namespace='##targetNamespace'"
	 * @generated
	 */
	LocationType getLocation();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getLocation <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' containment reference.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(LocationType value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Status</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' containment reference.
	 * @see #setStatus(StatusType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_Status()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Status' namespace='##targetNamespace'"
	 * @generated
	 */
	StatusType getStatus();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getStatus <em>Status</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' containment reference.
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(StatusType value);

	/**
	 * Returns the value of the '<em><b>Material Sublot Property</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialLotPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Sublot Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Sublot Property</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_MaterialSublotProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialSublotProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialLotPropertyType> getMaterialSublotProperty();

	/**
	 * Returns the value of the '<em><b>Storage Location</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Location</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Location</em>' containment reference.
	 * @see #setStorageLocation(StorageHierarchyScopeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_StorageLocation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='StorageLocation' namespace='##targetNamespace'"
	 * @generated
	 */
	StorageHierarchyScopeType getStorageLocation();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getStorageLocation <em>Storage Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Location</em>' containment reference.
	 * @see #getStorageLocation()
	 * @generated
	 */
	void setStorageLocation(StorageHierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

	/**
	 * Returns the value of the '<em><b>Material Sub Lot</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Sub Lot</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Sub Lot</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_MaterialSubLot()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialSubLot' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialSubLotType> getMaterialSubLot();

	/**
	 * Returns the value of the '<em><b>Material Lot ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Lot ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Lot ID</em>' containment reference.
	 * @see #setMaterialLotID(MaterialLotIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_MaterialLotID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialLotID' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialLotIDType getMaterialLotID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getMaterialLotID <em>Material Lot ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Lot ID</em>' containment reference.
	 * @see #getMaterialLotID()
	 * @generated
	 */
	void setMaterialLotID(MaterialLotIDType value);

	/**
	 * Returns the value of the '<em><b>Assembly Lot ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialLotType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Lot ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Lot ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_AssemblyLotID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyLotID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialLotType> getAssemblyLotID();

	/**
	 * Returns the value of the '<em><b>Assembly Sub Lot ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Sub Lot ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Sub Lot ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_AssemblySubLotID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblySubLotID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialSubLotType> getAssemblySubLotID();

	/**
	 * Returns the value of the '<em><b>Assembly Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Type</em>' containment reference.
	 * @see #setAssemblyType(AssemblyTypeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_AssemblyType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyType' namespace='##targetNamespace'"
	 * @generated
	 */
	AssemblyTypeType getAssemblyType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getAssemblyType <em>Assembly Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Type</em>' containment reference.
	 * @see #getAssemblyType()
	 * @generated
	 */
	void setAssemblyType(AssemblyTypeType value);

	/**
	 * Returns the value of the '<em><b>Assembly Relationship</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Relationship</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Relationship</em>' containment reference.
	 * @see #setAssemblyRelationship(AssemblyRelationshipType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialSubLotType_AssemblyRelationship()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyRelationship' namespace='##targetNamespace'"
	 * @generated
	 */
	AssemblyRelationshipType getAssemblyRelationship();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.MaterialSubLotType#getAssemblyRelationship <em>Assembly Relationship</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Relationship</em>' containment reference.
	 * @see #getAssemblyRelationship()
	 * @generated
	 */
	void setAssemblyRelationship(AssemblyRelationshipType value);

} // MaterialSubLotType
