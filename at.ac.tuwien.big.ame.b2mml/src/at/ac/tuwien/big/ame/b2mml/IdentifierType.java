/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Identifier Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeAgencyID <em>Scheme Agency ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeAgencyName <em>Scheme Agency Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeDataURI <em>Scheme Data URI</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeID <em>Scheme ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeName <em>Scheme Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeURI <em>Scheme URI</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeVersionID <em>Scheme Version ID</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getIdentifierType()
 * @model extendedMetaData="name='IdentifierType' kind='simple'"
 * @generated
 */
public interface IdentifierType extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getIdentifierType_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Scheme Agency ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheme Agency ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheme Agency ID</em>' attribute.
	 * @see #setSchemeAgencyID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getIdentifierType_SchemeAgencyID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='schemeAgencyID'"
	 * @generated
	 */
	String getSchemeAgencyID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeAgencyID <em>Scheme Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheme Agency ID</em>' attribute.
	 * @see #getSchemeAgencyID()
	 * @generated
	 */
	void setSchemeAgencyID(String value);

	/**
	 * Returns the value of the '<em><b>Scheme Agency Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheme Agency Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheme Agency Name</em>' attribute.
	 * @see #setSchemeAgencyName(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getIdentifierType_SchemeAgencyName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='schemeAgencyName'"
	 * @generated
	 */
	String getSchemeAgencyName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeAgencyName <em>Scheme Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheme Agency Name</em>' attribute.
	 * @see #getSchemeAgencyName()
	 * @generated
	 */
	void setSchemeAgencyName(String value);

	/**
	 * Returns the value of the '<em><b>Scheme Data URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheme Data URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheme Data URI</em>' attribute.
	 * @see #setSchemeDataURI(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getIdentifierType_SchemeDataURI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='schemeDataURI'"
	 * @generated
	 */
	String getSchemeDataURI();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeDataURI <em>Scheme Data URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheme Data URI</em>' attribute.
	 * @see #getSchemeDataURI()
	 * @generated
	 */
	void setSchemeDataURI(String value);

	/**
	 * Returns the value of the '<em><b>Scheme ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheme ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheme ID</em>' attribute.
	 * @see #setSchemeID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getIdentifierType_SchemeID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='schemeID'"
	 * @generated
	 */
	String getSchemeID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeID <em>Scheme ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheme ID</em>' attribute.
	 * @see #getSchemeID()
	 * @generated
	 */
	void setSchemeID(String value);

	/**
	 * Returns the value of the '<em><b>Scheme Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheme Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheme Name</em>' attribute.
	 * @see #setSchemeName(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getIdentifierType_SchemeName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='schemeName'"
	 * @generated
	 */
	String getSchemeName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeName <em>Scheme Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheme Name</em>' attribute.
	 * @see #getSchemeName()
	 * @generated
	 */
	void setSchemeName(String value);

	/**
	 * Returns the value of the '<em><b>Scheme URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheme URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheme URI</em>' attribute.
	 * @see #setSchemeURI(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getIdentifierType_SchemeURI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='schemeURI'"
	 * @generated
	 */
	String getSchemeURI();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeURI <em>Scheme URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheme URI</em>' attribute.
	 * @see #getSchemeURI()
	 * @generated
	 */
	void setSchemeURI(String value);

	/**
	 * Returns the value of the '<em><b>Scheme Version ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheme Version ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheme Version ID</em>' attribute.
	 * @see #setSchemeVersionID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getIdentifierType_SchemeVersionID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='schemeVersionID'"
	 * @generated
	 */
	String getSchemeVersionID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.IdentifierType#getSchemeVersionID <em>Scheme Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheme Version ID</em>' attribute.
	 * @see #getSchemeVersionID()
	 * @generated
	 */
	void setSchemeVersionID(String value);

} // IdentifierType
