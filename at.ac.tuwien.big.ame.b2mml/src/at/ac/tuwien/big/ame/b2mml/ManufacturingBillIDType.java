/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Manufacturing Bill ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getManufacturingBillIDType()
 * @model extendedMetaData="name='ManufacturingBillIDType' kind='simple'"
 * @generated
 */
public interface ManufacturingBillIDType extends IdentifierType {
} // ManufacturingBillIDType
