/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Op Equipment Specification Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationType#getEquipmentClassID <em>Equipment Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationType#getEquipmentID <em>Equipment ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationType#getEquipmentUse <em>Equipment Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationType#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationType#getEquipmentSpecificationProperty <em>Equipment Specification Property</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpEquipmentSpecificationType()
 * @model extendedMetaData="name='OpEquipmentSpecificationType' kind='elementOnly'"
 * @generated
 */
public interface OpEquipmentSpecificationType extends EObject {
	/**
	 * Returns the value of the '<em><b>Equipment Class ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.EquipmentClassIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Class ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Class ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpEquipmentSpecificationType_EquipmentClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentClassIDType> getEquipmentClassID();

	/**
	 * Returns the value of the '<em><b>Equipment ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.EquipmentIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpEquipmentSpecificationType_EquipmentID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentIDType> getEquipmentID();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpEquipmentSpecificationType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Equipment Use</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Use</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Use</em>' containment reference.
	 * @see #setEquipmentUse(EquipmentUseType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpEquipmentSpecificationType_EquipmentUse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentUse' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentUseType getEquipmentUse();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationType#getEquipmentUse <em>Equipment Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Use</em>' containment reference.
	 * @see #getEquipmentUse()
	 * @generated
	 */
	void setEquipmentUse(EquipmentUseType value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpEquipmentSpecificationType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

	/**
	 * Returns the value of the '<em><b>Equipment Specification Property</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpEquipmentSpecificationPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Specification Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Specification Property</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpEquipmentSpecificationType_EquipmentSpecificationProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentSpecificationProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpEquipmentSpecificationPropertyType> getEquipmentSpecificationProperty();

} // OpEquipmentSpecificationType
