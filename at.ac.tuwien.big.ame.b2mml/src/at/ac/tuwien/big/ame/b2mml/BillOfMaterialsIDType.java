/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bill Of Materials ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getBillOfMaterialsIDType()
 * @model extendedMetaData="name='BillOfMaterialsIDType' kind='simple'"
 * @generated
 */
public interface BillOfMaterialsIDType extends IdentifierType {
} // BillOfMaterialsIDType
