/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.QualificationTestSpecificationIDType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Qualification Test Specification ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class QualificationTestSpecificationIDTypeImpl extends IdentifierTypeImpl implements QualificationTestSpecificationIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QualificationTestSpecificationIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getQualificationTestSpecificationIDType();
	}

} //QualificationTestSpecificationIDTypeImpl
