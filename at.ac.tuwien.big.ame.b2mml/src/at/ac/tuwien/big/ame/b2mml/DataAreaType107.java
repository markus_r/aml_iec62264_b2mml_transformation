/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type107</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType107#getProcess <em>Process</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType107#getOperationsCapability <em>Operations Capability</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType107()
 * @model extendedMetaData="name='DataArea_._108_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType107 extends EObject {
	/**
	 * Returns the value of the '<em><b>Process</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process</em>' containment reference.
	 * @see #setProcess(TransProcessType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType107_Process()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Process' namespace='##targetNamespace'"
	 * @generated
	 */
	TransProcessType getProcess();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType107#getProcess <em>Process</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process</em>' containment reference.
	 * @see #getProcess()
	 * @generated
	 */
	void setProcess(TransProcessType value);

	/**
	 * Returns the value of the '<em><b>Operations Capability</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Capability</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType107_OperationsCapability()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OperationsCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsCapabilityType> getOperationsCapability();

} // DataAreaType107
