/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Capability Test Specification Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationType#getName <em>Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationType#getVersion <em>Version</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationType#getTestedPhysicalAssetProperty <em>Tested Physical Asset Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationType#getTestedPhysicalAssetClassProperty <em>Tested Physical Asset Class Property</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetCapabilityTestSpecificationType()
 * @model extendedMetaData="name='PhysicalAssetCapabilityTestSpecificationType' kind='elementOnly'"
 * @generated
 */
public interface PhysicalAssetCapabilityTestSpecificationType extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' containment reference.
	 * @see #setName(NameType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetCapabilityTestSpecificationType_Name()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Name' namespace='##targetNamespace'"
	 * @generated
	 */
	NameType getName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationType#getName <em>Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' containment reference.
	 * @see #getName()
	 * @generated
	 */
	void setName(NameType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetCapabilityTestSpecificationType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Version</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' containment reference.
	 * @see #setVersion(VersionType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetCapabilityTestSpecificationType_Version()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Version' namespace='##targetNamespace'"
	 * @generated
	 */
	VersionType getVersion();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationType#getVersion <em>Version</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' containment reference.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(VersionType value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetCapabilityTestSpecificationType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetCapabilityTestSpecificationType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Tested Physical Asset Property</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tested Physical Asset Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tested Physical Asset Property</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetCapabilityTestSpecificationType_TestedPhysicalAssetProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TestedPhysicalAssetProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TestedPhysicalAssetPropertyType> getTestedPhysicalAssetProperty();

	/**
	 * Returns the value of the '<em><b>Tested Physical Asset Class Property</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetClassPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tested Physical Asset Class Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tested Physical Asset Class Property</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetCapabilityTestSpecificationType_TestedPhysicalAssetClassProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TestedPhysicalAssetClassProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TestedPhysicalAssetClassPropertyType> getTestedPhysicalAssetClassProperty();

} // PhysicalAssetCapabilityTestSpecificationType
