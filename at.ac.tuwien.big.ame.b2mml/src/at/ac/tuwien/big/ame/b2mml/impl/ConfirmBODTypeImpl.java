/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.ConfirmBODType;
import at.ac.tuwien.big.ame.b2mml.DataAreaType164;
import at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Confirm BOD Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ConfirmBODTypeImpl#getApplicationArea <em>Application Area</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.ConfirmBODTypeImpl#getDataArea <em>Data Area</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConfirmBODTypeImpl extends MinimalEObjectImpl.Container implements ConfirmBODType {
	/**
	 * The cached value of the '{@link #getApplicationArea() <em>Application Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplicationArea()
	 * @generated
	 * @ordered
	 */
	protected TransApplicationAreaType applicationArea;

	/**
	 * The cached value of the '{@link #getDataArea() <em>Data Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataArea()
	 * @generated
	 * @ordered
	 */
	protected DataAreaType164 dataArea;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfirmBODTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getConfirmBODType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransApplicationAreaType getApplicationArea() {
		return applicationArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetApplicationArea(TransApplicationAreaType newApplicationArea, NotificationChain msgs) {
		TransApplicationAreaType oldApplicationArea = applicationArea;
		applicationArea = newApplicationArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.CONFIRM_BOD_TYPE__APPLICATION_AREA, oldApplicationArea, newApplicationArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicationArea(TransApplicationAreaType newApplicationArea) {
		if (newApplicationArea != applicationArea) {
			NotificationChain msgs = null;
			if (applicationArea != null)
				msgs = ((InternalEObject)applicationArea).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.CONFIRM_BOD_TYPE__APPLICATION_AREA, null, msgs);
			if (newApplicationArea != null)
				msgs = ((InternalEObject)newApplicationArea).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.CONFIRM_BOD_TYPE__APPLICATION_AREA, null, msgs);
			msgs = basicSetApplicationArea(newApplicationArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CONFIRM_BOD_TYPE__APPLICATION_AREA, newApplicationArea, newApplicationArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType164 getDataArea() {
		return dataArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataArea(DataAreaType164 newDataArea, NotificationChain msgs) {
		DataAreaType164 oldDataArea = dataArea;
		dataArea = newDataArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.CONFIRM_BOD_TYPE__DATA_AREA, oldDataArea, newDataArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataArea(DataAreaType164 newDataArea) {
		if (newDataArea != dataArea) {
			NotificationChain msgs = null;
			if (dataArea != null)
				msgs = ((InternalEObject)dataArea).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.CONFIRM_BOD_TYPE__DATA_AREA, null, msgs);
			if (newDataArea != null)
				msgs = ((InternalEObject)newDataArea).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.CONFIRM_BOD_TYPE__DATA_AREA, null, msgs);
			msgs = basicSetDataArea(newDataArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CONFIRM_BOD_TYPE__DATA_AREA, newDataArea, newDataArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.CONFIRM_BOD_TYPE__APPLICATION_AREA:
				return basicSetApplicationArea(null, msgs);
			case B2MMLPackage.CONFIRM_BOD_TYPE__DATA_AREA:
				return basicSetDataArea(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.CONFIRM_BOD_TYPE__APPLICATION_AREA:
				return getApplicationArea();
			case B2MMLPackage.CONFIRM_BOD_TYPE__DATA_AREA:
				return getDataArea();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.CONFIRM_BOD_TYPE__APPLICATION_AREA:
				setApplicationArea((TransApplicationAreaType)newValue);
				return;
			case B2MMLPackage.CONFIRM_BOD_TYPE__DATA_AREA:
				setDataArea((DataAreaType164)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.CONFIRM_BOD_TYPE__APPLICATION_AREA:
				setApplicationArea((TransApplicationAreaType)null);
				return;
			case B2MMLPackage.CONFIRM_BOD_TYPE__DATA_AREA:
				setDataArea((DataAreaType164)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.CONFIRM_BOD_TYPE__APPLICATION_AREA:
				return applicationArea != null;
			case B2MMLPackage.CONFIRM_BOD_TYPE__DATA_AREA:
				return dataArea != null;
		}
		return super.eIsSet(featureID);
	}

} //ConfirmBODTypeImpl
