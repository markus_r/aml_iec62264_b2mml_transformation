/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Capability Test Specification ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPhysicalAssetCapabilityTestSpecificationIDType()
 * @model extendedMetaData="name='PhysicalAssetCapabilityTestSpecificationIDType' kind='simple'"
 * @generated
 */
public interface PhysicalAssetCapabilityTestSpecificationIDType extends IdentifierType {
} // PhysicalAssetCapabilityTestSpecificationIDType
