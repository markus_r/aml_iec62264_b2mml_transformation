/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DataAreaType59;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassType;
import at.ac.tuwien.big.ame.b2mml.TransSyncType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type59</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType59Impl#getSync <em>Sync</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType59Impl#getPhysicalAssetClass <em>Physical Asset Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType59Impl extends MinimalEObjectImpl.Container implements DataAreaType59 {
	/**
	 * The cached value of the '{@link #getSync() <em>Sync</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSync()
	 * @generated
	 * @ordered
	 */
	protected TransSyncType sync;

	/**
	 * The cached value of the '{@link #getPhysicalAssetClass() <em>Physical Asset Class</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClass()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetClassType> physicalAssetClass;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType59Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType59();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransSyncType getSync() {
		return sync;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSync(TransSyncType newSync, NotificationChain msgs) {
		TransSyncType oldSync = sync;
		sync = newSync;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE59__SYNC, oldSync, newSync);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSync(TransSyncType newSync) {
		if (newSync != sync) {
			NotificationChain msgs = null;
			if (sync != null)
				msgs = ((InternalEObject)sync).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE59__SYNC, null, msgs);
			if (newSync != null)
				msgs = ((InternalEObject)newSync).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE59__SYNC, null, msgs);
			msgs = basicSetSync(newSync, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE59__SYNC, newSync, newSync));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetClassType> getPhysicalAssetClass() {
		if (physicalAssetClass == null) {
			physicalAssetClass = new EObjectContainmentEList<PhysicalAssetClassType>(PhysicalAssetClassType.class, this, B2MMLPackage.DATA_AREA_TYPE59__PHYSICAL_ASSET_CLASS);
		}
		return physicalAssetClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE59__SYNC:
				return basicSetSync(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE59__PHYSICAL_ASSET_CLASS:
				return ((InternalEList<?>)getPhysicalAssetClass()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE59__SYNC:
				return getSync();
			case B2MMLPackage.DATA_AREA_TYPE59__PHYSICAL_ASSET_CLASS:
				return getPhysicalAssetClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE59__SYNC:
				setSync((TransSyncType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE59__PHYSICAL_ASSET_CLASS:
				getPhysicalAssetClass().clear();
				getPhysicalAssetClass().addAll((Collection<? extends PhysicalAssetClassType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE59__SYNC:
				setSync((TransSyncType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE59__PHYSICAL_ASSET_CLASS:
				getPhysicalAssetClass().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE59__SYNC:
				return sync != null;
			case B2MMLPackage.DATA_AREA_TYPE59__PHYSICAL_ASSET_CLASS:
				return physicalAssetClass != null && !physicalAssetClass.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType59Impl
