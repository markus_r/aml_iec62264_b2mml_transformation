/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tested Physical Asset Class Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetClassPropertyType#getPhysicalAssetClassID <em>Physical Asset Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetClassPropertyType#getPropertyID <em>Property ID</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedPhysicalAssetClassPropertyType()
 * @model extendedMetaData="name='TestedPhysicalAssetClassPropertyType' kind='elementOnly'"
 * @generated
 */
public interface TestedPhysicalAssetClassPropertyType extends EObject {
	/**
	 * Returns the value of the '<em><b>Physical Asset Class ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Class ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Class ID</em>' containment reference.
	 * @see #setPhysicalAssetClassID(PhysicalAssetClassIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedPhysicalAssetClassPropertyType_PhysicalAssetClassID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	PhysicalAssetClassIDType getPhysicalAssetClassID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetClassPropertyType#getPhysicalAssetClassID <em>Physical Asset Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Class ID</em>' containment reference.
	 * @see #getPhysicalAssetClassID()
	 * @generated
	 */
	void setPhysicalAssetClassID(PhysicalAssetClassIDType value);

	/**
	 * Returns the value of the '<em><b>Property ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property ID</em>' containment reference.
	 * @see #setPropertyID(PropertyIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedPhysicalAssetClassPropertyType_PropertyID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PropertyID' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyIDType getPropertyID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestedPhysicalAssetClassPropertyType#getPropertyID <em>Property ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property ID</em>' containment reference.
	 * @see #getPropertyID()
	 * @generated
	 */
	void setPropertyID(PropertyIDType value);

} // TestedPhysicalAssetClassPropertyType
