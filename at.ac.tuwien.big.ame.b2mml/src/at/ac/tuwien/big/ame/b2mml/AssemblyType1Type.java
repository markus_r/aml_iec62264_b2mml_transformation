/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assembly Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getAssemblyType1Type()
 * @model extendedMetaData="name='AssemblyType1Type' kind='simple'"
 * @generated
 */
public interface AssemblyType1Type extends CodeType {
} // AssemblyType1Type
