/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DataAreaType28;
import at.ac.tuwien.big.ame.b2mml.OperationsDefinitionType;
import at.ac.tuwien.big.ame.b2mml.TransRespondType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type28</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType28Impl#getRespond <em>Respond</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType28Impl#getOperationsDefinition <em>Operations Definition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType28Impl extends MinimalEObjectImpl.Container implements DataAreaType28 {
	/**
	 * The cached value of the '{@link #getRespond() <em>Respond</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRespond()
	 * @generated
	 * @ordered
	 */
	protected TransRespondType respond;

	/**
	 * The cached value of the '{@link #getOperationsDefinition() <em>Operations Definition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsDefinitionType> operationsDefinition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType28Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType28();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransRespondType getRespond() {
		return respond;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespond(TransRespondType newRespond, NotificationChain msgs) {
		TransRespondType oldRespond = respond;
		respond = newRespond;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE28__RESPOND, oldRespond, newRespond);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespond(TransRespondType newRespond) {
		if (newRespond != respond) {
			NotificationChain msgs = null;
			if (respond != null)
				msgs = ((InternalEObject)respond).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE28__RESPOND, null, msgs);
			if (newRespond != null)
				msgs = ((InternalEObject)newRespond).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE28__RESPOND, null, msgs);
			msgs = basicSetRespond(newRespond, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE28__RESPOND, newRespond, newRespond));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsDefinitionType> getOperationsDefinition() {
		if (operationsDefinition == null) {
			operationsDefinition = new EObjectContainmentEList<OperationsDefinitionType>(OperationsDefinitionType.class, this, B2MMLPackage.DATA_AREA_TYPE28__OPERATIONS_DEFINITION);
		}
		return operationsDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE28__RESPOND:
				return basicSetRespond(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE28__OPERATIONS_DEFINITION:
				return ((InternalEList<?>)getOperationsDefinition()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE28__RESPOND:
				return getRespond();
			case B2MMLPackage.DATA_AREA_TYPE28__OPERATIONS_DEFINITION:
				return getOperationsDefinition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE28__RESPOND:
				setRespond((TransRespondType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE28__OPERATIONS_DEFINITION:
				getOperationsDefinition().clear();
				getOperationsDefinition().addAll((Collection<? extends OperationsDefinitionType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE28__RESPOND:
				setRespond((TransRespondType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE28__OPERATIONS_DEFINITION:
				getOperationsDefinition().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE28__RESPOND:
				return respond != null;
			case B2MMLPackage.DATA_AREA_TYPE28__OPERATIONS_DEFINITION:
				return operationsDefinition != null && !operationsDefinition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType28Impl
