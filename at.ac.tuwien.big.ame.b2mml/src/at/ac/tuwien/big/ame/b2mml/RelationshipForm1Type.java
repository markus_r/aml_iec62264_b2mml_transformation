/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relationship Form1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getRelationshipForm1Type()
 * @model extendedMetaData="name='RelationshipForm1Type' kind='simple'"
 * @generated
 */
public interface RelationshipForm1Type extends CodeType {
} // RelationshipForm1Type
