/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Production Schedule ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getProductionScheduleIDType()
 * @model extendedMetaData="name='ProductionScheduleIDType' kind='simple'"
 * @generated
 */
public interface ProductionScheduleIDType extends IdentifierType {
} // ProductionScheduleIDType
