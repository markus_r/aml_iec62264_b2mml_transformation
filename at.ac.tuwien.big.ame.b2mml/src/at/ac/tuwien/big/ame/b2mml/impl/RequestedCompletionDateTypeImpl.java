/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.RequestedCompletionDateType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requested Completion Date Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequestedCompletionDateTypeImpl extends DateTimeTypeImpl implements RequestedCompletionDateType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequestedCompletionDateTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getRequestedCompletionDateType();
	}

} //RequestedCompletionDateTypeImpl
