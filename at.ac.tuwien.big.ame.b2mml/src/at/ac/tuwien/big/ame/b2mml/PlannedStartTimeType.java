/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Planned Start Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPlannedStartTimeType()
 * @model extendedMetaData="name='PlannedStartTimeType' kind='simple'"
 * @generated
 */
public interface PlannedStartTimeType extends DateTimeType {
} // PlannedStartTimeType
