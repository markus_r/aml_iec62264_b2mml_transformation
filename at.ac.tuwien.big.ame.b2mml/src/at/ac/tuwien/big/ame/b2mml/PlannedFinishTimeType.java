/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Planned Finish Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPlannedFinishTimeType()
 * @model extendedMetaData="name='PlannedFinishTimeType' kind='simple'"
 * @generated
 */
public interface PlannedFinishTimeType extends DateTimeType {
} // PlannedFinishTimeType
