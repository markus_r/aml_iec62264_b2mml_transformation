/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type164</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType164#getConfirm <em>Confirm</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType164#getBOD <em>BOD</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType164()
 * @model extendedMetaData="name='DataArea_._165_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType164 extends EObject {
	/**
	 * Returns the value of the '<em><b>Confirm</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Confirm</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confirm</em>' containment reference.
	 * @see #setConfirm(TransConfirmType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType164_Confirm()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Confirm' namespace='##targetNamespace'"
	 * @generated
	 */
	TransConfirmType getConfirm();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType164#getConfirm <em>Confirm</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confirm</em>' containment reference.
	 * @see #getConfirm()
	 * @generated
	 */
	void setConfirm(TransConfirmType value);

	/**
	 * Returns the value of the '<em><b>BOD</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.BODType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>BOD</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BOD</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType164_BOD()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='BOD' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<BODType> getBOD();

} // DataAreaType164
