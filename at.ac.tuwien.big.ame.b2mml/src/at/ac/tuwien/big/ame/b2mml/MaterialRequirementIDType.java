/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Requirement ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialRequirementIDType()
 * @model extendedMetaData="name='MaterialRequirementIDType' kind='simple'"
 * @generated
 */
public interface MaterialRequirementIDType extends IdentifierType {
} // MaterialRequirementIDType
