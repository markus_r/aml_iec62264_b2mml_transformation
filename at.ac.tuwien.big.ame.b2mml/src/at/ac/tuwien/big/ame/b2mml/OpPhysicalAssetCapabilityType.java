/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Op Physical Asset Capability Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getPhysicalAssetClassID <em>Physical Asset Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getPhysicalAssetID <em>Physical Asset ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getReason <em>Reason</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getPhysicalAssetUse <em>Physical Asset Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getPhysicalAssetCapabilityProperty <em>Physical Asset Capability Property</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType()
 * @model extendedMetaData="name='OpPhysicalAssetCapabilityType' kind='elementOnly'"
 * @generated
 */
public interface OpPhysicalAssetCapabilityType extends EObject {
	/**
	 * Returns the value of the '<em><b>Physical Asset Class ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Class ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Class ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType_PhysicalAssetClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetClassIDType> getPhysicalAssetClassID();

	/**
	 * Returns the value of the '<em><b>Physical Asset ID</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.PhysicalAssetIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset ID</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType_PhysicalAssetID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetIDType> getPhysicalAssetID();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Capability Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capability Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capability Type</em>' containment reference.
	 * @see #setCapabilityType(CapabilityTypeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType_CapabilityType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='CapabilityType' namespace='##targetNamespace'"
	 * @generated
	 */
	CapabilityTypeType getCapabilityType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getCapabilityType <em>Capability Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Capability Type</em>' containment reference.
	 * @see #getCapabilityType()
	 * @generated
	 */
	void setCapabilityType(CapabilityTypeType value);

	/**
	 * Returns the value of the '<em><b>Reason</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reason</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reason</em>' containment reference.
	 * @see #setReason(ReasonType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType_Reason()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Reason' namespace='##targetNamespace'"
	 * @generated
	 */
	ReasonType getReason();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getReason <em>Reason</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reason</em>' containment reference.
	 * @see #getReason()
	 * @generated
	 */
	void setReason(ReasonType value);

	/**
	 * Returns the value of the '<em><b>Confidence Factor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Confidence Factor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confidence Factor</em>' containment reference.
	 * @see #setConfidenceFactor(ConfidenceFactorType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType_ConfidenceFactor()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ConfidenceFactor' namespace='##targetNamespace'"
	 * @generated
	 */
	ConfidenceFactorType getConfidenceFactor();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getConfidenceFactor <em>Confidence Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confidence Factor</em>' containment reference.
	 * @see #getConfidenceFactor()
	 * @generated
	 */
	void setConfidenceFactor(ConfidenceFactorType value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Physical Asset Use</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Use</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Use</em>' containment reference.
	 * @see #setPhysicalAssetUse(PhysicalAssetUseType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType_PhysicalAssetUse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetUse' namespace='##targetNamespace'"
	 * @generated
	 */
	PhysicalAssetUseType getPhysicalAssetUse();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getPhysicalAssetUse <em>Physical Asset Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Use</em>' containment reference.
	 * @see #getPhysicalAssetUse()
	 * @generated
	 */
	void setPhysicalAssetUse(PhysicalAssetUseType value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' containment reference.
	 * @see #setStartTime(StartTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType_StartTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='StartTime' namespace='##targetNamespace'"
	 * @generated
	 */
	StartTimeType getStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getStartTime <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' containment reference.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(StartTimeType value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' containment reference.
	 * @see #setEndTime(EndTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType_EndTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EndTime' namespace='##targetNamespace'"
	 * @generated
	 */
	EndTimeType getEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType#getEndTime <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' containment reference.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(EndTimeType value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

	/**
	 * Returns the value of the '<em><b>Physical Asset Capability Property</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Capability Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Capability Property</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOpPhysicalAssetCapabilityType_PhysicalAssetCapabilityProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetCapabilityProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPhysicalAssetCapabilityPropertyType> getPhysicalAssetCapabilityProperty();

} // OpPhysicalAssetCapabilityType
