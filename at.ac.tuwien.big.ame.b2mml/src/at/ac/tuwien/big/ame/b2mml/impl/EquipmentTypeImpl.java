/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.EquipmentAssetMappingType;
import at.ac.tuwien.big.ame.b2mml.EquipmentCapabilityTestSpecificationIDType;
import at.ac.tuwien.big.ame.b2mml.EquipmentClassIDType;
import at.ac.tuwien.big.ame.b2mml.EquipmentPropertyType;
import at.ac.tuwien.big.ame.b2mml.EquipmentType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.LocationType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentTypeImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentTypeImpl#getEquipmentLevel <em>Equipment Level</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentTypeImpl#getEquipmentAssetMapping <em>Equipment Asset Mapping</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentTypeImpl#getEquipmentProperty <em>Equipment Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentTypeImpl#getEquipment <em>Equipment</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentTypeImpl#getEquipmentClassID <em>Equipment Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentTypeImpl#getEquipmentCapabilityTestSpecificationID <em>Equipment Capability Test Specification ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EquipmentTypeImpl extends MinimalEObjectImpl.Container implements EquipmentType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected LocationType location;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getEquipmentLevel() <em>Equipment Level</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentLevel()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType equipmentLevel;

	/**
	 * The cached value of the '{@link #getEquipmentAssetMapping() <em>Equipment Asset Mapping</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentAssetMapping()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentAssetMappingType> equipmentAssetMapping;

	/**
	 * The cached value of the '{@link #getEquipmentProperty() <em>Equipment Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentPropertyType> equipmentProperty;

	/**
	 * The cached value of the '{@link #getEquipment() <em>Equipment</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipment()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentType> equipment;

	/**
	 * The cached value of the '{@link #getEquipmentClassID() <em>Equipment Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentClassIDType> equipmentClassID;

	/**
	 * The cached value of the '{@link #getEquipmentCapabilityTestSpecificationID() <em>Equipment Capability Test Specification ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentCapabilityTestSpecificationID()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentCapabilityTestSpecificationIDType> equipmentCapabilityTestSpecificationID;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getEquipmentType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.EQUIPMENT_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationType getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocation(LocationType newLocation, NotificationChain msgs) {
		LocationType oldLocation = location;
		location = newLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_TYPE__LOCATION, oldLocation, newLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(LocationType newLocation) {
		if (newLocation != location) {
			NotificationChain msgs = null;
			if (location != null)
				msgs = ((InternalEObject)location).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_TYPE__LOCATION, null, msgs);
			if (newLocation != null)
				msgs = ((InternalEObject)newLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_TYPE__LOCATION, null, msgs);
			msgs = basicSetLocation(newLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_TYPE__LOCATION, newLocation, newLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getEquipmentLevel() {
		return equipmentLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentLevel(HierarchyScopeType newEquipmentLevel, NotificationChain msgs) {
		HierarchyScopeType oldEquipmentLevel = equipmentLevel;
		equipmentLevel = newEquipmentLevel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_LEVEL, oldEquipmentLevel, newEquipmentLevel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentLevel(HierarchyScopeType newEquipmentLevel) {
		if (newEquipmentLevel != equipmentLevel) {
			NotificationChain msgs = null;
			if (equipmentLevel != null)
				msgs = ((InternalEObject)equipmentLevel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_LEVEL, null, msgs);
			if (newEquipmentLevel != null)
				msgs = ((InternalEObject)newEquipmentLevel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_LEVEL, null, msgs);
			msgs = basicSetEquipmentLevel(newEquipmentLevel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_LEVEL, newEquipmentLevel, newEquipmentLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentAssetMappingType> getEquipmentAssetMapping() {
		if (equipmentAssetMapping == null) {
			equipmentAssetMapping = new EObjectContainmentEList<EquipmentAssetMappingType>(EquipmentAssetMappingType.class, this, B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_ASSET_MAPPING);
		}
		return equipmentAssetMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentPropertyType> getEquipmentProperty() {
		if (equipmentProperty == null) {
			equipmentProperty = new EObjectContainmentEList<EquipmentPropertyType>(EquipmentPropertyType.class, this, B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_PROPERTY);
		}
		return equipmentProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentType> getEquipment() {
		if (equipment == null) {
			equipment = new EObjectContainmentEList<EquipmentType>(EquipmentType.class, this, B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT);
		}
		return equipment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentClassIDType> getEquipmentClassID() {
		if (equipmentClassID == null) {
			equipmentClassID = new EObjectContainmentEList<EquipmentClassIDType>(EquipmentClassIDType.class, this, B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_CLASS_ID);
		}
		return equipmentClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentCapabilityTestSpecificationIDType> getEquipmentCapabilityTestSpecificationID() {
		if (equipmentCapabilityTestSpecificationID == null) {
			equipmentCapabilityTestSpecificationID = new EObjectContainmentEList<EquipmentCapabilityTestSpecificationIDType>(EquipmentCapabilityTestSpecificationIDType.class, this, B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID);
		}
		return equipmentCapabilityTestSpecificationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.EQUIPMENT_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.EQUIPMENT_TYPE__LOCATION:
				return basicSetLocation(null, msgs);
			case B2MMLPackage.EQUIPMENT_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_LEVEL:
				return basicSetEquipmentLevel(null, msgs);
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_ASSET_MAPPING:
				return ((InternalEList<?>)getEquipmentAssetMapping()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_PROPERTY:
				return ((InternalEList<?>)getEquipmentProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT:
				return ((InternalEList<?>)getEquipment()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_CLASS_ID:
				return ((InternalEList<?>)getEquipmentClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID:
				return ((InternalEList<?>)getEquipmentCapabilityTestSpecificationID()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_TYPE__ID:
				return getID();
			case B2MMLPackage.EQUIPMENT_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.EQUIPMENT_TYPE__LOCATION:
				return getLocation();
			case B2MMLPackage.EQUIPMENT_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_LEVEL:
				return getEquipmentLevel();
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_ASSET_MAPPING:
				return getEquipmentAssetMapping();
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_PROPERTY:
				return getEquipmentProperty();
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT:
				return getEquipment();
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_CLASS_ID:
				return getEquipmentClassID();
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID:
				return getEquipmentCapabilityTestSpecificationID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__LOCATION:
				setLocation((LocationType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_LEVEL:
				setEquipmentLevel((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_ASSET_MAPPING:
				getEquipmentAssetMapping().clear();
				getEquipmentAssetMapping().addAll((Collection<? extends EquipmentAssetMappingType>)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_PROPERTY:
				getEquipmentProperty().clear();
				getEquipmentProperty().addAll((Collection<? extends EquipmentPropertyType>)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT:
				getEquipment().clear();
				getEquipment().addAll((Collection<? extends EquipmentType>)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_CLASS_ID:
				getEquipmentClassID().clear();
				getEquipmentClassID().addAll((Collection<? extends EquipmentClassIDType>)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID:
				getEquipmentCapabilityTestSpecificationID().clear();
				getEquipmentCapabilityTestSpecificationID().addAll((Collection<? extends EquipmentCapabilityTestSpecificationIDType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__LOCATION:
				setLocation((LocationType)null);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_LEVEL:
				setEquipmentLevel((HierarchyScopeType)null);
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_ASSET_MAPPING:
				getEquipmentAssetMapping().clear();
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_PROPERTY:
				getEquipmentProperty().clear();
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT:
				getEquipment().clear();
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_CLASS_ID:
				getEquipmentClassID().clear();
				return;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID:
				getEquipmentCapabilityTestSpecificationID().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_TYPE__ID:
				return iD != null;
			case B2MMLPackage.EQUIPMENT_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.EQUIPMENT_TYPE__LOCATION:
				return location != null;
			case B2MMLPackage.EQUIPMENT_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_LEVEL:
				return equipmentLevel != null;
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_ASSET_MAPPING:
				return equipmentAssetMapping != null && !equipmentAssetMapping.isEmpty();
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_PROPERTY:
				return equipmentProperty != null && !equipmentProperty.isEmpty();
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT:
				return equipment != null && !equipment.isEmpty();
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_CLASS_ID:
				return equipmentClassID != null && !equipmentClassID.isEmpty();
			case B2MMLPackage.EQUIPMENT_TYPE__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID:
				return equipmentCapabilityTestSpecificationID != null && !equipmentCapabilityTestSpecificationID.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EquipmentTypeImpl
