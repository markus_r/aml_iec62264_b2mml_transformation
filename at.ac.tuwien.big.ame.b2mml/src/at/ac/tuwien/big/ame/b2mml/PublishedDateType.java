/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Published Date Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPublishedDateType()
 * @model extendedMetaData="name='PublishedDateType' kind='simple'"
 * @generated
 */
public interface PublishedDateType extends DateTimeType {
} // PublishedDateType
