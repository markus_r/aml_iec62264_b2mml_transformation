/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.OpPersonnelSpecificationPropertyType;
import at.ac.tuwien.big.ame.b2mml.OpPersonnelSpecificationType;
import at.ac.tuwien.big.ame.b2mml.PersonIDType;
import at.ac.tuwien.big.ame.b2mml.PersonnelClassIDType;
import at.ac.tuwien.big.ame.b2mml.PersonnelUseType;
import at.ac.tuwien.big.ame.b2mml.QuantityValueType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Personnel Specification Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelSpecificationTypeImpl#getPersonnelClassID <em>Personnel Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelSpecificationTypeImpl#getPersonID <em>Person ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelSpecificationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelSpecificationTypeImpl#getPersonnelUse <em>Personnel Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelSpecificationTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OpPersonnelSpecificationTypeImpl#getPersonnelSpecificationProperty <em>Personnel Specification Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpPersonnelSpecificationTypeImpl extends MinimalEObjectImpl.Container implements OpPersonnelSpecificationType {
	/**
	 * The cached value of the '{@link #getPersonnelClassID() <em>Personnel Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelClassIDType> personnelClassID;

	/**
	 * The cached value of the '{@link #getPersonID() <em>Person ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonID()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonIDType> personID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getPersonnelUse() <em>Personnel Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelUse()
	 * @generated
	 * @ordered
	 */
	protected PersonnelUseType personnelUse;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getPersonnelSpecificationProperty() <em>Personnel Specification Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelSpecificationProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPersonnelSpecificationPropertyType> personnelSpecificationProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpPersonnelSpecificationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpPersonnelSpecificationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelClassIDType> getPersonnelClassID() {
		if (personnelClassID == null) {
			personnelClassID = new EObjectContainmentEList<PersonnelClassIDType>(PersonnelClassIDType.class, this, B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID);
		}
		return personnelClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonIDType> getPersonID() {
		if (personID == null) {
			personID = new EObjectContainmentEList<PersonIDType>(PersonIDType.class, this, B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSON_ID);
		}
		return personID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelUseType getPersonnelUse() {
		return personnelUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPersonnelUse(PersonnelUseType newPersonnelUse, NotificationChain msgs) {
		PersonnelUseType oldPersonnelUse = personnelUse;
		personnelUse = newPersonnelUse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_USE, oldPersonnelUse, newPersonnelUse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonnelUse(PersonnelUseType newPersonnelUse) {
		if (newPersonnelUse != personnelUse) {
			NotificationChain msgs = null;
			if (personnelUse != null)
				msgs = ((InternalEObject)personnelUse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_USE, null, msgs);
			if (newPersonnelUse != null)
				msgs = ((InternalEObject)newPersonnelUse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_USE, null, msgs);
			msgs = basicSetPersonnelUse(newPersonnelUse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_USE, newPersonnelUse, newPersonnelUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPersonnelSpecificationPropertyType> getPersonnelSpecificationProperty() {
		if (personnelSpecificationProperty == null) {
			personnelSpecificationProperty = new EObjectContainmentEList<OpPersonnelSpecificationPropertyType>(OpPersonnelSpecificationPropertyType.class, this, B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_SPECIFICATION_PROPERTY);
		}
		return personnelSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID:
				return ((InternalEList<?>)getPersonnelClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSON_ID:
				return ((InternalEList<?>)getPersonID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_USE:
				return basicSetPersonnelUse(null, msgs);
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_SPECIFICATION_PROPERTY:
				return ((InternalEList<?>)getPersonnelSpecificationProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID:
				return getPersonnelClassID();
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSON_ID:
				return getPersonID();
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_USE:
				return getPersonnelUse();
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_SPECIFICATION_PROPERTY:
				return getPersonnelSpecificationProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID:
				getPersonnelClassID().clear();
				getPersonnelClassID().addAll((Collection<? extends PersonnelClassIDType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSON_ID:
				getPersonID().clear();
				getPersonID().addAll((Collection<? extends PersonIDType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_USE:
				setPersonnelUse((PersonnelUseType)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_SPECIFICATION_PROPERTY:
				getPersonnelSpecificationProperty().clear();
				getPersonnelSpecificationProperty().addAll((Collection<? extends OpPersonnelSpecificationPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID:
				getPersonnelClassID().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSON_ID:
				getPersonID().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_USE:
				setPersonnelUse((PersonnelUseType)null);
				return;
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_SPECIFICATION_PROPERTY:
				getPersonnelSpecificationProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_CLASS_ID:
				return personnelClassID != null && !personnelClassID.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSON_ID:
				return personID != null && !personID.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_USE:
				return personnelUse != null;
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE__PERSONNEL_SPECIFICATION_PROPERTY:
				return personnelSpecificationProperty != null && !personnelSpecificationProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OpPersonnelSpecificationTypeImpl
