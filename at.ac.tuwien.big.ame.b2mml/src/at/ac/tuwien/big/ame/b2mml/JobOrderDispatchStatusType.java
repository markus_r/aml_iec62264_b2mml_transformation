/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Job Order Dispatch Status Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getJobOrderDispatchStatusType()
 * @model extendedMetaData="name='JobOrderDispatchStatusType' kind='simple'"
 * @generated
 */
public interface JobOrderDispatchStatusType extends CodeType {
} // JobOrderDispatchStatusType
