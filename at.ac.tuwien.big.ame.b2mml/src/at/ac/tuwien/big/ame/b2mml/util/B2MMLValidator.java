/**
 */
package at.ac.tuwien.big.ame.b2mml.util;

import at.ac.tuwien.big.ame.b2mml.*;

import java.util.Map;

import javax.xml.datatype.Duration;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.eclipse.emf.ecore.xml.type.util.XMLTypeUtil;
import org.eclipse.emf.ecore.xml.type.util.XMLTypeValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage
 * @generated
 */
public class B2MMLValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final B2MMLValidator INSTANCE = new B2MMLValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "at.ac.tuwien.big.ame.b2mml";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * The cached base package validator.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLTypeValidator xmlTypeValidator;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public B2MMLValidator() {
		super();
		xmlTypeValidator = XMLTypeValidator.INSTANCE;
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return B2MMLPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case B2MMLPackage.ACKNOWLEDGE_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE:
				return validateAcknowledgeEquipmentCapabilityTestSpecType((AcknowledgeEquipmentCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_EQUIPMENT_CLASS_TYPE:
				return validateAcknowledgeEquipmentClassType((AcknowledgeEquipmentClassType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_EQUIPMENT_INFORMATION_TYPE:
				return validateAcknowledgeEquipmentInformationType((AcknowledgeEquipmentInformationType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_EQUIPMENT_TYPE:
				return validateAcknowledgeEquipmentType((AcknowledgeEquipmentType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_CLASS_TYPE:
				return validateAcknowledgeMaterialClassType((AcknowledgeMaterialClassType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_DEFINITION_TYPE:
				return validateAcknowledgeMaterialDefinitionType((AcknowledgeMaterialDefinitionType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_INFORMATION_TYPE:
				return validateAcknowledgeMaterialInformationType((AcknowledgeMaterialInformationType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_LOT_TYPE:
				return validateAcknowledgeMaterialLotType((AcknowledgeMaterialLotType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_SUB_LOT_TYPE:
				return validateAcknowledgeMaterialSubLotType((AcknowledgeMaterialSubLotType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_TEST_SPEC_TYPE:
				return validateAcknowledgeMaterialTestSpecType((AcknowledgeMaterialTestSpecType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_CAPABILITY_INFORMATION_TYPE:
				return validateAcknowledgeOperationsCapabilityInformationType((AcknowledgeOperationsCapabilityInformationType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_CAPABILITY_TYPE:
				return validateAcknowledgeOperationsCapabilityType((AcknowledgeOperationsCapabilityType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_DEFINITION_INFORMATION_TYPE:
				return validateAcknowledgeOperationsDefinitionInformationType((AcknowledgeOperationsDefinitionInformationType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_DEFINITION_TYPE:
				return validateAcknowledgeOperationsDefinitionType((AcknowledgeOperationsDefinitionType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_PERFORMANCE_TYPE:
				return validateAcknowledgeOperationsPerformanceType((AcknowledgeOperationsPerformanceType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_SCHEDULE_TYPE:
				return validateAcknowledgeOperationsScheduleType((AcknowledgeOperationsScheduleType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_PERSONNEL_CLASS_TYPE:
				return validateAcknowledgePersonnelClassType((AcknowledgePersonnelClassType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_PERSONNEL_INFORMATION_TYPE:
				return validateAcknowledgePersonnelInformationType((AcknowledgePersonnelInformationType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_PERSON_TYPE:
				return validateAcknowledgePersonType((AcknowledgePersonType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE:
				return validateAcknowledgePhysicalAssetCapabilityTestSpecType((AcknowledgePhysicalAssetCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_PHYSICAL_ASSET_CLASS_TYPE:
				return validateAcknowledgePhysicalAssetClassType((AcknowledgePhysicalAssetClassType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_PHYSICAL_ASSET_INFORMATION_TYPE:
				return validateAcknowledgePhysicalAssetInformationType((AcknowledgePhysicalAssetInformationType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_PHYSICAL_ASSET_TYPE:
				return validateAcknowledgePhysicalAssetType((AcknowledgePhysicalAssetType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_PROCESS_SEGMENT_INFORMATION_TYPE:
				return validateAcknowledgeProcessSegmentInformationType((AcknowledgeProcessSegmentInformationType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_PROCESS_SEGMENT_TYPE:
				return validateAcknowledgeProcessSegmentType((AcknowledgeProcessSegmentType)value, diagnostics, context);
			case B2MMLPackage.ACKNOWLEDGE_QUALIFICATION_TEST_SPECIFICATION_TYPE:
				return validateAcknowledgeQualificationTestSpecificationType((AcknowledgeQualificationTestSpecificationType)value, diagnostics, context);
			case B2MMLPackage.ACTUAL_END_TIME_TYPE:
				return validateActualEndTimeType((ActualEndTimeType)value, diagnostics, context);
			case B2MMLPackage.ACTUAL_FINISH_TIME_TYPE:
				return validateActualFinishTimeType((ActualFinishTimeType)value, diagnostics, context);
			case B2MMLPackage.ACTUAL_START_TIME_TYPE:
				return validateActualStartTimeType((ActualStartTimeType)value, diagnostics, context);
			case B2MMLPackage.AMOUNT_TYPE:
				return validateAmountType((AmountType)value, diagnostics, context);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE:
				return validateAnyGenericValueType((AnyGenericValueType)value, diagnostics, context);
			case B2MMLPackage.ASSEMBLY_RELATIONSHIP1_TYPE:
				return validateAssemblyRelationship1Type((AssemblyRelationship1Type)value, diagnostics, context);
			case B2MMLPackage.ASSEMBLY_RELATIONSHIP_TYPE:
				return validateAssemblyRelationshipType((AssemblyRelationshipType)value, diagnostics, context);
			case B2MMLPackage.ASSEMBLY_TYPE1_TYPE:
				return validateAssemblyType1Type((AssemblyType1Type)value, diagnostics, context);
			case B2MMLPackage.ASSEMBLY_TYPE_TYPE:
				return validateAssemblyTypeType((AssemblyTypeType)value, diagnostics, context);
			case B2MMLPackage.BILL_OF_MATERIAL_ID_TYPE:
				return validateBillOfMaterialIDType((BillOfMaterialIDType)value, diagnostics, context);
			case B2MMLPackage.BILL_OF_MATERIALS_ID_TYPE:
				return validateBillOfMaterialsIDType((BillOfMaterialsIDType)value, diagnostics, context);
			case B2MMLPackage.BILL_OF_RESOURCES_ID_TYPE:
				return validateBillOfResourcesIDType((BillOfResourcesIDType)value, diagnostics, context);
			case B2MMLPackage.BINARY_OBJECT_TYPE:
				return validateBinaryObjectType((BinaryObjectType)value, diagnostics, context);
			case B2MMLPackage.BOD_TYPE:
				return validateBODType((BODType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE:
				return validateCancelEquipmentCapabilityTestSpecType((CancelEquipmentCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_EQUIPMENT_CLASS_TYPE:
				return validateCancelEquipmentClassType((CancelEquipmentClassType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_EQUIPMENT_INFORMATION_TYPE:
				return validateCancelEquipmentInformationType((CancelEquipmentInformationType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_EQUIPMENT_TYPE:
				return validateCancelEquipmentType((CancelEquipmentType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_MATERIAL_CLASS_TYPE:
				return validateCancelMaterialClassType((CancelMaterialClassType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_MATERIAL_DEFINITION_TYPE:
				return validateCancelMaterialDefinitionType((CancelMaterialDefinitionType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_MATERIAL_INFORMATION_TYPE:
				return validateCancelMaterialInformationType((CancelMaterialInformationType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_MATERIAL_LOT_TYPE:
				return validateCancelMaterialLotType((CancelMaterialLotType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_MATERIAL_SUB_LOT_TYPE:
				return validateCancelMaterialSubLotType((CancelMaterialSubLotType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_MATERIAL_TEST_SPEC_TYPE:
				return validateCancelMaterialTestSpecType((CancelMaterialTestSpecType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_OPERATIONS_CAPABILITY_INFORMATION_TYPE:
				return validateCancelOperationsCapabilityInformationType((CancelOperationsCapabilityInformationType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_OPERATIONS_CAPABILITY_TYPE:
				return validateCancelOperationsCapabilityType((CancelOperationsCapabilityType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_OPERATIONS_DEFINITION_INFORMATION_TYPE:
				return validateCancelOperationsDefinitionInformationType((CancelOperationsDefinitionInformationType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_OPERATIONS_DEFINITION_TYPE:
				return validateCancelOperationsDefinitionType((CancelOperationsDefinitionType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_OPERATIONS_PERFORMANCE_TYPE:
				return validateCancelOperationsPerformanceType((CancelOperationsPerformanceType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_OPERATIONS_SCHEDULE_TYPE:
				return validateCancelOperationsScheduleType((CancelOperationsScheduleType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_PERSONNEL_CLASS_TYPE:
				return validateCancelPersonnelClassType((CancelPersonnelClassType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE:
				return validateCancelPersonnelInformationType((CancelPersonnelInformationType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_PERSON_TYPE:
				return validateCancelPersonType((CancelPersonType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE:
				return validateCancelPhysicalAssetCapabilityTestSpecType((CancelPhysicalAssetCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_PHYSICAL_ASSET_CLASS_TYPE:
				return validateCancelPhysicalAssetClassType((CancelPhysicalAssetClassType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_PHYSICAL_ASSET_INFORMATION_TYPE:
				return validateCancelPhysicalAssetInformationType((CancelPhysicalAssetInformationType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_PHYSICAL_ASSET_TYPE:
				return validateCancelPhysicalAssetType((CancelPhysicalAssetType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_PROCESS_SEGMENT_INFORMATION_TYPE:
				return validateCancelProcessSegmentInformationType((CancelProcessSegmentInformationType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_PROCESS_SEGMENT_TYPE:
				return validateCancelProcessSegmentType((CancelProcessSegmentType)value, diagnostics, context);
			case B2MMLPackage.CANCEL_QUALIFICATION_TEST_SPECIFICATION_TYPE:
				return validateCancelQualificationTestSpecificationType((CancelQualificationTestSpecificationType)value, diagnostics, context);
			case B2MMLPackage.CAPABILITY_TYPE1_TYPE:
				return validateCapabilityType1Type((CapabilityType1Type)value, diagnostics, context);
			case B2MMLPackage.CAPABILITY_TYPE_TYPE:
				return validateCapabilityTypeType((CapabilityTypeType)value, diagnostics, context);
			case B2MMLPackage.CAUSE_TYPE:
				return validateCauseType((CauseType)value, diagnostics, context);
			case B2MMLPackage.CERTIFICATE_OF_ANALYSIS_REFERENCE_TYPE:
				return validateCertificateOfAnalysisReferenceType((CertificateOfAnalysisReferenceType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE:
				return validateChangeEquipmentCapabilityTestSpecType((ChangeEquipmentCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_EQUIPMENT_CLASS_TYPE:
				return validateChangeEquipmentClassType((ChangeEquipmentClassType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_EQUIPMENT_INFORMATION_TYPE:
				return validateChangeEquipmentInformationType((ChangeEquipmentInformationType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_EQUIPMENT_TYPE:
				return validateChangeEquipmentType((ChangeEquipmentType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_MATERIAL_CLASS_TYPE:
				return validateChangeMaterialClassType((ChangeMaterialClassType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_MATERIAL_DEFINITION_TYPE:
				return validateChangeMaterialDefinitionType((ChangeMaterialDefinitionType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_MATERIAL_INFORMATION_TYPE:
				return validateChangeMaterialInformationType((ChangeMaterialInformationType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_MATERIAL_LOT_TYPE:
				return validateChangeMaterialLotType((ChangeMaterialLotType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_MATERIAL_SUB_LOT_TYPE:
				return validateChangeMaterialSubLotType((ChangeMaterialSubLotType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_MATERIAL_TEST_SPEC_TYPE:
				return validateChangeMaterialTestSpecType((ChangeMaterialTestSpecType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_OPERATIONS_CAPABILITY_INFORMATION_TYPE:
				return validateChangeOperationsCapabilityInformationType((ChangeOperationsCapabilityInformationType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_OPERATIONS_CAPABILITY_TYPE:
				return validateChangeOperationsCapabilityType((ChangeOperationsCapabilityType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_OPERATIONS_DEFINITION_INFORMATION_TYPE:
				return validateChangeOperationsDefinitionInformationType((ChangeOperationsDefinitionInformationType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_OPERATIONS_DEFINITION_TYPE:
				return validateChangeOperationsDefinitionType((ChangeOperationsDefinitionType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_OPERATIONS_PERFORMANCE_TYPE:
				return validateChangeOperationsPerformanceType((ChangeOperationsPerformanceType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_OPERATIONS_SCHEDULE_TYPE:
				return validateChangeOperationsScheduleType((ChangeOperationsScheduleType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_PERSONNEL_CLASS_TYPE:
				return validateChangePersonnelClassType((ChangePersonnelClassType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_PERSONNEL_INFORMATION_TYPE:
				return validateChangePersonnelInformationType((ChangePersonnelInformationType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_PERSON_TYPE:
				return validateChangePersonType((ChangePersonType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE:
				return validateChangePhysicalAssetCapabilityTestSpecType((ChangePhysicalAssetCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_PHYSICAL_ASSET_CLASS_TYPE:
				return validateChangePhysicalAssetClassType((ChangePhysicalAssetClassType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_PHYSICAL_ASSET_INFORMATION_TYPE:
				return validateChangePhysicalAssetInformationType((ChangePhysicalAssetInformationType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_PHYSICAL_ASSET_TYPE:
				return validateChangePhysicalAssetType((ChangePhysicalAssetType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_PROCESS_SEGMENT_INFORMATION_TYPE:
				return validateChangeProcessSegmentInformationType((ChangeProcessSegmentInformationType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_PROCESS_SEGMENT_TYPE:
				return validateChangeProcessSegmentType((ChangeProcessSegmentType)value, diagnostics, context);
			case B2MMLPackage.CHANGE_QUALIFICATION_TEST_SPECIFICATION_TYPE:
				return validateChangeQualificationTestSpecificationType((ChangeQualificationTestSpecificationType)value, diagnostics, context);
			case B2MMLPackage.CODE_TYPE:
				return validateCodeType((CodeType)value, diagnostics, context);
			case B2MMLPackage.CONFIDENCE_FACTOR_TYPE:
				return validateConfidenceFactorType((ConfidenceFactorType)value, diagnostics, context);
			case B2MMLPackage.CONFIRM_BOD_TYPE:
				return validateConfirmBODType((ConfirmBODType)value, diagnostics, context);
			case B2MMLPackage.CORRECTION_TYPE:
				return validateCorrectionType((CorrectionType)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE:
				return validateDataAreaType((DataAreaType)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE1:
				return validateDataAreaType1((DataAreaType1)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE2:
				return validateDataAreaType2((DataAreaType2)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE3:
				return validateDataAreaType3((DataAreaType3)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE4:
				return validateDataAreaType4((DataAreaType4)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE5:
				return validateDataAreaType5((DataAreaType5)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE6:
				return validateDataAreaType6((DataAreaType6)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE7:
				return validateDataAreaType7((DataAreaType7)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE8:
				return validateDataAreaType8((DataAreaType8)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE9:
				return validateDataAreaType9((DataAreaType9)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE10:
				return validateDataAreaType10((DataAreaType10)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE11:
				return validateDataAreaType11((DataAreaType11)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE12:
				return validateDataAreaType12((DataAreaType12)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE13:
				return validateDataAreaType13((DataAreaType13)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE14:
				return validateDataAreaType14((DataAreaType14)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE15:
				return validateDataAreaType15((DataAreaType15)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE16:
				return validateDataAreaType16((DataAreaType16)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE17:
				return validateDataAreaType17((DataAreaType17)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE18:
				return validateDataAreaType18((DataAreaType18)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE19:
				return validateDataAreaType19((DataAreaType19)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE20:
				return validateDataAreaType20((DataAreaType20)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE21:
				return validateDataAreaType21((DataAreaType21)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE22:
				return validateDataAreaType22((DataAreaType22)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE23:
				return validateDataAreaType23((DataAreaType23)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE24:
				return validateDataAreaType24((DataAreaType24)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE25:
				return validateDataAreaType25((DataAreaType25)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE26:
				return validateDataAreaType26((DataAreaType26)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE27:
				return validateDataAreaType27((DataAreaType27)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE28:
				return validateDataAreaType28((DataAreaType28)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE29:
				return validateDataAreaType29((DataAreaType29)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE30:
				return validateDataAreaType30((DataAreaType30)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE31:
				return validateDataAreaType31((DataAreaType31)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE32:
				return validateDataAreaType32((DataAreaType32)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE33:
				return validateDataAreaType33((DataAreaType33)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE34:
				return validateDataAreaType34((DataAreaType34)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE35:
				return validateDataAreaType35((DataAreaType35)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE36:
				return validateDataAreaType36((DataAreaType36)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE37:
				return validateDataAreaType37((DataAreaType37)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE38:
				return validateDataAreaType38((DataAreaType38)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE39:
				return validateDataAreaType39((DataAreaType39)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE40:
				return validateDataAreaType40((DataAreaType40)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE41:
				return validateDataAreaType41((DataAreaType41)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE42:
				return validateDataAreaType42((DataAreaType42)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE43:
				return validateDataAreaType43((DataAreaType43)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE44:
				return validateDataAreaType44((DataAreaType44)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE45:
				return validateDataAreaType45((DataAreaType45)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE46:
				return validateDataAreaType46((DataAreaType46)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE47:
				return validateDataAreaType47((DataAreaType47)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE48:
				return validateDataAreaType48((DataAreaType48)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE49:
				return validateDataAreaType49((DataAreaType49)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE50:
				return validateDataAreaType50((DataAreaType50)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE51:
				return validateDataAreaType51((DataAreaType51)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE52:
				return validateDataAreaType52((DataAreaType52)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE53:
				return validateDataAreaType53((DataAreaType53)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE54:
				return validateDataAreaType54((DataAreaType54)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE55:
				return validateDataAreaType55((DataAreaType55)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE56:
				return validateDataAreaType56((DataAreaType56)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE57:
				return validateDataAreaType57((DataAreaType57)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE58:
				return validateDataAreaType58((DataAreaType58)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE59:
				return validateDataAreaType59((DataAreaType59)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE60:
				return validateDataAreaType60((DataAreaType60)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE61:
				return validateDataAreaType61((DataAreaType61)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE62:
				return validateDataAreaType62((DataAreaType62)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE63:
				return validateDataAreaType63((DataAreaType63)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE64:
				return validateDataAreaType64((DataAreaType64)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE65:
				return validateDataAreaType65((DataAreaType65)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE66:
				return validateDataAreaType66((DataAreaType66)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE67:
				return validateDataAreaType67((DataAreaType67)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE68:
				return validateDataAreaType68((DataAreaType68)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE69:
				return validateDataAreaType69((DataAreaType69)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE70:
				return validateDataAreaType70((DataAreaType70)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE71:
				return validateDataAreaType71((DataAreaType71)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE72:
				return validateDataAreaType72((DataAreaType72)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE73:
				return validateDataAreaType73((DataAreaType73)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE74:
				return validateDataAreaType74((DataAreaType74)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE75:
				return validateDataAreaType75((DataAreaType75)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE76:
				return validateDataAreaType76((DataAreaType76)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE77:
				return validateDataAreaType77((DataAreaType77)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE78:
				return validateDataAreaType78((DataAreaType78)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE79:
				return validateDataAreaType79((DataAreaType79)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE80:
				return validateDataAreaType80((DataAreaType80)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE81:
				return validateDataAreaType81((DataAreaType81)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE82:
				return validateDataAreaType82((DataAreaType82)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE83:
				return validateDataAreaType83((DataAreaType83)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE84:
				return validateDataAreaType84((DataAreaType84)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE85:
				return validateDataAreaType85((DataAreaType85)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE86:
				return validateDataAreaType86((DataAreaType86)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE87:
				return validateDataAreaType87((DataAreaType87)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE88:
				return validateDataAreaType88((DataAreaType88)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE89:
				return validateDataAreaType89((DataAreaType89)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE90:
				return validateDataAreaType90((DataAreaType90)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE91:
				return validateDataAreaType91((DataAreaType91)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE92:
				return validateDataAreaType92((DataAreaType92)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE93:
				return validateDataAreaType93((DataAreaType93)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE94:
				return validateDataAreaType94((DataAreaType94)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE95:
				return validateDataAreaType95((DataAreaType95)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE96:
				return validateDataAreaType96((DataAreaType96)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE97:
				return validateDataAreaType97((DataAreaType97)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE98:
				return validateDataAreaType98((DataAreaType98)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE99:
				return validateDataAreaType99((DataAreaType99)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE100:
				return validateDataAreaType100((DataAreaType100)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE101:
				return validateDataAreaType101((DataAreaType101)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE102:
				return validateDataAreaType102((DataAreaType102)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE103:
				return validateDataAreaType103((DataAreaType103)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE104:
				return validateDataAreaType104((DataAreaType104)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE105:
				return validateDataAreaType105((DataAreaType105)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE106:
				return validateDataAreaType106((DataAreaType106)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE107:
				return validateDataAreaType107((DataAreaType107)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE108:
				return validateDataAreaType108((DataAreaType108)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE109:
				return validateDataAreaType109((DataAreaType109)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE110:
				return validateDataAreaType110((DataAreaType110)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE111:
				return validateDataAreaType111((DataAreaType111)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE112:
				return validateDataAreaType112((DataAreaType112)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE113:
				return validateDataAreaType113((DataAreaType113)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE114:
				return validateDataAreaType114((DataAreaType114)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE115:
				return validateDataAreaType115((DataAreaType115)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE116:
				return validateDataAreaType116((DataAreaType116)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE117:
				return validateDataAreaType117((DataAreaType117)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE118:
				return validateDataAreaType118((DataAreaType118)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE119:
				return validateDataAreaType119((DataAreaType119)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE120:
				return validateDataAreaType120((DataAreaType120)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE121:
				return validateDataAreaType121((DataAreaType121)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE122:
				return validateDataAreaType122((DataAreaType122)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE123:
				return validateDataAreaType123((DataAreaType123)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE124:
				return validateDataAreaType124((DataAreaType124)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE125:
				return validateDataAreaType125((DataAreaType125)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE126:
				return validateDataAreaType126((DataAreaType126)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE127:
				return validateDataAreaType127((DataAreaType127)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE128:
				return validateDataAreaType128((DataAreaType128)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE129:
				return validateDataAreaType129((DataAreaType129)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE130:
				return validateDataAreaType130((DataAreaType130)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE131:
				return validateDataAreaType131((DataAreaType131)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE132:
				return validateDataAreaType132((DataAreaType132)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE133:
				return validateDataAreaType133((DataAreaType133)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE134:
				return validateDataAreaType134((DataAreaType134)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE135:
				return validateDataAreaType135((DataAreaType135)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE136:
				return validateDataAreaType136((DataAreaType136)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE137:
				return validateDataAreaType137((DataAreaType137)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE138:
				return validateDataAreaType138((DataAreaType138)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE139:
				return validateDataAreaType139((DataAreaType139)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE140:
				return validateDataAreaType140((DataAreaType140)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE141:
				return validateDataAreaType141((DataAreaType141)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE142:
				return validateDataAreaType142((DataAreaType142)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE143:
				return validateDataAreaType143((DataAreaType143)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE144:
				return validateDataAreaType144((DataAreaType144)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE145:
				return validateDataAreaType145((DataAreaType145)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE146:
				return validateDataAreaType146((DataAreaType146)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE147:
				return validateDataAreaType147((DataAreaType147)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE148:
				return validateDataAreaType148((DataAreaType148)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE149:
				return validateDataAreaType149((DataAreaType149)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE150:
				return validateDataAreaType150((DataAreaType150)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE151:
				return validateDataAreaType151((DataAreaType151)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE152:
				return validateDataAreaType152((DataAreaType152)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE153:
				return validateDataAreaType153((DataAreaType153)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE154:
				return validateDataAreaType154((DataAreaType154)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE155:
				return validateDataAreaType155((DataAreaType155)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE156:
				return validateDataAreaType156((DataAreaType156)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE157:
				return validateDataAreaType157((DataAreaType157)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE158:
				return validateDataAreaType158((DataAreaType158)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE159:
				return validateDataAreaType159((DataAreaType159)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE160:
				return validateDataAreaType160((DataAreaType160)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE161:
				return validateDataAreaType161((DataAreaType161)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE162:
				return validateDataAreaType162((DataAreaType162)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE163:
				return validateDataAreaType163((DataAreaType163)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE164:
				return validateDataAreaType164((DataAreaType164)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE165:
				return validateDataAreaType165((DataAreaType165)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE166:
				return validateDataAreaType166((DataAreaType166)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE167:
				return validateDataAreaType167((DataAreaType167)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE168:
				return validateDataAreaType168((DataAreaType168)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE169:
				return validateDataAreaType169((DataAreaType169)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE170:
				return validateDataAreaType170((DataAreaType170)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE171:
				return validateDataAreaType171((DataAreaType171)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE172:
				return validateDataAreaType172((DataAreaType172)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE173:
				return validateDataAreaType173((DataAreaType173)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE174:
				return validateDataAreaType174((DataAreaType174)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE175:
				return validateDataAreaType175((DataAreaType175)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE176:
				return validateDataAreaType176((DataAreaType176)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE177:
				return validateDataAreaType177((DataAreaType177)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE178:
				return validateDataAreaType178((DataAreaType178)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE179:
				return validateDataAreaType179((DataAreaType179)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE180:
				return validateDataAreaType180((DataAreaType180)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE181:
				return validateDataAreaType181((DataAreaType181)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE182:
				return validateDataAreaType182((DataAreaType182)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE183:
				return validateDataAreaType183((DataAreaType183)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE184:
				return validateDataAreaType184((DataAreaType184)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE185:
				return validateDataAreaType185((DataAreaType185)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE186:
				return validateDataAreaType186((DataAreaType186)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE187:
				return validateDataAreaType187((DataAreaType187)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE188:
				return validateDataAreaType188((DataAreaType188)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE189:
				return validateDataAreaType189((DataAreaType189)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE190:
				return validateDataAreaType190((DataAreaType190)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE191:
				return validateDataAreaType191((DataAreaType191)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE192:
				return validateDataAreaType192((DataAreaType192)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE193:
				return validateDataAreaType193((DataAreaType193)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE194:
				return validateDataAreaType194((DataAreaType194)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE195:
				return validateDataAreaType195((DataAreaType195)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE196:
				return validateDataAreaType196((DataAreaType196)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE197:
				return validateDataAreaType197((DataAreaType197)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE198:
				return validateDataAreaType198((DataAreaType198)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE199:
				return validateDataAreaType199((DataAreaType199)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE200:
				return validateDataAreaType200((DataAreaType200)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE201:
				return validateDataAreaType201((DataAreaType201)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE202:
				return validateDataAreaType202((DataAreaType202)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE203:
				return validateDataAreaType203((DataAreaType203)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE204:
				return validateDataAreaType204((DataAreaType204)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE205:
				return validateDataAreaType205((DataAreaType205)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE206:
				return validateDataAreaType206((DataAreaType206)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE207:
				return validateDataAreaType207((DataAreaType207)value, diagnostics, context);
			case B2MMLPackage.DATA_AREA_TYPE208:
				return validateDataAreaType208((DataAreaType208)value, diagnostics, context);
			case B2MMLPackage.DATA_TYPE1_TYPE:
				return validateDataType1Type((DataType1Type)value, diagnostics, context);
			case B2MMLPackage.DATA_TYPE_TYPE:
				return validateDataTypeType((DataTypeType)value, diagnostics, context);
			case B2MMLPackage.DATE_TIME_TYPE:
				return validateDateTimeType((DateTimeType)value, diagnostics, context);
			case B2MMLPackage.DEPENDENCY1_TYPE:
				return validateDependency1Type((Dependency1Type)value, diagnostics, context);
			case B2MMLPackage.DEPENDENCY_TYPE:
				return validateDependencyType((DependencyType)value, diagnostics, context);
			case B2MMLPackage.DESCRIPTION_TYPE:
				return validateDescriptionType((DescriptionType)value, diagnostics, context);
			case B2MMLPackage.DOCUMENT_ROOT:
				return validateDocumentRoot((DocumentRoot)value, diagnostics, context);
			case B2MMLPackage.EARLIEST_START_TIME_TYPE:
				return validateEarliestStartTimeType((EarliestStartTimeType)value, diagnostics, context);
			case B2MMLPackage.END_TIME_TYPE:
				return validateEndTimeType((EndTimeType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE:
				return validateEquipmentAssetMappingType((EquipmentAssetMappingType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID_TYPE:
				return validateEquipmentCapabilityTestSpecificationIDType((EquipmentCapabilityTestSpecificationIDType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE:
				return validateEquipmentCapabilityTestSpecificationType((EquipmentCapabilityTestSpecificationType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_CLASS_ID_TYPE:
				return validateEquipmentClassIDType((EquipmentClassIDType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE:
				return validateEquipmentClassPropertyType((EquipmentClassPropertyType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_CLASS_TYPE:
				return validateEquipmentClassType((EquipmentClassType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_ELEMENT_LEVEL1_TYPE:
				return validateEquipmentElementLevel1Type((EquipmentElementLevel1Type)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_ELEMENT_LEVEL_TYPE:
				return validateEquipmentElementLevelType((EquipmentElementLevelType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_ID_TYPE:
				return validateEquipmentIDType((EquipmentIDType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_INFORMATION_TYPE:
				return validateEquipmentInformationType((EquipmentInformationType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_PROPERTY_TYPE:
				return validateEquipmentPropertyType((EquipmentPropertyType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY_TYPE:
				return validateEquipmentSegmentSpecificationPropertyType((EquipmentSegmentSpecificationPropertyType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE:
				return validateEquipmentSegmentSpecificationType((EquipmentSegmentSpecificationType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_TYPE:
				return validateEquipmentType((EquipmentType)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_USE_TYPE:
				return validateEquipmentUseType((EquipmentUseType)value, diagnostics, context);
			case B2MMLPackage.EXPIRATION_TIME_TYPE:
				return validateExpirationTimeType((ExpirationTimeType)value, diagnostics, context);
			case B2MMLPackage.GET_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE:
				return validateGetEquipmentCapabilityTestSpecType((GetEquipmentCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.GET_EQUIPMENT_CLASS_TYPE:
				return validateGetEquipmentClassType((GetEquipmentClassType)value, diagnostics, context);
			case B2MMLPackage.GET_EQUIPMENT_INFORMATION_TYPE:
				return validateGetEquipmentInformationType((GetEquipmentInformationType)value, diagnostics, context);
			case B2MMLPackage.GET_EQUIPMENT_TYPE:
				return validateGetEquipmentType((GetEquipmentType)value, diagnostics, context);
			case B2MMLPackage.GET_MATERIAL_CLASS_TYPE:
				return validateGetMaterialClassType((GetMaterialClassType)value, diagnostics, context);
			case B2MMLPackage.GET_MATERIAL_DEFINITION_TYPE:
				return validateGetMaterialDefinitionType((GetMaterialDefinitionType)value, diagnostics, context);
			case B2MMLPackage.GET_MATERIAL_INFORMATION_TYPE:
				return validateGetMaterialInformationType((GetMaterialInformationType)value, diagnostics, context);
			case B2MMLPackage.GET_MATERIAL_LOT_TYPE:
				return validateGetMaterialLotType((GetMaterialLotType)value, diagnostics, context);
			case B2MMLPackage.GET_MATERIAL_SUB_LOT_TYPE:
				return validateGetMaterialSubLotType((GetMaterialSubLotType)value, diagnostics, context);
			case B2MMLPackage.GET_MATERIAL_TEST_SPEC_TYPE:
				return validateGetMaterialTestSpecType((GetMaterialTestSpecType)value, diagnostics, context);
			case B2MMLPackage.GET_OPERATIONS_CAPABILITY_INFORMATION_TYPE:
				return validateGetOperationsCapabilityInformationType((GetOperationsCapabilityInformationType)value, diagnostics, context);
			case B2MMLPackage.GET_OPERATIONS_CAPABILITY_TYPE:
				return validateGetOperationsCapabilityType((GetOperationsCapabilityType)value, diagnostics, context);
			case B2MMLPackage.GET_OPERATIONS_DEFINITION_INFORMATION_TYPE:
				return validateGetOperationsDefinitionInformationType((GetOperationsDefinitionInformationType)value, diagnostics, context);
			case B2MMLPackage.GET_OPERATIONS_DEFINITION_TYPE:
				return validateGetOperationsDefinitionType((GetOperationsDefinitionType)value, diagnostics, context);
			case B2MMLPackage.GET_OPERATIONS_PERFORMANCE_TYPE:
				return validateGetOperationsPerformanceType((GetOperationsPerformanceType)value, diagnostics, context);
			case B2MMLPackage.GET_OPERATIONS_SCHEDULE_TYPE:
				return validateGetOperationsScheduleType((GetOperationsScheduleType)value, diagnostics, context);
			case B2MMLPackage.GET_PERSONNEL_CLASS_TYPE:
				return validateGetPersonnelClassType((GetPersonnelClassType)value, diagnostics, context);
			case B2MMLPackage.GET_PERSONNEL_INFORMATION_TYPE:
				return validateGetPersonnelInformationType((GetPersonnelInformationType)value, diagnostics, context);
			case B2MMLPackage.GET_PERSON_TYPE:
				return validateGetPersonType((GetPersonType)value, diagnostics, context);
			case B2MMLPackage.GET_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE:
				return validateGetPhysicalAssetCapabilityTestSpecType((GetPhysicalAssetCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.GET_PHYSICAL_ASSET_CLASS_TYPE:
				return validateGetPhysicalAssetClassType((GetPhysicalAssetClassType)value, diagnostics, context);
			case B2MMLPackage.GET_PHYSICAL_ASSET_INFORMATION_TYPE:
				return validateGetPhysicalAssetInformationType((GetPhysicalAssetInformationType)value, diagnostics, context);
			case B2MMLPackage.GET_PHYSICAL_ASSET_TYPE:
				return validateGetPhysicalAssetType((GetPhysicalAssetType)value, diagnostics, context);
			case B2MMLPackage.GET_PROCESS_SEGMENT_INFORMATION_TYPE:
				return validateGetProcessSegmentInformationType((GetProcessSegmentInformationType)value, diagnostics, context);
			case B2MMLPackage.GET_PROCESS_SEGMENT_TYPE:
				return validateGetProcessSegmentType((GetProcessSegmentType)value, diagnostics, context);
			case B2MMLPackage.GET_QUALIFICATION_TEST_SPECIFICATION_TYPE:
				return validateGetQualificationTestSpecificationType((GetQualificationTestSpecificationType)value, diagnostics, context);
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE:
				return validateHierarchyScopeType((HierarchyScopeType)value, diagnostics, context);
			case B2MMLPackage.IDENTIFIER_TYPE:
				return validateIdentifierType((IdentifierType)value, diagnostics, context);
			case B2MMLPackage.JOB_ORDER_COMMAND1_TYPE:
				return validateJobOrderCommand1Type((JobOrderCommand1Type)value, diagnostics, context);
			case B2MMLPackage.JOB_ORDER_COMMAND_RULE_TYPE:
				return validateJobOrderCommandRuleType((JobOrderCommandRuleType)value, diagnostics, context);
			case B2MMLPackage.JOB_ORDER_COMMAND_TYPE:
				return validateJobOrderCommandType((JobOrderCommandType)value, diagnostics, context);
			case B2MMLPackage.JOB_ORDER_DISPATCH_STATUS_TYPE:
				return validateJobOrderDispatchStatusType((JobOrderDispatchStatusType)value, diagnostics, context);
			case B2MMLPackage.LATEST_END_TIME_TYPE:
				return validateLatestEndTimeType((LatestEndTimeType)value, diagnostics, context);
			case B2MMLPackage.LOCATION_TYPE:
				return validateLocationType((LocationType)value, diagnostics, context);
			case B2MMLPackage.MANUFACTURING_BILL_ID_TYPE:
				return validateManufacturingBillIDType((ManufacturingBillIDType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_ACTUAL_ID_TYPE:
				return validateMaterialActualIDType((MaterialActualIDType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_CAPABILITY_ID_TYPE:
				return validateMaterialCapabilityIDType((MaterialCapabilityIDType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_CLASS_ID_TYPE:
				return validateMaterialClassIDType((MaterialClassIDType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_CLASS_PROPERTY_TYPE:
				return validateMaterialClassPropertyType((MaterialClassPropertyType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_CLASS_TYPE:
				return validateMaterialClassType((MaterialClassType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_DEFINITION_ID_TYPE:
				return validateMaterialDefinitionIDType((MaterialDefinitionIDType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_DEFINITION_PROPERTY_TYPE:
				return validateMaterialDefinitionPropertyType((MaterialDefinitionPropertyType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_DEFINITION_TYPE:
				return validateMaterialDefinitionType((MaterialDefinitionType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE:
				return validateMaterialInformationType((MaterialInformationType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_LOT_ID_TYPE:
				return validateMaterialLotIDType((MaterialLotIDType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_LOT_PROPERTY_TYPE:
				return validateMaterialLotPropertyType((MaterialLotPropertyType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_LOT_TYPE:
				return validateMaterialLotType((MaterialLotType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_REQUIREMENT_ID_TYPE:
				return validateMaterialRequirementIDType((MaterialRequirementIDType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_PROPERTY_TYPE:
				return validateMaterialSegmentSpecificationPropertyType((MaterialSegmentSpecificationPropertyType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE:
				return validateMaterialSegmentSpecificationType((MaterialSegmentSpecificationType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_SPECIFICATION_ID_TYPE:
				return validateMaterialSpecificationIDType((MaterialSpecificationIDType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_SUB_LOT_ID_TYPE:
				return validateMaterialSubLotIDType((MaterialSubLotIDType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE:
				return validateMaterialSubLotType((MaterialSubLotType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_TEST_SPECIFICATION_ID_TYPE:
				return validateMaterialTestSpecificationIDType((MaterialTestSpecificationIDType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_TEST_SPECIFICATION_TYPE:
				return validateMaterialTestSpecificationType((MaterialTestSpecificationType)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_USE1_TYPE:
				return validateMaterialUse1Type((MaterialUse1Type)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_USE_TYPE:
				return validateMaterialUseType((MaterialUseType)value, diagnostics, context);
			case B2MMLPackage.MEASURE_TYPE:
				return validateMeasureType((MeasureType)value, diagnostics, context);
			case B2MMLPackage.NAME_TYPE:
				return validateNameType((NameType)value, diagnostics, context);
			case B2MMLPackage.NUMERIC_TYPE:
				return validateNumericType((NumericType)value, diagnostics, context);
			case B2MMLPackage.OP_EQUIPMENT_ACTUAL_PROPERTY_TYPE:
				return validateOpEquipmentActualPropertyType((OpEquipmentActualPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_EQUIPMENT_ACTUAL_TYPE:
				return validateOpEquipmentActualType((OpEquipmentActualType)value, diagnostics, context);
			case B2MMLPackage.OP_EQUIPMENT_CAPABILITY_PROPERTY_TYPE:
				return validateOpEquipmentCapabilityPropertyType((OpEquipmentCapabilityPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_EQUIPMENT_CAPABILITY_TYPE:
				return validateOpEquipmentCapabilityType((OpEquipmentCapabilityType)value, diagnostics, context);
			case B2MMLPackage.OP_EQUIPMENT_REQUIREMENT_PROPERTY_TYPE:
				return validateOpEquipmentRequirementPropertyType((OpEquipmentRequirementPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_EQUIPMENT_REQUIREMENT_TYPE:
				return validateOpEquipmentRequirementType((OpEquipmentRequirementType)value, diagnostics, context);
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_PROPERTY_TYPE:
				return validateOpEquipmentSpecificationPropertyType((OpEquipmentSpecificationPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE:
				return validateOpEquipmentSpecificationType((OpEquipmentSpecificationType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_CAPABILITY_INFORMATION_TYPE:
				return validateOperationsCapabilityInformationType((OperationsCapabilityInformationType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE:
				return validateOperationsCapabilityType((OperationsCapabilityType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_DEFINITION_ID_TYPE:
				return validateOperationsDefinitionIDType((OperationsDefinitionIDType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE:
				return validateOperationsDefinitionInformationType((OperationsDefinitionInformationType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE:
				return validateOperationsDefinitionType((OperationsDefinitionType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE:
				return validateOperationsMaterialBillItemType((OperationsMaterialBillItemType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_TYPE:
				return validateOperationsMaterialBillType((OperationsMaterialBillType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_PERFORMANCE_TYPE:
				return validateOperationsPerformanceType((OperationsPerformanceType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_REQUEST_ID_TYPE:
				return validateOperationsRequestIDType((OperationsRequestIDType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_REQUEST_TYPE:
				return validateOperationsRequestType((OperationsRequestType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE:
				return validateOperationsResponseType((OperationsResponseType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_SCHEDULE_ID_TYPE:
				return validateOperationsScheduleIDType((OperationsScheduleIDType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE:
				return validateOperationsScheduleType((OperationsScheduleType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_SEGMENT_ID_TYPE:
				return validateOperationsSegmentIDType((OperationsSegmentIDType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE:
				return validateOperationsSegmentType((OperationsSegmentType)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_TYPE1_TYPE:
				return validateOperationsType1Type((OperationsType1Type)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_TYPE_TYPE:
				return validateOperationsTypeType((OperationsTypeType)value, diagnostics, context);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE:
				return validateOpMaterialActualPropertyType((OpMaterialActualPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE:
				return validateOpMaterialActualType((OpMaterialActualType)value, diagnostics, context);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_PROPERTY_TYPE:
				return validateOpMaterialCapabilityPropertyType((OpMaterialCapabilityPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE:
				return validateOpMaterialCapabilityType((OpMaterialCapabilityType)value, diagnostics, context);
			case B2MMLPackage.OP_MATERIAL_REQUIREMENT_PROPERTY_TYPE:
				return validateOpMaterialRequirementPropertyType((OpMaterialRequirementPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_MATERIAL_REQUIREMENT_TYPE:
				return validateOpMaterialRequirementType((OpMaterialRequirementType)value, diagnostics, context);
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_PROPERTY_TYPE:
				return validateOpMaterialSpecificationPropertyType((OpMaterialSpecificationPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE:
				return validateOpMaterialSpecificationType((OpMaterialSpecificationType)value, diagnostics, context);
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_PROPERTY_TYPE:
				return validateOpPersonnelActualPropertyType((OpPersonnelActualPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE:
				return validateOpPersonnelActualType((OpPersonnelActualType)value, diagnostics, context);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_PROPERTY_TYPE:
				return validateOpPersonnelCapabilityPropertyType((OpPersonnelCapabilityPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE:
				return validateOpPersonnelCapabilityType((OpPersonnelCapabilityType)value, diagnostics, context);
			case B2MMLPackage.OP_PERSONNEL_REQUIREMENT_PROPERTY_TYPE:
				return validateOpPersonnelRequirementPropertyType((OpPersonnelRequirementPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_PERSONNEL_REQUIREMENT_TYPE:
				return validateOpPersonnelRequirementType((OpPersonnelRequirementType)value, diagnostics, context);
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_PROPERTY_TYPE:
				return validateOpPersonnelSpecificationPropertyType((OpPersonnelSpecificationPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE:
				return validateOpPersonnelSpecificationType((OpPersonnelSpecificationType)value, diagnostics, context);
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_PROPERTY_TYPE:
				return validateOpPhysicalAssetActualPropertyType((OpPhysicalAssetActualPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE:
				return validateOpPhysicalAssetActualType((OpPhysicalAssetActualType)value, diagnostics, context);
			case B2MMLPackage.OP_PHYSICAL_ASSET_CAPABILITY_PROPERTY_TYPE:
				return validateOpPhysicalAssetCapabilityPropertyType((OpPhysicalAssetCapabilityPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_PHYSICAL_ASSET_CAPABILITY_TYPE:
				return validateOpPhysicalAssetCapabilityType((OpPhysicalAssetCapabilityType)value, diagnostics, context);
			case B2MMLPackage.OP_PHYSICAL_ASSET_REQUIREMENT_PROPERTY_TYPE:
				return validateOpPhysicalAssetRequirementPropertyType((OpPhysicalAssetRequirementPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_PHYSICAL_ASSET_REQUIREMENT_TYPE:
				return validateOpPhysicalAssetRequirementType((OpPhysicalAssetRequirementType)value, diagnostics, context);
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_PROPERTY_TYPE:
				return validateOpPhysicalAssetSpecificationPropertyType((OpPhysicalAssetSpecificationPropertyType)value, diagnostics, context);
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE:
				return validateOpPhysicalAssetSpecificationType((OpPhysicalAssetSpecificationType)value, diagnostics, context);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE:
				return validateOpProcessSegmentCapabilityType((OpProcessSegmentCapabilityType)value, diagnostics, context);
			case B2MMLPackage.OP_SEGMENT_DATA_TYPE:
				return validateOpSegmentDataType((OpSegmentDataType)value, diagnostics, context);
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE:
				return validateOpSegmentRequirementType((OpSegmentRequirementType)value, diagnostics, context);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE:
				return validateOpSegmentResponseType((OpSegmentResponseType)value, diagnostics, context);
			case B2MMLPackage.OTHER_DEPENDENCY_TYPE:
				return validateOtherDependencyType((OtherDependencyType)value, diagnostics, context);
			case B2MMLPackage.PARAMETER_ID_TYPE:
				return validateParameterIDType((ParameterIDType)value, diagnostics, context);
			case B2MMLPackage.PARAMETER_TYPE:
				return validateParameterType((ParameterType)value, diagnostics, context);
			case B2MMLPackage.PERSON_ID_TYPE:
				return validatePersonIDType((PersonIDType)value, diagnostics, context);
			case B2MMLPackage.PERSON_NAME_TYPE:
				return validatePersonNameType((PersonNameType)value, diagnostics, context);
			case B2MMLPackage.PERSONNEL_CLASS_ID_TYPE:
				return validatePersonnelClassIDType((PersonnelClassIDType)value, diagnostics, context);
			case B2MMLPackage.PERSONNEL_CLASS_PROPERTY_TYPE:
				return validatePersonnelClassPropertyType((PersonnelClassPropertyType)value, diagnostics, context);
			case B2MMLPackage.PERSONNEL_CLASS_TYPE:
				return validatePersonnelClassType((PersonnelClassType)value, diagnostics, context);
			case B2MMLPackage.PERSONNEL_INFORMATION_TYPE:
				return validatePersonnelInformationType((PersonnelInformationType)value, diagnostics, context);
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY_TYPE:
				return validatePersonnelSegmentSpecificationPropertyType((PersonnelSegmentSpecificationPropertyType)value, diagnostics, context);
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE:
				return validatePersonnelSegmentSpecificationType((PersonnelSegmentSpecificationType)value, diagnostics, context);
			case B2MMLPackage.PERSONNEL_USE_TYPE:
				return validatePersonnelUseType((PersonnelUseType)value, diagnostics, context);
			case B2MMLPackage.PERSON_PROPERTY_TYPE:
				return validatePersonPropertyType((PersonPropertyType)value, diagnostics, context);
			case B2MMLPackage.PERSON_TYPE:
				return validatePersonType((PersonType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_ACTUAL_ID_TYPE:
				return validatePhysicalAssetActualIDType((PhysicalAssetActualIDType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID_TYPE:
				return validatePhysicalAssetCapabilityTestSpecificationIDType((PhysicalAssetCapabilityTestSpecificationIDType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE:
				return validatePhysicalAssetCapabilityTestSpecificationType((PhysicalAssetCapabilityTestSpecificationType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_ID_TYPE:
				return validatePhysicalAssetClassIDType((PhysicalAssetClassIDType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE:
				return validatePhysicalAssetClassPropertyType((PhysicalAssetClassPropertyType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE:
				return validatePhysicalAssetClassType((PhysicalAssetClassType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_ID_TYPE:
				return validatePhysicalAssetIDType((PhysicalAssetIDType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_INFORMATION_TYPE:
				return validatePhysicalAssetInformationType((PhysicalAssetInformationType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_PROPERTY_TYPE:
				return validatePhysicalAssetPropertyType((PhysicalAssetPropertyType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY_TYPE:
				return validatePhysicalAssetSegmentSpecificationPropertyType((PhysicalAssetSegmentSpecificationPropertyType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE:
				return validatePhysicalAssetSegmentSpecificationType((PhysicalAssetSegmentSpecificationType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_TYPE:
				return validatePhysicalAssetType((PhysicalAssetType)value, diagnostics, context);
			case B2MMLPackage.PHYSICAL_ASSET_USE_TYPE:
				return validatePhysicalAssetUseType((PhysicalAssetUseType)value, diagnostics, context);
			case B2MMLPackage.PLANNED_FINISH_TIME_TYPE:
				return validatePlannedFinishTimeType((PlannedFinishTimeType)value, diagnostics, context);
			case B2MMLPackage.PLANNED_START_TIME_TYPE:
				return validatePlannedStartTimeType((PlannedStartTimeType)value, diagnostics, context);
			case B2MMLPackage.PRIORITY_TYPE:
				return validatePriorityType((PriorityType)value, diagnostics, context);
			case B2MMLPackage.PROBLEM_TYPE:
				return validateProblemType((ProblemType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE:
				return validateProcessEquipmentCapabilityTestSpecType((ProcessEquipmentCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_EQUIPMENT_CLASS_TYPE:
				return validateProcessEquipmentClassType((ProcessEquipmentClassType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_EQUIPMENT_INFORMATION_TYPE:
				return validateProcessEquipmentInformationType((ProcessEquipmentInformationType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_EQUIPMENT_TYPE:
				return validateProcessEquipmentType((ProcessEquipmentType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_MATERIAL_CLASS_TYPE:
				return validateProcessMaterialClassType((ProcessMaterialClassType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_MATERIAL_DEFINITION_TYPE:
				return validateProcessMaterialDefinitionType((ProcessMaterialDefinitionType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_MATERIAL_INFORMATION_TYPE:
				return validateProcessMaterialInformationType((ProcessMaterialInformationType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_MATERIAL_LOT_TYPE:
				return validateProcessMaterialLotType((ProcessMaterialLotType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_MATERIAL_SUB_LOT_TYPE:
				return validateProcessMaterialSubLotType((ProcessMaterialSubLotType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_MATERIAL_TEST_SPEC_TYPE:
				return validateProcessMaterialTestSpecType((ProcessMaterialTestSpecType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_OPERATIONS_CAPABILITY_INFORMATION_TYPE:
				return validateProcessOperationsCapabilityInformationType((ProcessOperationsCapabilityInformationType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_OPERATIONS_CAPABILITY_TYPE:
				return validateProcessOperationsCapabilityType((ProcessOperationsCapabilityType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_OPERATIONS_DEFINITION_INFORMATION_TYPE:
				return validateProcessOperationsDefinitionInformationType((ProcessOperationsDefinitionInformationType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_OPERATIONS_DEFINITION_TYPE:
				return validateProcessOperationsDefinitionType((ProcessOperationsDefinitionType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_OPERATIONS_PERFORMANCE_TYPE:
				return validateProcessOperationsPerformanceType((ProcessOperationsPerformanceType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_OPERATIONS_SCHEDULE_TYPE:
				return validateProcessOperationsScheduleType((ProcessOperationsScheduleType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_PERSONNEL_CLASS_TYPE:
				return validateProcessPersonnelClassType((ProcessPersonnelClassType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_PERSONNEL_INFORMATION_TYPE:
				return validateProcessPersonnelInformationType((ProcessPersonnelInformationType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_PERSON_TYPE:
				return validateProcessPersonType((ProcessPersonType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE:
				return validateProcessPhysicalAssetCapabilityTestSpecType((ProcessPhysicalAssetCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_PHYSICAL_ASSET_CLASS_TYPE:
				return validateProcessPhysicalAssetClassType((ProcessPhysicalAssetClassType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_PHYSICAL_ASSET_INFORMATION_TYPE:
				return validateProcessPhysicalAssetInformationType((ProcessPhysicalAssetInformationType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_PHYSICAL_ASSET_TYPE:
				return validateProcessPhysicalAssetType((ProcessPhysicalAssetType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_PROCESS_SEGMENT_INFORMATION_TYPE:
				return validateProcessProcessSegmentInformationType((ProcessProcessSegmentInformationType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_PROCESS_SEGMENT_TYPE:
				return validateProcessProcessSegmentType((ProcessProcessSegmentType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_QUALIFICATION_TEST_SPECIFICATION_TYPE:
				return validateProcessQualificationTestSpecificationType((ProcessQualificationTestSpecificationType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_SEGMENT_ID_TYPE:
				return validateProcessSegmentIDType((ProcessSegmentIDType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_SEGMENT_INFORMATION_TYPE:
				return validateProcessSegmentInformationType((ProcessSegmentInformationType)value, diagnostics, context);
			case B2MMLPackage.PROCESS_SEGMENT_TYPE:
				return validateProcessSegmentType((ProcessSegmentType)value, diagnostics, context);
			case B2MMLPackage.PRODUCTION_REQUEST_ID_TYPE:
				return validateProductionRequestIDType((ProductionRequestIDType)value, diagnostics, context);
			case B2MMLPackage.PRODUCTION_SCHEDULE_ID_TYPE:
				return validateProductionScheduleIDType((ProductionScheduleIDType)value, diagnostics, context);
			case B2MMLPackage.PRODUCT_PRODUCTION_RULE_ID_TYPE:
				return validateProductProductionRuleIDType((ProductProductionRuleIDType)value, diagnostics, context);
			case B2MMLPackage.PRODUCT_PRODUCTION_RULE_TYPE:
				return validateProductProductionRuleType((ProductProductionRuleType)value, diagnostics, context);
			case B2MMLPackage.PRODUCT_SEGMENT_ID_TYPE:
				return validateProductSegmentIDType((ProductSegmentIDType)value, diagnostics, context);
			case B2MMLPackage.PROPERTY_ID_TYPE:
				return validatePropertyIDType((PropertyIDType)value, diagnostics, context);
			case B2MMLPackage.PUBLISHED_DATE_TYPE:
				return validatePublishedDateType((PublishedDateType)value, diagnostics, context);
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_ID_TYPE:
				return validateQualificationTestSpecificationIDType((QualificationTestSpecificationIDType)value, diagnostics, context);
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE:
				return validateQualificationTestSpecificationType((QualificationTestSpecificationType)value, diagnostics, context);
			case B2MMLPackage.QUANTITY_STRING_TYPE:
				return validateQuantityStringType((QuantityStringType)value, diagnostics, context);
			case B2MMLPackage.QUANTITY_TYPE:
				return validateQuantityType((QuantityType)value, diagnostics, context);
			case B2MMLPackage.QUANTITY_VALUE_TYPE:
				return validateQuantityValueType((QuantityValueType)value, diagnostics, context);
			case B2MMLPackage.REASON_TYPE:
				return validateReasonType((ReasonType)value, diagnostics, context);
			case B2MMLPackage.RELATIONSHIP_FORM1_TYPE:
				return validateRelationshipForm1Type((RelationshipForm1Type)value, diagnostics, context);
			case B2MMLPackage.RELATIONSHIP_FORM_TYPE:
				return validateRelationshipFormType((RelationshipFormType)value, diagnostics, context);
			case B2MMLPackage.RELATIONSHIP_TYPE1_TYPE:
				return validateRelationshipType1Type((RelationshipType1Type)value, diagnostics, context);
			case B2MMLPackage.RELATIONSHIP_TYPE_TYPE:
				return validateRelationshipTypeType((RelationshipTypeType)value, diagnostics, context);
			case B2MMLPackage.REQUESTED_COMPLETION_DATE_TYPE:
				return validateRequestedCompletionDateType((RequestedCompletionDateType)value, diagnostics, context);
			case B2MMLPackage.REQUESTED_PRIORITY_TYPE:
				return validateRequestedPriorityType((RequestedPriorityType)value, diagnostics, context);
			case B2MMLPackage.REQUEST_STATE1_TYPE:
				return validateRequestState1Type((RequestState1Type)value, diagnostics, context);
			case B2MMLPackage.REQUEST_STATE_TYPE:
				return validateRequestStateType((RequestStateType)value, diagnostics, context);
			case B2MMLPackage.REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE1_TYPE:
				return validateRequiredByRequestedSegmentResponse1Type((RequiredByRequestedSegmentResponse1Type)value, diagnostics, context);
			case B2MMLPackage.REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE_TYPE:
				return validateRequiredByRequestedSegmentResponseType((RequiredByRequestedSegmentResponseType)value, diagnostics, context);
			case B2MMLPackage.RESOURCE_ID_TYPE:
				return validateResourceIDType((ResourceIDType)value, diagnostics, context);
			case B2MMLPackage.RESOURCE_NETWORK_CONNECTION_ID_TYPE:
				return validateResourceNetworkConnectionIDType((ResourceNetworkConnectionIDType)value, diagnostics, context);
			case B2MMLPackage.RESOURCE_REFERENCE_TYPE1_TYPE:
				return validateResourceReferenceType1Type((ResourceReferenceType1Type)value, diagnostics, context);
			case B2MMLPackage.RESOURCE_REFERENCE_TYPE_TYPE:
				return validateResourceReferenceTypeType((ResourceReferenceTypeType)value, diagnostics, context);
			case B2MMLPackage.RESOURCES_TYPE:
				return validateResourcesType((ResourcesType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE:
				return validateRespondEquipmentCapabilityTestSpecType((RespondEquipmentCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_EQUIPMENT_CLASS_TYPE:
				return validateRespondEquipmentClassType((RespondEquipmentClassType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_EQUIPMENT_INFORMATION_TYPE:
				return validateRespondEquipmentInformationType((RespondEquipmentInformationType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_EQUIPMENT_TYPE:
				return validateRespondEquipmentType((RespondEquipmentType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_MATERIAL_CLASS_TYPE:
				return validateRespondMaterialClassType((RespondMaterialClassType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_MATERIAL_DEFINITION_TYPE:
				return validateRespondMaterialDefinitionType((RespondMaterialDefinitionType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_MATERIAL_INFORMATION_TYPE:
				return validateRespondMaterialInformationType((RespondMaterialInformationType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_MATERIAL_LOT_TYPE:
				return validateRespondMaterialLotType((RespondMaterialLotType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_MATERIAL_SUB_LOT_TYPE:
				return validateRespondMaterialSubLotType((RespondMaterialSubLotType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_MATERIAL_TEST_SPEC_TYPE:
				return validateRespondMaterialTestSpecType((RespondMaterialTestSpecType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_OPERATIONS_CAPABILITY_INFORMATION_TYPE:
				return validateRespondOperationsCapabilityInformationType((RespondOperationsCapabilityInformationType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_OPERATIONS_CAPABILITY_TYPE:
				return validateRespondOperationsCapabilityType((RespondOperationsCapabilityType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_OPERATIONS_DEFINITION_INFORMATION_TYPE:
				return validateRespondOperationsDefinitionInformationType((RespondOperationsDefinitionInformationType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_OPERATIONS_DEFINITION_TYPE:
				return validateRespondOperationsDefinitionType((RespondOperationsDefinitionType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_OPERATIONS_PERFORMANCE_TYPE:
				return validateRespondOperationsPerformanceType((RespondOperationsPerformanceType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE:
				return validateRespondOperationsScheduleType((RespondOperationsScheduleType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_PERSONNEL_CLASS_TYPE:
				return validateRespondPersonnelClassType((RespondPersonnelClassType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_PERSONNEL_INFORMATION_TYPE:
				return validateRespondPersonnelInformationType((RespondPersonnelInformationType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_PERSON_TYPE:
				return validateRespondPersonType((RespondPersonType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE:
				return validateRespondPhysicalAssetCapabilityTestSpecType((RespondPhysicalAssetCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_PHYSICAL_ASSET_CLASS_TYPE:
				return validateRespondPhysicalAssetClassType((RespondPhysicalAssetClassType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_PHYSICAL_ASSET_INFORMATION_TYPE:
				return validateRespondPhysicalAssetInformationType((RespondPhysicalAssetInformationType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_PHYSICAL_ASSET_TYPE:
				return validateRespondPhysicalAssetType((RespondPhysicalAssetType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_PROCESS_SEGMENT_INFORMATION_TYPE:
				return validateRespondProcessSegmentInformationType((RespondProcessSegmentInformationType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_PROCESS_SEGMENT_TYPE:
				return validateRespondProcessSegmentType((RespondProcessSegmentType)value, diagnostics, context);
			case B2MMLPackage.RESPOND_QUALIFICATION_TEST_SPECIFICATION_TYPE:
				return validateRespondQualificationTestSpecificationType((RespondQualificationTestSpecificationType)value, diagnostics, context);
			case B2MMLPackage.RESPONSE_STATE1_TYPE:
				return validateResponseState1Type((ResponseState1Type)value, diagnostics, context);
			case B2MMLPackage.RESPONSE_STATE_TYPE:
				return validateResponseStateType((ResponseStateType)value, diagnostics, context);
			case B2MMLPackage.RESULT_TYPE:
				return validateResultType((ResultType)value, diagnostics, context);
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE:
				return validateSegmentDependencyType((SegmentDependencyType)value, diagnostics, context);
			case B2MMLPackage.SHOW_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE:
				return validateShowEquipmentCapabilityTestSpecType((ShowEquipmentCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.SHOW_EQUIPMENT_CLASS_TYPE:
				return validateShowEquipmentClassType((ShowEquipmentClassType)value, diagnostics, context);
			case B2MMLPackage.SHOW_EQUIPMENT_INFORMATION_TYPE:
				return validateShowEquipmentInformationType((ShowEquipmentInformationType)value, diagnostics, context);
			case B2MMLPackage.SHOW_EQUIPMENT_TYPE:
				return validateShowEquipmentType((ShowEquipmentType)value, diagnostics, context);
			case B2MMLPackage.SHOW_MATERIAL_CLASS_TYPE:
				return validateShowMaterialClassType((ShowMaterialClassType)value, diagnostics, context);
			case B2MMLPackage.SHOW_MATERIAL_DEFINITION_TYPE:
				return validateShowMaterialDefinitionType((ShowMaterialDefinitionType)value, diagnostics, context);
			case B2MMLPackage.SHOW_MATERIAL_INFORMATION_TYPE:
				return validateShowMaterialInformationType((ShowMaterialInformationType)value, diagnostics, context);
			case B2MMLPackage.SHOW_MATERIAL_LOT_TYPE:
				return validateShowMaterialLotType((ShowMaterialLotType)value, diagnostics, context);
			case B2MMLPackage.SHOW_MATERIAL_SUB_LOT_TYPE:
				return validateShowMaterialSubLotType((ShowMaterialSubLotType)value, diagnostics, context);
			case B2MMLPackage.SHOW_MATERIAL_TEST_SPEC_TYPE:
				return validateShowMaterialTestSpecType((ShowMaterialTestSpecType)value, diagnostics, context);
			case B2MMLPackage.SHOW_OPERATIONS_CAPABILITY_INFORMATION_TYPE:
				return validateShowOperationsCapabilityInformationType((ShowOperationsCapabilityInformationType)value, diagnostics, context);
			case B2MMLPackage.SHOW_OPERATIONS_CAPABILITY_TYPE:
				return validateShowOperationsCapabilityType((ShowOperationsCapabilityType)value, diagnostics, context);
			case B2MMLPackage.SHOW_OPERATIONS_DEFINITION_INFORMATION_TYPE:
				return validateShowOperationsDefinitionInformationType((ShowOperationsDefinitionInformationType)value, diagnostics, context);
			case B2MMLPackage.SHOW_OPERATIONS_DEFINITION_TYPE:
				return validateShowOperationsDefinitionType((ShowOperationsDefinitionType)value, diagnostics, context);
			case B2MMLPackage.SHOW_OPERATIONS_PERFORMANCE_TYPE:
				return validateShowOperationsPerformanceType((ShowOperationsPerformanceType)value, diagnostics, context);
			case B2MMLPackage.SHOW_OPERATIONS_SCHEDULE_TYPE:
				return validateShowOperationsScheduleType((ShowOperationsScheduleType)value, diagnostics, context);
			case B2MMLPackage.SHOW_PERSONNEL_CLASS_TYPE:
				return validateShowPersonnelClassType((ShowPersonnelClassType)value, diagnostics, context);
			case B2MMLPackage.SHOW_PERSONNEL_INFORMATION_TYPE:
				return validateShowPersonnelInformationType((ShowPersonnelInformationType)value, diagnostics, context);
			case B2MMLPackage.SHOW_PERSON_TYPE:
				return validateShowPersonType((ShowPersonType)value, diagnostics, context);
			case B2MMLPackage.SHOW_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE:
				return validateShowPhysicalAssetCapabilityTestSpecType((ShowPhysicalAssetCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.SHOW_PHYSICAL_ASSET_CLASS_TYPE:
				return validateShowPhysicalAssetClassType((ShowPhysicalAssetClassType)value, diagnostics, context);
			case B2MMLPackage.SHOW_PHYSICAL_ASSET_INFORMATION_TYPE:
				return validateShowPhysicalAssetInformationType((ShowPhysicalAssetInformationType)value, diagnostics, context);
			case B2MMLPackage.SHOW_PHYSICAL_ASSET_TYPE:
				return validateShowPhysicalAssetType((ShowPhysicalAssetType)value, diagnostics, context);
			case B2MMLPackage.SHOW_PROCESS_SEGMENT_INFORMATION_TYPE:
				return validateShowProcessSegmentInformationType((ShowProcessSegmentInformationType)value, diagnostics, context);
			case B2MMLPackage.SHOW_PROCESS_SEGMENT_TYPE:
				return validateShowProcessSegmentType((ShowProcessSegmentType)value, diagnostics, context);
			case B2MMLPackage.SHOW_QUALIFICATION_TEST_SPECIFICATION_TYPE:
				return validateShowQualificationTestSpecificationType((ShowQualificationTestSpecificationType)value, diagnostics, context);
			case B2MMLPackage.START_TIME_TYPE:
				return validateStartTimeType((StartTimeType)value, diagnostics, context);
			case B2MMLPackage.STATUS_TIME_TYPE:
				return validateStatusTimeType((StatusTimeType)value, diagnostics, context);
			case B2MMLPackage.STATUS_TYPE:
				return validateStatusType((StatusType)value, diagnostics, context);
			case B2MMLPackage.STORAGE_HIERARCHY_SCOPE_TYPE:
				return validateStorageHierarchyScopeType((StorageHierarchyScopeType)value, diagnostics, context);
			case B2MMLPackage.STORAGE_LOCATION_TYPE:
				return validateStorageLocationType((StorageLocationType)value, diagnostics, context);
			case B2MMLPackage.SYNC_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE:
				return validateSyncEquipmentCapabilityTestSpecType((SyncEquipmentCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.SYNC_EQUIPMENT_CLASS_TYPE:
				return validateSyncEquipmentClassType((SyncEquipmentClassType)value, diagnostics, context);
			case B2MMLPackage.SYNC_EQUIPMENT_INFORMATION_TYPE:
				return validateSyncEquipmentInformationType((SyncEquipmentInformationType)value, diagnostics, context);
			case B2MMLPackage.SYNC_EQUIPMENT_TYPE:
				return validateSyncEquipmentType((SyncEquipmentType)value, diagnostics, context);
			case B2MMLPackage.SYNC_MATERIAL_CLASS_TYPE:
				return validateSyncMaterialClassType((SyncMaterialClassType)value, diagnostics, context);
			case B2MMLPackage.SYNC_MATERIAL_DEFINITION_TYPE:
				return validateSyncMaterialDefinitionType((SyncMaterialDefinitionType)value, diagnostics, context);
			case B2MMLPackage.SYNC_MATERIAL_INFORMATION_TYPE:
				return validateSyncMaterialInformationType((SyncMaterialInformationType)value, diagnostics, context);
			case B2MMLPackage.SYNC_MATERIAL_LOT_TYPE:
				return validateSyncMaterialLotType((SyncMaterialLotType)value, diagnostics, context);
			case B2MMLPackage.SYNC_MATERIAL_SUB_LOT_TYPE:
				return validateSyncMaterialSubLotType((SyncMaterialSubLotType)value, diagnostics, context);
			case B2MMLPackage.SYNC_MATERIAL_TEST_SPEC_TYPE:
				return validateSyncMaterialTestSpecType((SyncMaterialTestSpecType)value, diagnostics, context);
			case B2MMLPackage.SYNC_OPERATIONS_CAPABILITY_INFORMATION_TYPE:
				return validateSyncOperationsCapabilityInformationType((SyncOperationsCapabilityInformationType)value, diagnostics, context);
			case B2MMLPackage.SYNC_OPERATIONS_CAPABILITY_TYPE:
				return validateSyncOperationsCapabilityType((SyncOperationsCapabilityType)value, diagnostics, context);
			case B2MMLPackage.SYNC_OPERATIONS_DEFINITION_INFORMATION_TYPE:
				return validateSyncOperationsDefinitionInformationType((SyncOperationsDefinitionInformationType)value, diagnostics, context);
			case B2MMLPackage.SYNC_OPERATIONS_DEFINITION_TYPE:
				return validateSyncOperationsDefinitionType((SyncOperationsDefinitionType)value, diagnostics, context);
			case B2MMLPackage.SYNC_OPERATIONS_PERFORMANCE_TYPE:
				return validateSyncOperationsPerformanceType((SyncOperationsPerformanceType)value, diagnostics, context);
			case B2MMLPackage.SYNC_OPERATIONS_SCHEDULE_TYPE:
				return validateSyncOperationsScheduleType((SyncOperationsScheduleType)value, diagnostics, context);
			case B2MMLPackage.SYNC_PERSONNEL_CLASS_TYPE:
				return validateSyncPersonnelClassType((SyncPersonnelClassType)value, diagnostics, context);
			case B2MMLPackage.SYNC_PERSONNEL_INFORMATION_TYPE:
				return validateSyncPersonnelInformationType((SyncPersonnelInformationType)value, diagnostics, context);
			case B2MMLPackage.SYNC_PERSON_TYPE:
				return validateSyncPersonType((SyncPersonType)value, diagnostics, context);
			case B2MMLPackage.SYNC_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE:
				return validateSyncPhysicalAssetCapabilityTestSpecType((SyncPhysicalAssetCapabilityTestSpecType)value, diagnostics, context);
			case B2MMLPackage.SYNC_PHYSICAL_ASSET_CLASS_TYPE:
				return validateSyncPhysicalAssetClassType((SyncPhysicalAssetClassType)value, diagnostics, context);
			case B2MMLPackage.SYNC_PHYSICAL_ASSET_INFORMATION_TYPE:
				return validateSyncPhysicalAssetInformationType((SyncPhysicalAssetInformationType)value, diagnostics, context);
			case B2MMLPackage.SYNC_PHYSICAL_ASSET_TYPE:
				return validateSyncPhysicalAssetType((SyncPhysicalAssetType)value, diagnostics, context);
			case B2MMLPackage.SYNC_PROCESS_SEGMENT_INFORMATION_TYPE:
				return validateSyncProcessSegmentInformationType((SyncProcessSegmentInformationType)value, diagnostics, context);
			case B2MMLPackage.SYNC_PROCESS_SEGMENT_TYPE:
				return validateSyncProcessSegmentType((SyncProcessSegmentType)value, diagnostics, context);
			case B2MMLPackage.SYNC_QUALIFICATION_TEST_SPECIFICATION_TYPE:
				return validateSyncQualificationTestSpecificationType((SyncQualificationTestSpecificationType)value, diagnostics, context);
			case B2MMLPackage.TEST_DATE_TIME_TYPE:
				return validateTestDateTimeType((TestDateTimeType)value, diagnostics, context);
			case B2MMLPackage.TESTED_EQUIPMENT_CLASS_PROPERTY_TYPE:
				return validateTestedEquipmentClassPropertyType((TestedEquipmentClassPropertyType)value, diagnostics, context);
			case B2MMLPackage.TESTED_EQUIPMENT_PROPERTY_TYPE:
				return validateTestedEquipmentPropertyType((TestedEquipmentPropertyType)value, diagnostics, context);
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE:
				return validateTestedMaterialClassPropertyType((TestedMaterialClassPropertyType)value, diagnostics, context);
			case B2MMLPackage.TESTED_MATERIAL_DEFINITION_PROPERTY_TYPE:
				return validateTestedMaterialDefinitionPropertyType((TestedMaterialDefinitionPropertyType)value, diagnostics, context);
			case B2MMLPackage.TESTED_MATERIAL_LOT_PROPERTY_TYPE:
				return validateTestedMaterialLotPropertyType((TestedMaterialLotPropertyType)value, diagnostics, context);
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE:
				return validateTestedPersonnelClassPropertyType((TestedPersonnelClassPropertyType)value, diagnostics, context);
			case B2MMLPackage.TESTED_PERSON_PROPERTY_TYPE:
				return validateTestedPersonPropertyType((TestedPersonPropertyType)value, diagnostics, context);
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE:
				return validateTestedPhysicalAssetClassPropertyType((TestedPhysicalAssetClassPropertyType)value, diagnostics, context);
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_PROPERTY_TYPE:
				return validateTestedPhysicalAssetPropertyType((TestedPhysicalAssetPropertyType)value, diagnostics, context);
			case B2MMLPackage.TEST_RESULT_TYPE:
				return validateTestResultType((TestResultType)value, diagnostics, context);
			case B2MMLPackage.TEXT_TYPE:
				return validateTextType((TextType)value, diagnostics, context);
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE:
				return validateTransAcknowledgeType((TransAcknowledgeType)value, diagnostics, context);
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE:
				return validateTransActionCriteriaType((TransActionCriteriaType)value, diagnostics, context);
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE:
				return validateTransApplicationAreaType((TransApplicationAreaType)value, diagnostics, context);
			case B2MMLPackage.TRANS_CANCEL_TYPE:
				return validateTransCancelType((TransCancelType)value, diagnostics, context);
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE:
				return validateTransChangeStatusType((TransChangeStatusType)value, diagnostics, context);
			case B2MMLPackage.TRANS_CHANGE_TYPE:
				return validateTransChangeType((TransChangeType)value, diagnostics, context);
			case B2MMLPackage.TRANS_CONFIRMATION_CODE_TYPE:
				return validateTransConfirmationCodeType((TransConfirmationCodeType)value, diagnostics, context);
			case B2MMLPackage.TRANS_CONFIRM_TYPE:
				return validateTransConfirmType((TransConfirmType)value, diagnostics, context);
			case B2MMLPackage.TRANS_EXPRESSION1_TYPE:
				return validateTransExpression1Type((TransExpression1Type)value, diagnostics, context);
			case B2MMLPackage.TRANS_EXPRESSION_TYPE:
				return validateTransExpressionType((TransExpressionType)value, diagnostics, context);
			case B2MMLPackage.TRANS_GET_TYPE:
				return validateTransGetType((TransGetType)value, diagnostics, context);
			case B2MMLPackage.TRANS_PROCESS_TYPE:
				return validateTransProcessType((TransProcessType)value, diagnostics, context);
			case B2MMLPackage.TRANS_RECEIVER_TYPE:
				return validateTransReceiverType((TransReceiverType)value, diagnostics, context);
			case B2MMLPackage.TRANS_RESPOND_TYPE:
				return validateTransRespondType((TransRespondType)value, diagnostics, context);
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE:
				return validateTransResponseCriteriaType((TransResponseCriteriaType)value, diagnostics, context);
			case B2MMLPackage.TRANS_SENDER_TYPE:
				return validateTransSenderType((TransSenderType)value, diagnostics, context);
			case B2MMLPackage.TRANS_SHOW_TYPE:
				return validateTransShowType((TransShowType)value, diagnostics, context);
			case B2MMLPackage.TRANS_SIGNATURE_TYPE:
				return validateTransSignatureType((TransSignatureType)value, diagnostics, context);
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE:
				return validateTransStateChangeType((TransStateChangeType)value, diagnostics, context);
			case B2MMLPackage.TRANS_SYNC_TYPE:
				return validateTransSyncType((TransSyncType)value, diagnostics, context);
			case B2MMLPackage.TRANS_USER_AREA_TYPE:
				return validateTransUserAreaType((TransUserAreaType)value, diagnostics, context);
			case B2MMLPackage.UNIT_OF_MEASURE_TYPE:
				return validateUnitOfMeasureType((UnitOfMeasureType)value, diagnostics, context);
			case B2MMLPackage.VALUE_STRING_TYPE:
				return validateValueStringType((ValueStringType)value, diagnostics, context);
			case B2MMLPackage.VALUE_TYPE:
				return validateValueType((ValueType)value, diagnostics, context);
			case B2MMLPackage.VERSION_TYPE:
				return validateVersionType((VersionType)value, diagnostics, context);
			case B2MMLPackage.WORK_REQUEST_ID_TYPE:
				return validateWorkRequestIDType((WorkRequestIDType)value, diagnostics, context);
			case B2MMLPackage.WORK_SCHEDULE_ID_TYPE:
				return validateWorkScheduleIDType((WorkScheduleIDType)value, diagnostics, context);
			case B2MMLPackage.WORK_TYPE1_TYPE:
				return validateWorkType1Type((WorkType1Type)value, diagnostics, context);
			case B2MMLPackage.WORK_TYPE_TYPE:
				return validateWorkTypeType((WorkTypeType)value, diagnostics, context);
			case B2MMLPackage.ASSEMBLY_RELATIONSHIP1_TYPE_BASE:
				return validateAssemblyRelationship1TypeBase((AssemblyRelationship1TypeBase)value, diagnostics, context);
			case B2MMLPackage.ASSEMBLY_TYPE1_TYPE_BASE:
				return validateAssemblyType1TypeBase((AssemblyType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.CAPABILITY_TYPE1_TYPE_BASE:
				return validateCapabilityType1TypeBase((CapabilityType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.DATA_TYPE1_TYPE_BASE:
				return validateDataType1TypeBase((DataType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.DEPENDENCY1_TYPE_BASE:
				return validateDependency1TypeBase((Dependency1TypeBase)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_ELEMENT_LEVEL1_TYPE_BASE:
				return validateEquipmentElementLevel1TypeBase((EquipmentElementLevel1TypeBase)value, diagnostics, context);
			case B2MMLPackage.JOB_ORDER_COMMAND1_TYPE_BASE:
				return validateJobOrderCommand1TypeBase((JobOrderCommand1TypeBase)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_USE1_TYPE_BASE:
				return validateMaterialUse1TypeBase((MaterialUse1TypeBase)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_TYPE1_TYPE_BASE:
				return validateOperationsType1TypeBase((OperationsType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.RELATIONSHIP_FORM1_TYPE_BASE:
				return validateRelationshipForm1TypeBase((RelationshipForm1TypeBase)value, diagnostics, context);
			case B2MMLPackage.RELATIONSHIP_TYPE1_TYPE_BASE:
				return validateRelationshipType1TypeBase((RelationshipType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.REQUEST_STATE1_TYPE_BASE:
				return validateRequestState1TypeBase((RequestState1TypeBase)value, diagnostics, context);
			case B2MMLPackage.REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE1_TYPE_BASE:
				return validateRequiredByRequestedSegmentResponse1TypeBase((RequiredByRequestedSegmentResponse1TypeBase)value, diagnostics, context);
			case B2MMLPackage.RESOURCE_REFERENCE_TYPE1_TYPE_BASE:
				return validateResourceReferenceType1TypeBase((ResourceReferenceType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.RESPONSE_STATE1_TYPE_BASE:
				return validateResponseState1TypeBase((ResponseState1TypeBase)value, diagnostics, context);
			case B2MMLPackage.TRANS_ACTION_CODE_ENUMERATION_TYPE:
				return validateTransActionCodeEnumerationType((TransActionCodeEnumerationType)value, diagnostics, context);
			case B2MMLPackage.TRANS_RESPONSE_CODE_TYPE:
				return validateTransResponseCodeType((TransResponseCodeType)value, diagnostics, context);
			case B2MMLPackage.WORK_TYPE1_TYPE_BASE:
				return validateWorkType1TypeBase((WorkType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.ASSEMBLY_RELATIONSHIP1_TYPE_BASE_OBJECT:
				return validateAssemblyRelationship1TypeBaseObject((AssemblyRelationship1TypeBase)value, diagnostics, context);
			case B2MMLPackage.ASSEMBLY_TYPE1_TYPE_BASE_OBJECT:
				return validateAssemblyType1TypeBaseObject((AssemblyType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.CAPABILITY_TYPE1_TYPE_BASE_OBJECT:
				return validateCapabilityType1TypeBaseObject((CapabilityType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.DATA_TYPE1_TYPE_BASE_OBJECT:
				return validateDataType1TypeBaseObject((DataType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.DEPENDENCY1_TYPE_BASE_OBJECT:
				return validateDependency1TypeBaseObject((Dependency1TypeBase)value, diagnostics, context);
			case B2MMLPackage.DURATION_TYPE:
				return validateDurationType((Duration)value, diagnostics, context);
			case B2MMLPackage.EQUIPMENT_ELEMENT_LEVEL1_TYPE_BASE_OBJECT:
				return validateEquipmentElementLevel1TypeBaseObject((EquipmentElementLevel1TypeBase)value, diagnostics, context);
			case B2MMLPackage.INDICATOR_TYPE:
				return validateIndicatorType((Boolean)value, diagnostics, context);
			case B2MMLPackage.INDICATOR_TYPE_OBJECT:
				return validateIndicatorTypeObject((Boolean)value, diagnostics, context);
			case B2MMLPackage.JOB_ORDER_COMMAND1_TYPE_BASE_OBJECT:
				return validateJobOrderCommand1TypeBaseObject((JobOrderCommand1TypeBase)value, diagnostics, context);
			case B2MMLPackage.MATERIAL_USE1_TYPE_BASE_OBJECT:
				return validateMaterialUse1TypeBaseObject((MaterialUse1TypeBase)value, diagnostics, context);
			case B2MMLPackage.OPERATIONS_TYPE1_TYPE_BASE_OBJECT:
				return validateOperationsType1TypeBaseObject((OperationsType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.RELATIONSHIP_FORM1_TYPE_BASE_OBJECT:
				return validateRelationshipForm1TypeBaseObject((RelationshipForm1TypeBase)value, diagnostics, context);
			case B2MMLPackage.RELATIONSHIP_TYPE1_TYPE_BASE_OBJECT:
				return validateRelationshipType1TypeBaseObject((RelationshipType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.REQUEST_STATE1_TYPE_BASE_OBJECT:
				return validateRequestState1TypeBaseObject((RequestState1TypeBase)value, diagnostics, context);
			case B2MMLPackage.REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE1_TYPE_BASE_OBJECT:
				return validateRequiredByRequestedSegmentResponse1TypeBaseObject((RequiredByRequestedSegmentResponse1TypeBase)value, diagnostics, context);
			case B2MMLPackage.RESOURCE_REFERENCE_TYPE1_TYPE_BASE_OBJECT:
				return validateResourceReferenceType1TypeBaseObject((ResourceReferenceType1TypeBase)value, diagnostics, context);
			case B2MMLPackage.RESPONSE_STATE1_TYPE_BASE_OBJECT:
				return validateResponseState1TypeBaseObject((ResponseState1TypeBase)value, diagnostics, context);
			case B2MMLPackage.TRANS_ACTION_CODE_ENUMERATION_TYPE_OBJECT:
				return validateTransActionCodeEnumerationTypeObject((TransActionCodeEnumerationType)value, diagnostics, context);
			case B2MMLPackage.TRANS_ACTION_CODE_TYPE:
				return validateTransActionCodeType(value, diagnostics, context);
			case B2MMLPackage.TRANS_RESPONSE_CODE_TYPE_OBJECT:
				return validateTransResponseCodeTypeObject((TransResponseCodeType)value, diagnostics, context);
			case B2MMLPackage.WORK_TYPE1_TYPE_BASE_OBJECT:
				return validateWorkType1TypeBaseObject((WorkType1TypeBase)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeEquipmentCapabilityTestSpecType(AcknowledgeEquipmentCapabilityTestSpecType acknowledgeEquipmentCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeEquipmentCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeEquipmentClassType(AcknowledgeEquipmentClassType acknowledgeEquipmentClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeEquipmentClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeEquipmentInformationType(AcknowledgeEquipmentInformationType acknowledgeEquipmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeEquipmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeEquipmentType(AcknowledgeEquipmentType acknowledgeEquipmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeEquipmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeMaterialClassType(AcknowledgeMaterialClassType acknowledgeMaterialClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeMaterialClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeMaterialDefinitionType(AcknowledgeMaterialDefinitionType acknowledgeMaterialDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeMaterialDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeMaterialInformationType(AcknowledgeMaterialInformationType acknowledgeMaterialInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeMaterialInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeMaterialLotType(AcknowledgeMaterialLotType acknowledgeMaterialLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeMaterialLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeMaterialSubLotType(AcknowledgeMaterialSubLotType acknowledgeMaterialSubLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeMaterialSubLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeMaterialTestSpecType(AcknowledgeMaterialTestSpecType acknowledgeMaterialTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeMaterialTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeOperationsCapabilityInformationType(AcknowledgeOperationsCapabilityInformationType acknowledgeOperationsCapabilityInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeOperationsCapabilityInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeOperationsCapabilityType(AcknowledgeOperationsCapabilityType acknowledgeOperationsCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeOperationsCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeOperationsDefinitionInformationType(AcknowledgeOperationsDefinitionInformationType acknowledgeOperationsDefinitionInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeOperationsDefinitionInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeOperationsDefinitionType(AcknowledgeOperationsDefinitionType acknowledgeOperationsDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeOperationsDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeOperationsPerformanceType(AcknowledgeOperationsPerformanceType acknowledgeOperationsPerformanceType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeOperationsPerformanceType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeOperationsScheduleType(AcknowledgeOperationsScheduleType acknowledgeOperationsScheduleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeOperationsScheduleType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgePersonnelClassType(AcknowledgePersonnelClassType acknowledgePersonnelClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgePersonnelClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgePersonnelInformationType(AcknowledgePersonnelInformationType acknowledgePersonnelInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgePersonnelInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgePersonType(AcknowledgePersonType acknowledgePersonType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgePersonType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgePhysicalAssetCapabilityTestSpecType(AcknowledgePhysicalAssetCapabilityTestSpecType acknowledgePhysicalAssetCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgePhysicalAssetCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgePhysicalAssetClassType(AcknowledgePhysicalAssetClassType acknowledgePhysicalAssetClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgePhysicalAssetClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgePhysicalAssetInformationType(AcknowledgePhysicalAssetInformationType acknowledgePhysicalAssetInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgePhysicalAssetInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgePhysicalAssetType(AcknowledgePhysicalAssetType acknowledgePhysicalAssetType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgePhysicalAssetType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeProcessSegmentInformationType(AcknowledgeProcessSegmentInformationType acknowledgeProcessSegmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeProcessSegmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeProcessSegmentType(AcknowledgeProcessSegmentType acknowledgeProcessSegmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeProcessSegmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcknowledgeQualificationTestSpecificationType(AcknowledgeQualificationTestSpecificationType acknowledgeQualificationTestSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(acknowledgeQualificationTestSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateActualEndTimeType(ActualEndTimeType actualEndTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(actualEndTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateActualFinishTimeType(ActualFinishTimeType actualFinishTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(actualFinishTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateActualStartTimeType(ActualStartTimeType actualStartTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(actualStartTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAmountType(AmountType amountType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(amountType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAnyGenericValueType(AnyGenericValueType anyGenericValueType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(anyGenericValueType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssemblyRelationship1Type(AssemblyRelationship1Type assemblyRelationship1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assemblyRelationship1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssemblyRelationshipType(AssemblyRelationshipType assemblyRelationshipType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assemblyRelationshipType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssemblyType1Type(AssemblyType1Type assemblyType1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assemblyType1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssemblyTypeType(AssemblyTypeType assemblyTypeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assemblyTypeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBillOfMaterialIDType(BillOfMaterialIDType billOfMaterialIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(billOfMaterialIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBillOfMaterialsIDType(BillOfMaterialsIDType billOfMaterialsIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(billOfMaterialsIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBillOfResourcesIDType(BillOfResourcesIDType billOfResourcesIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(billOfResourcesIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBinaryObjectType(BinaryObjectType binaryObjectType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(binaryObjectType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBODType(BODType bodType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(bodType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelEquipmentCapabilityTestSpecType(CancelEquipmentCapabilityTestSpecType cancelEquipmentCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelEquipmentCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelEquipmentClassType(CancelEquipmentClassType cancelEquipmentClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelEquipmentClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelEquipmentInformationType(CancelEquipmentInformationType cancelEquipmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelEquipmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelEquipmentType(CancelEquipmentType cancelEquipmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelEquipmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelMaterialClassType(CancelMaterialClassType cancelMaterialClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelMaterialClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelMaterialDefinitionType(CancelMaterialDefinitionType cancelMaterialDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelMaterialDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelMaterialInformationType(CancelMaterialInformationType cancelMaterialInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelMaterialInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelMaterialLotType(CancelMaterialLotType cancelMaterialLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelMaterialLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelMaterialSubLotType(CancelMaterialSubLotType cancelMaterialSubLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelMaterialSubLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelMaterialTestSpecType(CancelMaterialTestSpecType cancelMaterialTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelMaterialTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelOperationsCapabilityInformationType(CancelOperationsCapabilityInformationType cancelOperationsCapabilityInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelOperationsCapabilityInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelOperationsCapabilityType(CancelOperationsCapabilityType cancelOperationsCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelOperationsCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelOperationsDefinitionInformationType(CancelOperationsDefinitionInformationType cancelOperationsDefinitionInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelOperationsDefinitionInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelOperationsDefinitionType(CancelOperationsDefinitionType cancelOperationsDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelOperationsDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelOperationsPerformanceType(CancelOperationsPerformanceType cancelOperationsPerformanceType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelOperationsPerformanceType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelOperationsScheduleType(CancelOperationsScheduleType cancelOperationsScheduleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelOperationsScheduleType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelPersonnelClassType(CancelPersonnelClassType cancelPersonnelClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelPersonnelClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelPersonnelInformationType(CancelPersonnelInformationType cancelPersonnelInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelPersonnelInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelPersonType(CancelPersonType cancelPersonType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelPersonType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelPhysicalAssetCapabilityTestSpecType(CancelPhysicalAssetCapabilityTestSpecType cancelPhysicalAssetCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelPhysicalAssetCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelPhysicalAssetClassType(CancelPhysicalAssetClassType cancelPhysicalAssetClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelPhysicalAssetClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelPhysicalAssetInformationType(CancelPhysicalAssetInformationType cancelPhysicalAssetInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelPhysicalAssetInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelPhysicalAssetType(CancelPhysicalAssetType cancelPhysicalAssetType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelPhysicalAssetType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelProcessSegmentInformationType(CancelProcessSegmentInformationType cancelProcessSegmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelProcessSegmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelProcessSegmentType(CancelProcessSegmentType cancelProcessSegmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelProcessSegmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCancelQualificationTestSpecificationType(CancelQualificationTestSpecificationType cancelQualificationTestSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(cancelQualificationTestSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCapabilityType1Type(CapabilityType1Type capabilityType1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(capabilityType1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCapabilityTypeType(CapabilityTypeType capabilityTypeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(capabilityTypeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCauseType(CauseType causeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(causeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCertificateOfAnalysisReferenceType(CertificateOfAnalysisReferenceType certificateOfAnalysisReferenceType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(certificateOfAnalysisReferenceType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeEquipmentCapabilityTestSpecType(ChangeEquipmentCapabilityTestSpecType changeEquipmentCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeEquipmentCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeEquipmentClassType(ChangeEquipmentClassType changeEquipmentClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeEquipmentClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeEquipmentInformationType(ChangeEquipmentInformationType changeEquipmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeEquipmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeEquipmentType(ChangeEquipmentType changeEquipmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeEquipmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeMaterialClassType(ChangeMaterialClassType changeMaterialClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeMaterialClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeMaterialDefinitionType(ChangeMaterialDefinitionType changeMaterialDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeMaterialDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeMaterialInformationType(ChangeMaterialInformationType changeMaterialInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeMaterialInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeMaterialLotType(ChangeMaterialLotType changeMaterialLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeMaterialLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeMaterialSubLotType(ChangeMaterialSubLotType changeMaterialSubLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeMaterialSubLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeMaterialTestSpecType(ChangeMaterialTestSpecType changeMaterialTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeMaterialTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeOperationsCapabilityInformationType(ChangeOperationsCapabilityInformationType changeOperationsCapabilityInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeOperationsCapabilityInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeOperationsCapabilityType(ChangeOperationsCapabilityType changeOperationsCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeOperationsCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeOperationsDefinitionInformationType(ChangeOperationsDefinitionInformationType changeOperationsDefinitionInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeOperationsDefinitionInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeOperationsDefinitionType(ChangeOperationsDefinitionType changeOperationsDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeOperationsDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeOperationsPerformanceType(ChangeOperationsPerformanceType changeOperationsPerformanceType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeOperationsPerformanceType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeOperationsScheduleType(ChangeOperationsScheduleType changeOperationsScheduleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeOperationsScheduleType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangePersonnelClassType(ChangePersonnelClassType changePersonnelClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changePersonnelClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangePersonnelInformationType(ChangePersonnelInformationType changePersonnelInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changePersonnelInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangePersonType(ChangePersonType changePersonType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changePersonType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangePhysicalAssetCapabilityTestSpecType(ChangePhysicalAssetCapabilityTestSpecType changePhysicalAssetCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changePhysicalAssetCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangePhysicalAssetClassType(ChangePhysicalAssetClassType changePhysicalAssetClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changePhysicalAssetClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangePhysicalAssetInformationType(ChangePhysicalAssetInformationType changePhysicalAssetInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changePhysicalAssetInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangePhysicalAssetType(ChangePhysicalAssetType changePhysicalAssetType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changePhysicalAssetType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeProcessSegmentInformationType(ChangeProcessSegmentInformationType changeProcessSegmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeProcessSegmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeProcessSegmentType(ChangeProcessSegmentType changeProcessSegmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeProcessSegmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChangeQualificationTestSpecificationType(ChangeQualificationTestSpecificationType changeQualificationTestSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(changeQualificationTestSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCodeType(CodeType codeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(codeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConfidenceFactorType(ConfidenceFactorType confidenceFactorType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(confidenceFactorType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConfirmBODType(ConfirmBODType confirmBODType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(confirmBODType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCorrectionType(CorrectionType correctionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(correctionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType(DataAreaType dataAreaType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType1(DataAreaType1 dataAreaType1, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType1, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType2(DataAreaType2 dataAreaType2, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType2, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType3(DataAreaType3 dataAreaType3, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType3, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType4(DataAreaType4 dataAreaType4, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType4, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType5(DataAreaType5 dataAreaType5, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType5, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType6(DataAreaType6 dataAreaType6, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType6, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType7(DataAreaType7 dataAreaType7, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType7, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType8(DataAreaType8 dataAreaType8, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType8, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType9(DataAreaType9 dataAreaType9, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType9, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType10(DataAreaType10 dataAreaType10, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType10, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType11(DataAreaType11 dataAreaType11, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType11, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType12(DataAreaType12 dataAreaType12, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType12, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType13(DataAreaType13 dataAreaType13, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType13, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType14(DataAreaType14 dataAreaType14, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType14, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType15(DataAreaType15 dataAreaType15, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType15, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType16(DataAreaType16 dataAreaType16, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType16, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType17(DataAreaType17 dataAreaType17, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType17, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType18(DataAreaType18 dataAreaType18, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType18, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType19(DataAreaType19 dataAreaType19, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType19, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType20(DataAreaType20 dataAreaType20, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType20, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType21(DataAreaType21 dataAreaType21, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType21, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType22(DataAreaType22 dataAreaType22, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType22, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType23(DataAreaType23 dataAreaType23, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType23, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType24(DataAreaType24 dataAreaType24, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType24, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType25(DataAreaType25 dataAreaType25, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType25, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType26(DataAreaType26 dataAreaType26, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType26, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType27(DataAreaType27 dataAreaType27, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType27, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType28(DataAreaType28 dataAreaType28, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType28, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType29(DataAreaType29 dataAreaType29, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType29, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType30(DataAreaType30 dataAreaType30, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType30, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType31(DataAreaType31 dataAreaType31, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType31, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType32(DataAreaType32 dataAreaType32, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType32, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType33(DataAreaType33 dataAreaType33, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType33, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType34(DataAreaType34 dataAreaType34, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType34, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType35(DataAreaType35 dataAreaType35, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType35, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType36(DataAreaType36 dataAreaType36, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType36, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType37(DataAreaType37 dataAreaType37, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType37, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType38(DataAreaType38 dataAreaType38, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType38, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType39(DataAreaType39 dataAreaType39, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType39, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType40(DataAreaType40 dataAreaType40, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType40, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType41(DataAreaType41 dataAreaType41, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType41, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType42(DataAreaType42 dataAreaType42, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType42, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType43(DataAreaType43 dataAreaType43, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType43, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType44(DataAreaType44 dataAreaType44, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType44, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType45(DataAreaType45 dataAreaType45, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType45, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType46(DataAreaType46 dataAreaType46, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType46, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType47(DataAreaType47 dataAreaType47, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType47, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType48(DataAreaType48 dataAreaType48, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType48, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType49(DataAreaType49 dataAreaType49, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType49, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType50(DataAreaType50 dataAreaType50, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType50, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType51(DataAreaType51 dataAreaType51, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType51, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType52(DataAreaType52 dataAreaType52, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType52, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType53(DataAreaType53 dataAreaType53, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType53, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType54(DataAreaType54 dataAreaType54, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType54, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType55(DataAreaType55 dataAreaType55, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType55, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType56(DataAreaType56 dataAreaType56, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType56, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType57(DataAreaType57 dataAreaType57, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType57, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType58(DataAreaType58 dataAreaType58, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType58, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType59(DataAreaType59 dataAreaType59, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType59, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType60(DataAreaType60 dataAreaType60, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType60, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType61(DataAreaType61 dataAreaType61, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType61, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType62(DataAreaType62 dataAreaType62, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType62, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType63(DataAreaType63 dataAreaType63, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType63, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType64(DataAreaType64 dataAreaType64, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType64, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType65(DataAreaType65 dataAreaType65, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType65, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType66(DataAreaType66 dataAreaType66, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType66, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType67(DataAreaType67 dataAreaType67, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType67, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType68(DataAreaType68 dataAreaType68, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType68, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType69(DataAreaType69 dataAreaType69, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType69, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType70(DataAreaType70 dataAreaType70, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType70, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType71(DataAreaType71 dataAreaType71, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType71, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType72(DataAreaType72 dataAreaType72, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType72, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType73(DataAreaType73 dataAreaType73, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType73, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType74(DataAreaType74 dataAreaType74, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType74, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType75(DataAreaType75 dataAreaType75, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType75, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType76(DataAreaType76 dataAreaType76, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType76, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType77(DataAreaType77 dataAreaType77, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType77, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType78(DataAreaType78 dataAreaType78, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType78, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType79(DataAreaType79 dataAreaType79, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType79, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType80(DataAreaType80 dataAreaType80, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType80, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType81(DataAreaType81 dataAreaType81, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType81, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType82(DataAreaType82 dataAreaType82, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType82, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType83(DataAreaType83 dataAreaType83, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType83, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType84(DataAreaType84 dataAreaType84, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType84, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType85(DataAreaType85 dataAreaType85, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType85, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType86(DataAreaType86 dataAreaType86, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType86, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType87(DataAreaType87 dataAreaType87, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType87, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType88(DataAreaType88 dataAreaType88, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType88, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType89(DataAreaType89 dataAreaType89, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType89, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType90(DataAreaType90 dataAreaType90, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType90, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType91(DataAreaType91 dataAreaType91, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType91, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType92(DataAreaType92 dataAreaType92, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType92, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType93(DataAreaType93 dataAreaType93, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType93, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType94(DataAreaType94 dataAreaType94, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType94, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType95(DataAreaType95 dataAreaType95, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType95, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType96(DataAreaType96 dataAreaType96, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType96, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType97(DataAreaType97 dataAreaType97, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType97, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType98(DataAreaType98 dataAreaType98, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType98, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType99(DataAreaType99 dataAreaType99, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType99, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType100(DataAreaType100 dataAreaType100, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType100, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType101(DataAreaType101 dataAreaType101, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType101, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType102(DataAreaType102 dataAreaType102, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType102, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType103(DataAreaType103 dataAreaType103, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType103, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType104(DataAreaType104 dataAreaType104, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType104, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType105(DataAreaType105 dataAreaType105, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType105, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType106(DataAreaType106 dataAreaType106, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType106, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType107(DataAreaType107 dataAreaType107, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType107, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType108(DataAreaType108 dataAreaType108, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType108, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType109(DataAreaType109 dataAreaType109, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType109, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType110(DataAreaType110 dataAreaType110, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType110, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType111(DataAreaType111 dataAreaType111, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType111, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType112(DataAreaType112 dataAreaType112, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType112, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType113(DataAreaType113 dataAreaType113, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType113, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType114(DataAreaType114 dataAreaType114, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType114, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType115(DataAreaType115 dataAreaType115, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType115, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType116(DataAreaType116 dataAreaType116, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType116, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType117(DataAreaType117 dataAreaType117, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType117, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType118(DataAreaType118 dataAreaType118, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType118, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType119(DataAreaType119 dataAreaType119, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType119, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType120(DataAreaType120 dataAreaType120, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType120, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType121(DataAreaType121 dataAreaType121, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType121, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType122(DataAreaType122 dataAreaType122, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType122, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType123(DataAreaType123 dataAreaType123, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType123, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType124(DataAreaType124 dataAreaType124, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType124, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType125(DataAreaType125 dataAreaType125, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType125, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType126(DataAreaType126 dataAreaType126, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType126, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType127(DataAreaType127 dataAreaType127, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType127, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType128(DataAreaType128 dataAreaType128, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType128, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType129(DataAreaType129 dataAreaType129, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType129, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType130(DataAreaType130 dataAreaType130, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType130, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType131(DataAreaType131 dataAreaType131, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType131, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType132(DataAreaType132 dataAreaType132, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType132, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType133(DataAreaType133 dataAreaType133, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType133, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType134(DataAreaType134 dataAreaType134, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType134, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType135(DataAreaType135 dataAreaType135, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType135, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType136(DataAreaType136 dataAreaType136, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType136, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType137(DataAreaType137 dataAreaType137, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType137, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType138(DataAreaType138 dataAreaType138, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType138, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType139(DataAreaType139 dataAreaType139, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType139, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType140(DataAreaType140 dataAreaType140, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType140, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType141(DataAreaType141 dataAreaType141, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType141, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType142(DataAreaType142 dataAreaType142, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType142, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType143(DataAreaType143 dataAreaType143, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType143, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType144(DataAreaType144 dataAreaType144, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType144, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType145(DataAreaType145 dataAreaType145, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType145, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType146(DataAreaType146 dataAreaType146, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType146, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType147(DataAreaType147 dataAreaType147, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType147, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType148(DataAreaType148 dataAreaType148, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType148, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType149(DataAreaType149 dataAreaType149, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType149, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType150(DataAreaType150 dataAreaType150, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType150, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType151(DataAreaType151 dataAreaType151, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType151, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType152(DataAreaType152 dataAreaType152, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType152, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType153(DataAreaType153 dataAreaType153, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType153, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType154(DataAreaType154 dataAreaType154, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType154, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType155(DataAreaType155 dataAreaType155, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType155, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType156(DataAreaType156 dataAreaType156, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType156, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType157(DataAreaType157 dataAreaType157, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType157, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType158(DataAreaType158 dataAreaType158, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType158, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType159(DataAreaType159 dataAreaType159, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType159, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType160(DataAreaType160 dataAreaType160, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType160, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType161(DataAreaType161 dataAreaType161, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType161, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType162(DataAreaType162 dataAreaType162, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType162, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType163(DataAreaType163 dataAreaType163, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType163, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType164(DataAreaType164 dataAreaType164, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType164, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType165(DataAreaType165 dataAreaType165, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType165, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType166(DataAreaType166 dataAreaType166, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType166, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType167(DataAreaType167 dataAreaType167, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType167, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType168(DataAreaType168 dataAreaType168, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType168, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType169(DataAreaType169 dataAreaType169, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType169, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType170(DataAreaType170 dataAreaType170, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType170, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType171(DataAreaType171 dataAreaType171, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType171, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType172(DataAreaType172 dataAreaType172, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType172, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType173(DataAreaType173 dataAreaType173, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType173, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType174(DataAreaType174 dataAreaType174, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType174, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType175(DataAreaType175 dataAreaType175, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType175, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType176(DataAreaType176 dataAreaType176, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType176, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType177(DataAreaType177 dataAreaType177, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType177, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType178(DataAreaType178 dataAreaType178, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType178, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType179(DataAreaType179 dataAreaType179, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType179, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType180(DataAreaType180 dataAreaType180, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType180, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType181(DataAreaType181 dataAreaType181, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType181, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType182(DataAreaType182 dataAreaType182, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType182, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType183(DataAreaType183 dataAreaType183, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType183, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType184(DataAreaType184 dataAreaType184, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType184, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType185(DataAreaType185 dataAreaType185, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType185, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType186(DataAreaType186 dataAreaType186, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType186, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType187(DataAreaType187 dataAreaType187, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType187, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType188(DataAreaType188 dataAreaType188, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType188, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType189(DataAreaType189 dataAreaType189, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType189, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType190(DataAreaType190 dataAreaType190, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType190, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType191(DataAreaType191 dataAreaType191, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType191, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType192(DataAreaType192 dataAreaType192, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType192, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType193(DataAreaType193 dataAreaType193, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType193, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType194(DataAreaType194 dataAreaType194, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType194, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType195(DataAreaType195 dataAreaType195, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType195, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType196(DataAreaType196 dataAreaType196, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType196, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType197(DataAreaType197 dataAreaType197, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType197, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType198(DataAreaType198 dataAreaType198, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType198, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType199(DataAreaType199 dataAreaType199, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType199, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType200(DataAreaType200 dataAreaType200, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType200, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType201(DataAreaType201 dataAreaType201, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType201, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType202(DataAreaType202 dataAreaType202, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType202, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType203(DataAreaType203 dataAreaType203, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType203, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType204(DataAreaType204 dataAreaType204, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType204, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType205(DataAreaType205 dataAreaType205, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType205, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType206(DataAreaType206 dataAreaType206, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType206, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType207(DataAreaType207 dataAreaType207, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType207, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataAreaType208(DataAreaType208 dataAreaType208, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataAreaType208, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataType1Type(DataType1Type dataType1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataType1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataTypeType(DataTypeType dataTypeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataTypeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDateTimeType(DateTimeType dateTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dateTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDependency1Type(Dependency1Type dependency1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dependency1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDependencyType(DependencyType dependencyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dependencyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDescriptionType(DescriptionType descriptionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(descriptionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDocumentRoot(DocumentRoot documentRoot, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(documentRoot, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEarliestStartTimeType(EarliestStartTimeType earliestStartTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(earliestStartTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEndTimeType(EndTimeType endTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(endTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentAssetMappingType(EquipmentAssetMappingType equipmentAssetMappingType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentAssetMappingType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentCapabilityTestSpecificationIDType(EquipmentCapabilityTestSpecificationIDType equipmentCapabilityTestSpecificationIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentCapabilityTestSpecificationIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentCapabilityTestSpecificationType(EquipmentCapabilityTestSpecificationType equipmentCapabilityTestSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentCapabilityTestSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentClassIDType(EquipmentClassIDType equipmentClassIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentClassIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentClassPropertyType(EquipmentClassPropertyType equipmentClassPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentClassPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentClassType(EquipmentClassType equipmentClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentElementLevel1Type(EquipmentElementLevel1Type equipmentElementLevel1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentElementLevel1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentElementLevelType(EquipmentElementLevelType equipmentElementLevelType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentElementLevelType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentIDType(EquipmentIDType equipmentIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentInformationType(EquipmentInformationType equipmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentPropertyType(EquipmentPropertyType equipmentPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentSegmentSpecificationPropertyType(EquipmentSegmentSpecificationPropertyType equipmentSegmentSpecificationPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentSegmentSpecificationPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentSegmentSpecificationType(EquipmentSegmentSpecificationType equipmentSegmentSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentSegmentSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentType(EquipmentType equipmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentUseType(EquipmentUseType equipmentUseType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(equipmentUseType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExpirationTimeType(ExpirationTimeType expirationTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(expirationTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetEquipmentCapabilityTestSpecType(GetEquipmentCapabilityTestSpecType getEquipmentCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getEquipmentCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetEquipmentClassType(GetEquipmentClassType getEquipmentClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getEquipmentClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetEquipmentInformationType(GetEquipmentInformationType getEquipmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getEquipmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetEquipmentType(GetEquipmentType getEquipmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getEquipmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetMaterialClassType(GetMaterialClassType getMaterialClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getMaterialClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetMaterialDefinitionType(GetMaterialDefinitionType getMaterialDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getMaterialDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetMaterialInformationType(GetMaterialInformationType getMaterialInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getMaterialInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetMaterialLotType(GetMaterialLotType getMaterialLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getMaterialLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetMaterialSubLotType(GetMaterialSubLotType getMaterialSubLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getMaterialSubLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetMaterialTestSpecType(GetMaterialTestSpecType getMaterialTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getMaterialTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetOperationsCapabilityInformationType(GetOperationsCapabilityInformationType getOperationsCapabilityInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getOperationsCapabilityInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetOperationsCapabilityType(GetOperationsCapabilityType getOperationsCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getOperationsCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetOperationsDefinitionInformationType(GetOperationsDefinitionInformationType getOperationsDefinitionInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getOperationsDefinitionInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetOperationsDefinitionType(GetOperationsDefinitionType getOperationsDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getOperationsDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetOperationsPerformanceType(GetOperationsPerformanceType getOperationsPerformanceType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getOperationsPerformanceType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetOperationsScheduleType(GetOperationsScheduleType getOperationsScheduleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getOperationsScheduleType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetPersonnelClassType(GetPersonnelClassType getPersonnelClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getPersonnelClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetPersonnelInformationType(GetPersonnelInformationType getPersonnelInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getPersonnelInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetPersonType(GetPersonType getPersonType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getPersonType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetPhysicalAssetCapabilityTestSpecType(GetPhysicalAssetCapabilityTestSpecType getPhysicalAssetCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getPhysicalAssetCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetPhysicalAssetClassType(GetPhysicalAssetClassType getPhysicalAssetClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getPhysicalAssetClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetPhysicalAssetInformationType(GetPhysicalAssetInformationType getPhysicalAssetInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getPhysicalAssetInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetPhysicalAssetType(GetPhysicalAssetType getPhysicalAssetType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getPhysicalAssetType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetProcessSegmentInformationType(GetProcessSegmentInformationType getProcessSegmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getProcessSegmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetProcessSegmentType(GetProcessSegmentType getProcessSegmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getProcessSegmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGetQualificationTestSpecificationType(GetQualificationTestSpecificationType getQualificationTestSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(getQualificationTestSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHierarchyScopeType(HierarchyScopeType hierarchyScopeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(hierarchyScopeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIdentifierType(IdentifierType identifierType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(identifierType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateJobOrderCommand1Type(JobOrderCommand1Type jobOrderCommand1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(jobOrderCommand1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateJobOrderCommandRuleType(JobOrderCommandRuleType jobOrderCommandRuleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(jobOrderCommandRuleType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateJobOrderCommandType(JobOrderCommandType jobOrderCommandType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(jobOrderCommandType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateJobOrderDispatchStatusType(JobOrderDispatchStatusType jobOrderDispatchStatusType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(jobOrderDispatchStatusType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLatestEndTimeType(LatestEndTimeType latestEndTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(latestEndTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLocationType(LocationType locationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(locationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateManufacturingBillIDType(ManufacturingBillIDType manufacturingBillIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(manufacturingBillIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialActualIDType(MaterialActualIDType materialActualIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialActualIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialCapabilityIDType(MaterialCapabilityIDType materialCapabilityIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialCapabilityIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialClassIDType(MaterialClassIDType materialClassIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialClassIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialClassPropertyType(MaterialClassPropertyType materialClassPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialClassPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialClassType(MaterialClassType materialClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialDefinitionIDType(MaterialDefinitionIDType materialDefinitionIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialDefinitionIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialDefinitionPropertyType(MaterialDefinitionPropertyType materialDefinitionPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialDefinitionPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialDefinitionType(MaterialDefinitionType materialDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialInformationType(MaterialInformationType materialInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialLotIDType(MaterialLotIDType materialLotIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialLotIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialLotPropertyType(MaterialLotPropertyType materialLotPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialLotPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialLotType(MaterialLotType materialLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialRequirementIDType(MaterialRequirementIDType materialRequirementIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialRequirementIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialSegmentSpecificationPropertyType(MaterialSegmentSpecificationPropertyType materialSegmentSpecificationPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialSegmentSpecificationPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialSegmentSpecificationType(MaterialSegmentSpecificationType materialSegmentSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialSegmentSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialSpecificationIDType(MaterialSpecificationIDType materialSpecificationIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialSpecificationIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialSubLotIDType(MaterialSubLotIDType materialSubLotIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialSubLotIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialSubLotType(MaterialSubLotType materialSubLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialSubLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialTestSpecificationIDType(MaterialTestSpecificationIDType materialTestSpecificationIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialTestSpecificationIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialTestSpecificationType(MaterialTestSpecificationType materialTestSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialTestSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialUse1Type(MaterialUse1Type materialUse1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialUse1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialUseType(MaterialUseType materialUseType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(materialUseType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMeasureType(MeasureType measureType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(measureType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNameType(NameType nameType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(nameType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNumericType(NumericType numericType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(numericType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpEquipmentActualPropertyType(OpEquipmentActualPropertyType opEquipmentActualPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opEquipmentActualPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpEquipmentActualType(OpEquipmentActualType opEquipmentActualType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opEquipmentActualType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpEquipmentCapabilityPropertyType(OpEquipmentCapabilityPropertyType opEquipmentCapabilityPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opEquipmentCapabilityPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpEquipmentCapabilityType(OpEquipmentCapabilityType opEquipmentCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opEquipmentCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpEquipmentRequirementPropertyType(OpEquipmentRequirementPropertyType opEquipmentRequirementPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opEquipmentRequirementPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpEquipmentRequirementType(OpEquipmentRequirementType opEquipmentRequirementType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opEquipmentRequirementType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpEquipmentSpecificationPropertyType(OpEquipmentSpecificationPropertyType opEquipmentSpecificationPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opEquipmentSpecificationPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpEquipmentSpecificationType(OpEquipmentSpecificationType opEquipmentSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opEquipmentSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsCapabilityInformationType(OperationsCapabilityInformationType operationsCapabilityInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsCapabilityInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsCapabilityType(OperationsCapabilityType operationsCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsDefinitionIDType(OperationsDefinitionIDType operationsDefinitionIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsDefinitionIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsDefinitionInformationType(OperationsDefinitionInformationType operationsDefinitionInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsDefinitionInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsDefinitionType(OperationsDefinitionType operationsDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsMaterialBillItemType(OperationsMaterialBillItemType operationsMaterialBillItemType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsMaterialBillItemType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsMaterialBillType(OperationsMaterialBillType operationsMaterialBillType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsMaterialBillType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsPerformanceType(OperationsPerformanceType operationsPerformanceType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsPerformanceType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsRequestIDType(OperationsRequestIDType operationsRequestIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsRequestIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsRequestType(OperationsRequestType operationsRequestType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsRequestType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsResponseType(OperationsResponseType operationsResponseType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsResponseType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsScheduleIDType(OperationsScheduleIDType operationsScheduleIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsScheduleIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsScheduleType(OperationsScheduleType operationsScheduleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsScheduleType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsSegmentIDType(OperationsSegmentIDType operationsSegmentIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsSegmentIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsSegmentType(OperationsSegmentType operationsSegmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsSegmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsType1Type(OperationsType1Type operationsType1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsType1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsTypeType(OperationsTypeType operationsTypeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operationsTypeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpMaterialActualPropertyType(OpMaterialActualPropertyType opMaterialActualPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opMaterialActualPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpMaterialActualType(OpMaterialActualType opMaterialActualType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opMaterialActualType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpMaterialCapabilityPropertyType(OpMaterialCapabilityPropertyType opMaterialCapabilityPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opMaterialCapabilityPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpMaterialCapabilityType(OpMaterialCapabilityType opMaterialCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opMaterialCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpMaterialRequirementPropertyType(OpMaterialRequirementPropertyType opMaterialRequirementPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opMaterialRequirementPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpMaterialRequirementType(OpMaterialRequirementType opMaterialRequirementType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opMaterialRequirementType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpMaterialSpecificationPropertyType(OpMaterialSpecificationPropertyType opMaterialSpecificationPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opMaterialSpecificationPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpMaterialSpecificationType(OpMaterialSpecificationType opMaterialSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opMaterialSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPersonnelActualPropertyType(OpPersonnelActualPropertyType opPersonnelActualPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPersonnelActualPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPersonnelActualType(OpPersonnelActualType opPersonnelActualType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPersonnelActualType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPersonnelCapabilityPropertyType(OpPersonnelCapabilityPropertyType opPersonnelCapabilityPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPersonnelCapabilityPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPersonnelCapabilityType(OpPersonnelCapabilityType opPersonnelCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPersonnelCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPersonnelRequirementPropertyType(OpPersonnelRequirementPropertyType opPersonnelRequirementPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPersonnelRequirementPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPersonnelRequirementType(OpPersonnelRequirementType opPersonnelRequirementType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPersonnelRequirementType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPersonnelSpecificationPropertyType(OpPersonnelSpecificationPropertyType opPersonnelSpecificationPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPersonnelSpecificationPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPersonnelSpecificationType(OpPersonnelSpecificationType opPersonnelSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPersonnelSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPhysicalAssetActualPropertyType(OpPhysicalAssetActualPropertyType opPhysicalAssetActualPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPhysicalAssetActualPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPhysicalAssetActualType(OpPhysicalAssetActualType opPhysicalAssetActualType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPhysicalAssetActualType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPhysicalAssetCapabilityPropertyType(OpPhysicalAssetCapabilityPropertyType opPhysicalAssetCapabilityPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPhysicalAssetCapabilityPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPhysicalAssetCapabilityType(OpPhysicalAssetCapabilityType opPhysicalAssetCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPhysicalAssetCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPhysicalAssetRequirementPropertyType(OpPhysicalAssetRequirementPropertyType opPhysicalAssetRequirementPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPhysicalAssetRequirementPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPhysicalAssetRequirementType(OpPhysicalAssetRequirementType opPhysicalAssetRequirementType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPhysicalAssetRequirementType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPhysicalAssetSpecificationPropertyType(OpPhysicalAssetSpecificationPropertyType opPhysicalAssetSpecificationPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPhysicalAssetSpecificationPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpPhysicalAssetSpecificationType(OpPhysicalAssetSpecificationType opPhysicalAssetSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opPhysicalAssetSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpProcessSegmentCapabilityType(OpProcessSegmentCapabilityType opProcessSegmentCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opProcessSegmentCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpSegmentDataType(OpSegmentDataType opSegmentDataType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opSegmentDataType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpSegmentRequirementType(OpSegmentRequirementType opSegmentRequirementType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opSegmentRequirementType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOpSegmentResponseType(OpSegmentResponseType opSegmentResponseType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(opSegmentResponseType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOtherDependencyType(OtherDependencyType otherDependencyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(otherDependencyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateParameterIDType(ParameterIDType parameterIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(parameterIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateParameterType(ParameterType parameterType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(parameterType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePersonIDType(PersonIDType personIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(personIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePersonNameType(PersonNameType personNameType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(personNameType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePersonnelClassIDType(PersonnelClassIDType personnelClassIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(personnelClassIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePersonnelClassPropertyType(PersonnelClassPropertyType personnelClassPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(personnelClassPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePersonnelClassType(PersonnelClassType personnelClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(personnelClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePersonnelInformationType(PersonnelInformationType personnelInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(personnelInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePersonnelSegmentSpecificationPropertyType(PersonnelSegmentSpecificationPropertyType personnelSegmentSpecificationPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(personnelSegmentSpecificationPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePersonnelSegmentSpecificationType(PersonnelSegmentSpecificationType personnelSegmentSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(personnelSegmentSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePersonnelUseType(PersonnelUseType personnelUseType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(personnelUseType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePersonPropertyType(PersonPropertyType personPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(personPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePersonType(PersonType personType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(personType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetActualIDType(PhysicalAssetActualIDType physicalAssetActualIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetActualIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetCapabilityTestSpecificationIDType(PhysicalAssetCapabilityTestSpecificationIDType physicalAssetCapabilityTestSpecificationIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetCapabilityTestSpecificationIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetCapabilityTestSpecificationType(PhysicalAssetCapabilityTestSpecificationType physicalAssetCapabilityTestSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetCapabilityTestSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetClassIDType(PhysicalAssetClassIDType physicalAssetClassIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetClassIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetClassPropertyType(PhysicalAssetClassPropertyType physicalAssetClassPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetClassPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetClassType(PhysicalAssetClassType physicalAssetClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetIDType(PhysicalAssetIDType physicalAssetIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetInformationType(PhysicalAssetInformationType physicalAssetInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetPropertyType(PhysicalAssetPropertyType physicalAssetPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetSegmentSpecificationPropertyType(PhysicalAssetSegmentSpecificationPropertyType physicalAssetSegmentSpecificationPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetSegmentSpecificationPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetSegmentSpecificationType(PhysicalAssetSegmentSpecificationType physicalAssetSegmentSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetSegmentSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetType(PhysicalAssetType physicalAssetType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePhysicalAssetUseType(PhysicalAssetUseType physicalAssetUseType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(physicalAssetUseType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePlannedFinishTimeType(PlannedFinishTimeType plannedFinishTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(plannedFinishTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePlannedStartTimeType(PlannedStartTimeType plannedStartTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(plannedStartTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePriorityType(PriorityType priorityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(priorityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProblemType(ProblemType problemType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(problemType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessEquipmentCapabilityTestSpecType(ProcessEquipmentCapabilityTestSpecType processEquipmentCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processEquipmentCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessEquipmentClassType(ProcessEquipmentClassType processEquipmentClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processEquipmentClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessEquipmentInformationType(ProcessEquipmentInformationType processEquipmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processEquipmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessEquipmentType(ProcessEquipmentType processEquipmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processEquipmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessMaterialClassType(ProcessMaterialClassType processMaterialClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processMaterialClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessMaterialDefinitionType(ProcessMaterialDefinitionType processMaterialDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processMaterialDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessMaterialInformationType(ProcessMaterialInformationType processMaterialInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processMaterialInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessMaterialLotType(ProcessMaterialLotType processMaterialLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processMaterialLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessMaterialSubLotType(ProcessMaterialSubLotType processMaterialSubLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processMaterialSubLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessMaterialTestSpecType(ProcessMaterialTestSpecType processMaterialTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processMaterialTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessOperationsCapabilityInformationType(ProcessOperationsCapabilityInformationType processOperationsCapabilityInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processOperationsCapabilityInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessOperationsCapabilityType(ProcessOperationsCapabilityType processOperationsCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processOperationsCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessOperationsDefinitionInformationType(ProcessOperationsDefinitionInformationType processOperationsDefinitionInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processOperationsDefinitionInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessOperationsDefinitionType(ProcessOperationsDefinitionType processOperationsDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processOperationsDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessOperationsPerformanceType(ProcessOperationsPerformanceType processOperationsPerformanceType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processOperationsPerformanceType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessOperationsScheduleType(ProcessOperationsScheduleType processOperationsScheduleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processOperationsScheduleType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessPersonnelClassType(ProcessPersonnelClassType processPersonnelClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processPersonnelClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessPersonnelInformationType(ProcessPersonnelInformationType processPersonnelInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processPersonnelInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessPersonType(ProcessPersonType processPersonType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processPersonType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessPhysicalAssetCapabilityTestSpecType(ProcessPhysicalAssetCapabilityTestSpecType processPhysicalAssetCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processPhysicalAssetCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessPhysicalAssetClassType(ProcessPhysicalAssetClassType processPhysicalAssetClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processPhysicalAssetClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessPhysicalAssetInformationType(ProcessPhysicalAssetInformationType processPhysicalAssetInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processPhysicalAssetInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessPhysicalAssetType(ProcessPhysicalAssetType processPhysicalAssetType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processPhysicalAssetType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessProcessSegmentInformationType(ProcessProcessSegmentInformationType processProcessSegmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processProcessSegmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessProcessSegmentType(ProcessProcessSegmentType processProcessSegmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processProcessSegmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessQualificationTestSpecificationType(ProcessQualificationTestSpecificationType processQualificationTestSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processQualificationTestSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessSegmentIDType(ProcessSegmentIDType processSegmentIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processSegmentIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessSegmentInformationType(ProcessSegmentInformationType processSegmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processSegmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessSegmentType(ProcessSegmentType processSegmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processSegmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProductionRequestIDType(ProductionRequestIDType productionRequestIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(productionRequestIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProductionScheduleIDType(ProductionScheduleIDType productionScheduleIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(productionScheduleIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProductProductionRuleIDType(ProductProductionRuleIDType productProductionRuleIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(productProductionRuleIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProductProductionRuleType(ProductProductionRuleType productProductionRuleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(productProductionRuleType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProductSegmentIDType(ProductSegmentIDType productSegmentIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(productSegmentIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePropertyIDType(PropertyIDType propertyIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(propertyIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePublishedDateType(PublishedDateType publishedDateType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(publishedDateType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateQualificationTestSpecificationIDType(QualificationTestSpecificationIDType qualificationTestSpecificationIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(qualificationTestSpecificationIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateQualificationTestSpecificationType(QualificationTestSpecificationType qualificationTestSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(qualificationTestSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateQuantityStringType(QuantityStringType quantityStringType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(quantityStringType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateQuantityType(QuantityType quantityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(quantityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateQuantityValueType(QuantityValueType quantityValueType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(quantityValueType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReasonType(ReasonType reasonType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(reasonType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRelationshipForm1Type(RelationshipForm1Type relationshipForm1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(relationshipForm1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRelationshipFormType(RelationshipFormType relationshipFormType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(relationshipFormType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRelationshipType1Type(RelationshipType1Type relationshipType1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(relationshipType1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRelationshipTypeType(RelationshipTypeType relationshipTypeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(relationshipTypeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequestedCompletionDateType(RequestedCompletionDateType requestedCompletionDateType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(requestedCompletionDateType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequestedPriorityType(RequestedPriorityType requestedPriorityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(requestedPriorityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequestState1Type(RequestState1Type requestState1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(requestState1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequestStateType(RequestStateType requestStateType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(requestStateType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequiredByRequestedSegmentResponse1Type(RequiredByRequestedSegmentResponse1Type requiredByRequestedSegmentResponse1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(requiredByRequestedSegmentResponse1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequiredByRequestedSegmentResponseType(RequiredByRequestedSegmentResponseType requiredByRequestedSegmentResponseType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(requiredByRequestedSegmentResponseType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceIDType(ResourceIDType resourceIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(resourceIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceNetworkConnectionIDType(ResourceNetworkConnectionIDType resourceNetworkConnectionIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(resourceNetworkConnectionIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceReferenceType1Type(ResourceReferenceType1Type resourceReferenceType1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(resourceReferenceType1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceReferenceTypeType(ResourceReferenceTypeType resourceReferenceTypeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(resourceReferenceTypeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourcesType(ResourcesType resourcesType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(resourcesType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondEquipmentCapabilityTestSpecType(RespondEquipmentCapabilityTestSpecType respondEquipmentCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondEquipmentCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondEquipmentClassType(RespondEquipmentClassType respondEquipmentClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondEquipmentClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondEquipmentInformationType(RespondEquipmentInformationType respondEquipmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondEquipmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondEquipmentType(RespondEquipmentType respondEquipmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondEquipmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondMaterialClassType(RespondMaterialClassType respondMaterialClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondMaterialClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondMaterialDefinitionType(RespondMaterialDefinitionType respondMaterialDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondMaterialDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondMaterialInformationType(RespondMaterialInformationType respondMaterialInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondMaterialInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondMaterialLotType(RespondMaterialLotType respondMaterialLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondMaterialLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondMaterialSubLotType(RespondMaterialSubLotType respondMaterialSubLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondMaterialSubLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondMaterialTestSpecType(RespondMaterialTestSpecType respondMaterialTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondMaterialTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondOperationsCapabilityInformationType(RespondOperationsCapabilityInformationType respondOperationsCapabilityInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondOperationsCapabilityInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondOperationsCapabilityType(RespondOperationsCapabilityType respondOperationsCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondOperationsCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondOperationsDefinitionInformationType(RespondOperationsDefinitionInformationType respondOperationsDefinitionInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondOperationsDefinitionInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondOperationsDefinitionType(RespondOperationsDefinitionType respondOperationsDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondOperationsDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondOperationsPerformanceType(RespondOperationsPerformanceType respondOperationsPerformanceType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondOperationsPerformanceType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondOperationsScheduleType(RespondOperationsScheduleType respondOperationsScheduleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondOperationsScheduleType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondPersonnelClassType(RespondPersonnelClassType respondPersonnelClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondPersonnelClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondPersonnelInformationType(RespondPersonnelInformationType respondPersonnelInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondPersonnelInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondPersonType(RespondPersonType respondPersonType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondPersonType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondPhysicalAssetCapabilityTestSpecType(RespondPhysicalAssetCapabilityTestSpecType respondPhysicalAssetCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondPhysicalAssetCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondPhysicalAssetClassType(RespondPhysicalAssetClassType respondPhysicalAssetClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondPhysicalAssetClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondPhysicalAssetInformationType(RespondPhysicalAssetInformationType respondPhysicalAssetInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondPhysicalAssetInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondPhysicalAssetType(RespondPhysicalAssetType respondPhysicalAssetType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondPhysicalAssetType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondProcessSegmentInformationType(RespondProcessSegmentInformationType respondProcessSegmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondProcessSegmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondProcessSegmentType(RespondProcessSegmentType respondProcessSegmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondProcessSegmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRespondQualificationTestSpecificationType(RespondQualificationTestSpecificationType respondQualificationTestSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(respondQualificationTestSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResponseState1Type(ResponseState1Type responseState1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(responseState1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResponseStateType(ResponseStateType responseStateType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(responseStateType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResultType(ResultType resultType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(resultType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSegmentDependencyType(SegmentDependencyType segmentDependencyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(segmentDependencyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowEquipmentCapabilityTestSpecType(ShowEquipmentCapabilityTestSpecType showEquipmentCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showEquipmentCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowEquipmentClassType(ShowEquipmentClassType showEquipmentClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showEquipmentClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowEquipmentInformationType(ShowEquipmentInformationType showEquipmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showEquipmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowEquipmentType(ShowEquipmentType showEquipmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showEquipmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowMaterialClassType(ShowMaterialClassType showMaterialClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showMaterialClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowMaterialDefinitionType(ShowMaterialDefinitionType showMaterialDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showMaterialDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowMaterialInformationType(ShowMaterialInformationType showMaterialInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showMaterialInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowMaterialLotType(ShowMaterialLotType showMaterialLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showMaterialLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowMaterialSubLotType(ShowMaterialSubLotType showMaterialSubLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showMaterialSubLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowMaterialTestSpecType(ShowMaterialTestSpecType showMaterialTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showMaterialTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowOperationsCapabilityInformationType(ShowOperationsCapabilityInformationType showOperationsCapabilityInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showOperationsCapabilityInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowOperationsCapabilityType(ShowOperationsCapabilityType showOperationsCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showOperationsCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowOperationsDefinitionInformationType(ShowOperationsDefinitionInformationType showOperationsDefinitionInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showOperationsDefinitionInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowOperationsDefinitionType(ShowOperationsDefinitionType showOperationsDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showOperationsDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowOperationsPerformanceType(ShowOperationsPerformanceType showOperationsPerformanceType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showOperationsPerformanceType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowOperationsScheduleType(ShowOperationsScheduleType showOperationsScheduleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showOperationsScheduleType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowPersonnelClassType(ShowPersonnelClassType showPersonnelClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showPersonnelClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowPersonnelInformationType(ShowPersonnelInformationType showPersonnelInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showPersonnelInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowPersonType(ShowPersonType showPersonType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showPersonType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowPhysicalAssetCapabilityTestSpecType(ShowPhysicalAssetCapabilityTestSpecType showPhysicalAssetCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showPhysicalAssetCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowPhysicalAssetClassType(ShowPhysicalAssetClassType showPhysicalAssetClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showPhysicalAssetClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowPhysicalAssetInformationType(ShowPhysicalAssetInformationType showPhysicalAssetInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showPhysicalAssetInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowPhysicalAssetType(ShowPhysicalAssetType showPhysicalAssetType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showPhysicalAssetType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowProcessSegmentInformationType(ShowProcessSegmentInformationType showProcessSegmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showProcessSegmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowProcessSegmentType(ShowProcessSegmentType showProcessSegmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showProcessSegmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowQualificationTestSpecificationType(ShowQualificationTestSpecificationType showQualificationTestSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(showQualificationTestSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStartTimeType(StartTimeType startTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(startTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStatusTimeType(StatusTimeType statusTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(statusTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStatusType(StatusType statusType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(statusType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStorageHierarchyScopeType(StorageHierarchyScopeType storageHierarchyScopeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(storageHierarchyScopeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStorageLocationType(StorageLocationType storageLocationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(storageLocationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncEquipmentCapabilityTestSpecType(SyncEquipmentCapabilityTestSpecType syncEquipmentCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncEquipmentCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncEquipmentClassType(SyncEquipmentClassType syncEquipmentClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncEquipmentClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncEquipmentInformationType(SyncEquipmentInformationType syncEquipmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncEquipmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncEquipmentType(SyncEquipmentType syncEquipmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncEquipmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncMaterialClassType(SyncMaterialClassType syncMaterialClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncMaterialClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncMaterialDefinitionType(SyncMaterialDefinitionType syncMaterialDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncMaterialDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncMaterialInformationType(SyncMaterialInformationType syncMaterialInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncMaterialInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncMaterialLotType(SyncMaterialLotType syncMaterialLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncMaterialLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncMaterialSubLotType(SyncMaterialSubLotType syncMaterialSubLotType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncMaterialSubLotType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncMaterialTestSpecType(SyncMaterialTestSpecType syncMaterialTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncMaterialTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncOperationsCapabilityInformationType(SyncOperationsCapabilityInformationType syncOperationsCapabilityInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncOperationsCapabilityInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncOperationsCapabilityType(SyncOperationsCapabilityType syncOperationsCapabilityType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncOperationsCapabilityType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncOperationsDefinitionInformationType(SyncOperationsDefinitionInformationType syncOperationsDefinitionInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncOperationsDefinitionInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncOperationsDefinitionType(SyncOperationsDefinitionType syncOperationsDefinitionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncOperationsDefinitionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncOperationsPerformanceType(SyncOperationsPerformanceType syncOperationsPerformanceType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncOperationsPerformanceType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncOperationsScheduleType(SyncOperationsScheduleType syncOperationsScheduleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncOperationsScheduleType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncPersonnelClassType(SyncPersonnelClassType syncPersonnelClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncPersonnelClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncPersonnelInformationType(SyncPersonnelInformationType syncPersonnelInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncPersonnelInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncPersonType(SyncPersonType syncPersonType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncPersonType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncPhysicalAssetCapabilityTestSpecType(SyncPhysicalAssetCapabilityTestSpecType syncPhysicalAssetCapabilityTestSpecType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncPhysicalAssetCapabilityTestSpecType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncPhysicalAssetClassType(SyncPhysicalAssetClassType syncPhysicalAssetClassType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncPhysicalAssetClassType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncPhysicalAssetInformationType(SyncPhysicalAssetInformationType syncPhysicalAssetInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncPhysicalAssetInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncPhysicalAssetType(SyncPhysicalAssetType syncPhysicalAssetType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncPhysicalAssetType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncProcessSegmentInformationType(SyncProcessSegmentInformationType syncProcessSegmentInformationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncProcessSegmentInformationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncProcessSegmentType(SyncProcessSegmentType syncProcessSegmentType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncProcessSegmentType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSyncQualificationTestSpecificationType(SyncQualificationTestSpecificationType syncQualificationTestSpecificationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(syncQualificationTestSpecificationType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestDateTimeType(TestDateTimeType testDateTimeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testDateTimeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestedEquipmentClassPropertyType(TestedEquipmentClassPropertyType testedEquipmentClassPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testedEquipmentClassPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestedEquipmentPropertyType(TestedEquipmentPropertyType testedEquipmentPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testedEquipmentPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestedMaterialClassPropertyType(TestedMaterialClassPropertyType testedMaterialClassPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testedMaterialClassPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestedMaterialDefinitionPropertyType(TestedMaterialDefinitionPropertyType testedMaterialDefinitionPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testedMaterialDefinitionPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestedMaterialLotPropertyType(TestedMaterialLotPropertyType testedMaterialLotPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testedMaterialLotPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestedPersonnelClassPropertyType(TestedPersonnelClassPropertyType testedPersonnelClassPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testedPersonnelClassPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestedPersonPropertyType(TestedPersonPropertyType testedPersonPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testedPersonPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestedPhysicalAssetClassPropertyType(TestedPhysicalAssetClassPropertyType testedPhysicalAssetClassPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testedPhysicalAssetClassPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestedPhysicalAssetPropertyType(TestedPhysicalAssetPropertyType testedPhysicalAssetPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testedPhysicalAssetPropertyType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTestResultType(TestResultType testResultType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(testResultType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTextType(TextType textType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(textType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransAcknowledgeType(TransAcknowledgeType transAcknowledgeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transAcknowledgeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransActionCriteriaType(TransActionCriteriaType transActionCriteriaType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transActionCriteriaType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransApplicationAreaType(TransApplicationAreaType transApplicationAreaType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transApplicationAreaType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransCancelType(TransCancelType transCancelType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transCancelType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransChangeStatusType(TransChangeStatusType transChangeStatusType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transChangeStatusType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransChangeType(TransChangeType transChangeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transChangeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransConfirmationCodeType(TransConfirmationCodeType transConfirmationCodeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transConfirmationCodeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransConfirmType(TransConfirmType transConfirmType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transConfirmType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransExpression1Type(TransExpression1Type transExpression1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transExpression1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransExpressionType(TransExpressionType transExpressionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transExpressionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransGetType(TransGetType transGetType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transGetType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransProcessType(TransProcessType transProcessType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transProcessType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransReceiverType(TransReceiverType transReceiverType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transReceiverType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransRespondType(TransRespondType transRespondType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transRespondType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransResponseCriteriaType(TransResponseCriteriaType transResponseCriteriaType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transResponseCriteriaType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransSenderType(TransSenderType transSenderType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transSenderType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransShowType(TransShowType transShowType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transShowType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransSignatureType(TransSignatureType transSignatureType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transSignatureType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransStateChangeType(TransStateChangeType transStateChangeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transStateChangeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransSyncType(TransSyncType transSyncType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transSyncType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransUserAreaType(TransUserAreaType transUserAreaType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transUserAreaType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnitOfMeasureType(UnitOfMeasureType unitOfMeasureType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(unitOfMeasureType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateValueStringType(ValueStringType valueStringType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(valueStringType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateValueType(ValueType valueType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(valueType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVersionType(VersionType versionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(versionType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWorkRequestIDType(WorkRequestIDType workRequestIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(workRequestIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWorkScheduleIDType(WorkScheduleIDType workScheduleIDType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(workScheduleIDType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWorkType1Type(WorkType1Type workType1Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(workType1Type, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWorkTypeType(WorkTypeType workTypeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(workTypeType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssemblyRelationship1TypeBase(AssemblyRelationship1TypeBase assemblyRelationship1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssemblyType1TypeBase(AssemblyType1TypeBase assemblyType1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCapabilityType1TypeBase(CapabilityType1TypeBase capabilityType1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataType1TypeBase(DataType1TypeBase dataType1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDependency1TypeBase(Dependency1TypeBase dependency1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentElementLevel1TypeBase(EquipmentElementLevel1TypeBase equipmentElementLevel1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateJobOrderCommand1TypeBase(JobOrderCommand1TypeBase jobOrderCommand1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialUse1TypeBase(MaterialUse1TypeBase materialUse1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsType1TypeBase(OperationsType1TypeBase operationsType1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRelationshipForm1TypeBase(RelationshipForm1TypeBase relationshipForm1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRelationshipType1TypeBase(RelationshipType1TypeBase relationshipType1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequestState1TypeBase(RequestState1TypeBase requestState1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequiredByRequestedSegmentResponse1TypeBase(RequiredByRequestedSegmentResponse1TypeBase requiredByRequestedSegmentResponse1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceReferenceType1TypeBase(ResourceReferenceType1TypeBase resourceReferenceType1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResponseState1TypeBase(ResponseState1TypeBase responseState1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransActionCodeEnumerationType(TransActionCodeEnumerationType transActionCodeEnumerationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransResponseCodeType(TransResponseCodeType transResponseCodeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWorkType1TypeBase(WorkType1TypeBase workType1TypeBase, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssemblyRelationship1TypeBaseObject(AssemblyRelationship1TypeBase assemblyRelationship1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssemblyType1TypeBaseObject(AssemblyType1TypeBase assemblyType1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCapabilityType1TypeBaseObject(CapabilityType1TypeBase capabilityType1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataType1TypeBaseObject(DataType1TypeBase dataType1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDependency1TypeBaseObject(Dependency1TypeBase dependency1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDurationType(Duration durationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEquipmentElementLevel1TypeBaseObject(EquipmentElementLevel1TypeBase equipmentElementLevel1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIndicatorType(boolean indicatorType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateIndicatorType_Pattern(indicatorType, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validateIndicatorType_Pattern
	 */
	public static final  PatternMatcher [][] INDICATOR_TYPE__PATTERN__VALUES =
		new PatternMatcher [][] {
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("false"),
				XMLTypeUtil.createPatternMatcher("true")
			}
		};

	/**
	 * Validates the Pattern constraint of '<em>Indicator Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIndicatorType_Pattern(boolean indicatorType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validatePattern(B2MMLPackage.eINSTANCE.getIndicatorType(), indicatorType, INDICATOR_TYPE__PATTERN__VALUES, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIndicatorTypeObject(Boolean indicatorTypeObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateIndicatorType_Pattern(indicatorTypeObject, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateJobOrderCommand1TypeBaseObject(JobOrderCommand1TypeBase jobOrderCommand1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaterialUse1TypeBaseObject(MaterialUse1TypeBase materialUse1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperationsType1TypeBaseObject(OperationsType1TypeBase operationsType1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRelationshipForm1TypeBaseObject(RelationshipForm1TypeBase relationshipForm1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRelationshipType1TypeBaseObject(RelationshipType1TypeBase relationshipType1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequestState1TypeBaseObject(RequestState1TypeBase requestState1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequiredByRequestedSegmentResponse1TypeBaseObject(RequiredByRequestedSegmentResponse1TypeBase requiredByRequestedSegmentResponse1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceReferenceType1TypeBaseObject(ResourceReferenceType1TypeBase resourceReferenceType1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResponseState1TypeBaseObject(ResponseState1TypeBase responseState1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransActionCodeEnumerationTypeObject(TransActionCodeEnumerationType transActionCodeEnumerationTypeObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransActionCodeType(Object transActionCodeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateTransActionCodeType_MemberTypes(transActionCodeType, diagnostics, context);
		return result;
	}

	/**
	 * Validates the MemberTypes constraint of '<em>Trans Action Code Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransActionCodeType_MemberTypes(Object transActionCodeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (diagnostics != null) {
			BasicDiagnostic tempDiagnostics = new BasicDiagnostic();
			if (B2MMLPackage.eINSTANCE.getTransActionCodeEnumerationType().isInstance(transActionCodeType)) {
				if (validateTransActionCodeEnumerationType((TransActionCodeEnumerationType)transActionCodeType, tempDiagnostics, context)) return true;
			}
			if (XMLTypePackage.Literals.NORMALIZED_STRING.isInstance(transActionCodeType)) {
				if (xmlTypeValidator.validateNormalizedString((String)transActionCodeType, tempDiagnostics, context)) return true;
			}
			for (Diagnostic diagnostic : tempDiagnostics.getChildren()) {
				diagnostics.add(diagnostic);
			}
		}
		else {
			if (B2MMLPackage.eINSTANCE.getTransActionCodeEnumerationType().isInstance(transActionCodeType)) {
				if (validateTransActionCodeEnumerationType((TransActionCodeEnumerationType)transActionCodeType, null, context)) return true;
			}
			if (XMLTypePackage.Literals.NORMALIZED_STRING.isInstance(transActionCodeType)) {
				if (xmlTypeValidator.validateNormalizedString((String)transActionCodeType, null, context)) return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransResponseCodeTypeObject(TransResponseCodeType transResponseCodeTypeObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWorkType1TypeBaseObject(WorkType1TypeBase workType1TypeBaseObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //B2MMLValidator
