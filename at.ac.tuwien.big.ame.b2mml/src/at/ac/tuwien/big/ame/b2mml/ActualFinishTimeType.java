/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actual Finish Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getActualFinishTimeType()
 * @model extendedMetaData="name='ActualFinishTimeType' kind='simple'"
 * @generated
 */
public interface ActualFinishTimeType extends DateTimeType {
} // ActualFinishTimeType
