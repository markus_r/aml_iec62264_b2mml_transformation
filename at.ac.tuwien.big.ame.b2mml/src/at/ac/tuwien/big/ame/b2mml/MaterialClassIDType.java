/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Class ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialClassIDType()
 * @model extendedMetaData="name='MaterialClassIDType' kind='simple'"
 * @generated
 */
public interface MaterialClassIDType extends IdentifierType {
} // MaterialClassIDType
