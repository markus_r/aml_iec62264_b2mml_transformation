/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quantity Value Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getQuantityString <em>Quantity String</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getDataType <em>Data Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getUnitOfMeasure <em>Unit Of Measure</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getKey <em>Key</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQuantityValueType()
 * @model extendedMetaData="name='QuantityValueType' kind='elementOnly'"
 * @generated
 */
public interface QuantityValueType extends EObject {
	/**
	 * Returns the value of the '<em><b>Quantity String</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity String</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity String</em>' containment reference.
	 * @see #isSetQuantityString()
	 * @see #unsetQuantityString()
	 * @see #setQuantityString(QuantityStringType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQuantityValueType_QuantityString()
	 * @model containment="true" unsettable="true" required="true"
	 *        extendedMetaData="kind='element' name='QuantityString' namespace='##targetNamespace'"
	 * @generated
	 */
	QuantityStringType getQuantityString();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getQuantityString <em>Quantity String</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity String</em>' containment reference.
	 * @see #isSetQuantityString()
	 * @see #unsetQuantityString()
	 * @see #getQuantityString()
	 * @generated
	 */
	void setQuantityString(QuantityStringType value);

	/**
	 * Unsets the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getQuantityString <em>Quantity String</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetQuantityString()
	 * @see #getQuantityString()
	 * @see #setQuantityString(QuantityStringType)
	 * @generated
	 */
	void unsetQuantityString();

	/**
	 * Returns whether the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getQuantityString <em>Quantity String</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Quantity String</em>' containment reference is set.
	 * @see #unsetQuantityString()
	 * @see #getQuantityString()
	 * @see #setQuantityString(QuantityStringType)
	 * @generated
	 */
	boolean isSetQuantityString();

	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' containment reference.
	 * @see #isSetDataType()
	 * @see #unsetDataType()
	 * @see #setDataType(DataTypeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQuantityValueType_DataType()
	 * @model containment="true" unsettable="true"
	 *        extendedMetaData="kind='element' name='DataType' namespace='##targetNamespace'"
	 * @generated
	 */
	DataTypeType getDataType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getDataType <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' containment reference.
	 * @see #isSetDataType()
	 * @see #unsetDataType()
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(DataTypeType value);

	/**
	 * Unsets the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getDataType <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDataType()
	 * @see #getDataType()
	 * @see #setDataType(DataTypeType)
	 * @generated
	 */
	void unsetDataType();

	/**
	 * Returns whether the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getDataType <em>Data Type</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Data Type</em>' containment reference is set.
	 * @see #unsetDataType()
	 * @see #getDataType()
	 * @see #setDataType(DataTypeType)
	 * @generated
	 */
	boolean isSetDataType();

	/**
	 * Returns the value of the '<em><b>Unit Of Measure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Of Measure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Of Measure</em>' containment reference.
	 * @see #isSetUnitOfMeasure()
	 * @see #unsetUnitOfMeasure()
	 * @see #setUnitOfMeasure(UnitOfMeasureType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQuantityValueType_UnitOfMeasure()
	 * @model containment="true" unsettable="true"
	 *        extendedMetaData="kind='element' name='UnitOfMeasure' namespace='##targetNamespace'"
	 * @generated
	 */
	UnitOfMeasureType getUnitOfMeasure();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getUnitOfMeasure <em>Unit Of Measure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Of Measure</em>' containment reference.
	 * @see #isSetUnitOfMeasure()
	 * @see #unsetUnitOfMeasure()
	 * @see #getUnitOfMeasure()
	 * @generated
	 */
	void setUnitOfMeasure(UnitOfMeasureType value);

	/**
	 * Unsets the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getUnitOfMeasure <em>Unit Of Measure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUnitOfMeasure()
	 * @see #getUnitOfMeasure()
	 * @see #setUnitOfMeasure(UnitOfMeasureType)
	 * @generated
	 */
	void unsetUnitOfMeasure();

	/**
	 * Returns whether the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getUnitOfMeasure <em>Unit Of Measure</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Unit Of Measure</em>' containment reference is set.
	 * @see #unsetUnitOfMeasure()
	 * @see #getUnitOfMeasure()
	 * @see #setUnitOfMeasure(UnitOfMeasureType)
	 * @generated
	 */
	boolean isSetUnitOfMeasure();

	/**
	 * Returns the value of the '<em><b>Key</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' containment reference.
	 * @see #setKey(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQuantityValueType_Key()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Key' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getKey();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityValueType#getKey <em>Key</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' containment reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(IdentifierType value);

} // QuantityValueType
