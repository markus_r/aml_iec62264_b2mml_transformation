/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Personnel Class ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPersonnelClassIDType()
 * @model extendedMetaData="name='PersonnelClassIDType' kind='simple'"
 * @generated
 */
public interface PersonnelClassIDType extends IdentifierType {
} // PersonnelClassIDType
