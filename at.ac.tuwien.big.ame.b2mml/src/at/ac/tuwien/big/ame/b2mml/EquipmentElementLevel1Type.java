/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Element Level1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEquipmentElementLevel1Type()
 * @model extendedMetaData="name='EquipmentElementLevel1Type' kind='simple'"
 * @generated
 */
public interface EquipmentElementLevel1Type extends CodeType {
} // EquipmentElementLevel1Type
