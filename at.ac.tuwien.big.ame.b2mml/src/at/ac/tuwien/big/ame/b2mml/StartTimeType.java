/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Start Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getStartTimeType()
 * @model extendedMetaData="name='StartTimeType' kind='simple'"
 * @generated
 */
public interface StartTimeType extends DateTimeType {
} // StartTimeType
