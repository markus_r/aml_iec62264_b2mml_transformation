/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getPropertyIDType()
 * @model extendedMetaData="name='PropertyIDType' kind='simple'"
 * @generated
 */
public interface PropertyIDType extends IdentifierType {
} // PropertyIDType
