/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>End Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getEndTimeType()
 * @model extendedMetaData="name='EndTimeType' kind='simple'"
 * @generated
 */
public interface EndTimeType extends DateTimeType {
} // EndTimeType
