/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Get Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransGetType#getExpression <em>Expression</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransGetType()
 * @model extendedMetaData="name='TransGetType' kind='elementOnly'"
 * @generated
 */
public interface TransGetType extends EObject {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' attribute list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransGetType_Expression()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.Token"
	 *        extendedMetaData="kind='element' name='Expression' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<String> getExpression();

} // TransGetType
