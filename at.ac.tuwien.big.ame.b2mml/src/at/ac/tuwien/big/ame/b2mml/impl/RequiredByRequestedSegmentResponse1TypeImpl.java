/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.RequiredByRequestedSegmentResponse1Type;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Required By Requested Segment Response1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequiredByRequestedSegmentResponse1TypeImpl extends CodeTypeImpl implements RequiredByRequestedSegmentResponse1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequiredByRequestedSegmentResponse1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getRequiredByRequestedSegmentResponse1Type();
	}

} //RequiredByRequestedSegmentResponse1TypeImpl
