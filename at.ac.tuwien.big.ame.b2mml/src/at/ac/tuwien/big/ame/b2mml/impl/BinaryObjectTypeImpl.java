/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.BinaryObjectType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Object Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.BinaryObjectTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.BinaryObjectTypeImpl#getCharacterSetCode <em>Character Set Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.BinaryObjectTypeImpl#getEncodingCode <em>Encoding Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.BinaryObjectTypeImpl#getFilename <em>Filename</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.BinaryObjectTypeImpl#getFormat <em>Format</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.BinaryObjectTypeImpl#getMimeCode <em>Mime Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.BinaryObjectTypeImpl#getUri <em>Uri</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BinaryObjectTypeImpl extends MinimalEObjectImpl.Container implements BinaryObjectType {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected byte[] value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCharacterSetCode() <em>Character Set Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacterSetCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CHARACTER_SET_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCharacterSetCode() <em>Character Set Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacterSetCode()
	 * @generated
	 * @ordered
	 */
	protected String characterSetCode = CHARACTER_SET_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getEncodingCode() <em>Encoding Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEncodingCode()
	 * @generated
	 * @ordered
	 */
	protected static final String ENCODING_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEncodingCode() <em>Encoding Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEncodingCode()
	 * @generated
	 * @ordered
	 */
	protected String encodingCode = ENCODING_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFilename() <em>Filename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilename()
	 * @generated
	 * @ordered
	 */
	protected static final String FILENAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFilename() <em>Filename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilename()
	 * @generated
	 * @ordered
	 */
	protected String filename = FILENAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getFormat() <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormat()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMAT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFormat() <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormat()
	 * @generated
	 * @ordered
	 */
	protected String format = FORMAT_EDEFAULT;

	/**
	 * The default value of the '{@link #getMimeCode() <em>Mime Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMimeCode()
	 * @generated
	 * @ordered
	 */
	protected static final String MIME_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMimeCode() <em>Mime Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMimeCode()
	 * @generated
	 * @ordered
	 */
	protected String mimeCode = MIME_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUri() <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUri()
	 * @generated
	 * @ordered
	 */
	protected static final String URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUri() <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUri()
	 * @generated
	 * @ordered
	 */
	protected String uri = URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryObjectTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getBinaryObjectType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public byte[] getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(byte[] newValue) {
		byte[] oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.BINARY_OBJECT_TYPE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCharacterSetCode() {
		return characterSetCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCharacterSetCode(String newCharacterSetCode) {
		String oldCharacterSetCode = characterSetCode;
		characterSetCode = newCharacterSetCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.BINARY_OBJECT_TYPE__CHARACTER_SET_CODE, oldCharacterSetCode, characterSetCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEncodingCode() {
		return encodingCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEncodingCode(String newEncodingCode) {
		String oldEncodingCode = encodingCode;
		encodingCode = newEncodingCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.BINARY_OBJECT_TYPE__ENCODING_CODE, oldEncodingCode, encodingCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilename(String newFilename) {
		String oldFilename = filename;
		filename = newFilename;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.BINARY_OBJECT_TYPE__FILENAME, oldFilename, filename));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormat(String newFormat) {
		String oldFormat = format;
		format = newFormat;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.BINARY_OBJECT_TYPE__FORMAT, oldFormat, format));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMimeCode() {
		return mimeCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMimeCode(String newMimeCode) {
		String oldMimeCode = mimeCode;
		mimeCode = newMimeCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.BINARY_OBJECT_TYPE__MIME_CODE, oldMimeCode, mimeCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUri(String newUri) {
		String oldUri = uri;
		uri = newUri;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.BINARY_OBJECT_TYPE__URI, oldUri, uri));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.BINARY_OBJECT_TYPE__VALUE:
				return getValue();
			case B2MMLPackage.BINARY_OBJECT_TYPE__CHARACTER_SET_CODE:
				return getCharacterSetCode();
			case B2MMLPackage.BINARY_OBJECT_TYPE__ENCODING_CODE:
				return getEncodingCode();
			case B2MMLPackage.BINARY_OBJECT_TYPE__FILENAME:
				return getFilename();
			case B2MMLPackage.BINARY_OBJECT_TYPE__FORMAT:
				return getFormat();
			case B2MMLPackage.BINARY_OBJECT_TYPE__MIME_CODE:
				return getMimeCode();
			case B2MMLPackage.BINARY_OBJECT_TYPE__URI:
				return getUri();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.BINARY_OBJECT_TYPE__VALUE:
				setValue((byte[])newValue);
				return;
			case B2MMLPackage.BINARY_OBJECT_TYPE__CHARACTER_SET_CODE:
				setCharacterSetCode((String)newValue);
				return;
			case B2MMLPackage.BINARY_OBJECT_TYPE__ENCODING_CODE:
				setEncodingCode((String)newValue);
				return;
			case B2MMLPackage.BINARY_OBJECT_TYPE__FILENAME:
				setFilename((String)newValue);
				return;
			case B2MMLPackage.BINARY_OBJECT_TYPE__FORMAT:
				setFormat((String)newValue);
				return;
			case B2MMLPackage.BINARY_OBJECT_TYPE__MIME_CODE:
				setMimeCode((String)newValue);
				return;
			case B2MMLPackage.BINARY_OBJECT_TYPE__URI:
				setUri((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.BINARY_OBJECT_TYPE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case B2MMLPackage.BINARY_OBJECT_TYPE__CHARACTER_SET_CODE:
				setCharacterSetCode(CHARACTER_SET_CODE_EDEFAULT);
				return;
			case B2MMLPackage.BINARY_OBJECT_TYPE__ENCODING_CODE:
				setEncodingCode(ENCODING_CODE_EDEFAULT);
				return;
			case B2MMLPackage.BINARY_OBJECT_TYPE__FILENAME:
				setFilename(FILENAME_EDEFAULT);
				return;
			case B2MMLPackage.BINARY_OBJECT_TYPE__FORMAT:
				setFormat(FORMAT_EDEFAULT);
				return;
			case B2MMLPackage.BINARY_OBJECT_TYPE__MIME_CODE:
				setMimeCode(MIME_CODE_EDEFAULT);
				return;
			case B2MMLPackage.BINARY_OBJECT_TYPE__URI:
				setUri(URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.BINARY_OBJECT_TYPE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case B2MMLPackage.BINARY_OBJECT_TYPE__CHARACTER_SET_CODE:
				return CHARACTER_SET_CODE_EDEFAULT == null ? characterSetCode != null : !CHARACTER_SET_CODE_EDEFAULT.equals(characterSetCode);
			case B2MMLPackage.BINARY_OBJECT_TYPE__ENCODING_CODE:
				return ENCODING_CODE_EDEFAULT == null ? encodingCode != null : !ENCODING_CODE_EDEFAULT.equals(encodingCode);
			case B2MMLPackage.BINARY_OBJECT_TYPE__FILENAME:
				return FILENAME_EDEFAULT == null ? filename != null : !FILENAME_EDEFAULT.equals(filename);
			case B2MMLPackage.BINARY_OBJECT_TYPE__FORMAT:
				return FORMAT_EDEFAULT == null ? format != null : !FORMAT_EDEFAULT.equals(format);
			case B2MMLPackage.BINARY_OBJECT_TYPE__MIME_CODE:
				return MIME_CODE_EDEFAULT == null ? mimeCode != null : !MIME_CODE_EDEFAULT.equals(mimeCode);
			case B2MMLPackage.BINARY_OBJECT_TYPE__URI:
				return URI_EDEFAULT == null ? uri != null : !URI_EDEFAULT.equals(uri);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", characterSetCode: ");
		result.append(characterSetCode);
		result.append(", encodingCode: ");
		result.append(encodingCode);
		result.append(", filename: ");
		result.append(filename);
		result.append(", format: ");
		result.append(format);
		result.append(", mimeCode: ");
		result.append(mimeCode);
		result.append(", uri: ");
		result.append(uri);
		result.append(')');
		return result.toString();
	}

} //BinaryObjectTypeImpl
