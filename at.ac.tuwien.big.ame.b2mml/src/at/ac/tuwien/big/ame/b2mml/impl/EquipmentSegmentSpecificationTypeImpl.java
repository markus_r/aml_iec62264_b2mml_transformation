/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.CodeType;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.EquipmentClassIDType;
import at.ac.tuwien.big.ame.b2mml.EquipmentIDType;
import at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationPropertyType;
import at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType;
import at.ac.tuwien.big.ame.b2mml.QuantityValueType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Segment Specification Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentSegmentSpecificationTypeImpl#getEquipmentClassID <em>Equipment Class ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentSegmentSpecificationTypeImpl#getEquipmentID <em>Equipment ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentSegmentSpecificationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentSegmentSpecificationTypeImpl#getEquipmentUse <em>Equipment Use</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentSegmentSpecificationTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.EquipmentSegmentSpecificationTypeImpl#getEquipmentSegmentSpecificationProperty <em>Equipment Segment Specification Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EquipmentSegmentSpecificationTypeImpl extends MinimalEObjectImpl.Container implements EquipmentSegmentSpecificationType {
	/**
	 * The cached value of the '{@link #getEquipmentClassID() <em>Equipment Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentClassID()
	 * @generated
	 * @ordered
	 */
	protected EquipmentClassIDType equipmentClassID;

	/**
	 * The cached value of the '{@link #getEquipmentID() <em>Equipment ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentID()
	 * @generated
	 * @ordered
	 */
	protected EquipmentIDType equipmentID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getEquipmentUse() <em>Equipment Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentUse()
	 * @generated
	 * @ordered
	 */
	protected CodeType equipmentUse;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getEquipmentSegmentSpecificationProperty() <em>Equipment Segment Specification Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentSegmentSpecificationProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentSegmentSpecificationPropertyType> equipmentSegmentSpecificationProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentSegmentSpecificationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getEquipmentSegmentSpecificationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClassIDType getEquipmentClassID() {
		return equipmentClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentClassID(EquipmentClassIDType newEquipmentClassID, NotificationChain msgs) {
		EquipmentClassIDType oldEquipmentClassID = equipmentClassID;
		equipmentClassID = newEquipmentClassID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID, oldEquipmentClassID, newEquipmentClassID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentClassID(EquipmentClassIDType newEquipmentClassID) {
		if (newEquipmentClassID != equipmentClassID) {
			NotificationChain msgs = null;
			if (equipmentClassID != null)
				msgs = ((InternalEObject)equipmentClassID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID, null, msgs);
			if (newEquipmentClassID != null)
				msgs = ((InternalEObject)newEquipmentClassID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID, null, msgs);
			msgs = basicSetEquipmentClassID(newEquipmentClassID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID, newEquipmentClassID, newEquipmentClassID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentIDType getEquipmentID() {
		return equipmentID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentID(EquipmentIDType newEquipmentID, NotificationChain msgs) {
		EquipmentIDType oldEquipmentID = equipmentID;
		equipmentID = newEquipmentID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_ID, oldEquipmentID, newEquipmentID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentID(EquipmentIDType newEquipmentID) {
		if (newEquipmentID != equipmentID) {
			NotificationChain msgs = null;
			if (equipmentID != null)
				msgs = ((InternalEObject)equipmentID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_ID, null, msgs);
			if (newEquipmentID != null)
				msgs = ((InternalEObject)newEquipmentID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_ID, null, msgs);
			msgs = basicSetEquipmentID(newEquipmentID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_ID, newEquipmentID, newEquipmentID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeType getEquipmentUse() {
		return equipmentUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentUse(CodeType newEquipmentUse, NotificationChain msgs) {
		CodeType oldEquipmentUse = equipmentUse;
		equipmentUse = newEquipmentUse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_USE, oldEquipmentUse, newEquipmentUse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentUse(CodeType newEquipmentUse) {
		if (newEquipmentUse != equipmentUse) {
			NotificationChain msgs = null;
			if (equipmentUse != null)
				msgs = ((InternalEObject)equipmentUse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_USE, null, msgs);
			if (newEquipmentUse != null)
				msgs = ((InternalEObject)newEquipmentUse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_USE, null, msgs);
			msgs = basicSetEquipmentUse(newEquipmentUse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_USE, newEquipmentUse, newEquipmentUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentSegmentSpecificationPropertyType> getEquipmentSegmentSpecificationProperty() {
		if (equipmentSegmentSpecificationProperty == null) {
			equipmentSegmentSpecificationProperty = new EObjectContainmentEList<EquipmentSegmentSpecificationPropertyType>(EquipmentSegmentSpecificationPropertyType.class, this, B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY);
		}
		return equipmentSegmentSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID:
				return basicSetEquipmentClassID(null, msgs);
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_ID:
				return basicSetEquipmentID(null, msgs);
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_USE:
				return basicSetEquipmentUse(null, msgs);
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY:
				return ((InternalEList<?>)getEquipmentSegmentSpecificationProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID:
				return getEquipmentClassID();
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_ID:
				return getEquipmentID();
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_USE:
				return getEquipmentUse();
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY:
				return getEquipmentSegmentSpecificationProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID:
				setEquipmentClassID((EquipmentClassIDType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_ID:
				setEquipmentID((EquipmentIDType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_USE:
				setEquipmentUse((CodeType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY:
				getEquipmentSegmentSpecificationProperty().clear();
				getEquipmentSegmentSpecificationProperty().addAll((Collection<? extends EquipmentSegmentSpecificationPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID:
				setEquipmentClassID((EquipmentClassIDType)null);
				return;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_ID:
				setEquipmentID((EquipmentIDType)null);
				return;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_USE:
				setEquipmentUse((CodeType)null);
				return;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY:
				getEquipmentSegmentSpecificationProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID:
				return equipmentClassID != null;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_ID:
				return equipmentID != null;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_USE:
				return equipmentUse != null;
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE__EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY:
				return equipmentSegmentSpecificationProperty != null && !equipmentSegmentSpecificationProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EquipmentSegmentSpecificationTypeImpl
