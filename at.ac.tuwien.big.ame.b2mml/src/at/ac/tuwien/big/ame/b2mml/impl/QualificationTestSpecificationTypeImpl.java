/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.LocationType;
import at.ac.tuwien.big.ame.b2mml.QualificationTestSpecificationType;
import at.ac.tuwien.big.ame.b2mml.TestedPersonPropertyType;
import at.ac.tuwien.big.ame.b2mml.TestedPersonnelClassPropertyType;
import at.ac.tuwien.big.ame.b2mml.VersionType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Qualification Test Specification Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.QualificationTestSpecificationTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.QualificationTestSpecificationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.QualificationTestSpecificationTypeImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.QualificationTestSpecificationTypeImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.QualificationTestSpecificationTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.QualificationTestSpecificationTypeImpl#getTestedPersonProperty <em>Tested Person Property</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.QualificationTestSpecificationTypeImpl#getTestedPersonnelClassProperty <em>Tested Personnel Class Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class QualificationTestSpecificationTypeImpl extends MinimalEObjectImpl.Container implements QualificationTestSpecificationType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected VersionType version;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected LocationType location;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getTestedPersonProperty() <em>Tested Person Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestedPersonProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<TestedPersonPropertyType> testedPersonProperty;

	/**
	 * The cached value of the '{@link #getTestedPersonnelClassProperty() <em>Tested Personnel Class Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestedPersonnelClassProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<TestedPersonnelClassPropertyType> testedPersonnelClassProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QualificationTestSpecificationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getQualificationTestSpecificationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VersionType getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVersion(VersionType newVersion, NotificationChain msgs) {
		VersionType oldVersion = version;
		version = newVersion;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__VERSION, oldVersion, newVersion);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(VersionType newVersion) {
		if (newVersion != version) {
			NotificationChain msgs = null;
			if (version != null)
				msgs = ((InternalEObject)version).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__VERSION, null, msgs);
			if (newVersion != null)
				msgs = ((InternalEObject)newVersion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__VERSION, null, msgs);
			msgs = basicSetVersion(newVersion, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__VERSION, newVersion, newVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationType getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocation(LocationType newLocation, NotificationChain msgs) {
		LocationType oldLocation = location;
		location = newLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__LOCATION, oldLocation, newLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(LocationType newLocation) {
		if (newLocation != location) {
			NotificationChain msgs = null;
			if (location != null)
				msgs = ((InternalEObject)location).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__LOCATION, null, msgs);
			if (newLocation != null)
				msgs = ((InternalEObject)newLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__LOCATION, null, msgs);
			msgs = basicSetLocation(newLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__LOCATION, newLocation, newLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestedPersonPropertyType> getTestedPersonProperty() {
		if (testedPersonProperty == null) {
			testedPersonProperty = new EObjectContainmentEList<TestedPersonPropertyType>(TestedPersonPropertyType.class, this, B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__TESTED_PERSON_PROPERTY);
		}
		return testedPersonProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestedPersonnelClassPropertyType> getTestedPersonnelClassProperty() {
		if (testedPersonnelClassProperty == null) {
			testedPersonnelClassProperty = new EObjectContainmentEList<TestedPersonnelClassPropertyType>(TestedPersonnelClassPropertyType.class, this, B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__TESTED_PERSONNEL_CLASS_PROPERTY);
		}
		return testedPersonnelClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__VERSION:
				return basicSetVersion(null, msgs);
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__LOCATION:
				return basicSetLocation(null, msgs);
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__TESTED_PERSON_PROPERTY:
				return ((InternalEList<?>)getTestedPersonProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__TESTED_PERSONNEL_CLASS_PROPERTY:
				return ((InternalEList<?>)getTestedPersonnelClassProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__ID:
				return getID();
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__VERSION:
				return getVersion();
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__LOCATION:
				return getLocation();
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__TESTED_PERSON_PROPERTY:
				return getTestedPersonProperty();
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__TESTED_PERSONNEL_CLASS_PROPERTY:
				return getTestedPersonnelClassProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__VERSION:
				setVersion((VersionType)newValue);
				return;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__LOCATION:
				setLocation((LocationType)newValue);
				return;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__TESTED_PERSON_PROPERTY:
				getTestedPersonProperty().clear();
				getTestedPersonProperty().addAll((Collection<? extends TestedPersonPropertyType>)newValue);
				return;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__TESTED_PERSONNEL_CLASS_PROPERTY:
				getTestedPersonnelClassProperty().clear();
				getTestedPersonnelClassProperty().addAll((Collection<? extends TestedPersonnelClassPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__VERSION:
				setVersion((VersionType)null);
				return;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__LOCATION:
				setLocation((LocationType)null);
				return;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__TESTED_PERSON_PROPERTY:
				getTestedPersonProperty().clear();
				return;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__TESTED_PERSONNEL_CLASS_PROPERTY:
				getTestedPersonnelClassProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__ID:
				return iD != null;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__VERSION:
				return version != null;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__LOCATION:
				return location != null;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__TESTED_PERSON_PROPERTY:
				return testedPersonProperty != null && !testedPersonProperty.isEmpty();
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE__TESTED_PERSONNEL_CLASS_PROPERTY:
				return testedPersonnelClassProperty != null && !testedPersonnelClassProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //QualificationTestSpecificationTypeImpl
