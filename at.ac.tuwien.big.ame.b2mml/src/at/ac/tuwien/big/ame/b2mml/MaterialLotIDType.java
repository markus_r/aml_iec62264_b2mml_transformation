/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Lot ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getMaterialLotIDType()
 * @model extendedMetaData="name='MaterialLotIDType' kind='simple'"
 * @generated
 */
public interface MaterialLotIDType extends IdentifierType {
} // MaterialLotIDType
