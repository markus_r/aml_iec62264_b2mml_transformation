/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requested Completion Date Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getRequestedCompletionDateType()
 * @model extendedMetaData="name='RequestedCompletionDateType' kind='simple'"
 * @generated
 */
public interface RequestedCompletionDateType extends DateTimeType {
} // RequestedCompletionDateType
