/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Capability Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getReason <em>Reason</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getPersonnelCapability <em>Personnel Capability</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getEquipmentCapability <em>Equipment Capability</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getPhysicalAssetCapability <em>Physical Asset Capability</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getMaterialCapability <em>Material Capability</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getProcessSegmentCapability <em>Process Segment Capability</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType()
 * @model extendedMetaData="name='OperationsCapabilityType' kind='elementOnly'"
 * @generated
 */
public interface OperationsCapabilityType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_ID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Capability Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capability Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capability Type</em>' containment reference.
	 * @see #setCapabilityType(CapabilityTypeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_CapabilityType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='CapabilityType' namespace='##targetNamespace'"
	 * @generated
	 */
	CapabilityTypeType getCapabilityType();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getCapabilityType <em>Capability Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Capability Type</em>' containment reference.
	 * @see #getCapabilityType()
	 * @generated
	 */
	void setCapabilityType(CapabilityTypeType value);

	/**
	 * Returns the value of the '<em><b>Reason</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reason</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reason</em>' containment reference.
	 * @see #setReason(ReasonType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_Reason()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Reason' namespace='##targetNamespace'"
	 * @generated
	 */
	ReasonType getReason();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getReason <em>Reason</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reason</em>' containment reference.
	 * @see #getReason()
	 * @generated
	 */
	void setReason(ReasonType value);

	/**
	 * Returns the value of the '<em><b>Confidence Factor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Confidence Factor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confidence Factor</em>' containment reference.
	 * @see #setConfidenceFactor(ConfidenceFactorType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_ConfidenceFactor()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ConfidenceFactor' namespace='##targetNamespace'"
	 * @generated
	 */
	ConfidenceFactorType getConfidenceFactor();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getConfidenceFactor <em>Confidence Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confidence Factor</em>' containment reference.
	 * @see #getConfidenceFactor()
	 * @generated
	 */
	void setConfidenceFactor(ConfidenceFactorType value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' containment reference.
	 * @see #setStartTime(StartTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_StartTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='StartTime' namespace='##targetNamespace'"
	 * @generated
	 */
	StartTimeType getStartTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getStartTime <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' containment reference.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(StartTimeType value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' containment reference.
	 * @see #setEndTime(EndTimeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_EndTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EndTime' namespace='##targetNamespace'"
	 * @generated
	 */
	EndTimeType getEndTime();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getEndTime <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' containment reference.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(EndTimeType value);

	/**
	 * Returns the value of the '<em><b>Published Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Published Date</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Published Date</em>' containment reference.
	 * @see #setPublishedDate(PublishedDateType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_PublishedDate()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PublishedDate' namespace='##targetNamespace'"
	 * @generated
	 */
	PublishedDateType getPublishedDate();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType#getPublishedDate <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Published Date</em>' containment reference.
	 * @see #getPublishedDate()
	 * @generated
	 */
	void setPublishedDate(PublishedDateType value);

	/**
	 * Returns the value of the '<em><b>Personnel Capability</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpPersonnelCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Capability</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_PersonnelCapability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonnelCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPersonnelCapabilityType> getPersonnelCapability();

	/**
	 * Returns the value of the '<em><b>Equipment Capability</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpEquipmentCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Capability</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_EquipmentCapability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpEquipmentCapabilityType> getEquipmentCapability();

	/**
	 * Returns the value of the '<em><b>Physical Asset Capability</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpPhysicalAssetCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Capability</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_PhysicalAssetCapability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPhysicalAssetCapabilityType> getPhysicalAssetCapability();

	/**
	 * Returns the value of the '<em><b>Material Capability</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpMaterialCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Capability</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_MaterialCapability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpMaterialCapabilityType> getMaterialCapability();

	/**
	 * Returns the value of the '<em><b>Process Segment Capability</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OpProcessSegmentCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment Capability</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityType_ProcessSegmentCapability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ProcessSegmentCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpProcessSegmentCapabilityType> getProcessSegmentCapability();

} // OperationsCapabilityType
