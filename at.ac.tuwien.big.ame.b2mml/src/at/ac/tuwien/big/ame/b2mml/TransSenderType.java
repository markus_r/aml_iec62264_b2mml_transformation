/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Sender Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransSenderType#getLogicalID <em>Logical ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransSenderType#getComponentID <em>Component ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransSenderType#getTaskID <em>Task ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransSenderType#getReferenceID <em>Reference ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransSenderType#getConfirmationCode <em>Confirmation Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TransSenderType#getAuthorizationID <em>Authorization ID</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransSenderType()
 * @model extendedMetaData="name='TransSenderType' kind='elementOnly'"
 * @generated
 */
public interface TransSenderType extends EObject {
	/**
	 * Returns the value of the '<em><b>Logical ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logical ID</em>' containment reference.
	 * @see #setLogicalID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransSenderType_LogicalID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LogicalID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getLogicalID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransSenderType#getLogicalID <em>Logical ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Logical ID</em>' containment reference.
	 * @see #getLogicalID()
	 * @generated
	 */
	void setLogicalID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Component ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component ID</em>' containment reference.
	 * @see #setComponentID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransSenderType_ComponentID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ComponentID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getComponentID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransSenderType#getComponentID <em>Component ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component ID</em>' containment reference.
	 * @see #getComponentID()
	 * @generated
	 */
	void setComponentID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Task ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task ID</em>' containment reference.
	 * @see #setTaskID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransSenderType_TaskID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TaskID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getTaskID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransSenderType#getTaskID <em>Task ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task ID</em>' containment reference.
	 * @see #getTaskID()
	 * @generated
	 */
	void setTaskID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Reference ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference ID</em>' containment reference.
	 * @see #setReferenceID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransSenderType_ReferenceID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ReferenceID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getReferenceID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransSenderType#getReferenceID <em>Reference ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference ID</em>' containment reference.
	 * @see #getReferenceID()
	 * @generated
	 */
	void setReferenceID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Confirmation Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Confirmation Code</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confirmation Code</em>' containment reference.
	 * @see #setConfirmationCode(TransConfirmationCodeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransSenderType_ConfirmationCode()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ConfirmationCode' namespace='##targetNamespace'"
	 * @generated
	 */
	TransConfirmationCodeType getConfirmationCode();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransSenderType#getConfirmationCode <em>Confirmation Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confirmation Code</em>' containment reference.
	 * @see #getConfirmationCode()
	 * @generated
	 */
	void setConfirmationCode(TransConfirmationCodeType value);

	/**
	 * Returns the value of the '<em><b>Authorization ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Authorization ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Authorization ID</em>' containment reference.
	 * @see #setAuthorizationID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTransSenderType_AuthorizationID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AuthorizationID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getAuthorizationID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TransSenderType#getAuthorizationID <em>Authorization ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Authorization ID</em>' containment reference.
	 * @see #getAuthorizationID()
	 * @generated
	 */
	void setAuthorizationID(IdentifierType value);

} // TransSenderType
