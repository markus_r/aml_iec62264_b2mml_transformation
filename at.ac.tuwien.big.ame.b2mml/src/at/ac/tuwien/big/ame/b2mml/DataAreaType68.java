/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type68</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType68#getSync <em>Sync</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.DataAreaType68#getOperationsDefinition <em>Operations Definition</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType68()
 * @model extendedMetaData="name='DataArea_._69_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType68 extends EObject {
	/**
	 * Returns the value of the '<em><b>Sync</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync</em>' containment reference.
	 * @see #setSync(TransSyncType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType68_Sync()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Sync' namespace='##targetNamespace'"
	 * @generated
	 */
	TransSyncType getSync();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.DataAreaType68#getSync <em>Sync</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync</em>' containment reference.
	 * @see #getSync()
	 * @generated
	 */
	void setSync(TransSyncType value);

	/**
	 * Returns the value of the '<em><b>Operations Definition</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OperationsDefinitionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getDataAreaType68_OperationsDefinition()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OperationsDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsDefinitionType> getOperationsDefinition();

} // DataAreaType68
