/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Status Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getStatusTimeType()
 * @model extendedMetaData="name='StatusTimeType' kind='simple'"
 * @generated
 */
public interface StatusTimeType extends DateTimeType {
} // StatusTimeType
