/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.BODType;
import at.ac.tuwien.big.ame.b2mml.TextType;
import at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType;
import at.ac.tuwien.big.ame.b2mml.TransUserAreaType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BOD Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.BODTypeImpl#getOriginalApplicationArea <em>Original Application Area</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.BODTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.BODTypeImpl#getNote <em>Note</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.BODTypeImpl#getUserArea <em>User Area</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BODTypeImpl extends MinimalEObjectImpl.Container implements BODType {
	/**
	 * The cached value of the '{@link #getOriginalApplicationArea() <em>Original Application Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginalApplicationArea()
	 * @generated
	 * @ordered
	 */
	protected TransApplicationAreaType originalApplicationArea;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<TextType> description;

	/**
	 * The cached value of the '{@link #getNote() <em>Note</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNote()
	 * @generated
	 * @ordered
	 */
	protected EList<TextType> note;

	/**
	 * The cached value of the '{@link #getUserArea() <em>User Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserArea()
	 * @generated
	 * @ordered
	 */
	protected TransUserAreaType userArea;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BODTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getBODType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransApplicationAreaType getOriginalApplicationArea() {
		return originalApplicationArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOriginalApplicationArea(TransApplicationAreaType newOriginalApplicationArea, NotificationChain msgs) {
		TransApplicationAreaType oldOriginalApplicationArea = originalApplicationArea;
		originalApplicationArea = newOriginalApplicationArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.BOD_TYPE__ORIGINAL_APPLICATION_AREA, oldOriginalApplicationArea, newOriginalApplicationArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOriginalApplicationArea(TransApplicationAreaType newOriginalApplicationArea) {
		if (newOriginalApplicationArea != originalApplicationArea) {
			NotificationChain msgs = null;
			if (originalApplicationArea != null)
				msgs = ((InternalEObject)originalApplicationArea).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.BOD_TYPE__ORIGINAL_APPLICATION_AREA, null, msgs);
			if (newOriginalApplicationArea != null)
				msgs = ((InternalEObject)newOriginalApplicationArea).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.BOD_TYPE__ORIGINAL_APPLICATION_AREA, null, msgs);
			msgs = basicSetOriginalApplicationArea(newOriginalApplicationArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.BOD_TYPE__ORIGINAL_APPLICATION_AREA, newOriginalApplicationArea, newOriginalApplicationArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TextType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<TextType>(TextType.class, this, B2MMLPackage.BOD_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TextType> getNote() {
		if (note == null) {
			note = new EObjectContainmentEList<TextType>(TextType.class, this, B2MMLPackage.BOD_TYPE__NOTE);
		}
		return note;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransUserAreaType getUserArea() {
		return userArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUserArea(TransUserAreaType newUserArea, NotificationChain msgs) {
		TransUserAreaType oldUserArea = userArea;
		userArea = newUserArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.BOD_TYPE__USER_AREA, oldUserArea, newUserArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUserArea(TransUserAreaType newUserArea) {
		if (newUserArea != userArea) {
			NotificationChain msgs = null;
			if (userArea != null)
				msgs = ((InternalEObject)userArea).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.BOD_TYPE__USER_AREA, null, msgs);
			if (newUserArea != null)
				msgs = ((InternalEObject)newUserArea).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.BOD_TYPE__USER_AREA, null, msgs);
			msgs = basicSetUserArea(newUserArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.BOD_TYPE__USER_AREA, newUserArea, newUserArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.BOD_TYPE__ORIGINAL_APPLICATION_AREA:
				return basicSetOriginalApplicationArea(null, msgs);
			case B2MMLPackage.BOD_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.BOD_TYPE__NOTE:
				return ((InternalEList<?>)getNote()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.BOD_TYPE__USER_AREA:
				return basicSetUserArea(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.BOD_TYPE__ORIGINAL_APPLICATION_AREA:
				return getOriginalApplicationArea();
			case B2MMLPackage.BOD_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.BOD_TYPE__NOTE:
				return getNote();
			case B2MMLPackage.BOD_TYPE__USER_AREA:
				return getUserArea();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.BOD_TYPE__ORIGINAL_APPLICATION_AREA:
				setOriginalApplicationArea((TransApplicationAreaType)newValue);
				return;
			case B2MMLPackage.BOD_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends TextType>)newValue);
				return;
			case B2MMLPackage.BOD_TYPE__NOTE:
				getNote().clear();
				getNote().addAll((Collection<? extends TextType>)newValue);
				return;
			case B2MMLPackage.BOD_TYPE__USER_AREA:
				setUserArea((TransUserAreaType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.BOD_TYPE__ORIGINAL_APPLICATION_AREA:
				setOriginalApplicationArea((TransApplicationAreaType)null);
				return;
			case B2MMLPackage.BOD_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.BOD_TYPE__NOTE:
				getNote().clear();
				return;
			case B2MMLPackage.BOD_TYPE__USER_AREA:
				setUserArea((TransUserAreaType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.BOD_TYPE__ORIGINAL_APPLICATION_AREA:
				return originalApplicationArea != null;
			case B2MMLPackage.BOD_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.BOD_TYPE__NOTE:
				return note != null && !note.isEmpty();
			case B2MMLPackage.BOD_TYPE__USER_AREA:
				return userArea != null;
		}
		return super.eIsSet(featureID);
	}

} //BODTypeImpl
