/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Product Production Rule ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getProductProductionRuleIDType()
 * @model extendedMetaData="name='ProductProductionRuleIDType' kind='simple'"
 * @generated
 */
public interface ProductProductionRuleIDType extends IdentifierType {
} // ProductProductionRuleIDType
