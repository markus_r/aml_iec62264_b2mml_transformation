/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DateTimeType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.TransApplicationAreaType;
import at.ac.tuwien.big.ame.b2mml.TransReceiverType;
import at.ac.tuwien.big.ame.b2mml.TransSenderType;
import at.ac.tuwien.big.ame.b2mml.TransSignatureType;
import at.ac.tuwien.big.ame.b2mml.TransUserAreaType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trans Application Area Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransApplicationAreaTypeImpl#getSender <em>Sender</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransApplicationAreaTypeImpl#getReceiver <em>Receiver</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransApplicationAreaTypeImpl#getCreationDateTime <em>Creation Date Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransApplicationAreaTypeImpl#getSignature <em>Signature</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransApplicationAreaTypeImpl#getBODID <em>BODID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.TransApplicationAreaTypeImpl#getUserArea <em>User Area</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransApplicationAreaTypeImpl extends MinimalEObjectImpl.Container implements TransApplicationAreaType {
	/**
	 * The cached value of the '{@link #getSender() <em>Sender</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSender()
	 * @generated
	 * @ordered
	 */
	protected TransSenderType sender;

	/**
	 * The cached value of the '{@link #getReceiver() <em>Receiver</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceiver()
	 * @generated
	 * @ordered
	 */
	protected EList<TransReceiverType> receiver;

	/**
	 * The cached value of the '{@link #getCreationDateTime() <em>Creation Date Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreationDateTime()
	 * @generated
	 * @ordered
	 */
	protected DateTimeType creationDateTime;

	/**
	 * The cached value of the '{@link #getSignature() <em>Signature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignature()
	 * @generated
	 * @ordered
	 */
	protected TransSignatureType signature;

	/**
	 * The cached value of the '{@link #getBODID() <em>BODID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBODID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType bODID;

	/**
	 * The cached value of the '{@link #getUserArea() <em>User Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserArea()
	 * @generated
	 * @ordered
	 */
	protected TransUserAreaType userArea;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransApplicationAreaTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTransApplicationAreaType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransSenderType getSender() {
		return sender;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSender(TransSenderType newSender, NotificationChain msgs) {
		TransSenderType oldSender = sender;
		sender = newSender;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SENDER, oldSender, newSender);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSender(TransSenderType newSender) {
		if (newSender != sender) {
			NotificationChain msgs = null;
			if (sender != null)
				msgs = ((InternalEObject)sender).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SENDER, null, msgs);
			if (newSender != null)
				msgs = ((InternalEObject)newSender).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SENDER, null, msgs);
			msgs = basicSetSender(newSender, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SENDER, newSender, newSender));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TransReceiverType> getReceiver() {
		if (receiver == null) {
			receiver = new EObjectContainmentEList<TransReceiverType>(TransReceiverType.class, this, B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__RECEIVER);
		}
		return receiver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTimeType getCreationDateTime() {
		return creationDateTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCreationDateTime(DateTimeType newCreationDateTime, NotificationChain msgs) {
		DateTimeType oldCreationDateTime = creationDateTime;
		creationDateTime = newCreationDateTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__CREATION_DATE_TIME, oldCreationDateTime, newCreationDateTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreationDateTime(DateTimeType newCreationDateTime) {
		if (newCreationDateTime != creationDateTime) {
			NotificationChain msgs = null;
			if (creationDateTime != null)
				msgs = ((InternalEObject)creationDateTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__CREATION_DATE_TIME, null, msgs);
			if (newCreationDateTime != null)
				msgs = ((InternalEObject)newCreationDateTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__CREATION_DATE_TIME, null, msgs);
			msgs = basicSetCreationDateTime(newCreationDateTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__CREATION_DATE_TIME, newCreationDateTime, newCreationDateTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransSignatureType getSignature() {
		return signature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSignature(TransSignatureType newSignature, NotificationChain msgs) {
		TransSignatureType oldSignature = signature;
		signature = newSignature;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SIGNATURE, oldSignature, newSignature);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignature(TransSignatureType newSignature) {
		if (newSignature != signature) {
			NotificationChain msgs = null;
			if (signature != null)
				msgs = ((InternalEObject)signature).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SIGNATURE, null, msgs);
			if (newSignature != null)
				msgs = ((InternalEObject)newSignature).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SIGNATURE, null, msgs);
			msgs = basicSetSignature(newSignature, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SIGNATURE, newSignature, newSignature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getBODID() {
		return bODID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBODID(IdentifierType newBODID, NotificationChain msgs) {
		IdentifierType oldBODID = bODID;
		bODID = newBODID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__BODID, oldBODID, newBODID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBODID(IdentifierType newBODID) {
		if (newBODID != bODID) {
			NotificationChain msgs = null;
			if (bODID != null)
				msgs = ((InternalEObject)bODID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__BODID, null, msgs);
			if (newBODID != null)
				msgs = ((InternalEObject)newBODID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__BODID, null, msgs);
			msgs = basicSetBODID(newBODID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__BODID, newBODID, newBODID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransUserAreaType getUserArea() {
		return userArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUserArea(TransUserAreaType newUserArea, NotificationChain msgs) {
		TransUserAreaType oldUserArea = userArea;
		userArea = newUserArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__USER_AREA, oldUserArea, newUserArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUserArea(TransUserAreaType newUserArea) {
		if (newUserArea != userArea) {
			NotificationChain msgs = null;
			if (userArea != null)
				msgs = ((InternalEObject)userArea).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__USER_AREA, null, msgs);
			if (newUserArea != null)
				msgs = ((InternalEObject)newUserArea).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__USER_AREA, null, msgs);
			msgs = basicSetUserArea(newUserArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__USER_AREA, newUserArea, newUserArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SENDER:
				return basicSetSender(null, msgs);
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__RECEIVER:
				return ((InternalEList<?>)getReceiver()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__CREATION_DATE_TIME:
				return basicSetCreationDateTime(null, msgs);
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SIGNATURE:
				return basicSetSignature(null, msgs);
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__BODID:
				return basicSetBODID(null, msgs);
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__USER_AREA:
				return basicSetUserArea(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SENDER:
				return getSender();
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__RECEIVER:
				return getReceiver();
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__CREATION_DATE_TIME:
				return getCreationDateTime();
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SIGNATURE:
				return getSignature();
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__BODID:
				return getBODID();
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__USER_AREA:
				return getUserArea();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SENDER:
				setSender((TransSenderType)newValue);
				return;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__RECEIVER:
				getReceiver().clear();
				getReceiver().addAll((Collection<? extends TransReceiverType>)newValue);
				return;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__CREATION_DATE_TIME:
				setCreationDateTime((DateTimeType)newValue);
				return;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SIGNATURE:
				setSignature((TransSignatureType)newValue);
				return;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__BODID:
				setBODID((IdentifierType)newValue);
				return;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__USER_AREA:
				setUserArea((TransUserAreaType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SENDER:
				setSender((TransSenderType)null);
				return;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__RECEIVER:
				getReceiver().clear();
				return;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__CREATION_DATE_TIME:
				setCreationDateTime((DateTimeType)null);
				return;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SIGNATURE:
				setSignature((TransSignatureType)null);
				return;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__BODID:
				setBODID((IdentifierType)null);
				return;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__USER_AREA:
				setUserArea((TransUserAreaType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SENDER:
				return sender != null;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__RECEIVER:
				return receiver != null && !receiver.isEmpty();
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__CREATION_DATE_TIME:
				return creationDateTime != null;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__SIGNATURE:
				return signature != null;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__BODID:
				return bODID != null;
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE__USER_AREA:
				return userArea != null;
		}
		return super.eIsSet(featureID);
	}

} //TransApplicationAreaTypeImpl
