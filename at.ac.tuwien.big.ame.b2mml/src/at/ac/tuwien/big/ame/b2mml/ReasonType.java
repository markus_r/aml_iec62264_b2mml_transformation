/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reason Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getReasonType()
 * @model extendedMetaData="name='ReasonType' kind='simple'"
 * @generated
 */
public interface ReasonType extends CodeType {
} // ReasonType
