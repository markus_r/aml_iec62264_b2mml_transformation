/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.StorageHierarchyScopeType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Storage Hierarchy Scope Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StorageHierarchyScopeTypeImpl extends IdentifierTypeImpl implements StorageHierarchyScopeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StorageHierarchyScopeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getStorageHierarchyScopeType();
	}

} //StorageHierarchyScopeTypeImpl
