/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DataAreaType4;
import at.ac.tuwien.big.ame.b2mml.PersonnelClassType;
import at.ac.tuwien.big.ame.b2mml.TransGetType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type4</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType4Impl#getGet <em>Get</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType4Impl#getPersonnelClass <em>Personnel Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType4Impl extends MinimalEObjectImpl.Container implements DataAreaType4 {
	/**
	 * The cached value of the '{@link #getGet() <em>Get</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGet()
	 * @generated
	 * @ordered
	 */
	protected TransGetType get;

	/**
	 * The cached value of the '{@link #getPersonnelClass() <em>Personnel Class</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelClass()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelClassType> personnelClass;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType4Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType4();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransGetType getGet() {
		return get;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGet(TransGetType newGet, NotificationChain msgs) {
		TransGetType oldGet = get;
		get = newGet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE4__GET, oldGet, newGet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGet(TransGetType newGet) {
		if (newGet != get) {
			NotificationChain msgs = null;
			if (get != null)
				msgs = ((InternalEObject)get).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE4__GET, null, msgs);
			if (newGet != null)
				msgs = ((InternalEObject)newGet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE4__GET, null, msgs);
			msgs = basicSetGet(newGet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE4__GET, newGet, newGet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelClassType> getPersonnelClass() {
		if (personnelClass == null) {
			personnelClass = new EObjectContainmentEList<PersonnelClassType>(PersonnelClassType.class, this, B2MMLPackage.DATA_AREA_TYPE4__PERSONNEL_CLASS);
		}
		return personnelClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE4__GET:
				return basicSetGet(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE4__PERSONNEL_CLASS:
				return ((InternalEList<?>)getPersonnelClass()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE4__GET:
				return getGet();
			case B2MMLPackage.DATA_AREA_TYPE4__PERSONNEL_CLASS:
				return getPersonnelClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE4__GET:
				setGet((TransGetType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE4__PERSONNEL_CLASS:
				getPersonnelClass().clear();
				getPersonnelClass().addAll((Collection<? extends PersonnelClassType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE4__GET:
				setGet((TransGetType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE4__PERSONNEL_CLASS:
				getPersonnelClass().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE4__GET:
				return get != null;
			case B2MMLPackage.DATA_AREA_TYPE4__PERSONNEL_CLASS:
				return personnelClass != null && !personnelClass.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType4Impl
