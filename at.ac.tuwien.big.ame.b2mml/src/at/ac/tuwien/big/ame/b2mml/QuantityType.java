/**
 */
package at.ac.tuwien.big.ame.b2mml;

import java.math.BigDecimal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quantity Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.QuantityType#getValue <em>Value</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.QuantityType#getUnitCode <em>Unit Code</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.QuantityType#getUnitCodeListAgencyID <em>Unit Code List Agency ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.QuantityType#getUnitCodeListAgencyName <em>Unit Code List Agency Name</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.QuantityType#getUnitCodeListID <em>Unit Code List ID</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQuantityType()
 * @model extendedMetaData="name='QuantityType' kind='simple'"
 * @generated
 */
public interface QuantityType extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(BigDecimal)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQuantityType_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	BigDecimal getValue();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityType#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Unit Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Code</em>' attribute.
	 * @see #setUnitCode(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQuantityType_UnitCode()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='unitCode'"
	 * @generated
	 */
	String getUnitCode();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityType#getUnitCode <em>Unit Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Code</em>' attribute.
	 * @see #getUnitCode()
	 * @generated
	 */
	void setUnitCode(String value);

	/**
	 * Returns the value of the '<em><b>Unit Code List Agency ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Code List Agency ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Code List Agency ID</em>' attribute.
	 * @see #setUnitCodeListAgencyID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQuantityType_UnitCodeListAgencyID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='unitCodeListAgencyID'"
	 * @generated
	 */
	String getUnitCodeListAgencyID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityType#getUnitCodeListAgencyID <em>Unit Code List Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Code List Agency ID</em>' attribute.
	 * @see #getUnitCodeListAgencyID()
	 * @generated
	 */
	void setUnitCodeListAgencyID(String value);

	/**
	 * Returns the value of the '<em><b>Unit Code List Agency Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Code List Agency Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Code List Agency Name</em>' attribute.
	 * @see #setUnitCodeListAgencyName(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQuantityType_UnitCodeListAgencyName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='unitCodeListAgencyName'"
	 * @generated
	 */
	String getUnitCodeListAgencyName();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityType#getUnitCodeListAgencyName <em>Unit Code List Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Code List Agency Name</em>' attribute.
	 * @see #getUnitCodeListAgencyName()
	 * @generated
	 */
	void setUnitCodeListAgencyName(String value);

	/**
	 * Returns the value of the '<em><b>Unit Code List ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Code List ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Code List ID</em>' attribute.
	 * @see #setUnitCodeListID(String)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getQuantityType_UnitCodeListID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='unitCodeListID'"
	 * @generated
	 */
	String getUnitCodeListID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.QuantityType#getUnitCodeListID <em>Unit Code List ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Code List ID</em>' attribute.
	 * @see #getUnitCodeListID()
	 * @generated
	 */
	void setUnitCodeListID(String value);

} // QuantityType
