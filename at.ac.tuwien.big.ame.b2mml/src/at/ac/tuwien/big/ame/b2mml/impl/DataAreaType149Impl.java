/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DataAreaType149;
import at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType;
import at.ac.tuwien.big.ame.b2mml.TransAcknowledgeType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type149</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType149Impl#getAcknowledge <em>Acknowledge</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType149Impl#getOperationsCapability <em>Operations Capability</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType149Impl extends MinimalEObjectImpl.Container implements DataAreaType149 {
	/**
	 * The cached value of the '{@link #getAcknowledge() <em>Acknowledge</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcknowledge()
	 * @generated
	 * @ordered
	 */
	protected TransAcknowledgeType acknowledge;

	/**
	 * The cached value of the '{@link #getOperationsCapability() <em>Operations Capability</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsCapability()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsCapabilityType> operationsCapability;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType149Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType149();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransAcknowledgeType getAcknowledge() {
		return acknowledge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledge(TransAcknowledgeType newAcknowledge, NotificationChain msgs) {
		TransAcknowledgeType oldAcknowledge = acknowledge;
		acknowledge = newAcknowledge;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE149__ACKNOWLEDGE, oldAcknowledge, newAcknowledge);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledge(TransAcknowledgeType newAcknowledge) {
		if (newAcknowledge != acknowledge) {
			NotificationChain msgs = null;
			if (acknowledge != null)
				msgs = ((InternalEObject)acknowledge).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE149__ACKNOWLEDGE, null, msgs);
			if (newAcknowledge != null)
				msgs = ((InternalEObject)newAcknowledge).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE149__ACKNOWLEDGE, null, msgs);
			msgs = basicSetAcknowledge(newAcknowledge, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE149__ACKNOWLEDGE, newAcknowledge, newAcknowledge));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsCapabilityType> getOperationsCapability() {
		if (operationsCapability == null) {
			operationsCapability = new EObjectContainmentEList<OperationsCapabilityType>(OperationsCapabilityType.class, this, B2MMLPackage.DATA_AREA_TYPE149__OPERATIONS_CAPABILITY);
		}
		return operationsCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE149__ACKNOWLEDGE:
				return basicSetAcknowledge(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE149__OPERATIONS_CAPABILITY:
				return ((InternalEList<?>)getOperationsCapability()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE149__ACKNOWLEDGE:
				return getAcknowledge();
			case B2MMLPackage.DATA_AREA_TYPE149__OPERATIONS_CAPABILITY:
				return getOperationsCapability();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE149__ACKNOWLEDGE:
				setAcknowledge((TransAcknowledgeType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE149__OPERATIONS_CAPABILITY:
				getOperationsCapability().clear();
				getOperationsCapability().addAll((Collection<? extends OperationsCapabilityType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE149__ACKNOWLEDGE:
				setAcknowledge((TransAcknowledgeType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE149__OPERATIONS_CAPABILITY:
				getOperationsCapability().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE149__ACKNOWLEDGE:
				return acknowledge != null;
			case B2MMLPackage.DATA_AREA_TYPE149__OPERATIONS_CAPABILITY:
				return operationsCapability != null && !operationsCapability.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType149Impl
