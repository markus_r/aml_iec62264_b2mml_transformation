/**
 */
package at.ac.tuwien.big.ame.b2mml.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see at.ac.tuwien.big.ame.b2mml.util.B2MMLResourceFactoryImpl
 * @generated
 */
public class B2MMLResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public B2MMLResourceImpl(URI uri) {
		super(uri);
	}

} //B2MMLResourceImpl
