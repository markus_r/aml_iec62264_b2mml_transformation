/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Capability Information Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getOperationsCapability <em>Operations Capability</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityInformationType()
 * @model extendedMetaData="name='OperationsCapabilityInformationType' kind='elementOnly'"
 * @generated
 */
public interface OperationsCapabilityInformationType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #isSetID()
	 * @see #unsetID()
	 * @see #setID(IdentifierType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityInformationType_ID()
	 * @model containment="true" unsettable="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #isSetID()
	 * @see #unsetID()
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Unsets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetID()
	 * @see #getID()
	 * @see #setID(IdentifierType)
	 * @generated
	 */
	void unsetID();

	/**
	 * Returns whether the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getID <em>ID</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ID</em>' containment reference is set.
	 * @see #unsetID()
	 * @see #getID()
	 * @see #setID(IdentifierType)
	 * @generated
	 */
	boolean isSetID();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityInformationType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #isSetHierarchyScope()
	 * @see #unsetHierarchyScope()
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityInformationType_HierarchyScope()
	 * @model containment="true" unsettable="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #isSetHierarchyScope()
	 * @see #unsetHierarchyScope()
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Unsets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHierarchyScope()
	 * @see #getHierarchyScope()
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @generated
	 */
	void unsetHierarchyScope();

	/**
	 * Returns whether the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Hierarchy Scope</em>' containment reference is set.
	 * @see #unsetHierarchyScope()
	 * @see #getHierarchyScope()
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @generated
	 */
	boolean isSetHierarchyScope();

	/**
	 * Returns the value of the '<em><b>Published Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Published Date</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Published Date</em>' containment reference.
	 * @see #isSetPublishedDate()
	 * @see #unsetPublishedDate()
	 * @see #setPublishedDate(PublishedDateType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityInformationType_PublishedDate()
	 * @model containment="true" unsettable="true"
	 *        extendedMetaData="kind='element' name='PublishedDate' namespace='##targetNamespace'"
	 * @generated
	 */
	PublishedDateType getPublishedDate();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getPublishedDate <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Published Date</em>' containment reference.
	 * @see #isSetPublishedDate()
	 * @see #unsetPublishedDate()
	 * @see #getPublishedDate()
	 * @generated
	 */
	void setPublishedDate(PublishedDateType value);

	/**
	 * Unsets the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getPublishedDate <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPublishedDate()
	 * @see #getPublishedDate()
	 * @see #setPublishedDate(PublishedDateType)
	 * @generated
	 */
	void unsetPublishedDate();

	/**
	 * Returns whether the value of the '{@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityInformationType#getPublishedDate <em>Published Date</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Published Date</em>' containment reference is set.
	 * @see #unsetPublishedDate()
	 * @see #getPublishedDate()
	 * @see #setPublishedDate(PublishedDateType)
	 * @generated
	 */
	boolean isSetPublishedDate();

	/**
	 * Returns the value of the '<em><b>Operations Capability</b></em>' containment reference list.
	 * The list contents are of type {@link at.ac.tuwien.big.ame.b2mml.OperationsCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Capability</em>' containment reference list.
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getOperationsCapabilityInformationType_OperationsCapability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsCapabilityType> getOperationsCapability();

} // OperationsCapabilityInformationType
