/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.EndTimeType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.OperationsRequestType;
import at.ac.tuwien.big.ame.b2mml.OperationsScheduleType;
import at.ac.tuwien.big.ame.b2mml.OperationsTypeType;
import at.ac.tuwien.big.ame.b2mml.PublishedDateType;
import at.ac.tuwien.big.ame.b2mml.RequestStateType;
import at.ac.tuwien.big.ame.b2mml.StartTimeType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Schedule Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsScheduleTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsScheduleTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsScheduleTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsScheduleTypeImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsScheduleTypeImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsScheduleTypeImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsScheduleTypeImpl#getScheduleState <em>Schedule State</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsScheduleTypeImpl#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.OperationsScheduleTypeImpl#getOperationsRequest <em>Operations Request</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsScheduleTypeImpl extends MinimalEObjectImpl.Container implements OperationsScheduleType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsTypeType operationsType;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected StartTimeType startTime;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected EndTimeType endTime;

	/**
	 * The cached value of the '{@link #getScheduleState() <em>Schedule State</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScheduleState()
	 * @generated
	 * @ordered
	 */
	protected RequestStateType scheduleState;

	/**
	 * The cached value of the '{@link #getPublishedDate() <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublishedDate()
	 * @generated
	 * @ordered
	 */
	protected PublishedDateType publishedDate;

	/**
	 * The cached value of the '{@link #getOperationsRequest() <em>Operations Request</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsRequest()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsRequestType> operationsRequest;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsScheduleTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOperationsScheduleType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsTypeType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsType(OperationsTypeType newOperationsType, NotificationChain msgs) {
		OperationsTypeType oldOperationsType = operationsType;
		operationsType = newOperationsType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_TYPE, oldOperationsType, newOperationsType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsTypeType newOperationsType) {
		if (newOperationsType != operationsType) {
			NotificationChain msgs = null;
			if (operationsType != null)
				msgs = ((InternalEObject)operationsType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_TYPE, null, msgs);
			if (newOperationsType != null)
				msgs = ((InternalEObject)newOperationsType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_TYPE, null, msgs);
			msgs = basicSetOperationsType(newOperationsType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_TYPE, newOperationsType, newOperationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StartTimeType getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStartTime(StartTimeType newStartTime, NotificationChain msgs) {
		StartTimeType oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__START_TIME, oldStartTime, newStartTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(StartTimeType newStartTime) {
		if (newStartTime != startTime) {
			NotificationChain msgs = null;
			if (startTime != null)
				msgs = ((InternalEObject)startTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__START_TIME, null, msgs);
			if (newStartTime != null)
				msgs = ((InternalEObject)newStartTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__START_TIME, null, msgs);
			msgs = basicSetStartTime(newStartTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__START_TIME, newStartTime, newStartTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EndTimeType getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEndTime(EndTimeType newEndTime, NotificationChain msgs) {
		EndTimeType oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__END_TIME, oldEndTime, newEndTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(EndTimeType newEndTime) {
		if (newEndTime != endTime) {
			NotificationChain msgs = null;
			if (endTime != null)
				msgs = ((InternalEObject)endTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__END_TIME, null, msgs);
			if (newEndTime != null)
				msgs = ((InternalEObject)newEndTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__END_TIME, null, msgs);
			msgs = basicSetEndTime(newEndTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__END_TIME, newEndTime, newEndTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequestStateType getScheduleState() {
		return scheduleState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScheduleState(RequestStateType newScheduleState, NotificationChain msgs) {
		RequestStateType oldScheduleState = scheduleState;
		scheduleState = newScheduleState;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__SCHEDULE_STATE, oldScheduleState, newScheduleState);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScheduleState(RequestStateType newScheduleState) {
		if (newScheduleState != scheduleState) {
			NotificationChain msgs = null;
			if (scheduleState != null)
				msgs = ((InternalEObject)scheduleState).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__SCHEDULE_STATE, null, msgs);
			if (newScheduleState != null)
				msgs = ((InternalEObject)newScheduleState).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__SCHEDULE_STATE, null, msgs);
			msgs = basicSetScheduleState(newScheduleState, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__SCHEDULE_STATE, newScheduleState, newScheduleState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PublishedDateType getPublishedDate() {
		return publishedDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPublishedDate(PublishedDateType newPublishedDate, NotificationChain msgs) {
		PublishedDateType oldPublishedDate = publishedDate;
		publishedDate = newPublishedDate;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__PUBLISHED_DATE, oldPublishedDate, newPublishedDate);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublishedDate(PublishedDateType newPublishedDate) {
		if (newPublishedDate != publishedDate) {
			NotificationChain msgs = null;
			if (publishedDate != null)
				msgs = ((InternalEObject)publishedDate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__PUBLISHED_DATE, null, msgs);
			if (newPublishedDate != null)
				msgs = ((InternalEObject)newPublishedDate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__PUBLISHED_DATE, null, msgs);
			msgs = basicSetPublishedDate(newPublishedDate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__PUBLISHED_DATE, newPublishedDate, newPublishedDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsRequestType> getOperationsRequest() {
		if (operationsRequest == null) {
			operationsRequest = new EObjectContainmentEList<OperationsRequestType>(OperationsRequestType.class, this, B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_REQUEST);
		}
		return operationsRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_TYPE:
				return basicSetOperationsType(null, msgs);
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__START_TIME:
				return basicSetStartTime(null, msgs);
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__END_TIME:
				return basicSetEndTime(null, msgs);
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__SCHEDULE_STATE:
				return basicSetScheduleState(null, msgs);
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__PUBLISHED_DATE:
				return basicSetPublishedDate(null, msgs);
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_REQUEST:
				return ((InternalEList<?>)getOperationsRequest()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__ID:
				return getID();
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_TYPE:
				return getOperationsType();
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__START_TIME:
				return getStartTime();
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__END_TIME:
				return getEndTime();
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__SCHEDULE_STATE:
				return getScheduleState();
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__PUBLISHED_DATE:
				return getPublishedDate();
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_REQUEST:
				return getOperationsRequest();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__START_TIME:
				setStartTime((StartTimeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__END_TIME:
				setEndTime((EndTimeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__SCHEDULE_STATE:
				setScheduleState((RequestStateType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__PUBLISHED_DATE:
				setPublishedDate((PublishedDateType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_REQUEST:
				getOperationsRequest().clear();
				getOperationsRequest().addAll((Collection<? extends OperationsRequestType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)null);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__START_TIME:
				setStartTime((StartTimeType)null);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__END_TIME:
				setEndTime((EndTimeType)null);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__SCHEDULE_STATE:
				setScheduleState((RequestStateType)null);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__PUBLISHED_DATE:
				setPublishedDate((PublishedDateType)null);
				return;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_REQUEST:
				getOperationsRequest().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__ID:
				return iD != null;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_TYPE:
				return operationsType != null;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__START_TIME:
				return startTime != null;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__END_TIME:
				return endTime != null;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__SCHEDULE_STATE:
				return scheduleState != null;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__PUBLISHED_DATE:
				return publishedDate != null;
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE__OPERATIONS_REQUEST:
				return operationsRequest != null && !operationsRequest.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OperationsScheduleTypeImpl
