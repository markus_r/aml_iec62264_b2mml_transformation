/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Storage Hierarchy Scope Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getStorageHierarchyScopeType()
 * @model extendedMetaData="name='StorageHierarchyScopeType' kind='simple'"
 * @generated
 */
public interface StorageHierarchyScopeType extends IdentifierType {
} // StorageHierarchyScopeType
