/**
 */
package at.ac.tuwien.big.ame.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tested Person Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestedPersonPropertyType#getPersonID <em>Person ID</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.TestedPersonPropertyType#getPropertyID <em>Property ID</em>}</li>
 * </ul>
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedPersonPropertyType()
 * @model extendedMetaData="name='TestedPersonPropertyType' kind='elementOnly'"
 * @generated
 */
public interface TestedPersonPropertyType extends EObject {
	/**
	 * Returns the value of the '<em><b>Person ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person ID</em>' containment reference.
	 * @see #setPersonID(PersonIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedPersonPropertyType_PersonID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PersonID' namespace='##targetNamespace'"
	 * @generated
	 */
	PersonIDType getPersonID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestedPersonPropertyType#getPersonID <em>Person ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Person ID</em>' containment reference.
	 * @see #getPersonID()
	 * @generated
	 */
	void setPersonID(PersonIDType value);

	/**
	 * Returns the value of the '<em><b>Property ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property ID</em>' containment reference.
	 * @see #setPropertyID(PropertyIDType)
	 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getTestedPersonPropertyType_PropertyID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PropertyID' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyIDType getPropertyID();

	/**
	 * Sets the value of the '{@link at.ac.tuwien.big.ame.b2mml.TestedPersonPropertyType#getPropertyID <em>Property ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property ID</em>' containment reference.
	 * @see #getPropertyID()
	 * @generated
	 */
	void setPropertyID(PropertyIDType value);

} // TestedPersonPropertyType
