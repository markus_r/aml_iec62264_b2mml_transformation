/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.DataAreaType85;
import at.ac.tuwien.big.ame.b2mml.EquipmentInformationType;
import at.ac.tuwien.big.ame.b2mml.TransShowType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type85</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType85Impl#getShow <em>Show</em>}</li>
 *   <li>{@link at.ac.tuwien.big.ame.b2mml.impl.DataAreaType85Impl#getEquipmentInformation <em>Equipment Information</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType85Impl extends MinimalEObjectImpl.Container implements DataAreaType85 {
	/**
	 * The cached value of the '{@link #getShow() <em>Show</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShow()
	 * @generated
	 * @ordered
	 */
	protected TransShowType show;

	/**
	 * The cached value of the '{@link #getEquipmentInformation() <em>Equipment Information</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentInformation()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentInformationType> equipmentInformation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType85Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType85();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransShowType getShow() {
		return show;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShow(TransShowType newShow, NotificationChain msgs) {
		TransShowType oldShow = show;
		show = newShow;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE85__SHOW, oldShow, newShow);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShow(TransShowType newShow) {
		if (newShow != show) {
			NotificationChain msgs = null;
			if (show != null)
				msgs = ((InternalEObject)show).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE85__SHOW, null, msgs);
			if (newShow != null)
				msgs = ((InternalEObject)newShow).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE85__SHOW, null, msgs);
			msgs = basicSetShow(newShow, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE85__SHOW, newShow, newShow));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentInformationType> getEquipmentInformation() {
		if (equipmentInformation == null) {
			equipmentInformation = new EObjectContainmentEList<EquipmentInformationType>(EquipmentInformationType.class, this, B2MMLPackage.DATA_AREA_TYPE85__EQUIPMENT_INFORMATION);
		}
		return equipmentInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE85__SHOW:
				return basicSetShow(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE85__EQUIPMENT_INFORMATION:
				return ((InternalEList<?>)getEquipmentInformation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE85__SHOW:
				return getShow();
			case B2MMLPackage.DATA_AREA_TYPE85__EQUIPMENT_INFORMATION:
				return getEquipmentInformation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE85__SHOW:
				setShow((TransShowType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE85__EQUIPMENT_INFORMATION:
				getEquipmentInformation().clear();
				getEquipmentInformation().addAll((Collection<? extends EquipmentInformationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE85__SHOW:
				setShow((TransShowType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE85__EQUIPMENT_INFORMATION:
				getEquipmentInformation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE85__SHOW:
				return show != null;
			case B2MMLPackage.DATA_AREA_TYPE85__EQUIPMENT_INFORMATION:
				return equipmentInformation != null && !equipmentInformation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType85Impl
