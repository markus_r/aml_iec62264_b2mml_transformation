/**
 */
package at.ac.tuwien.big.ame.b2mml.impl;

import at.ac.tuwien.big.ame.b2mml.B2MMLPackage;
import at.ac.tuwien.big.ame.b2mml.OperationsSegmentIDType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Segment ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OperationsSegmentIDTypeImpl extends IdentifierTypeImpl implements OperationsSegmentIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsSegmentIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOperationsSegmentIDType();
	}

} //OperationsSegmentIDTypeImpl
