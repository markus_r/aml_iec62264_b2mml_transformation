/**
 */
package at.ac.tuwien.big.ame.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actual Start Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.b2mml.B2MMLPackage#getActualStartTimeType()
 * @model extendedMetaData="name='ActualStartTimeType' kind='simple'"
 * @generated
 */
public interface ActualStartTimeType extends DateTimeType {
} // ActualStartTimeType
