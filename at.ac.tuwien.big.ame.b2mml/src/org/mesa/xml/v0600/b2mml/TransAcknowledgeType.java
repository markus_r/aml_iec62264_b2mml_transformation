/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Acknowledge Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransAcknowledgeType#getOriginalApplicationArea <em>Original Application Area</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransAcknowledgeType#getResponseCriteria <em>Response Criteria</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransAcknowledgeType()
 * @model extendedMetaData="name='TransAcknowledgeType' kind='elementOnly'"
 * @generated
 */
public interface TransAcknowledgeType extends EObject {
	/**
	 * Returns the value of the '<em><b>Original Application Area</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Original Application Area</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Original Application Area</em>' containment reference.
	 * @see #setOriginalApplicationArea(TransApplicationAreaType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransAcknowledgeType_OriginalApplicationArea()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OriginalApplicationArea' namespace='##targetNamespace'"
	 * @generated
	 */
	TransApplicationAreaType getOriginalApplicationArea();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TransAcknowledgeType#getOriginalApplicationArea <em>Original Application Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Original Application Area</em>' containment reference.
	 * @see #getOriginalApplicationArea()
	 * @generated
	 */
	void setOriginalApplicationArea(TransApplicationAreaType value);

	/**
	 * Returns the value of the '<em><b>Response Criteria</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.TransResponseCriteriaType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response Criteria</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Response Criteria</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransAcknowledgeType_ResponseCriteria()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ResponseCriteria' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TransResponseCriteriaType> getResponseCriteria();

} // TransAcknowledgeType
