/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hierarchy Scope Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.HierarchyScopeType#getEquipmentID <em>Equipment ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.HierarchyScopeType#getEquipmentElementLevel <em>Equipment Element Level</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.HierarchyScopeType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getHierarchyScopeType()
 * @model extendedMetaData="name='HierarchyScopeType' kind='elementOnly'"
 * @generated
 */
public interface HierarchyScopeType extends EObject {
	/**
	 * Returns the value of the '<em><b>Equipment ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment ID</em>' containment reference.
	 * @see #setEquipmentID(EquipmentIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getHierarchyScopeType_EquipmentID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='EquipmentID' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentIDType getEquipmentID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.HierarchyScopeType#getEquipmentID <em>Equipment ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment ID</em>' containment reference.
	 * @see #getEquipmentID()
	 * @generated
	 */
	void setEquipmentID(EquipmentIDType value);

	/**
	 * Returns the value of the '<em><b>Equipment Element Level</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Element Level</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Element Level</em>' containment reference.
	 * @see #setEquipmentElementLevel(EquipmentElementLevelType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getHierarchyScopeType_EquipmentElementLevel()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='EquipmentElementLevel' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentElementLevelType getEquipmentElementLevel();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.HierarchyScopeType#getEquipmentElementLevel <em>Equipment Element Level</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Element Level</em>' containment reference.
	 * @see #getEquipmentElementLevel()
	 * @generated
	 */
	void setEquipmentElementLevel(EquipmentElementLevelType value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getHierarchyScopeType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.HierarchyScopeType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

} // HierarchyScopeType
