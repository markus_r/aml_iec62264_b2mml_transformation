/**
 */
package org.mesa.xml.v0600.b2mml;

import javax.xml.datatype.Duration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Segment Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getDuration <em>Duration</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getProcessSegmentID <em>Process Segment ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getParameter <em>Parameter</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getPersonnelSpecification <em>Personnel Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getEquipmentSpecification <em>Equipment Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getPhysicalAssetSpecification <em>Physical Asset Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getMaterialSpecification <em>Material Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getSegmentDependency <em>Segment Dependency</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getOperationsSegment <em>Operations Segment</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType()
 * @model extendedMetaData="name='OperationsSegmentType' kind='elementOnly'"
 * @generated
 */
public interface OperationsSegmentType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_ID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(DescriptionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	DescriptionType getDescription();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(DescriptionType value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' containment reference.
	 * @see #setOperationsType(OperationsTypeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_OperationsType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsType' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsTypeType getOperationsType();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getOperationsType <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' containment reference.
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsTypeType value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(Duration)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_Duration()
	 * @model dataType="org.mesa.xml.v0600.b2mml.DurationType"
	 *        extendedMetaData="kind='element' name='Duration' namespace='##targetNamespace'"
	 * @generated
	 */
	Duration getDuration();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsSegmentType#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(Duration value);

	/**
	 * Returns the value of the '<em><b>Process Segment ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.ProcessSegmentIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_ProcessSegmentID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ProcessSegmentID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ProcessSegmentIDType> getProcessSegmentID();

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.ParameterType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_Parameter()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Parameter' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ParameterType> getParameter();

	/**
	 * Returns the value of the '<em><b>Personnel Specification</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpPersonnelSpecificationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Specification</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_PersonnelSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonnelSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPersonnelSpecificationType> getPersonnelSpecification();

	/**
	 * Returns the value of the '<em><b>Equipment Specification</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpEquipmentSpecificationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Specification</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_EquipmentSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpEquipmentSpecificationType> getEquipmentSpecification();

	/**
	 * Returns the value of the '<em><b>Physical Asset Specification</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetSpecificationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Specification</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_PhysicalAssetSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPhysicalAssetSpecificationType> getPhysicalAssetSpecification();

	/**
	 * Returns the value of the '<em><b>Material Specification</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpMaterialSpecificationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Specification</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_MaterialSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpMaterialSpecificationType> getMaterialSpecification();

	/**
	 * Returns the value of the '<em><b>Segment Dependency</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.SegmentDependencyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment Dependency</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment Dependency</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_SegmentDependency()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SegmentDependency' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<SegmentDependencyType> getSegmentDependency();

	/**
	 * Returns the value of the '<em><b>Operations Segment</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OperationsSegmentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Segment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Segment</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentType_OperationsSegment()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsSegment' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsSegmentType> getOperationsSegment();

} // OperationsSegmentType
