/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Confidence Factor Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getConfidenceFactorType()
 * @model extendedMetaData="name='ConfidenceFactorType' kind='simple'"
 * @generated
 */
public interface ConfidenceFactorType extends IdentifierType {
} // ConfidenceFactorType
