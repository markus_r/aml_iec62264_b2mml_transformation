/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resources Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getResourcesType()
 * @model extendedMetaData="name='ResourcesType' kind='simple'"
 * @generated
 */
public interface ResourcesType extends CodeType {
} // ResourcesType
