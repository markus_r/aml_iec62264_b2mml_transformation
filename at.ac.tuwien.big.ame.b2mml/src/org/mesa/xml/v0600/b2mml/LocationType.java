/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Location Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.LocationType#getEquipmentID <em>Equipment ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.LocationType#getEquipmentElementLevel <em>Equipment Element Level</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.LocationType#getLocation <em>Location</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getLocationType()
 * @model extendedMetaData="name='LocationType' kind='elementOnly'"
 * @generated
 */
public interface LocationType extends EObject {
	/**
	 * Returns the value of the '<em><b>Equipment ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment ID</em>' containment reference.
	 * @see #setEquipmentID(EquipmentIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getLocationType_EquipmentID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='EquipmentID' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentIDType getEquipmentID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.LocationType#getEquipmentID <em>Equipment ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment ID</em>' containment reference.
	 * @see #getEquipmentID()
	 * @generated
	 */
	void setEquipmentID(EquipmentIDType value);

	/**
	 * Returns the value of the '<em><b>Equipment Element Level</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Element Level</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Element Level</em>' containment reference.
	 * @see #setEquipmentElementLevel(EquipmentElementLevelType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getLocationType_EquipmentElementLevel()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='EquipmentElementLevel' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentElementLevelType getEquipmentElementLevel();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.LocationType#getEquipmentElementLevel <em>Equipment Element Level</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Element Level</em>' containment reference.
	 * @see #getEquipmentElementLevel()
	 * @generated
	 */
	void setEquipmentElementLevel(EquipmentElementLevelType value);

	/**
	 * Returns the value of the '<em><b>Location</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' containment reference.
	 * @see #setLocation(LocationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getLocationType_Location()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Location' namespace='##targetNamespace'"
	 * @generated
	 */
	LocationType getLocation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.LocationType#getLocation <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' containment reference.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(LocationType value);

} // LocationType
