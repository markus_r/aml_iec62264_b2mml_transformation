/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Certificate Of Analysis Reference Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getCertificateOfAnalysisReferenceType()
 * @model extendedMetaData="name='CertificateOfAnalysisReferenceType' kind='simple'"
 * @generated
 */
public interface CertificateOfAnalysisReferenceType extends IdentifierType {
} // CertificateOfAnalysisReferenceType
