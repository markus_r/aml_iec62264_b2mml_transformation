/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Priority Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPriorityType()
 * @model extendedMetaData="name='PriorityType' kind='simple'"
 * @generated
 */
public interface PriorityType extends NumericType {
} // PriorityType
