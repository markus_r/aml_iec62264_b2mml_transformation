/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Actual ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialActualIDType()
 * @model extendedMetaData="name='MaterialActualIDType' kind='simple'"
 * @generated
 */
public interface MaterialActualIDType extends IdentifierType {
} // MaterialActualIDType
