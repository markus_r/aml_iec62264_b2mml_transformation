/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Production Schedule ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getProductionScheduleIDType()
 * @model extendedMetaData="name='ProductionScheduleIDType' kind='simple'"
 * @generated
 */
public interface ProductionScheduleIDType extends IdentifierType {
} // ProductionScheduleIDType
