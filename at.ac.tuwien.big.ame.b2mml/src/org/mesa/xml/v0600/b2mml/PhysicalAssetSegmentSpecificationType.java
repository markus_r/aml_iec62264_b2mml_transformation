/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Segment Specification Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetSegmentSpecificationType#getPhysicalAssetClassID <em>Physical Asset Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetSegmentSpecificationType#getPhysicalAssetID <em>Physical Asset ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetSegmentSpecificationType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetSegmentSpecificationType#getPhysicalAssetUse <em>Physical Asset Use</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetSegmentSpecificationType#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetSegmentSpecificationType#getPhysicalAssetSegmentSpecificationProperty <em>Physical Asset Segment Specification Property</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetSegmentSpecificationType()
 * @model extendedMetaData="name='PhysicalAssetSegmentSpecificationType' kind='elementOnly'"
 * @generated
 */
public interface PhysicalAssetSegmentSpecificationType extends EObject {
	/**
	 * Returns the value of the '<em><b>Physical Asset Class ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Class ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Class ID</em>' containment reference.
	 * @see #setPhysicalAssetClassID(PhysicalAssetClassIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetSegmentSpecificationType_PhysicalAssetClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	PhysicalAssetClassIDType getPhysicalAssetClassID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.PhysicalAssetSegmentSpecificationType#getPhysicalAssetClassID <em>Physical Asset Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Class ID</em>' containment reference.
	 * @see #getPhysicalAssetClassID()
	 * @generated
	 */
	void setPhysicalAssetClassID(PhysicalAssetClassIDType value);

	/**
	 * Returns the value of the '<em><b>Physical Asset ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset ID</em>' containment reference.
	 * @see #setPhysicalAssetID(PhysicalAssetIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetSegmentSpecificationType_PhysicalAssetID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetID' namespace='##targetNamespace'"
	 * @generated
	 */
	PhysicalAssetIDType getPhysicalAssetID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.PhysicalAssetSegmentSpecificationType#getPhysicalAssetID <em>Physical Asset ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset ID</em>' containment reference.
	 * @see #getPhysicalAssetID()
	 * @generated
	 */
	void setPhysicalAssetID(PhysicalAssetIDType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetSegmentSpecificationType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Physical Asset Use</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Use</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Use</em>' containment reference.
	 * @see #setPhysicalAssetUse(CodeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetSegmentSpecificationType_PhysicalAssetUse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetUse' namespace='##targetNamespace'"
	 * @generated
	 */
	CodeType getPhysicalAssetUse();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.PhysicalAssetSegmentSpecificationType#getPhysicalAssetUse <em>Physical Asset Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Use</em>' containment reference.
	 * @see #getPhysicalAssetUse()
	 * @generated
	 */
	void setPhysicalAssetUse(CodeType value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetSegmentSpecificationType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

	/**
	 * Returns the value of the '<em><b>Physical Asset Segment Specification Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PhysicalAssetSegmentSpecificationPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Segment Specification Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Segment Specification Property</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetSegmentSpecificationType_PhysicalAssetSegmentSpecificationProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetSegmentSpecificationProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetSegmentSpecificationPropertyType> getPhysicalAssetSegmentSpecificationProperty();

} // PhysicalAssetSegmentSpecificationType
