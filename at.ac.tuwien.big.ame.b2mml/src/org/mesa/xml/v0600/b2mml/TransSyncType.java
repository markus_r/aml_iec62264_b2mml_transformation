/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Sync Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransSyncType#getActionCriteria <em>Action Criteria</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransSyncType()
 * @model extendedMetaData="name='TransSyncType' kind='elementOnly'"
 * @generated
 */
public interface TransSyncType extends EObject {
	/**
	 * Returns the value of the '<em><b>Action Criteria</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.TransActionCriteriaType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action Criteria</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Criteria</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransSyncType_ActionCriteria()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ActionCriteria' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TransActionCriteriaType> getActionCriteria();

} // TransSyncType
