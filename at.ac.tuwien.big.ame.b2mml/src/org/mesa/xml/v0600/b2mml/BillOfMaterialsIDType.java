/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bill Of Materials ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBillOfMaterialsIDType()
 * @model extendedMetaData="name='BillOfMaterialsIDType' kind='simple'"
 * @generated
 */
public interface BillOfMaterialsIDType extends IdentifierType {
} // BillOfMaterialsIDType
