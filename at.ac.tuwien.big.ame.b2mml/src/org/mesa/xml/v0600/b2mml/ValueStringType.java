/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value String Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getValueStringType()
 * @model extendedMetaData="name='ValueStringType' kind='simple'"
 * @generated
 */
public interface ValueStringType extends AnyGenericValueType {
} // ValueStringType
