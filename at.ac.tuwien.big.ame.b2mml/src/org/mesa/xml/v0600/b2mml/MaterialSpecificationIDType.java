/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Specification ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSpecificationIDType()
 * @model extendedMetaData="name='MaterialSpecificationIDType' kind='simple'"
 * @generated
 */
public interface MaterialSpecificationIDType extends IdentifierType {
} // MaterialSpecificationIDType
