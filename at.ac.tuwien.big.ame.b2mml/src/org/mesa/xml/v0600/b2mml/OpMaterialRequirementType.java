/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Op Material Requirement Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getMaterialClassID <em>Material Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getMaterialDefinitionID <em>Material Definition ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getMaterialLotID <em>Material Lot ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getMaterialSubLotID <em>Material Sub Lot ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getStorageLocation <em>Storage Location</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getAssemblyRequirement <em>Assembly Requirement</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getMaterialRequirementProperty <em>Material Requirement Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType()
 * @model extendedMetaData="name='OpMaterialRequirementType' kind='elementOnly'"
 * @generated
 */
public interface OpMaterialRequirementType extends EObject {
	/**
	 * Returns the value of the '<em><b>Material Class ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialClassIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Class ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Class ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_MaterialClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialClassIDType> getMaterialClassID();

	/**
	 * Returns the value of the '<em><b>Material Definition ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialDefinitionIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Definition ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Definition ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_MaterialDefinitionID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialDefinitionID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialDefinitionIDType> getMaterialDefinitionID();

	/**
	 * Returns the value of the '<em><b>Material Lot ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialLotIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Lot ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Lot ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_MaterialLotID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialLotID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialLotIDType> getMaterialLotID();

	/**
	 * Returns the value of the '<em><b>Material Sub Lot ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialSubLotIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Sub Lot ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Sub Lot ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_MaterialSubLotID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialSubLotID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialSubLotIDType> getMaterialSubLotID();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Material Use</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Use</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Use</em>' containment reference.
	 * @see #setMaterialUse(MaterialUseType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_MaterialUse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialUse' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialUseType getMaterialUse();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getMaterialUse <em>Material Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Use</em>' containment reference.
	 * @see #getMaterialUse()
	 * @generated
	 */
	void setMaterialUse(MaterialUseType value);

	/**
	 * Returns the value of the '<em><b>Storage Location</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Location</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Location</em>' containment reference.
	 * @see #setStorageLocation(StorageLocationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_StorageLocation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='StorageLocation' namespace='##targetNamespace'"
	 * @generated
	 */
	StorageLocationType getStorageLocation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getStorageLocation <em>Storage Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Location</em>' containment reference.
	 * @see #getStorageLocation()
	 * @generated
	 */
	void setStorageLocation(StorageLocationType value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

	/**
	 * Returns the value of the '<em><b>Assembly Requirement</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Requirement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Requirement</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_AssemblyRequirement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyRequirement' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpMaterialRequirementType> getAssemblyRequirement();

	/**
	 * Returns the value of the '<em><b>Assembly Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Type</em>' containment reference.
	 * @see #setAssemblyType(AssemblyTypeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_AssemblyType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyType' namespace='##targetNamespace'"
	 * @generated
	 */
	AssemblyTypeType getAssemblyType();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getAssemblyType <em>Assembly Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Type</em>' containment reference.
	 * @see #getAssemblyType()
	 * @generated
	 */
	void setAssemblyType(AssemblyTypeType value);

	/**
	 * Returns the value of the '<em><b>Assembly Relationship</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Relationship</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Relationship</em>' containment reference.
	 * @see #setAssemblyRelationship(AssemblyRelationshipType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_AssemblyRelationship()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyRelationship' namespace='##targetNamespace'"
	 * @generated
	 */
	AssemblyRelationshipType getAssemblyRelationship();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getAssemblyRelationship <em>Assembly Relationship</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Relationship</em>' containment reference.
	 * @see #getAssemblyRelationship()
	 * @generated
	 */
	void setAssemblyRelationship(AssemblyRelationshipType value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Material Requirement Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Requirement Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Requirement Property</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_MaterialRequirementProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialRequirementProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpMaterialRequirementPropertyType> getMaterialRequirementProperty();

	/**
	 * Returns the value of the '<em><b>Required By Requested Segment Response</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required By Requested Segment Response</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required By Requested Segment Response</em>' containment reference.
	 * @see #setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialRequirementType_RequiredByRequestedSegmentResponse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='RequiredByRequestedSegmentResponse' namespace='##targetNamespace'"
	 * @generated
	 */
	RequiredByRequestedSegmentResponseType getRequiredByRequestedSegmentResponse();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required By Requested Segment Response</em>' containment reference.
	 * @see #getRequiredByRequestedSegmentResponse()
	 * @generated
	 */
	void setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType value);

} // OpMaterialRequirementType
