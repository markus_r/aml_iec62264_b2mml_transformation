/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type133</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType133#getAcknowledge <em>Acknowledge</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType133#getPersonnelInformation <em>Personnel Information</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType133()
 * @model extendedMetaData="name='DataArea_._134_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType133 extends EObject {
	/**
	 * Returns the value of the '<em><b>Acknowledge</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge</em>' containment reference.
	 * @see #setAcknowledge(TransAcknowledgeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType133_Acknowledge()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Acknowledge' namespace='##targetNamespace'"
	 * @generated
	 */
	TransAcknowledgeType getAcknowledge();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DataAreaType133#getAcknowledge <em>Acknowledge</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge</em>' containment reference.
	 * @see #getAcknowledge()
	 * @generated
	 */
	void setAcknowledge(TransAcknowledgeType value);

	/**
	 * Returns the value of the '<em><b>Personnel Information</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PersonnelInformationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Information</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Information</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType133_PersonnelInformation()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PersonnelInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PersonnelInformationType> getPersonnelInformation();

} // DataAreaType133
