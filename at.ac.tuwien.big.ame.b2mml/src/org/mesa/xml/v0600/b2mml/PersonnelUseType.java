/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Personnel Use Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPersonnelUseType()
 * @model extendedMetaData="name='PersonnelUseType' kind='simple'"
 * @generated
 */
public interface PersonnelUseType extends CodeType {
} // PersonnelUseType
