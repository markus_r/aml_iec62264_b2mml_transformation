/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Request State1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getRequestState1Type()
 * @model extendedMetaData="name='RequestState1Type' kind='simple'"
 * @generated
 */
public interface RequestState1Type extends CodeType {
} // RequestState1Type
