/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Request ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsRequestIDType()
 * @model extendedMetaData="name='OperationsRequestIDType' kind='simple'"
 * @generated
 */
public interface OperationsRequestIDType extends IdentifierType {
} // OperationsRequestIDType
