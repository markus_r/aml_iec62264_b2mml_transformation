/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Storage Hierarchy Scope Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getStorageHierarchyScopeType()
 * @model extendedMetaData="name='StorageHierarchyScopeType' kind='simple'"
 * @generated
 */
public interface StorageHierarchyScopeType extends IdentifierType {
} // StorageHierarchyScopeType
