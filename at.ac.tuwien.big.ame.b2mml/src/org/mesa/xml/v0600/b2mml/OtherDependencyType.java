/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Other Dependency Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOtherDependencyType()
 * @model extendedMetaData="name='OtherDependencyType' kind='simple'"
 * @generated
 */
public interface OtherDependencyType extends CodeType {
} // OtherDependencyType
