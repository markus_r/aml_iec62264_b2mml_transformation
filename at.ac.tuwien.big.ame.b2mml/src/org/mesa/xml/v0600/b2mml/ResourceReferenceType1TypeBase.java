/**
 */
package org.mesa.xml.v0600.b2mml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Resource Reference Type1 Type Base</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getResourceReferenceType1TypeBase()
 * @model extendedMetaData="name='ResourceReferenceType1Type_._base'"
 * @generated
 */
public enum ResourceReferenceType1TypeBase implements Enumerator {
	/**
	 * The '<em><b>Personnel</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PERSONNEL_VALUE
	 * @generated
	 * @ordered
	 */
	PERSONNEL(0, "Personnel", "Personnel"),

	/**
	 * The '<em><b>Personnel Class</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PERSONNEL_CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	PERSONNEL_CLASS(1, "PersonnelClass", "Personnel Class"),

	/**
	 * The '<em><b>Equipment</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EQUIPMENT_VALUE
	 * @generated
	 * @ordered
	 */
	EQUIPMENT(2, "Equipment", "Equipment"),

	/**
	 * The '<em><b>Equipment Class</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EQUIPMENT_CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	EQUIPMENT_CLASS(3, "EquipmentClass", "Equipment Class"),

	/**
	 * The '<em><b>Material Class</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	MATERIAL_CLASS(4, "MaterialClass", "Material Class"),

	/**
	 * The '<em><b>Material Definition</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_DEFINITION_VALUE
	 * @generated
	 * @ordered
	 */
	MATERIAL_DEFINITION(5, "MaterialDefinition", "Material Definition"),

	/**
	 * The '<em><b>Material Lot</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_LOT_VALUE
	 * @generated
	 * @ordered
	 */
	MATERIAL_LOT(6, "MaterialLot", "Material Lot"),

	/**
	 * The '<em><b>Material Sublot</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_SUBLOT_VALUE
	 * @generated
	 * @ordered
	 */
	MATERIAL_SUBLOT(7, "MaterialSublot", "Material Sublot"),

	/**
	 * The '<em><b>Physical Asset</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PHYSICAL_ASSET_VALUE
	 * @generated
	 * @ordered
	 */
	PHYSICAL_ASSET(8, "PhysicalAsset", "Physical Asset"),

	/**
	 * The '<em><b>Physical Asset Class</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PHYSICAL_ASSET_CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	PHYSICAL_ASSET_CLASS(9, "PhysicalAssetClass", "Physical Asset Class"),

	/**
	 * The '<em><b>Work Master</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WORK_MASTER_VALUE
	 * @generated
	 * @ordered
	 */
	WORK_MASTER(10, "WorkMaster", "Work Master"),

	/**
	 * The '<em><b>Process Segment</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROCESS_SEGMENT_VALUE
	 * @generated
	 * @ordered
	 */
	PROCESS_SEGMENT(11, "ProcessSegment", "Process Segment"),

	/**
	 * The '<em><b>Other</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	OTHER(12, "Other", "Other");

	/**
	 * The '<em><b>Personnel</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Personnel</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PERSONNEL
	 * @model name="Personnel"
	 * @generated
	 * @ordered
	 */
	public static final int PERSONNEL_VALUE = 0;

	/**
	 * The '<em><b>Personnel Class</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Personnel Class</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PERSONNEL_CLASS
	 * @model name="PersonnelClass" literal="Personnel Class"
	 * @generated
	 * @ordered
	 */
	public static final int PERSONNEL_CLASS_VALUE = 1;

	/**
	 * The '<em><b>Equipment</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Equipment</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EQUIPMENT
	 * @model name="Equipment"
	 * @generated
	 * @ordered
	 */
	public static final int EQUIPMENT_VALUE = 2;

	/**
	 * The '<em><b>Equipment Class</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Equipment Class</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EQUIPMENT_CLASS
	 * @model name="EquipmentClass" literal="Equipment Class"
	 * @generated
	 * @ordered
	 */
	public static final int EQUIPMENT_CLASS_VALUE = 3;

	/**
	 * The '<em><b>Material Class</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Material Class</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_CLASS
	 * @model name="MaterialClass" literal="Material Class"
	 * @generated
	 * @ordered
	 */
	public static final int MATERIAL_CLASS_VALUE = 4;

	/**
	 * The '<em><b>Material Definition</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Material Definition</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_DEFINITION
	 * @model name="MaterialDefinition" literal="Material Definition"
	 * @generated
	 * @ordered
	 */
	public static final int MATERIAL_DEFINITION_VALUE = 5;

	/**
	 * The '<em><b>Material Lot</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Material Lot</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_LOT
	 * @model name="MaterialLot" literal="Material Lot"
	 * @generated
	 * @ordered
	 */
	public static final int MATERIAL_LOT_VALUE = 6;

	/**
	 * The '<em><b>Material Sublot</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Material Sublot</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_SUBLOT
	 * @model name="MaterialSublot" literal="Material Sublot"
	 * @generated
	 * @ordered
	 */
	public static final int MATERIAL_SUBLOT_VALUE = 7;

	/**
	 * The '<em><b>Physical Asset</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Physical Asset</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PHYSICAL_ASSET
	 * @model name="PhysicalAsset" literal="Physical Asset"
	 * @generated
	 * @ordered
	 */
	public static final int PHYSICAL_ASSET_VALUE = 8;

	/**
	 * The '<em><b>Physical Asset Class</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Physical Asset Class</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PHYSICAL_ASSET_CLASS
	 * @model name="PhysicalAssetClass" literal="Physical Asset Class"
	 * @generated
	 * @ordered
	 */
	public static final int PHYSICAL_ASSET_CLASS_VALUE = 9;

	/**
	 * The '<em><b>Work Master</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Work Master</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WORK_MASTER
	 * @model name="WorkMaster" literal="Work Master"
	 * @generated
	 * @ordered
	 */
	public static final int WORK_MASTER_VALUE = 10;

	/**
	 * The '<em><b>Process Segment</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Process Segment</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PROCESS_SEGMENT
	 * @model name="ProcessSegment" literal="Process Segment"
	 * @generated
	 * @ordered
	 */
	public static final int PROCESS_SEGMENT_VALUE = 11;

	/**
	 * The '<em><b>Other</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Other</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OTHER
	 * @model name="Other"
	 * @generated
	 * @ordered
	 */
	public static final int OTHER_VALUE = 12;

	/**
	 * An array of all the '<em><b>Resource Reference Type1 Type Base</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ResourceReferenceType1TypeBase[] VALUES_ARRAY =
		new ResourceReferenceType1TypeBase[] {
			PERSONNEL,
			PERSONNEL_CLASS,
			EQUIPMENT,
			EQUIPMENT_CLASS,
			MATERIAL_CLASS,
			MATERIAL_DEFINITION,
			MATERIAL_LOT,
			MATERIAL_SUBLOT,
			PHYSICAL_ASSET,
			PHYSICAL_ASSET_CLASS,
			WORK_MASTER,
			PROCESS_SEGMENT,
			OTHER,
		};

	/**
	 * A public read-only list of all the '<em><b>Resource Reference Type1 Type Base</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ResourceReferenceType1TypeBase> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Resource Reference Type1 Type Base</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ResourceReferenceType1TypeBase get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ResourceReferenceType1TypeBase result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Resource Reference Type1 Type Base</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ResourceReferenceType1TypeBase getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ResourceReferenceType1TypeBase result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Resource Reference Type1 Type Base</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ResourceReferenceType1TypeBase get(int value) {
		switch (value) {
			case PERSONNEL_VALUE: return PERSONNEL;
			case PERSONNEL_CLASS_VALUE: return PERSONNEL_CLASS;
			case EQUIPMENT_VALUE: return EQUIPMENT;
			case EQUIPMENT_CLASS_VALUE: return EQUIPMENT_CLASS;
			case MATERIAL_CLASS_VALUE: return MATERIAL_CLASS;
			case MATERIAL_DEFINITION_VALUE: return MATERIAL_DEFINITION;
			case MATERIAL_LOT_VALUE: return MATERIAL_LOT;
			case MATERIAL_SUBLOT_VALUE: return MATERIAL_SUBLOT;
			case PHYSICAL_ASSET_VALUE: return PHYSICAL_ASSET;
			case PHYSICAL_ASSET_CLASS_VALUE: return PHYSICAL_ASSET_CLASS;
			case WORK_MASTER_VALUE: return WORK_MASTER;
			case PROCESS_SEGMENT_VALUE: return PROCESS_SEGMENT;
			case OTHER_VALUE: return OTHER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ResourceReferenceType1TypeBase(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ResourceReferenceType1TypeBase
