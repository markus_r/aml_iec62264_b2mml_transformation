/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Class ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialClassIDType()
 * @model extendedMetaData="name='MaterialClassIDType' kind='simple'"
 * @generated
 */
public interface MaterialClassIDType extends IdentifierType {
} // MaterialClassIDType
