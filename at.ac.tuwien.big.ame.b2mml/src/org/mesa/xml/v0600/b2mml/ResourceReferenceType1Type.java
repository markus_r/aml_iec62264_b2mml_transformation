/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Reference Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getResourceReferenceType1Type()
 * @model extendedMetaData="name='ResourceReferenceType1Type' kind='simple'"
 * @generated
 */
public interface ResourceReferenceType1Type extends CodeType {
} // ResourceReferenceType1Type
