/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Date Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTestDateTimeType()
 * @model extendedMetaData="name='TestDateTimeType' kind='simple'"
 * @generated
 */
public interface TestDateTimeType extends DateTimeType {
} // TestDateTimeType
