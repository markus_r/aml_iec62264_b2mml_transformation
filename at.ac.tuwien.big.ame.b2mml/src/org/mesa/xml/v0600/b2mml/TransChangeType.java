/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Change Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransChangeType#getActionCriteria <em>Action Criteria</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransChangeType#getResponseCode <em>Response Code</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransChangeType()
 * @model extendedMetaData="name='TransChangeType' kind='elementOnly'"
 * @generated
 */
public interface TransChangeType extends EObject {
	/**
	 * Returns the value of the '<em><b>Action Criteria</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.TransActionCriteriaType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action Criteria</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Criteria</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransChangeType_ActionCriteria()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ActionCriteria' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TransActionCriteriaType> getActionCriteria();

	/**
	 * Returns the value of the '<em><b>Response Code</b></em>' attribute.
	 * The literals are from the enumeration {@link org.mesa.xml.v0600.b2mml.TransResponseCodeType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Response Code</em>' attribute.
	 * @see org.mesa.xml.v0600.b2mml.TransResponseCodeType
	 * @see #isSetResponseCode()
	 * @see #unsetResponseCode()
	 * @see #setResponseCode(TransResponseCodeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransChangeType_ResponseCode()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='responseCode'"
	 * @generated
	 */
	TransResponseCodeType getResponseCode();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TransChangeType#getResponseCode <em>Response Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Response Code</em>' attribute.
	 * @see org.mesa.xml.v0600.b2mml.TransResponseCodeType
	 * @see #isSetResponseCode()
	 * @see #unsetResponseCode()
	 * @see #getResponseCode()
	 * @generated
	 */
	void setResponseCode(TransResponseCodeType value);

	/**
	 * Unsets the value of the '{@link org.mesa.xml.v0600.b2mml.TransChangeType#getResponseCode <em>Response Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetResponseCode()
	 * @see #getResponseCode()
	 * @see #setResponseCode(TransResponseCodeType)
	 * @generated
	 */
	void unsetResponseCode();

	/**
	 * Returns whether the value of the '{@link org.mesa.xml.v0600.b2mml.TransChangeType#getResponseCode <em>Response Code</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Response Code</em>' attribute is set.
	 * @see #unsetResponseCode()
	 * @see #getResponseCode()
	 * @see #setResponseCode(TransResponseCodeType)
	 * @generated
	 */
	boolean isSetResponseCode();

} // TransChangeType
