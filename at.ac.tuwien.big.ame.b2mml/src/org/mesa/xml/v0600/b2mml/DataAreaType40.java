/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type40</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType40#getShow <em>Show</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType40#getOperationsCapability <em>Operations Capability</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType40()
 * @model extendedMetaData="name='DataArea_._41_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType40 extends EObject {
	/**
	 * Returns the value of the '<em><b>Show</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show</em>' containment reference.
	 * @see #setShow(TransShowType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType40_Show()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Show' namespace='##targetNamespace'"
	 * @generated
	 */
	TransShowType getShow();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DataAreaType40#getShow <em>Show</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show</em>' containment reference.
	 * @see #getShow()
	 * @generated
	 */
	void setShow(TransShowType value);

	/**
	 * Returns the value of the '<em><b>Operations Capability</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OperationsCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Capability</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType40_OperationsCapability()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OperationsCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsCapabilityType> getOperationsCapability();

} // DataAreaType40
