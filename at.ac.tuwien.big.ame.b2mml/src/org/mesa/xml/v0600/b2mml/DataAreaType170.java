/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type170</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType170#getGet <em>Get</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType170#getMaterialInformation <em>Material Information</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType170()
 * @model extendedMetaData="name='DataArea_._171_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType170 extends EObject {
	/**
	 * Returns the value of the '<em><b>Get</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get</em>' containment reference.
	 * @see #setGet(TransGetType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType170_Get()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Get' namespace='##targetNamespace'"
	 * @generated
	 */
	TransGetType getGet();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DataAreaType170#getGet <em>Get</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get</em>' containment reference.
	 * @see #getGet()
	 * @generated
	 */
	void setGet(TransGetType value);

	/**
	 * Returns the value of the '<em><b>Material Information</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialInformationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Information</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Information</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType170_MaterialInformation()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MaterialInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialInformationType> getMaterialInformation();

} // DataAreaType170
