/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Storage Location Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getStorageLocationType()
 * @model extendedMetaData="name='StorageLocationType' kind='simple'"
 * @generated
 */
public interface StorageLocationType extends IdentifierType {
} // StorageLocationType
