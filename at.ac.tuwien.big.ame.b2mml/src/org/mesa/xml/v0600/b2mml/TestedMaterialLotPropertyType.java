/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tested Material Lot Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TestedMaterialLotPropertyType#getMaterialLotID <em>Material Lot ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TestedMaterialLotPropertyType#getPropertyID <em>Property ID</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTestedMaterialLotPropertyType()
 * @model extendedMetaData="name='TestedMaterialLotPropertyType' kind='elementOnly'"
 * @generated
 */
public interface TestedMaterialLotPropertyType extends EObject {
	/**
	 * Returns the value of the '<em><b>Material Lot ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Lot ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Lot ID</em>' containment reference.
	 * @see #setMaterialLotID(MaterialLotIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTestedMaterialLotPropertyType_MaterialLotID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MaterialLotID' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialLotIDType getMaterialLotID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TestedMaterialLotPropertyType#getMaterialLotID <em>Material Lot ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Lot ID</em>' containment reference.
	 * @see #getMaterialLotID()
	 * @generated
	 */
	void setMaterialLotID(MaterialLotIDType value);

	/**
	 * Returns the value of the '<em><b>Property ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property ID</em>' containment reference.
	 * @see #setPropertyID(PropertyIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTestedMaterialLotPropertyType_PropertyID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PropertyID' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyIDType getPropertyID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TestedMaterialLotPropertyType#getPropertyID <em>Property ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property ID</em>' containment reference.
	 * @see #getPropertyID()
	 * @generated
	 */
	void setPropertyID(PropertyIDType value);

} // TestedMaterialLotPropertyType
