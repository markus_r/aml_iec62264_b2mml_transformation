/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Class Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialClassPropertyType#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialClassPropertyType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialClassPropertyType#getValue <em>Value</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialClassPropertyType#getMaterialClassProperty <em>Material Class Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialClassPropertyType#getMaterialTestSpecificationID <em>Material Test Specification ID</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialClassPropertyType()
 * @model extendedMetaData="name='MaterialClassPropertyType' kind='elementOnly'"
 * @generated
 */
public interface MaterialClassPropertyType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialClassPropertyType_ID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.MaterialClassPropertyType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialClassPropertyType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.ValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialClassPropertyType_Value()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Value' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ValueType> getValue();

	/**
	 * Returns the value of the '<em><b>Material Class Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialClassPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Class Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Class Property</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialClassPropertyType_MaterialClassProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialClassProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialClassPropertyType> getMaterialClassProperty();

	/**
	 * Returns the value of the '<em><b>Material Test Specification ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialTestSpecificationIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Test Specification ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Test Specification ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialClassPropertyType_MaterialTestSpecificationID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialTestSpecificationID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialTestSpecificationIDType> getMaterialTestSpecificationID();

} // MaterialClassPropertyType
