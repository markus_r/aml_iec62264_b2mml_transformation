/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Qualification Test Specification ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getQualificationTestSpecificationIDType()
 * @model extendedMetaData="name='QualificationTestSpecificationIDType' kind='simple'"
 * @generated
 */
public interface QualificationTestSpecificationIDType extends IdentifierType {
} // QualificationTestSpecificationIDType
