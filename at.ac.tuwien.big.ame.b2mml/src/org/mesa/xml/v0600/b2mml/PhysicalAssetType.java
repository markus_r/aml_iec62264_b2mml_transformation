/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getPhysicalLocation <em>Physical Location</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getFixedAssetID <em>Fixed Asset ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getVendorID <em>Vendor ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getEquipmentLevel <em>Equipment Level</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getEquipmentAssetMapping <em>Equipment Asset Mapping</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getPhysicalAssetProperty <em>Physical Asset Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getPhysicalAsset <em>Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getPhysicalAssetClassID <em>Physical Asset Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getPhysicalAssetCapabilityTestSpecificationID <em>Physical Asset Capability Test Specification ID</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType()
 * @model extendedMetaData="name='PhysicalAssetType' kind='elementOnly'"
 * @generated
 */
public interface PhysicalAssetType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType_ID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Physical Location</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Location</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Location</em>' containment reference.
	 * @see #setPhysicalLocation(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType_PhysicalLocation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalLocation' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getPhysicalLocation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getPhysicalLocation <em>Physical Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Location</em>' containment reference.
	 * @see #getPhysicalLocation()
	 * @generated
	 */
	void setPhysicalLocation(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Fixed Asset ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fixed Asset ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fixed Asset ID</em>' containment reference.
	 * @see #setFixedAssetID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType_FixedAssetID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FixedAssetID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getFixedAssetID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getFixedAssetID <em>Fixed Asset ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fixed Asset ID</em>' containment reference.
	 * @see #getFixedAssetID()
	 * @generated
	 */
	void setFixedAssetID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Vendor ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vendor ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vendor ID</em>' containment reference.
	 * @see #setVendorID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType_VendorID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='VendorID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getVendorID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getVendorID <em>Vendor ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vendor ID</em>' containment reference.
	 * @see #getVendorID()
	 * @generated
	 */
	void setVendorID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Equipment Level</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Level</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Level</em>' containment reference.
	 * @see #setEquipmentLevel(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType_EquipmentLevel()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentLevel' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getEquipmentLevel();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.PhysicalAssetType#getEquipmentLevel <em>Equipment Level</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Level</em>' containment reference.
	 * @see #getEquipmentLevel()
	 * @generated
	 */
	void setEquipmentLevel(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Equipment Asset Mapping</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.EquipmentAssetMappingType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Asset Mapping</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Asset Mapping</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType_EquipmentAssetMapping()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentAssetMapping' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentAssetMappingType> getEquipmentAssetMapping();

	/**
	 * Returns the value of the '<em><b>Physical Asset Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PhysicalAssetPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Property</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType_PhysicalAssetProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetPropertyType> getPhysicalAssetProperty();

	/**
	 * Returns the value of the '<em><b>Physical Asset</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PhysicalAssetType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType_PhysicalAsset()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAsset' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetType> getPhysicalAsset();

	/**
	 * Returns the value of the '<em><b>Physical Asset Class ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PhysicalAssetClassIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Class ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Class ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType_PhysicalAssetClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetClassIDType> getPhysicalAssetClassID();

	/**
	 * Returns the value of the '<em><b>Physical Asset Capability Test Specification ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PhysicalAssetCapabilityTestSpecificationIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Capability Test Specification ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Capability Test Specification ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetType_PhysicalAssetCapabilityTestSpecificationID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetCapabilityTestSpecificationID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetCapabilityTestSpecificationIDType> getPhysicalAssetCapabilityTestSpecificationID();

} // PhysicalAssetType
