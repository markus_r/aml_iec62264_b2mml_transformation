/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assembly Relationship1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getAssemblyRelationship1Type()
 * @model extendedMetaData="name='AssemblyRelationship1Type' kind='simple'"
 * @generated
 */
public interface AssemblyRelationship1Type extends CodeType {
} // AssemblyRelationship1Type
