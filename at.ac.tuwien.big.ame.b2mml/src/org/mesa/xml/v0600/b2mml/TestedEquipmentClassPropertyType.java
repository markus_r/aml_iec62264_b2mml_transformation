/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tested Equipment Class Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TestedEquipmentClassPropertyType#getEquipmentClassID <em>Equipment Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TestedEquipmentClassPropertyType#getPropertyID <em>Property ID</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTestedEquipmentClassPropertyType()
 * @model extendedMetaData="name='TestedEquipmentClassPropertyType' kind='elementOnly'"
 * @generated
 */
public interface TestedEquipmentClassPropertyType extends EObject {
	/**
	 * Returns the value of the '<em><b>Equipment Class ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Class ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Class ID</em>' containment reference.
	 * @see #setEquipmentClassID(EquipmentClassIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTestedEquipmentClassPropertyType_EquipmentClassID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='EquipmentClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentClassIDType getEquipmentClassID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TestedEquipmentClassPropertyType#getEquipmentClassID <em>Equipment Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Class ID</em>' containment reference.
	 * @see #getEquipmentClassID()
	 * @generated
	 */
	void setEquipmentClassID(EquipmentClassIDType value);

	/**
	 * Returns the value of the '<em><b>Property ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property ID</em>' containment reference.
	 * @see #setPropertyID(PropertyIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTestedEquipmentClassPropertyType_PropertyID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PropertyID' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyIDType getPropertyID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TestedEquipmentClassPropertyType#getPropertyID <em>Property ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property ID</em>' containment reference.
	 * @see #getPropertyID()
	 * @generated
	 */
	void setPropertyID(PropertyIDType value);

} // TestedEquipmentClassPropertyType
