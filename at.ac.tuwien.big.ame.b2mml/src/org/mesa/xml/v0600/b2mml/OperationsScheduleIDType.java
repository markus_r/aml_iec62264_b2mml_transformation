/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Schedule ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsScheduleIDType()
 * @model extendedMetaData="name='OperationsScheduleIDType' kind='simple'"
 * @generated
 */
public interface OperationsScheduleIDType extends IdentifierType {
} // OperationsScheduleIDType
