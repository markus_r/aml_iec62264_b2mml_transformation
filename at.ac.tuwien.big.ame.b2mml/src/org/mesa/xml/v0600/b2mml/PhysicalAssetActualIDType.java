/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Actual ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetActualIDType()
 * @model extendedMetaData="name='PhysicalAssetActualIDType' kind='simple'"
 * @generated
 */
public interface PhysicalAssetActualIDType extends IdentifierType {
} // PhysicalAssetActualIDType
