/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type44</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType44#getShow <em>Show</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType44#getPersonnelInformation <em>Personnel Information</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType44()
 * @model extendedMetaData="name='DataArea_._45_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType44 extends EObject {
	/**
	 * Returns the value of the '<em><b>Show</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show</em>' containment reference.
	 * @see #setShow(TransShowType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType44_Show()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Show' namespace='##targetNamespace'"
	 * @generated
	 */
	TransShowType getShow();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DataAreaType44#getShow <em>Show</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show</em>' containment reference.
	 * @see #getShow()
	 * @generated
	 */
	void setShow(TransShowType value);

	/**
	 * Returns the value of the '<em><b>Personnel Information</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PersonnelInformationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Information</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Information</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType44_PersonnelInformation()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PersonnelInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PersonnelInformationType> getPersonnelInformation();

} // DataAreaType44
