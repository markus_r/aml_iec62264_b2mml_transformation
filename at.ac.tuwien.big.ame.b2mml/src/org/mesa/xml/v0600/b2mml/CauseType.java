/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cause Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getCauseType()
 * @model extendedMetaData="name='CauseType' kind='simple'"
 * @generated
 */
public interface CauseType extends CodeType {
} // CauseType
