/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Performance Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getOperationsScheduleID <em>Operations Schedule ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getEndTime <em>End Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getPerformanceState <em>Performance State</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getOperationsResponse <em>Operations Response</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsPerformanceType()
 * @model extendedMetaData="name='OperationsPerformanceType' kind='elementOnly'"
 * @generated
 */
public interface OperationsPerformanceType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsPerformanceType_ID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsPerformanceType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsPerformanceType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' containment reference.
	 * @see #setOperationsType(OperationsTypeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsPerformanceType_OperationsType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsType' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsTypeType getOperationsType();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getOperationsType <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' containment reference.
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsTypeType value);

	/**
	 * Returns the value of the '<em><b>Operations Schedule ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Schedule ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Schedule ID</em>' containment reference.
	 * @see #setOperationsScheduleID(OperationsScheduleIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsPerformanceType_OperationsScheduleID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsScheduleID' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsScheduleIDType getOperationsScheduleID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getOperationsScheduleID <em>Operations Schedule ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Schedule ID</em>' containment reference.
	 * @see #getOperationsScheduleID()
	 * @generated
	 */
	void setOperationsScheduleID(OperationsScheduleIDType value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' containment reference.
	 * @see #setStartTime(StartTimeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsPerformanceType_StartTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='StartTime' namespace='##targetNamespace'"
	 * @generated
	 */
	StartTimeType getStartTime();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getStartTime <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' containment reference.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(StartTimeType value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' containment reference.
	 * @see #setEndTime(EndTimeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsPerformanceType_EndTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EndTime' namespace='##targetNamespace'"
	 * @generated
	 */
	EndTimeType getEndTime();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getEndTime <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' containment reference.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(EndTimeType value);

	/**
	 * Returns the value of the '<em><b>Performance State</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Performance State</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Performance State</em>' containment reference.
	 * @see #setPerformanceState(ResponseStateType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsPerformanceType_PerformanceState()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PerformanceState' namespace='##targetNamespace'"
	 * @generated
	 */
	ResponseStateType getPerformanceState();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getPerformanceState <em>Performance State</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Performance State</em>' containment reference.
	 * @see #getPerformanceState()
	 * @generated
	 */
	void setPerformanceState(ResponseStateType value);

	/**
	 * Returns the value of the '<em><b>Published Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Published Date</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Published Date</em>' containment reference.
	 * @see #setPublishedDate(PublishedDateType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsPerformanceType_PublishedDate()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PublishedDate' namespace='##targetNamespace'"
	 * @generated
	 */
	PublishedDateType getPublishedDate();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType#getPublishedDate <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Published Date</em>' containment reference.
	 * @see #getPublishedDate()
	 * @generated
	 */
	void setPublishedDate(PublishedDateType value);

	/**
	 * Returns the value of the '<em><b>Operations Response</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OperationsResponseType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Response</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Response</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsPerformanceType_OperationsResponse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsResponse' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsResponseType> getOperationsResponse();

} // OperationsPerformanceType
