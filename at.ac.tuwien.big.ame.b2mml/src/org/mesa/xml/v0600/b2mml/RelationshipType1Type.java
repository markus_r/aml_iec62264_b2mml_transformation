/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relationship Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getRelationshipType1Type()
 * @model extendedMetaData="name='RelationshipType1Type' kind='simple'"
 * @generated
 */
public interface RelationshipType1Type extends CodeType {
} // RelationshipType1Type
