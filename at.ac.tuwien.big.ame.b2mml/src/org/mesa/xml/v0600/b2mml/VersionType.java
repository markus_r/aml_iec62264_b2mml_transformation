/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Version Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getVersionType()
 * @model extendedMetaData="name='VersionType' kind='simple'"
 * @generated
 */
public interface VersionType extends IdentifierType {
} // VersionType
