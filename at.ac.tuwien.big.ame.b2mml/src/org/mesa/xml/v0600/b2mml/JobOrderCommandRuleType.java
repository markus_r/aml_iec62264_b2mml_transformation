/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Job Order Command Rule Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getJobOrderCommandRuleType()
 * @model extendedMetaData="name='JobOrderCommandRuleType' kind='simple'"
 * @generated
 */
public interface JobOrderCommandRuleType extends TextType {
} // JobOrderCommandRuleType
