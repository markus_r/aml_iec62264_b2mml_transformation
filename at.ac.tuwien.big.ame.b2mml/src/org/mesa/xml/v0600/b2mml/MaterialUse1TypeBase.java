/**
 */
package org.mesa.xml.v0600.b2mml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Material Use1 Type Base</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialUse1TypeBase()
 * @model extendedMetaData="name='MaterialUse1Type_._base'"
 * @generated
 */
public enum MaterialUse1TypeBase implements Enumerator {
	/**
	 * The '<em><b>Consumed</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONSUMED_VALUE
	 * @generated
	 * @ordered
	 */
	CONSUMED(0, "Consumed", "Consumed"),

	/**
	 * The '<em><b>Produced</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PRODUCED_VALUE
	 * @generated
	 * @ordered
	 */
	PRODUCED(1, "Produced", "Produced"),

	/**
	 * The '<em><b>Consumable</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONSUMABLE_VALUE
	 * @generated
	 * @ordered
	 */
	CONSUMABLE(2, "Consumable", "Consumable"),

	/**
	 * The '<em><b>Replaced Assetn</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REPLACED_ASSETN_VALUE
	 * @generated
	 * @ordered
	 */
	REPLACED_ASSETN(3, "ReplacedAssetn", "Replaced Assetn"),

	/**
	 * The '<em><b>Replacement Asset</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REPLACEMENT_ASSET_VALUE
	 * @generated
	 * @ordered
	 */
	REPLACEMENT_ASSET(4, "ReplacementAsset", "Replacement Asset"),

	/**
	 * The '<em><b>Sample</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAMPLE_VALUE
	 * @generated
	 * @ordered
	 */
	SAMPLE(5, "Sample", "Sample"),

	/**
	 * The '<em><b>Resurned Sample</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RESURNED_SAMPLE_VALUE
	 * @generated
	 * @ordered
	 */
	RESURNED_SAMPLE(6, "ResurnedSample", "Resurned Sample"),

	/**
	 * The '<em><b>Carrier</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CARRIER_VALUE
	 * @generated
	 * @ordered
	 */
	CARRIER(7, "Carrier", "Carrier"),

	/**
	 * The '<em><b>Returned Carrier</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RETURNED_CARRIER_VALUE
	 * @generated
	 * @ordered
	 */
	RETURNED_CARRIER(8, "ReturnedCarrier", "Returned Carrier"),

	/**
	 * The '<em><b>Other</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	OTHER(9, "Other", "Other");

	/**
	 * The '<em><b>Consumed</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Consumed</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSUMED
	 * @model name="Consumed"
	 * @generated
	 * @ordered
	 */
	public static final int CONSUMED_VALUE = 0;

	/**
	 * The '<em><b>Produced</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Produced</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PRODUCED
	 * @model name="Produced"
	 * @generated
	 * @ordered
	 */
	public static final int PRODUCED_VALUE = 1;

	/**
	 * The '<em><b>Consumable</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Consumable</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSUMABLE
	 * @model name="Consumable"
	 * @generated
	 * @ordered
	 */
	public static final int CONSUMABLE_VALUE = 2;

	/**
	 * The '<em><b>Replaced Assetn</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Replaced Assetn</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REPLACED_ASSETN
	 * @model name="ReplacedAssetn" literal="Replaced Assetn"
	 * @generated
	 * @ordered
	 */
	public static final int REPLACED_ASSETN_VALUE = 3;

	/**
	 * The '<em><b>Replacement Asset</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Replacement Asset</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REPLACEMENT_ASSET
	 * @model name="ReplacementAsset" literal="Replacement Asset"
	 * @generated
	 * @ordered
	 */
	public static final int REPLACEMENT_ASSET_VALUE = 4;

	/**
	 * The '<em><b>Sample</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Sample</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SAMPLE
	 * @model name="Sample"
	 * @generated
	 * @ordered
	 */
	public static final int SAMPLE_VALUE = 5;

	/**
	 * The '<em><b>Resurned Sample</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Resurned Sample</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RESURNED_SAMPLE
	 * @model name="ResurnedSample" literal="Resurned Sample"
	 * @generated
	 * @ordered
	 */
	public static final int RESURNED_SAMPLE_VALUE = 6;

	/**
	 * The '<em><b>Carrier</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Carrier</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CARRIER
	 * @model name="Carrier"
	 * @generated
	 * @ordered
	 */
	public static final int CARRIER_VALUE = 7;

	/**
	 * The '<em><b>Returned Carrier</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Returned Carrier</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RETURNED_CARRIER
	 * @model name="ReturnedCarrier" literal="Returned Carrier"
	 * @generated
	 * @ordered
	 */
	public static final int RETURNED_CARRIER_VALUE = 8;

	/**
	 * The '<em><b>Other</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Other</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OTHER
	 * @model name="Other"
	 * @generated
	 * @ordered
	 */
	public static final int OTHER_VALUE = 9;

	/**
	 * An array of all the '<em><b>Material Use1 Type Base</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MaterialUse1TypeBase[] VALUES_ARRAY =
		new MaterialUse1TypeBase[] {
			CONSUMED,
			PRODUCED,
			CONSUMABLE,
			REPLACED_ASSETN,
			REPLACEMENT_ASSET,
			SAMPLE,
			RESURNED_SAMPLE,
			CARRIER,
			RETURNED_CARRIER,
			OTHER,
		};

	/**
	 * A public read-only list of all the '<em><b>Material Use1 Type Base</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MaterialUse1TypeBase> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Material Use1 Type Base</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MaterialUse1TypeBase get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MaterialUse1TypeBase result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Material Use1 Type Base</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MaterialUse1TypeBase getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MaterialUse1TypeBase result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Material Use1 Type Base</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MaterialUse1TypeBase get(int value) {
		switch (value) {
			case CONSUMED_VALUE: return CONSUMED;
			case PRODUCED_VALUE: return PRODUCED;
			case CONSUMABLE_VALUE: return CONSUMABLE;
			case REPLACED_ASSETN_VALUE: return REPLACED_ASSETN;
			case REPLACEMENT_ASSET_VALUE: return REPLACEMENT_ASSET;
			case SAMPLE_VALUE: return SAMPLE;
			case RESURNED_SAMPLE_VALUE: return RESURNED_SAMPLE;
			case CARRIER_VALUE: return CARRIER;
			case RETURNED_CARRIER_VALUE: return RETURNED_CARRIER;
			case OTHER_VALUE: return OTHER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MaterialUse1TypeBase(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //MaterialUse1TypeBase
