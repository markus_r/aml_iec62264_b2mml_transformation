/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Definition ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialDefinitionIDType()
 * @model extendedMetaData="name='MaterialDefinitionIDType' kind='simple'"
 * @generated
 */
public interface MaterialDefinitionIDType extends IdentifierType {
} // MaterialDefinitionIDType
