/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unit Of Measure Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getUnitOfMeasureType()
 * @model extendedMetaData="name='UnitOfMeasureType' kind='simple'"
 * @generated
 */
public interface UnitOfMeasureType extends CodeType {
} // UnitOfMeasureType
