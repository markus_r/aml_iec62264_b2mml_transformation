/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Sub Lot ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSubLotIDType()
 * @model extendedMetaData="name='MaterialSubLotIDType' kind='simple'"
 * @generated
 */
public interface MaterialSubLotIDType extends IdentifierType {
} // MaterialSubLotIDType
