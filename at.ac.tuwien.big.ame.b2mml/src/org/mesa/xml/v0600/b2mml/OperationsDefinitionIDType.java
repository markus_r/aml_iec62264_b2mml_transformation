/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Definition ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionIDType()
 * @model extendedMetaData="name='OperationsDefinitionIDType' kind='simple'"
 * @generated
 */
public interface OperationsDefinitionIDType extends IdentifierType {
} // OperationsDefinitionIDType
