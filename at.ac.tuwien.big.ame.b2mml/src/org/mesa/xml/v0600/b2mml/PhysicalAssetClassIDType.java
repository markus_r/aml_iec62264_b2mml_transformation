/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Class ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetClassIDType()
 * @model extendedMetaData="name='PhysicalAssetClassIDType' kind='simple'"
 * @generated
 */
public interface PhysicalAssetClassIDType extends IdentifierType {
} // PhysicalAssetClassIDType
