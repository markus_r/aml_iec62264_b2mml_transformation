/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Planned Finish Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPlannedFinishTimeType()
 * @model extendedMetaData="name='PlannedFinishTimeType' kind='simple'"
 * @generated
 */
public interface PlannedFinishTimeType extends DateTimeType {
} // PlannedFinishTimeType
