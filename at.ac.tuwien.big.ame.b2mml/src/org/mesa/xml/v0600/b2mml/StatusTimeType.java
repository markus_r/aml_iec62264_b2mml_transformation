/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Status Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getStatusTimeType()
 * @model extendedMetaData="name='StatusTimeType' kind='simple'"
 * @generated
 */
public interface StatusTimeType extends DateTimeType {
} // StatusTimeType
