/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Earliest Start Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEarliestStartTimeType()
 * @model extendedMetaData="name='EarliestStartTimeType' kind='simple'"
 * @generated
 */
public interface EarliestStartTimeType extends DateTimeType {
} // EarliestStartTimeType
