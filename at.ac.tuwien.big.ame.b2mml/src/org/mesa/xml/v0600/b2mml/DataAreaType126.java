/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type126</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType126#getChange <em>Change</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType126#getOperationsPerformance <em>Operations Performance</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType126()
 * @model extendedMetaData="name='DataArea_._127_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType126 extends EObject {
	/**
	 * Returns the value of the '<em><b>Change</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change</em>' containment reference.
	 * @see #setChange(TransChangeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType126_Change()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Change' namespace='##targetNamespace'"
	 * @generated
	 */
	TransChangeType getChange();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DataAreaType126#getChange <em>Change</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change</em>' containment reference.
	 * @see #getChange()
	 * @generated
	 */
	void setChange(TransChangeType value);

	/**
	 * Returns the value of the '<em><b>Operations Performance</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OperationsPerformanceType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Performance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Performance</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType126_OperationsPerformance()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OperationsPerformance' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsPerformanceType> getOperationsPerformance();

} // DataAreaType126
