/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Op Material Capability Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getMaterialClassID <em>Material Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getMaterialDefinitionID <em>Material Definition ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getMaterialLotID <em>Material Lot ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getMaterialSubLotID <em>Material Sub Lot ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getReason <em>Reason</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getEndTime <em>End Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getAssemblyCapability <em>Assembly Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getMaterialCapabilityProperty <em>Material Capability Property</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType()
 * @model extendedMetaData="name='OpMaterialCapabilityType' kind='elementOnly'"
 * @generated
 */
public interface OpMaterialCapabilityType extends EObject {
	/**
	 * Returns the value of the '<em><b>Material Class ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialClassIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Class ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Class ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_MaterialClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialClassIDType> getMaterialClassID();

	/**
	 * Returns the value of the '<em><b>Material Definition ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialDefinitionIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Definition ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Definition ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_MaterialDefinitionID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialDefinitionID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialDefinitionIDType> getMaterialDefinitionID();

	/**
	 * Returns the value of the '<em><b>Material Lot ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialLotIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Lot ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Lot ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_MaterialLotID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialLotID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialLotIDType> getMaterialLotID();

	/**
	 * Returns the value of the '<em><b>Material Sub Lot ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialSubLotIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Sub Lot ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Sub Lot ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_MaterialSubLotID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialSubLotID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialSubLotIDType> getMaterialSubLotID();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Capability Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capability Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capability Type</em>' containment reference.
	 * @see #setCapabilityType(CapabilityTypeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_CapabilityType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='CapabilityType' namespace='##targetNamespace'"
	 * @generated
	 */
	CapabilityTypeType getCapabilityType();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getCapabilityType <em>Capability Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Capability Type</em>' containment reference.
	 * @see #getCapabilityType()
	 * @generated
	 */
	void setCapabilityType(CapabilityTypeType value);

	/**
	 * Returns the value of the '<em><b>Reason</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reason</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reason</em>' containment reference.
	 * @see #setReason(ReasonType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_Reason()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Reason' namespace='##targetNamespace'"
	 * @generated
	 */
	ReasonType getReason();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getReason <em>Reason</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reason</em>' containment reference.
	 * @see #getReason()
	 * @generated
	 */
	void setReason(ReasonType value);

	/**
	 * Returns the value of the '<em><b>Confidence Factor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Confidence Factor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confidence Factor</em>' containment reference.
	 * @see #setConfidenceFactor(ConfidenceFactorType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_ConfidenceFactor()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ConfidenceFactor' namespace='##targetNamespace'"
	 * @generated
	 */
	ConfidenceFactorType getConfidenceFactor();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getConfidenceFactor <em>Confidence Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confidence Factor</em>' containment reference.
	 * @see #getConfidenceFactor()
	 * @generated
	 */
	void setConfidenceFactor(ConfidenceFactorType value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Material Use</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Use</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Use</em>' containment reference.
	 * @see #setMaterialUse(MaterialUseType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_MaterialUse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialUse' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialUseType getMaterialUse();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getMaterialUse <em>Material Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Use</em>' containment reference.
	 * @see #getMaterialUse()
	 * @generated
	 */
	void setMaterialUse(MaterialUseType value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' containment reference.
	 * @see #setStartTime(StartTimeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_StartTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='StartTime' namespace='##targetNamespace'"
	 * @generated
	 */
	StartTimeType getStartTime();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getStartTime <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' containment reference.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(StartTimeType value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' containment reference.
	 * @see #setEndTime(EndTimeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_EndTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EndTime' namespace='##targetNamespace'"
	 * @generated
	 */
	EndTimeType getEndTime();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getEndTime <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' containment reference.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(EndTimeType value);

	/**
	 * Returns the value of the '<em><b>Assembly Capability</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Capability</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_AssemblyCapability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpMaterialCapabilityType> getAssemblyCapability();

	/**
	 * Returns the value of the '<em><b>Assembly Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Type</em>' containment reference.
	 * @see #setAssemblyType(AssemblyTypeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_AssemblyType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyType' namespace='##targetNamespace'"
	 * @generated
	 */
	AssemblyTypeType getAssemblyType();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getAssemblyType <em>Assembly Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Type</em>' containment reference.
	 * @see #getAssemblyType()
	 * @generated
	 */
	void setAssemblyType(AssemblyTypeType value);

	/**
	 * Returns the value of the '<em><b>Assembly Relationship</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Relationship</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Relationship</em>' containment reference.
	 * @see #setAssemblyRelationship(AssemblyRelationshipType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_AssemblyRelationship()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyRelationship' namespace='##targetNamespace'"
	 * @generated
	 */
	AssemblyRelationshipType getAssemblyRelationship();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType#getAssemblyRelationship <em>Assembly Relationship</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Relationship</em>' containment reference.
	 * @see #getAssemblyRelationship()
	 * @generated
	 */
	void setAssemblyRelationship(AssemblyRelationshipType value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

	/**
	 * Returns the value of the '<em><b>Material Capability Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Capability Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Capability Property</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpMaterialCapabilityType_MaterialCapabilityProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialCapabilityProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpMaterialCapabilityPropertyType> getMaterialCapabilityProperty();

} // OpMaterialCapabilityType
