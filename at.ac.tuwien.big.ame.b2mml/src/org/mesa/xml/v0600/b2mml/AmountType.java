/**
 */
package org.mesa.xml.v0600.b2mml;

import java.math.BigDecimal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Amount Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.AmountType#getValue <em>Value</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.AmountType#getCurrencyCodeListVersionID <em>Currency Code List Version ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.AmountType#getCurrencyID <em>Currency ID</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getAmountType()
 * @model extendedMetaData="name='AmountType' kind='simple'"
 * @generated
 */
public interface AmountType extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(BigDecimal)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getAmountType_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Decimal"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	BigDecimal getValue();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.AmountType#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Currency Code List Version ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Currency Code List Version ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Currency Code List Version ID</em>' attribute.
	 * @see #setCurrencyCodeListVersionID(String)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getAmountType_CurrencyCodeListVersionID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='currencyCodeListVersionID'"
	 * @generated
	 */
	String getCurrencyCodeListVersionID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.AmountType#getCurrencyCodeListVersionID <em>Currency Code List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Currency Code List Version ID</em>' attribute.
	 * @see #getCurrencyCodeListVersionID()
	 * @generated
	 */
	void setCurrencyCodeListVersionID(String value);

	/**
	 * Returns the value of the '<em><b>Currency ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Currency ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Currency ID</em>' attribute.
	 * @see #setCurrencyID(String)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getAmountType_CurrencyID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='currencyID'"
	 * @generated
	 */
	String getCurrencyID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.AmountType#getCurrencyID <em>Currency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Currency ID</em>' attribute.
	 * @see #getCurrencyID()
	 * @generated
	 */
	void setCurrencyID(String value);

} // AmountType
