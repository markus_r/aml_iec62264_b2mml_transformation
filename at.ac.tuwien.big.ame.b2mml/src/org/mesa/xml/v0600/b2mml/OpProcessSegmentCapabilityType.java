/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Op Process Segment Capability Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getProcessSegmentID <em>Process Segment ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getReason <em>Reason</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getEquipmentElementLevel <em>Equipment Element Level</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getEndTime <em>End Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getPersonnelCapability <em>Personnel Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getEquipmentCapability <em>Equipment Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getPhysicalAssetCapability <em>Physical Asset Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getMaterialCapability <em>Material Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getProcessSegmentCapability <em>Process Segment Capability</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType()
 * @model extendedMetaData="name='OpProcessSegmentCapabilityType' kind='elementOnly'"
 * @generated
 */
public interface OpProcessSegmentCapabilityType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_ID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Process Segment ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.ProcessSegmentIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_ProcessSegmentID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ProcessSegmentID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ProcessSegmentIDType> getProcessSegmentID();

	/**
	 * Returns the value of the '<em><b>Capability Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capability Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capability Type</em>' containment reference.
	 * @see #setCapabilityType(CapabilityTypeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_CapabilityType()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='CapabilityType' namespace='##targetNamespace'"
	 * @generated
	 */
	CapabilityTypeType getCapabilityType();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getCapabilityType <em>Capability Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Capability Type</em>' containment reference.
	 * @see #getCapabilityType()
	 * @generated
	 */
	void setCapabilityType(CapabilityTypeType value);

	/**
	 * Returns the value of the '<em><b>Reason</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.ReasonType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reason</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reason</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_Reason()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Reason' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ReasonType> getReason();

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.HierarchyScopeType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<HierarchyScopeType> getHierarchyScope();

	/**
	 * Returns the value of the '<em><b>Equipment Element Level</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.EquipmentElementLevelType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Element Level</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Element Level</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_EquipmentElementLevel()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentElementLevel' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentElementLevelType> getEquipmentElementLevel();

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' containment reference.
	 * @see #setStartTime(StartTimeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_StartTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='StartTime' namespace='##targetNamespace'"
	 * @generated
	 */
	StartTimeType getStartTime();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getStartTime <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' containment reference.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(StartTimeType value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' containment reference.
	 * @see #setEndTime(EndTimeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_EndTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EndTime' namespace='##targetNamespace'"
	 * @generated
	 */
	EndTimeType getEndTime();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType#getEndTime <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' containment reference.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(EndTimeType value);

	/**
	 * Returns the value of the '<em><b>Personnel Capability</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Capability</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_PersonnelCapability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonnelCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPersonnelCapabilityType> getPersonnelCapability();

	/**
	 * Returns the value of the '<em><b>Equipment Capability</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpEquipmentCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Capability</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_EquipmentCapability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpEquipmentCapabilityType> getEquipmentCapability();

	/**
	 * Returns the value of the '<em><b>Physical Asset Capability</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Capability</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_PhysicalAssetCapability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPhysicalAssetCapabilityType> getPhysicalAssetCapability();

	/**
	 * Returns the value of the '<em><b>Material Capability</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Capability</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_MaterialCapability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpMaterialCapabilityType> getMaterialCapability();

	/**
	 * Returns the value of the '<em><b>Process Segment Capability</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment Capability</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment Capability</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpProcessSegmentCapabilityType_ProcessSegmentCapability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ProcessSegmentCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpProcessSegmentCapabilityType> getProcessSegmentCapability();

} // OpProcessSegmentCapabilityType
