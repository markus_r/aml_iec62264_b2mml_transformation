/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Use Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetUseType()
 * @model extendedMetaData="name='PhysicalAssetUseType' kind='simple'"
 * @generated
 */
public interface PhysicalAssetUseType extends CodeType {
} // PhysicalAssetUseType
