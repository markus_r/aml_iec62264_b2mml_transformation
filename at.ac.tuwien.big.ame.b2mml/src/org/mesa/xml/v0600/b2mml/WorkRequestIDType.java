/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Work Request ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getWorkRequestIDType()
 * @model extendedMetaData="name='WorkRequestIDType' kind='simple'"
 * @generated
 */
public interface WorkRequestIDType extends IdentifierType {
} // WorkRequestIDType
