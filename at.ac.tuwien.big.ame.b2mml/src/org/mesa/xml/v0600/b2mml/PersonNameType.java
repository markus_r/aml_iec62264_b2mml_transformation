/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person Name Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPersonNameType()
 * @model extendedMetaData="name='PersonNameType' kind='simple'"
 * @generated
 */
public interface PersonNameType extends IdentifierType {
} // PersonNameType
