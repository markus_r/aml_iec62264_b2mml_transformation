/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Op Equipment Requirement Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getEquipmentClassID <em>Equipment Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getEquipmentID <em>Equipment ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getEquipmentUse <em>Equipment Use</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getEquipmentLevel <em>Equipment Level</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getEquipmentRequirementProperty <em>Equipment Requirement Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpEquipmentRequirementType()
 * @model extendedMetaData="name='OpEquipmentRequirementType' kind='elementOnly'"
 * @generated
 */
public interface OpEquipmentRequirementType extends EObject {
	/**
	 * Returns the value of the '<em><b>Equipment Class ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.EquipmentClassIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Class ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Class ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpEquipmentRequirementType_EquipmentClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentClassIDType> getEquipmentClassID();

	/**
	 * Returns the value of the '<em><b>Equipment ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.EquipmentIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpEquipmentRequirementType_EquipmentID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentIDType> getEquipmentID();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpEquipmentRequirementType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Equipment Use</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Use</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Use</em>' containment reference.
	 * @see #setEquipmentUse(EquipmentUseType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpEquipmentRequirementType_EquipmentUse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentUse' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentUseType getEquipmentUse();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getEquipmentUse <em>Equipment Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Use</em>' containment reference.
	 * @see #getEquipmentUse()
	 * @generated
	 */
	void setEquipmentUse(EquipmentUseType value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpEquipmentRequirementType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpEquipmentRequirementType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Equipment Level</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Level</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Level</em>' containment reference.
	 * @see #setEquipmentLevel(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpEquipmentRequirementType_EquipmentLevel()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentLevel' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getEquipmentLevel();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getEquipmentLevel <em>Equipment Level</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Level</em>' containment reference.
	 * @see #getEquipmentLevel()
	 * @generated
	 */
	void setEquipmentLevel(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Equipment Requirement Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Requirement Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Requirement Property</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpEquipmentRequirementType_EquipmentRequirementProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentRequirementProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpEquipmentRequirementPropertyType> getEquipmentRequirementProperty();

	/**
	 * Returns the value of the '<em><b>Required By Requested Segment Response</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required By Requested Segment Response</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required By Requested Segment Response</em>' containment reference.
	 * @see #setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpEquipmentRequirementType_RequiredByRequestedSegmentResponse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='RequiredByRequestedSegmentResponse' namespace='##targetNamespace'"
	 * @generated
	 */
	RequiredByRequestedSegmentResponseType getRequiredByRequestedSegmentResponse();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required By Requested Segment Response</em>' containment reference.
	 * @see #getRequiredByRequestedSegmentResponse()
	 * @generated
	 */
	void setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType value);

} // OpEquipmentRequirementType
