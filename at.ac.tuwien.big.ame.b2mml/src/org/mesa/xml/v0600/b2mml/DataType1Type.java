/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataType1Type()
 * @model extendedMetaData="name='DataType1Type' kind='simple'"
 * @generated
 */
public interface DataType1Type extends CodeType {
} // DataType1Type
