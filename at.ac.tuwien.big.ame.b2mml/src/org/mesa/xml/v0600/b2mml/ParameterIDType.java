/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getParameterIDType()
 * @model extendedMetaData="name='ParameterIDType' kind='simple'"
 * @generated
 */
public interface ParameterIDType extends IdentifierType {
} // ParameterIDType
