/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Problem Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getProblemType()
 * @model extendedMetaData="name='ProblemType' kind='simple'"
 * @generated
 */
public interface ProblemType extends CodeType {
} // ProblemType
