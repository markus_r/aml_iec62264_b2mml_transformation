/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quantity String Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getQuantityStringType()
 * @model extendedMetaData="name='QuantityStringType' kind='simple'"
 * @generated
 */
public interface QuantityStringType extends AnyGenericValueType {
} // QuantityStringType
