/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Lot ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialLotIDType()
 * @model extendedMetaData="name='MaterialLotIDType' kind='simple'"
 * @generated
 */
public interface MaterialLotIDType extends IdentifierType {
} // MaterialLotIDType
