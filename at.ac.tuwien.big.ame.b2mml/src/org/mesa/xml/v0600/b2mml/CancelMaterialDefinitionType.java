/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cancel Material Definition Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.CancelMaterialDefinitionType#getApplicationArea <em>Application Area</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.CancelMaterialDefinitionType#getDataArea <em>Data Area</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.CancelMaterialDefinitionType#getReleaseID <em>Release ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.CancelMaterialDefinitionType#getVersionID <em>Version ID</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getCancelMaterialDefinitionType()
 * @model extendedMetaData="name='CancelMaterialDefinitionType' kind='elementOnly'"
 * @generated
 */
public interface CancelMaterialDefinitionType extends EObject {
	/**
	 * Returns the value of the '<em><b>Application Area</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application Area</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application Area</em>' containment reference.
	 * @see #setApplicationArea(TransApplicationAreaType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getCancelMaterialDefinitionType_ApplicationArea()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ApplicationArea' namespace='##targetNamespace'"
	 * @generated
	 */
	TransApplicationAreaType getApplicationArea();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.CancelMaterialDefinitionType#getApplicationArea <em>Application Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Application Area</em>' containment reference.
	 * @see #getApplicationArea()
	 * @generated
	 */
	void setApplicationArea(TransApplicationAreaType value);

	/**
	 * Returns the value of the '<em><b>Data Area</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Area</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Area</em>' containment reference.
	 * @see #setDataArea(DataAreaType195)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getCancelMaterialDefinitionType_DataArea()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DataArea' namespace='##targetNamespace'"
	 * @generated
	 */
	DataAreaType195 getDataArea();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.CancelMaterialDefinitionType#getDataArea <em>Data Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Area</em>' containment reference.
	 * @see #getDataArea()
	 * @generated
	 */
	void setDataArea(DataAreaType195 value);

	/**
	 * Returns the value of the '<em><b>Release ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Release ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Release ID</em>' attribute.
	 * @see #setReleaseID(String)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getCancelMaterialDefinitionType_ReleaseID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString" required="true"
	 *        extendedMetaData="kind='attribute' name='releaseID'"
	 * @generated
	 */
	String getReleaseID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.CancelMaterialDefinitionType#getReleaseID <em>Release ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Release ID</em>' attribute.
	 * @see #getReleaseID()
	 * @generated
	 */
	void setReleaseID(String value);

	/**
	 * Returns the value of the '<em><b>Version ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version ID</em>' attribute.
	 * @see #setVersionID(String)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getCancelMaterialDefinitionType_VersionID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='versionID'"
	 * @generated
	 */
	String getVersionID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.CancelMaterialDefinitionType#getVersionID <em>Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version ID</em>' attribute.
	 * @see #getVersionID()
	 * @generated
	 */
	void setVersionID(String value);

} // CancelMaterialDefinitionType
