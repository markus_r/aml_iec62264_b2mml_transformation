/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Definition Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getVersion <em>Version</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getBillOfMaterialsID <em>Bill Of Materials ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getWorkDefinitionID <em>Work Definition ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getBillOfResourcesID <em>Bill Of Resources ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getOperationsMaterialBill <em>Operations Material Bill</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getOperationsSegment <em>Operations Segment</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionType()
 * @model extendedMetaData="name='OperationsDefinitionType' kind='elementOnly'"
 * @generated
 */
public interface OperationsDefinitionType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionType_ID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' containment reference.
	 * @see #setVersion(VersionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionType_Version()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Version' namespace='##targetNamespace'"
	 * @generated
	 */
	VersionType getVersion();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getVersion <em>Version</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' containment reference.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(VersionType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' containment reference.
	 * @see #setOperationsType(OperationsTypeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionType_OperationsType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsType' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsTypeType getOperationsType();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getOperationsType <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' containment reference.
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsTypeType value);

	/**
	 * Returns the value of the '<em><b>Published Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Published Date</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Published Date</em>' containment reference.
	 * @see #setPublishedDate(PublishedDateType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionType_PublishedDate()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PublishedDate' namespace='##targetNamespace'"
	 * @generated
	 */
	PublishedDateType getPublishedDate();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getPublishedDate <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Published Date</em>' containment reference.
	 * @see #getPublishedDate()
	 * @generated
	 */
	void setPublishedDate(PublishedDateType value);

	/**
	 * Returns the value of the '<em><b>Bill Of Materials ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bill Of Materials ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bill Of Materials ID</em>' containment reference.
	 * @see #setBillOfMaterialsID(BillOfMaterialsIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionType_BillOfMaterialsID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='BillOfMaterialsID' namespace='##targetNamespace'"
	 * @generated
	 */
	BillOfMaterialsIDType getBillOfMaterialsID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getBillOfMaterialsID <em>Bill Of Materials ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bill Of Materials ID</em>' containment reference.
	 * @see #getBillOfMaterialsID()
	 * @generated
	 */
	void setBillOfMaterialsID(BillOfMaterialsIDType value);

	/**
	 * Returns the value of the '<em><b>Work Definition ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Work Definition ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Work Definition ID</em>' containment reference.
	 * @see #setWorkDefinitionID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionType_WorkDefinitionID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='WorkDefinitionID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getWorkDefinitionID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getWorkDefinitionID <em>Work Definition ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Work Definition ID</em>' containment reference.
	 * @see #getWorkDefinitionID()
	 * @generated
	 */
	void setWorkDefinitionID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Bill Of Resources ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bill Of Resources ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bill Of Resources ID</em>' containment reference.
	 * @see #setBillOfResourcesID(BillOfResourcesIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionType_BillOfResourcesID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='BillOfResourcesID' namespace='##targetNamespace'"
	 * @generated
	 */
	BillOfResourcesIDType getBillOfResourcesID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType#getBillOfResourcesID <em>Bill Of Resources ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bill Of Resources ID</em>' containment reference.
	 * @see #getBillOfResourcesID()
	 * @generated
	 */
	void setBillOfResourcesID(BillOfResourcesIDType value);

	/**
	 * Returns the value of the '<em><b>Operations Material Bill</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OperationsMaterialBillType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Material Bill</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Material Bill</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionType_OperationsMaterialBill()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsMaterialBill' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsMaterialBillType> getOperationsMaterialBill();

	/**
	 * Returns the value of the '<em><b>Operations Segment</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OperationsSegmentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Segment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Segment</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsDefinitionType_OperationsSegment()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsSegment' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsSegmentType> getOperationsSegment();

} // OperationsDefinitionType
