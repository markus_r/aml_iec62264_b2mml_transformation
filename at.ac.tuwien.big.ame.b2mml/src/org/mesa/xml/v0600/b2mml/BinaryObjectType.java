/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Object Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getValue <em>Value</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getCharacterSetCode <em>Character Set Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getEncodingCode <em>Encoding Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getFilename <em>Filename</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getFormat <em>Format</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getMimeCode <em>Mime Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getUri <em>Uri</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBinaryObjectType()
 * @model extendedMetaData="name='BinaryObjectType' kind='simple'"
 * @generated
 */
public interface BinaryObjectType extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(byte[])
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBinaryObjectType_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Base64Binary"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	byte[] getValue();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(byte[] value);

	/**
	 * Returns the value of the '<em><b>Character Set Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Character Set Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Character Set Code</em>' attribute.
	 * @see #setCharacterSetCode(String)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBinaryObjectType_CharacterSetCode()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='characterSetCode'"
	 * @generated
	 */
	String getCharacterSetCode();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getCharacterSetCode <em>Character Set Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Character Set Code</em>' attribute.
	 * @see #getCharacterSetCode()
	 * @generated
	 */
	void setCharacterSetCode(String value);

	/**
	 * Returns the value of the '<em><b>Encoding Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Encoding Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Encoding Code</em>' attribute.
	 * @see #setEncodingCode(String)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBinaryObjectType_EncodingCode()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='encodingCode'"
	 * @generated
	 */
	String getEncodingCode();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getEncodingCode <em>Encoding Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Encoding Code</em>' attribute.
	 * @see #getEncodingCode()
	 * @generated
	 */
	void setEncodingCode(String value);

	/**
	 * Returns the value of the '<em><b>Filename</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filename</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filename</em>' attribute.
	 * @see #setFilename(String)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBinaryObjectType_Filename()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='filename'"
	 * @generated
	 */
	String getFilename();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getFilename <em>Filename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filename</em>' attribute.
	 * @see #getFilename()
	 * @generated
	 */
	void setFilename(String value);

	/**
	 * Returns the value of the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Format</em>' attribute.
	 * @see #setFormat(String)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBinaryObjectType_Format()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='format'"
	 * @generated
	 */
	String getFormat();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getFormat <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Format</em>' attribute.
	 * @see #getFormat()
	 * @generated
	 */
	void setFormat(String value);

	/**
	 * Returns the value of the '<em><b>Mime Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mime Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mime Code</em>' attribute.
	 * @see #setMimeCode(String)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBinaryObjectType_MimeCode()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='mimeCode'"
	 * @generated
	 */
	String getMimeCode();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getMimeCode <em>Mime Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mime Code</em>' attribute.
	 * @see #getMimeCode()
	 * @generated
	 */
	void setMimeCode(String value);

	/**
	 * Returns the value of the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uri</em>' attribute.
	 * @see #setUri(String)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBinaryObjectType_Uri()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnyURI"
	 *        extendedMetaData="kind='attribute' name='uri'"
	 * @generated
	 */
	String getUri();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.BinaryObjectType#getUri <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uri</em>' attribute.
	 * @see #getUri()
	 * @generated
	 */
	void setUri(String value);

} // BinaryObjectType
