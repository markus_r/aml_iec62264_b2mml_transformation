/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Product Segment ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getProductSegmentIDType()
 * @model extendedMetaData="name='ProductSegmentIDType' kind='simple'"
 * @generated
 */
public interface ProductSegmentIDType extends IdentifierType {
} // ProductSegmentIDType
