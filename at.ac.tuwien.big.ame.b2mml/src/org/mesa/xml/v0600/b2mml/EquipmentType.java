/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.EquipmentType#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.EquipmentType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.EquipmentType#getLocation <em>Location</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.EquipmentType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.EquipmentType#getEquipmentLevel <em>Equipment Level</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.EquipmentType#getEquipmentAssetMapping <em>Equipment Asset Mapping</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.EquipmentType#getEquipmentProperty <em>Equipment Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.EquipmentType#getEquipment <em>Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.EquipmentType#getEquipmentClassID <em>Equipment Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.EquipmentType#getEquipmentCapabilityTestSpecificationID <em>Equipment Capability Test Specification ID</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentType()
 * @model extendedMetaData="name='EquipmentType' kind='elementOnly'"
 * @generated
 */
public interface EquipmentType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentType_ID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.EquipmentType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Location</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' containment reference.
	 * @see #setLocation(LocationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentType_Location()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Location' namespace='##targetNamespace'"
	 * @generated
	 */
	LocationType getLocation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.EquipmentType#getLocation <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' containment reference.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(LocationType value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.EquipmentType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Equipment Level</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Level</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Level</em>' containment reference.
	 * @see #setEquipmentLevel(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentType_EquipmentLevel()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentLevel' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getEquipmentLevel();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.EquipmentType#getEquipmentLevel <em>Equipment Level</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Level</em>' containment reference.
	 * @see #getEquipmentLevel()
	 * @generated
	 */
	void setEquipmentLevel(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Equipment Asset Mapping</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.EquipmentAssetMappingType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Asset Mapping</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Asset Mapping</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentType_EquipmentAssetMapping()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentAssetMapping' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentAssetMappingType> getEquipmentAssetMapping();

	/**
	 * Returns the value of the '<em><b>Equipment Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.EquipmentPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Property</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentType_EquipmentProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentPropertyType> getEquipmentProperty();

	/**
	 * Returns the value of the '<em><b>Equipment</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.EquipmentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentType_Equipment()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Equipment' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentType> getEquipment();

	/**
	 * Returns the value of the '<em><b>Equipment Class ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.EquipmentClassIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Class ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Class ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentType_EquipmentClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentClassIDType> getEquipmentClassID();

	/**
	 * Returns the value of the '<em><b>Equipment Capability Test Specification ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.EquipmentCapabilityTestSpecificationIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Capability Test Specification ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Capability Test Specification ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentType_EquipmentCapabilityTestSpecificationID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentCapabilityTestSpecificationID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EquipmentCapabilityTestSpecificationIDType> getEquipmentCapabilityTestSpecificationID();

} // EquipmentType
