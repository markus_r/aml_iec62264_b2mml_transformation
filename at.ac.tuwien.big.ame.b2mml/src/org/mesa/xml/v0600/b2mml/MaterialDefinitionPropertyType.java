/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Definition Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialDefinitionPropertyType#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialDefinitionPropertyType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialDefinitionPropertyType#getValue <em>Value</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialDefinitionPropertyType#getMaterialDefinitionProperty <em>Material Definition Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialDefinitionPropertyType#getMaterialTestSpecificationID <em>Material Test Specification ID</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialDefinitionPropertyType()
 * @model extendedMetaData="name='MaterialDefinitionPropertyType' kind='elementOnly'"
 * @generated
 */
public interface MaterialDefinitionPropertyType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialDefinitionPropertyType_ID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.MaterialDefinitionPropertyType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialDefinitionPropertyType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.ValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialDefinitionPropertyType_Value()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Value' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ValueType> getValue();

	/**
	 * Returns the value of the '<em><b>Material Definition Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialDefinitionPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Definition Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Definition Property</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialDefinitionPropertyType_MaterialDefinitionProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialDefinitionProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialDefinitionPropertyType> getMaterialDefinitionProperty();

	/**
	 * Returns the value of the '<em><b>Material Test Specification ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialTestSpecificationIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Test Specification ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Test Specification ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialDefinitionPropertyType_MaterialTestSpecificationID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialTestSpecificationID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialTestSpecificationIDType> getMaterialTestSpecificationID();

} // MaterialDefinitionPropertyType
