/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Correction Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getCorrectionType()
 * @model extendedMetaData="name='CorrectionType' kind='simple'"
 * @generated
 */
public interface CorrectionType extends CodeType {
} // CorrectionType
