/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Use Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentUseType()
 * @model extendedMetaData="name='EquipmentUseType' kind='simple'"
 * @generated
 */
public interface EquipmentUseType extends CodeType {
} // EquipmentUseType
