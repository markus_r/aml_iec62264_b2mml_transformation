/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requested Priority Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getRequestedPriorityType()
 * @model extendedMetaData="name='RequestedPriorityType' kind='simple'"
 * @generated
 */
public interface RequestedPriorityType extends NumericType {
} // RequestedPriorityType
