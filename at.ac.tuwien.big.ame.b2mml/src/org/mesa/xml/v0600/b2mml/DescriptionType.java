/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Description Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDescriptionType()
 * @model extendedMetaData="name='DescriptionType' kind='simple'"
 * @generated
 */
public interface DescriptionType extends TextType {
} // DescriptionType
