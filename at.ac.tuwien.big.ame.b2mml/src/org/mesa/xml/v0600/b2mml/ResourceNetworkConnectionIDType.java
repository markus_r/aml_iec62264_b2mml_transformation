/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Network Connection ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getResourceNetworkConnectionIDType()
 * @model extendedMetaData="name='ResourceNetworkConnectionIDType' kind='simple'"
 * @generated
 */
public interface ResourceNetworkConnectionIDType extends IdentifierType {
} // ResourceNetworkConnectionIDType
