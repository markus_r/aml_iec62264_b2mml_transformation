/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type121</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType121#getGet <em>Get</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType121#getMaterialSubLot <em>Material Sub Lot</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType121()
 * @model extendedMetaData="name='DataArea_._122_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType121 extends EObject {
	/**
	 * Returns the value of the '<em><b>Get</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get</em>' containment reference.
	 * @see #setGet(TransGetType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType121_Get()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Get' namespace='##targetNamespace'"
	 * @generated
	 */
	TransGetType getGet();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DataAreaType121#getGet <em>Get</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get</em>' containment reference.
	 * @see #getGet()
	 * @generated
	 */
	void setGet(TransGetType value);

	/**
	 * Returns the value of the '<em><b>Material Sub Lot</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialSubLotType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Sub Lot</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Sub Lot</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType121_MaterialSubLot()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MaterialSubLot' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialSubLotType> getMaterialSubLot();

} // DataAreaType121
