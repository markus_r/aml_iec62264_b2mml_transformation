/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Capability ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialCapabilityIDType()
 * @model extendedMetaData="name='MaterialCapabilityIDType' kind='simple'"
 * @generated
 */
public interface MaterialCapabilityIDType extends IdentifierType {
} // MaterialCapabilityIDType
