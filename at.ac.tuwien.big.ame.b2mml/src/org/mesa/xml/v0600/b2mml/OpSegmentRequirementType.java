/**
 */
package org.mesa.xml.v0600.b2mml;

import javax.xml.datatype.Duration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Op Segment Requirement Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getProcessSegmentID <em>Process Segment ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getEarliestStartTime <em>Earliest Start Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getLatestEndTime <em>Latest End Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getDuration <em>Duration</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getOperationsDefinitionID <em>Operations Definition ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getSegmentState <em>Segment State</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getSegmentParameter <em>Segment Parameter</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getPersonnelRequirement <em>Personnel Requirement</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getEquipmentRequirement <em>Equipment Requirement</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getPhysicalAssetRequirement <em>Physical Asset Requirement</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getMaterialRequirement <em>Material Requirement</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getSegmentRequirement <em>Segment Requirement</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType()
 * @model extendedMetaData="name='OpSegmentRequirementType' kind='elementOnly'"
 * @generated
 */
public interface OpSegmentRequirementType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_ID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Operations Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Type</em>' containment reference.
	 * @see #setOperationsType(OperationsTypeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_OperationsType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsType' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsTypeType getOperationsType();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getOperationsType <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Type</em>' containment reference.
	 * @see #getOperationsType()
	 * @generated
	 */
	void setOperationsType(OperationsTypeType value);

	/**
	 * Returns the value of the '<em><b>Process Segment ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment ID</em>' containment reference.
	 * @see #setProcessSegmentID(ProcessSegmentIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_ProcessSegmentID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ProcessSegmentID' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessSegmentIDType getProcessSegmentID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getProcessSegmentID <em>Process Segment ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Segment ID</em>' containment reference.
	 * @see #getProcessSegmentID()
	 * @generated
	 */
	void setProcessSegmentID(ProcessSegmentIDType value);

	/**
	 * Returns the value of the '<em><b>Earliest Start Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Earliest Start Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Earliest Start Time</em>' containment reference.
	 * @see #setEarliestStartTime(EarliestStartTimeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_EarliestStartTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EarliestStartTime' namespace='##targetNamespace'"
	 * @generated
	 */
	EarliestStartTimeType getEarliestStartTime();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getEarliestStartTime <em>Earliest Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Earliest Start Time</em>' containment reference.
	 * @see #getEarliestStartTime()
	 * @generated
	 */
	void setEarliestStartTime(EarliestStartTimeType value);

	/**
	 * Returns the value of the '<em><b>Latest End Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Latest End Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Latest End Time</em>' containment reference.
	 * @see #setLatestEndTime(LatestEndTimeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_LatestEndTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LatestEndTime' namespace='##targetNamespace'"
	 * @generated
	 */
	LatestEndTimeType getLatestEndTime();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getLatestEndTime <em>Latest End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Latest End Time</em>' containment reference.
	 * @see #getLatestEndTime()
	 * @generated
	 */
	void setLatestEndTime(LatestEndTimeType value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(Duration)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_Duration()
	 * @model dataType="org.mesa.xml.v0600.b2mml.DurationType"
	 *        extendedMetaData="kind='element' name='Duration' namespace='##targetNamespace'"
	 * @generated
	 */
	Duration getDuration();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(Duration value);

	/**
	 * Returns the value of the '<em><b>Operations Definition ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition ID</em>' containment reference.
	 * @see #setOperationsDefinitionID(OperationsDefinitionIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_OperationsDefinitionID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OperationsDefinitionID' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsDefinitionIDType getOperationsDefinitionID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getOperationsDefinitionID <em>Operations Definition ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Definition ID</em>' containment reference.
	 * @see #getOperationsDefinitionID()
	 * @generated
	 */
	void setOperationsDefinitionID(OperationsDefinitionIDType value);

	/**
	 * Returns the value of the '<em><b>Segment State</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment State</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment State</em>' containment reference.
	 * @see #setSegmentState(RequestStateType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_SegmentState()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SegmentState' namespace='##targetNamespace'"
	 * @generated
	 */
	RequestStateType getSegmentState();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getSegmentState <em>Segment State</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Segment State</em>' containment reference.
	 * @see #getSegmentState()
	 * @generated
	 */
	void setSegmentState(RequestStateType value);

	/**
	 * Returns the value of the '<em><b>Segment Parameter</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.ParameterType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment Parameter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment Parameter</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_SegmentParameter()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SegmentParameter' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ParameterType> getSegmentParameter();

	/**
	 * Returns the value of the '<em><b>Personnel Requirement</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpPersonnelRequirementType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Requirement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Requirement</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_PersonnelRequirement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonnelRequirement' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPersonnelRequirementType> getPersonnelRequirement();

	/**
	 * Returns the value of the '<em><b>Equipment Requirement</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpEquipmentRequirementType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Requirement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Requirement</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_EquipmentRequirement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EquipmentRequirement' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpEquipmentRequirementType> getEquipmentRequirement();

	/**
	 * Returns the value of the '<em><b>Physical Asset Requirement</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetRequirementType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Requirement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Requirement</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_PhysicalAssetRequirement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetRequirement' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPhysicalAssetRequirementType> getPhysicalAssetRequirement();

	/**
	 * Returns the value of the '<em><b>Material Requirement</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpMaterialRequirementType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Requirement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Requirement</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_MaterialRequirement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialRequirement' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpMaterialRequirementType> getMaterialRequirement();

	/**
	 * Returns the value of the '<em><b>Segment Requirement</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Segment Requirement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Segment Requirement</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_SegmentRequirement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SegmentRequirement' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpSegmentRequirementType> getSegmentRequirement();

	/**
	 * Returns the value of the '<em><b>Required By Requested Segment Response</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required By Requested Segment Response</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required By Requested Segment Response</em>' containment reference.
	 * @see #setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpSegmentRequirementType_RequiredByRequestedSegmentResponse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='RequiredByRequestedSegmentResponse' namespace='##targetNamespace'"
	 * @generated
	 */
	RequiredByRequestedSegmentResponseType getRequiredByRequestedSegmentResponse();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpSegmentRequirementType#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required By Requested Segment Response</em>' containment reference.
	 * @see #getRequiredByRequestedSegmentResponse()
	 * @generated
	 */
	void setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType value);

} // OpSegmentRequirementType
