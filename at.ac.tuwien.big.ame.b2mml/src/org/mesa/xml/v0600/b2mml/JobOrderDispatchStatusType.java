/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Job Order Dispatch Status Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getJobOrderDispatchStatusType()
 * @model extendedMetaData="name='JobOrderDispatchStatusType' kind='simple'"
 * @generated
 */
public interface JobOrderDispatchStatusType extends CodeType {
} // JobOrderDispatchStatusType
