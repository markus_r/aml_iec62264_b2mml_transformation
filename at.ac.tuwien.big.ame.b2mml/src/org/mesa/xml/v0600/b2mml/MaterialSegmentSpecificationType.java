/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Segment Specification Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getMaterialClassID <em>Material Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getMaterialDefinitionID <em>Material Definition ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getAssemblySpecificationID <em>Assembly Specification ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getMaterialSegmentSpecificationProperty <em>Material Segment Specification Property</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSegmentSpecificationType()
 * @model extendedMetaData="name='MaterialSegmentSpecificationType' kind='elementOnly'"
 * @generated
 */
public interface MaterialSegmentSpecificationType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSegmentSpecificationType_ID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Material Class ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Class ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Class ID</em>' containment reference.
	 * @see #setMaterialClassID(MaterialClassIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSegmentSpecificationType_MaterialClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialClassIDType getMaterialClassID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getMaterialClassID <em>Material Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Class ID</em>' containment reference.
	 * @see #getMaterialClassID()
	 * @generated
	 */
	void setMaterialClassID(MaterialClassIDType value);

	/**
	 * Returns the value of the '<em><b>Material Definition ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Definition ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Definition ID</em>' containment reference.
	 * @see #setMaterialDefinitionID(MaterialDefinitionIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSegmentSpecificationType_MaterialDefinitionID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialDefinitionID' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialDefinitionIDType getMaterialDefinitionID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getMaterialDefinitionID <em>Material Definition ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Definition ID</em>' containment reference.
	 * @see #getMaterialDefinitionID()
	 * @generated
	 */
	void setMaterialDefinitionID(MaterialDefinitionIDType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSegmentSpecificationType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Assembly Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Type</em>' containment reference.
	 * @see #setAssemblyType(AssemblyTypeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSegmentSpecificationType_AssemblyType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyType' namespace='##targetNamespace'"
	 * @generated
	 */
	AssemblyTypeType getAssemblyType();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getAssemblyType <em>Assembly Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Type</em>' containment reference.
	 * @see #getAssemblyType()
	 * @generated
	 */
	void setAssemblyType(AssemblyTypeType value);

	/**
	 * Returns the value of the '<em><b>Assembly Relationship</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Relationship</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Relationship</em>' containment reference.
	 * @see #setAssemblyRelationship(AssemblyRelationshipType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSegmentSpecificationType_AssemblyRelationship()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblyRelationship' namespace='##targetNamespace'"
	 * @generated
	 */
	AssemblyRelationshipType getAssemblyRelationship();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getAssemblyRelationship <em>Assembly Relationship</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assembly Relationship</em>' containment reference.
	 * @see #getAssemblyRelationship()
	 * @generated
	 */
	void setAssemblyRelationship(AssemblyRelationshipType value);

	/**
	 * Returns the value of the '<em><b>Assembly Specification ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.IdentifierType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assembly Specification ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assembly Specification ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSegmentSpecificationType_AssemblySpecificationID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AssemblySpecificationID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<IdentifierType> getAssemblySpecificationID();

	/**
	 * Returns the value of the '<em><b>Material Use</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Use</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Use</em>' containment reference.
	 * @see #setMaterialUse(MaterialUseType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSegmentSpecificationType_MaterialUse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialUse' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialUseType getMaterialUse();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType#getMaterialUse <em>Material Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Use</em>' containment reference.
	 * @see #getMaterialUse()
	 * @generated
	 */
	void setMaterialUse(MaterialUseType value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSegmentSpecificationType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

	/**
	 * Returns the value of the '<em><b>Material Segment Specification Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Segment Specification Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Segment Specification Property</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialSegmentSpecificationType_MaterialSegmentSpecificationProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MaterialSegmentSpecificationProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MaterialSegmentSpecificationPropertyType> getMaterialSegmentSpecificationProperty();

} // MaterialSegmentSpecificationType
