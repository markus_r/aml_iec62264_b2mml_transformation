/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reason Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getReasonType()
 * @model extendedMetaData="name='ReasonType' kind='simple'"
 * @generated
 */
public interface ReasonType extends CodeType {
} // ReasonType
