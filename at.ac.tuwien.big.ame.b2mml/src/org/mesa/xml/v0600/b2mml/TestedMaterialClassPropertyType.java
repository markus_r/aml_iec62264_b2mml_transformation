/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tested Material Class Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TestedMaterialClassPropertyType#getMaterialClassID <em>Material Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TestedMaterialClassPropertyType#getPropertyID <em>Property ID</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTestedMaterialClassPropertyType()
 * @model extendedMetaData="name='TestedMaterialClassPropertyType' kind='elementOnly'"
 * @generated
 */
public interface TestedMaterialClassPropertyType extends EObject {
	/**
	 * Returns the value of the '<em><b>Material Class ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Class ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Class ID</em>' containment reference.
	 * @see #setMaterialClassID(MaterialClassIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTestedMaterialClassPropertyType_MaterialClassID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MaterialClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialClassIDType getMaterialClassID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TestedMaterialClassPropertyType#getMaterialClassID <em>Material Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Class ID</em>' containment reference.
	 * @see #getMaterialClassID()
	 * @generated
	 */
	void setMaterialClassID(MaterialClassIDType value);

	/**
	 * Returns the value of the '<em><b>Property ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property ID</em>' containment reference.
	 * @see #setPropertyID(PropertyIDType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTestedMaterialClassPropertyType_PropertyID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PropertyID' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyIDType getPropertyID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TestedMaterialClassPropertyType#getPropertyID <em>Property ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property ID</em>' containment reference.
	 * @see #getPropertyID()
	 * @generated
	 */
	void setPropertyID(PropertyIDType value);

} // TestedMaterialClassPropertyType
