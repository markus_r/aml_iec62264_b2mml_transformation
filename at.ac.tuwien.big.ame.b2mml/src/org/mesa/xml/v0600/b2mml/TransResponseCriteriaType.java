/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Response Criteria Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransResponseCriteriaType#getResponseExpression <em>Response Expression</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransResponseCriteriaType#getChangeStatus <em>Change Status</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransResponseCriteriaType()
 * @model extendedMetaData="name='TransResponseCriteriaType' kind='elementOnly'"
 * @generated
 */
public interface TransResponseCriteriaType extends EObject {
	/**
	 * Returns the value of the '<em><b>Response Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Response Expression</em>' containment reference.
	 * @see #setResponseExpression(TransExpressionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransResponseCriteriaType_ResponseExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ResponseExpression' namespace='##targetNamespace'"
	 * @generated
	 */
	TransExpressionType getResponseExpression();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TransResponseCriteriaType#getResponseExpression <em>Response Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Response Expression</em>' containment reference.
	 * @see #getResponseExpression()
	 * @generated
	 */
	void setResponseExpression(TransExpressionType value);

	/**
	 * Returns the value of the '<em><b>Change Status</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Status</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Status</em>' containment reference.
	 * @see #setChangeStatus(TransChangeStatusType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransResponseCriteriaType_ChangeStatus()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ChangeStatus' namespace='##targetNamespace'"
	 * @generated
	 */
	TransChangeStatusType getChangeStatus();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TransResponseCriteriaType#getChangeStatus <em>Change Status</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Status</em>' containment reference.
	 * @see #getChangeStatus()
	 * @generated
	 */
	void setChangeStatus(TransChangeStatusType value);

} // TransResponseCriteriaType
