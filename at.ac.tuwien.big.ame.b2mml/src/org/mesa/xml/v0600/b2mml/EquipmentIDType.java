/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentIDType()
 * @model extendedMetaData="name='EquipmentIDType' kind='simple'"
 * @generated
 */
public interface EquipmentIDType extends IdentifierType {
} // EquipmentIDType
