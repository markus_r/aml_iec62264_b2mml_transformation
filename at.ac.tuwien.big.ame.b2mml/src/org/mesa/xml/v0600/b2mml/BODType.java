/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BOD Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.BODType#getOriginalApplicationArea <em>Original Application Area</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.BODType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.BODType#getNote <em>Note</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.BODType#getUserArea <em>User Area</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBODType()
 * @model extendedMetaData="name='BODType' kind='elementOnly'"
 * @generated
 */
public interface BODType extends EObject {
	/**
	 * Returns the value of the '<em><b>Original Application Area</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Original Application Area</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Original Application Area</em>' containment reference.
	 * @see #setOriginalApplicationArea(TransApplicationAreaType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBODType_OriginalApplicationArea()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OriginalApplicationArea' namespace='##targetNamespace'"
	 * @generated
	 */
	TransApplicationAreaType getOriginalApplicationArea();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.BODType#getOriginalApplicationArea <em>Original Application Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Original Application Area</em>' containment reference.
	 * @see #getOriginalApplicationArea()
	 * @generated
	 */
	void setOriginalApplicationArea(TransApplicationAreaType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.TextType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBODType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TextType> getDescription();

	/**
	 * Returns the value of the '<em><b>Note</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.TextType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Note</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Note</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBODType_Note()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Note' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TextType> getNote();

	/**
	 * Returns the value of the '<em><b>User Area</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Area</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Area</em>' containment reference.
	 * @see #setUserArea(TransUserAreaType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBODType_UserArea()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UserArea' namespace='##targetNamespace'"
	 * @generated
	 */
	TransUserAreaType getUserArea();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.BODType#getUserArea <em>User Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Area</em>' containment reference.
	 * @see #getUserArea()
	 * @generated
	 */
	void setUserArea(TransUserAreaType value);

} // BODType
