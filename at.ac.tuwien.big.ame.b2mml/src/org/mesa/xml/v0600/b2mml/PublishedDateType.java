/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Published Date Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPublishedDateType()
 * @model extendedMetaData="name='PublishedDateType' kind='simple'"
 * @generated
 */
public interface PublishedDateType extends DateTimeType {
} // PublishedDateType
