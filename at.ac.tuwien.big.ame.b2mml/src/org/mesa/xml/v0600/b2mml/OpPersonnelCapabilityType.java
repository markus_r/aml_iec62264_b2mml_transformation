/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Op Personnel Capability Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getPersonnelClassID <em>Personnel Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getPersonID <em>Person ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getReason <em>Reason</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getPersonnelUse <em>Personnel Use</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getEndTime <em>End Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getPersonnelCapabilityProperty <em>Personnel Capability Property</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType()
 * @model extendedMetaData="name='OpPersonnelCapabilityType' kind='elementOnly'"
 * @generated
 */
public interface OpPersonnelCapabilityType extends EObject {
	/**
	 * Returns the value of the '<em><b>Personnel Class ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PersonnelClassIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Class ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Class ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType_PersonnelClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonnelClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PersonnelClassIDType> getPersonnelClassID();

	/**
	 * Returns the value of the '<em><b>Person ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PersonIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType_PersonID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PersonIDType> getPersonID();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Capability Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capability Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capability Type</em>' containment reference.
	 * @see #setCapabilityType(CapabilityTypeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType_CapabilityType()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='CapabilityType' namespace='##targetNamespace'"
	 * @generated
	 */
	CapabilityTypeType getCapabilityType();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getCapabilityType <em>Capability Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Capability Type</em>' containment reference.
	 * @see #getCapabilityType()
	 * @generated
	 */
	void setCapabilityType(CapabilityTypeType value);

	/**
	 * Returns the value of the '<em><b>Reason</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reason</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reason</em>' containment reference.
	 * @see #setReason(ReasonType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType_Reason()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Reason' namespace='##targetNamespace'"
	 * @generated
	 */
	ReasonType getReason();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getReason <em>Reason</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reason</em>' containment reference.
	 * @see #getReason()
	 * @generated
	 */
	void setReason(ReasonType value);

	/**
	 * Returns the value of the '<em><b>Confidence Factor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Confidence Factor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confidence Factor</em>' containment reference.
	 * @see #setConfidenceFactor(ConfidenceFactorType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType_ConfidenceFactor()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ConfidenceFactor' namespace='##targetNamespace'"
	 * @generated
	 */
	ConfidenceFactorType getConfidenceFactor();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getConfidenceFactor <em>Confidence Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confidence Factor</em>' containment reference.
	 * @see #getConfidenceFactor()
	 * @generated
	 */
	void setConfidenceFactor(ConfidenceFactorType value);

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Personnel Use</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Use</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Use</em>' containment reference.
	 * @see #setPersonnelUse(PersonnelUseType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType_PersonnelUse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonnelUse' namespace='##targetNamespace'"
	 * @generated
	 */
	PersonnelUseType getPersonnelUse();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getPersonnelUse <em>Personnel Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Personnel Use</em>' containment reference.
	 * @see #getPersonnelUse()
	 * @generated
	 */
	void setPersonnelUse(PersonnelUseType value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' containment reference.
	 * @see #setStartTime(StartTimeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType_StartTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='StartTime' namespace='##targetNamespace'"
	 * @generated
	 */
	StartTimeType getStartTime();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getStartTime <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' containment reference.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(StartTimeType value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' containment reference.
	 * @see #setEndTime(EndTimeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType_EndTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EndTime' namespace='##targetNamespace'"
	 * @generated
	 */
	EndTimeType getEndTime();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType#getEndTime <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' containment reference.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(EndTimeType value);

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

	/**
	 * Returns the value of the '<em><b>Personnel Capability Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Capability Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Capability Property</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPersonnelCapabilityType_PersonnelCapabilityProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PersonnelCapabilityProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPersonnelCapabilityPropertyType> getPersonnelCapabilityProperty();

} // OpPersonnelCapabilityType
