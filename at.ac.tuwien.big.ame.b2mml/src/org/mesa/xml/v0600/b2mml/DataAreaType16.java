/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type16</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType16#getProcess <em>Process</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType16#getOperationsSchedule <em>Operations Schedule</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType16()
 * @model extendedMetaData="name='DataArea_._17_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType16 extends EObject {
	/**
	 * Returns the value of the '<em><b>Process</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process</em>' containment reference.
	 * @see #setProcess(TransProcessType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType16_Process()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Process' namespace='##targetNamespace'"
	 * @generated
	 */
	TransProcessType getProcess();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DataAreaType16#getProcess <em>Process</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process</em>' containment reference.
	 * @see #getProcess()
	 * @generated
	 */
	void setProcess(TransProcessType value);

	/**
	 * Returns the value of the '<em><b>Operations Schedule</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OperationsScheduleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Schedule</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Schedule</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType16_OperationsSchedule()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OperationsSchedule' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsScheduleType> getOperationsSchedule();

} // DataAreaType16
