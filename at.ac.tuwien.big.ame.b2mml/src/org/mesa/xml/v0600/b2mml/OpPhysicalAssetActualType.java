/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Op Physical Asset Actual Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetActualType#getPhysicalAssetClassID <em>Physical Asset Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetActualType#getPhysicalAssetID <em>Physical Asset ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetActualType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetActualType#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetActualType#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetActualType#getPhysicalAssetActualProperty <em>Physical Asset Actual Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetActualType#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPhysicalAssetActualType()
 * @model extendedMetaData="name='OpPhysicalAssetActualType' kind='elementOnly'"
 * @generated
 */
public interface OpPhysicalAssetActualType extends EObject {
	/**
	 * Returns the value of the '<em><b>Physical Asset Class ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PhysicalAssetClassIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Class ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Class ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPhysicalAssetActualType_PhysicalAssetClassID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetClassID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetClassIDType> getPhysicalAssetClassID();

	/**
	 * Returns the value of the '<em><b>Physical Asset ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PhysicalAssetIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPhysicalAssetActualType_PhysicalAssetID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetIDType> getPhysicalAssetID();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPhysicalAssetActualType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.QuantityValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPhysicalAssetActualType_Quantity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Quantity' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QuantityValueType> getQuantity();

	/**
	 * Returns the value of the '<em><b>Hierarchy Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchy Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #setHierarchyScope(HierarchyScopeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPhysicalAssetActualType_HierarchyScope()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='HierarchyScope' namespace='##targetNamespace'"
	 * @generated
	 */
	HierarchyScopeType getHierarchyScope();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetActualType#getHierarchyScope <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hierarchy Scope</em>' containment reference.
	 * @see #getHierarchyScope()
	 * @generated
	 */
	void setHierarchyScope(HierarchyScopeType value);

	/**
	 * Returns the value of the '<em><b>Physical Asset Actual Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetActualPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Actual Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Actual Property</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPhysicalAssetActualType_PhysicalAssetActualProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetActualProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OpPhysicalAssetActualPropertyType> getPhysicalAssetActualProperty();

	/**
	 * Returns the value of the '<em><b>Required By Requested Segment Response</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required By Requested Segment Response</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required By Requested Segment Response</em>' containment reference.
	 * @see #setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOpPhysicalAssetActualType_RequiredByRequestedSegmentResponse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='RequiredByRequestedSegmentResponse' namespace='##targetNamespace'"
	 * @generated
	 */
	RequiredByRequestedSegmentResponseType getRequiredByRequestedSegmentResponse();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.OpPhysicalAssetActualType#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required By Requested Segment Response</em>' containment reference.
	 * @see #getRequiredByRequestedSegmentResponse()
	 * @generated
	 */
	void setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType value);

} // OpPhysicalAssetActualType
