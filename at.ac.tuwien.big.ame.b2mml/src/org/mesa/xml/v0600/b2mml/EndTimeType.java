/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>End Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEndTimeType()
 * @model extendedMetaData="name='EndTimeType' kind='simple'"
 * @generated
 */
public interface EndTimeType extends DateTimeType {
} // EndTimeType
