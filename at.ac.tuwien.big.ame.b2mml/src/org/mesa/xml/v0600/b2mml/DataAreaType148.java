/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type148</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType148#getCancel <em>Cancel</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType148#getQualificationTestSpecification <em>Qualification Test Specification</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType148()
 * @model extendedMetaData="name='DataArea_._149_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType148 extends EObject {
	/**
	 * Returns the value of the '<em><b>Cancel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel</em>' containment reference.
	 * @see #setCancel(TransCancelType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType148_Cancel()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Cancel' namespace='##targetNamespace'"
	 * @generated
	 */
	TransCancelType getCancel();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DataAreaType148#getCancel <em>Cancel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel</em>' containment reference.
	 * @see #getCancel()
	 * @generated
	 */
	void setCancel(TransCancelType value);

	/**
	 * Returns the value of the '<em><b>Qualification Test Specification</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.QualificationTestSpecificationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualification Test Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualification Test Specification</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType148_QualificationTestSpecification()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='QualificationTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<QualificationTestSpecificationType> getQualificationTestSpecification();

} // DataAreaType148
