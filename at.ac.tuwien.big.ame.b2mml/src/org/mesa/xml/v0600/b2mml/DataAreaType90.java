/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type90</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType90#getRespond <em>Respond</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType90#getPhysicalAsset <em>Physical Asset</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType90()
 * @model extendedMetaData="name='DataArea_._91_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType90 extends EObject {
	/**
	 * Returns the value of the '<em><b>Respond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond</em>' containment reference.
	 * @see #setRespond(TransRespondType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType90_Respond()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Respond' namespace='##targetNamespace'"
	 * @generated
	 */
	TransRespondType getRespond();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DataAreaType90#getRespond <em>Respond</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond</em>' containment reference.
	 * @see #getRespond()
	 * @generated
	 */
	void setRespond(TransRespondType value);

	/**
	 * Returns the value of the '<em><b>Physical Asset</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PhysicalAssetType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType90_PhysicalAsset()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PhysicalAsset' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetType> getPhysicalAsset();

} // DataAreaType90
