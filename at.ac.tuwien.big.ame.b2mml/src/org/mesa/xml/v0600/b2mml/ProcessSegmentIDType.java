/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process Segment ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getProcessSegmentIDType()
 * @model extendedMetaData="name='ProcessSegmentIDType' kind='simple'"
 * @generated
 */
public interface ProcessSegmentIDType extends IdentifierType {
} // ProcessSegmentIDType
