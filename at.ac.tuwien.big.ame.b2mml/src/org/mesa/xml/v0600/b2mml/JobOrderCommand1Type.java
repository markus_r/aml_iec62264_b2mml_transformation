/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Job Order Command1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getJobOrderCommand1Type()
 * @model extendedMetaData="name='JobOrderCommand1Type' kind='simple'"
 * @generated
 */
public interface JobOrderCommand1Type extends CodeType {
} // JobOrderCommand1Type
