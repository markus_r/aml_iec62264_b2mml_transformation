/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Change Status Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransChangeStatusType#getCode <em>Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransChangeStatusType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransChangeStatusType#getEffectiveDateTime <em>Effective Date Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransChangeStatusType#getReasonCode <em>Reason Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransChangeStatusType#getReason <em>Reason</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransChangeStatusType#getStateChange <em>State Change</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransChangeStatusType#getUserArea <em>User Area</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransChangeStatusType()
 * @model extendedMetaData="name='TransChangeStatusType' kind='elementOnly'"
 * @generated
 */
public interface TransChangeStatusType extends EObject {
	/**
	 * Returns the value of the '<em><b>Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' containment reference.
	 * @see #setCode(CodeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransChangeStatusType_Code()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Code' namespace='##targetNamespace'"
	 * @generated
	 */
	CodeType getCode();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TransChangeStatusType#getCode <em>Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' containment reference.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(CodeType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(DescriptionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransChangeStatusType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	DescriptionType getDescription();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TransChangeStatusType#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(DescriptionType value);

	/**
	 * Returns the value of the '<em><b>Effective Date Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Effective Date Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Effective Date Time</em>' containment reference.
	 * @see #setEffectiveDateTime(DateTimeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransChangeStatusType_EffectiveDateTime()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EffectiveDateTime' namespace='##targetNamespace'"
	 * @generated
	 */
	DateTimeType getEffectiveDateTime();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TransChangeStatusType#getEffectiveDateTime <em>Effective Date Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Effective Date Time</em>' containment reference.
	 * @see #getEffectiveDateTime()
	 * @generated
	 */
	void setEffectiveDateTime(DateTimeType value);

	/**
	 * Returns the value of the '<em><b>Reason Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reason Code</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reason Code</em>' containment reference.
	 * @see #setReasonCode(CodeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransChangeStatusType_ReasonCode()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ReasonCode' namespace='##targetNamespace'"
	 * @generated
	 */
	CodeType getReasonCode();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TransChangeStatusType#getReasonCode <em>Reason Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reason Code</em>' containment reference.
	 * @see #getReasonCode()
	 * @generated
	 */
	void setReasonCode(CodeType value);

	/**
	 * Returns the value of the '<em><b>Reason</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.CodeType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reason</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reason</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransChangeStatusType_Reason()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Reason' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<CodeType> getReason();

	/**
	 * Returns the value of the '<em><b>State Change</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.TransStateChangeType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Change</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Change</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransChangeStatusType_StateChange()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='StateChange' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TransStateChangeType> getStateChange();

	/**
	 * Returns the value of the '<em><b>User Area</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Area</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Area</em>' containment reference.
	 * @see #setUserArea(TransUserAreaType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransChangeStatusType_UserArea()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UserArea' namespace='##targetNamespace'"
	 * @generated
	 */
	TransUserAreaType getUserArea();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TransChangeStatusType#getUserArea <em>User Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Area</em>' containment reference.
	 * @see #getUserArea()
	 * @generated
	 */
	void setUserArea(TransUserAreaType value);

} // TransChangeStatusType
