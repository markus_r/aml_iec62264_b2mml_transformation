/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bill Of Material ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getBillOfMaterialIDType()
 * @model extendedMetaData="name='BillOfMaterialIDType' kind='simple'"
 * @generated
 */
public interface BillOfMaterialIDType extends IdentifierType {
} // BillOfMaterialIDType
