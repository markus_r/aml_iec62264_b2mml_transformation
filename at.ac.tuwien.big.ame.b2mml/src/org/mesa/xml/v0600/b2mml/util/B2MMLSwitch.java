/**
 */
package org.mesa.xml.v0600.b2mml.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.mesa.xml.v0600.b2mml.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage
 * @generated
 */
public class B2MMLSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static B2MMLPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public B2MMLSwitch() {
		if (modelPackage == null) {
			modelPackage = B2MMLPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case B2MMLPackage.ACKNOWLEDGE_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: {
				AcknowledgeEquipmentCapabilityTestSpecType acknowledgeEquipmentCapabilityTestSpecType = (AcknowledgeEquipmentCapabilityTestSpecType)theEObject;
				T result = caseAcknowledgeEquipmentCapabilityTestSpecType(acknowledgeEquipmentCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_EQUIPMENT_CLASS_TYPE: {
				AcknowledgeEquipmentClassType acknowledgeEquipmentClassType = (AcknowledgeEquipmentClassType)theEObject;
				T result = caseAcknowledgeEquipmentClassType(acknowledgeEquipmentClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_EQUIPMENT_INFORMATION_TYPE: {
				AcknowledgeEquipmentInformationType acknowledgeEquipmentInformationType = (AcknowledgeEquipmentInformationType)theEObject;
				T result = caseAcknowledgeEquipmentInformationType(acknowledgeEquipmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_EQUIPMENT_TYPE: {
				AcknowledgeEquipmentType acknowledgeEquipmentType = (AcknowledgeEquipmentType)theEObject;
				T result = caseAcknowledgeEquipmentType(acknowledgeEquipmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_CLASS_TYPE: {
				AcknowledgeMaterialClassType acknowledgeMaterialClassType = (AcknowledgeMaterialClassType)theEObject;
				T result = caseAcknowledgeMaterialClassType(acknowledgeMaterialClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_DEFINITION_TYPE: {
				AcknowledgeMaterialDefinitionType acknowledgeMaterialDefinitionType = (AcknowledgeMaterialDefinitionType)theEObject;
				T result = caseAcknowledgeMaterialDefinitionType(acknowledgeMaterialDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_INFORMATION_TYPE: {
				AcknowledgeMaterialInformationType acknowledgeMaterialInformationType = (AcknowledgeMaterialInformationType)theEObject;
				T result = caseAcknowledgeMaterialInformationType(acknowledgeMaterialInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_LOT_TYPE: {
				AcknowledgeMaterialLotType acknowledgeMaterialLotType = (AcknowledgeMaterialLotType)theEObject;
				T result = caseAcknowledgeMaterialLotType(acknowledgeMaterialLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_SUB_LOT_TYPE: {
				AcknowledgeMaterialSubLotType acknowledgeMaterialSubLotType = (AcknowledgeMaterialSubLotType)theEObject;
				T result = caseAcknowledgeMaterialSubLotType(acknowledgeMaterialSubLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_TEST_SPEC_TYPE: {
				AcknowledgeMaterialTestSpecType acknowledgeMaterialTestSpecType = (AcknowledgeMaterialTestSpecType)theEObject;
				T result = caseAcknowledgeMaterialTestSpecType(acknowledgeMaterialTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_CAPABILITY_INFORMATION_TYPE: {
				AcknowledgeOperationsCapabilityInformationType acknowledgeOperationsCapabilityInformationType = (AcknowledgeOperationsCapabilityInformationType)theEObject;
				T result = caseAcknowledgeOperationsCapabilityInformationType(acknowledgeOperationsCapabilityInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_CAPABILITY_TYPE: {
				AcknowledgeOperationsCapabilityType acknowledgeOperationsCapabilityType = (AcknowledgeOperationsCapabilityType)theEObject;
				T result = caseAcknowledgeOperationsCapabilityType(acknowledgeOperationsCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_DEFINITION_INFORMATION_TYPE: {
				AcknowledgeOperationsDefinitionInformationType acknowledgeOperationsDefinitionInformationType = (AcknowledgeOperationsDefinitionInformationType)theEObject;
				T result = caseAcknowledgeOperationsDefinitionInformationType(acknowledgeOperationsDefinitionInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_DEFINITION_TYPE: {
				AcknowledgeOperationsDefinitionType acknowledgeOperationsDefinitionType = (AcknowledgeOperationsDefinitionType)theEObject;
				T result = caseAcknowledgeOperationsDefinitionType(acknowledgeOperationsDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_PERFORMANCE_TYPE: {
				AcknowledgeOperationsPerformanceType acknowledgeOperationsPerformanceType = (AcknowledgeOperationsPerformanceType)theEObject;
				T result = caseAcknowledgeOperationsPerformanceType(acknowledgeOperationsPerformanceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_SCHEDULE_TYPE: {
				AcknowledgeOperationsScheduleType acknowledgeOperationsScheduleType = (AcknowledgeOperationsScheduleType)theEObject;
				T result = caseAcknowledgeOperationsScheduleType(acknowledgeOperationsScheduleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_PERSONNEL_CLASS_TYPE: {
				AcknowledgePersonnelClassType acknowledgePersonnelClassType = (AcknowledgePersonnelClassType)theEObject;
				T result = caseAcknowledgePersonnelClassType(acknowledgePersonnelClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_PERSONNEL_INFORMATION_TYPE: {
				AcknowledgePersonnelInformationType acknowledgePersonnelInformationType = (AcknowledgePersonnelInformationType)theEObject;
				T result = caseAcknowledgePersonnelInformationType(acknowledgePersonnelInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_PERSON_TYPE: {
				AcknowledgePersonType acknowledgePersonType = (AcknowledgePersonType)theEObject;
				T result = caseAcknowledgePersonType(acknowledgePersonType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: {
				AcknowledgePhysicalAssetCapabilityTestSpecType acknowledgePhysicalAssetCapabilityTestSpecType = (AcknowledgePhysicalAssetCapabilityTestSpecType)theEObject;
				T result = caseAcknowledgePhysicalAssetCapabilityTestSpecType(acknowledgePhysicalAssetCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_PHYSICAL_ASSET_CLASS_TYPE: {
				AcknowledgePhysicalAssetClassType acknowledgePhysicalAssetClassType = (AcknowledgePhysicalAssetClassType)theEObject;
				T result = caseAcknowledgePhysicalAssetClassType(acknowledgePhysicalAssetClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_PHYSICAL_ASSET_INFORMATION_TYPE: {
				AcknowledgePhysicalAssetInformationType acknowledgePhysicalAssetInformationType = (AcknowledgePhysicalAssetInformationType)theEObject;
				T result = caseAcknowledgePhysicalAssetInformationType(acknowledgePhysicalAssetInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_PHYSICAL_ASSET_TYPE: {
				AcknowledgePhysicalAssetType acknowledgePhysicalAssetType = (AcknowledgePhysicalAssetType)theEObject;
				T result = caseAcknowledgePhysicalAssetType(acknowledgePhysicalAssetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_PROCESS_SEGMENT_INFORMATION_TYPE: {
				AcknowledgeProcessSegmentInformationType acknowledgeProcessSegmentInformationType = (AcknowledgeProcessSegmentInformationType)theEObject;
				T result = caseAcknowledgeProcessSegmentInformationType(acknowledgeProcessSegmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_PROCESS_SEGMENT_TYPE: {
				AcknowledgeProcessSegmentType acknowledgeProcessSegmentType = (AcknowledgeProcessSegmentType)theEObject;
				T result = caseAcknowledgeProcessSegmentType(acknowledgeProcessSegmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACKNOWLEDGE_QUALIFICATION_TEST_SPECIFICATION_TYPE: {
				AcknowledgeQualificationTestSpecificationType acknowledgeQualificationTestSpecificationType = (AcknowledgeQualificationTestSpecificationType)theEObject;
				T result = caseAcknowledgeQualificationTestSpecificationType(acknowledgeQualificationTestSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACTUAL_END_TIME_TYPE: {
				ActualEndTimeType actualEndTimeType = (ActualEndTimeType)theEObject;
				T result = caseActualEndTimeType(actualEndTimeType);
				if (result == null) result = caseDateTimeType(actualEndTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACTUAL_FINISH_TIME_TYPE: {
				ActualFinishTimeType actualFinishTimeType = (ActualFinishTimeType)theEObject;
				T result = caseActualFinishTimeType(actualFinishTimeType);
				if (result == null) result = caseDateTimeType(actualFinishTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ACTUAL_START_TIME_TYPE: {
				ActualStartTimeType actualStartTimeType = (ActualStartTimeType)theEObject;
				T result = caseActualStartTimeType(actualStartTimeType);
				if (result == null) result = caseDateTimeType(actualStartTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.AMOUNT_TYPE: {
				AmountType amountType = (AmountType)theEObject;
				T result = caseAmountType(amountType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE: {
				AnyGenericValueType anyGenericValueType = (AnyGenericValueType)theEObject;
				T result = caseAnyGenericValueType(anyGenericValueType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ASSEMBLY_RELATIONSHIP1_TYPE: {
				AssemblyRelationship1Type assemblyRelationship1Type = (AssemblyRelationship1Type)theEObject;
				T result = caseAssemblyRelationship1Type(assemblyRelationship1Type);
				if (result == null) result = caseCodeType(assemblyRelationship1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ASSEMBLY_RELATIONSHIP_TYPE: {
				AssemblyRelationshipType assemblyRelationshipType = (AssemblyRelationshipType)theEObject;
				T result = caseAssemblyRelationshipType(assemblyRelationshipType);
				if (result == null) result = caseAssemblyRelationship1Type(assemblyRelationshipType);
				if (result == null) result = caseCodeType(assemblyRelationshipType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ASSEMBLY_TYPE1_TYPE: {
				AssemblyType1Type assemblyType1Type = (AssemblyType1Type)theEObject;
				T result = caseAssemblyType1Type(assemblyType1Type);
				if (result == null) result = caseCodeType(assemblyType1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.ASSEMBLY_TYPE_TYPE: {
				AssemblyTypeType assemblyTypeType = (AssemblyTypeType)theEObject;
				T result = caseAssemblyTypeType(assemblyTypeType);
				if (result == null) result = caseAssemblyType1Type(assemblyTypeType);
				if (result == null) result = caseCodeType(assemblyTypeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.BILL_OF_MATERIAL_ID_TYPE: {
				BillOfMaterialIDType billOfMaterialIDType = (BillOfMaterialIDType)theEObject;
				T result = caseBillOfMaterialIDType(billOfMaterialIDType);
				if (result == null) result = caseIdentifierType(billOfMaterialIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.BILL_OF_MATERIALS_ID_TYPE: {
				BillOfMaterialsIDType billOfMaterialsIDType = (BillOfMaterialsIDType)theEObject;
				T result = caseBillOfMaterialsIDType(billOfMaterialsIDType);
				if (result == null) result = caseIdentifierType(billOfMaterialsIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.BILL_OF_RESOURCES_ID_TYPE: {
				BillOfResourcesIDType billOfResourcesIDType = (BillOfResourcesIDType)theEObject;
				T result = caseBillOfResourcesIDType(billOfResourcesIDType);
				if (result == null) result = caseIdentifierType(billOfResourcesIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.BINARY_OBJECT_TYPE: {
				BinaryObjectType binaryObjectType = (BinaryObjectType)theEObject;
				T result = caseBinaryObjectType(binaryObjectType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.BOD_TYPE: {
				BODType bodType = (BODType)theEObject;
				T result = caseBODType(bodType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: {
				CancelEquipmentCapabilityTestSpecType cancelEquipmentCapabilityTestSpecType = (CancelEquipmentCapabilityTestSpecType)theEObject;
				T result = caseCancelEquipmentCapabilityTestSpecType(cancelEquipmentCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_EQUIPMENT_CLASS_TYPE: {
				CancelEquipmentClassType cancelEquipmentClassType = (CancelEquipmentClassType)theEObject;
				T result = caseCancelEquipmentClassType(cancelEquipmentClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_EQUIPMENT_INFORMATION_TYPE: {
				CancelEquipmentInformationType cancelEquipmentInformationType = (CancelEquipmentInformationType)theEObject;
				T result = caseCancelEquipmentInformationType(cancelEquipmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_EQUIPMENT_TYPE: {
				CancelEquipmentType cancelEquipmentType = (CancelEquipmentType)theEObject;
				T result = caseCancelEquipmentType(cancelEquipmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_MATERIAL_CLASS_TYPE: {
				CancelMaterialClassType cancelMaterialClassType = (CancelMaterialClassType)theEObject;
				T result = caseCancelMaterialClassType(cancelMaterialClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_MATERIAL_DEFINITION_TYPE: {
				CancelMaterialDefinitionType cancelMaterialDefinitionType = (CancelMaterialDefinitionType)theEObject;
				T result = caseCancelMaterialDefinitionType(cancelMaterialDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_MATERIAL_INFORMATION_TYPE: {
				CancelMaterialInformationType cancelMaterialInformationType = (CancelMaterialInformationType)theEObject;
				T result = caseCancelMaterialInformationType(cancelMaterialInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_MATERIAL_LOT_TYPE: {
				CancelMaterialLotType cancelMaterialLotType = (CancelMaterialLotType)theEObject;
				T result = caseCancelMaterialLotType(cancelMaterialLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_MATERIAL_SUB_LOT_TYPE: {
				CancelMaterialSubLotType cancelMaterialSubLotType = (CancelMaterialSubLotType)theEObject;
				T result = caseCancelMaterialSubLotType(cancelMaterialSubLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_MATERIAL_TEST_SPEC_TYPE: {
				CancelMaterialTestSpecType cancelMaterialTestSpecType = (CancelMaterialTestSpecType)theEObject;
				T result = caseCancelMaterialTestSpecType(cancelMaterialTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_OPERATIONS_CAPABILITY_INFORMATION_TYPE: {
				CancelOperationsCapabilityInformationType cancelOperationsCapabilityInformationType = (CancelOperationsCapabilityInformationType)theEObject;
				T result = caseCancelOperationsCapabilityInformationType(cancelOperationsCapabilityInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_OPERATIONS_CAPABILITY_TYPE: {
				CancelOperationsCapabilityType cancelOperationsCapabilityType = (CancelOperationsCapabilityType)theEObject;
				T result = caseCancelOperationsCapabilityType(cancelOperationsCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_OPERATIONS_DEFINITION_INFORMATION_TYPE: {
				CancelOperationsDefinitionInformationType cancelOperationsDefinitionInformationType = (CancelOperationsDefinitionInformationType)theEObject;
				T result = caseCancelOperationsDefinitionInformationType(cancelOperationsDefinitionInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_OPERATIONS_DEFINITION_TYPE: {
				CancelOperationsDefinitionType cancelOperationsDefinitionType = (CancelOperationsDefinitionType)theEObject;
				T result = caseCancelOperationsDefinitionType(cancelOperationsDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_OPERATIONS_PERFORMANCE_TYPE: {
				CancelOperationsPerformanceType cancelOperationsPerformanceType = (CancelOperationsPerformanceType)theEObject;
				T result = caseCancelOperationsPerformanceType(cancelOperationsPerformanceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_OPERATIONS_SCHEDULE_TYPE: {
				CancelOperationsScheduleType cancelOperationsScheduleType = (CancelOperationsScheduleType)theEObject;
				T result = caseCancelOperationsScheduleType(cancelOperationsScheduleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_PERSONNEL_CLASS_TYPE: {
				CancelPersonnelClassType cancelPersonnelClassType = (CancelPersonnelClassType)theEObject;
				T result = caseCancelPersonnelClassType(cancelPersonnelClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE: {
				CancelPersonnelInformationType cancelPersonnelInformationType = (CancelPersonnelInformationType)theEObject;
				T result = caseCancelPersonnelInformationType(cancelPersonnelInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_PERSON_TYPE: {
				CancelPersonType cancelPersonType = (CancelPersonType)theEObject;
				T result = caseCancelPersonType(cancelPersonType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: {
				CancelPhysicalAssetCapabilityTestSpecType cancelPhysicalAssetCapabilityTestSpecType = (CancelPhysicalAssetCapabilityTestSpecType)theEObject;
				T result = caseCancelPhysicalAssetCapabilityTestSpecType(cancelPhysicalAssetCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_PHYSICAL_ASSET_CLASS_TYPE: {
				CancelPhysicalAssetClassType cancelPhysicalAssetClassType = (CancelPhysicalAssetClassType)theEObject;
				T result = caseCancelPhysicalAssetClassType(cancelPhysicalAssetClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_PHYSICAL_ASSET_INFORMATION_TYPE: {
				CancelPhysicalAssetInformationType cancelPhysicalAssetInformationType = (CancelPhysicalAssetInformationType)theEObject;
				T result = caseCancelPhysicalAssetInformationType(cancelPhysicalAssetInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_PHYSICAL_ASSET_TYPE: {
				CancelPhysicalAssetType cancelPhysicalAssetType = (CancelPhysicalAssetType)theEObject;
				T result = caseCancelPhysicalAssetType(cancelPhysicalAssetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_PROCESS_SEGMENT_INFORMATION_TYPE: {
				CancelProcessSegmentInformationType cancelProcessSegmentInformationType = (CancelProcessSegmentInformationType)theEObject;
				T result = caseCancelProcessSegmentInformationType(cancelProcessSegmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_PROCESS_SEGMENT_TYPE: {
				CancelProcessSegmentType cancelProcessSegmentType = (CancelProcessSegmentType)theEObject;
				T result = caseCancelProcessSegmentType(cancelProcessSegmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CANCEL_QUALIFICATION_TEST_SPECIFICATION_TYPE: {
				CancelQualificationTestSpecificationType cancelQualificationTestSpecificationType = (CancelQualificationTestSpecificationType)theEObject;
				T result = caseCancelQualificationTestSpecificationType(cancelQualificationTestSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CAPABILITY_TYPE1_TYPE: {
				CapabilityType1Type capabilityType1Type = (CapabilityType1Type)theEObject;
				T result = caseCapabilityType1Type(capabilityType1Type);
				if (result == null) result = caseCodeType(capabilityType1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CAPABILITY_TYPE_TYPE: {
				CapabilityTypeType capabilityTypeType = (CapabilityTypeType)theEObject;
				T result = caseCapabilityTypeType(capabilityTypeType);
				if (result == null) result = caseCapabilityType1Type(capabilityTypeType);
				if (result == null) result = caseCodeType(capabilityTypeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CAUSE_TYPE: {
				CauseType causeType = (CauseType)theEObject;
				T result = caseCauseType(causeType);
				if (result == null) result = caseCodeType(causeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CERTIFICATE_OF_ANALYSIS_REFERENCE_TYPE: {
				CertificateOfAnalysisReferenceType certificateOfAnalysisReferenceType = (CertificateOfAnalysisReferenceType)theEObject;
				T result = caseCertificateOfAnalysisReferenceType(certificateOfAnalysisReferenceType);
				if (result == null) result = caseIdentifierType(certificateOfAnalysisReferenceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: {
				ChangeEquipmentCapabilityTestSpecType changeEquipmentCapabilityTestSpecType = (ChangeEquipmentCapabilityTestSpecType)theEObject;
				T result = caseChangeEquipmentCapabilityTestSpecType(changeEquipmentCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_EQUIPMENT_CLASS_TYPE: {
				ChangeEquipmentClassType changeEquipmentClassType = (ChangeEquipmentClassType)theEObject;
				T result = caseChangeEquipmentClassType(changeEquipmentClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_EQUIPMENT_INFORMATION_TYPE: {
				ChangeEquipmentInformationType changeEquipmentInformationType = (ChangeEquipmentInformationType)theEObject;
				T result = caseChangeEquipmentInformationType(changeEquipmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_EQUIPMENT_TYPE: {
				ChangeEquipmentType changeEquipmentType = (ChangeEquipmentType)theEObject;
				T result = caseChangeEquipmentType(changeEquipmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_MATERIAL_CLASS_TYPE: {
				ChangeMaterialClassType changeMaterialClassType = (ChangeMaterialClassType)theEObject;
				T result = caseChangeMaterialClassType(changeMaterialClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_MATERIAL_DEFINITION_TYPE: {
				ChangeMaterialDefinitionType changeMaterialDefinitionType = (ChangeMaterialDefinitionType)theEObject;
				T result = caseChangeMaterialDefinitionType(changeMaterialDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_MATERIAL_INFORMATION_TYPE: {
				ChangeMaterialInformationType changeMaterialInformationType = (ChangeMaterialInformationType)theEObject;
				T result = caseChangeMaterialInformationType(changeMaterialInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_MATERIAL_LOT_TYPE: {
				ChangeMaterialLotType changeMaterialLotType = (ChangeMaterialLotType)theEObject;
				T result = caseChangeMaterialLotType(changeMaterialLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_MATERIAL_SUB_LOT_TYPE: {
				ChangeMaterialSubLotType changeMaterialSubLotType = (ChangeMaterialSubLotType)theEObject;
				T result = caseChangeMaterialSubLotType(changeMaterialSubLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_MATERIAL_TEST_SPEC_TYPE: {
				ChangeMaterialTestSpecType changeMaterialTestSpecType = (ChangeMaterialTestSpecType)theEObject;
				T result = caseChangeMaterialTestSpecType(changeMaterialTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_OPERATIONS_CAPABILITY_INFORMATION_TYPE: {
				ChangeOperationsCapabilityInformationType changeOperationsCapabilityInformationType = (ChangeOperationsCapabilityInformationType)theEObject;
				T result = caseChangeOperationsCapabilityInformationType(changeOperationsCapabilityInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_OPERATIONS_CAPABILITY_TYPE: {
				ChangeOperationsCapabilityType changeOperationsCapabilityType = (ChangeOperationsCapabilityType)theEObject;
				T result = caseChangeOperationsCapabilityType(changeOperationsCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_OPERATIONS_DEFINITION_INFORMATION_TYPE: {
				ChangeOperationsDefinitionInformationType changeOperationsDefinitionInformationType = (ChangeOperationsDefinitionInformationType)theEObject;
				T result = caseChangeOperationsDefinitionInformationType(changeOperationsDefinitionInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_OPERATIONS_DEFINITION_TYPE: {
				ChangeOperationsDefinitionType changeOperationsDefinitionType = (ChangeOperationsDefinitionType)theEObject;
				T result = caseChangeOperationsDefinitionType(changeOperationsDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_OPERATIONS_PERFORMANCE_TYPE: {
				ChangeOperationsPerformanceType changeOperationsPerformanceType = (ChangeOperationsPerformanceType)theEObject;
				T result = caseChangeOperationsPerformanceType(changeOperationsPerformanceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_OPERATIONS_SCHEDULE_TYPE: {
				ChangeOperationsScheduleType changeOperationsScheduleType = (ChangeOperationsScheduleType)theEObject;
				T result = caseChangeOperationsScheduleType(changeOperationsScheduleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_PERSONNEL_CLASS_TYPE: {
				ChangePersonnelClassType changePersonnelClassType = (ChangePersonnelClassType)theEObject;
				T result = caseChangePersonnelClassType(changePersonnelClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_PERSONNEL_INFORMATION_TYPE: {
				ChangePersonnelInformationType changePersonnelInformationType = (ChangePersonnelInformationType)theEObject;
				T result = caseChangePersonnelInformationType(changePersonnelInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_PERSON_TYPE: {
				ChangePersonType changePersonType = (ChangePersonType)theEObject;
				T result = caseChangePersonType(changePersonType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: {
				ChangePhysicalAssetCapabilityTestSpecType changePhysicalAssetCapabilityTestSpecType = (ChangePhysicalAssetCapabilityTestSpecType)theEObject;
				T result = caseChangePhysicalAssetCapabilityTestSpecType(changePhysicalAssetCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_PHYSICAL_ASSET_CLASS_TYPE: {
				ChangePhysicalAssetClassType changePhysicalAssetClassType = (ChangePhysicalAssetClassType)theEObject;
				T result = caseChangePhysicalAssetClassType(changePhysicalAssetClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_PHYSICAL_ASSET_INFORMATION_TYPE: {
				ChangePhysicalAssetInformationType changePhysicalAssetInformationType = (ChangePhysicalAssetInformationType)theEObject;
				T result = caseChangePhysicalAssetInformationType(changePhysicalAssetInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_PHYSICAL_ASSET_TYPE: {
				ChangePhysicalAssetType changePhysicalAssetType = (ChangePhysicalAssetType)theEObject;
				T result = caseChangePhysicalAssetType(changePhysicalAssetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_PROCESS_SEGMENT_INFORMATION_TYPE: {
				ChangeProcessSegmentInformationType changeProcessSegmentInformationType = (ChangeProcessSegmentInformationType)theEObject;
				T result = caseChangeProcessSegmentInformationType(changeProcessSegmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_PROCESS_SEGMENT_TYPE: {
				ChangeProcessSegmentType changeProcessSegmentType = (ChangeProcessSegmentType)theEObject;
				T result = caseChangeProcessSegmentType(changeProcessSegmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CHANGE_QUALIFICATION_TEST_SPECIFICATION_TYPE: {
				ChangeQualificationTestSpecificationType changeQualificationTestSpecificationType = (ChangeQualificationTestSpecificationType)theEObject;
				T result = caseChangeQualificationTestSpecificationType(changeQualificationTestSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CODE_TYPE: {
				CodeType codeType = (CodeType)theEObject;
				T result = caseCodeType(codeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CONFIDENCE_FACTOR_TYPE: {
				ConfidenceFactorType confidenceFactorType = (ConfidenceFactorType)theEObject;
				T result = caseConfidenceFactorType(confidenceFactorType);
				if (result == null) result = caseIdentifierType(confidenceFactorType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CONFIRM_BOD_TYPE: {
				ConfirmBODType confirmBODType = (ConfirmBODType)theEObject;
				T result = caseConfirmBODType(confirmBODType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.CORRECTION_TYPE: {
				CorrectionType correctionType = (CorrectionType)theEObject;
				T result = caseCorrectionType(correctionType);
				if (result == null) result = caseCodeType(correctionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE: {
				DataAreaType dataAreaType = (DataAreaType)theEObject;
				T result = caseDataAreaType(dataAreaType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE1: {
				DataAreaType1 dataAreaType1 = (DataAreaType1)theEObject;
				T result = caseDataAreaType1(dataAreaType1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE2: {
				DataAreaType2 dataAreaType2 = (DataAreaType2)theEObject;
				T result = caseDataAreaType2(dataAreaType2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE3: {
				DataAreaType3 dataAreaType3 = (DataAreaType3)theEObject;
				T result = caseDataAreaType3(dataAreaType3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE4: {
				DataAreaType4 dataAreaType4 = (DataAreaType4)theEObject;
				T result = caseDataAreaType4(dataAreaType4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE5: {
				DataAreaType5 dataAreaType5 = (DataAreaType5)theEObject;
				T result = caseDataAreaType5(dataAreaType5);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE6: {
				DataAreaType6 dataAreaType6 = (DataAreaType6)theEObject;
				T result = caseDataAreaType6(dataAreaType6);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE7: {
				DataAreaType7 dataAreaType7 = (DataAreaType7)theEObject;
				T result = caseDataAreaType7(dataAreaType7);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE8: {
				DataAreaType8 dataAreaType8 = (DataAreaType8)theEObject;
				T result = caseDataAreaType8(dataAreaType8);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE9: {
				DataAreaType9 dataAreaType9 = (DataAreaType9)theEObject;
				T result = caseDataAreaType9(dataAreaType9);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE10: {
				DataAreaType10 dataAreaType10 = (DataAreaType10)theEObject;
				T result = caseDataAreaType10(dataAreaType10);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE11: {
				DataAreaType11 dataAreaType11 = (DataAreaType11)theEObject;
				T result = caseDataAreaType11(dataAreaType11);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE12: {
				DataAreaType12 dataAreaType12 = (DataAreaType12)theEObject;
				T result = caseDataAreaType12(dataAreaType12);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE13: {
				DataAreaType13 dataAreaType13 = (DataAreaType13)theEObject;
				T result = caseDataAreaType13(dataAreaType13);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE14: {
				DataAreaType14 dataAreaType14 = (DataAreaType14)theEObject;
				T result = caseDataAreaType14(dataAreaType14);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE15: {
				DataAreaType15 dataAreaType15 = (DataAreaType15)theEObject;
				T result = caseDataAreaType15(dataAreaType15);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE16: {
				DataAreaType16 dataAreaType16 = (DataAreaType16)theEObject;
				T result = caseDataAreaType16(dataAreaType16);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE17: {
				DataAreaType17 dataAreaType17 = (DataAreaType17)theEObject;
				T result = caseDataAreaType17(dataAreaType17);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE18: {
				DataAreaType18 dataAreaType18 = (DataAreaType18)theEObject;
				T result = caseDataAreaType18(dataAreaType18);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE19: {
				DataAreaType19 dataAreaType19 = (DataAreaType19)theEObject;
				T result = caseDataAreaType19(dataAreaType19);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE20: {
				DataAreaType20 dataAreaType20 = (DataAreaType20)theEObject;
				T result = caseDataAreaType20(dataAreaType20);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE21: {
				DataAreaType21 dataAreaType21 = (DataAreaType21)theEObject;
				T result = caseDataAreaType21(dataAreaType21);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE22: {
				DataAreaType22 dataAreaType22 = (DataAreaType22)theEObject;
				T result = caseDataAreaType22(dataAreaType22);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE23: {
				DataAreaType23 dataAreaType23 = (DataAreaType23)theEObject;
				T result = caseDataAreaType23(dataAreaType23);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE24: {
				DataAreaType24 dataAreaType24 = (DataAreaType24)theEObject;
				T result = caseDataAreaType24(dataAreaType24);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE25: {
				DataAreaType25 dataAreaType25 = (DataAreaType25)theEObject;
				T result = caseDataAreaType25(dataAreaType25);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE26: {
				DataAreaType26 dataAreaType26 = (DataAreaType26)theEObject;
				T result = caseDataAreaType26(dataAreaType26);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE27: {
				DataAreaType27 dataAreaType27 = (DataAreaType27)theEObject;
				T result = caseDataAreaType27(dataAreaType27);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE28: {
				DataAreaType28 dataAreaType28 = (DataAreaType28)theEObject;
				T result = caseDataAreaType28(dataAreaType28);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE29: {
				DataAreaType29 dataAreaType29 = (DataAreaType29)theEObject;
				T result = caseDataAreaType29(dataAreaType29);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE30: {
				DataAreaType30 dataAreaType30 = (DataAreaType30)theEObject;
				T result = caseDataAreaType30(dataAreaType30);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE31: {
				DataAreaType31 dataAreaType31 = (DataAreaType31)theEObject;
				T result = caseDataAreaType31(dataAreaType31);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE32: {
				DataAreaType32 dataAreaType32 = (DataAreaType32)theEObject;
				T result = caseDataAreaType32(dataAreaType32);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE33: {
				DataAreaType33 dataAreaType33 = (DataAreaType33)theEObject;
				T result = caseDataAreaType33(dataAreaType33);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE34: {
				DataAreaType34 dataAreaType34 = (DataAreaType34)theEObject;
				T result = caseDataAreaType34(dataAreaType34);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE35: {
				DataAreaType35 dataAreaType35 = (DataAreaType35)theEObject;
				T result = caseDataAreaType35(dataAreaType35);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE36: {
				DataAreaType36 dataAreaType36 = (DataAreaType36)theEObject;
				T result = caseDataAreaType36(dataAreaType36);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE37: {
				DataAreaType37 dataAreaType37 = (DataAreaType37)theEObject;
				T result = caseDataAreaType37(dataAreaType37);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE38: {
				DataAreaType38 dataAreaType38 = (DataAreaType38)theEObject;
				T result = caseDataAreaType38(dataAreaType38);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE39: {
				DataAreaType39 dataAreaType39 = (DataAreaType39)theEObject;
				T result = caseDataAreaType39(dataAreaType39);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE40: {
				DataAreaType40 dataAreaType40 = (DataAreaType40)theEObject;
				T result = caseDataAreaType40(dataAreaType40);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE41: {
				DataAreaType41 dataAreaType41 = (DataAreaType41)theEObject;
				T result = caseDataAreaType41(dataAreaType41);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE42: {
				DataAreaType42 dataAreaType42 = (DataAreaType42)theEObject;
				T result = caseDataAreaType42(dataAreaType42);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE43: {
				DataAreaType43 dataAreaType43 = (DataAreaType43)theEObject;
				T result = caseDataAreaType43(dataAreaType43);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE44: {
				DataAreaType44 dataAreaType44 = (DataAreaType44)theEObject;
				T result = caseDataAreaType44(dataAreaType44);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE45: {
				DataAreaType45 dataAreaType45 = (DataAreaType45)theEObject;
				T result = caseDataAreaType45(dataAreaType45);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE46: {
				DataAreaType46 dataAreaType46 = (DataAreaType46)theEObject;
				T result = caseDataAreaType46(dataAreaType46);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE47: {
				DataAreaType47 dataAreaType47 = (DataAreaType47)theEObject;
				T result = caseDataAreaType47(dataAreaType47);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE48: {
				DataAreaType48 dataAreaType48 = (DataAreaType48)theEObject;
				T result = caseDataAreaType48(dataAreaType48);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE49: {
				DataAreaType49 dataAreaType49 = (DataAreaType49)theEObject;
				T result = caseDataAreaType49(dataAreaType49);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE50: {
				DataAreaType50 dataAreaType50 = (DataAreaType50)theEObject;
				T result = caseDataAreaType50(dataAreaType50);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE51: {
				DataAreaType51 dataAreaType51 = (DataAreaType51)theEObject;
				T result = caseDataAreaType51(dataAreaType51);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE52: {
				DataAreaType52 dataAreaType52 = (DataAreaType52)theEObject;
				T result = caseDataAreaType52(dataAreaType52);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE53: {
				DataAreaType53 dataAreaType53 = (DataAreaType53)theEObject;
				T result = caseDataAreaType53(dataAreaType53);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE54: {
				DataAreaType54 dataAreaType54 = (DataAreaType54)theEObject;
				T result = caseDataAreaType54(dataAreaType54);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE55: {
				DataAreaType55 dataAreaType55 = (DataAreaType55)theEObject;
				T result = caseDataAreaType55(dataAreaType55);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE56: {
				DataAreaType56 dataAreaType56 = (DataAreaType56)theEObject;
				T result = caseDataAreaType56(dataAreaType56);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE57: {
				DataAreaType57 dataAreaType57 = (DataAreaType57)theEObject;
				T result = caseDataAreaType57(dataAreaType57);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE58: {
				DataAreaType58 dataAreaType58 = (DataAreaType58)theEObject;
				T result = caseDataAreaType58(dataAreaType58);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE59: {
				DataAreaType59 dataAreaType59 = (DataAreaType59)theEObject;
				T result = caseDataAreaType59(dataAreaType59);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE60: {
				DataAreaType60 dataAreaType60 = (DataAreaType60)theEObject;
				T result = caseDataAreaType60(dataAreaType60);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE61: {
				DataAreaType61 dataAreaType61 = (DataAreaType61)theEObject;
				T result = caseDataAreaType61(dataAreaType61);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE62: {
				DataAreaType62 dataAreaType62 = (DataAreaType62)theEObject;
				T result = caseDataAreaType62(dataAreaType62);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE63: {
				DataAreaType63 dataAreaType63 = (DataAreaType63)theEObject;
				T result = caseDataAreaType63(dataAreaType63);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE64: {
				DataAreaType64 dataAreaType64 = (DataAreaType64)theEObject;
				T result = caseDataAreaType64(dataAreaType64);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE65: {
				DataAreaType65 dataAreaType65 = (DataAreaType65)theEObject;
				T result = caseDataAreaType65(dataAreaType65);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE66: {
				DataAreaType66 dataAreaType66 = (DataAreaType66)theEObject;
				T result = caseDataAreaType66(dataAreaType66);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE67: {
				DataAreaType67 dataAreaType67 = (DataAreaType67)theEObject;
				T result = caseDataAreaType67(dataAreaType67);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE68: {
				DataAreaType68 dataAreaType68 = (DataAreaType68)theEObject;
				T result = caseDataAreaType68(dataAreaType68);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE69: {
				DataAreaType69 dataAreaType69 = (DataAreaType69)theEObject;
				T result = caseDataAreaType69(dataAreaType69);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE70: {
				DataAreaType70 dataAreaType70 = (DataAreaType70)theEObject;
				T result = caseDataAreaType70(dataAreaType70);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE71: {
				DataAreaType71 dataAreaType71 = (DataAreaType71)theEObject;
				T result = caseDataAreaType71(dataAreaType71);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE72: {
				DataAreaType72 dataAreaType72 = (DataAreaType72)theEObject;
				T result = caseDataAreaType72(dataAreaType72);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE73: {
				DataAreaType73 dataAreaType73 = (DataAreaType73)theEObject;
				T result = caseDataAreaType73(dataAreaType73);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE74: {
				DataAreaType74 dataAreaType74 = (DataAreaType74)theEObject;
				T result = caseDataAreaType74(dataAreaType74);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE75: {
				DataAreaType75 dataAreaType75 = (DataAreaType75)theEObject;
				T result = caseDataAreaType75(dataAreaType75);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE76: {
				DataAreaType76 dataAreaType76 = (DataAreaType76)theEObject;
				T result = caseDataAreaType76(dataAreaType76);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE77: {
				DataAreaType77 dataAreaType77 = (DataAreaType77)theEObject;
				T result = caseDataAreaType77(dataAreaType77);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE78: {
				DataAreaType78 dataAreaType78 = (DataAreaType78)theEObject;
				T result = caseDataAreaType78(dataAreaType78);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE79: {
				DataAreaType79 dataAreaType79 = (DataAreaType79)theEObject;
				T result = caseDataAreaType79(dataAreaType79);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE80: {
				DataAreaType80 dataAreaType80 = (DataAreaType80)theEObject;
				T result = caseDataAreaType80(dataAreaType80);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE81: {
				DataAreaType81 dataAreaType81 = (DataAreaType81)theEObject;
				T result = caseDataAreaType81(dataAreaType81);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE82: {
				DataAreaType82 dataAreaType82 = (DataAreaType82)theEObject;
				T result = caseDataAreaType82(dataAreaType82);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE83: {
				DataAreaType83 dataAreaType83 = (DataAreaType83)theEObject;
				T result = caseDataAreaType83(dataAreaType83);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE84: {
				DataAreaType84 dataAreaType84 = (DataAreaType84)theEObject;
				T result = caseDataAreaType84(dataAreaType84);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE85: {
				DataAreaType85 dataAreaType85 = (DataAreaType85)theEObject;
				T result = caseDataAreaType85(dataAreaType85);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE86: {
				DataAreaType86 dataAreaType86 = (DataAreaType86)theEObject;
				T result = caseDataAreaType86(dataAreaType86);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE87: {
				DataAreaType87 dataAreaType87 = (DataAreaType87)theEObject;
				T result = caseDataAreaType87(dataAreaType87);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE88: {
				DataAreaType88 dataAreaType88 = (DataAreaType88)theEObject;
				T result = caseDataAreaType88(dataAreaType88);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE89: {
				DataAreaType89 dataAreaType89 = (DataAreaType89)theEObject;
				T result = caseDataAreaType89(dataAreaType89);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE90: {
				DataAreaType90 dataAreaType90 = (DataAreaType90)theEObject;
				T result = caseDataAreaType90(dataAreaType90);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE91: {
				DataAreaType91 dataAreaType91 = (DataAreaType91)theEObject;
				T result = caseDataAreaType91(dataAreaType91);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE92: {
				DataAreaType92 dataAreaType92 = (DataAreaType92)theEObject;
				T result = caseDataAreaType92(dataAreaType92);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE93: {
				DataAreaType93 dataAreaType93 = (DataAreaType93)theEObject;
				T result = caseDataAreaType93(dataAreaType93);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE94: {
				DataAreaType94 dataAreaType94 = (DataAreaType94)theEObject;
				T result = caseDataAreaType94(dataAreaType94);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE95: {
				DataAreaType95 dataAreaType95 = (DataAreaType95)theEObject;
				T result = caseDataAreaType95(dataAreaType95);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE96: {
				DataAreaType96 dataAreaType96 = (DataAreaType96)theEObject;
				T result = caseDataAreaType96(dataAreaType96);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE97: {
				DataAreaType97 dataAreaType97 = (DataAreaType97)theEObject;
				T result = caseDataAreaType97(dataAreaType97);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE98: {
				DataAreaType98 dataAreaType98 = (DataAreaType98)theEObject;
				T result = caseDataAreaType98(dataAreaType98);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE99: {
				DataAreaType99 dataAreaType99 = (DataAreaType99)theEObject;
				T result = caseDataAreaType99(dataAreaType99);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE100: {
				DataAreaType100 dataAreaType100 = (DataAreaType100)theEObject;
				T result = caseDataAreaType100(dataAreaType100);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE101: {
				DataAreaType101 dataAreaType101 = (DataAreaType101)theEObject;
				T result = caseDataAreaType101(dataAreaType101);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE102: {
				DataAreaType102 dataAreaType102 = (DataAreaType102)theEObject;
				T result = caseDataAreaType102(dataAreaType102);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE103: {
				DataAreaType103 dataAreaType103 = (DataAreaType103)theEObject;
				T result = caseDataAreaType103(dataAreaType103);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE104: {
				DataAreaType104 dataAreaType104 = (DataAreaType104)theEObject;
				T result = caseDataAreaType104(dataAreaType104);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE105: {
				DataAreaType105 dataAreaType105 = (DataAreaType105)theEObject;
				T result = caseDataAreaType105(dataAreaType105);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE106: {
				DataAreaType106 dataAreaType106 = (DataAreaType106)theEObject;
				T result = caseDataAreaType106(dataAreaType106);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE107: {
				DataAreaType107 dataAreaType107 = (DataAreaType107)theEObject;
				T result = caseDataAreaType107(dataAreaType107);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE108: {
				DataAreaType108 dataAreaType108 = (DataAreaType108)theEObject;
				T result = caseDataAreaType108(dataAreaType108);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE109: {
				DataAreaType109 dataAreaType109 = (DataAreaType109)theEObject;
				T result = caseDataAreaType109(dataAreaType109);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE110: {
				DataAreaType110 dataAreaType110 = (DataAreaType110)theEObject;
				T result = caseDataAreaType110(dataAreaType110);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE111: {
				DataAreaType111 dataAreaType111 = (DataAreaType111)theEObject;
				T result = caseDataAreaType111(dataAreaType111);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE112: {
				DataAreaType112 dataAreaType112 = (DataAreaType112)theEObject;
				T result = caseDataAreaType112(dataAreaType112);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE113: {
				DataAreaType113 dataAreaType113 = (DataAreaType113)theEObject;
				T result = caseDataAreaType113(dataAreaType113);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE114: {
				DataAreaType114 dataAreaType114 = (DataAreaType114)theEObject;
				T result = caseDataAreaType114(dataAreaType114);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE115: {
				DataAreaType115 dataAreaType115 = (DataAreaType115)theEObject;
				T result = caseDataAreaType115(dataAreaType115);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE116: {
				DataAreaType116 dataAreaType116 = (DataAreaType116)theEObject;
				T result = caseDataAreaType116(dataAreaType116);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE117: {
				DataAreaType117 dataAreaType117 = (DataAreaType117)theEObject;
				T result = caseDataAreaType117(dataAreaType117);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE118: {
				DataAreaType118 dataAreaType118 = (DataAreaType118)theEObject;
				T result = caseDataAreaType118(dataAreaType118);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE119: {
				DataAreaType119 dataAreaType119 = (DataAreaType119)theEObject;
				T result = caseDataAreaType119(dataAreaType119);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE120: {
				DataAreaType120 dataAreaType120 = (DataAreaType120)theEObject;
				T result = caseDataAreaType120(dataAreaType120);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE121: {
				DataAreaType121 dataAreaType121 = (DataAreaType121)theEObject;
				T result = caseDataAreaType121(dataAreaType121);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE122: {
				DataAreaType122 dataAreaType122 = (DataAreaType122)theEObject;
				T result = caseDataAreaType122(dataAreaType122);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE123: {
				DataAreaType123 dataAreaType123 = (DataAreaType123)theEObject;
				T result = caseDataAreaType123(dataAreaType123);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE124: {
				DataAreaType124 dataAreaType124 = (DataAreaType124)theEObject;
				T result = caseDataAreaType124(dataAreaType124);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE125: {
				DataAreaType125 dataAreaType125 = (DataAreaType125)theEObject;
				T result = caseDataAreaType125(dataAreaType125);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE126: {
				DataAreaType126 dataAreaType126 = (DataAreaType126)theEObject;
				T result = caseDataAreaType126(dataAreaType126);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE127: {
				DataAreaType127 dataAreaType127 = (DataAreaType127)theEObject;
				T result = caseDataAreaType127(dataAreaType127);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE128: {
				DataAreaType128 dataAreaType128 = (DataAreaType128)theEObject;
				T result = caseDataAreaType128(dataAreaType128);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE129: {
				DataAreaType129 dataAreaType129 = (DataAreaType129)theEObject;
				T result = caseDataAreaType129(dataAreaType129);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE130: {
				DataAreaType130 dataAreaType130 = (DataAreaType130)theEObject;
				T result = caseDataAreaType130(dataAreaType130);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE131: {
				DataAreaType131 dataAreaType131 = (DataAreaType131)theEObject;
				T result = caseDataAreaType131(dataAreaType131);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE132: {
				DataAreaType132 dataAreaType132 = (DataAreaType132)theEObject;
				T result = caseDataAreaType132(dataAreaType132);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE133: {
				DataAreaType133 dataAreaType133 = (DataAreaType133)theEObject;
				T result = caseDataAreaType133(dataAreaType133);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE134: {
				DataAreaType134 dataAreaType134 = (DataAreaType134)theEObject;
				T result = caseDataAreaType134(dataAreaType134);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE135: {
				DataAreaType135 dataAreaType135 = (DataAreaType135)theEObject;
				T result = caseDataAreaType135(dataAreaType135);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE136: {
				DataAreaType136 dataAreaType136 = (DataAreaType136)theEObject;
				T result = caseDataAreaType136(dataAreaType136);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE137: {
				DataAreaType137 dataAreaType137 = (DataAreaType137)theEObject;
				T result = caseDataAreaType137(dataAreaType137);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE138: {
				DataAreaType138 dataAreaType138 = (DataAreaType138)theEObject;
				T result = caseDataAreaType138(dataAreaType138);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE139: {
				DataAreaType139 dataAreaType139 = (DataAreaType139)theEObject;
				T result = caseDataAreaType139(dataAreaType139);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE140: {
				DataAreaType140 dataAreaType140 = (DataAreaType140)theEObject;
				T result = caseDataAreaType140(dataAreaType140);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE141: {
				DataAreaType141 dataAreaType141 = (DataAreaType141)theEObject;
				T result = caseDataAreaType141(dataAreaType141);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE142: {
				DataAreaType142 dataAreaType142 = (DataAreaType142)theEObject;
				T result = caseDataAreaType142(dataAreaType142);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE143: {
				DataAreaType143 dataAreaType143 = (DataAreaType143)theEObject;
				T result = caseDataAreaType143(dataAreaType143);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE144: {
				DataAreaType144 dataAreaType144 = (DataAreaType144)theEObject;
				T result = caseDataAreaType144(dataAreaType144);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE145: {
				DataAreaType145 dataAreaType145 = (DataAreaType145)theEObject;
				T result = caseDataAreaType145(dataAreaType145);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE146: {
				DataAreaType146 dataAreaType146 = (DataAreaType146)theEObject;
				T result = caseDataAreaType146(dataAreaType146);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE147: {
				DataAreaType147 dataAreaType147 = (DataAreaType147)theEObject;
				T result = caseDataAreaType147(dataAreaType147);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE148: {
				DataAreaType148 dataAreaType148 = (DataAreaType148)theEObject;
				T result = caseDataAreaType148(dataAreaType148);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE149: {
				DataAreaType149 dataAreaType149 = (DataAreaType149)theEObject;
				T result = caseDataAreaType149(dataAreaType149);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE150: {
				DataAreaType150 dataAreaType150 = (DataAreaType150)theEObject;
				T result = caseDataAreaType150(dataAreaType150);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE151: {
				DataAreaType151 dataAreaType151 = (DataAreaType151)theEObject;
				T result = caseDataAreaType151(dataAreaType151);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE152: {
				DataAreaType152 dataAreaType152 = (DataAreaType152)theEObject;
				T result = caseDataAreaType152(dataAreaType152);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE153: {
				DataAreaType153 dataAreaType153 = (DataAreaType153)theEObject;
				T result = caseDataAreaType153(dataAreaType153);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE154: {
				DataAreaType154 dataAreaType154 = (DataAreaType154)theEObject;
				T result = caseDataAreaType154(dataAreaType154);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE155: {
				DataAreaType155 dataAreaType155 = (DataAreaType155)theEObject;
				T result = caseDataAreaType155(dataAreaType155);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE156: {
				DataAreaType156 dataAreaType156 = (DataAreaType156)theEObject;
				T result = caseDataAreaType156(dataAreaType156);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE157: {
				DataAreaType157 dataAreaType157 = (DataAreaType157)theEObject;
				T result = caseDataAreaType157(dataAreaType157);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE158: {
				DataAreaType158 dataAreaType158 = (DataAreaType158)theEObject;
				T result = caseDataAreaType158(dataAreaType158);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE159: {
				DataAreaType159 dataAreaType159 = (DataAreaType159)theEObject;
				T result = caseDataAreaType159(dataAreaType159);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE160: {
				DataAreaType160 dataAreaType160 = (DataAreaType160)theEObject;
				T result = caseDataAreaType160(dataAreaType160);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE161: {
				DataAreaType161 dataAreaType161 = (DataAreaType161)theEObject;
				T result = caseDataAreaType161(dataAreaType161);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE162: {
				DataAreaType162 dataAreaType162 = (DataAreaType162)theEObject;
				T result = caseDataAreaType162(dataAreaType162);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE163: {
				DataAreaType163 dataAreaType163 = (DataAreaType163)theEObject;
				T result = caseDataAreaType163(dataAreaType163);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE164: {
				DataAreaType164 dataAreaType164 = (DataAreaType164)theEObject;
				T result = caseDataAreaType164(dataAreaType164);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE165: {
				DataAreaType165 dataAreaType165 = (DataAreaType165)theEObject;
				T result = caseDataAreaType165(dataAreaType165);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE166: {
				DataAreaType166 dataAreaType166 = (DataAreaType166)theEObject;
				T result = caseDataAreaType166(dataAreaType166);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE167: {
				DataAreaType167 dataAreaType167 = (DataAreaType167)theEObject;
				T result = caseDataAreaType167(dataAreaType167);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE168: {
				DataAreaType168 dataAreaType168 = (DataAreaType168)theEObject;
				T result = caseDataAreaType168(dataAreaType168);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE169: {
				DataAreaType169 dataAreaType169 = (DataAreaType169)theEObject;
				T result = caseDataAreaType169(dataAreaType169);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE170: {
				DataAreaType170 dataAreaType170 = (DataAreaType170)theEObject;
				T result = caseDataAreaType170(dataAreaType170);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE171: {
				DataAreaType171 dataAreaType171 = (DataAreaType171)theEObject;
				T result = caseDataAreaType171(dataAreaType171);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE172: {
				DataAreaType172 dataAreaType172 = (DataAreaType172)theEObject;
				T result = caseDataAreaType172(dataAreaType172);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE173: {
				DataAreaType173 dataAreaType173 = (DataAreaType173)theEObject;
				T result = caseDataAreaType173(dataAreaType173);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE174: {
				DataAreaType174 dataAreaType174 = (DataAreaType174)theEObject;
				T result = caseDataAreaType174(dataAreaType174);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE175: {
				DataAreaType175 dataAreaType175 = (DataAreaType175)theEObject;
				T result = caseDataAreaType175(dataAreaType175);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE176: {
				DataAreaType176 dataAreaType176 = (DataAreaType176)theEObject;
				T result = caseDataAreaType176(dataAreaType176);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE177: {
				DataAreaType177 dataAreaType177 = (DataAreaType177)theEObject;
				T result = caseDataAreaType177(dataAreaType177);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE178: {
				DataAreaType178 dataAreaType178 = (DataAreaType178)theEObject;
				T result = caseDataAreaType178(dataAreaType178);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE179: {
				DataAreaType179 dataAreaType179 = (DataAreaType179)theEObject;
				T result = caseDataAreaType179(dataAreaType179);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE180: {
				DataAreaType180 dataAreaType180 = (DataAreaType180)theEObject;
				T result = caseDataAreaType180(dataAreaType180);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE181: {
				DataAreaType181 dataAreaType181 = (DataAreaType181)theEObject;
				T result = caseDataAreaType181(dataAreaType181);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE182: {
				DataAreaType182 dataAreaType182 = (DataAreaType182)theEObject;
				T result = caseDataAreaType182(dataAreaType182);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE183: {
				DataAreaType183 dataAreaType183 = (DataAreaType183)theEObject;
				T result = caseDataAreaType183(dataAreaType183);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE184: {
				DataAreaType184 dataAreaType184 = (DataAreaType184)theEObject;
				T result = caseDataAreaType184(dataAreaType184);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE185: {
				DataAreaType185 dataAreaType185 = (DataAreaType185)theEObject;
				T result = caseDataAreaType185(dataAreaType185);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE186: {
				DataAreaType186 dataAreaType186 = (DataAreaType186)theEObject;
				T result = caseDataAreaType186(dataAreaType186);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE187: {
				DataAreaType187 dataAreaType187 = (DataAreaType187)theEObject;
				T result = caseDataAreaType187(dataAreaType187);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE188: {
				DataAreaType188 dataAreaType188 = (DataAreaType188)theEObject;
				T result = caseDataAreaType188(dataAreaType188);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE189: {
				DataAreaType189 dataAreaType189 = (DataAreaType189)theEObject;
				T result = caseDataAreaType189(dataAreaType189);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE190: {
				DataAreaType190 dataAreaType190 = (DataAreaType190)theEObject;
				T result = caseDataAreaType190(dataAreaType190);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE191: {
				DataAreaType191 dataAreaType191 = (DataAreaType191)theEObject;
				T result = caseDataAreaType191(dataAreaType191);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE192: {
				DataAreaType192 dataAreaType192 = (DataAreaType192)theEObject;
				T result = caseDataAreaType192(dataAreaType192);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE193: {
				DataAreaType193 dataAreaType193 = (DataAreaType193)theEObject;
				T result = caseDataAreaType193(dataAreaType193);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE194: {
				DataAreaType194 dataAreaType194 = (DataAreaType194)theEObject;
				T result = caseDataAreaType194(dataAreaType194);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE195: {
				DataAreaType195 dataAreaType195 = (DataAreaType195)theEObject;
				T result = caseDataAreaType195(dataAreaType195);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE196: {
				DataAreaType196 dataAreaType196 = (DataAreaType196)theEObject;
				T result = caseDataAreaType196(dataAreaType196);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE197: {
				DataAreaType197 dataAreaType197 = (DataAreaType197)theEObject;
				T result = caseDataAreaType197(dataAreaType197);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE198: {
				DataAreaType198 dataAreaType198 = (DataAreaType198)theEObject;
				T result = caseDataAreaType198(dataAreaType198);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE199: {
				DataAreaType199 dataAreaType199 = (DataAreaType199)theEObject;
				T result = caseDataAreaType199(dataAreaType199);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE200: {
				DataAreaType200 dataAreaType200 = (DataAreaType200)theEObject;
				T result = caseDataAreaType200(dataAreaType200);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE201: {
				DataAreaType201 dataAreaType201 = (DataAreaType201)theEObject;
				T result = caseDataAreaType201(dataAreaType201);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE202: {
				DataAreaType202 dataAreaType202 = (DataAreaType202)theEObject;
				T result = caseDataAreaType202(dataAreaType202);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE203: {
				DataAreaType203 dataAreaType203 = (DataAreaType203)theEObject;
				T result = caseDataAreaType203(dataAreaType203);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE204: {
				DataAreaType204 dataAreaType204 = (DataAreaType204)theEObject;
				T result = caseDataAreaType204(dataAreaType204);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE205: {
				DataAreaType205 dataAreaType205 = (DataAreaType205)theEObject;
				T result = caseDataAreaType205(dataAreaType205);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE206: {
				DataAreaType206 dataAreaType206 = (DataAreaType206)theEObject;
				T result = caseDataAreaType206(dataAreaType206);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE207: {
				DataAreaType207 dataAreaType207 = (DataAreaType207)theEObject;
				T result = caseDataAreaType207(dataAreaType207);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_AREA_TYPE208: {
				DataAreaType208 dataAreaType208 = (DataAreaType208)theEObject;
				T result = caseDataAreaType208(dataAreaType208);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_TYPE1_TYPE: {
				DataType1Type dataType1Type = (DataType1Type)theEObject;
				T result = caseDataType1Type(dataType1Type);
				if (result == null) result = caseCodeType(dataType1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATA_TYPE_TYPE: {
				DataTypeType dataTypeType = (DataTypeType)theEObject;
				T result = caseDataTypeType(dataTypeType);
				if (result == null) result = caseDataType1Type(dataTypeType);
				if (result == null) result = caseCodeType(dataTypeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DATE_TIME_TYPE: {
				DateTimeType dateTimeType = (DateTimeType)theEObject;
				T result = caseDateTimeType(dateTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DEPENDENCY1_TYPE: {
				Dependency1Type dependency1Type = (Dependency1Type)theEObject;
				T result = caseDependency1Type(dependency1Type);
				if (result == null) result = caseCodeType(dependency1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DEPENDENCY_TYPE: {
				DependencyType dependencyType = (DependencyType)theEObject;
				T result = caseDependencyType(dependencyType);
				if (result == null) result = caseDependency1Type(dependencyType);
				if (result == null) result = caseCodeType(dependencyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DESCRIPTION_TYPE: {
				DescriptionType descriptionType = (DescriptionType)theEObject;
				T result = caseDescriptionType(descriptionType);
				if (result == null) result = caseTextType(descriptionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.DOCUMENT_ROOT: {
				DocumentRoot documentRoot = (DocumentRoot)theEObject;
				T result = caseDocumentRoot(documentRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EARLIEST_START_TIME_TYPE: {
				EarliestStartTimeType earliestStartTimeType = (EarliestStartTimeType)theEObject;
				T result = caseEarliestStartTimeType(earliestStartTimeType);
				if (result == null) result = caseDateTimeType(earliestStartTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.END_TIME_TYPE: {
				EndTimeType endTimeType = (EndTimeType)theEObject;
				T result = caseEndTimeType(endTimeType);
				if (result == null) result = caseDateTimeType(endTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE: {
				EquipmentAssetMappingType equipmentAssetMappingType = (EquipmentAssetMappingType)theEObject;
				T result = caseEquipmentAssetMappingType(equipmentAssetMappingType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID_TYPE: {
				EquipmentCapabilityTestSpecificationIDType equipmentCapabilityTestSpecificationIDType = (EquipmentCapabilityTestSpecificationIDType)theEObject;
				T result = caseEquipmentCapabilityTestSpecificationIDType(equipmentCapabilityTestSpecificationIDType);
				if (result == null) result = caseIdentifierType(equipmentCapabilityTestSpecificationIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE: {
				EquipmentCapabilityTestSpecificationType equipmentCapabilityTestSpecificationType = (EquipmentCapabilityTestSpecificationType)theEObject;
				T result = caseEquipmentCapabilityTestSpecificationType(equipmentCapabilityTestSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_CLASS_ID_TYPE: {
				EquipmentClassIDType equipmentClassIDType = (EquipmentClassIDType)theEObject;
				T result = caseEquipmentClassIDType(equipmentClassIDType);
				if (result == null) result = caseIdentifierType(equipmentClassIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE: {
				EquipmentClassPropertyType equipmentClassPropertyType = (EquipmentClassPropertyType)theEObject;
				T result = caseEquipmentClassPropertyType(equipmentClassPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_CLASS_TYPE: {
				EquipmentClassType equipmentClassType = (EquipmentClassType)theEObject;
				T result = caseEquipmentClassType(equipmentClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_ELEMENT_LEVEL1_TYPE: {
				EquipmentElementLevel1Type equipmentElementLevel1Type = (EquipmentElementLevel1Type)theEObject;
				T result = caseEquipmentElementLevel1Type(equipmentElementLevel1Type);
				if (result == null) result = caseCodeType(equipmentElementLevel1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_ELEMENT_LEVEL_TYPE: {
				EquipmentElementLevelType equipmentElementLevelType = (EquipmentElementLevelType)theEObject;
				T result = caseEquipmentElementLevelType(equipmentElementLevelType);
				if (result == null) result = caseEquipmentElementLevel1Type(equipmentElementLevelType);
				if (result == null) result = caseCodeType(equipmentElementLevelType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_ID_TYPE: {
				EquipmentIDType equipmentIDType = (EquipmentIDType)theEObject;
				T result = caseEquipmentIDType(equipmentIDType);
				if (result == null) result = caseIdentifierType(equipmentIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_INFORMATION_TYPE: {
				EquipmentInformationType equipmentInformationType = (EquipmentInformationType)theEObject;
				T result = caseEquipmentInformationType(equipmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_PROPERTY_TYPE: {
				EquipmentPropertyType equipmentPropertyType = (EquipmentPropertyType)theEObject;
				T result = caseEquipmentPropertyType(equipmentPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY_TYPE: {
				EquipmentSegmentSpecificationPropertyType equipmentSegmentSpecificationPropertyType = (EquipmentSegmentSpecificationPropertyType)theEObject;
				T result = caseEquipmentSegmentSpecificationPropertyType(equipmentSegmentSpecificationPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE: {
				EquipmentSegmentSpecificationType equipmentSegmentSpecificationType = (EquipmentSegmentSpecificationType)theEObject;
				T result = caseEquipmentSegmentSpecificationType(equipmentSegmentSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_TYPE: {
				EquipmentType equipmentType = (EquipmentType)theEObject;
				T result = caseEquipmentType(equipmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EQUIPMENT_USE_TYPE: {
				EquipmentUseType equipmentUseType = (EquipmentUseType)theEObject;
				T result = caseEquipmentUseType(equipmentUseType);
				if (result == null) result = caseCodeType(equipmentUseType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.EXPIRATION_TIME_TYPE: {
				ExpirationTimeType expirationTimeType = (ExpirationTimeType)theEObject;
				T result = caseExpirationTimeType(expirationTimeType);
				if (result == null) result = caseDateTimeType(expirationTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: {
				GetEquipmentCapabilityTestSpecType getEquipmentCapabilityTestSpecType = (GetEquipmentCapabilityTestSpecType)theEObject;
				T result = caseGetEquipmentCapabilityTestSpecType(getEquipmentCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_EQUIPMENT_CLASS_TYPE: {
				GetEquipmentClassType getEquipmentClassType = (GetEquipmentClassType)theEObject;
				T result = caseGetEquipmentClassType(getEquipmentClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_EQUIPMENT_INFORMATION_TYPE: {
				GetEquipmentInformationType getEquipmentInformationType = (GetEquipmentInformationType)theEObject;
				T result = caseGetEquipmentInformationType(getEquipmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_EQUIPMENT_TYPE: {
				GetEquipmentType getEquipmentType = (GetEquipmentType)theEObject;
				T result = caseGetEquipmentType(getEquipmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_MATERIAL_CLASS_TYPE: {
				GetMaterialClassType getMaterialClassType = (GetMaterialClassType)theEObject;
				T result = caseGetMaterialClassType(getMaterialClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_MATERIAL_DEFINITION_TYPE: {
				GetMaterialDefinitionType getMaterialDefinitionType = (GetMaterialDefinitionType)theEObject;
				T result = caseGetMaterialDefinitionType(getMaterialDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_MATERIAL_INFORMATION_TYPE: {
				GetMaterialInformationType getMaterialInformationType = (GetMaterialInformationType)theEObject;
				T result = caseGetMaterialInformationType(getMaterialInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_MATERIAL_LOT_TYPE: {
				GetMaterialLotType getMaterialLotType = (GetMaterialLotType)theEObject;
				T result = caseGetMaterialLotType(getMaterialLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_MATERIAL_SUB_LOT_TYPE: {
				GetMaterialSubLotType getMaterialSubLotType = (GetMaterialSubLotType)theEObject;
				T result = caseGetMaterialSubLotType(getMaterialSubLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_MATERIAL_TEST_SPEC_TYPE: {
				GetMaterialTestSpecType getMaterialTestSpecType = (GetMaterialTestSpecType)theEObject;
				T result = caseGetMaterialTestSpecType(getMaterialTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_OPERATIONS_CAPABILITY_INFORMATION_TYPE: {
				GetOperationsCapabilityInformationType getOperationsCapabilityInformationType = (GetOperationsCapabilityInformationType)theEObject;
				T result = caseGetOperationsCapabilityInformationType(getOperationsCapabilityInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_OPERATIONS_CAPABILITY_TYPE: {
				GetOperationsCapabilityType getOperationsCapabilityType = (GetOperationsCapabilityType)theEObject;
				T result = caseGetOperationsCapabilityType(getOperationsCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_OPERATIONS_DEFINITION_INFORMATION_TYPE: {
				GetOperationsDefinitionInformationType getOperationsDefinitionInformationType = (GetOperationsDefinitionInformationType)theEObject;
				T result = caseGetOperationsDefinitionInformationType(getOperationsDefinitionInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_OPERATIONS_DEFINITION_TYPE: {
				GetOperationsDefinitionType getOperationsDefinitionType = (GetOperationsDefinitionType)theEObject;
				T result = caseGetOperationsDefinitionType(getOperationsDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_OPERATIONS_PERFORMANCE_TYPE: {
				GetOperationsPerformanceType getOperationsPerformanceType = (GetOperationsPerformanceType)theEObject;
				T result = caseGetOperationsPerformanceType(getOperationsPerformanceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_OPERATIONS_SCHEDULE_TYPE: {
				GetOperationsScheduleType getOperationsScheduleType = (GetOperationsScheduleType)theEObject;
				T result = caseGetOperationsScheduleType(getOperationsScheduleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_PERSONNEL_CLASS_TYPE: {
				GetPersonnelClassType getPersonnelClassType = (GetPersonnelClassType)theEObject;
				T result = caseGetPersonnelClassType(getPersonnelClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_PERSONNEL_INFORMATION_TYPE: {
				GetPersonnelInformationType getPersonnelInformationType = (GetPersonnelInformationType)theEObject;
				T result = caseGetPersonnelInformationType(getPersonnelInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_PERSON_TYPE: {
				GetPersonType getPersonType = (GetPersonType)theEObject;
				T result = caseGetPersonType(getPersonType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: {
				GetPhysicalAssetCapabilityTestSpecType getPhysicalAssetCapabilityTestSpecType = (GetPhysicalAssetCapabilityTestSpecType)theEObject;
				T result = caseGetPhysicalAssetCapabilityTestSpecType(getPhysicalAssetCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_PHYSICAL_ASSET_CLASS_TYPE: {
				GetPhysicalAssetClassType getPhysicalAssetClassType = (GetPhysicalAssetClassType)theEObject;
				T result = caseGetPhysicalAssetClassType(getPhysicalAssetClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_PHYSICAL_ASSET_INFORMATION_TYPE: {
				GetPhysicalAssetInformationType getPhysicalAssetInformationType = (GetPhysicalAssetInformationType)theEObject;
				T result = caseGetPhysicalAssetInformationType(getPhysicalAssetInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_PHYSICAL_ASSET_TYPE: {
				GetPhysicalAssetType getPhysicalAssetType = (GetPhysicalAssetType)theEObject;
				T result = caseGetPhysicalAssetType(getPhysicalAssetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_PROCESS_SEGMENT_INFORMATION_TYPE: {
				GetProcessSegmentInformationType getProcessSegmentInformationType = (GetProcessSegmentInformationType)theEObject;
				T result = caseGetProcessSegmentInformationType(getProcessSegmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_PROCESS_SEGMENT_TYPE: {
				GetProcessSegmentType getProcessSegmentType = (GetProcessSegmentType)theEObject;
				T result = caseGetProcessSegmentType(getProcessSegmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.GET_QUALIFICATION_TEST_SPECIFICATION_TYPE: {
				GetQualificationTestSpecificationType getQualificationTestSpecificationType = (GetQualificationTestSpecificationType)theEObject;
				T result = caseGetQualificationTestSpecificationType(getQualificationTestSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE: {
				HierarchyScopeType hierarchyScopeType = (HierarchyScopeType)theEObject;
				T result = caseHierarchyScopeType(hierarchyScopeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.IDENTIFIER_TYPE: {
				IdentifierType identifierType = (IdentifierType)theEObject;
				T result = caseIdentifierType(identifierType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.JOB_ORDER_COMMAND1_TYPE: {
				JobOrderCommand1Type jobOrderCommand1Type = (JobOrderCommand1Type)theEObject;
				T result = caseJobOrderCommand1Type(jobOrderCommand1Type);
				if (result == null) result = caseCodeType(jobOrderCommand1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.JOB_ORDER_COMMAND_RULE_TYPE: {
				JobOrderCommandRuleType jobOrderCommandRuleType = (JobOrderCommandRuleType)theEObject;
				T result = caseJobOrderCommandRuleType(jobOrderCommandRuleType);
				if (result == null) result = caseTextType(jobOrderCommandRuleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.JOB_ORDER_COMMAND_TYPE: {
				JobOrderCommandType jobOrderCommandType = (JobOrderCommandType)theEObject;
				T result = caseJobOrderCommandType(jobOrderCommandType);
				if (result == null) result = caseJobOrderCommand1Type(jobOrderCommandType);
				if (result == null) result = caseCodeType(jobOrderCommandType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.JOB_ORDER_DISPATCH_STATUS_TYPE: {
				JobOrderDispatchStatusType jobOrderDispatchStatusType = (JobOrderDispatchStatusType)theEObject;
				T result = caseJobOrderDispatchStatusType(jobOrderDispatchStatusType);
				if (result == null) result = caseCodeType(jobOrderDispatchStatusType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.LATEST_END_TIME_TYPE: {
				LatestEndTimeType latestEndTimeType = (LatestEndTimeType)theEObject;
				T result = caseLatestEndTimeType(latestEndTimeType);
				if (result == null) result = caseDateTimeType(latestEndTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.LOCATION_TYPE: {
				LocationType locationType = (LocationType)theEObject;
				T result = caseLocationType(locationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MANUFACTURING_BILL_ID_TYPE: {
				ManufacturingBillIDType manufacturingBillIDType = (ManufacturingBillIDType)theEObject;
				T result = caseManufacturingBillIDType(manufacturingBillIDType);
				if (result == null) result = caseIdentifierType(manufacturingBillIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_ACTUAL_ID_TYPE: {
				MaterialActualIDType materialActualIDType = (MaterialActualIDType)theEObject;
				T result = caseMaterialActualIDType(materialActualIDType);
				if (result == null) result = caseIdentifierType(materialActualIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_CAPABILITY_ID_TYPE: {
				MaterialCapabilityIDType materialCapabilityIDType = (MaterialCapabilityIDType)theEObject;
				T result = caseMaterialCapabilityIDType(materialCapabilityIDType);
				if (result == null) result = caseIdentifierType(materialCapabilityIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_CLASS_ID_TYPE: {
				MaterialClassIDType materialClassIDType = (MaterialClassIDType)theEObject;
				T result = caseMaterialClassIDType(materialClassIDType);
				if (result == null) result = caseIdentifierType(materialClassIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_CLASS_PROPERTY_TYPE: {
				MaterialClassPropertyType materialClassPropertyType = (MaterialClassPropertyType)theEObject;
				T result = caseMaterialClassPropertyType(materialClassPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_CLASS_TYPE: {
				MaterialClassType materialClassType = (MaterialClassType)theEObject;
				T result = caseMaterialClassType(materialClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_DEFINITION_ID_TYPE: {
				MaterialDefinitionIDType materialDefinitionIDType = (MaterialDefinitionIDType)theEObject;
				T result = caseMaterialDefinitionIDType(materialDefinitionIDType);
				if (result == null) result = caseIdentifierType(materialDefinitionIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_DEFINITION_PROPERTY_TYPE: {
				MaterialDefinitionPropertyType materialDefinitionPropertyType = (MaterialDefinitionPropertyType)theEObject;
				T result = caseMaterialDefinitionPropertyType(materialDefinitionPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_DEFINITION_TYPE: {
				MaterialDefinitionType materialDefinitionType = (MaterialDefinitionType)theEObject;
				T result = caseMaterialDefinitionType(materialDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE: {
				MaterialInformationType materialInformationType = (MaterialInformationType)theEObject;
				T result = caseMaterialInformationType(materialInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_LOT_ID_TYPE: {
				MaterialLotIDType materialLotIDType = (MaterialLotIDType)theEObject;
				T result = caseMaterialLotIDType(materialLotIDType);
				if (result == null) result = caseIdentifierType(materialLotIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_LOT_PROPERTY_TYPE: {
				MaterialLotPropertyType materialLotPropertyType = (MaterialLotPropertyType)theEObject;
				T result = caseMaterialLotPropertyType(materialLotPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_LOT_TYPE: {
				MaterialLotType materialLotType = (MaterialLotType)theEObject;
				T result = caseMaterialLotType(materialLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_REQUIREMENT_ID_TYPE: {
				MaterialRequirementIDType materialRequirementIDType = (MaterialRequirementIDType)theEObject;
				T result = caseMaterialRequirementIDType(materialRequirementIDType);
				if (result == null) result = caseIdentifierType(materialRequirementIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_PROPERTY_TYPE: {
				MaterialSegmentSpecificationPropertyType materialSegmentSpecificationPropertyType = (MaterialSegmentSpecificationPropertyType)theEObject;
				T result = caseMaterialSegmentSpecificationPropertyType(materialSegmentSpecificationPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE: {
				MaterialSegmentSpecificationType materialSegmentSpecificationType = (MaterialSegmentSpecificationType)theEObject;
				T result = caseMaterialSegmentSpecificationType(materialSegmentSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_SPECIFICATION_ID_TYPE: {
				MaterialSpecificationIDType materialSpecificationIDType = (MaterialSpecificationIDType)theEObject;
				T result = caseMaterialSpecificationIDType(materialSpecificationIDType);
				if (result == null) result = caseIdentifierType(materialSpecificationIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_SUB_LOT_ID_TYPE: {
				MaterialSubLotIDType materialSubLotIDType = (MaterialSubLotIDType)theEObject;
				T result = caseMaterialSubLotIDType(materialSubLotIDType);
				if (result == null) result = caseIdentifierType(materialSubLotIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE: {
				MaterialSubLotType materialSubLotType = (MaterialSubLotType)theEObject;
				T result = caseMaterialSubLotType(materialSubLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_TEST_SPECIFICATION_ID_TYPE: {
				MaterialTestSpecificationIDType materialTestSpecificationIDType = (MaterialTestSpecificationIDType)theEObject;
				T result = caseMaterialTestSpecificationIDType(materialTestSpecificationIDType);
				if (result == null) result = caseIdentifierType(materialTestSpecificationIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_TEST_SPECIFICATION_TYPE: {
				MaterialTestSpecificationType materialTestSpecificationType = (MaterialTestSpecificationType)theEObject;
				T result = caseMaterialTestSpecificationType(materialTestSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_USE1_TYPE: {
				MaterialUse1Type materialUse1Type = (MaterialUse1Type)theEObject;
				T result = caseMaterialUse1Type(materialUse1Type);
				if (result == null) result = caseCodeType(materialUse1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MATERIAL_USE_TYPE: {
				MaterialUseType materialUseType = (MaterialUseType)theEObject;
				T result = caseMaterialUseType(materialUseType);
				if (result == null) result = caseMaterialUse1Type(materialUseType);
				if (result == null) result = caseCodeType(materialUseType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.MEASURE_TYPE: {
				MeasureType measureType = (MeasureType)theEObject;
				T result = caseMeasureType(measureType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.NAME_TYPE: {
				NameType nameType = (NameType)theEObject;
				T result = caseNameType(nameType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.NUMERIC_TYPE: {
				NumericType numericType = (NumericType)theEObject;
				T result = caseNumericType(numericType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_EQUIPMENT_ACTUAL_PROPERTY_TYPE: {
				OpEquipmentActualPropertyType opEquipmentActualPropertyType = (OpEquipmentActualPropertyType)theEObject;
				T result = caseOpEquipmentActualPropertyType(opEquipmentActualPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_EQUIPMENT_ACTUAL_TYPE: {
				OpEquipmentActualType opEquipmentActualType = (OpEquipmentActualType)theEObject;
				T result = caseOpEquipmentActualType(opEquipmentActualType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_EQUIPMENT_CAPABILITY_PROPERTY_TYPE: {
				OpEquipmentCapabilityPropertyType opEquipmentCapabilityPropertyType = (OpEquipmentCapabilityPropertyType)theEObject;
				T result = caseOpEquipmentCapabilityPropertyType(opEquipmentCapabilityPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_EQUIPMENT_CAPABILITY_TYPE: {
				OpEquipmentCapabilityType opEquipmentCapabilityType = (OpEquipmentCapabilityType)theEObject;
				T result = caseOpEquipmentCapabilityType(opEquipmentCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_EQUIPMENT_REQUIREMENT_PROPERTY_TYPE: {
				OpEquipmentRequirementPropertyType opEquipmentRequirementPropertyType = (OpEquipmentRequirementPropertyType)theEObject;
				T result = caseOpEquipmentRequirementPropertyType(opEquipmentRequirementPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_EQUIPMENT_REQUIREMENT_TYPE: {
				OpEquipmentRequirementType opEquipmentRequirementType = (OpEquipmentRequirementType)theEObject;
				T result = caseOpEquipmentRequirementType(opEquipmentRequirementType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_PROPERTY_TYPE: {
				OpEquipmentSpecificationPropertyType opEquipmentSpecificationPropertyType = (OpEquipmentSpecificationPropertyType)theEObject;
				T result = caseOpEquipmentSpecificationPropertyType(opEquipmentSpecificationPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE: {
				OpEquipmentSpecificationType opEquipmentSpecificationType = (OpEquipmentSpecificationType)theEObject;
				T result = caseOpEquipmentSpecificationType(opEquipmentSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_CAPABILITY_INFORMATION_TYPE: {
				OperationsCapabilityInformationType operationsCapabilityInformationType = (OperationsCapabilityInformationType)theEObject;
				T result = caseOperationsCapabilityInformationType(operationsCapabilityInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE: {
				OperationsCapabilityType operationsCapabilityType = (OperationsCapabilityType)theEObject;
				T result = caseOperationsCapabilityType(operationsCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_DEFINITION_ID_TYPE: {
				OperationsDefinitionIDType operationsDefinitionIDType = (OperationsDefinitionIDType)theEObject;
				T result = caseOperationsDefinitionIDType(operationsDefinitionIDType);
				if (result == null) result = caseIdentifierType(operationsDefinitionIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE: {
				OperationsDefinitionInformationType operationsDefinitionInformationType = (OperationsDefinitionInformationType)theEObject;
				T result = caseOperationsDefinitionInformationType(operationsDefinitionInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE: {
				OperationsDefinitionType operationsDefinitionType = (OperationsDefinitionType)theEObject;
				T result = caseOperationsDefinitionType(operationsDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE: {
				OperationsMaterialBillItemType operationsMaterialBillItemType = (OperationsMaterialBillItemType)theEObject;
				T result = caseOperationsMaterialBillItemType(operationsMaterialBillItemType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_TYPE: {
				OperationsMaterialBillType operationsMaterialBillType = (OperationsMaterialBillType)theEObject;
				T result = caseOperationsMaterialBillType(operationsMaterialBillType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_PERFORMANCE_TYPE: {
				OperationsPerformanceType operationsPerformanceType = (OperationsPerformanceType)theEObject;
				T result = caseOperationsPerformanceType(operationsPerformanceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_REQUEST_ID_TYPE: {
				OperationsRequestIDType operationsRequestIDType = (OperationsRequestIDType)theEObject;
				T result = caseOperationsRequestIDType(operationsRequestIDType);
				if (result == null) result = caseIdentifierType(operationsRequestIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_REQUEST_TYPE: {
				OperationsRequestType operationsRequestType = (OperationsRequestType)theEObject;
				T result = caseOperationsRequestType(operationsRequestType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE: {
				OperationsResponseType operationsResponseType = (OperationsResponseType)theEObject;
				T result = caseOperationsResponseType(operationsResponseType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_SCHEDULE_ID_TYPE: {
				OperationsScheduleIDType operationsScheduleIDType = (OperationsScheduleIDType)theEObject;
				T result = caseOperationsScheduleIDType(operationsScheduleIDType);
				if (result == null) result = caseIdentifierType(operationsScheduleIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE: {
				OperationsScheduleType operationsScheduleType = (OperationsScheduleType)theEObject;
				T result = caseOperationsScheduleType(operationsScheduleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_SEGMENT_ID_TYPE: {
				OperationsSegmentIDType operationsSegmentIDType = (OperationsSegmentIDType)theEObject;
				T result = caseOperationsSegmentIDType(operationsSegmentIDType);
				if (result == null) result = caseIdentifierType(operationsSegmentIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE: {
				OperationsSegmentType operationsSegmentType = (OperationsSegmentType)theEObject;
				T result = caseOperationsSegmentType(operationsSegmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_TYPE1_TYPE: {
				OperationsType1Type operationsType1Type = (OperationsType1Type)theEObject;
				T result = caseOperationsType1Type(operationsType1Type);
				if (result == null) result = caseCodeType(operationsType1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OPERATIONS_TYPE_TYPE: {
				OperationsTypeType operationsTypeType = (OperationsTypeType)theEObject;
				T result = caseOperationsTypeType(operationsTypeType);
				if (result == null) result = caseOperationsType1Type(operationsTypeType);
				if (result == null) result = caseCodeType(operationsTypeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE: {
				OpMaterialActualPropertyType opMaterialActualPropertyType = (OpMaterialActualPropertyType)theEObject;
				T result = caseOpMaterialActualPropertyType(opMaterialActualPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE: {
				OpMaterialActualType opMaterialActualType = (OpMaterialActualType)theEObject;
				T result = caseOpMaterialActualType(opMaterialActualType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_PROPERTY_TYPE: {
				OpMaterialCapabilityPropertyType opMaterialCapabilityPropertyType = (OpMaterialCapabilityPropertyType)theEObject;
				T result = caseOpMaterialCapabilityPropertyType(opMaterialCapabilityPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE: {
				OpMaterialCapabilityType opMaterialCapabilityType = (OpMaterialCapabilityType)theEObject;
				T result = caseOpMaterialCapabilityType(opMaterialCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_MATERIAL_REQUIREMENT_PROPERTY_TYPE: {
				OpMaterialRequirementPropertyType opMaterialRequirementPropertyType = (OpMaterialRequirementPropertyType)theEObject;
				T result = caseOpMaterialRequirementPropertyType(opMaterialRequirementPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_MATERIAL_REQUIREMENT_TYPE: {
				OpMaterialRequirementType opMaterialRequirementType = (OpMaterialRequirementType)theEObject;
				T result = caseOpMaterialRequirementType(opMaterialRequirementType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_PROPERTY_TYPE: {
				OpMaterialSpecificationPropertyType opMaterialSpecificationPropertyType = (OpMaterialSpecificationPropertyType)theEObject;
				T result = caseOpMaterialSpecificationPropertyType(opMaterialSpecificationPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE: {
				OpMaterialSpecificationType opMaterialSpecificationType = (OpMaterialSpecificationType)theEObject;
				T result = caseOpMaterialSpecificationType(opMaterialSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_PROPERTY_TYPE: {
				OpPersonnelActualPropertyType opPersonnelActualPropertyType = (OpPersonnelActualPropertyType)theEObject;
				T result = caseOpPersonnelActualPropertyType(opPersonnelActualPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE: {
				OpPersonnelActualType opPersonnelActualType = (OpPersonnelActualType)theEObject;
				T result = caseOpPersonnelActualType(opPersonnelActualType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_PROPERTY_TYPE: {
				OpPersonnelCapabilityPropertyType opPersonnelCapabilityPropertyType = (OpPersonnelCapabilityPropertyType)theEObject;
				T result = caseOpPersonnelCapabilityPropertyType(opPersonnelCapabilityPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE: {
				OpPersonnelCapabilityType opPersonnelCapabilityType = (OpPersonnelCapabilityType)theEObject;
				T result = caseOpPersonnelCapabilityType(opPersonnelCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PERSONNEL_REQUIREMENT_PROPERTY_TYPE: {
				OpPersonnelRequirementPropertyType opPersonnelRequirementPropertyType = (OpPersonnelRequirementPropertyType)theEObject;
				T result = caseOpPersonnelRequirementPropertyType(opPersonnelRequirementPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PERSONNEL_REQUIREMENT_TYPE: {
				OpPersonnelRequirementType opPersonnelRequirementType = (OpPersonnelRequirementType)theEObject;
				T result = caseOpPersonnelRequirementType(opPersonnelRequirementType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_PROPERTY_TYPE: {
				OpPersonnelSpecificationPropertyType opPersonnelSpecificationPropertyType = (OpPersonnelSpecificationPropertyType)theEObject;
				T result = caseOpPersonnelSpecificationPropertyType(opPersonnelSpecificationPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE: {
				OpPersonnelSpecificationType opPersonnelSpecificationType = (OpPersonnelSpecificationType)theEObject;
				T result = caseOpPersonnelSpecificationType(opPersonnelSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_PROPERTY_TYPE: {
				OpPhysicalAssetActualPropertyType opPhysicalAssetActualPropertyType = (OpPhysicalAssetActualPropertyType)theEObject;
				T result = caseOpPhysicalAssetActualPropertyType(opPhysicalAssetActualPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE: {
				OpPhysicalAssetActualType opPhysicalAssetActualType = (OpPhysicalAssetActualType)theEObject;
				T result = caseOpPhysicalAssetActualType(opPhysicalAssetActualType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PHYSICAL_ASSET_CAPABILITY_PROPERTY_TYPE: {
				OpPhysicalAssetCapabilityPropertyType opPhysicalAssetCapabilityPropertyType = (OpPhysicalAssetCapabilityPropertyType)theEObject;
				T result = caseOpPhysicalAssetCapabilityPropertyType(opPhysicalAssetCapabilityPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PHYSICAL_ASSET_CAPABILITY_TYPE: {
				OpPhysicalAssetCapabilityType opPhysicalAssetCapabilityType = (OpPhysicalAssetCapabilityType)theEObject;
				T result = caseOpPhysicalAssetCapabilityType(opPhysicalAssetCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PHYSICAL_ASSET_REQUIREMENT_PROPERTY_TYPE: {
				OpPhysicalAssetRequirementPropertyType opPhysicalAssetRequirementPropertyType = (OpPhysicalAssetRequirementPropertyType)theEObject;
				T result = caseOpPhysicalAssetRequirementPropertyType(opPhysicalAssetRequirementPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PHYSICAL_ASSET_REQUIREMENT_TYPE: {
				OpPhysicalAssetRequirementType opPhysicalAssetRequirementType = (OpPhysicalAssetRequirementType)theEObject;
				T result = caseOpPhysicalAssetRequirementType(opPhysicalAssetRequirementType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_PROPERTY_TYPE: {
				OpPhysicalAssetSpecificationPropertyType opPhysicalAssetSpecificationPropertyType = (OpPhysicalAssetSpecificationPropertyType)theEObject;
				T result = caseOpPhysicalAssetSpecificationPropertyType(opPhysicalAssetSpecificationPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE: {
				OpPhysicalAssetSpecificationType opPhysicalAssetSpecificationType = (OpPhysicalAssetSpecificationType)theEObject;
				T result = caseOpPhysicalAssetSpecificationType(opPhysicalAssetSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE: {
				OpProcessSegmentCapabilityType opProcessSegmentCapabilityType = (OpProcessSegmentCapabilityType)theEObject;
				T result = caseOpProcessSegmentCapabilityType(opProcessSegmentCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_SEGMENT_DATA_TYPE: {
				OpSegmentDataType opSegmentDataType = (OpSegmentDataType)theEObject;
				T result = caseOpSegmentDataType(opSegmentDataType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE: {
				OpSegmentRequirementType opSegmentRequirementType = (OpSegmentRequirementType)theEObject;
				T result = caseOpSegmentRequirementType(opSegmentRequirementType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE: {
				OpSegmentResponseType opSegmentResponseType = (OpSegmentResponseType)theEObject;
				T result = caseOpSegmentResponseType(opSegmentResponseType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.OTHER_DEPENDENCY_TYPE: {
				OtherDependencyType otherDependencyType = (OtherDependencyType)theEObject;
				T result = caseOtherDependencyType(otherDependencyType);
				if (result == null) result = caseCodeType(otherDependencyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PARAMETER_ID_TYPE: {
				ParameterIDType parameterIDType = (ParameterIDType)theEObject;
				T result = caseParameterIDType(parameterIDType);
				if (result == null) result = caseIdentifierType(parameterIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PARAMETER_TYPE: {
				ParameterType parameterType = (ParameterType)theEObject;
				T result = caseParameterType(parameterType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PERSON_ID_TYPE: {
				PersonIDType personIDType = (PersonIDType)theEObject;
				T result = casePersonIDType(personIDType);
				if (result == null) result = caseIdentifierType(personIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PERSON_NAME_TYPE: {
				PersonNameType personNameType = (PersonNameType)theEObject;
				T result = casePersonNameType(personNameType);
				if (result == null) result = caseIdentifierType(personNameType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PERSONNEL_CLASS_ID_TYPE: {
				PersonnelClassIDType personnelClassIDType = (PersonnelClassIDType)theEObject;
				T result = casePersonnelClassIDType(personnelClassIDType);
				if (result == null) result = caseIdentifierType(personnelClassIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PERSONNEL_CLASS_PROPERTY_TYPE: {
				PersonnelClassPropertyType personnelClassPropertyType = (PersonnelClassPropertyType)theEObject;
				T result = casePersonnelClassPropertyType(personnelClassPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PERSONNEL_CLASS_TYPE: {
				PersonnelClassType personnelClassType = (PersonnelClassType)theEObject;
				T result = casePersonnelClassType(personnelClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PERSONNEL_INFORMATION_TYPE: {
				PersonnelInformationType personnelInformationType = (PersonnelInformationType)theEObject;
				T result = casePersonnelInformationType(personnelInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY_TYPE: {
				PersonnelSegmentSpecificationPropertyType personnelSegmentSpecificationPropertyType = (PersonnelSegmentSpecificationPropertyType)theEObject;
				T result = casePersonnelSegmentSpecificationPropertyType(personnelSegmentSpecificationPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE: {
				PersonnelSegmentSpecificationType personnelSegmentSpecificationType = (PersonnelSegmentSpecificationType)theEObject;
				T result = casePersonnelSegmentSpecificationType(personnelSegmentSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PERSONNEL_USE_TYPE: {
				PersonnelUseType personnelUseType = (PersonnelUseType)theEObject;
				T result = casePersonnelUseType(personnelUseType);
				if (result == null) result = caseCodeType(personnelUseType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PERSON_PROPERTY_TYPE: {
				PersonPropertyType personPropertyType = (PersonPropertyType)theEObject;
				T result = casePersonPropertyType(personPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PERSON_TYPE: {
				PersonType personType = (PersonType)theEObject;
				T result = casePersonType(personType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_ACTUAL_ID_TYPE: {
				PhysicalAssetActualIDType physicalAssetActualIDType = (PhysicalAssetActualIDType)theEObject;
				T result = casePhysicalAssetActualIDType(physicalAssetActualIDType);
				if (result == null) result = caseIdentifierType(physicalAssetActualIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID_TYPE: {
				PhysicalAssetCapabilityTestSpecificationIDType physicalAssetCapabilityTestSpecificationIDType = (PhysicalAssetCapabilityTestSpecificationIDType)theEObject;
				T result = casePhysicalAssetCapabilityTestSpecificationIDType(physicalAssetCapabilityTestSpecificationIDType);
				if (result == null) result = caseIdentifierType(physicalAssetCapabilityTestSpecificationIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE: {
				PhysicalAssetCapabilityTestSpecificationType physicalAssetCapabilityTestSpecificationType = (PhysicalAssetCapabilityTestSpecificationType)theEObject;
				T result = casePhysicalAssetCapabilityTestSpecificationType(physicalAssetCapabilityTestSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_ID_TYPE: {
				PhysicalAssetClassIDType physicalAssetClassIDType = (PhysicalAssetClassIDType)theEObject;
				T result = casePhysicalAssetClassIDType(physicalAssetClassIDType);
				if (result == null) result = caseIdentifierType(physicalAssetClassIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE: {
				PhysicalAssetClassPropertyType physicalAssetClassPropertyType = (PhysicalAssetClassPropertyType)theEObject;
				T result = casePhysicalAssetClassPropertyType(physicalAssetClassPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE: {
				PhysicalAssetClassType physicalAssetClassType = (PhysicalAssetClassType)theEObject;
				T result = casePhysicalAssetClassType(physicalAssetClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_ID_TYPE: {
				PhysicalAssetIDType physicalAssetIDType = (PhysicalAssetIDType)theEObject;
				T result = casePhysicalAssetIDType(physicalAssetIDType);
				if (result == null) result = caseIdentifierType(physicalAssetIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_INFORMATION_TYPE: {
				PhysicalAssetInformationType physicalAssetInformationType = (PhysicalAssetInformationType)theEObject;
				T result = casePhysicalAssetInformationType(physicalAssetInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_PROPERTY_TYPE: {
				PhysicalAssetPropertyType physicalAssetPropertyType = (PhysicalAssetPropertyType)theEObject;
				T result = casePhysicalAssetPropertyType(physicalAssetPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY_TYPE: {
				PhysicalAssetSegmentSpecificationPropertyType physicalAssetSegmentSpecificationPropertyType = (PhysicalAssetSegmentSpecificationPropertyType)theEObject;
				T result = casePhysicalAssetSegmentSpecificationPropertyType(physicalAssetSegmentSpecificationPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE: {
				PhysicalAssetSegmentSpecificationType physicalAssetSegmentSpecificationType = (PhysicalAssetSegmentSpecificationType)theEObject;
				T result = casePhysicalAssetSegmentSpecificationType(physicalAssetSegmentSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_TYPE: {
				PhysicalAssetType physicalAssetType = (PhysicalAssetType)theEObject;
				T result = casePhysicalAssetType(physicalAssetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PHYSICAL_ASSET_USE_TYPE: {
				PhysicalAssetUseType physicalAssetUseType = (PhysicalAssetUseType)theEObject;
				T result = casePhysicalAssetUseType(physicalAssetUseType);
				if (result == null) result = caseCodeType(physicalAssetUseType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PLANNED_FINISH_TIME_TYPE: {
				PlannedFinishTimeType plannedFinishTimeType = (PlannedFinishTimeType)theEObject;
				T result = casePlannedFinishTimeType(plannedFinishTimeType);
				if (result == null) result = caseDateTimeType(plannedFinishTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PLANNED_START_TIME_TYPE: {
				PlannedStartTimeType plannedStartTimeType = (PlannedStartTimeType)theEObject;
				T result = casePlannedStartTimeType(plannedStartTimeType);
				if (result == null) result = caseDateTimeType(plannedStartTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PRIORITY_TYPE: {
				PriorityType priorityType = (PriorityType)theEObject;
				T result = casePriorityType(priorityType);
				if (result == null) result = caseNumericType(priorityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROBLEM_TYPE: {
				ProblemType problemType = (ProblemType)theEObject;
				T result = caseProblemType(problemType);
				if (result == null) result = caseCodeType(problemType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: {
				ProcessEquipmentCapabilityTestSpecType processEquipmentCapabilityTestSpecType = (ProcessEquipmentCapabilityTestSpecType)theEObject;
				T result = caseProcessEquipmentCapabilityTestSpecType(processEquipmentCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_EQUIPMENT_CLASS_TYPE: {
				ProcessEquipmentClassType processEquipmentClassType = (ProcessEquipmentClassType)theEObject;
				T result = caseProcessEquipmentClassType(processEquipmentClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_EQUIPMENT_INFORMATION_TYPE: {
				ProcessEquipmentInformationType processEquipmentInformationType = (ProcessEquipmentInformationType)theEObject;
				T result = caseProcessEquipmentInformationType(processEquipmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_EQUIPMENT_TYPE: {
				ProcessEquipmentType processEquipmentType = (ProcessEquipmentType)theEObject;
				T result = caseProcessEquipmentType(processEquipmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_MATERIAL_CLASS_TYPE: {
				ProcessMaterialClassType processMaterialClassType = (ProcessMaterialClassType)theEObject;
				T result = caseProcessMaterialClassType(processMaterialClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_MATERIAL_DEFINITION_TYPE: {
				ProcessMaterialDefinitionType processMaterialDefinitionType = (ProcessMaterialDefinitionType)theEObject;
				T result = caseProcessMaterialDefinitionType(processMaterialDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_MATERIAL_INFORMATION_TYPE: {
				ProcessMaterialInformationType processMaterialInformationType = (ProcessMaterialInformationType)theEObject;
				T result = caseProcessMaterialInformationType(processMaterialInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_MATERIAL_LOT_TYPE: {
				ProcessMaterialLotType processMaterialLotType = (ProcessMaterialLotType)theEObject;
				T result = caseProcessMaterialLotType(processMaterialLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_MATERIAL_SUB_LOT_TYPE: {
				ProcessMaterialSubLotType processMaterialSubLotType = (ProcessMaterialSubLotType)theEObject;
				T result = caseProcessMaterialSubLotType(processMaterialSubLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_MATERIAL_TEST_SPEC_TYPE: {
				ProcessMaterialTestSpecType processMaterialTestSpecType = (ProcessMaterialTestSpecType)theEObject;
				T result = caseProcessMaterialTestSpecType(processMaterialTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_OPERATIONS_CAPABILITY_INFORMATION_TYPE: {
				ProcessOperationsCapabilityInformationType processOperationsCapabilityInformationType = (ProcessOperationsCapabilityInformationType)theEObject;
				T result = caseProcessOperationsCapabilityInformationType(processOperationsCapabilityInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_OPERATIONS_CAPABILITY_TYPE: {
				ProcessOperationsCapabilityType processOperationsCapabilityType = (ProcessOperationsCapabilityType)theEObject;
				T result = caseProcessOperationsCapabilityType(processOperationsCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_OPERATIONS_DEFINITION_INFORMATION_TYPE: {
				ProcessOperationsDefinitionInformationType processOperationsDefinitionInformationType = (ProcessOperationsDefinitionInformationType)theEObject;
				T result = caseProcessOperationsDefinitionInformationType(processOperationsDefinitionInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_OPERATIONS_DEFINITION_TYPE: {
				ProcessOperationsDefinitionType processOperationsDefinitionType = (ProcessOperationsDefinitionType)theEObject;
				T result = caseProcessOperationsDefinitionType(processOperationsDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_OPERATIONS_PERFORMANCE_TYPE: {
				ProcessOperationsPerformanceType processOperationsPerformanceType = (ProcessOperationsPerformanceType)theEObject;
				T result = caseProcessOperationsPerformanceType(processOperationsPerformanceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_OPERATIONS_SCHEDULE_TYPE: {
				ProcessOperationsScheduleType processOperationsScheduleType = (ProcessOperationsScheduleType)theEObject;
				T result = caseProcessOperationsScheduleType(processOperationsScheduleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_PERSONNEL_CLASS_TYPE: {
				ProcessPersonnelClassType processPersonnelClassType = (ProcessPersonnelClassType)theEObject;
				T result = caseProcessPersonnelClassType(processPersonnelClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_PERSONNEL_INFORMATION_TYPE: {
				ProcessPersonnelInformationType processPersonnelInformationType = (ProcessPersonnelInformationType)theEObject;
				T result = caseProcessPersonnelInformationType(processPersonnelInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_PERSON_TYPE: {
				ProcessPersonType processPersonType = (ProcessPersonType)theEObject;
				T result = caseProcessPersonType(processPersonType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: {
				ProcessPhysicalAssetCapabilityTestSpecType processPhysicalAssetCapabilityTestSpecType = (ProcessPhysicalAssetCapabilityTestSpecType)theEObject;
				T result = caseProcessPhysicalAssetCapabilityTestSpecType(processPhysicalAssetCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_PHYSICAL_ASSET_CLASS_TYPE: {
				ProcessPhysicalAssetClassType processPhysicalAssetClassType = (ProcessPhysicalAssetClassType)theEObject;
				T result = caseProcessPhysicalAssetClassType(processPhysicalAssetClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_PHYSICAL_ASSET_INFORMATION_TYPE: {
				ProcessPhysicalAssetInformationType processPhysicalAssetInformationType = (ProcessPhysicalAssetInformationType)theEObject;
				T result = caseProcessPhysicalAssetInformationType(processPhysicalAssetInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_PHYSICAL_ASSET_TYPE: {
				ProcessPhysicalAssetType processPhysicalAssetType = (ProcessPhysicalAssetType)theEObject;
				T result = caseProcessPhysicalAssetType(processPhysicalAssetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_PROCESS_SEGMENT_INFORMATION_TYPE: {
				ProcessProcessSegmentInformationType processProcessSegmentInformationType = (ProcessProcessSegmentInformationType)theEObject;
				T result = caseProcessProcessSegmentInformationType(processProcessSegmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_PROCESS_SEGMENT_TYPE: {
				ProcessProcessSegmentType processProcessSegmentType = (ProcessProcessSegmentType)theEObject;
				T result = caseProcessProcessSegmentType(processProcessSegmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_QUALIFICATION_TEST_SPECIFICATION_TYPE: {
				ProcessQualificationTestSpecificationType processQualificationTestSpecificationType = (ProcessQualificationTestSpecificationType)theEObject;
				T result = caseProcessQualificationTestSpecificationType(processQualificationTestSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_SEGMENT_ID_TYPE: {
				ProcessSegmentIDType processSegmentIDType = (ProcessSegmentIDType)theEObject;
				T result = caseProcessSegmentIDType(processSegmentIDType);
				if (result == null) result = caseIdentifierType(processSegmentIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_SEGMENT_INFORMATION_TYPE: {
				ProcessSegmentInformationType processSegmentInformationType = (ProcessSegmentInformationType)theEObject;
				T result = caseProcessSegmentInformationType(processSegmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROCESS_SEGMENT_TYPE: {
				ProcessSegmentType processSegmentType = (ProcessSegmentType)theEObject;
				T result = caseProcessSegmentType(processSegmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PRODUCTION_REQUEST_ID_TYPE: {
				ProductionRequestIDType productionRequestIDType = (ProductionRequestIDType)theEObject;
				T result = caseProductionRequestIDType(productionRequestIDType);
				if (result == null) result = caseIdentifierType(productionRequestIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PRODUCTION_SCHEDULE_ID_TYPE: {
				ProductionScheduleIDType productionScheduleIDType = (ProductionScheduleIDType)theEObject;
				T result = caseProductionScheduleIDType(productionScheduleIDType);
				if (result == null) result = caseIdentifierType(productionScheduleIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PRODUCT_PRODUCTION_RULE_ID_TYPE: {
				ProductProductionRuleIDType productProductionRuleIDType = (ProductProductionRuleIDType)theEObject;
				T result = caseProductProductionRuleIDType(productProductionRuleIDType);
				if (result == null) result = caseIdentifierType(productProductionRuleIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PRODUCT_PRODUCTION_RULE_TYPE: {
				ProductProductionRuleType productProductionRuleType = (ProductProductionRuleType)theEObject;
				T result = caseProductProductionRuleType(productProductionRuleType);
				if (result == null) result = caseIdentifierType(productProductionRuleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PRODUCT_SEGMENT_ID_TYPE: {
				ProductSegmentIDType productSegmentIDType = (ProductSegmentIDType)theEObject;
				T result = caseProductSegmentIDType(productSegmentIDType);
				if (result == null) result = caseIdentifierType(productSegmentIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PROPERTY_ID_TYPE: {
				PropertyIDType propertyIDType = (PropertyIDType)theEObject;
				T result = casePropertyIDType(propertyIDType);
				if (result == null) result = caseIdentifierType(propertyIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.PUBLISHED_DATE_TYPE: {
				PublishedDateType publishedDateType = (PublishedDateType)theEObject;
				T result = casePublishedDateType(publishedDateType);
				if (result == null) result = caseDateTimeType(publishedDateType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_ID_TYPE: {
				QualificationTestSpecificationIDType qualificationTestSpecificationIDType = (QualificationTestSpecificationIDType)theEObject;
				T result = caseQualificationTestSpecificationIDType(qualificationTestSpecificationIDType);
				if (result == null) result = caseIdentifierType(qualificationTestSpecificationIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE: {
				QualificationTestSpecificationType qualificationTestSpecificationType = (QualificationTestSpecificationType)theEObject;
				T result = caseQualificationTestSpecificationType(qualificationTestSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.QUANTITY_STRING_TYPE: {
				QuantityStringType quantityStringType = (QuantityStringType)theEObject;
				T result = caseQuantityStringType(quantityStringType);
				if (result == null) result = caseAnyGenericValueType(quantityStringType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.QUANTITY_TYPE: {
				QuantityType quantityType = (QuantityType)theEObject;
				T result = caseQuantityType(quantityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.QUANTITY_VALUE_TYPE: {
				QuantityValueType quantityValueType = (QuantityValueType)theEObject;
				T result = caseQuantityValueType(quantityValueType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.REASON_TYPE: {
				ReasonType reasonType = (ReasonType)theEObject;
				T result = caseReasonType(reasonType);
				if (result == null) result = caseCodeType(reasonType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RELATIONSHIP_FORM1_TYPE: {
				RelationshipForm1Type relationshipForm1Type = (RelationshipForm1Type)theEObject;
				T result = caseRelationshipForm1Type(relationshipForm1Type);
				if (result == null) result = caseCodeType(relationshipForm1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RELATIONSHIP_FORM_TYPE: {
				RelationshipFormType relationshipFormType = (RelationshipFormType)theEObject;
				T result = caseRelationshipFormType(relationshipFormType);
				if (result == null) result = caseRelationshipForm1Type(relationshipFormType);
				if (result == null) result = caseCodeType(relationshipFormType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RELATIONSHIP_TYPE1_TYPE: {
				RelationshipType1Type relationshipType1Type = (RelationshipType1Type)theEObject;
				T result = caseRelationshipType1Type(relationshipType1Type);
				if (result == null) result = caseCodeType(relationshipType1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RELATIONSHIP_TYPE_TYPE: {
				RelationshipTypeType relationshipTypeType = (RelationshipTypeType)theEObject;
				T result = caseRelationshipTypeType(relationshipTypeType);
				if (result == null) result = caseRelationshipType1Type(relationshipTypeType);
				if (result == null) result = caseCodeType(relationshipTypeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.REQUESTED_COMPLETION_DATE_TYPE: {
				RequestedCompletionDateType requestedCompletionDateType = (RequestedCompletionDateType)theEObject;
				T result = caseRequestedCompletionDateType(requestedCompletionDateType);
				if (result == null) result = caseDateTimeType(requestedCompletionDateType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.REQUESTED_PRIORITY_TYPE: {
				RequestedPriorityType requestedPriorityType = (RequestedPriorityType)theEObject;
				T result = caseRequestedPriorityType(requestedPriorityType);
				if (result == null) result = caseNumericType(requestedPriorityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.REQUEST_STATE1_TYPE: {
				RequestState1Type requestState1Type = (RequestState1Type)theEObject;
				T result = caseRequestState1Type(requestState1Type);
				if (result == null) result = caseCodeType(requestState1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.REQUEST_STATE_TYPE: {
				RequestStateType requestStateType = (RequestStateType)theEObject;
				T result = caseRequestStateType(requestStateType);
				if (result == null) result = caseRequestState1Type(requestStateType);
				if (result == null) result = caseCodeType(requestStateType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE1_TYPE: {
				RequiredByRequestedSegmentResponse1Type requiredByRequestedSegmentResponse1Type = (RequiredByRequestedSegmentResponse1Type)theEObject;
				T result = caseRequiredByRequestedSegmentResponse1Type(requiredByRequestedSegmentResponse1Type);
				if (result == null) result = caseCodeType(requiredByRequestedSegmentResponse1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE_TYPE: {
				RequiredByRequestedSegmentResponseType requiredByRequestedSegmentResponseType = (RequiredByRequestedSegmentResponseType)theEObject;
				T result = caseRequiredByRequestedSegmentResponseType(requiredByRequestedSegmentResponseType);
				if (result == null) result = caseRequiredByRequestedSegmentResponse1Type(requiredByRequestedSegmentResponseType);
				if (result == null) result = caseCodeType(requiredByRequestedSegmentResponseType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESOURCE_ID_TYPE: {
				ResourceIDType resourceIDType = (ResourceIDType)theEObject;
				T result = caseResourceIDType(resourceIDType);
				if (result == null) result = caseIdentifierType(resourceIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESOURCE_NETWORK_CONNECTION_ID_TYPE: {
				ResourceNetworkConnectionIDType resourceNetworkConnectionIDType = (ResourceNetworkConnectionIDType)theEObject;
				T result = caseResourceNetworkConnectionIDType(resourceNetworkConnectionIDType);
				if (result == null) result = caseIdentifierType(resourceNetworkConnectionIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESOURCE_REFERENCE_TYPE1_TYPE: {
				ResourceReferenceType1Type resourceReferenceType1Type = (ResourceReferenceType1Type)theEObject;
				T result = caseResourceReferenceType1Type(resourceReferenceType1Type);
				if (result == null) result = caseCodeType(resourceReferenceType1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESOURCE_REFERENCE_TYPE_TYPE: {
				ResourceReferenceTypeType resourceReferenceTypeType = (ResourceReferenceTypeType)theEObject;
				T result = caseResourceReferenceTypeType(resourceReferenceTypeType);
				if (result == null) result = caseResourceReferenceType1Type(resourceReferenceTypeType);
				if (result == null) result = caseCodeType(resourceReferenceTypeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESOURCES_TYPE: {
				ResourcesType resourcesType = (ResourcesType)theEObject;
				T result = caseResourcesType(resourcesType);
				if (result == null) result = caseCodeType(resourcesType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: {
				RespondEquipmentCapabilityTestSpecType respondEquipmentCapabilityTestSpecType = (RespondEquipmentCapabilityTestSpecType)theEObject;
				T result = caseRespondEquipmentCapabilityTestSpecType(respondEquipmentCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_EQUIPMENT_CLASS_TYPE: {
				RespondEquipmentClassType respondEquipmentClassType = (RespondEquipmentClassType)theEObject;
				T result = caseRespondEquipmentClassType(respondEquipmentClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_EQUIPMENT_INFORMATION_TYPE: {
				RespondEquipmentInformationType respondEquipmentInformationType = (RespondEquipmentInformationType)theEObject;
				T result = caseRespondEquipmentInformationType(respondEquipmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_EQUIPMENT_TYPE: {
				RespondEquipmentType respondEquipmentType = (RespondEquipmentType)theEObject;
				T result = caseRespondEquipmentType(respondEquipmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_MATERIAL_CLASS_TYPE: {
				RespondMaterialClassType respondMaterialClassType = (RespondMaterialClassType)theEObject;
				T result = caseRespondMaterialClassType(respondMaterialClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_MATERIAL_DEFINITION_TYPE: {
				RespondMaterialDefinitionType respondMaterialDefinitionType = (RespondMaterialDefinitionType)theEObject;
				T result = caseRespondMaterialDefinitionType(respondMaterialDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_MATERIAL_INFORMATION_TYPE: {
				RespondMaterialInformationType respondMaterialInformationType = (RespondMaterialInformationType)theEObject;
				T result = caseRespondMaterialInformationType(respondMaterialInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_MATERIAL_LOT_TYPE: {
				RespondMaterialLotType respondMaterialLotType = (RespondMaterialLotType)theEObject;
				T result = caseRespondMaterialLotType(respondMaterialLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_MATERIAL_SUB_LOT_TYPE: {
				RespondMaterialSubLotType respondMaterialSubLotType = (RespondMaterialSubLotType)theEObject;
				T result = caseRespondMaterialSubLotType(respondMaterialSubLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_MATERIAL_TEST_SPEC_TYPE: {
				RespondMaterialTestSpecType respondMaterialTestSpecType = (RespondMaterialTestSpecType)theEObject;
				T result = caseRespondMaterialTestSpecType(respondMaterialTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_OPERATIONS_CAPABILITY_INFORMATION_TYPE: {
				RespondOperationsCapabilityInformationType respondOperationsCapabilityInformationType = (RespondOperationsCapabilityInformationType)theEObject;
				T result = caseRespondOperationsCapabilityInformationType(respondOperationsCapabilityInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_OPERATIONS_CAPABILITY_TYPE: {
				RespondOperationsCapabilityType respondOperationsCapabilityType = (RespondOperationsCapabilityType)theEObject;
				T result = caseRespondOperationsCapabilityType(respondOperationsCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_OPERATIONS_DEFINITION_INFORMATION_TYPE: {
				RespondOperationsDefinitionInformationType respondOperationsDefinitionInformationType = (RespondOperationsDefinitionInformationType)theEObject;
				T result = caseRespondOperationsDefinitionInformationType(respondOperationsDefinitionInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_OPERATIONS_DEFINITION_TYPE: {
				RespondOperationsDefinitionType respondOperationsDefinitionType = (RespondOperationsDefinitionType)theEObject;
				T result = caseRespondOperationsDefinitionType(respondOperationsDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_OPERATIONS_PERFORMANCE_TYPE: {
				RespondOperationsPerformanceType respondOperationsPerformanceType = (RespondOperationsPerformanceType)theEObject;
				T result = caseRespondOperationsPerformanceType(respondOperationsPerformanceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE: {
				RespondOperationsScheduleType respondOperationsScheduleType = (RespondOperationsScheduleType)theEObject;
				T result = caseRespondOperationsScheduleType(respondOperationsScheduleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_PERSONNEL_CLASS_TYPE: {
				RespondPersonnelClassType respondPersonnelClassType = (RespondPersonnelClassType)theEObject;
				T result = caseRespondPersonnelClassType(respondPersonnelClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_PERSONNEL_INFORMATION_TYPE: {
				RespondPersonnelInformationType respondPersonnelInformationType = (RespondPersonnelInformationType)theEObject;
				T result = caseRespondPersonnelInformationType(respondPersonnelInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_PERSON_TYPE: {
				RespondPersonType respondPersonType = (RespondPersonType)theEObject;
				T result = caseRespondPersonType(respondPersonType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: {
				RespondPhysicalAssetCapabilityTestSpecType respondPhysicalAssetCapabilityTestSpecType = (RespondPhysicalAssetCapabilityTestSpecType)theEObject;
				T result = caseRespondPhysicalAssetCapabilityTestSpecType(respondPhysicalAssetCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_PHYSICAL_ASSET_CLASS_TYPE: {
				RespondPhysicalAssetClassType respondPhysicalAssetClassType = (RespondPhysicalAssetClassType)theEObject;
				T result = caseRespondPhysicalAssetClassType(respondPhysicalAssetClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_PHYSICAL_ASSET_INFORMATION_TYPE: {
				RespondPhysicalAssetInformationType respondPhysicalAssetInformationType = (RespondPhysicalAssetInformationType)theEObject;
				T result = caseRespondPhysicalAssetInformationType(respondPhysicalAssetInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_PHYSICAL_ASSET_TYPE: {
				RespondPhysicalAssetType respondPhysicalAssetType = (RespondPhysicalAssetType)theEObject;
				T result = caseRespondPhysicalAssetType(respondPhysicalAssetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_PROCESS_SEGMENT_INFORMATION_TYPE: {
				RespondProcessSegmentInformationType respondProcessSegmentInformationType = (RespondProcessSegmentInformationType)theEObject;
				T result = caseRespondProcessSegmentInformationType(respondProcessSegmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_PROCESS_SEGMENT_TYPE: {
				RespondProcessSegmentType respondProcessSegmentType = (RespondProcessSegmentType)theEObject;
				T result = caseRespondProcessSegmentType(respondProcessSegmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPOND_QUALIFICATION_TEST_SPECIFICATION_TYPE: {
				RespondQualificationTestSpecificationType respondQualificationTestSpecificationType = (RespondQualificationTestSpecificationType)theEObject;
				T result = caseRespondQualificationTestSpecificationType(respondQualificationTestSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPONSE_STATE1_TYPE: {
				ResponseState1Type responseState1Type = (ResponseState1Type)theEObject;
				T result = caseResponseState1Type(responseState1Type);
				if (result == null) result = caseCodeType(responseState1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESPONSE_STATE_TYPE: {
				ResponseStateType responseStateType = (ResponseStateType)theEObject;
				T result = caseResponseStateType(responseStateType);
				if (result == null) result = caseResponseState1Type(responseStateType);
				if (result == null) result = caseCodeType(responseStateType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.RESULT_TYPE: {
				ResultType resultType = (ResultType)theEObject;
				T result = caseResultType(resultType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE: {
				SegmentDependencyType segmentDependencyType = (SegmentDependencyType)theEObject;
				T result = caseSegmentDependencyType(segmentDependencyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: {
				ShowEquipmentCapabilityTestSpecType showEquipmentCapabilityTestSpecType = (ShowEquipmentCapabilityTestSpecType)theEObject;
				T result = caseShowEquipmentCapabilityTestSpecType(showEquipmentCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_EQUIPMENT_CLASS_TYPE: {
				ShowEquipmentClassType showEquipmentClassType = (ShowEquipmentClassType)theEObject;
				T result = caseShowEquipmentClassType(showEquipmentClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_EQUIPMENT_INFORMATION_TYPE: {
				ShowEquipmentInformationType showEquipmentInformationType = (ShowEquipmentInformationType)theEObject;
				T result = caseShowEquipmentInformationType(showEquipmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_EQUIPMENT_TYPE: {
				ShowEquipmentType showEquipmentType = (ShowEquipmentType)theEObject;
				T result = caseShowEquipmentType(showEquipmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_MATERIAL_CLASS_TYPE: {
				ShowMaterialClassType showMaterialClassType = (ShowMaterialClassType)theEObject;
				T result = caseShowMaterialClassType(showMaterialClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_MATERIAL_DEFINITION_TYPE: {
				ShowMaterialDefinitionType showMaterialDefinitionType = (ShowMaterialDefinitionType)theEObject;
				T result = caseShowMaterialDefinitionType(showMaterialDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_MATERIAL_INFORMATION_TYPE: {
				ShowMaterialInformationType showMaterialInformationType = (ShowMaterialInformationType)theEObject;
				T result = caseShowMaterialInformationType(showMaterialInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_MATERIAL_LOT_TYPE: {
				ShowMaterialLotType showMaterialLotType = (ShowMaterialLotType)theEObject;
				T result = caseShowMaterialLotType(showMaterialLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_MATERIAL_SUB_LOT_TYPE: {
				ShowMaterialSubLotType showMaterialSubLotType = (ShowMaterialSubLotType)theEObject;
				T result = caseShowMaterialSubLotType(showMaterialSubLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_MATERIAL_TEST_SPEC_TYPE: {
				ShowMaterialTestSpecType showMaterialTestSpecType = (ShowMaterialTestSpecType)theEObject;
				T result = caseShowMaterialTestSpecType(showMaterialTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_OPERATIONS_CAPABILITY_INFORMATION_TYPE: {
				ShowOperationsCapabilityInformationType showOperationsCapabilityInformationType = (ShowOperationsCapabilityInformationType)theEObject;
				T result = caseShowOperationsCapabilityInformationType(showOperationsCapabilityInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_OPERATIONS_CAPABILITY_TYPE: {
				ShowOperationsCapabilityType showOperationsCapabilityType = (ShowOperationsCapabilityType)theEObject;
				T result = caseShowOperationsCapabilityType(showOperationsCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_OPERATIONS_DEFINITION_INFORMATION_TYPE: {
				ShowOperationsDefinitionInformationType showOperationsDefinitionInformationType = (ShowOperationsDefinitionInformationType)theEObject;
				T result = caseShowOperationsDefinitionInformationType(showOperationsDefinitionInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_OPERATIONS_DEFINITION_TYPE: {
				ShowOperationsDefinitionType showOperationsDefinitionType = (ShowOperationsDefinitionType)theEObject;
				T result = caseShowOperationsDefinitionType(showOperationsDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_OPERATIONS_PERFORMANCE_TYPE: {
				ShowOperationsPerformanceType showOperationsPerformanceType = (ShowOperationsPerformanceType)theEObject;
				T result = caseShowOperationsPerformanceType(showOperationsPerformanceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_OPERATIONS_SCHEDULE_TYPE: {
				ShowOperationsScheduleType showOperationsScheduleType = (ShowOperationsScheduleType)theEObject;
				T result = caseShowOperationsScheduleType(showOperationsScheduleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_PERSONNEL_CLASS_TYPE: {
				ShowPersonnelClassType showPersonnelClassType = (ShowPersonnelClassType)theEObject;
				T result = caseShowPersonnelClassType(showPersonnelClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_PERSONNEL_INFORMATION_TYPE: {
				ShowPersonnelInformationType showPersonnelInformationType = (ShowPersonnelInformationType)theEObject;
				T result = caseShowPersonnelInformationType(showPersonnelInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_PERSON_TYPE: {
				ShowPersonType showPersonType = (ShowPersonType)theEObject;
				T result = caseShowPersonType(showPersonType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: {
				ShowPhysicalAssetCapabilityTestSpecType showPhysicalAssetCapabilityTestSpecType = (ShowPhysicalAssetCapabilityTestSpecType)theEObject;
				T result = caseShowPhysicalAssetCapabilityTestSpecType(showPhysicalAssetCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_PHYSICAL_ASSET_CLASS_TYPE: {
				ShowPhysicalAssetClassType showPhysicalAssetClassType = (ShowPhysicalAssetClassType)theEObject;
				T result = caseShowPhysicalAssetClassType(showPhysicalAssetClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_PHYSICAL_ASSET_INFORMATION_TYPE: {
				ShowPhysicalAssetInformationType showPhysicalAssetInformationType = (ShowPhysicalAssetInformationType)theEObject;
				T result = caseShowPhysicalAssetInformationType(showPhysicalAssetInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_PHYSICAL_ASSET_TYPE: {
				ShowPhysicalAssetType showPhysicalAssetType = (ShowPhysicalAssetType)theEObject;
				T result = caseShowPhysicalAssetType(showPhysicalAssetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_PROCESS_SEGMENT_INFORMATION_TYPE: {
				ShowProcessSegmentInformationType showProcessSegmentInformationType = (ShowProcessSegmentInformationType)theEObject;
				T result = caseShowProcessSegmentInformationType(showProcessSegmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_PROCESS_SEGMENT_TYPE: {
				ShowProcessSegmentType showProcessSegmentType = (ShowProcessSegmentType)theEObject;
				T result = caseShowProcessSegmentType(showProcessSegmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SHOW_QUALIFICATION_TEST_SPECIFICATION_TYPE: {
				ShowQualificationTestSpecificationType showQualificationTestSpecificationType = (ShowQualificationTestSpecificationType)theEObject;
				T result = caseShowQualificationTestSpecificationType(showQualificationTestSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.START_TIME_TYPE: {
				StartTimeType startTimeType = (StartTimeType)theEObject;
				T result = caseStartTimeType(startTimeType);
				if (result == null) result = caseDateTimeType(startTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.STATUS_TIME_TYPE: {
				StatusTimeType statusTimeType = (StatusTimeType)theEObject;
				T result = caseStatusTimeType(statusTimeType);
				if (result == null) result = caseDateTimeType(statusTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.STATUS_TYPE: {
				StatusType statusType = (StatusType)theEObject;
				T result = caseStatusType(statusType);
				if (result == null) result = caseCodeType(statusType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.STORAGE_HIERARCHY_SCOPE_TYPE: {
				StorageHierarchyScopeType storageHierarchyScopeType = (StorageHierarchyScopeType)theEObject;
				T result = caseStorageHierarchyScopeType(storageHierarchyScopeType);
				if (result == null) result = caseIdentifierType(storageHierarchyScopeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.STORAGE_LOCATION_TYPE: {
				StorageLocationType storageLocationType = (StorageLocationType)theEObject;
				T result = caseStorageLocationType(storageLocationType);
				if (result == null) result = caseIdentifierType(storageLocationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: {
				SyncEquipmentCapabilityTestSpecType syncEquipmentCapabilityTestSpecType = (SyncEquipmentCapabilityTestSpecType)theEObject;
				T result = caseSyncEquipmentCapabilityTestSpecType(syncEquipmentCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_EQUIPMENT_CLASS_TYPE: {
				SyncEquipmentClassType syncEquipmentClassType = (SyncEquipmentClassType)theEObject;
				T result = caseSyncEquipmentClassType(syncEquipmentClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_EQUIPMENT_INFORMATION_TYPE: {
				SyncEquipmentInformationType syncEquipmentInformationType = (SyncEquipmentInformationType)theEObject;
				T result = caseSyncEquipmentInformationType(syncEquipmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_EQUIPMENT_TYPE: {
				SyncEquipmentType syncEquipmentType = (SyncEquipmentType)theEObject;
				T result = caseSyncEquipmentType(syncEquipmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_MATERIAL_CLASS_TYPE: {
				SyncMaterialClassType syncMaterialClassType = (SyncMaterialClassType)theEObject;
				T result = caseSyncMaterialClassType(syncMaterialClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_MATERIAL_DEFINITION_TYPE: {
				SyncMaterialDefinitionType syncMaterialDefinitionType = (SyncMaterialDefinitionType)theEObject;
				T result = caseSyncMaterialDefinitionType(syncMaterialDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_MATERIAL_INFORMATION_TYPE: {
				SyncMaterialInformationType syncMaterialInformationType = (SyncMaterialInformationType)theEObject;
				T result = caseSyncMaterialInformationType(syncMaterialInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_MATERIAL_LOT_TYPE: {
				SyncMaterialLotType syncMaterialLotType = (SyncMaterialLotType)theEObject;
				T result = caseSyncMaterialLotType(syncMaterialLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_MATERIAL_SUB_LOT_TYPE: {
				SyncMaterialSubLotType syncMaterialSubLotType = (SyncMaterialSubLotType)theEObject;
				T result = caseSyncMaterialSubLotType(syncMaterialSubLotType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_MATERIAL_TEST_SPEC_TYPE: {
				SyncMaterialTestSpecType syncMaterialTestSpecType = (SyncMaterialTestSpecType)theEObject;
				T result = caseSyncMaterialTestSpecType(syncMaterialTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_OPERATIONS_CAPABILITY_INFORMATION_TYPE: {
				SyncOperationsCapabilityInformationType syncOperationsCapabilityInformationType = (SyncOperationsCapabilityInformationType)theEObject;
				T result = caseSyncOperationsCapabilityInformationType(syncOperationsCapabilityInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_OPERATIONS_CAPABILITY_TYPE: {
				SyncOperationsCapabilityType syncOperationsCapabilityType = (SyncOperationsCapabilityType)theEObject;
				T result = caseSyncOperationsCapabilityType(syncOperationsCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_OPERATIONS_DEFINITION_INFORMATION_TYPE: {
				SyncOperationsDefinitionInformationType syncOperationsDefinitionInformationType = (SyncOperationsDefinitionInformationType)theEObject;
				T result = caseSyncOperationsDefinitionInformationType(syncOperationsDefinitionInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_OPERATIONS_DEFINITION_TYPE: {
				SyncOperationsDefinitionType syncOperationsDefinitionType = (SyncOperationsDefinitionType)theEObject;
				T result = caseSyncOperationsDefinitionType(syncOperationsDefinitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_OPERATIONS_PERFORMANCE_TYPE: {
				SyncOperationsPerformanceType syncOperationsPerformanceType = (SyncOperationsPerformanceType)theEObject;
				T result = caseSyncOperationsPerformanceType(syncOperationsPerformanceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_OPERATIONS_SCHEDULE_TYPE: {
				SyncOperationsScheduleType syncOperationsScheduleType = (SyncOperationsScheduleType)theEObject;
				T result = caseSyncOperationsScheduleType(syncOperationsScheduleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_PERSONNEL_CLASS_TYPE: {
				SyncPersonnelClassType syncPersonnelClassType = (SyncPersonnelClassType)theEObject;
				T result = caseSyncPersonnelClassType(syncPersonnelClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_PERSONNEL_INFORMATION_TYPE: {
				SyncPersonnelInformationType syncPersonnelInformationType = (SyncPersonnelInformationType)theEObject;
				T result = caseSyncPersonnelInformationType(syncPersonnelInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_PERSON_TYPE: {
				SyncPersonType syncPersonType = (SyncPersonType)theEObject;
				T result = caseSyncPersonType(syncPersonType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: {
				SyncPhysicalAssetCapabilityTestSpecType syncPhysicalAssetCapabilityTestSpecType = (SyncPhysicalAssetCapabilityTestSpecType)theEObject;
				T result = caseSyncPhysicalAssetCapabilityTestSpecType(syncPhysicalAssetCapabilityTestSpecType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_PHYSICAL_ASSET_CLASS_TYPE: {
				SyncPhysicalAssetClassType syncPhysicalAssetClassType = (SyncPhysicalAssetClassType)theEObject;
				T result = caseSyncPhysicalAssetClassType(syncPhysicalAssetClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_PHYSICAL_ASSET_INFORMATION_TYPE: {
				SyncPhysicalAssetInformationType syncPhysicalAssetInformationType = (SyncPhysicalAssetInformationType)theEObject;
				T result = caseSyncPhysicalAssetInformationType(syncPhysicalAssetInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_PHYSICAL_ASSET_TYPE: {
				SyncPhysicalAssetType syncPhysicalAssetType = (SyncPhysicalAssetType)theEObject;
				T result = caseSyncPhysicalAssetType(syncPhysicalAssetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_PROCESS_SEGMENT_INFORMATION_TYPE: {
				SyncProcessSegmentInformationType syncProcessSegmentInformationType = (SyncProcessSegmentInformationType)theEObject;
				T result = caseSyncProcessSegmentInformationType(syncProcessSegmentInformationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_PROCESS_SEGMENT_TYPE: {
				SyncProcessSegmentType syncProcessSegmentType = (SyncProcessSegmentType)theEObject;
				T result = caseSyncProcessSegmentType(syncProcessSegmentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.SYNC_QUALIFICATION_TEST_SPECIFICATION_TYPE: {
				SyncQualificationTestSpecificationType syncQualificationTestSpecificationType = (SyncQualificationTestSpecificationType)theEObject;
				T result = caseSyncQualificationTestSpecificationType(syncQualificationTestSpecificationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TEST_DATE_TIME_TYPE: {
				TestDateTimeType testDateTimeType = (TestDateTimeType)theEObject;
				T result = caseTestDateTimeType(testDateTimeType);
				if (result == null) result = caseDateTimeType(testDateTimeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TESTED_EQUIPMENT_CLASS_PROPERTY_TYPE: {
				TestedEquipmentClassPropertyType testedEquipmentClassPropertyType = (TestedEquipmentClassPropertyType)theEObject;
				T result = caseTestedEquipmentClassPropertyType(testedEquipmentClassPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TESTED_EQUIPMENT_PROPERTY_TYPE: {
				TestedEquipmentPropertyType testedEquipmentPropertyType = (TestedEquipmentPropertyType)theEObject;
				T result = caseTestedEquipmentPropertyType(testedEquipmentPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE: {
				TestedMaterialClassPropertyType testedMaterialClassPropertyType = (TestedMaterialClassPropertyType)theEObject;
				T result = caseTestedMaterialClassPropertyType(testedMaterialClassPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TESTED_MATERIAL_DEFINITION_PROPERTY_TYPE: {
				TestedMaterialDefinitionPropertyType testedMaterialDefinitionPropertyType = (TestedMaterialDefinitionPropertyType)theEObject;
				T result = caseTestedMaterialDefinitionPropertyType(testedMaterialDefinitionPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TESTED_MATERIAL_LOT_PROPERTY_TYPE: {
				TestedMaterialLotPropertyType testedMaterialLotPropertyType = (TestedMaterialLotPropertyType)theEObject;
				T result = caseTestedMaterialLotPropertyType(testedMaterialLotPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE: {
				TestedPersonnelClassPropertyType testedPersonnelClassPropertyType = (TestedPersonnelClassPropertyType)theEObject;
				T result = caseTestedPersonnelClassPropertyType(testedPersonnelClassPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TESTED_PERSON_PROPERTY_TYPE: {
				TestedPersonPropertyType testedPersonPropertyType = (TestedPersonPropertyType)theEObject;
				T result = caseTestedPersonPropertyType(testedPersonPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE: {
				TestedPhysicalAssetClassPropertyType testedPhysicalAssetClassPropertyType = (TestedPhysicalAssetClassPropertyType)theEObject;
				T result = caseTestedPhysicalAssetClassPropertyType(testedPhysicalAssetClassPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_PROPERTY_TYPE: {
				TestedPhysicalAssetPropertyType testedPhysicalAssetPropertyType = (TestedPhysicalAssetPropertyType)theEObject;
				T result = caseTestedPhysicalAssetPropertyType(testedPhysicalAssetPropertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TEST_RESULT_TYPE: {
				TestResultType testResultType = (TestResultType)theEObject;
				T result = caseTestResultType(testResultType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TEXT_TYPE: {
				TextType textType = (TextType)theEObject;
				T result = caseTextType(textType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE: {
				TransAcknowledgeType transAcknowledgeType = (TransAcknowledgeType)theEObject;
				T result = caseTransAcknowledgeType(transAcknowledgeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE: {
				TransActionCriteriaType transActionCriteriaType = (TransActionCriteriaType)theEObject;
				T result = caseTransActionCriteriaType(transActionCriteriaType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE: {
				TransApplicationAreaType transApplicationAreaType = (TransApplicationAreaType)theEObject;
				T result = caseTransApplicationAreaType(transApplicationAreaType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_CANCEL_TYPE: {
				TransCancelType transCancelType = (TransCancelType)theEObject;
				T result = caseTransCancelType(transCancelType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE: {
				TransChangeStatusType transChangeStatusType = (TransChangeStatusType)theEObject;
				T result = caseTransChangeStatusType(transChangeStatusType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_CHANGE_TYPE: {
				TransChangeType transChangeType = (TransChangeType)theEObject;
				T result = caseTransChangeType(transChangeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_CONFIRMATION_CODE_TYPE: {
				TransConfirmationCodeType transConfirmationCodeType = (TransConfirmationCodeType)theEObject;
				T result = caseTransConfirmationCodeType(transConfirmationCodeType);
				if (result == null) result = caseCodeType(transConfirmationCodeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_CONFIRM_TYPE: {
				TransConfirmType transConfirmType = (TransConfirmType)theEObject;
				T result = caseTransConfirmType(transConfirmType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_EXPRESSION1_TYPE: {
				TransExpression1Type transExpression1Type = (TransExpression1Type)theEObject;
				T result = caseTransExpression1Type(transExpression1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_EXPRESSION_TYPE: {
				TransExpressionType transExpressionType = (TransExpressionType)theEObject;
				T result = caseTransExpressionType(transExpressionType);
				if (result == null) result = caseTransExpression1Type(transExpressionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_GET_TYPE: {
				TransGetType transGetType = (TransGetType)theEObject;
				T result = caseTransGetType(transGetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_PROCESS_TYPE: {
				TransProcessType transProcessType = (TransProcessType)theEObject;
				T result = caseTransProcessType(transProcessType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_RECEIVER_TYPE: {
				TransReceiverType transReceiverType = (TransReceiverType)theEObject;
				T result = caseTransReceiverType(transReceiverType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_RESPOND_TYPE: {
				TransRespondType transRespondType = (TransRespondType)theEObject;
				T result = caseTransRespondType(transRespondType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE: {
				TransResponseCriteriaType transResponseCriteriaType = (TransResponseCriteriaType)theEObject;
				T result = caseTransResponseCriteriaType(transResponseCriteriaType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_SENDER_TYPE: {
				TransSenderType transSenderType = (TransSenderType)theEObject;
				T result = caseTransSenderType(transSenderType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_SHOW_TYPE: {
				TransShowType transShowType = (TransShowType)theEObject;
				T result = caseTransShowType(transShowType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_SIGNATURE_TYPE: {
				TransSignatureType transSignatureType = (TransSignatureType)theEObject;
				T result = caseTransSignatureType(transSignatureType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE: {
				TransStateChangeType transStateChangeType = (TransStateChangeType)theEObject;
				T result = caseTransStateChangeType(transStateChangeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_SYNC_TYPE: {
				TransSyncType transSyncType = (TransSyncType)theEObject;
				T result = caseTransSyncType(transSyncType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.TRANS_USER_AREA_TYPE: {
				TransUserAreaType transUserAreaType = (TransUserAreaType)theEObject;
				T result = caseTransUserAreaType(transUserAreaType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.UNIT_OF_MEASURE_TYPE: {
				UnitOfMeasureType unitOfMeasureType = (UnitOfMeasureType)theEObject;
				T result = caseUnitOfMeasureType(unitOfMeasureType);
				if (result == null) result = caseCodeType(unitOfMeasureType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.VALUE_STRING_TYPE: {
				ValueStringType valueStringType = (ValueStringType)theEObject;
				T result = caseValueStringType(valueStringType);
				if (result == null) result = caseAnyGenericValueType(valueStringType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.VALUE_TYPE: {
				ValueType valueType = (ValueType)theEObject;
				T result = caseValueType(valueType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.VERSION_TYPE: {
				VersionType versionType = (VersionType)theEObject;
				T result = caseVersionType(versionType);
				if (result == null) result = caseIdentifierType(versionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.WORK_REQUEST_ID_TYPE: {
				WorkRequestIDType workRequestIDType = (WorkRequestIDType)theEObject;
				T result = caseWorkRequestIDType(workRequestIDType);
				if (result == null) result = caseIdentifierType(workRequestIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.WORK_SCHEDULE_ID_TYPE: {
				WorkScheduleIDType workScheduleIDType = (WorkScheduleIDType)theEObject;
				T result = caseWorkScheduleIDType(workScheduleIDType);
				if (result == null) result = caseIdentifierType(workScheduleIDType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.WORK_TYPE1_TYPE: {
				WorkType1Type workType1Type = (WorkType1Type)theEObject;
				T result = caseWorkType1Type(workType1Type);
				if (result == null) result = caseCodeType(workType1Type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case B2MMLPackage.WORK_TYPE_TYPE: {
				WorkTypeType workTypeType = (WorkTypeType)theEObject;
				T result = caseWorkTypeType(workTypeType);
				if (result == null) result = caseWorkType1Type(workTypeType);
				if (result == null) result = caseCodeType(workTypeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Equipment Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeEquipmentCapabilityTestSpecType(AcknowledgeEquipmentCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Equipment Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeEquipmentClassType(AcknowledgeEquipmentClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Equipment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeEquipmentInformationType(AcknowledgeEquipmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Equipment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeEquipmentType(AcknowledgeEquipmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Material Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeMaterialClassType(AcknowledgeMaterialClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Material Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeMaterialDefinitionType(AcknowledgeMaterialDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Material Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeMaterialInformationType(AcknowledgeMaterialInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Material Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeMaterialLotType(AcknowledgeMaterialLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Material Sub Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeMaterialSubLotType(AcknowledgeMaterialSubLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Material Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeMaterialTestSpecType(AcknowledgeMaterialTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Operations Capability Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeOperationsCapabilityInformationType(AcknowledgeOperationsCapabilityInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Operations Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeOperationsCapabilityType(AcknowledgeOperationsCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Operations Definition Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeOperationsDefinitionInformationType(AcknowledgeOperationsDefinitionInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Operations Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeOperationsDefinitionType(AcknowledgeOperationsDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Operations Performance Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeOperationsPerformanceType(AcknowledgeOperationsPerformanceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Operations Schedule Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeOperationsScheduleType(AcknowledgeOperationsScheduleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Personnel Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgePersonnelClassType(AcknowledgePersonnelClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Personnel Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgePersonnelInformationType(AcknowledgePersonnelInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Person Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgePersonType(AcknowledgePersonType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Physical Asset Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgePhysicalAssetCapabilityTestSpecType(AcknowledgePhysicalAssetCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Physical Asset Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgePhysicalAssetClassType(AcknowledgePhysicalAssetClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Physical Asset Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgePhysicalAssetInformationType(AcknowledgePhysicalAssetInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Physical Asset Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgePhysicalAssetType(AcknowledgePhysicalAssetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Process Segment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeProcessSegmentInformationType(AcknowledgeProcessSegmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Process Segment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeProcessSegmentType(AcknowledgeProcessSegmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Acknowledge Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Acknowledge Qualification Test Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcknowledgeQualificationTestSpecificationType(AcknowledgeQualificationTestSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Actual End Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Actual End Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActualEndTimeType(ActualEndTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Actual Finish Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Actual Finish Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActualFinishTimeType(ActualFinishTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Actual Start Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Actual Start Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActualStartTimeType(ActualStartTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Amount Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Amount Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAmountType(AmountType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Any Generic Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Any Generic Value Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnyGenericValueType(AnyGenericValueType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assembly Relationship1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assembly Relationship1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssemblyRelationship1Type(AssemblyRelationship1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assembly Relationship Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assembly Relationship Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssemblyRelationshipType(AssemblyRelationshipType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assembly Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assembly Type1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssemblyType1Type(AssemblyType1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assembly Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assembly Type Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssemblyTypeType(AssemblyTypeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bill Of Material ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bill Of Material ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBillOfMaterialIDType(BillOfMaterialIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bill Of Materials ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bill Of Materials ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBillOfMaterialsIDType(BillOfMaterialsIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bill Of Resources ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bill Of Resources ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBillOfResourcesIDType(BillOfResourcesIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Object Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Object Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryObjectType(BinaryObjectType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BOD Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BOD Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBODType(BODType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Equipment Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelEquipmentCapabilityTestSpecType(CancelEquipmentCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Equipment Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelEquipmentClassType(CancelEquipmentClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Equipment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelEquipmentInformationType(CancelEquipmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Equipment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelEquipmentType(CancelEquipmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Material Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelMaterialClassType(CancelMaterialClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Material Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelMaterialDefinitionType(CancelMaterialDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Material Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelMaterialInformationType(CancelMaterialInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Material Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelMaterialLotType(CancelMaterialLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Material Sub Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelMaterialSubLotType(CancelMaterialSubLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Material Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelMaterialTestSpecType(CancelMaterialTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Operations Capability Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelOperationsCapabilityInformationType(CancelOperationsCapabilityInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Operations Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelOperationsCapabilityType(CancelOperationsCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Operations Definition Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelOperationsDefinitionInformationType(CancelOperationsDefinitionInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Operations Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelOperationsDefinitionType(CancelOperationsDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Operations Performance Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelOperationsPerformanceType(CancelOperationsPerformanceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Operations Schedule Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelOperationsScheduleType(CancelOperationsScheduleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Personnel Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelPersonnelClassType(CancelPersonnelClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Personnel Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelPersonnelInformationType(CancelPersonnelInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Person Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelPersonType(CancelPersonType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Physical Asset Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelPhysicalAssetCapabilityTestSpecType(CancelPhysicalAssetCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Physical Asset Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelPhysicalAssetClassType(CancelPhysicalAssetClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Physical Asset Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelPhysicalAssetInformationType(CancelPhysicalAssetInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Physical Asset Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelPhysicalAssetType(CancelPhysicalAssetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Process Segment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelProcessSegmentInformationType(CancelProcessSegmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Process Segment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelProcessSegmentType(CancelProcessSegmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cancel Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cancel Qualification Test Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCancelQualificationTestSpecificationType(CancelQualificationTestSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Capability Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Capability Type1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCapabilityType1Type(CapabilityType1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Capability Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Capability Type Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCapabilityTypeType(CapabilityTypeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cause Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cause Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCauseType(CauseType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Certificate Of Analysis Reference Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Certificate Of Analysis Reference Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCertificateOfAnalysisReferenceType(CertificateOfAnalysisReferenceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Equipment Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeEquipmentCapabilityTestSpecType(ChangeEquipmentCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Equipment Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeEquipmentClassType(ChangeEquipmentClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Equipment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeEquipmentInformationType(ChangeEquipmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Equipment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeEquipmentType(ChangeEquipmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Material Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeMaterialClassType(ChangeMaterialClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Material Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeMaterialDefinitionType(ChangeMaterialDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Material Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeMaterialInformationType(ChangeMaterialInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Material Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeMaterialLotType(ChangeMaterialLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Material Sub Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeMaterialSubLotType(ChangeMaterialSubLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Material Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeMaterialTestSpecType(ChangeMaterialTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Operations Capability Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeOperationsCapabilityInformationType(ChangeOperationsCapabilityInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Operations Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeOperationsCapabilityType(ChangeOperationsCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Operations Definition Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeOperationsDefinitionInformationType(ChangeOperationsDefinitionInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Operations Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeOperationsDefinitionType(ChangeOperationsDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Operations Performance Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeOperationsPerformanceType(ChangeOperationsPerformanceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Operations Schedule Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeOperationsScheduleType(ChangeOperationsScheduleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Personnel Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangePersonnelClassType(ChangePersonnelClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Personnel Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangePersonnelInformationType(ChangePersonnelInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Person Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangePersonType(ChangePersonType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Physical Asset Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangePhysicalAssetCapabilityTestSpecType(ChangePhysicalAssetCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Physical Asset Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangePhysicalAssetClassType(ChangePhysicalAssetClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Physical Asset Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangePhysicalAssetInformationType(ChangePhysicalAssetInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Physical Asset Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangePhysicalAssetType(ChangePhysicalAssetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Process Segment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeProcessSegmentInformationType(ChangeProcessSegmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Process Segment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeProcessSegmentType(ChangeProcessSegmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Qualification Test Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeQualificationTestSpecificationType(ChangeQualificationTestSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeType(CodeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Confidence Factor Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Confidence Factor Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfidenceFactorType(ConfidenceFactorType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Confirm BOD Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Confirm BOD Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfirmBODType(ConfirmBODType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Correction Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Correction Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrectionType(CorrectionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType(DataAreaType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType1(DataAreaType1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType2(DataAreaType2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType3(DataAreaType3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType4(DataAreaType4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type5</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type5</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType5(DataAreaType5 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type6</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type6</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType6(DataAreaType6 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type7</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type7</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType7(DataAreaType7 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type8</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type8</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType8(DataAreaType8 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type9</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type9</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType9(DataAreaType9 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type10</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type10</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType10(DataAreaType10 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type11</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type11</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType11(DataAreaType11 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type12</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type12</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType12(DataAreaType12 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type13</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type13</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType13(DataAreaType13 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type14</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type14</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType14(DataAreaType14 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type15</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type15</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType15(DataAreaType15 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type16</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type16</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType16(DataAreaType16 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type17</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type17</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType17(DataAreaType17 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type18</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type18</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType18(DataAreaType18 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type19</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type19</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType19(DataAreaType19 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type20</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type20</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType20(DataAreaType20 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type21</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type21</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType21(DataAreaType21 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type22</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type22</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType22(DataAreaType22 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type23</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type23</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType23(DataAreaType23 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type24</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type24</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType24(DataAreaType24 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type25</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type25</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType25(DataAreaType25 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type26</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type26</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType26(DataAreaType26 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type27</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type27</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType27(DataAreaType27 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type28</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type28</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType28(DataAreaType28 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type29</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type29</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType29(DataAreaType29 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type30</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type30</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType30(DataAreaType30 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type31</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type31</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType31(DataAreaType31 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type32</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type32</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType32(DataAreaType32 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type33</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type33</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType33(DataAreaType33 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type34</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type34</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType34(DataAreaType34 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type35</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type35</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType35(DataAreaType35 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type36</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type36</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType36(DataAreaType36 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type37</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type37</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType37(DataAreaType37 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type38</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type38</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType38(DataAreaType38 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type39</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type39</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType39(DataAreaType39 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type40</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type40</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType40(DataAreaType40 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type41</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type41</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType41(DataAreaType41 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type42</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type42</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType42(DataAreaType42 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type43</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type43</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType43(DataAreaType43 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type44</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type44</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType44(DataAreaType44 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type45</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type45</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType45(DataAreaType45 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type46</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type46</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType46(DataAreaType46 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type47</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type47</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType47(DataAreaType47 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type48</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type48</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType48(DataAreaType48 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type49</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type49</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType49(DataAreaType49 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type50</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type50</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType50(DataAreaType50 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type51</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type51</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType51(DataAreaType51 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type52</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type52</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType52(DataAreaType52 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type53</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type53</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType53(DataAreaType53 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type54</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type54</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType54(DataAreaType54 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type55</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type55</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType55(DataAreaType55 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type56</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type56</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType56(DataAreaType56 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type57</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type57</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType57(DataAreaType57 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type58</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type58</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType58(DataAreaType58 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type59</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type59</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType59(DataAreaType59 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type60</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type60</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType60(DataAreaType60 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type61</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type61</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType61(DataAreaType61 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type62</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type62</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType62(DataAreaType62 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type63</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type63</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType63(DataAreaType63 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type64</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type64</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType64(DataAreaType64 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type65</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type65</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType65(DataAreaType65 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type66</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type66</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType66(DataAreaType66 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type67</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type67</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType67(DataAreaType67 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type68</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type68</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType68(DataAreaType68 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type69</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type69</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType69(DataAreaType69 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type70</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type70</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType70(DataAreaType70 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type71</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type71</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType71(DataAreaType71 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type72</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type72</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType72(DataAreaType72 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type73</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type73</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType73(DataAreaType73 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type74</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type74</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType74(DataAreaType74 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type75</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type75</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType75(DataAreaType75 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type76</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type76</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType76(DataAreaType76 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type77</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type77</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType77(DataAreaType77 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type78</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type78</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType78(DataAreaType78 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type79</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type79</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType79(DataAreaType79 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type80</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type80</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType80(DataAreaType80 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type81</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type81</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType81(DataAreaType81 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type82</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type82</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType82(DataAreaType82 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type83</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type83</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType83(DataAreaType83 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type84</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type84</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType84(DataAreaType84 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type85</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type85</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType85(DataAreaType85 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type86</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type86</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType86(DataAreaType86 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type87</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type87</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType87(DataAreaType87 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type88</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type88</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType88(DataAreaType88 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type89</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type89</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType89(DataAreaType89 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type90</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type90</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType90(DataAreaType90 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type91</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type91</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType91(DataAreaType91 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type92</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type92</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType92(DataAreaType92 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type93</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type93</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType93(DataAreaType93 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type94</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type94</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType94(DataAreaType94 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type95</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type95</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType95(DataAreaType95 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type96</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type96</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType96(DataAreaType96 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type97</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type97</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType97(DataAreaType97 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type98</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type98</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType98(DataAreaType98 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type99</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type99</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType99(DataAreaType99 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type100</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type100</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType100(DataAreaType100 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type101</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type101</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType101(DataAreaType101 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type102</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type102</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType102(DataAreaType102 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type103</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type103</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType103(DataAreaType103 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type104</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type104</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType104(DataAreaType104 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type105</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type105</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType105(DataAreaType105 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type106</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type106</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType106(DataAreaType106 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type107</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type107</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType107(DataAreaType107 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type108</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type108</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType108(DataAreaType108 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type109</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type109</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType109(DataAreaType109 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type110</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type110</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType110(DataAreaType110 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type111</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type111</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType111(DataAreaType111 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type112</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type112</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType112(DataAreaType112 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type113</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type113</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType113(DataAreaType113 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type114</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type114</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType114(DataAreaType114 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type115</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type115</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType115(DataAreaType115 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type116</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type116</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType116(DataAreaType116 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type117</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type117</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType117(DataAreaType117 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type118</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type118</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType118(DataAreaType118 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type119</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type119</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType119(DataAreaType119 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type120</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type120</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType120(DataAreaType120 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type121</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type121</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType121(DataAreaType121 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type122</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type122</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType122(DataAreaType122 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type123</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type123</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType123(DataAreaType123 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type124</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type124</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType124(DataAreaType124 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type125</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type125</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType125(DataAreaType125 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type126</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type126</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType126(DataAreaType126 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type127</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type127</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType127(DataAreaType127 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type128</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type128</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType128(DataAreaType128 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type129</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type129</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType129(DataAreaType129 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type130</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type130</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType130(DataAreaType130 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type131</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type131</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType131(DataAreaType131 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type132</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type132</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType132(DataAreaType132 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type133</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type133</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType133(DataAreaType133 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type134</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type134</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType134(DataAreaType134 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type135</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type135</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType135(DataAreaType135 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type136</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type136</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType136(DataAreaType136 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type137</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type137</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType137(DataAreaType137 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type138</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type138</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType138(DataAreaType138 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type139</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type139</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType139(DataAreaType139 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type140</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type140</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType140(DataAreaType140 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type141</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type141</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType141(DataAreaType141 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type142</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type142</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType142(DataAreaType142 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type143</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type143</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType143(DataAreaType143 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type144</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type144</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType144(DataAreaType144 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type145</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type145</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType145(DataAreaType145 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type146</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type146</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType146(DataAreaType146 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type147</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type147</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType147(DataAreaType147 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type148</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type148</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType148(DataAreaType148 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type149</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type149</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType149(DataAreaType149 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type150</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type150</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType150(DataAreaType150 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type151</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type151</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType151(DataAreaType151 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type152</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type152</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType152(DataAreaType152 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type153</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type153</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType153(DataAreaType153 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type154</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type154</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType154(DataAreaType154 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type155</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type155</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType155(DataAreaType155 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type156</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type156</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType156(DataAreaType156 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type157</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type157</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType157(DataAreaType157 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type158</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type158</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType158(DataAreaType158 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type159</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type159</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType159(DataAreaType159 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type160</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type160</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType160(DataAreaType160 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type161</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type161</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType161(DataAreaType161 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type162</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type162</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType162(DataAreaType162 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type163</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type163</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType163(DataAreaType163 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type164</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type164</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType164(DataAreaType164 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type165</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type165</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType165(DataAreaType165 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type166</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type166</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType166(DataAreaType166 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type167</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type167</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType167(DataAreaType167 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type168</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type168</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType168(DataAreaType168 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type169</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type169</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType169(DataAreaType169 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type170</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type170</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType170(DataAreaType170 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type171</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type171</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType171(DataAreaType171 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type172</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type172</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType172(DataAreaType172 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type173</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type173</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType173(DataAreaType173 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type174</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type174</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType174(DataAreaType174 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type175</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type175</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType175(DataAreaType175 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type176</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type176</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType176(DataAreaType176 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type177</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type177</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType177(DataAreaType177 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type178</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type178</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType178(DataAreaType178 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type179</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type179</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType179(DataAreaType179 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type180</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type180</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType180(DataAreaType180 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type181</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type181</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType181(DataAreaType181 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type182</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type182</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType182(DataAreaType182 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type183</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type183</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType183(DataAreaType183 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type184</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type184</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType184(DataAreaType184 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type185</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type185</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType185(DataAreaType185 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type186</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type186</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType186(DataAreaType186 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type187</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type187</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType187(DataAreaType187 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type188</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type188</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType188(DataAreaType188 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type189</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type189</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType189(DataAreaType189 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type190</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type190</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType190(DataAreaType190 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type191</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type191</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType191(DataAreaType191 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type192</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type192</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType192(DataAreaType192 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type193</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type193</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType193(DataAreaType193 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type194</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type194</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType194(DataAreaType194 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type195</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type195</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType195(DataAreaType195 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type196</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type196</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType196(DataAreaType196 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type197</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type197</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType197(DataAreaType197 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type198</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type198</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType198(DataAreaType198 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type199</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type199</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType199(DataAreaType199 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type200</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type200</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType200(DataAreaType200 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type201</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type201</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType201(DataAreaType201 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type202</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type202</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType202(DataAreaType202 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type203</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type203</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType203(DataAreaType203 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type204</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type204</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType204(DataAreaType204 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type205</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type205</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType205(DataAreaType205 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type206</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type206</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType206(DataAreaType206 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type207</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type207</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType207(DataAreaType207 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Area Type208</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Area Type208</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataAreaType208(DataAreaType208 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Type1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataType1Type(DataType1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Type Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataTypeType(DataTypeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Date Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Date Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDateTimeType(DateTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dependency1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dependency1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDependency1Type(Dependency1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dependency Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dependency Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDependencyType(DependencyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Description Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Description Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDescriptionType(DescriptionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDocumentRoot(DocumentRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earliest Start Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earliest Start Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarliestStartTimeType(EarliestStartTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>End Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>End Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEndTimeType(EndTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Asset Mapping Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Asset Mapping Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentAssetMappingType(EquipmentAssetMappingType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Capability Test Specification ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Capability Test Specification ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentCapabilityTestSpecificationIDType(EquipmentCapabilityTestSpecificationIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Capability Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Capability Test Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentCapabilityTestSpecificationType(EquipmentCapabilityTestSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Class ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Class ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentClassIDType(EquipmentClassIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Class Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentClassPropertyType(EquipmentClassPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentClassType(EquipmentClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Element Level1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Element Level1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentElementLevel1Type(EquipmentElementLevel1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Element Level Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Element Level Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentElementLevelType(EquipmentElementLevelType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentIDType(EquipmentIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentInformationType(EquipmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentPropertyType(EquipmentPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Segment Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Segment Specification Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentSegmentSpecificationPropertyType(EquipmentSegmentSpecificationPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Segment Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Segment Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentSegmentSpecificationType(EquipmentSegmentSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentType(EquipmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equipment Use Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equipment Use Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEquipmentUseType(EquipmentUseType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expiration Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expiration Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpirationTimeType(ExpirationTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Equipment Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetEquipmentCapabilityTestSpecType(GetEquipmentCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Equipment Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetEquipmentClassType(GetEquipmentClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Equipment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetEquipmentInformationType(GetEquipmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Equipment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetEquipmentType(GetEquipmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Material Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetMaterialClassType(GetMaterialClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Material Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetMaterialDefinitionType(GetMaterialDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Material Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetMaterialInformationType(GetMaterialInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Material Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetMaterialLotType(GetMaterialLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Material Sub Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetMaterialSubLotType(GetMaterialSubLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Material Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetMaterialTestSpecType(GetMaterialTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Operations Capability Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetOperationsCapabilityInformationType(GetOperationsCapabilityInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Operations Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetOperationsCapabilityType(GetOperationsCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Operations Definition Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetOperationsDefinitionInformationType(GetOperationsDefinitionInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Operations Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetOperationsDefinitionType(GetOperationsDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Operations Performance Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetOperationsPerformanceType(GetOperationsPerformanceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Operations Schedule Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetOperationsScheduleType(GetOperationsScheduleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Personnel Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetPersonnelClassType(GetPersonnelClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Personnel Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetPersonnelInformationType(GetPersonnelInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Person Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetPersonType(GetPersonType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Physical Asset Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetPhysicalAssetCapabilityTestSpecType(GetPhysicalAssetCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Physical Asset Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetPhysicalAssetClassType(GetPhysicalAssetClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Physical Asset Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetPhysicalAssetInformationType(GetPhysicalAssetInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Physical Asset Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetPhysicalAssetType(GetPhysicalAssetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Process Segment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetProcessSegmentInformationType(GetProcessSegmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Process Segment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetProcessSegmentType(GetProcessSegmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Qualification Test Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetQualificationTestSpecificationType(GetQualificationTestSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hierarchy Scope Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hierarchy Scope Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHierarchyScopeType(HierarchyScopeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Identifier Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Identifier Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdentifierType(IdentifierType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Job Order Command1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Job Order Command1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJobOrderCommand1Type(JobOrderCommand1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Job Order Command Rule Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Job Order Command Rule Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJobOrderCommandRuleType(JobOrderCommandRuleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Job Order Command Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Job Order Command Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJobOrderCommandType(JobOrderCommandType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Job Order Dispatch Status Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Job Order Dispatch Status Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJobOrderDispatchStatusType(JobOrderDispatchStatusType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Latest End Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Latest End Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLatestEndTimeType(LatestEndTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Location Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Location Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocationType(LocationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Manufacturing Bill ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Manufacturing Bill ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseManufacturingBillIDType(ManufacturingBillIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Actual ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Actual ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialActualIDType(MaterialActualIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Capability ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Capability ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialCapabilityIDType(MaterialCapabilityIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Class ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Class ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialClassIDType(MaterialClassIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Class Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialClassPropertyType(MaterialClassPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialClassType(MaterialClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Definition ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Definition ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialDefinitionIDType(MaterialDefinitionIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Definition Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Definition Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialDefinitionPropertyType(MaterialDefinitionPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialDefinitionType(MaterialDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialInformationType(MaterialInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Lot ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Lot ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialLotIDType(MaterialLotIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Lot Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Lot Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialLotPropertyType(MaterialLotPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialLotType(MaterialLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Requirement ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Requirement ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialRequirementIDType(MaterialRequirementIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Segment Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Segment Specification Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialSegmentSpecificationPropertyType(MaterialSegmentSpecificationPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Segment Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Segment Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialSegmentSpecificationType(MaterialSegmentSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Specification ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Specification ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialSpecificationIDType(MaterialSpecificationIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Sub Lot ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Sub Lot ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialSubLotIDType(MaterialSubLotIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Sub Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialSubLotType(MaterialSubLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Test Specification ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Test Specification ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialTestSpecificationIDType(MaterialTestSpecificationIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Test Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialTestSpecificationType(MaterialTestSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Use1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Use1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialUse1Type(MaterialUse1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Material Use Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Material Use Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaterialUseType(MaterialUseType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Measure Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Measure Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMeasureType(MeasureType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNameType(NameType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Numeric Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Numeric Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumericType(NumericType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Equipment Actual Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Equipment Actual Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpEquipmentActualPropertyType(OpEquipmentActualPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Equipment Actual Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Equipment Actual Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpEquipmentActualType(OpEquipmentActualType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Equipment Capability Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Equipment Capability Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpEquipmentCapabilityPropertyType(OpEquipmentCapabilityPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Equipment Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Equipment Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpEquipmentCapabilityType(OpEquipmentCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Equipment Requirement Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Equipment Requirement Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpEquipmentRequirementPropertyType(OpEquipmentRequirementPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Equipment Requirement Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Equipment Requirement Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpEquipmentRequirementType(OpEquipmentRequirementType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Equipment Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Equipment Specification Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpEquipmentSpecificationPropertyType(OpEquipmentSpecificationPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Equipment Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Equipment Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpEquipmentSpecificationType(OpEquipmentSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Capability Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsCapabilityInformationType(OperationsCapabilityInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsCapabilityType(OperationsCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Definition ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Definition ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsDefinitionIDType(OperationsDefinitionIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Definition Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsDefinitionInformationType(OperationsDefinitionInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsDefinitionType(OperationsDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Material Bill Item Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Material Bill Item Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsMaterialBillItemType(OperationsMaterialBillItemType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Material Bill Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Material Bill Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsMaterialBillType(OperationsMaterialBillType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Performance Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsPerformanceType(OperationsPerformanceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Request ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Request ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsRequestIDType(OperationsRequestIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Request Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Request Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsRequestType(OperationsRequestType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Response Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Response Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsResponseType(OperationsResponseType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Schedule ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Schedule ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsScheduleIDType(OperationsScheduleIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Schedule Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsScheduleType(OperationsScheduleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Segment ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Segment ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsSegmentIDType(OperationsSegmentIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Segment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsSegmentType(OperationsSegmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Type1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsType1Type(OperationsType1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operations Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operations Type Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationsTypeType(OperationsTypeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Material Actual Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Material Actual Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpMaterialActualPropertyType(OpMaterialActualPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Material Actual Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Material Actual Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpMaterialActualType(OpMaterialActualType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Material Capability Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Material Capability Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpMaterialCapabilityPropertyType(OpMaterialCapabilityPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Material Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Material Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpMaterialCapabilityType(OpMaterialCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Material Requirement Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Material Requirement Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpMaterialRequirementPropertyType(OpMaterialRequirementPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Material Requirement Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Material Requirement Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpMaterialRequirementType(OpMaterialRequirementType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Material Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Material Specification Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpMaterialSpecificationPropertyType(OpMaterialSpecificationPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Material Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Material Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpMaterialSpecificationType(OpMaterialSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Personnel Actual Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Personnel Actual Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPersonnelActualPropertyType(OpPersonnelActualPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Personnel Actual Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Personnel Actual Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPersonnelActualType(OpPersonnelActualType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Personnel Capability Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Personnel Capability Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPersonnelCapabilityPropertyType(OpPersonnelCapabilityPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Personnel Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Personnel Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPersonnelCapabilityType(OpPersonnelCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Personnel Requirement Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Personnel Requirement Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPersonnelRequirementPropertyType(OpPersonnelRequirementPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Personnel Requirement Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Personnel Requirement Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPersonnelRequirementType(OpPersonnelRequirementType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Personnel Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Personnel Specification Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPersonnelSpecificationPropertyType(OpPersonnelSpecificationPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Personnel Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Personnel Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPersonnelSpecificationType(OpPersonnelSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Physical Asset Actual Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Physical Asset Actual Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPhysicalAssetActualPropertyType(OpPhysicalAssetActualPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Physical Asset Actual Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Physical Asset Actual Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPhysicalAssetActualType(OpPhysicalAssetActualType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Physical Asset Capability Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Physical Asset Capability Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPhysicalAssetCapabilityPropertyType(OpPhysicalAssetCapabilityPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Physical Asset Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Physical Asset Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPhysicalAssetCapabilityType(OpPhysicalAssetCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Physical Asset Requirement Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Physical Asset Requirement Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPhysicalAssetRequirementPropertyType(OpPhysicalAssetRequirementPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Physical Asset Requirement Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Physical Asset Requirement Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPhysicalAssetRequirementType(OpPhysicalAssetRequirementType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Physical Asset Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Physical Asset Specification Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPhysicalAssetSpecificationPropertyType(OpPhysicalAssetSpecificationPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Physical Asset Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Physical Asset Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpPhysicalAssetSpecificationType(OpPhysicalAssetSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Process Segment Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Process Segment Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpProcessSegmentCapabilityType(OpProcessSegmentCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Segment Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Segment Data Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpSegmentDataType(OpSegmentDataType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Segment Requirement Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Segment Requirement Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpSegmentRequirementType(OpSegmentRequirementType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Op Segment Response Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Op Segment Response Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOpSegmentResponseType(OpSegmentResponseType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Other Dependency Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Other Dependency Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOtherDependencyType(OtherDependencyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterIDType(ParameterIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterType(ParameterType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Person ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Person ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonIDType(PersonIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Person Name Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Person Name Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonNameType(PersonNameType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Class ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Class ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelClassIDType(PersonnelClassIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Class Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelClassPropertyType(PersonnelClassPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelClassType(PersonnelClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelInformationType(PersonnelInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Segment Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Segment Specification Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelSegmentSpecificationPropertyType(PersonnelSegmentSpecificationPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Segment Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Segment Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelSegmentSpecificationType(PersonnelSegmentSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Personnel Use Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Personnel Use Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonnelUseType(PersonnelUseType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Person Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Person Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonPropertyType(PersonPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Person Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonType(PersonType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Actual ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Actual ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetActualIDType(PhysicalAssetActualIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Capability Test Specification ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Capability Test Specification ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetCapabilityTestSpecificationIDType(PhysicalAssetCapabilityTestSpecificationIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Capability Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Capability Test Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetCapabilityTestSpecificationType(PhysicalAssetCapabilityTestSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Class ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Class ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetClassIDType(PhysicalAssetClassIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Class Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetClassPropertyType(PhysicalAssetClassPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetClassType(PhysicalAssetClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetIDType(PhysicalAssetIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetInformationType(PhysicalAssetInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetPropertyType(PhysicalAssetPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Segment Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Segment Specification Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetSegmentSpecificationPropertyType(PhysicalAssetSegmentSpecificationPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Segment Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Segment Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetSegmentSpecificationType(PhysicalAssetSegmentSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetType(PhysicalAssetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Asset Use Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Asset Use Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhysicalAssetUseType(PhysicalAssetUseType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Planned Finish Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Planned Finish Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePlannedFinishTimeType(PlannedFinishTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Planned Start Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Planned Start Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePlannedStartTimeType(PlannedStartTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Priority Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Priority Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePriorityType(PriorityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Problem Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Problem Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProblemType(ProblemType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Equipment Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessEquipmentCapabilityTestSpecType(ProcessEquipmentCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Equipment Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessEquipmentClassType(ProcessEquipmentClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Equipment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessEquipmentInformationType(ProcessEquipmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Equipment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessEquipmentType(ProcessEquipmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Material Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessMaterialClassType(ProcessMaterialClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Material Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessMaterialDefinitionType(ProcessMaterialDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Material Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessMaterialInformationType(ProcessMaterialInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Material Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessMaterialLotType(ProcessMaterialLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Material Sub Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessMaterialSubLotType(ProcessMaterialSubLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Material Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessMaterialTestSpecType(ProcessMaterialTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Operations Capability Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessOperationsCapabilityInformationType(ProcessOperationsCapabilityInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Operations Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessOperationsCapabilityType(ProcessOperationsCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Operations Definition Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessOperationsDefinitionInformationType(ProcessOperationsDefinitionInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Operations Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessOperationsDefinitionType(ProcessOperationsDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Operations Performance Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessOperationsPerformanceType(ProcessOperationsPerformanceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Operations Schedule Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessOperationsScheduleType(ProcessOperationsScheduleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Personnel Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessPersonnelClassType(ProcessPersonnelClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Personnel Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessPersonnelInformationType(ProcessPersonnelInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Person Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessPersonType(ProcessPersonType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Physical Asset Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessPhysicalAssetCapabilityTestSpecType(ProcessPhysicalAssetCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Physical Asset Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessPhysicalAssetClassType(ProcessPhysicalAssetClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Physical Asset Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessPhysicalAssetInformationType(ProcessPhysicalAssetInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Physical Asset Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessPhysicalAssetType(ProcessPhysicalAssetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Process Segment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessProcessSegmentInformationType(ProcessProcessSegmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Process Segment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessProcessSegmentType(ProcessProcessSegmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Qualification Test Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessQualificationTestSpecificationType(ProcessQualificationTestSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Segment ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Segment ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessSegmentIDType(ProcessSegmentIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Segment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessSegmentInformationType(ProcessSegmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process Segment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessSegmentType(ProcessSegmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Production Request ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Production Request ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProductionRequestIDType(ProductionRequestIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Production Schedule ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Production Schedule ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProductionScheduleIDType(ProductionScheduleIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Product Production Rule ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Product Production Rule ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProductProductionRuleIDType(ProductProductionRuleIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Product Production Rule Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Product Production Rule Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProductProductionRuleType(ProductProductionRuleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Product Segment ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Product Segment ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProductSegmentIDType(ProductSegmentIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertyIDType(PropertyIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Published Date Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Published Date Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePublishedDateType(PublishedDateType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Qualification Test Specification ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Qualification Test Specification ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQualificationTestSpecificationIDType(QualificationTestSpecificationIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Qualification Test Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQualificationTestSpecificationType(QualificationTestSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Quantity String Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Quantity String Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQuantityStringType(QuantityStringType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Quantity Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Quantity Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQuantityType(QuantityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Quantity Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Quantity Value Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQuantityValueType(QuantityValueType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reason Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reason Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReasonType(ReasonType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relationship Form1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relationship Form1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelationshipForm1Type(RelationshipForm1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relationship Form Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relationship Form Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelationshipFormType(RelationshipFormType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relationship Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relationship Type1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelationshipType1Type(RelationshipType1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relationship Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relationship Type Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelationshipTypeType(RelationshipTypeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Requested Completion Date Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Requested Completion Date Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequestedCompletionDateType(RequestedCompletionDateType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Requested Priority Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Requested Priority Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequestedPriorityType(RequestedPriorityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Request State1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Request State1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequestState1Type(RequestState1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Request State Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Request State Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequestStateType(RequestStateType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Required By Requested Segment Response1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Required By Requested Segment Response1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequiredByRequestedSegmentResponse1Type(RequiredByRequestedSegmentResponse1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Required By Requested Segment Response Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Required By Requested Segment Response Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequiredByRequestedSegmentResponseType(RequiredByRequestedSegmentResponseType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceIDType(ResourceIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Network Connection ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Network Connection ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceNetworkConnectionIDType(ResourceNetworkConnectionIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Reference Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Reference Type1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceReferenceType1Type(ResourceReferenceType1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Reference Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Reference Type Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceReferenceTypeType(ResourceReferenceTypeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resources Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resources Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourcesType(ResourcesType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Equipment Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondEquipmentCapabilityTestSpecType(RespondEquipmentCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Equipment Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondEquipmentClassType(RespondEquipmentClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Equipment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondEquipmentInformationType(RespondEquipmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Equipment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondEquipmentType(RespondEquipmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Material Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondMaterialClassType(RespondMaterialClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Material Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondMaterialDefinitionType(RespondMaterialDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Material Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondMaterialInformationType(RespondMaterialInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Material Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondMaterialLotType(RespondMaterialLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Material Sub Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondMaterialSubLotType(RespondMaterialSubLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Material Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondMaterialTestSpecType(RespondMaterialTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Operations Capability Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondOperationsCapabilityInformationType(RespondOperationsCapabilityInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Operations Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondOperationsCapabilityType(RespondOperationsCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Operations Definition Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondOperationsDefinitionInformationType(RespondOperationsDefinitionInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Operations Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondOperationsDefinitionType(RespondOperationsDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Operations Performance Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondOperationsPerformanceType(RespondOperationsPerformanceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Operations Schedule Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondOperationsScheduleType(RespondOperationsScheduleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Personnel Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondPersonnelClassType(RespondPersonnelClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Personnel Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondPersonnelInformationType(RespondPersonnelInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Person Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondPersonType(RespondPersonType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Physical Asset Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondPhysicalAssetCapabilityTestSpecType(RespondPhysicalAssetCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Physical Asset Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondPhysicalAssetClassType(RespondPhysicalAssetClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Physical Asset Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondPhysicalAssetInformationType(RespondPhysicalAssetInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Physical Asset Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondPhysicalAssetType(RespondPhysicalAssetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Process Segment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondProcessSegmentInformationType(RespondProcessSegmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Process Segment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondProcessSegmentType(RespondProcessSegmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Respond Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Respond Qualification Test Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRespondQualificationTestSpecificationType(RespondQualificationTestSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Response State1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Response State1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponseState1Type(ResponseState1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Response State Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Response State Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponseStateType(ResponseStateType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Result Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Result Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResultType(ResultType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Segment Dependency Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Segment Dependency Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSegmentDependencyType(SegmentDependencyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Equipment Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowEquipmentCapabilityTestSpecType(ShowEquipmentCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Equipment Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowEquipmentClassType(ShowEquipmentClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Equipment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowEquipmentInformationType(ShowEquipmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Equipment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowEquipmentType(ShowEquipmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Material Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowMaterialClassType(ShowMaterialClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Material Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowMaterialDefinitionType(ShowMaterialDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Material Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowMaterialInformationType(ShowMaterialInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Material Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowMaterialLotType(ShowMaterialLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Material Sub Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowMaterialSubLotType(ShowMaterialSubLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Material Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowMaterialTestSpecType(ShowMaterialTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Operations Capability Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowOperationsCapabilityInformationType(ShowOperationsCapabilityInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Operations Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowOperationsCapabilityType(ShowOperationsCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Operations Definition Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowOperationsDefinitionInformationType(ShowOperationsDefinitionInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Operations Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowOperationsDefinitionType(ShowOperationsDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Operations Performance Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowOperationsPerformanceType(ShowOperationsPerformanceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Operations Schedule Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowOperationsScheduleType(ShowOperationsScheduleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Personnel Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowPersonnelClassType(ShowPersonnelClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Personnel Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowPersonnelInformationType(ShowPersonnelInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Person Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowPersonType(ShowPersonType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Physical Asset Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowPhysicalAssetCapabilityTestSpecType(ShowPhysicalAssetCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Physical Asset Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowPhysicalAssetClassType(ShowPhysicalAssetClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Physical Asset Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowPhysicalAssetInformationType(ShowPhysicalAssetInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Physical Asset Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowPhysicalAssetType(ShowPhysicalAssetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Process Segment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowProcessSegmentInformationType(ShowProcessSegmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Process Segment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowProcessSegmentType(ShowProcessSegmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Qualification Test Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowQualificationTestSpecificationType(ShowQualificationTestSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Start Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Start Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStartTimeType(StartTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Status Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Status Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStatusTimeType(StatusTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Status Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Status Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStatusType(StatusType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Storage Hierarchy Scope Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Storage Hierarchy Scope Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStorageHierarchyScopeType(StorageHierarchyScopeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Storage Location Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Storage Location Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStorageLocationType(StorageLocationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Equipment Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncEquipmentCapabilityTestSpecType(SyncEquipmentCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Equipment Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncEquipmentClassType(SyncEquipmentClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Equipment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncEquipmentInformationType(SyncEquipmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Equipment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncEquipmentType(SyncEquipmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Material Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncMaterialClassType(SyncMaterialClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Material Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncMaterialDefinitionType(SyncMaterialDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Material Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncMaterialInformationType(SyncMaterialInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Material Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncMaterialLotType(SyncMaterialLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Material Sub Lot Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncMaterialSubLotType(SyncMaterialSubLotType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Material Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncMaterialTestSpecType(SyncMaterialTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Operations Capability Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncOperationsCapabilityInformationType(SyncOperationsCapabilityInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Operations Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncOperationsCapabilityType(SyncOperationsCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Operations Definition Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncOperationsDefinitionInformationType(SyncOperationsDefinitionInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Operations Definition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncOperationsDefinitionType(SyncOperationsDefinitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Operations Performance Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncOperationsPerformanceType(SyncOperationsPerformanceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Operations Schedule Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncOperationsScheduleType(SyncOperationsScheduleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Personnel Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncPersonnelClassType(SyncPersonnelClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Personnel Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncPersonnelInformationType(SyncPersonnelInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Person Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncPersonType(SyncPersonType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Physical Asset Capability Test Spec Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncPhysicalAssetCapabilityTestSpecType(SyncPhysicalAssetCapabilityTestSpecType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Physical Asset Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncPhysicalAssetClassType(SyncPhysicalAssetClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Physical Asset Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncPhysicalAssetInformationType(SyncPhysicalAssetInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Physical Asset Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncPhysicalAssetType(SyncPhysicalAssetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Process Segment Information Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncProcessSegmentInformationType(SyncProcessSegmentInformationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Process Segment Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncProcessSegmentType(SyncProcessSegmentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Qualification Test Specification Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncQualificationTestSpecificationType(SyncQualificationTestSpecificationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Test Date Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Test Date Time Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestDateTimeType(TestDateTimeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tested Equipment Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tested Equipment Class Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestedEquipmentClassPropertyType(TestedEquipmentClassPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tested Equipment Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tested Equipment Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestedEquipmentPropertyType(TestedEquipmentPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tested Material Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tested Material Class Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestedMaterialClassPropertyType(TestedMaterialClassPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tested Material Definition Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tested Material Definition Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestedMaterialDefinitionPropertyType(TestedMaterialDefinitionPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tested Material Lot Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tested Material Lot Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestedMaterialLotPropertyType(TestedMaterialLotPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tested Personnel Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tested Personnel Class Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestedPersonnelClassPropertyType(TestedPersonnelClassPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tested Person Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tested Person Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestedPersonPropertyType(TestedPersonPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tested Physical Asset Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tested Physical Asset Class Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestedPhysicalAssetClassPropertyType(TestedPhysicalAssetClassPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tested Physical Asset Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tested Physical Asset Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestedPhysicalAssetPropertyType(TestedPhysicalAssetPropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Test Result Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Test Result Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestResultType(TestResultType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Text Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Text Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTextType(TextType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Acknowledge Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Acknowledge Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransAcknowledgeType(TransAcknowledgeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Action Criteria Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Action Criteria Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransActionCriteriaType(TransActionCriteriaType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Application Area Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Application Area Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransApplicationAreaType(TransApplicationAreaType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Cancel Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Cancel Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransCancelType(TransCancelType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Change Status Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Change Status Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransChangeStatusType(TransChangeStatusType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Change Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Change Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransChangeType(TransChangeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Confirmation Code Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Confirmation Code Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransConfirmationCodeType(TransConfirmationCodeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Confirm Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Confirm Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransConfirmType(TransConfirmType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Expression1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Expression1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransExpression1Type(TransExpression1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Expression Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Expression Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransExpressionType(TransExpressionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Get Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Get Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransGetType(TransGetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Process Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Process Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransProcessType(TransProcessType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Receiver Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Receiver Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransReceiverType(TransReceiverType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Respond Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Respond Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransRespondType(TransRespondType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Response Criteria Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Response Criteria Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransResponseCriteriaType(TransResponseCriteriaType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Sender Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Sender Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransSenderType(TransSenderType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Show Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Show Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransShowType(TransShowType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Signature Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Signature Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransSignatureType(TransSignatureType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans State Change Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans State Change Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransStateChangeType(TransStateChangeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans Sync Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans Sync Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransSyncType(TransSyncType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trans User Area Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trans User Area Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransUserAreaType(TransUserAreaType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unit Of Measure Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unit Of Measure Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnitOfMeasureType(UnitOfMeasureType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value String Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value String Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValueStringType(ValueStringType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValueType(ValueType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Version Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Version Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVersionType(VersionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Work Request ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Work Request ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorkRequestIDType(WorkRequestIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Work Schedule ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Work Schedule ID Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorkScheduleIDType(WorkScheduleIDType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Work Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Work Type1 Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorkType1Type(WorkType1Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Work Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Work Type Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorkTypeType(WorkTypeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //B2MMLSwitch
