/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equipment Capability Test Specification ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getEquipmentCapabilityTestSpecificationIDType()
 * @model extendedMetaData="name='EquipmentCapabilityTestSpecificationIDType' kind='simple'"
 * @generated
 */
public interface EquipmentCapabilityTestSpecificationIDType extends IdentifierType {
} // EquipmentCapabilityTestSpecificationIDType
