/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Asset Property Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetPropertyType#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetPropertyType#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetPropertyType#getValue <em>Value</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetPropertyType#getPhysicalAssetProperty <em>Physical Asset Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetPropertyType#getPhysicalAssetCapabilityTestSpecificationID <em>Physical Asset Capability Test Specification ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.PhysicalAssetPropertyType#getTestResult <em>Test Result</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetPropertyType()
 * @model extendedMetaData="name='PhysicalAssetPropertyType' kind='elementOnly'"
 * @generated
 */
public interface PhysicalAssetPropertyType extends EObject {
	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference.
	 * @see #setID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetPropertyType_ID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.PhysicalAssetPropertyType#getID <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' containment reference.
	 * @see #getID()
	 * @generated
	 */
	void setID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.DescriptionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetPropertyType_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Description' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DescriptionType> getDescription();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.ValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetPropertyType_Value()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Value' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ValueType> getValue();

	/**
	 * Returns the value of the '<em><b>Physical Asset Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PhysicalAssetPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Property</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetPropertyType_PhysicalAssetProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetPropertyType> getPhysicalAssetProperty();

	/**
	 * Returns the value of the '<em><b>Physical Asset Capability Test Specification ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.PhysicalAssetCapabilityTestSpecificationIDType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Capability Test Specification ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Capability Test Specification ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetPropertyType_PhysicalAssetCapabilityTestSpecificationID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetCapabilityTestSpecificationID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PhysicalAssetCapabilityTestSpecificationIDType> getPhysicalAssetCapabilityTestSpecificationID();

	/**
	 * Returns the value of the '<em><b>Test Result</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.TestResultType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Result</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Result</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPhysicalAssetPropertyType_TestResult()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TestResult' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TestResultType> getTestResult();

} // PhysicalAssetPropertyType
