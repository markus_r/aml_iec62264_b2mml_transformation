/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operations Segment ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getOperationsSegmentIDType()
 * @model extendedMetaData="name='OperationsSegmentIDType' kind='simple'"
 * @generated
 */
public interface OperationsSegmentIDType extends IdentifierType {
} // OperationsSegmentIDType
