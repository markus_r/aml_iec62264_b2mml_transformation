/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Test Specification ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getMaterialTestSpecificationIDType()
 * @model extendedMetaData="name='MaterialTestSpecificationIDType' kind='simple'"
 * @generated
 */
public interface MaterialTestSpecificationIDType extends IdentifierType {
} // MaterialTestSpecificationIDType
