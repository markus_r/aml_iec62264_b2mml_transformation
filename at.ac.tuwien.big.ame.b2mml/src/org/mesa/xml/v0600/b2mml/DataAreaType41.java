/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type41</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType41#getShow <em>Show</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType41#getOperationsDefinition <em>Operations Definition</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType41()
 * @model extendedMetaData="name='DataArea_._42_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType41 extends EObject {
	/**
	 * Returns the value of the '<em><b>Show</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show</em>' containment reference.
	 * @see #setShow(TransShowType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType41_Show()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Show' namespace='##targetNamespace'"
	 * @generated
	 */
	TransShowType getShow();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DataAreaType41#getShow <em>Show</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show</em>' containment reference.
	 * @see #getShow()
	 * @generated
	 */
	void setShow(TransShowType value);

	/**
	 * Returns the value of the '<em><b>Operations Definition</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OperationsDefinitionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType41_OperationsDefinition()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OperationsDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsDefinitionType> getOperationsDefinition();

} // DataAreaType41
