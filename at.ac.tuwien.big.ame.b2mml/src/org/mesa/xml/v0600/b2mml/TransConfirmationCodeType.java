/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Confirmation Code Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransConfirmationCodeType()
 * @model extendedMetaData="name='TransConfirmationCodeType' kind='simple'"
 * @generated
 */
public interface TransConfirmationCodeType extends CodeType {
} // TransConfirmationCodeType
