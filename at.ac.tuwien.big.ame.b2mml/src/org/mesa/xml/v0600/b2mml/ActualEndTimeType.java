/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actual End Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getActualEndTimeType()
 * @model extendedMetaData="name='ActualEndTimeType' kind='simple'"
 * @generated
 */
public interface ActualEndTimeType extends DateTimeType {
} // ActualEndTimeType
