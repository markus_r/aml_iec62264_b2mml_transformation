/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Personnel Class ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPersonnelClassIDType()
 * @model extendedMetaData="name='PersonnelClassIDType' kind='simple'"
 * @generated
 */
public interface PersonnelClassIDType extends IdentifierType {
} // PersonnelClassIDType
