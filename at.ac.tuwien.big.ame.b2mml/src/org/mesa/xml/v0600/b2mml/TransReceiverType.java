/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Receiver Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransReceiverType#getLogicalID <em>Logical ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransReceiverType#getComponentID <em>Component ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransReceiverType#getID <em>ID</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransReceiverType()
 * @model extendedMetaData="name='TransReceiverType' kind='elementOnly'"
 * @generated
 */
public interface TransReceiverType extends EObject {
	/**
	 * Returns the value of the '<em><b>Logical ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logical ID</em>' containment reference.
	 * @see #setLogicalID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransReceiverType_LogicalID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LogicalID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getLogicalID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TransReceiverType#getLogicalID <em>Logical ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Logical ID</em>' containment reference.
	 * @see #getLogicalID()
	 * @generated
	 */
	void setLogicalID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>Component ID</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component ID</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component ID</em>' containment reference.
	 * @see #setComponentID(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransReceiverType_ComponentID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ComponentID' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getComponentID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TransReceiverType#getComponentID <em>Component ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component ID</em>' containment reference.
	 * @see #getComponentID()
	 * @generated
	 */
	void setComponentID(IdentifierType value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.IdentifierType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransReceiverType_ID()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<IdentifierType> getID();

} // TransReceiverType
