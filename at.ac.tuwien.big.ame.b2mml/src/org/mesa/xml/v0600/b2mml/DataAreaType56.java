/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type56</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType56#getSync <em>Sync</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType56#getOperationsSchedule <em>Operations Schedule</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType56()
 * @model extendedMetaData="name='DataArea_._57_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType56 extends EObject {
	/**
	 * Returns the value of the '<em><b>Sync</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync</em>' containment reference.
	 * @see #setSync(TransSyncType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType56_Sync()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Sync' namespace='##targetNamespace'"
	 * @generated
	 */
	TransSyncType getSync();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DataAreaType56#getSync <em>Sync</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync</em>' containment reference.
	 * @see #getSync()
	 * @generated
	 */
	void setSync(TransSyncType value);

	/**
	 * Returns the value of the '<em><b>Operations Schedule</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OperationsScheduleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Schedule</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Schedule</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType56_OperationsSchedule()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OperationsSchedule' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsScheduleType> getOperationsSchedule();

} // DataAreaType56
