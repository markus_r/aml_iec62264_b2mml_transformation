/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Area Type128</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType128#getChange <em>Change</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DataAreaType128#getOperationsDefinitionInformation <em>Operations Definition Information</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType128()
 * @model extendedMetaData="name='DataArea_._129_._type' kind='elementOnly'"
 * @generated
 */
public interface DataAreaType128 extends EObject {
	/**
	 * Returns the value of the '<em><b>Change</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change</em>' containment reference.
	 * @see #setChange(TransChangeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType128_Change()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Change' namespace='##targetNamespace'"
	 * @generated
	 */
	TransChangeType getChange();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DataAreaType128#getChange <em>Change</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change</em>' containment reference.
	 * @see #getChange()
	 * @generated
	 */
	void setChange(TransChangeType value);

	/**
	 * Returns the value of the '<em><b>Operations Definition Information</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.OperationsDefinitionInformationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition Information</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition Information</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDataAreaType128_OperationsDefinitionInformation()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OperationsDefinitionInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationsDefinitionInformationType> getOperationsDefinitionInformation();

} // DataAreaType128
