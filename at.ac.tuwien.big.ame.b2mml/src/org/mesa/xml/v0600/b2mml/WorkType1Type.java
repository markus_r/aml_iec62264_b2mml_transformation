/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Work Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getWorkType1Type()
 * @model extendedMetaData="name='WorkType1Type' kind='simple'"
 * @generated
 */
public interface WorkType1Type extends CodeType {
} // WorkType1Type
