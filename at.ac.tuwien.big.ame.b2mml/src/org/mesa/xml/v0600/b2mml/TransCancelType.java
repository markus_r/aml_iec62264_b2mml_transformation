/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Cancel Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransCancelType#getActionCriteria <em>Action Criteria</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransCancelType()
 * @model extendedMetaData="name='TransCancelType' kind='elementOnly'"
 * @generated
 */
public interface TransCancelType extends EObject {
	/**
	 * Returns the value of the '<em><b>Action Criteria</b></em>' containment reference list.
	 * The list contents are of type {@link org.mesa.xml.v0600.b2mml.TransActionCriteriaType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action Criteria</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Criteria</em>' containment reference list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransCancelType_ActionCriteria()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ActionCriteria' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TransActionCriteriaType> getActionCriteria();

} // TransCancelType
