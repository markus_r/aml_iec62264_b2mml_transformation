/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getPersonIDType()
 * @model extendedMetaData="name='PersonIDType' kind='simple'"
 * @generated
 */
public interface PersonIDType extends IdentifierType {
} // PersonIDType
