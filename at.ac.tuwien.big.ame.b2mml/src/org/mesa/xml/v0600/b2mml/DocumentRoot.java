/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMixed <em>Mixed</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeEquipment <em>Acknowledge Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeEquipmentCapabilityTestSpec <em>Acknowledge Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeEquipmentClass <em>Acknowledge Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeEquipmentInformation <em>Acknowledge Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeMaterialClass <em>Acknowledge Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeMaterialDefinition <em>Acknowledge Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeMaterialInformation <em>Acknowledge Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeMaterialLot <em>Acknowledge Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeMaterialSubLot <em>Acknowledge Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeMaterialTestSpec <em>Acknowledge Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeOperationsCapability <em>Acknowledge Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeOperationsCapabilityInformation <em>Acknowledge Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeOperationsDefinition <em>Acknowledge Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeOperationsDefinitionInformation <em>Acknowledge Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeOperationsPerformance <em>Acknowledge Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeOperationsSchedule <em>Acknowledge Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePerson <em>Acknowledge Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePersonnelClass <em>Acknowledge Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePersonnelInformation <em>Acknowledge Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePhysicalAsset <em>Acknowledge Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePhysicalAssetCapabilityTestSpec <em>Acknowledge Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePhysicalAssetClass <em>Acknowledge Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePhysicalAssetInformation <em>Acknowledge Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeProcessSegment <em>Acknowledge Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeProcessSegmentInformation <em>Acknowledge Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeQualificationTestSpecification <em>Acknowledge Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelEquipment <em>Cancel Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelEquipmentCapabilityTestSpec <em>Cancel Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelEquipmentClass <em>Cancel Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelEquipmentInformation <em>Cancel Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelMaterialClass <em>Cancel Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelMaterialDefinition <em>Cancel Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelMaterialInformation <em>Cancel Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelMaterialLot <em>Cancel Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelMaterialSubLot <em>Cancel Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelMaterialTestSpec <em>Cancel Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelOperationsCapability <em>Cancel Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelOperationsCapabilityInformation <em>Cancel Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelOperationsDefinition <em>Cancel Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelOperationsDefinitionInformation <em>Cancel Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelOperationsPerformance <em>Cancel Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelOperationsSchedule <em>Cancel Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPerson <em>Cancel Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPersonnelClass <em>Cancel Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPersonnelInformation <em>Cancel Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPhysicalAsset <em>Cancel Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPhysicalAssetCapabilityTestSpec <em>Cancel Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPhysicalAssetClass <em>Cancel Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPhysicalAssetInformation <em>Cancel Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelProcessSegment <em>Cancel Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelProcessSegmentInformation <em>Cancel Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelQualificationTestSpecification <em>Cancel Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeEquipment <em>Change Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeEquipmentCapabilityTestSpec <em>Change Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeEquipmentClass <em>Change Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeEquipmentInformation <em>Change Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeMaterialClass <em>Change Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeMaterialDefinition <em>Change Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeMaterialInformation <em>Change Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeMaterialLot <em>Change Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeMaterialSubLot <em>Change Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeMaterialTestSpec <em>Change Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeOperationsCapability <em>Change Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeOperationsCapabilityInformation <em>Change Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeOperationsDefinition <em>Change Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeOperationsDefinitionInformation <em>Change Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeOperationsPerformance <em>Change Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeOperationsSchedule <em>Change Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePerson <em>Change Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePersonnelClass <em>Change Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePersonnelInformation <em>Change Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePhysicalAsset <em>Change Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePhysicalAssetCapabilityTestSpec <em>Change Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePhysicalAssetClass <em>Change Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePhysicalAssetInformation <em>Change Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeProcessSegment <em>Change Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeProcessSegmentInformation <em>Change Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeQualificationTestSpecification <em>Change Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getConfirmBOD <em>Confirm BOD</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getEquipment <em>Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getEquipmentCapabilityTestSpecification <em>Equipment Capability Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getEquipmentClass <em>Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getEquipmentInformation <em>Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetEquipment <em>Get Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetEquipmentCapabilityTestSpec <em>Get Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetEquipmentClass <em>Get Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetEquipmentInformation <em>Get Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetMaterialClass <em>Get Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetMaterialDefinition <em>Get Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetMaterialInformation <em>Get Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetMaterialLot <em>Get Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetMaterialSubLot <em>Get Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetMaterialTestSpec <em>Get Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetOperationsCapability <em>Get Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetOperationsCapabilityInformation <em>Get Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetOperationsDefinition <em>Get Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetOperationsDefinitionInformation <em>Get Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetOperationsPerformance <em>Get Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetOperationsSchedule <em>Get Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPerson <em>Get Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPersonnelClass <em>Get Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPersonnelInformation <em>Get Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPhysicalAsset <em>Get Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPhysicalAssetCapabilityTestSpec <em>Get Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPhysicalAssetClass <em>Get Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPhysicalAssetInformation <em>Get Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetProcessSegment <em>Get Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetProcessSegmentInformation <em>Get Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetQualificationTestSpecification <em>Get Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMaterialClass <em>Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMaterialDefinition <em>Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMaterialInformation <em>Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMaterialLot <em>Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMaterialSubLot <em>Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMaterialTestSpecification <em>Material Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsCapability <em>Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsCapabilityInformation <em>Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsDefinition <em>Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsDefinitionInformation <em>Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsPerformance <em>Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsRequest <em>Operations Request</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsResponse <em>Operations Response</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsSchedule <em>Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPerson <em>Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPersonnelClass <em>Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPersonnelInformation <em>Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPhysicalAsset <em>Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPhysicalAssetCapabilityTestSpecification <em>Physical Asset Capability Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPhysicalAssetClass <em>Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPhysicalAssetInformation <em>Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessEquipment <em>Process Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessEquipmentCapabilityTestSpec <em>Process Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessEquipmentClass <em>Process Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessEquipmentInformation <em>Process Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessMaterialClass <em>Process Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessMaterialDefinition <em>Process Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessMaterialInformation <em>Process Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessMaterialLot <em>Process Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessMaterialSubLot <em>Process Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessMaterialTestSpec <em>Process Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessOperationsCapability <em>Process Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessOperationsCapabilityInformation <em>Process Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessOperationsDefinition <em>Process Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessOperationsDefinitionInformation <em>Process Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessOperationsPerformance <em>Process Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessOperationsSchedule <em>Process Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPerson <em>Process Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPersonnelClass <em>Process Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPersonnelInformation <em>Process Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPhysicalAsset <em>Process Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPhysicalAssetCapabilityTestSpec <em>Process Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPhysicalAssetClass <em>Process Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPhysicalAssetInformation <em>Process Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessProcessSegment <em>Process Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessProcessSegmentInformation <em>Process Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessQualificationTestSpecification <em>Process Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessSegment <em>Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessSegmentInformation <em>Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getQualificationTestSpecification <em>Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondEquipment <em>Respond Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondEquipmentCapabilityTestSpec <em>Respond Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondEquipmentClass <em>Respond Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondEquipmentInformation <em>Respond Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondMaterialClass <em>Respond Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondMaterialDefinition <em>Respond Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondMaterialInformation <em>Respond Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondMaterialLot <em>Respond Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondMaterialSubLot <em>Respond Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondMaterialTestSpec <em>Respond Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondOperationsCapability <em>Respond Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondOperationsCapabilityInformation <em>Respond Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondOperationsDefinition <em>Respond Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondOperationsDefinitionInformation <em>Respond Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondOperationsPerformance <em>Respond Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondOperationsSchedule <em>Respond Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPerson <em>Respond Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPersonnelClass <em>Respond Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPersonnelInformation <em>Respond Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPhysicalAsset <em>Respond Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPhysicalAssetCapabilityTestSpec <em>Respond Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPhysicalAssetClass <em>Respond Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPhysicalAssetInformation <em>Respond Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondProcessSegment <em>Respond Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondProcessSegmentInformation <em>Respond Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondQualificationTestSpecification <em>Respond Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowEquipment <em>Show Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowEquipmentCapabilityTestSpec <em>Show Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowEquipmentClass <em>Show Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowEquipmentInformation <em>Show Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowMaterialClass <em>Show Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowMaterialDefinition <em>Show Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowMaterialInformation <em>Show Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowMaterialLot <em>Show Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowMaterialSubLot <em>Show Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowMaterialTestSpec <em>Show Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowOperationsCapability <em>Show Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowOperationsCapabilityInformation <em>Show Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowOperationsDefinition <em>Show Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowOperationsDefinitionInformation <em>Show Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowOperationsPerformance <em>Show Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowOperationsSchedule <em>Show Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPerson <em>Show Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPersonnelClass <em>Show Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPersonnelInformation <em>Show Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPhysicalAsset <em>Show Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPhysicalAssetCapabilityTestSpec <em>Show Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPhysicalAssetClass <em>Show Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPhysicalAssetInformation <em>Show Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowProcessSegment <em>Show Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowProcessSegmentInformation <em>Show Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowQualificationTestSpecification <em>Show Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncEquipment <em>Sync Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncEquipmentCapabilityTestSpec <em>Sync Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncEquipmentClass <em>Sync Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncEquipmentInformation <em>Sync Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncMaterialClass <em>Sync Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncMaterialDefinition <em>Sync Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncMaterialInformation <em>Sync Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncMaterialLot <em>Sync Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncMaterialSubLot <em>Sync Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncMaterialTestSpec <em>Sync Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncOperationsCapability <em>Sync Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncOperationsCapabilityInformation <em>Sync Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncOperationsDefinition <em>Sync Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncOperationsDefinitionInformation <em>Sync Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncOperationsPerformance <em>Sync Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncOperationsSchedule <em>Sync Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPerson <em>Sync Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPersonnelClass <em>Sync Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPersonnelInformation <em>Sync Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPhysicalAsset <em>Sync Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPhysicalAssetCapabilityTestSpec <em>Sync Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPhysicalAssetClass <em>Sync Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPhysicalAssetInformation <em>Sync Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncProcessSegment <em>Sync Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncProcessSegmentInformation <em>Sync Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncQualificationTestSpecification <em>Sync Qualification Test Specification</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot()
 * @model extendedMetaData="name='' kind='mixed'"
 * @generated
 */
public interface DocumentRoot extends EObject {
	/**
	 * Returns the value of the '<em><b>Mixed</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mixed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mixed</em>' attribute list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_Mixed()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' name=':mixed'"
	 * @generated
	 */
	FeatureMap getMixed();

	/**
	 * Returns the value of the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XMLNS Prefix Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XMLNS Prefix Map</em>' map.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_XMLNSPrefixMap()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;" transient="true"
	 *        extendedMetaData="kind='attribute' name='xmlns:prefix'"
	 * @generated
	 */
	EMap<String, String> getXMLNSPrefixMap();

	/**
	 * Returns the value of the '<em><b>XSI Schema Location</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XSI Schema Location</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XSI Schema Location</em>' map.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_XSISchemaLocation()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;" transient="true"
	 *        extendedMetaData="kind='attribute' name='xsi:schemaLocation'"
	 * @generated
	 */
	EMap<String, String> getXSISchemaLocation();

	/**
	 * Returns the value of the '<em><b>Acknowledge Equipment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Equipment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Equipment</em>' containment reference.
	 * @see #setAcknowledgeEquipment(AcknowledgeEquipmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeEquipment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeEquipment' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeEquipmentType getAcknowledgeEquipment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeEquipment <em>Acknowledge Equipment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Equipment</em>' containment reference.
	 * @see #getAcknowledgeEquipment()
	 * @generated
	 */
	void setAcknowledgeEquipment(AcknowledgeEquipmentType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Equipment Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Equipment Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Equipment Capability Test Spec</em>' containment reference.
	 * @see #setAcknowledgeEquipmentCapabilityTestSpec(AcknowledgeEquipmentCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeEquipmentCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeEquipmentCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeEquipmentCapabilityTestSpecType getAcknowledgeEquipmentCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeEquipmentCapabilityTestSpec <em>Acknowledge Equipment Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Equipment Capability Test Spec</em>' containment reference.
	 * @see #getAcknowledgeEquipmentCapabilityTestSpec()
	 * @generated
	 */
	void setAcknowledgeEquipmentCapabilityTestSpec(AcknowledgeEquipmentCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Equipment Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Equipment Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Equipment Class</em>' containment reference.
	 * @see #setAcknowledgeEquipmentClass(AcknowledgeEquipmentClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeEquipmentClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeEquipmentClass' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeEquipmentClassType getAcknowledgeEquipmentClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeEquipmentClass <em>Acknowledge Equipment Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Equipment Class</em>' containment reference.
	 * @see #getAcknowledgeEquipmentClass()
	 * @generated
	 */
	void setAcknowledgeEquipmentClass(AcknowledgeEquipmentClassType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Equipment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Equipment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Equipment Information</em>' containment reference.
	 * @see #setAcknowledgeEquipmentInformation(AcknowledgeEquipmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeEquipmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeEquipmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeEquipmentInformationType getAcknowledgeEquipmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeEquipmentInformation <em>Acknowledge Equipment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Equipment Information</em>' containment reference.
	 * @see #getAcknowledgeEquipmentInformation()
	 * @generated
	 */
	void setAcknowledgeEquipmentInformation(AcknowledgeEquipmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Material Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Material Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Material Class</em>' containment reference.
	 * @see #setAcknowledgeMaterialClass(AcknowledgeMaterialClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeMaterialClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeMaterialClass' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeMaterialClassType getAcknowledgeMaterialClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeMaterialClass <em>Acknowledge Material Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Material Class</em>' containment reference.
	 * @see #getAcknowledgeMaterialClass()
	 * @generated
	 */
	void setAcknowledgeMaterialClass(AcknowledgeMaterialClassType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Material Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Material Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Material Definition</em>' containment reference.
	 * @see #setAcknowledgeMaterialDefinition(AcknowledgeMaterialDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeMaterialDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeMaterialDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeMaterialDefinitionType getAcknowledgeMaterialDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeMaterialDefinition <em>Acknowledge Material Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Material Definition</em>' containment reference.
	 * @see #getAcknowledgeMaterialDefinition()
	 * @generated
	 */
	void setAcknowledgeMaterialDefinition(AcknowledgeMaterialDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Material Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Material Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Material Information</em>' containment reference.
	 * @see #setAcknowledgeMaterialInformation(AcknowledgeMaterialInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeMaterialInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeMaterialInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeMaterialInformationType getAcknowledgeMaterialInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeMaterialInformation <em>Acknowledge Material Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Material Information</em>' containment reference.
	 * @see #getAcknowledgeMaterialInformation()
	 * @generated
	 */
	void setAcknowledgeMaterialInformation(AcknowledgeMaterialInformationType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Material Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Material Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Material Lot</em>' containment reference.
	 * @see #setAcknowledgeMaterialLot(AcknowledgeMaterialLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeMaterialLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeMaterialLot' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeMaterialLotType getAcknowledgeMaterialLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeMaterialLot <em>Acknowledge Material Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Material Lot</em>' containment reference.
	 * @see #getAcknowledgeMaterialLot()
	 * @generated
	 */
	void setAcknowledgeMaterialLot(AcknowledgeMaterialLotType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Material Sub Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Material Sub Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Material Sub Lot</em>' containment reference.
	 * @see #setAcknowledgeMaterialSubLot(AcknowledgeMaterialSubLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeMaterialSubLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeMaterialSubLot' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeMaterialSubLotType getAcknowledgeMaterialSubLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeMaterialSubLot <em>Acknowledge Material Sub Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Material Sub Lot</em>' containment reference.
	 * @see #getAcknowledgeMaterialSubLot()
	 * @generated
	 */
	void setAcknowledgeMaterialSubLot(AcknowledgeMaterialSubLotType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Material Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Material Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Material Test Spec</em>' containment reference.
	 * @see #setAcknowledgeMaterialTestSpec(AcknowledgeMaterialTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeMaterialTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeMaterialTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeMaterialTestSpecType getAcknowledgeMaterialTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeMaterialTestSpec <em>Acknowledge Material Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Material Test Spec</em>' containment reference.
	 * @see #getAcknowledgeMaterialTestSpec()
	 * @generated
	 */
	void setAcknowledgeMaterialTestSpec(AcknowledgeMaterialTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Operations Capability</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Operations Capability</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Operations Capability</em>' containment reference.
	 * @see #setAcknowledgeOperationsCapability(AcknowledgeOperationsCapabilityType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeOperationsCapability()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeOperationsCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeOperationsCapabilityType getAcknowledgeOperationsCapability();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeOperationsCapability <em>Acknowledge Operations Capability</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Operations Capability</em>' containment reference.
	 * @see #getAcknowledgeOperationsCapability()
	 * @generated
	 */
	void setAcknowledgeOperationsCapability(AcknowledgeOperationsCapabilityType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Operations Capability Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Operations Capability Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Operations Capability Information</em>' containment reference.
	 * @see #setAcknowledgeOperationsCapabilityInformation(AcknowledgeOperationsCapabilityInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeOperationsCapabilityInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeOperationsCapabilityInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeOperationsCapabilityInformationType getAcknowledgeOperationsCapabilityInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeOperationsCapabilityInformation <em>Acknowledge Operations Capability Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Operations Capability Information</em>' containment reference.
	 * @see #getAcknowledgeOperationsCapabilityInformation()
	 * @generated
	 */
	void setAcknowledgeOperationsCapabilityInformation(AcknowledgeOperationsCapabilityInformationType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Operations Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Operations Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Operations Definition</em>' containment reference.
	 * @see #setAcknowledgeOperationsDefinition(AcknowledgeOperationsDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeOperationsDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeOperationsDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeOperationsDefinitionType getAcknowledgeOperationsDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeOperationsDefinition <em>Acknowledge Operations Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Operations Definition</em>' containment reference.
	 * @see #getAcknowledgeOperationsDefinition()
	 * @generated
	 */
	void setAcknowledgeOperationsDefinition(AcknowledgeOperationsDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Operations Definition Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Operations Definition Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Operations Definition Information</em>' containment reference.
	 * @see #setAcknowledgeOperationsDefinitionInformation(AcknowledgeOperationsDefinitionInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeOperationsDefinitionInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeOperationsDefinitionInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeOperationsDefinitionInformationType getAcknowledgeOperationsDefinitionInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeOperationsDefinitionInformation <em>Acknowledge Operations Definition Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Operations Definition Information</em>' containment reference.
	 * @see #getAcknowledgeOperationsDefinitionInformation()
	 * @generated
	 */
	void setAcknowledgeOperationsDefinitionInformation(AcknowledgeOperationsDefinitionInformationType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Operations Performance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Operations Performance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Operations Performance</em>' containment reference.
	 * @see #setAcknowledgeOperationsPerformance(AcknowledgeOperationsPerformanceType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeOperationsPerformance()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeOperationsPerformance' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeOperationsPerformanceType getAcknowledgeOperationsPerformance();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeOperationsPerformance <em>Acknowledge Operations Performance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Operations Performance</em>' containment reference.
	 * @see #getAcknowledgeOperationsPerformance()
	 * @generated
	 */
	void setAcknowledgeOperationsPerformance(AcknowledgeOperationsPerformanceType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Operations Schedule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Operations Schedule</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Operations Schedule</em>' containment reference.
	 * @see #setAcknowledgeOperationsSchedule(AcknowledgeOperationsScheduleType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeOperationsSchedule()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeOperationsSchedule' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeOperationsScheduleType getAcknowledgeOperationsSchedule();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeOperationsSchedule <em>Acknowledge Operations Schedule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Operations Schedule</em>' containment reference.
	 * @see #getAcknowledgeOperationsSchedule()
	 * @generated
	 */
	void setAcknowledgeOperationsSchedule(AcknowledgeOperationsScheduleType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Person</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Person</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Person</em>' containment reference.
	 * @see #setAcknowledgePerson(AcknowledgePersonType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgePerson()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgePerson' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgePersonType getAcknowledgePerson();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePerson <em>Acknowledge Person</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Person</em>' containment reference.
	 * @see #getAcknowledgePerson()
	 * @generated
	 */
	void setAcknowledgePerson(AcknowledgePersonType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Personnel Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Personnel Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Personnel Class</em>' containment reference.
	 * @see #setAcknowledgePersonnelClass(AcknowledgePersonnelClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgePersonnelClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgePersonnelClass' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgePersonnelClassType getAcknowledgePersonnelClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePersonnelClass <em>Acknowledge Personnel Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Personnel Class</em>' containment reference.
	 * @see #getAcknowledgePersonnelClass()
	 * @generated
	 */
	void setAcknowledgePersonnelClass(AcknowledgePersonnelClassType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Personnel Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Personnel Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Personnel Information</em>' containment reference.
	 * @see #setAcknowledgePersonnelInformation(AcknowledgePersonnelInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgePersonnelInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgePersonnelInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgePersonnelInformationType getAcknowledgePersonnelInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePersonnelInformation <em>Acknowledge Personnel Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Personnel Information</em>' containment reference.
	 * @see #getAcknowledgePersonnelInformation()
	 * @generated
	 */
	void setAcknowledgePersonnelInformation(AcknowledgePersonnelInformationType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Physical Asset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Physical Asset</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Physical Asset</em>' containment reference.
	 * @see #setAcknowledgePhysicalAsset(AcknowledgePhysicalAssetType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgePhysicalAsset()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgePhysicalAsset' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgePhysicalAssetType getAcknowledgePhysicalAsset();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePhysicalAsset <em>Acknowledge Physical Asset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Physical Asset</em>' containment reference.
	 * @see #getAcknowledgePhysicalAsset()
	 * @generated
	 */
	void setAcknowledgePhysicalAsset(AcknowledgePhysicalAssetType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Physical Asset Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Physical Asset Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #setAcknowledgePhysicalAssetCapabilityTestSpec(AcknowledgePhysicalAssetCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgePhysicalAssetCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgePhysicalAssetCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgePhysicalAssetCapabilityTestSpecType getAcknowledgePhysicalAssetCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePhysicalAssetCapabilityTestSpec <em>Acknowledge Physical Asset Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #getAcknowledgePhysicalAssetCapabilityTestSpec()
	 * @generated
	 */
	void setAcknowledgePhysicalAssetCapabilityTestSpec(AcknowledgePhysicalAssetCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Physical Asset Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Physical Asset Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Physical Asset Class</em>' containment reference.
	 * @see #setAcknowledgePhysicalAssetClass(AcknowledgePhysicalAssetClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgePhysicalAssetClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgePhysicalAssetClass' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgePhysicalAssetClassType getAcknowledgePhysicalAssetClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePhysicalAssetClass <em>Acknowledge Physical Asset Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Physical Asset Class</em>' containment reference.
	 * @see #getAcknowledgePhysicalAssetClass()
	 * @generated
	 */
	void setAcknowledgePhysicalAssetClass(AcknowledgePhysicalAssetClassType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Physical Asset Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Physical Asset Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Physical Asset Information</em>' containment reference.
	 * @see #setAcknowledgePhysicalAssetInformation(AcknowledgePhysicalAssetInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgePhysicalAssetInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgePhysicalAssetInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgePhysicalAssetInformationType getAcknowledgePhysicalAssetInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgePhysicalAssetInformation <em>Acknowledge Physical Asset Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Physical Asset Information</em>' containment reference.
	 * @see #getAcknowledgePhysicalAssetInformation()
	 * @generated
	 */
	void setAcknowledgePhysicalAssetInformation(AcknowledgePhysicalAssetInformationType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Process Segment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Process Segment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Process Segment</em>' containment reference.
	 * @see #setAcknowledgeProcessSegment(AcknowledgeProcessSegmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeProcessSegment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeProcessSegment' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeProcessSegmentType getAcknowledgeProcessSegment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeProcessSegment <em>Acknowledge Process Segment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Process Segment</em>' containment reference.
	 * @see #getAcknowledgeProcessSegment()
	 * @generated
	 */
	void setAcknowledgeProcessSegment(AcknowledgeProcessSegmentType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Process Segment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Process Segment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Process Segment Information</em>' containment reference.
	 * @see #setAcknowledgeProcessSegmentInformation(AcknowledgeProcessSegmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeProcessSegmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeProcessSegmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeProcessSegmentInformationType getAcknowledgeProcessSegmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeProcessSegmentInformation <em>Acknowledge Process Segment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Process Segment Information</em>' containment reference.
	 * @see #getAcknowledgeProcessSegmentInformation()
	 * @generated
	 */
	void setAcknowledgeProcessSegmentInformation(AcknowledgeProcessSegmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Acknowledge Qualification Test Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acknowledge Qualification Test Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acknowledge Qualification Test Specification</em>' containment reference.
	 * @see #setAcknowledgeQualificationTestSpecification(AcknowledgeQualificationTestSpecificationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_AcknowledgeQualificationTestSpecification()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AcknowledgeQualificationTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	AcknowledgeQualificationTestSpecificationType getAcknowledgeQualificationTestSpecification();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getAcknowledgeQualificationTestSpecification <em>Acknowledge Qualification Test Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Acknowledge Qualification Test Specification</em>' containment reference.
	 * @see #getAcknowledgeQualificationTestSpecification()
	 * @generated
	 */
	void setAcknowledgeQualificationTestSpecification(AcknowledgeQualificationTestSpecificationType value);

	/**
	 * Returns the value of the '<em><b>Cancel Equipment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Equipment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Equipment</em>' containment reference.
	 * @see #setCancelEquipment(CancelEquipmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelEquipment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelEquipment' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelEquipmentType getCancelEquipment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelEquipment <em>Cancel Equipment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Equipment</em>' containment reference.
	 * @see #getCancelEquipment()
	 * @generated
	 */
	void setCancelEquipment(CancelEquipmentType value);

	/**
	 * Returns the value of the '<em><b>Cancel Equipment Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Equipment Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Equipment Capability Test Spec</em>' containment reference.
	 * @see #setCancelEquipmentCapabilityTestSpec(CancelEquipmentCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelEquipmentCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelEquipmentCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelEquipmentCapabilityTestSpecType getCancelEquipmentCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelEquipmentCapabilityTestSpec <em>Cancel Equipment Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Equipment Capability Test Spec</em>' containment reference.
	 * @see #getCancelEquipmentCapabilityTestSpec()
	 * @generated
	 */
	void setCancelEquipmentCapabilityTestSpec(CancelEquipmentCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Cancel Equipment Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Equipment Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Equipment Class</em>' containment reference.
	 * @see #setCancelEquipmentClass(CancelEquipmentClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelEquipmentClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelEquipmentClass' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelEquipmentClassType getCancelEquipmentClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelEquipmentClass <em>Cancel Equipment Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Equipment Class</em>' containment reference.
	 * @see #getCancelEquipmentClass()
	 * @generated
	 */
	void setCancelEquipmentClass(CancelEquipmentClassType value);

	/**
	 * Returns the value of the '<em><b>Cancel Equipment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Equipment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Equipment Information</em>' containment reference.
	 * @see #setCancelEquipmentInformation(CancelEquipmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelEquipmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelEquipmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelEquipmentInformationType getCancelEquipmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelEquipmentInformation <em>Cancel Equipment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Equipment Information</em>' containment reference.
	 * @see #getCancelEquipmentInformation()
	 * @generated
	 */
	void setCancelEquipmentInformation(CancelEquipmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Cancel Material Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Material Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Material Class</em>' containment reference.
	 * @see #setCancelMaterialClass(CancelMaterialClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelMaterialClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelMaterialClass' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelMaterialClassType getCancelMaterialClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelMaterialClass <em>Cancel Material Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Material Class</em>' containment reference.
	 * @see #getCancelMaterialClass()
	 * @generated
	 */
	void setCancelMaterialClass(CancelMaterialClassType value);

	/**
	 * Returns the value of the '<em><b>Cancel Material Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Material Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Material Definition</em>' containment reference.
	 * @see #setCancelMaterialDefinition(CancelMaterialDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelMaterialDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelMaterialDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelMaterialDefinitionType getCancelMaterialDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelMaterialDefinition <em>Cancel Material Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Material Definition</em>' containment reference.
	 * @see #getCancelMaterialDefinition()
	 * @generated
	 */
	void setCancelMaterialDefinition(CancelMaterialDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Cancel Material Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Material Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Material Information</em>' containment reference.
	 * @see #setCancelMaterialInformation(CancelMaterialInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelMaterialInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelMaterialInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelMaterialInformationType getCancelMaterialInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelMaterialInformation <em>Cancel Material Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Material Information</em>' containment reference.
	 * @see #getCancelMaterialInformation()
	 * @generated
	 */
	void setCancelMaterialInformation(CancelMaterialInformationType value);

	/**
	 * Returns the value of the '<em><b>Cancel Material Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Material Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Material Lot</em>' containment reference.
	 * @see #setCancelMaterialLot(CancelMaterialLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelMaterialLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelMaterialLot' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelMaterialLotType getCancelMaterialLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelMaterialLot <em>Cancel Material Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Material Lot</em>' containment reference.
	 * @see #getCancelMaterialLot()
	 * @generated
	 */
	void setCancelMaterialLot(CancelMaterialLotType value);

	/**
	 * Returns the value of the '<em><b>Cancel Material Sub Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Material Sub Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Material Sub Lot</em>' containment reference.
	 * @see #setCancelMaterialSubLot(CancelMaterialSubLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelMaterialSubLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelMaterialSubLot' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelMaterialSubLotType getCancelMaterialSubLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelMaterialSubLot <em>Cancel Material Sub Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Material Sub Lot</em>' containment reference.
	 * @see #getCancelMaterialSubLot()
	 * @generated
	 */
	void setCancelMaterialSubLot(CancelMaterialSubLotType value);

	/**
	 * Returns the value of the '<em><b>Cancel Material Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Material Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Material Test Spec</em>' containment reference.
	 * @see #setCancelMaterialTestSpec(CancelMaterialTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelMaterialTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelMaterialTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelMaterialTestSpecType getCancelMaterialTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelMaterialTestSpec <em>Cancel Material Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Material Test Spec</em>' containment reference.
	 * @see #getCancelMaterialTestSpec()
	 * @generated
	 */
	void setCancelMaterialTestSpec(CancelMaterialTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Cancel Operations Capability</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Operations Capability</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Operations Capability</em>' containment reference.
	 * @see #setCancelOperationsCapability(CancelOperationsCapabilityType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelOperationsCapability()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelOperationsCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelOperationsCapabilityType getCancelOperationsCapability();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelOperationsCapability <em>Cancel Operations Capability</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Operations Capability</em>' containment reference.
	 * @see #getCancelOperationsCapability()
	 * @generated
	 */
	void setCancelOperationsCapability(CancelOperationsCapabilityType value);

	/**
	 * Returns the value of the '<em><b>Cancel Operations Capability Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Operations Capability Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Operations Capability Information</em>' containment reference.
	 * @see #setCancelOperationsCapabilityInformation(CancelOperationsCapabilityInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelOperationsCapabilityInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelOperationsCapabilityInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelOperationsCapabilityInformationType getCancelOperationsCapabilityInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelOperationsCapabilityInformation <em>Cancel Operations Capability Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Operations Capability Information</em>' containment reference.
	 * @see #getCancelOperationsCapabilityInformation()
	 * @generated
	 */
	void setCancelOperationsCapabilityInformation(CancelOperationsCapabilityInformationType value);

	/**
	 * Returns the value of the '<em><b>Cancel Operations Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Operations Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Operations Definition</em>' containment reference.
	 * @see #setCancelOperationsDefinition(CancelOperationsDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelOperationsDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelOperationsDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelOperationsDefinitionType getCancelOperationsDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelOperationsDefinition <em>Cancel Operations Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Operations Definition</em>' containment reference.
	 * @see #getCancelOperationsDefinition()
	 * @generated
	 */
	void setCancelOperationsDefinition(CancelOperationsDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Cancel Operations Definition Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Operations Definition Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Operations Definition Information</em>' containment reference.
	 * @see #setCancelOperationsDefinitionInformation(CancelOperationsDefinitionInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelOperationsDefinitionInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelOperationsDefinitionInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelOperationsDefinitionInformationType getCancelOperationsDefinitionInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelOperationsDefinitionInformation <em>Cancel Operations Definition Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Operations Definition Information</em>' containment reference.
	 * @see #getCancelOperationsDefinitionInformation()
	 * @generated
	 */
	void setCancelOperationsDefinitionInformation(CancelOperationsDefinitionInformationType value);

	/**
	 * Returns the value of the '<em><b>Cancel Operations Performance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Operations Performance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Operations Performance</em>' containment reference.
	 * @see #setCancelOperationsPerformance(CancelOperationsPerformanceType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelOperationsPerformance()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelOperationsPerformance' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelOperationsPerformanceType getCancelOperationsPerformance();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelOperationsPerformance <em>Cancel Operations Performance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Operations Performance</em>' containment reference.
	 * @see #getCancelOperationsPerformance()
	 * @generated
	 */
	void setCancelOperationsPerformance(CancelOperationsPerformanceType value);

	/**
	 * Returns the value of the '<em><b>Cancel Operations Schedule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Operations Schedule</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Operations Schedule</em>' containment reference.
	 * @see #setCancelOperationsSchedule(CancelOperationsScheduleType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelOperationsSchedule()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelOperationsSchedule' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelOperationsScheduleType getCancelOperationsSchedule();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelOperationsSchedule <em>Cancel Operations Schedule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Operations Schedule</em>' containment reference.
	 * @see #getCancelOperationsSchedule()
	 * @generated
	 */
	void setCancelOperationsSchedule(CancelOperationsScheduleType value);

	/**
	 * Returns the value of the '<em><b>Cancel Person</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Person</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Person</em>' containment reference.
	 * @see #setCancelPerson(CancelPersonType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelPerson()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelPerson' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelPersonType getCancelPerson();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPerson <em>Cancel Person</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Person</em>' containment reference.
	 * @see #getCancelPerson()
	 * @generated
	 */
	void setCancelPerson(CancelPersonType value);

	/**
	 * Returns the value of the '<em><b>Cancel Personnel Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Personnel Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Personnel Class</em>' containment reference.
	 * @see #setCancelPersonnelClass(CancelPersonnelClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelPersonnelClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelPersonnelClass' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelPersonnelClassType getCancelPersonnelClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPersonnelClass <em>Cancel Personnel Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Personnel Class</em>' containment reference.
	 * @see #getCancelPersonnelClass()
	 * @generated
	 */
	void setCancelPersonnelClass(CancelPersonnelClassType value);

	/**
	 * Returns the value of the '<em><b>Cancel Personnel Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Personnel Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Personnel Information</em>' containment reference.
	 * @see #setCancelPersonnelInformation(CancelPersonnelInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelPersonnelInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelPersonnelInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelPersonnelInformationType getCancelPersonnelInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPersonnelInformation <em>Cancel Personnel Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Personnel Information</em>' containment reference.
	 * @see #getCancelPersonnelInformation()
	 * @generated
	 */
	void setCancelPersonnelInformation(CancelPersonnelInformationType value);

	/**
	 * Returns the value of the '<em><b>Cancel Physical Asset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Physical Asset</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Physical Asset</em>' containment reference.
	 * @see #setCancelPhysicalAsset(CancelPhysicalAssetType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelPhysicalAsset()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelPhysicalAsset' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelPhysicalAssetType getCancelPhysicalAsset();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPhysicalAsset <em>Cancel Physical Asset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Physical Asset</em>' containment reference.
	 * @see #getCancelPhysicalAsset()
	 * @generated
	 */
	void setCancelPhysicalAsset(CancelPhysicalAssetType value);

	/**
	 * Returns the value of the '<em><b>Cancel Physical Asset Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Physical Asset Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #setCancelPhysicalAssetCapabilityTestSpec(CancelPhysicalAssetCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelPhysicalAssetCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelPhysicalAssetCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelPhysicalAssetCapabilityTestSpecType getCancelPhysicalAssetCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPhysicalAssetCapabilityTestSpec <em>Cancel Physical Asset Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #getCancelPhysicalAssetCapabilityTestSpec()
	 * @generated
	 */
	void setCancelPhysicalAssetCapabilityTestSpec(CancelPhysicalAssetCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Cancel Physical Asset Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Physical Asset Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Physical Asset Class</em>' containment reference.
	 * @see #setCancelPhysicalAssetClass(CancelPhysicalAssetClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelPhysicalAssetClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelPhysicalAssetClass' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelPhysicalAssetClassType getCancelPhysicalAssetClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPhysicalAssetClass <em>Cancel Physical Asset Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Physical Asset Class</em>' containment reference.
	 * @see #getCancelPhysicalAssetClass()
	 * @generated
	 */
	void setCancelPhysicalAssetClass(CancelPhysicalAssetClassType value);

	/**
	 * Returns the value of the '<em><b>Cancel Physical Asset Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Physical Asset Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Physical Asset Information</em>' containment reference.
	 * @see #setCancelPhysicalAssetInformation(CancelPhysicalAssetInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelPhysicalAssetInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelPhysicalAssetInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelPhysicalAssetInformationType getCancelPhysicalAssetInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelPhysicalAssetInformation <em>Cancel Physical Asset Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Physical Asset Information</em>' containment reference.
	 * @see #getCancelPhysicalAssetInformation()
	 * @generated
	 */
	void setCancelPhysicalAssetInformation(CancelPhysicalAssetInformationType value);

	/**
	 * Returns the value of the '<em><b>Cancel Process Segment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Process Segment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Process Segment</em>' containment reference.
	 * @see #setCancelProcessSegment(CancelProcessSegmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelProcessSegment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelProcessSegment' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelProcessSegmentType getCancelProcessSegment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelProcessSegment <em>Cancel Process Segment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Process Segment</em>' containment reference.
	 * @see #getCancelProcessSegment()
	 * @generated
	 */
	void setCancelProcessSegment(CancelProcessSegmentType value);

	/**
	 * Returns the value of the '<em><b>Cancel Process Segment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Process Segment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Process Segment Information</em>' containment reference.
	 * @see #setCancelProcessSegmentInformation(CancelProcessSegmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelProcessSegmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelProcessSegmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelProcessSegmentInformationType getCancelProcessSegmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelProcessSegmentInformation <em>Cancel Process Segment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Process Segment Information</em>' containment reference.
	 * @see #getCancelProcessSegmentInformation()
	 * @generated
	 */
	void setCancelProcessSegmentInformation(CancelProcessSegmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Cancel Qualification Test Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cancel Qualification Test Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cancel Qualification Test Specification</em>' containment reference.
	 * @see #setCancelQualificationTestSpecification(CancelQualificationTestSpecificationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_CancelQualificationTestSpecification()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CancelQualificationTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	CancelQualificationTestSpecificationType getCancelQualificationTestSpecification();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getCancelQualificationTestSpecification <em>Cancel Qualification Test Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cancel Qualification Test Specification</em>' containment reference.
	 * @see #getCancelQualificationTestSpecification()
	 * @generated
	 */
	void setCancelQualificationTestSpecification(CancelQualificationTestSpecificationType value);

	/**
	 * Returns the value of the '<em><b>Change Equipment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Equipment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Equipment</em>' containment reference.
	 * @see #setChangeEquipment(ChangeEquipmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeEquipment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeEquipment' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeEquipmentType getChangeEquipment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeEquipment <em>Change Equipment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Equipment</em>' containment reference.
	 * @see #getChangeEquipment()
	 * @generated
	 */
	void setChangeEquipment(ChangeEquipmentType value);

	/**
	 * Returns the value of the '<em><b>Change Equipment Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Equipment Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Equipment Capability Test Spec</em>' containment reference.
	 * @see #setChangeEquipmentCapabilityTestSpec(ChangeEquipmentCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeEquipmentCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeEquipmentCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeEquipmentCapabilityTestSpecType getChangeEquipmentCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeEquipmentCapabilityTestSpec <em>Change Equipment Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Equipment Capability Test Spec</em>' containment reference.
	 * @see #getChangeEquipmentCapabilityTestSpec()
	 * @generated
	 */
	void setChangeEquipmentCapabilityTestSpec(ChangeEquipmentCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Change Equipment Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Equipment Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Equipment Class</em>' containment reference.
	 * @see #setChangeEquipmentClass(ChangeEquipmentClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeEquipmentClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeEquipmentClass' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeEquipmentClassType getChangeEquipmentClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeEquipmentClass <em>Change Equipment Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Equipment Class</em>' containment reference.
	 * @see #getChangeEquipmentClass()
	 * @generated
	 */
	void setChangeEquipmentClass(ChangeEquipmentClassType value);

	/**
	 * Returns the value of the '<em><b>Change Equipment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Equipment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Equipment Information</em>' containment reference.
	 * @see #setChangeEquipmentInformation(ChangeEquipmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeEquipmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeEquipmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeEquipmentInformationType getChangeEquipmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeEquipmentInformation <em>Change Equipment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Equipment Information</em>' containment reference.
	 * @see #getChangeEquipmentInformation()
	 * @generated
	 */
	void setChangeEquipmentInformation(ChangeEquipmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Change Material Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Material Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Material Class</em>' containment reference.
	 * @see #setChangeMaterialClass(ChangeMaterialClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeMaterialClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeMaterialClass' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeMaterialClassType getChangeMaterialClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeMaterialClass <em>Change Material Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Material Class</em>' containment reference.
	 * @see #getChangeMaterialClass()
	 * @generated
	 */
	void setChangeMaterialClass(ChangeMaterialClassType value);

	/**
	 * Returns the value of the '<em><b>Change Material Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Material Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Material Definition</em>' containment reference.
	 * @see #setChangeMaterialDefinition(ChangeMaterialDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeMaterialDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeMaterialDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeMaterialDefinitionType getChangeMaterialDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeMaterialDefinition <em>Change Material Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Material Definition</em>' containment reference.
	 * @see #getChangeMaterialDefinition()
	 * @generated
	 */
	void setChangeMaterialDefinition(ChangeMaterialDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Change Material Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Material Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Material Information</em>' containment reference.
	 * @see #setChangeMaterialInformation(ChangeMaterialInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeMaterialInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeMaterialInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeMaterialInformationType getChangeMaterialInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeMaterialInformation <em>Change Material Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Material Information</em>' containment reference.
	 * @see #getChangeMaterialInformation()
	 * @generated
	 */
	void setChangeMaterialInformation(ChangeMaterialInformationType value);

	/**
	 * Returns the value of the '<em><b>Change Material Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Material Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Material Lot</em>' containment reference.
	 * @see #setChangeMaterialLot(ChangeMaterialLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeMaterialLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeMaterialLot' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeMaterialLotType getChangeMaterialLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeMaterialLot <em>Change Material Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Material Lot</em>' containment reference.
	 * @see #getChangeMaterialLot()
	 * @generated
	 */
	void setChangeMaterialLot(ChangeMaterialLotType value);

	/**
	 * Returns the value of the '<em><b>Change Material Sub Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Material Sub Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Material Sub Lot</em>' containment reference.
	 * @see #setChangeMaterialSubLot(ChangeMaterialSubLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeMaterialSubLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeMaterialSubLot' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeMaterialSubLotType getChangeMaterialSubLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeMaterialSubLot <em>Change Material Sub Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Material Sub Lot</em>' containment reference.
	 * @see #getChangeMaterialSubLot()
	 * @generated
	 */
	void setChangeMaterialSubLot(ChangeMaterialSubLotType value);

	/**
	 * Returns the value of the '<em><b>Change Material Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Material Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Material Test Spec</em>' containment reference.
	 * @see #setChangeMaterialTestSpec(ChangeMaterialTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeMaterialTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeMaterialTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeMaterialTestSpecType getChangeMaterialTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeMaterialTestSpec <em>Change Material Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Material Test Spec</em>' containment reference.
	 * @see #getChangeMaterialTestSpec()
	 * @generated
	 */
	void setChangeMaterialTestSpec(ChangeMaterialTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Change Operations Capability</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Operations Capability</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Operations Capability</em>' containment reference.
	 * @see #setChangeOperationsCapability(ChangeOperationsCapabilityType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeOperationsCapability()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeOperationsCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeOperationsCapabilityType getChangeOperationsCapability();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeOperationsCapability <em>Change Operations Capability</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Operations Capability</em>' containment reference.
	 * @see #getChangeOperationsCapability()
	 * @generated
	 */
	void setChangeOperationsCapability(ChangeOperationsCapabilityType value);

	/**
	 * Returns the value of the '<em><b>Change Operations Capability Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Operations Capability Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Operations Capability Information</em>' containment reference.
	 * @see #setChangeOperationsCapabilityInformation(ChangeOperationsCapabilityInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeOperationsCapabilityInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeOperationsCapabilityInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeOperationsCapabilityInformationType getChangeOperationsCapabilityInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeOperationsCapabilityInformation <em>Change Operations Capability Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Operations Capability Information</em>' containment reference.
	 * @see #getChangeOperationsCapabilityInformation()
	 * @generated
	 */
	void setChangeOperationsCapabilityInformation(ChangeOperationsCapabilityInformationType value);

	/**
	 * Returns the value of the '<em><b>Change Operations Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Operations Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Operations Definition</em>' containment reference.
	 * @see #setChangeOperationsDefinition(ChangeOperationsDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeOperationsDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeOperationsDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeOperationsDefinitionType getChangeOperationsDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeOperationsDefinition <em>Change Operations Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Operations Definition</em>' containment reference.
	 * @see #getChangeOperationsDefinition()
	 * @generated
	 */
	void setChangeOperationsDefinition(ChangeOperationsDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Change Operations Definition Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Operations Definition Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Operations Definition Information</em>' containment reference.
	 * @see #setChangeOperationsDefinitionInformation(ChangeOperationsDefinitionInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeOperationsDefinitionInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeOperationsDefinitionInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeOperationsDefinitionInformationType getChangeOperationsDefinitionInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeOperationsDefinitionInformation <em>Change Operations Definition Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Operations Definition Information</em>' containment reference.
	 * @see #getChangeOperationsDefinitionInformation()
	 * @generated
	 */
	void setChangeOperationsDefinitionInformation(ChangeOperationsDefinitionInformationType value);

	/**
	 * Returns the value of the '<em><b>Change Operations Performance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Operations Performance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Operations Performance</em>' containment reference.
	 * @see #setChangeOperationsPerformance(ChangeOperationsPerformanceType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeOperationsPerformance()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeOperationsPerformance' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeOperationsPerformanceType getChangeOperationsPerformance();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeOperationsPerformance <em>Change Operations Performance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Operations Performance</em>' containment reference.
	 * @see #getChangeOperationsPerformance()
	 * @generated
	 */
	void setChangeOperationsPerformance(ChangeOperationsPerformanceType value);

	/**
	 * Returns the value of the '<em><b>Change Operations Schedule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Operations Schedule</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Operations Schedule</em>' containment reference.
	 * @see #setChangeOperationsSchedule(ChangeOperationsScheduleType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeOperationsSchedule()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeOperationsSchedule' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeOperationsScheduleType getChangeOperationsSchedule();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeOperationsSchedule <em>Change Operations Schedule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Operations Schedule</em>' containment reference.
	 * @see #getChangeOperationsSchedule()
	 * @generated
	 */
	void setChangeOperationsSchedule(ChangeOperationsScheduleType value);

	/**
	 * Returns the value of the '<em><b>Change Person</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Person</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Person</em>' containment reference.
	 * @see #setChangePerson(ChangePersonType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangePerson()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangePerson' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangePersonType getChangePerson();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePerson <em>Change Person</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Person</em>' containment reference.
	 * @see #getChangePerson()
	 * @generated
	 */
	void setChangePerson(ChangePersonType value);

	/**
	 * Returns the value of the '<em><b>Change Personnel Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Personnel Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Personnel Class</em>' containment reference.
	 * @see #setChangePersonnelClass(ChangePersonnelClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangePersonnelClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangePersonnelClass' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangePersonnelClassType getChangePersonnelClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePersonnelClass <em>Change Personnel Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Personnel Class</em>' containment reference.
	 * @see #getChangePersonnelClass()
	 * @generated
	 */
	void setChangePersonnelClass(ChangePersonnelClassType value);

	/**
	 * Returns the value of the '<em><b>Change Personnel Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Personnel Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Personnel Information</em>' containment reference.
	 * @see #setChangePersonnelInformation(ChangePersonnelInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangePersonnelInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangePersonnelInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangePersonnelInformationType getChangePersonnelInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePersonnelInformation <em>Change Personnel Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Personnel Information</em>' containment reference.
	 * @see #getChangePersonnelInformation()
	 * @generated
	 */
	void setChangePersonnelInformation(ChangePersonnelInformationType value);

	/**
	 * Returns the value of the '<em><b>Change Physical Asset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Physical Asset</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Physical Asset</em>' containment reference.
	 * @see #setChangePhysicalAsset(ChangePhysicalAssetType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangePhysicalAsset()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangePhysicalAsset' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangePhysicalAssetType getChangePhysicalAsset();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePhysicalAsset <em>Change Physical Asset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Physical Asset</em>' containment reference.
	 * @see #getChangePhysicalAsset()
	 * @generated
	 */
	void setChangePhysicalAsset(ChangePhysicalAssetType value);

	/**
	 * Returns the value of the '<em><b>Change Physical Asset Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Physical Asset Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #setChangePhysicalAssetCapabilityTestSpec(ChangePhysicalAssetCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangePhysicalAssetCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangePhysicalAssetCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangePhysicalAssetCapabilityTestSpecType getChangePhysicalAssetCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePhysicalAssetCapabilityTestSpec <em>Change Physical Asset Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #getChangePhysicalAssetCapabilityTestSpec()
	 * @generated
	 */
	void setChangePhysicalAssetCapabilityTestSpec(ChangePhysicalAssetCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Change Physical Asset Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Physical Asset Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Physical Asset Class</em>' containment reference.
	 * @see #setChangePhysicalAssetClass(ChangePhysicalAssetClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangePhysicalAssetClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangePhysicalAssetClass' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangePhysicalAssetClassType getChangePhysicalAssetClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePhysicalAssetClass <em>Change Physical Asset Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Physical Asset Class</em>' containment reference.
	 * @see #getChangePhysicalAssetClass()
	 * @generated
	 */
	void setChangePhysicalAssetClass(ChangePhysicalAssetClassType value);

	/**
	 * Returns the value of the '<em><b>Change Physical Asset Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Physical Asset Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Physical Asset Information</em>' containment reference.
	 * @see #setChangePhysicalAssetInformation(ChangePhysicalAssetInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangePhysicalAssetInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangePhysicalAssetInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangePhysicalAssetInformationType getChangePhysicalAssetInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangePhysicalAssetInformation <em>Change Physical Asset Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Physical Asset Information</em>' containment reference.
	 * @see #getChangePhysicalAssetInformation()
	 * @generated
	 */
	void setChangePhysicalAssetInformation(ChangePhysicalAssetInformationType value);

	/**
	 * Returns the value of the '<em><b>Change Process Segment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Process Segment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Process Segment</em>' containment reference.
	 * @see #setChangeProcessSegment(ChangeProcessSegmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeProcessSegment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeProcessSegment' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeProcessSegmentType getChangeProcessSegment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeProcessSegment <em>Change Process Segment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Process Segment</em>' containment reference.
	 * @see #getChangeProcessSegment()
	 * @generated
	 */
	void setChangeProcessSegment(ChangeProcessSegmentType value);

	/**
	 * Returns the value of the '<em><b>Change Process Segment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Process Segment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Process Segment Information</em>' containment reference.
	 * @see #setChangeProcessSegmentInformation(ChangeProcessSegmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeProcessSegmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeProcessSegmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeProcessSegmentInformationType getChangeProcessSegmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeProcessSegmentInformation <em>Change Process Segment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Process Segment Information</em>' containment reference.
	 * @see #getChangeProcessSegmentInformation()
	 * @generated
	 */
	void setChangeProcessSegmentInformation(ChangeProcessSegmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Change Qualification Test Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Change Qualification Test Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Change Qualification Test Specification</em>' containment reference.
	 * @see #setChangeQualificationTestSpecification(ChangeQualificationTestSpecificationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ChangeQualificationTestSpecification()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ChangeQualificationTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	ChangeQualificationTestSpecificationType getChangeQualificationTestSpecification();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getChangeQualificationTestSpecification <em>Change Qualification Test Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Change Qualification Test Specification</em>' containment reference.
	 * @see #getChangeQualificationTestSpecification()
	 * @generated
	 */
	void setChangeQualificationTestSpecification(ChangeQualificationTestSpecificationType value);

	/**
	 * Returns the value of the '<em><b>Confirm BOD</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Confirm BOD</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confirm BOD</em>' containment reference.
	 * @see #setConfirmBOD(ConfirmBODType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ConfirmBOD()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ConfirmBOD' namespace='##targetNamespace'"
	 * @generated
	 */
	ConfirmBODType getConfirmBOD();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getConfirmBOD <em>Confirm BOD</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confirm BOD</em>' containment reference.
	 * @see #getConfirmBOD()
	 * @generated
	 */
	void setConfirmBOD(ConfirmBODType value);

	/**
	 * Returns the value of the '<em><b>Equipment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment</em>' containment reference.
	 * @see #setEquipment(EquipmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_Equipment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Equipment' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentType getEquipment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getEquipment <em>Equipment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment</em>' containment reference.
	 * @see #getEquipment()
	 * @generated
	 */
	void setEquipment(EquipmentType value);

	/**
	 * Returns the value of the '<em><b>Equipment Capability Test Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Capability Test Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Capability Test Specification</em>' containment reference.
	 * @see #setEquipmentCapabilityTestSpecification(EquipmentCapabilityTestSpecificationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_EquipmentCapabilityTestSpecification()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='EquipmentCapabilityTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentCapabilityTestSpecificationType getEquipmentCapabilityTestSpecification();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getEquipmentCapabilityTestSpecification <em>Equipment Capability Test Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Capability Test Specification</em>' containment reference.
	 * @see #getEquipmentCapabilityTestSpecification()
	 * @generated
	 */
	void setEquipmentCapabilityTestSpecification(EquipmentCapabilityTestSpecificationType value);

	/**
	 * Returns the value of the '<em><b>Equipment Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Class</em>' containment reference.
	 * @see #setEquipmentClass(EquipmentClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_EquipmentClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='EquipmentClass' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentClassType getEquipmentClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getEquipmentClass <em>Equipment Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Class</em>' containment reference.
	 * @see #getEquipmentClass()
	 * @generated
	 */
	void setEquipmentClass(EquipmentClassType value);

	/**
	 * Returns the value of the '<em><b>Equipment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equipment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equipment Information</em>' containment reference.
	 * @see #setEquipmentInformation(EquipmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_EquipmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='EquipmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	EquipmentInformationType getEquipmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getEquipmentInformation <em>Equipment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equipment Information</em>' containment reference.
	 * @see #getEquipmentInformation()
	 * @generated
	 */
	void setEquipmentInformation(EquipmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Get Equipment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Equipment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Equipment</em>' containment reference.
	 * @see #setGetEquipment(GetEquipmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetEquipment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetEquipment' namespace='##targetNamespace'"
	 * @generated
	 */
	GetEquipmentType getGetEquipment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetEquipment <em>Get Equipment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Equipment</em>' containment reference.
	 * @see #getGetEquipment()
	 * @generated
	 */
	void setGetEquipment(GetEquipmentType value);

	/**
	 * Returns the value of the '<em><b>Get Equipment Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Equipment Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Equipment Capability Test Spec</em>' containment reference.
	 * @see #setGetEquipmentCapabilityTestSpec(GetEquipmentCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetEquipmentCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetEquipmentCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	GetEquipmentCapabilityTestSpecType getGetEquipmentCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetEquipmentCapabilityTestSpec <em>Get Equipment Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Equipment Capability Test Spec</em>' containment reference.
	 * @see #getGetEquipmentCapabilityTestSpec()
	 * @generated
	 */
	void setGetEquipmentCapabilityTestSpec(GetEquipmentCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Get Equipment Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Equipment Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Equipment Class</em>' containment reference.
	 * @see #setGetEquipmentClass(GetEquipmentClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetEquipmentClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetEquipmentClass' namespace='##targetNamespace'"
	 * @generated
	 */
	GetEquipmentClassType getGetEquipmentClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetEquipmentClass <em>Get Equipment Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Equipment Class</em>' containment reference.
	 * @see #getGetEquipmentClass()
	 * @generated
	 */
	void setGetEquipmentClass(GetEquipmentClassType value);

	/**
	 * Returns the value of the '<em><b>Get Equipment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Equipment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Equipment Information</em>' containment reference.
	 * @see #setGetEquipmentInformation(GetEquipmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetEquipmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetEquipmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	GetEquipmentInformationType getGetEquipmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetEquipmentInformation <em>Get Equipment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Equipment Information</em>' containment reference.
	 * @see #getGetEquipmentInformation()
	 * @generated
	 */
	void setGetEquipmentInformation(GetEquipmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Get Material Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Material Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Material Class</em>' containment reference.
	 * @see #setGetMaterialClass(GetMaterialClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetMaterialClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetMaterialClass' namespace='##targetNamespace'"
	 * @generated
	 */
	GetMaterialClassType getGetMaterialClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetMaterialClass <em>Get Material Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Material Class</em>' containment reference.
	 * @see #getGetMaterialClass()
	 * @generated
	 */
	void setGetMaterialClass(GetMaterialClassType value);

	/**
	 * Returns the value of the '<em><b>Get Material Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Material Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Material Definition</em>' containment reference.
	 * @see #setGetMaterialDefinition(GetMaterialDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetMaterialDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetMaterialDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	GetMaterialDefinitionType getGetMaterialDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetMaterialDefinition <em>Get Material Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Material Definition</em>' containment reference.
	 * @see #getGetMaterialDefinition()
	 * @generated
	 */
	void setGetMaterialDefinition(GetMaterialDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Get Material Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Material Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Material Information</em>' containment reference.
	 * @see #setGetMaterialInformation(GetMaterialInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetMaterialInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetMaterialInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	GetMaterialInformationType getGetMaterialInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetMaterialInformation <em>Get Material Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Material Information</em>' containment reference.
	 * @see #getGetMaterialInformation()
	 * @generated
	 */
	void setGetMaterialInformation(GetMaterialInformationType value);

	/**
	 * Returns the value of the '<em><b>Get Material Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Material Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Material Lot</em>' containment reference.
	 * @see #setGetMaterialLot(GetMaterialLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetMaterialLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetMaterialLot' namespace='##targetNamespace'"
	 * @generated
	 */
	GetMaterialLotType getGetMaterialLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetMaterialLot <em>Get Material Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Material Lot</em>' containment reference.
	 * @see #getGetMaterialLot()
	 * @generated
	 */
	void setGetMaterialLot(GetMaterialLotType value);

	/**
	 * Returns the value of the '<em><b>Get Material Sub Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Material Sub Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Material Sub Lot</em>' containment reference.
	 * @see #setGetMaterialSubLot(GetMaterialSubLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetMaterialSubLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetMaterialSubLot' namespace='##targetNamespace'"
	 * @generated
	 */
	GetMaterialSubLotType getGetMaterialSubLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetMaterialSubLot <em>Get Material Sub Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Material Sub Lot</em>' containment reference.
	 * @see #getGetMaterialSubLot()
	 * @generated
	 */
	void setGetMaterialSubLot(GetMaterialSubLotType value);

	/**
	 * Returns the value of the '<em><b>Get Material Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Material Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Material Test Spec</em>' containment reference.
	 * @see #setGetMaterialTestSpec(GetMaterialTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetMaterialTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetMaterialTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	GetMaterialTestSpecType getGetMaterialTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetMaterialTestSpec <em>Get Material Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Material Test Spec</em>' containment reference.
	 * @see #getGetMaterialTestSpec()
	 * @generated
	 */
	void setGetMaterialTestSpec(GetMaterialTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Get Operations Capability</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Operations Capability</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Operations Capability</em>' containment reference.
	 * @see #setGetOperationsCapability(GetOperationsCapabilityType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetOperationsCapability()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetOperationsCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	GetOperationsCapabilityType getGetOperationsCapability();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetOperationsCapability <em>Get Operations Capability</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Operations Capability</em>' containment reference.
	 * @see #getGetOperationsCapability()
	 * @generated
	 */
	void setGetOperationsCapability(GetOperationsCapabilityType value);

	/**
	 * Returns the value of the '<em><b>Get Operations Capability Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Operations Capability Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Operations Capability Information</em>' containment reference.
	 * @see #setGetOperationsCapabilityInformation(GetOperationsCapabilityInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetOperationsCapabilityInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetOperationsCapabilityInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	GetOperationsCapabilityInformationType getGetOperationsCapabilityInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetOperationsCapabilityInformation <em>Get Operations Capability Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Operations Capability Information</em>' containment reference.
	 * @see #getGetOperationsCapabilityInformation()
	 * @generated
	 */
	void setGetOperationsCapabilityInformation(GetOperationsCapabilityInformationType value);

	/**
	 * Returns the value of the '<em><b>Get Operations Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Operations Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Operations Definition</em>' containment reference.
	 * @see #setGetOperationsDefinition(GetOperationsDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetOperationsDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetOperationsDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	GetOperationsDefinitionType getGetOperationsDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetOperationsDefinition <em>Get Operations Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Operations Definition</em>' containment reference.
	 * @see #getGetOperationsDefinition()
	 * @generated
	 */
	void setGetOperationsDefinition(GetOperationsDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Get Operations Definition Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Operations Definition Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Operations Definition Information</em>' containment reference.
	 * @see #setGetOperationsDefinitionInformation(GetOperationsDefinitionInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetOperationsDefinitionInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetOperationsDefinitionInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	GetOperationsDefinitionInformationType getGetOperationsDefinitionInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetOperationsDefinitionInformation <em>Get Operations Definition Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Operations Definition Information</em>' containment reference.
	 * @see #getGetOperationsDefinitionInformation()
	 * @generated
	 */
	void setGetOperationsDefinitionInformation(GetOperationsDefinitionInformationType value);

	/**
	 * Returns the value of the '<em><b>Get Operations Performance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Operations Performance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Operations Performance</em>' containment reference.
	 * @see #setGetOperationsPerformance(GetOperationsPerformanceType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetOperationsPerformance()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetOperationsPerformance' namespace='##targetNamespace'"
	 * @generated
	 */
	GetOperationsPerformanceType getGetOperationsPerformance();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetOperationsPerformance <em>Get Operations Performance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Operations Performance</em>' containment reference.
	 * @see #getGetOperationsPerformance()
	 * @generated
	 */
	void setGetOperationsPerformance(GetOperationsPerformanceType value);

	/**
	 * Returns the value of the '<em><b>Get Operations Schedule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Operations Schedule</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Operations Schedule</em>' containment reference.
	 * @see #setGetOperationsSchedule(GetOperationsScheduleType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetOperationsSchedule()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetOperationsSchedule' namespace='##targetNamespace'"
	 * @generated
	 */
	GetOperationsScheduleType getGetOperationsSchedule();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetOperationsSchedule <em>Get Operations Schedule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Operations Schedule</em>' containment reference.
	 * @see #getGetOperationsSchedule()
	 * @generated
	 */
	void setGetOperationsSchedule(GetOperationsScheduleType value);

	/**
	 * Returns the value of the '<em><b>Get Person</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Person</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Person</em>' containment reference.
	 * @see #setGetPerson(GetPersonType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetPerson()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetPerson' namespace='##targetNamespace'"
	 * @generated
	 */
	GetPersonType getGetPerson();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPerson <em>Get Person</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Person</em>' containment reference.
	 * @see #getGetPerson()
	 * @generated
	 */
	void setGetPerson(GetPersonType value);

	/**
	 * Returns the value of the '<em><b>Get Personnel Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Personnel Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Personnel Class</em>' containment reference.
	 * @see #setGetPersonnelClass(GetPersonnelClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetPersonnelClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetPersonnelClass' namespace='##targetNamespace'"
	 * @generated
	 */
	GetPersonnelClassType getGetPersonnelClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPersonnelClass <em>Get Personnel Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Personnel Class</em>' containment reference.
	 * @see #getGetPersonnelClass()
	 * @generated
	 */
	void setGetPersonnelClass(GetPersonnelClassType value);

	/**
	 * Returns the value of the '<em><b>Get Personnel Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Personnel Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Personnel Information</em>' containment reference.
	 * @see #setGetPersonnelInformation(GetPersonnelInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetPersonnelInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetPersonnelInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	GetPersonnelInformationType getGetPersonnelInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPersonnelInformation <em>Get Personnel Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Personnel Information</em>' containment reference.
	 * @see #getGetPersonnelInformation()
	 * @generated
	 */
	void setGetPersonnelInformation(GetPersonnelInformationType value);

	/**
	 * Returns the value of the '<em><b>Get Physical Asset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Physical Asset</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Physical Asset</em>' containment reference.
	 * @see #setGetPhysicalAsset(GetPhysicalAssetType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetPhysicalAsset()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetPhysicalAsset' namespace='##targetNamespace'"
	 * @generated
	 */
	GetPhysicalAssetType getGetPhysicalAsset();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPhysicalAsset <em>Get Physical Asset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Physical Asset</em>' containment reference.
	 * @see #getGetPhysicalAsset()
	 * @generated
	 */
	void setGetPhysicalAsset(GetPhysicalAssetType value);

	/**
	 * Returns the value of the '<em><b>Get Physical Asset Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Physical Asset Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #setGetPhysicalAssetCapabilityTestSpec(GetPhysicalAssetCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetPhysicalAssetCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetPhysicalAssetCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	GetPhysicalAssetCapabilityTestSpecType getGetPhysicalAssetCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPhysicalAssetCapabilityTestSpec <em>Get Physical Asset Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #getGetPhysicalAssetCapabilityTestSpec()
	 * @generated
	 */
	void setGetPhysicalAssetCapabilityTestSpec(GetPhysicalAssetCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Get Physical Asset Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Physical Asset Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Physical Asset Class</em>' containment reference.
	 * @see #setGetPhysicalAssetClass(GetPhysicalAssetClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetPhysicalAssetClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetPhysicalAssetClass' namespace='##targetNamespace'"
	 * @generated
	 */
	GetPhysicalAssetClassType getGetPhysicalAssetClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPhysicalAssetClass <em>Get Physical Asset Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Physical Asset Class</em>' containment reference.
	 * @see #getGetPhysicalAssetClass()
	 * @generated
	 */
	void setGetPhysicalAssetClass(GetPhysicalAssetClassType value);

	/**
	 * Returns the value of the '<em><b>Get Physical Asset Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Physical Asset Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Physical Asset Information</em>' containment reference.
	 * @see #setGetPhysicalAssetInformation(GetPhysicalAssetInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetPhysicalAssetInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetPhysicalAssetInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	GetPhysicalAssetInformationType getGetPhysicalAssetInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetPhysicalAssetInformation <em>Get Physical Asset Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Physical Asset Information</em>' containment reference.
	 * @see #getGetPhysicalAssetInformation()
	 * @generated
	 */
	void setGetPhysicalAssetInformation(GetPhysicalAssetInformationType value);

	/**
	 * Returns the value of the '<em><b>Get Process Segment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Process Segment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Process Segment</em>' containment reference.
	 * @see #setGetProcessSegment(GetProcessSegmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetProcessSegment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetProcessSegment' namespace='##targetNamespace'"
	 * @generated
	 */
	GetProcessSegmentType getGetProcessSegment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetProcessSegment <em>Get Process Segment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Process Segment</em>' containment reference.
	 * @see #getGetProcessSegment()
	 * @generated
	 */
	void setGetProcessSegment(GetProcessSegmentType value);

	/**
	 * Returns the value of the '<em><b>Get Process Segment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Process Segment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Process Segment Information</em>' containment reference.
	 * @see #setGetProcessSegmentInformation(GetProcessSegmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetProcessSegmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetProcessSegmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	GetProcessSegmentInformationType getGetProcessSegmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetProcessSegmentInformation <em>Get Process Segment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Process Segment Information</em>' containment reference.
	 * @see #getGetProcessSegmentInformation()
	 * @generated
	 */
	void setGetProcessSegmentInformation(GetProcessSegmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Get Qualification Test Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Qualification Test Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Qualification Test Specification</em>' containment reference.
	 * @see #setGetQualificationTestSpecification(GetQualificationTestSpecificationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_GetQualificationTestSpecification()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='GetQualificationTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	GetQualificationTestSpecificationType getGetQualificationTestSpecification();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getGetQualificationTestSpecification <em>Get Qualification Test Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Qualification Test Specification</em>' containment reference.
	 * @see #getGetQualificationTestSpecification()
	 * @generated
	 */
	void setGetQualificationTestSpecification(GetQualificationTestSpecificationType value);

	/**
	 * Returns the value of the '<em><b>Material Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Class</em>' containment reference.
	 * @see #setMaterialClass(MaterialClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_MaterialClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='MaterialClass' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialClassType getMaterialClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMaterialClass <em>Material Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Class</em>' containment reference.
	 * @see #getMaterialClass()
	 * @generated
	 */
	void setMaterialClass(MaterialClassType value);

	/**
	 * Returns the value of the '<em><b>Material Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Definition</em>' containment reference.
	 * @see #setMaterialDefinition(MaterialDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_MaterialDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='MaterialDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialDefinitionType getMaterialDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMaterialDefinition <em>Material Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Definition</em>' containment reference.
	 * @see #getMaterialDefinition()
	 * @generated
	 */
	void setMaterialDefinition(MaterialDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Material Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Information</em>' containment reference.
	 * @see #setMaterialInformation(MaterialInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_MaterialInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='MaterialInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialInformationType getMaterialInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMaterialInformation <em>Material Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Information</em>' containment reference.
	 * @see #getMaterialInformation()
	 * @generated
	 */
	void setMaterialInformation(MaterialInformationType value);

	/**
	 * Returns the value of the '<em><b>Material Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Lot</em>' containment reference.
	 * @see #setMaterialLot(MaterialLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_MaterialLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='MaterialLot' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialLotType getMaterialLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMaterialLot <em>Material Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Lot</em>' containment reference.
	 * @see #getMaterialLot()
	 * @generated
	 */
	void setMaterialLot(MaterialLotType value);

	/**
	 * Returns the value of the '<em><b>Material Sub Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Sub Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Sub Lot</em>' containment reference.
	 * @see #setMaterialSubLot(MaterialSubLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_MaterialSubLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='MaterialSubLot' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialSubLotType getMaterialSubLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMaterialSubLot <em>Material Sub Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Sub Lot</em>' containment reference.
	 * @see #getMaterialSubLot()
	 * @generated
	 */
	void setMaterialSubLot(MaterialSubLotType value);

	/**
	 * Returns the value of the '<em><b>Material Test Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Material Test Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Material Test Specification</em>' containment reference.
	 * @see #setMaterialTestSpecification(MaterialTestSpecificationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_MaterialTestSpecification()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='MaterialTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	MaterialTestSpecificationType getMaterialTestSpecification();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getMaterialTestSpecification <em>Material Test Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Material Test Specification</em>' containment reference.
	 * @see #getMaterialTestSpecification()
	 * @generated
	 */
	void setMaterialTestSpecification(MaterialTestSpecificationType value);

	/**
	 * Returns the value of the '<em><b>Operations Capability</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Capability</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Capability</em>' containment reference.
	 * @see #setOperationsCapability(OperationsCapabilityType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_OperationsCapability()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='OperationsCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsCapabilityType getOperationsCapability();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsCapability <em>Operations Capability</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Capability</em>' containment reference.
	 * @see #getOperationsCapability()
	 * @generated
	 */
	void setOperationsCapability(OperationsCapabilityType value);

	/**
	 * Returns the value of the '<em><b>Operations Capability Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Capability Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Capability Information</em>' containment reference.
	 * @see #setOperationsCapabilityInformation(OperationsCapabilityInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_OperationsCapabilityInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='OperationsCapabilityInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsCapabilityInformationType getOperationsCapabilityInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsCapabilityInformation <em>Operations Capability Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Capability Information</em>' containment reference.
	 * @see #getOperationsCapabilityInformation()
	 * @generated
	 */
	void setOperationsCapabilityInformation(OperationsCapabilityInformationType value);

	/**
	 * Returns the value of the '<em><b>Operations Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition</em>' containment reference.
	 * @see #setOperationsDefinition(OperationsDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_OperationsDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='OperationsDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsDefinitionType getOperationsDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsDefinition <em>Operations Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Definition</em>' containment reference.
	 * @see #getOperationsDefinition()
	 * @generated
	 */
	void setOperationsDefinition(OperationsDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Operations Definition Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Definition Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Definition Information</em>' containment reference.
	 * @see #setOperationsDefinitionInformation(OperationsDefinitionInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_OperationsDefinitionInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='OperationsDefinitionInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsDefinitionInformationType getOperationsDefinitionInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsDefinitionInformation <em>Operations Definition Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Definition Information</em>' containment reference.
	 * @see #getOperationsDefinitionInformation()
	 * @generated
	 */
	void setOperationsDefinitionInformation(OperationsDefinitionInformationType value);

	/**
	 * Returns the value of the '<em><b>Operations Performance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Performance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Performance</em>' containment reference.
	 * @see #setOperationsPerformance(OperationsPerformanceType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_OperationsPerformance()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='OperationsPerformance' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsPerformanceType getOperationsPerformance();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsPerformance <em>Operations Performance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Performance</em>' containment reference.
	 * @see #getOperationsPerformance()
	 * @generated
	 */
	void setOperationsPerformance(OperationsPerformanceType value);

	/**
	 * Returns the value of the '<em><b>Operations Request</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Request</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Request</em>' containment reference.
	 * @see #setOperationsRequest(OperationsRequestType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_OperationsRequest()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='OperationsRequest' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsRequestType getOperationsRequest();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsRequest <em>Operations Request</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Request</em>' containment reference.
	 * @see #getOperationsRequest()
	 * @generated
	 */
	void setOperationsRequest(OperationsRequestType value);

	/**
	 * Returns the value of the '<em><b>Operations Response</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Response</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Response</em>' containment reference.
	 * @see #setOperationsResponse(OperationsResponseType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_OperationsResponse()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='OperationsResponse' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsResponseType getOperationsResponse();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsResponse <em>Operations Response</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Response</em>' containment reference.
	 * @see #getOperationsResponse()
	 * @generated
	 */
	void setOperationsResponse(OperationsResponseType value);

	/**
	 * Returns the value of the '<em><b>Operations Schedule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations Schedule</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations Schedule</em>' containment reference.
	 * @see #setOperationsSchedule(OperationsScheduleType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_OperationsSchedule()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='OperationsSchedule' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationsScheduleType getOperationsSchedule();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getOperationsSchedule <em>Operations Schedule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operations Schedule</em>' containment reference.
	 * @see #getOperationsSchedule()
	 * @generated
	 */
	void setOperationsSchedule(OperationsScheduleType value);

	/**
	 * Returns the value of the '<em><b>Person</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person</em>' containment reference.
	 * @see #setPerson(PersonType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_Person()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Person' namespace='##targetNamespace'"
	 * @generated
	 */
	PersonType getPerson();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPerson <em>Person</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Person</em>' containment reference.
	 * @see #getPerson()
	 * @generated
	 */
	void setPerson(PersonType value);

	/**
	 * Returns the value of the '<em><b>Personnel Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Class</em>' containment reference.
	 * @see #setPersonnelClass(PersonnelClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_PersonnelClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='PersonnelClass' namespace='##targetNamespace'"
	 * @generated
	 */
	PersonnelClassType getPersonnelClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPersonnelClass <em>Personnel Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Personnel Class</em>' containment reference.
	 * @see #getPersonnelClass()
	 * @generated
	 */
	void setPersonnelClass(PersonnelClassType value);

	/**
	 * Returns the value of the '<em><b>Personnel Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Personnel Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Personnel Information</em>' containment reference.
	 * @see #setPersonnelInformation(PersonnelInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_PersonnelInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='PersonnelInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	PersonnelInformationType getPersonnelInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPersonnelInformation <em>Personnel Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Personnel Information</em>' containment reference.
	 * @see #getPersonnelInformation()
	 * @generated
	 */
	void setPersonnelInformation(PersonnelInformationType value);

	/**
	 * Returns the value of the '<em><b>Physical Asset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset</em>' containment reference.
	 * @see #setPhysicalAsset(PhysicalAssetType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_PhysicalAsset()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='PhysicalAsset' namespace='##targetNamespace'"
	 * @generated
	 */
	PhysicalAssetType getPhysicalAsset();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPhysicalAsset <em>Physical Asset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset</em>' containment reference.
	 * @see #getPhysicalAsset()
	 * @generated
	 */
	void setPhysicalAsset(PhysicalAssetType value);

	/**
	 * Returns the value of the '<em><b>Physical Asset Capability Test Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Capability Test Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Capability Test Specification</em>' containment reference.
	 * @see #setPhysicalAssetCapabilityTestSpecification(PhysicalAssetCapabilityTestSpecificationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_PhysicalAssetCapabilityTestSpecification()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetCapabilityTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	PhysicalAssetCapabilityTestSpecificationType getPhysicalAssetCapabilityTestSpecification();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPhysicalAssetCapabilityTestSpecification <em>Physical Asset Capability Test Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Capability Test Specification</em>' containment reference.
	 * @see #getPhysicalAssetCapabilityTestSpecification()
	 * @generated
	 */
	void setPhysicalAssetCapabilityTestSpecification(PhysicalAssetCapabilityTestSpecificationType value);

	/**
	 * Returns the value of the '<em><b>Physical Asset Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Class</em>' containment reference.
	 * @see #setPhysicalAssetClass(PhysicalAssetClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_PhysicalAssetClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetClass' namespace='##targetNamespace'"
	 * @generated
	 */
	PhysicalAssetClassType getPhysicalAssetClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPhysicalAssetClass <em>Physical Asset Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Class</em>' containment reference.
	 * @see #getPhysicalAssetClass()
	 * @generated
	 */
	void setPhysicalAssetClass(PhysicalAssetClassType value);

	/**
	 * Returns the value of the '<em><b>Physical Asset Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Asset Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Asset Information</em>' containment reference.
	 * @see #setPhysicalAssetInformation(PhysicalAssetInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_PhysicalAssetInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='PhysicalAssetInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	PhysicalAssetInformationType getPhysicalAssetInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getPhysicalAssetInformation <em>Physical Asset Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Asset Information</em>' containment reference.
	 * @see #getPhysicalAssetInformation()
	 * @generated
	 */
	void setPhysicalAssetInformation(PhysicalAssetInformationType value);

	/**
	 * Returns the value of the '<em><b>Process Equipment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Equipment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Equipment</em>' containment reference.
	 * @see #setProcessEquipment(ProcessEquipmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessEquipment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessEquipment' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessEquipmentType getProcessEquipment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessEquipment <em>Process Equipment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Equipment</em>' containment reference.
	 * @see #getProcessEquipment()
	 * @generated
	 */
	void setProcessEquipment(ProcessEquipmentType value);

	/**
	 * Returns the value of the '<em><b>Process Equipment Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Equipment Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Equipment Capability Test Spec</em>' containment reference.
	 * @see #setProcessEquipmentCapabilityTestSpec(ProcessEquipmentCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessEquipmentCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessEquipmentCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessEquipmentCapabilityTestSpecType getProcessEquipmentCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessEquipmentCapabilityTestSpec <em>Process Equipment Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Equipment Capability Test Spec</em>' containment reference.
	 * @see #getProcessEquipmentCapabilityTestSpec()
	 * @generated
	 */
	void setProcessEquipmentCapabilityTestSpec(ProcessEquipmentCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Process Equipment Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Equipment Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Equipment Class</em>' containment reference.
	 * @see #setProcessEquipmentClass(ProcessEquipmentClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessEquipmentClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessEquipmentClass' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessEquipmentClassType getProcessEquipmentClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessEquipmentClass <em>Process Equipment Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Equipment Class</em>' containment reference.
	 * @see #getProcessEquipmentClass()
	 * @generated
	 */
	void setProcessEquipmentClass(ProcessEquipmentClassType value);

	/**
	 * Returns the value of the '<em><b>Process Equipment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Equipment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Equipment Information</em>' containment reference.
	 * @see #setProcessEquipmentInformation(ProcessEquipmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessEquipmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessEquipmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessEquipmentInformationType getProcessEquipmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessEquipmentInformation <em>Process Equipment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Equipment Information</em>' containment reference.
	 * @see #getProcessEquipmentInformation()
	 * @generated
	 */
	void setProcessEquipmentInformation(ProcessEquipmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Process Material Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Material Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Material Class</em>' containment reference.
	 * @see #setProcessMaterialClass(ProcessMaterialClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessMaterialClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessMaterialClass' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessMaterialClassType getProcessMaterialClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessMaterialClass <em>Process Material Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Material Class</em>' containment reference.
	 * @see #getProcessMaterialClass()
	 * @generated
	 */
	void setProcessMaterialClass(ProcessMaterialClassType value);

	/**
	 * Returns the value of the '<em><b>Process Material Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Material Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Material Definition</em>' containment reference.
	 * @see #setProcessMaterialDefinition(ProcessMaterialDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessMaterialDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessMaterialDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessMaterialDefinitionType getProcessMaterialDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessMaterialDefinition <em>Process Material Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Material Definition</em>' containment reference.
	 * @see #getProcessMaterialDefinition()
	 * @generated
	 */
	void setProcessMaterialDefinition(ProcessMaterialDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Process Material Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Material Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Material Information</em>' containment reference.
	 * @see #setProcessMaterialInformation(ProcessMaterialInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessMaterialInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessMaterialInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessMaterialInformationType getProcessMaterialInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessMaterialInformation <em>Process Material Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Material Information</em>' containment reference.
	 * @see #getProcessMaterialInformation()
	 * @generated
	 */
	void setProcessMaterialInformation(ProcessMaterialInformationType value);

	/**
	 * Returns the value of the '<em><b>Process Material Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Material Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Material Lot</em>' containment reference.
	 * @see #setProcessMaterialLot(ProcessMaterialLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessMaterialLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessMaterialLot' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessMaterialLotType getProcessMaterialLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessMaterialLot <em>Process Material Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Material Lot</em>' containment reference.
	 * @see #getProcessMaterialLot()
	 * @generated
	 */
	void setProcessMaterialLot(ProcessMaterialLotType value);

	/**
	 * Returns the value of the '<em><b>Process Material Sub Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Material Sub Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Material Sub Lot</em>' containment reference.
	 * @see #setProcessMaterialSubLot(ProcessMaterialSubLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessMaterialSubLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessMaterialSubLot' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessMaterialSubLotType getProcessMaterialSubLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessMaterialSubLot <em>Process Material Sub Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Material Sub Lot</em>' containment reference.
	 * @see #getProcessMaterialSubLot()
	 * @generated
	 */
	void setProcessMaterialSubLot(ProcessMaterialSubLotType value);

	/**
	 * Returns the value of the '<em><b>Process Material Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Material Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Material Test Spec</em>' containment reference.
	 * @see #setProcessMaterialTestSpec(ProcessMaterialTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessMaterialTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessMaterialTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessMaterialTestSpecType getProcessMaterialTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessMaterialTestSpec <em>Process Material Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Material Test Spec</em>' containment reference.
	 * @see #getProcessMaterialTestSpec()
	 * @generated
	 */
	void setProcessMaterialTestSpec(ProcessMaterialTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Process Operations Capability</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Operations Capability</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Operations Capability</em>' containment reference.
	 * @see #setProcessOperationsCapability(ProcessOperationsCapabilityType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessOperationsCapability()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessOperationsCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessOperationsCapabilityType getProcessOperationsCapability();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessOperationsCapability <em>Process Operations Capability</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Operations Capability</em>' containment reference.
	 * @see #getProcessOperationsCapability()
	 * @generated
	 */
	void setProcessOperationsCapability(ProcessOperationsCapabilityType value);

	/**
	 * Returns the value of the '<em><b>Process Operations Capability Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Operations Capability Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Operations Capability Information</em>' containment reference.
	 * @see #setProcessOperationsCapabilityInformation(ProcessOperationsCapabilityInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessOperationsCapabilityInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessOperationsCapabilityInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessOperationsCapabilityInformationType getProcessOperationsCapabilityInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessOperationsCapabilityInformation <em>Process Operations Capability Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Operations Capability Information</em>' containment reference.
	 * @see #getProcessOperationsCapabilityInformation()
	 * @generated
	 */
	void setProcessOperationsCapabilityInformation(ProcessOperationsCapabilityInformationType value);

	/**
	 * Returns the value of the '<em><b>Process Operations Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Operations Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Operations Definition</em>' containment reference.
	 * @see #setProcessOperationsDefinition(ProcessOperationsDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessOperationsDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessOperationsDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessOperationsDefinitionType getProcessOperationsDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessOperationsDefinition <em>Process Operations Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Operations Definition</em>' containment reference.
	 * @see #getProcessOperationsDefinition()
	 * @generated
	 */
	void setProcessOperationsDefinition(ProcessOperationsDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Process Operations Definition Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Operations Definition Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Operations Definition Information</em>' containment reference.
	 * @see #setProcessOperationsDefinitionInformation(ProcessOperationsDefinitionInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessOperationsDefinitionInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessOperationsDefinitionInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessOperationsDefinitionInformationType getProcessOperationsDefinitionInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessOperationsDefinitionInformation <em>Process Operations Definition Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Operations Definition Information</em>' containment reference.
	 * @see #getProcessOperationsDefinitionInformation()
	 * @generated
	 */
	void setProcessOperationsDefinitionInformation(ProcessOperationsDefinitionInformationType value);

	/**
	 * Returns the value of the '<em><b>Process Operations Performance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Operations Performance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Operations Performance</em>' containment reference.
	 * @see #setProcessOperationsPerformance(ProcessOperationsPerformanceType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessOperationsPerformance()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessOperationsPerformance' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessOperationsPerformanceType getProcessOperationsPerformance();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessOperationsPerformance <em>Process Operations Performance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Operations Performance</em>' containment reference.
	 * @see #getProcessOperationsPerformance()
	 * @generated
	 */
	void setProcessOperationsPerformance(ProcessOperationsPerformanceType value);

	/**
	 * Returns the value of the '<em><b>Process Operations Schedule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Operations Schedule</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Operations Schedule</em>' containment reference.
	 * @see #setProcessOperationsSchedule(ProcessOperationsScheduleType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessOperationsSchedule()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessOperationsSchedule' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessOperationsScheduleType getProcessOperationsSchedule();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessOperationsSchedule <em>Process Operations Schedule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Operations Schedule</em>' containment reference.
	 * @see #getProcessOperationsSchedule()
	 * @generated
	 */
	void setProcessOperationsSchedule(ProcessOperationsScheduleType value);

	/**
	 * Returns the value of the '<em><b>Process Person</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Person</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Person</em>' containment reference.
	 * @see #setProcessPerson(ProcessPersonType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessPerson()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessPerson' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessPersonType getProcessPerson();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPerson <em>Process Person</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Person</em>' containment reference.
	 * @see #getProcessPerson()
	 * @generated
	 */
	void setProcessPerson(ProcessPersonType value);

	/**
	 * Returns the value of the '<em><b>Process Personnel Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Personnel Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Personnel Class</em>' containment reference.
	 * @see #setProcessPersonnelClass(ProcessPersonnelClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessPersonnelClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessPersonnelClass' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessPersonnelClassType getProcessPersonnelClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPersonnelClass <em>Process Personnel Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Personnel Class</em>' containment reference.
	 * @see #getProcessPersonnelClass()
	 * @generated
	 */
	void setProcessPersonnelClass(ProcessPersonnelClassType value);

	/**
	 * Returns the value of the '<em><b>Process Personnel Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Personnel Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Personnel Information</em>' containment reference.
	 * @see #setProcessPersonnelInformation(ProcessPersonnelInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessPersonnelInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessPersonnelInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessPersonnelInformationType getProcessPersonnelInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPersonnelInformation <em>Process Personnel Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Personnel Information</em>' containment reference.
	 * @see #getProcessPersonnelInformation()
	 * @generated
	 */
	void setProcessPersonnelInformation(ProcessPersonnelInformationType value);

	/**
	 * Returns the value of the '<em><b>Process Physical Asset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Physical Asset</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Physical Asset</em>' containment reference.
	 * @see #setProcessPhysicalAsset(ProcessPhysicalAssetType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessPhysicalAsset()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessPhysicalAsset' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessPhysicalAssetType getProcessPhysicalAsset();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPhysicalAsset <em>Process Physical Asset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Physical Asset</em>' containment reference.
	 * @see #getProcessPhysicalAsset()
	 * @generated
	 */
	void setProcessPhysicalAsset(ProcessPhysicalAssetType value);

	/**
	 * Returns the value of the '<em><b>Process Physical Asset Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Physical Asset Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #setProcessPhysicalAssetCapabilityTestSpec(ProcessPhysicalAssetCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessPhysicalAssetCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessPhysicalAssetCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessPhysicalAssetCapabilityTestSpecType getProcessPhysicalAssetCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPhysicalAssetCapabilityTestSpec <em>Process Physical Asset Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #getProcessPhysicalAssetCapabilityTestSpec()
	 * @generated
	 */
	void setProcessPhysicalAssetCapabilityTestSpec(ProcessPhysicalAssetCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Process Physical Asset Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Physical Asset Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Physical Asset Class</em>' containment reference.
	 * @see #setProcessPhysicalAssetClass(ProcessPhysicalAssetClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessPhysicalAssetClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessPhysicalAssetClass' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessPhysicalAssetClassType getProcessPhysicalAssetClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPhysicalAssetClass <em>Process Physical Asset Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Physical Asset Class</em>' containment reference.
	 * @see #getProcessPhysicalAssetClass()
	 * @generated
	 */
	void setProcessPhysicalAssetClass(ProcessPhysicalAssetClassType value);

	/**
	 * Returns the value of the '<em><b>Process Physical Asset Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Physical Asset Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Physical Asset Information</em>' containment reference.
	 * @see #setProcessPhysicalAssetInformation(ProcessPhysicalAssetInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessPhysicalAssetInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessPhysicalAssetInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessPhysicalAssetInformationType getProcessPhysicalAssetInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessPhysicalAssetInformation <em>Process Physical Asset Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Physical Asset Information</em>' containment reference.
	 * @see #getProcessPhysicalAssetInformation()
	 * @generated
	 */
	void setProcessPhysicalAssetInformation(ProcessPhysicalAssetInformationType value);

	/**
	 * Returns the value of the '<em><b>Process Process Segment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Process Segment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Process Segment</em>' containment reference.
	 * @see #setProcessProcessSegment(ProcessProcessSegmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessProcessSegment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessProcessSegment' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessProcessSegmentType getProcessProcessSegment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessProcessSegment <em>Process Process Segment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Process Segment</em>' containment reference.
	 * @see #getProcessProcessSegment()
	 * @generated
	 */
	void setProcessProcessSegment(ProcessProcessSegmentType value);

	/**
	 * Returns the value of the '<em><b>Process Process Segment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Process Segment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Process Segment Information</em>' containment reference.
	 * @see #setProcessProcessSegmentInformation(ProcessProcessSegmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessProcessSegmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessProcessSegmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessProcessSegmentInformationType getProcessProcessSegmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessProcessSegmentInformation <em>Process Process Segment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Process Segment Information</em>' containment reference.
	 * @see #getProcessProcessSegmentInformation()
	 * @generated
	 */
	void setProcessProcessSegmentInformation(ProcessProcessSegmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Process Qualification Test Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Qualification Test Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Qualification Test Specification</em>' containment reference.
	 * @see #setProcessQualificationTestSpecification(ProcessQualificationTestSpecificationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessQualificationTestSpecification()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessQualificationTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessQualificationTestSpecificationType getProcessQualificationTestSpecification();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessQualificationTestSpecification <em>Process Qualification Test Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Qualification Test Specification</em>' containment reference.
	 * @see #getProcessQualificationTestSpecification()
	 * @generated
	 */
	void setProcessQualificationTestSpecification(ProcessQualificationTestSpecificationType value);

	/**
	 * Returns the value of the '<em><b>Process Segment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment</em>' containment reference.
	 * @see #setProcessSegment(ProcessSegmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessSegment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessSegment' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessSegmentType getProcessSegment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessSegment <em>Process Segment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Segment</em>' containment reference.
	 * @see #getProcessSegment()
	 * @generated
	 */
	void setProcessSegment(ProcessSegmentType value);

	/**
	 * Returns the value of the '<em><b>Process Segment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Segment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Segment Information</em>' containment reference.
	 * @see #setProcessSegmentInformation(ProcessSegmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ProcessSegmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ProcessSegmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcessSegmentInformationType getProcessSegmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getProcessSegmentInformation <em>Process Segment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Segment Information</em>' containment reference.
	 * @see #getProcessSegmentInformation()
	 * @generated
	 */
	void setProcessSegmentInformation(ProcessSegmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Qualification Test Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualification Test Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualification Test Specification</em>' containment reference.
	 * @see #setQualificationTestSpecification(QualificationTestSpecificationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_QualificationTestSpecification()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='QualificationTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	QualificationTestSpecificationType getQualificationTestSpecification();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getQualificationTestSpecification <em>Qualification Test Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qualification Test Specification</em>' containment reference.
	 * @see #getQualificationTestSpecification()
	 * @generated
	 */
	void setQualificationTestSpecification(QualificationTestSpecificationType value);

	/**
	 * Returns the value of the '<em><b>Respond Equipment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Equipment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Equipment</em>' containment reference.
	 * @see #setRespondEquipment(RespondEquipmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondEquipment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondEquipment' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondEquipmentType getRespondEquipment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondEquipment <em>Respond Equipment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Equipment</em>' containment reference.
	 * @see #getRespondEquipment()
	 * @generated
	 */
	void setRespondEquipment(RespondEquipmentType value);

	/**
	 * Returns the value of the '<em><b>Respond Equipment Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Equipment Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Equipment Capability Test Spec</em>' containment reference.
	 * @see #setRespondEquipmentCapabilityTestSpec(RespondEquipmentCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondEquipmentCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondEquipmentCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondEquipmentCapabilityTestSpecType getRespondEquipmentCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondEquipmentCapabilityTestSpec <em>Respond Equipment Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Equipment Capability Test Spec</em>' containment reference.
	 * @see #getRespondEquipmentCapabilityTestSpec()
	 * @generated
	 */
	void setRespondEquipmentCapabilityTestSpec(RespondEquipmentCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Respond Equipment Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Equipment Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Equipment Class</em>' containment reference.
	 * @see #setRespondEquipmentClass(RespondEquipmentClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondEquipmentClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondEquipmentClass' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondEquipmentClassType getRespondEquipmentClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondEquipmentClass <em>Respond Equipment Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Equipment Class</em>' containment reference.
	 * @see #getRespondEquipmentClass()
	 * @generated
	 */
	void setRespondEquipmentClass(RespondEquipmentClassType value);

	/**
	 * Returns the value of the '<em><b>Respond Equipment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Equipment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Equipment Information</em>' containment reference.
	 * @see #setRespondEquipmentInformation(RespondEquipmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondEquipmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondEquipmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondEquipmentInformationType getRespondEquipmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondEquipmentInformation <em>Respond Equipment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Equipment Information</em>' containment reference.
	 * @see #getRespondEquipmentInformation()
	 * @generated
	 */
	void setRespondEquipmentInformation(RespondEquipmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Respond Material Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Material Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Material Class</em>' containment reference.
	 * @see #setRespondMaterialClass(RespondMaterialClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondMaterialClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondMaterialClass' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondMaterialClassType getRespondMaterialClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondMaterialClass <em>Respond Material Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Material Class</em>' containment reference.
	 * @see #getRespondMaterialClass()
	 * @generated
	 */
	void setRespondMaterialClass(RespondMaterialClassType value);

	/**
	 * Returns the value of the '<em><b>Respond Material Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Material Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Material Definition</em>' containment reference.
	 * @see #setRespondMaterialDefinition(RespondMaterialDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondMaterialDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondMaterialDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondMaterialDefinitionType getRespondMaterialDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondMaterialDefinition <em>Respond Material Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Material Definition</em>' containment reference.
	 * @see #getRespondMaterialDefinition()
	 * @generated
	 */
	void setRespondMaterialDefinition(RespondMaterialDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Respond Material Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Material Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Material Information</em>' containment reference.
	 * @see #setRespondMaterialInformation(RespondMaterialInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondMaterialInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondMaterialInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondMaterialInformationType getRespondMaterialInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondMaterialInformation <em>Respond Material Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Material Information</em>' containment reference.
	 * @see #getRespondMaterialInformation()
	 * @generated
	 */
	void setRespondMaterialInformation(RespondMaterialInformationType value);

	/**
	 * Returns the value of the '<em><b>Respond Material Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Material Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Material Lot</em>' containment reference.
	 * @see #setRespondMaterialLot(RespondMaterialLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondMaterialLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondMaterialLot' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondMaterialLotType getRespondMaterialLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondMaterialLot <em>Respond Material Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Material Lot</em>' containment reference.
	 * @see #getRespondMaterialLot()
	 * @generated
	 */
	void setRespondMaterialLot(RespondMaterialLotType value);

	/**
	 * Returns the value of the '<em><b>Respond Material Sub Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Material Sub Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Material Sub Lot</em>' containment reference.
	 * @see #setRespondMaterialSubLot(RespondMaterialSubLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondMaterialSubLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondMaterialSubLot' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondMaterialSubLotType getRespondMaterialSubLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondMaterialSubLot <em>Respond Material Sub Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Material Sub Lot</em>' containment reference.
	 * @see #getRespondMaterialSubLot()
	 * @generated
	 */
	void setRespondMaterialSubLot(RespondMaterialSubLotType value);

	/**
	 * Returns the value of the '<em><b>Respond Material Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Material Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Material Test Spec</em>' containment reference.
	 * @see #setRespondMaterialTestSpec(RespondMaterialTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondMaterialTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondMaterialTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondMaterialTestSpecType getRespondMaterialTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondMaterialTestSpec <em>Respond Material Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Material Test Spec</em>' containment reference.
	 * @see #getRespondMaterialTestSpec()
	 * @generated
	 */
	void setRespondMaterialTestSpec(RespondMaterialTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Respond Operations Capability</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Operations Capability</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Operations Capability</em>' containment reference.
	 * @see #setRespondOperationsCapability(RespondOperationsCapabilityType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondOperationsCapability()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondOperationsCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondOperationsCapabilityType getRespondOperationsCapability();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondOperationsCapability <em>Respond Operations Capability</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Operations Capability</em>' containment reference.
	 * @see #getRespondOperationsCapability()
	 * @generated
	 */
	void setRespondOperationsCapability(RespondOperationsCapabilityType value);

	/**
	 * Returns the value of the '<em><b>Respond Operations Capability Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Operations Capability Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Operations Capability Information</em>' containment reference.
	 * @see #setRespondOperationsCapabilityInformation(RespondOperationsCapabilityInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondOperationsCapabilityInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondOperationsCapabilityInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondOperationsCapabilityInformationType getRespondOperationsCapabilityInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondOperationsCapabilityInformation <em>Respond Operations Capability Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Operations Capability Information</em>' containment reference.
	 * @see #getRespondOperationsCapabilityInformation()
	 * @generated
	 */
	void setRespondOperationsCapabilityInformation(RespondOperationsCapabilityInformationType value);

	/**
	 * Returns the value of the '<em><b>Respond Operations Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Operations Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Operations Definition</em>' containment reference.
	 * @see #setRespondOperationsDefinition(RespondOperationsDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondOperationsDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondOperationsDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondOperationsDefinitionType getRespondOperationsDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondOperationsDefinition <em>Respond Operations Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Operations Definition</em>' containment reference.
	 * @see #getRespondOperationsDefinition()
	 * @generated
	 */
	void setRespondOperationsDefinition(RespondOperationsDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Respond Operations Definition Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Operations Definition Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Operations Definition Information</em>' containment reference.
	 * @see #setRespondOperationsDefinitionInformation(RespondOperationsDefinitionInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondOperationsDefinitionInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondOperationsDefinitionInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondOperationsDefinitionInformationType getRespondOperationsDefinitionInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondOperationsDefinitionInformation <em>Respond Operations Definition Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Operations Definition Information</em>' containment reference.
	 * @see #getRespondOperationsDefinitionInformation()
	 * @generated
	 */
	void setRespondOperationsDefinitionInformation(RespondOperationsDefinitionInformationType value);

	/**
	 * Returns the value of the '<em><b>Respond Operations Performance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Operations Performance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Operations Performance</em>' containment reference.
	 * @see #setRespondOperationsPerformance(RespondOperationsPerformanceType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondOperationsPerformance()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondOperationsPerformance' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondOperationsPerformanceType getRespondOperationsPerformance();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondOperationsPerformance <em>Respond Operations Performance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Operations Performance</em>' containment reference.
	 * @see #getRespondOperationsPerformance()
	 * @generated
	 */
	void setRespondOperationsPerformance(RespondOperationsPerformanceType value);

	/**
	 * Returns the value of the '<em><b>Respond Operations Schedule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Operations Schedule</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Operations Schedule</em>' containment reference.
	 * @see #setRespondOperationsSchedule(RespondOperationsScheduleType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondOperationsSchedule()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondOperationsSchedule' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondOperationsScheduleType getRespondOperationsSchedule();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondOperationsSchedule <em>Respond Operations Schedule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Operations Schedule</em>' containment reference.
	 * @see #getRespondOperationsSchedule()
	 * @generated
	 */
	void setRespondOperationsSchedule(RespondOperationsScheduleType value);

	/**
	 * Returns the value of the '<em><b>Respond Person</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Person</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Person</em>' containment reference.
	 * @see #setRespondPerson(RespondPersonType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondPerson()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondPerson' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondPersonType getRespondPerson();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPerson <em>Respond Person</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Person</em>' containment reference.
	 * @see #getRespondPerson()
	 * @generated
	 */
	void setRespondPerson(RespondPersonType value);

	/**
	 * Returns the value of the '<em><b>Respond Personnel Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Personnel Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Personnel Class</em>' containment reference.
	 * @see #setRespondPersonnelClass(RespondPersonnelClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondPersonnelClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondPersonnelClass' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondPersonnelClassType getRespondPersonnelClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPersonnelClass <em>Respond Personnel Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Personnel Class</em>' containment reference.
	 * @see #getRespondPersonnelClass()
	 * @generated
	 */
	void setRespondPersonnelClass(RespondPersonnelClassType value);

	/**
	 * Returns the value of the '<em><b>Respond Personnel Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Personnel Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Personnel Information</em>' containment reference.
	 * @see #setRespondPersonnelInformation(RespondPersonnelInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondPersonnelInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondPersonnelInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondPersonnelInformationType getRespondPersonnelInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPersonnelInformation <em>Respond Personnel Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Personnel Information</em>' containment reference.
	 * @see #getRespondPersonnelInformation()
	 * @generated
	 */
	void setRespondPersonnelInformation(RespondPersonnelInformationType value);

	/**
	 * Returns the value of the '<em><b>Respond Physical Asset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Physical Asset</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Physical Asset</em>' containment reference.
	 * @see #setRespondPhysicalAsset(RespondPhysicalAssetType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondPhysicalAsset()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondPhysicalAsset' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondPhysicalAssetType getRespondPhysicalAsset();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPhysicalAsset <em>Respond Physical Asset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Physical Asset</em>' containment reference.
	 * @see #getRespondPhysicalAsset()
	 * @generated
	 */
	void setRespondPhysicalAsset(RespondPhysicalAssetType value);

	/**
	 * Returns the value of the '<em><b>Respond Physical Asset Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Physical Asset Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #setRespondPhysicalAssetCapabilityTestSpec(RespondPhysicalAssetCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondPhysicalAssetCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondPhysicalAssetCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondPhysicalAssetCapabilityTestSpecType getRespondPhysicalAssetCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPhysicalAssetCapabilityTestSpec <em>Respond Physical Asset Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #getRespondPhysicalAssetCapabilityTestSpec()
	 * @generated
	 */
	void setRespondPhysicalAssetCapabilityTestSpec(RespondPhysicalAssetCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Respond Physical Asset Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Physical Asset Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Physical Asset Class</em>' containment reference.
	 * @see #setRespondPhysicalAssetClass(RespondPhysicalAssetClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondPhysicalAssetClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondPhysicalAssetClass' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondPhysicalAssetClassType getRespondPhysicalAssetClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPhysicalAssetClass <em>Respond Physical Asset Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Physical Asset Class</em>' containment reference.
	 * @see #getRespondPhysicalAssetClass()
	 * @generated
	 */
	void setRespondPhysicalAssetClass(RespondPhysicalAssetClassType value);

	/**
	 * Returns the value of the '<em><b>Respond Physical Asset Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Physical Asset Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Physical Asset Information</em>' containment reference.
	 * @see #setRespondPhysicalAssetInformation(RespondPhysicalAssetInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondPhysicalAssetInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondPhysicalAssetInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondPhysicalAssetInformationType getRespondPhysicalAssetInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondPhysicalAssetInformation <em>Respond Physical Asset Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Physical Asset Information</em>' containment reference.
	 * @see #getRespondPhysicalAssetInformation()
	 * @generated
	 */
	void setRespondPhysicalAssetInformation(RespondPhysicalAssetInformationType value);

	/**
	 * Returns the value of the '<em><b>Respond Process Segment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Process Segment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Process Segment</em>' containment reference.
	 * @see #setRespondProcessSegment(RespondProcessSegmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondProcessSegment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondProcessSegment' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondProcessSegmentType getRespondProcessSegment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondProcessSegment <em>Respond Process Segment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Process Segment</em>' containment reference.
	 * @see #getRespondProcessSegment()
	 * @generated
	 */
	void setRespondProcessSegment(RespondProcessSegmentType value);

	/**
	 * Returns the value of the '<em><b>Respond Process Segment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Process Segment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Process Segment Information</em>' containment reference.
	 * @see #setRespondProcessSegmentInformation(RespondProcessSegmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondProcessSegmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondProcessSegmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondProcessSegmentInformationType getRespondProcessSegmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondProcessSegmentInformation <em>Respond Process Segment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Process Segment Information</em>' containment reference.
	 * @see #getRespondProcessSegmentInformation()
	 * @generated
	 */
	void setRespondProcessSegmentInformation(RespondProcessSegmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Respond Qualification Test Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Respond Qualification Test Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Respond Qualification Test Specification</em>' containment reference.
	 * @see #setRespondQualificationTestSpecification(RespondQualificationTestSpecificationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_RespondQualificationTestSpecification()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RespondQualificationTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	RespondQualificationTestSpecificationType getRespondQualificationTestSpecification();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getRespondQualificationTestSpecification <em>Respond Qualification Test Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Respond Qualification Test Specification</em>' containment reference.
	 * @see #getRespondQualificationTestSpecification()
	 * @generated
	 */
	void setRespondQualificationTestSpecification(RespondQualificationTestSpecificationType value);

	/**
	 * Returns the value of the '<em><b>Show Equipment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Equipment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Equipment</em>' containment reference.
	 * @see #setShowEquipment(ShowEquipmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowEquipment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowEquipment' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowEquipmentType getShowEquipment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowEquipment <em>Show Equipment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Equipment</em>' containment reference.
	 * @see #getShowEquipment()
	 * @generated
	 */
	void setShowEquipment(ShowEquipmentType value);

	/**
	 * Returns the value of the '<em><b>Show Equipment Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Equipment Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Equipment Capability Test Spec</em>' containment reference.
	 * @see #setShowEquipmentCapabilityTestSpec(ShowEquipmentCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowEquipmentCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowEquipmentCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowEquipmentCapabilityTestSpecType getShowEquipmentCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowEquipmentCapabilityTestSpec <em>Show Equipment Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Equipment Capability Test Spec</em>' containment reference.
	 * @see #getShowEquipmentCapabilityTestSpec()
	 * @generated
	 */
	void setShowEquipmentCapabilityTestSpec(ShowEquipmentCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Show Equipment Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Equipment Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Equipment Class</em>' containment reference.
	 * @see #setShowEquipmentClass(ShowEquipmentClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowEquipmentClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowEquipmentClass' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowEquipmentClassType getShowEquipmentClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowEquipmentClass <em>Show Equipment Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Equipment Class</em>' containment reference.
	 * @see #getShowEquipmentClass()
	 * @generated
	 */
	void setShowEquipmentClass(ShowEquipmentClassType value);

	/**
	 * Returns the value of the '<em><b>Show Equipment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Equipment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Equipment Information</em>' containment reference.
	 * @see #setShowEquipmentInformation(ShowEquipmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowEquipmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowEquipmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowEquipmentInformationType getShowEquipmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowEquipmentInformation <em>Show Equipment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Equipment Information</em>' containment reference.
	 * @see #getShowEquipmentInformation()
	 * @generated
	 */
	void setShowEquipmentInformation(ShowEquipmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Show Material Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Material Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Material Class</em>' containment reference.
	 * @see #setShowMaterialClass(ShowMaterialClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowMaterialClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowMaterialClass' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowMaterialClassType getShowMaterialClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowMaterialClass <em>Show Material Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Material Class</em>' containment reference.
	 * @see #getShowMaterialClass()
	 * @generated
	 */
	void setShowMaterialClass(ShowMaterialClassType value);

	/**
	 * Returns the value of the '<em><b>Show Material Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Material Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Material Definition</em>' containment reference.
	 * @see #setShowMaterialDefinition(ShowMaterialDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowMaterialDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowMaterialDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowMaterialDefinitionType getShowMaterialDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowMaterialDefinition <em>Show Material Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Material Definition</em>' containment reference.
	 * @see #getShowMaterialDefinition()
	 * @generated
	 */
	void setShowMaterialDefinition(ShowMaterialDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Show Material Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Material Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Material Information</em>' containment reference.
	 * @see #setShowMaterialInformation(ShowMaterialInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowMaterialInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowMaterialInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowMaterialInformationType getShowMaterialInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowMaterialInformation <em>Show Material Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Material Information</em>' containment reference.
	 * @see #getShowMaterialInformation()
	 * @generated
	 */
	void setShowMaterialInformation(ShowMaterialInformationType value);

	/**
	 * Returns the value of the '<em><b>Show Material Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Material Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Material Lot</em>' containment reference.
	 * @see #setShowMaterialLot(ShowMaterialLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowMaterialLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowMaterialLot' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowMaterialLotType getShowMaterialLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowMaterialLot <em>Show Material Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Material Lot</em>' containment reference.
	 * @see #getShowMaterialLot()
	 * @generated
	 */
	void setShowMaterialLot(ShowMaterialLotType value);

	/**
	 * Returns the value of the '<em><b>Show Material Sub Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Material Sub Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Material Sub Lot</em>' containment reference.
	 * @see #setShowMaterialSubLot(ShowMaterialSubLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowMaterialSubLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowMaterialSubLot' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowMaterialSubLotType getShowMaterialSubLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowMaterialSubLot <em>Show Material Sub Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Material Sub Lot</em>' containment reference.
	 * @see #getShowMaterialSubLot()
	 * @generated
	 */
	void setShowMaterialSubLot(ShowMaterialSubLotType value);

	/**
	 * Returns the value of the '<em><b>Show Material Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Material Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Material Test Spec</em>' containment reference.
	 * @see #setShowMaterialTestSpec(ShowMaterialTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowMaterialTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowMaterialTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowMaterialTestSpecType getShowMaterialTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowMaterialTestSpec <em>Show Material Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Material Test Spec</em>' containment reference.
	 * @see #getShowMaterialTestSpec()
	 * @generated
	 */
	void setShowMaterialTestSpec(ShowMaterialTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Show Operations Capability</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Operations Capability</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Operations Capability</em>' containment reference.
	 * @see #setShowOperationsCapability(ShowOperationsCapabilityType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowOperationsCapability()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowOperationsCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowOperationsCapabilityType getShowOperationsCapability();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowOperationsCapability <em>Show Operations Capability</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Operations Capability</em>' containment reference.
	 * @see #getShowOperationsCapability()
	 * @generated
	 */
	void setShowOperationsCapability(ShowOperationsCapabilityType value);

	/**
	 * Returns the value of the '<em><b>Show Operations Capability Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Operations Capability Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Operations Capability Information</em>' containment reference.
	 * @see #setShowOperationsCapabilityInformation(ShowOperationsCapabilityInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowOperationsCapabilityInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowOperationsCapabilityInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowOperationsCapabilityInformationType getShowOperationsCapabilityInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowOperationsCapabilityInformation <em>Show Operations Capability Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Operations Capability Information</em>' containment reference.
	 * @see #getShowOperationsCapabilityInformation()
	 * @generated
	 */
	void setShowOperationsCapabilityInformation(ShowOperationsCapabilityInformationType value);

	/**
	 * Returns the value of the '<em><b>Show Operations Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Operations Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Operations Definition</em>' containment reference.
	 * @see #setShowOperationsDefinition(ShowOperationsDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowOperationsDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowOperationsDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowOperationsDefinitionType getShowOperationsDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowOperationsDefinition <em>Show Operations Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Operations Definition</em>' containment reference.
	 * @see #getShowOperationsDefinition()
	 * @generated
	 */
	void setShowOperationsDefinition(ShowOperationsDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Show Operations Definition Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Operations Definition Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Operations Definition Information</em>' containment reference.
	 * @see #setShowOperationsDefinitionInformation(ShowOperationsDefinitionInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowOperationsDefinitionInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowOperationsDefinitionInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowOperationsDefinitionInformationType getShowOperationsDefinitionInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowOperationsDefinitionInformation <em>Show Operations Definition Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Operations Definition Information</em>' containment reference.
	 * @see #getShowOperationsDefinitionInformation()
	 * @generated
	 */
	void setShowOperationsDefinitionInformation(ShowOperationsDefinitionInformationType value);

	/**
	 * Returns the value of the '<em><b>Show Operations Performance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Operations Performance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Operations Performance</em>' containment reference.
	 * @see #setShowOperationsPerformance(ShowOperationsPerformanceType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowOperationsPerformance()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowOperationsPerformance' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowOperationsPerformanceType getShowOperationsPerformance();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowOperationsPerformance <em>Show Operations Performance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Operations Performance</em>' containment reference.
	 * @see #getShowOperationsPerformance()
	 * @generated
	 */
	void setShowOperationsPerformance(ShowOperationsPerformanceType value);

	/**
	 * Returns the value of the '<em><b>Show Operations Schedule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Operations Schedule</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Operations Schedule</em>' containment reference.
	 * @see #setShowOperationsSchedule(ShowOperationsScheduleType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowOperationsSchedule()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowOperationsSchedule' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowOperationsScheduleType getShowOperationsSchedule();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowOperationsSchedule <em>Show Operations Schedule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Operations Schedule</em>' containment reference.
	 * @see #getShowOperationsSchedule()
	 * @generated
	 */
	void setShowOperationsSchedule(ShowOperationsScheduleType value);

	/**
	 * Returns the value of the '<em><b>Show Person</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Person</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Person</em>' containment reference.
	 * @see #setShowPerson(ShowPersonType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowPerson()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowPerson' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowPersonType getShowPerson();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPerson <em>Show Person</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Person</em>' containment reference.
	 * @see #getShowPerson()
	 * @generated
	 */
	void setShowPerson(ShowPersonType value);

	/**
	 * Returns the value of the '<em><b>Show Personnel Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Personnel Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Personnel Class</em>' containment reference.
	 * @see #setShowPersonnelClass(ShowPersonnelClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowPersonnelClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowPersonnelClass' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowPersonnelClassType getShowPersonnelClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPersonnelClass <em>Show Personnel Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Personnel Class</em>' containment reference.
	 * @see #getShowPersonnelClass()
	 * @generated
	 */
	void setShowPersonnelClass(ShowPersonnelClassType value);

	/**
	 * Returns the value of the '<em><b>Show Personnel Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Personnel Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Personnel Information</em>' containment reference.
	 * @see #setShowPersonnelInformation(ShowPersonnelInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowPersonnelInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowPersonnelInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowPersonnelInformationType getShowPersonnelInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPersonnelInformation <em>Show Personnel Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Personnel Information</em>' containment reference.
	 * @see #getShowPersonnelInformation()
	 * @generated
	 */
	void setShowPersonnelInformation(ShowPersonnelInformationType value);

	/**
	 * Returns the value of the '<em><b>Show Physical Asset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Physical Asset</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Physical Asset</em>' containment reference.
	 * @see #setShowPhysicalAsset(ShowPhysicalAssetType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowPhysicalAsset()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowPhysicalAsset' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowPhysicalAssetType getShowPhysicalAsset();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPhysicalAsset <em>Show Physical Asset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Physical Asset</em>' containment reference.
	 * @see #getShowPhysicalAsset()
	 * @generated
	 */
	void setShowPhysicalAsset(ShowPhysicalAssetType value);

	/**
	 * Returns the value of the '<em><b>Show Physical Asset Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Physical Asset Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #setShowPhysicalAssetCapabilityTestSpec(ShowPhysicalAssetCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowPhysicalAssetCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowPhysicalAssetCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowPhysicalAssetCapabilityTestSpecType getShowPhysicalAssetCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPhysicalAssetCapabilityTestSpec <em>Show Physical Asset Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #getShowPhysicalAssetCapabilityTestSpec()
	 * @generated
	 */
	void setShowPhysicalAssetCapabilityTestSpec(ShowPhysicalAssetCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Show Physical Asset Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Physical Asset Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Physical Asset Class</em>' containment reference.
	 * @see #setShowPhysicalAssetClass(ShowPhysicalAssetClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowPhysicalAssetClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowPhysicalAssetClass' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowPhysicalAssetClassType getShowPhysicalAssetClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPhysicalAssetClass <em>Show Physical Asset Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Physical Asset Class</em>' containment reference.
	 * @see #getShowPhysicalAssetClass()
	 * @generated
	 */
	void setShowPhysicalAssetClass(ShowPhysicalAssetClassType value);

	/**
	 * Returns the value of the '<em><b>Show Physical Asset Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Physical Asset Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Physical Asset Information</em>' containment reference.
	 * @see #setShowPhysicalAssetInformation(ShowPhysicalAssetInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowPhysicalAssetInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowPhysicalAssetInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowPhysicalAssetInformationType getShowPhysicalAssetInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowPhysicalAssetInformation <em>Show Physical Asset Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Physical Asset Information</em>' containment reference.
	 * @see #getShowPhysicalAssetInformation()
	 * @generated
	 */
	void setShowPhysicalAssetInformation(ShowPhysicalAssetInformationType value);

	/**
	 * Returns the value of the '<em><b>Show Process Segment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Process Segment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Process Segment</em>' containment reference.
	 * @see #setShowProcessSegment(ShowProcessSegmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowProcessSegment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowProcessSegment' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowProcessSegmentType getShowProcessSegment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowProcessSegment <em>Show Process Segment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Process Segment</em>' containment reference.
	 * @see #getShowProcessSegment()
	 * @generated
	 */
	void setShowProcessSegment(ShowProcessSegmentType value);

	/**
	 * Returns the value of the '<em><b>Show Process Segment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Process Segment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Process Segment Information</em>' containment reference.
	 * @see #setShowProcessSegmentInformation(ShowProcessSegmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowProcessSegmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowProcessSegmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowProcessSegmentInformationType getShowProcessSegmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowProcessSegmentInformation <em>Show Process Segment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Process Segment Information</em>' containment reference.
	 * @see #getShowProcessSegmentInformation()
	 * @generated
	 */
	void setShowProcessSegmentInformation(ShowProcessSegmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Show Qualification Test Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show Qualification Test Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show Qualification Test Specification</em>' containment reference.
	 * @see #setShowQualificationTestSpecification(ShowQualificationTestSpecificationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_ShowQualificationTestSpecification()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ShowQualificationTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	ShowQualificationTestSpecificationType getShowQualificationTestSpecification();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getShowQualificationTestSpecification <em>Show Qualification Test Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show Qualification Test Specification</em>' containment reference.
	 * @see #getShowQualificationTestSpecification()
	 * @generated
	 */
	void setShowQualificationTestSpecification(ShowQualificationTestSpecificationType value);

	/**
	 * Returns the value of the '<em><b>Sync Equipment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Equipment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Equipment</em>' containment reference.
	 * @see #setSyncEquipment(SyncEquipmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncEquipment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncEquipment' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncEquipmentType getSyncEquipment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncEquipment <em>Sync Equipment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Equipment</em>' containment reference.
	 * @see #getSyncEquipment()
	 * @generated
	 */
	void setSyncEquipment(SyncEquipmentType value);

	/**
	 * Returns the value of the '<em><b>Sync Equipment Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Equipment Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Equipment Capability Test Spec</em>' containment reference.
	 * @see #setSyncEquipmentCapabilityTestSpec(SyncEquipmentCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncEquipmentCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncEquipmentCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncEquipmentCapabilityTestSpecType getSyncEquipmentCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncEquipmentCapabilityTestSpec <em>Sync Equipment Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Equipment Capability Test Spec</em>' containment reference.
	 * @see #getSyncEquipmentCapabilityTestSpec()
	 * @generated
	 */
	void setSyncEquipmentCapabilityTestSpec(SyncEquipmentCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Sync Equipment Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Equipment Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Equipment Class</em>' containment reference.
	 * @see #setSyncEquipmentClass(SyncEquipmentClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncEquipmentClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncEquipmentClass' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncEquipmentClassType getSyncEquipmentClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncEquipmentClass <em>Sync Equipment Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Equipment Class</em>' containment reference.
	 * @see #getSyncEquipmentClass()
	 * @generated
	 */
	void setSyncEquipmentClass(SyncEquipmentClassType value);

	/**
	 * Returns the value of the '<em><b>Sync Equipment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Equipment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Equipment Information</em>' containment reference.
	 * @see #setSyncEquipmentInformation(SyncEquipmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncEquipmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncEquipmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncEquipmentInformationType getSyncEquipmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncEquipmentInformation <em>Sync Equipment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Equipment Information</em>' containment reference.
	 * @see #getSyncEquipmentInformation()
	 * @generated
	 */
	void setSyncEquipmentInformation(SyncEquipmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Sync Material Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Material Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Material Class</em>' containment reference.
	 * @see #setSyncMaterialClass(SyncMaterialClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncMaterialClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncMaterialClass' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncMaterialClassType getSyncMaterialClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncMaterialClass <em>Sync Material Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Material Class</em>' containment reference.
	 * @see #getSyncMaterialClass()
	 * @generated
	 */
	void setSyncMaterialClass(SyncMaterialClassType value);

	/**
	 * Returns the value of the '<em><b>Sync Material Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Material Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Material Definition</em>' containment reference.
	 * @see #setSyncMaterialDefinition(SyncMaterialDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncMaterialDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncMaterialDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncMaterialDefinitionType getSyncMaterialDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncMaterialDefinition <em>Sync Material Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Material Definition</em>' containment reference.
	 * @see #getSyncMaterialDefinition()
	 * @generated
	 */
	void setSyncMaterialDefinition(SyncMaterialDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Sync Material Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Material Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Material Information</em>' containment reference.
	 * @see #setSyncMaterialInformation(SyncMaterialInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncMaterialInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncMaterialInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncMaterialInformationType getSyncMaterialInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncMaterialInformation <em>Sync Material Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Material Information</em>' containment reference.
	 * @see #getSyncMaterialInformation()
	 * @generated
	 */
	void setSyncMaterialInformation(SyncMaterialInformationType value);

	/**
	 * Returns the value of the '<em><b>Sync Material Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Material Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Material Lot</em>' containment reference.
	 * @see #setSyncMaterialLot(SyncMaterialLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncMaterialLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncMaterialLot' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncMaterialLotType getSyncMaterialLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncMaterialLot <em>Sync Material Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Material Lot</em>' containment reference.
	 * @see #getSyncMaterialLot()
	 * @generated
	 */
	void setSyncMaterialLot(SyncMaterialLotType value);

	/**
	 * Returns the value of the '<em><b>Sync Material Sub Lot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Material Sub Lot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Material Sub Lot</em>' containment reference.
	 * @see #setSyncMaterialSubLot(SyncMaterialSubLotType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncMaterialSubLot()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncMaterialSubLot' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncMaterialSubLotType getSyncMaterialSubLot();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncMaterialSubLot <em>Sync Material Sub Lot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Material Sub Lot</em>' containment reference.
	 * @see #getSyncMaterialSubLot()
	 * @generated
	 */
	void setSyncMaterialSubLot(SyncMaterialSubLotType value);

	/**
	 * Returns the value of the '<em><b>Sync Material Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Material Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Material Test Spec</em>' containment reference.
	 * @see #setSyncMaterialTestSpec(SyncMaterialTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncMaterialTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncMaterialTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncMaterialTestSpecType getSyncMaterialTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncMaterialTestSpec <em>Sync Material Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Material Test Spec</em>' containment reference.
	 * @see #getSyncMaterialTestSpec()
	 * @generated
	 */
	void setSyncMaterialTestSpec(SyncMaterialTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Sync Operations Capability</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Operations Capability</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Operations Capability</em>' containment reference.
	 * @see #setSyncOperationsCapability(SyncOperationsCapabilityType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncOperationsCapability()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncOperationsCapability' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncOperationsCapabilityType getSyncOperationsCapability();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncOperationsCapability <em>Sync Operations Capability</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Operations Capability</em>' containment reference.
	 * @see #getSyncOperationsCapability()
	 * @generated
	 */
	void setSyncOperationsCapability(SyncOperationsCapabilityType value);

	/**
	 * Returns the value of the '<em><b>Sync Operations Capability Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Operations Capability Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Operations Capability Information</em>' containment reference.
	 * @see #setSyncOperationsCapabilityInformation(SyncOperationsCapabilityInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncOperationsCapabilityInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncOperationsCapabilityInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncOperationsCapabilityInformationType getSyncOperationsCapabilityInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncOperationsCapabilityInformation <em>Sync Operations Capability Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Operations Capability Information</em>' containment reference.
	 * @see #getSyncOperationsCapabilityInformation()
	 * @generated
	 */
	void setSyncOperationsCapabilityInformation(SyncOperationsCapabilityInformationType value);

	/**
	 * Returns the value of the '<em><b>Sync Operations Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Operations Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Operations Definition</em>' containment reference.
	 * @see #setSyncOperationsDefinition(SyncOperationsDefinitionType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncOperationsDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncOperationsDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncOperationsDefinitionType getSyncOperationsDefinition();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncOperationsDefinition <em>Sync Operations Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Operations Definition</em>' containment reference.
	 * @see #getSyncOperationsDefinition()
	 * @generated
	 */
	void setSyncOperationsDefinition(SyncOperationsDefinitionType value);

	/**
	 * Returns the value of the '<em><b>Sync Operations Definition Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Operations Definition Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Operations Definition Information</em>' containment reference.
	 * @see #setSyncOperationsDefinitionInformation(SyncOperationsDefinitionInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncOperationsDefinitionInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncOperationsDefinitionInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncOperationsDefinitionInformationType getSyncOperationsDefinitionInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncOperationsDefinitionInformation <em>Sync Operations Definition Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Operations Definition Information</em>' containment reference.
	 * @see #getSyncOperationsDefinitionInformation()
	 * @generated
	 */
	void setSyncOperationsDefinitionInformation(SyncOperationsDefinitionInformationType value);

	/**
	 * Returns the value of the '<em><b>Sync Operations Performance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Operations Performance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Operations Performance</em>' containment reference.
	 * @see #setSyncOperationsPerformance(SyncOperationsPerformanceType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncOperationsPerformance()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncOperationsPerformance' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncOperationsPerformanceType getSyncOperationsPerformance();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncOperationsPerformance <em>Sync Operations Performance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Operations Performance</em>' containment reference.
	 * @see #getSyncOperationsPerformance()
	 * @generated
	 */
	void setSyncOperationsPerformance(SyncOperationsPerformanceType value);

	/**
	 * Returns the value of the '<em><b>Sync Operations Schedule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Operations Schedule</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Operations Schedule</em>' containment reference.
	 * @see #setSyncOperationsSchedule(SyncOperationsScheduleType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncOperationsSchedule()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncOperationsSchedule' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncOperationsScheduleType getSyncOperationsSchedule();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncOperationsSchedule <em>Sync Operations Schedule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Operations Schedule</em>' containment reference.
	 * @see #getSyncOperationsSchedule()
	 * @generated
	 */
	void setSyncOperationsSchedule(SyncOperationsScheduleType value);

	/**
	 * Returns the value of the '<em><b>Sync Person</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Person</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Person</em>' containment reference.
	 * @see #setSyncPerson(SyncPersonType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncPerson()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncPerson' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncPersonType getSyncPerson();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPerson <em>Sync Person</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Person</em>' containment reference.
	 * @see #getSyncPerson()
	 * @generated
	 */
	void setSyncPerson(SyncPersonType value);

	/**
	 * Returns the value of the '<em><b>Sync Personnel Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Personnel Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Personnel Class</em>' containment reference.
	 * @see #setSyncPersonnelClass(SyncPersonnelClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncPersonnelClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncPersonnelClass' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncPersonnelClassType getSyncPersonnelClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPersonnelClass <em>Sync Personnel Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Personnel Class</em>' containment reference.
	 * @see #getSyncPersonnelClass()
	 * @generated
	 */
	void setSyncPersonnelClass(SyncPersonnelClassType value);

	/**
	 * Returns the value of the '<em><b>Sync Personnel Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Personnel Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Personnel Information</em>' containment reference.
	 * @see #setSyncPersonnelInformation(SyncPersonnelInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncPersonnelInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncPersonnelInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncPersonnelInformationType getSyncPersonnelInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPersonnelInformation <em>Sync Personnel Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Personnel Information</em>' containment reference.
	 * @see #getSyncPersonnelInformation()
	 * @generated
	 */
	void setSyncPersonnelInformation(SyncPersonnelInformationType value);

	/**
	 * Returns the value of the '<em><b>Sync Physical Asset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Physical Asset</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Physical Asset</em>' containment reference.
	 * @see #setSyncPhysicalAsset(SyncPhysicalAssetType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncPhysicalAsset()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncPhysicalAsset' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncPhysicalAssetType getSyncPhysicalAsset();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPhysicalAsset <em>Sync Physical Asset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Physical Asset</em>' containment reference.
	 * @see #getSyncPhysicalAsset()
	 * @generated
	 */
	void setSyncPhysicalAsset(SyncPhysicalAssetType value);

	/**
	 * Returns the value of the '<em><b>Sync Physical Asset Capability Test Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Physical Asset Capability Test Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #setSyncPhysicalAssetCapabilityTestSpec(SyncPhysicalAssetCapabilityTestSpecType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncPhysicalAssetCapabilityTestSpec()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncPhysicalAssetCapabilityTestSpec' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncPhysicalAssetCapabilityTestSpecType getSyncPhysicalAssetCapabilityTestSpec();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPhysicalAssetCapabilityTestSpec <em>Sync Physical Asset Capability Test Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Physical Asset Capability Test Spec</em>' containment reference.
	 * @see #getSyncPhysicalAssetCapabilityTestSpec()
	 * @generated
	 */
	void setSyncPhysicalAssetCapabilityTestSpec(SyncPhysicalAssetCapabilityTestSpecType value);

	/**
	 * Returns the value of the '<em><b>Sync Physical Asset Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Physical Asset Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Physical Asset Class</em>' containment reference.
	 * @see #setSyncPhysicalAssetClass(SyncPhysicalAssetClassType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncPhysicalAssetClass()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncPhysicalAssetClass' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncPhysicalAssetClassType getSyncPhysicalAssetClass();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPhysicalAssetClass <em>Sync Physical Asset Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Physical Asset Class</em>' containment reference.
	 * @see #getSyncPhysicalAssetClass()
	 * @generated
	 */
	void setSyncPhysicalAssetClass(SyncPhysicalAssetClassType value);

	/**
	 * Returns the value of the '<em><b>Sync Physical Asset Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Physical Asset Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Physical Asset Information</em>' containment reference.
	 * @see #setSyncPhysicalAssetInformation(SyncPhysicalAssetInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncPhysicalAssetInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncPhysicalAssetInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncPhysicalAssetInformationType getSyncPhysicalAssetInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncPhysicalAssetInformation <em>Sync Physical Asset Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Physical Asset Information</em>' containment reference.
	 * @see #getSyncPhysicalAssetInformation()
	 * @generated
	 */
	void setSyncPhysicalAssetInformation(SyncPhysicalAssetInformationType value);

	/**
	 * Returns the value of the '<em><b>Sync Process Segment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Process Segment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Process Segment</em>' containment reference.
	 * @see #setSyncProcessSegment(SyncProcessSegmentType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncProcessSegment()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncProcessSegment' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncProcessSegmentType getSyncProcessSegment();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncProcessSegment <em>Sync Process Segment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Process Segment</em>' containment reference.
	 * @see #getSyncProcessSegment()
	 * @generated
	 */
	void setSyncProcessSegment(SyncProcessSegmentType value);

	/**
	 * Returns the value of the '<em><b>Sync Process Segment Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Process Segment Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Process Segment Information</em>' containment reference.
	 * @see #setSyncProcessSegmentInformation(SyncProcessSegmentInformationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncProcessSegmentInformation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncProcessSegmentInformation' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncProcessSegmentInformationType getSyncProcessSegmentInformation();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncProcessSegmentInformation <em>Sync Process Segment Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Process Segment Information</em>' containment reference.
	 * @see #getSyncProcessSegmentInformation()
	 * @generated
	 */
	void setSyncProcessSegmentInformation(SyncProcessSegmentInformationType value);

	/**
	 * Returns the value of the '<em><b>Sync Qualification Test Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Qualification Test Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Qualification Test Specification</em>' containment reference.
	 * @see #setSyncQualificationTestSpecification(SyncQualificationTestSpecificationType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getDocumentRoot_SyncQualificationTestSpecification()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SyncQualificationTestSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	SyncQualificationTestSpecificationType getSyncQualificationTestSpecification();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.DocumentRoot#getSyncQualificationTestSpecification <em>Sync Qualification Test Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Qualification Test Specification</em>' containment reference.
	 * @see #getSyncQualificationTestSpecification()
	 * @generated
	 */
	void setSyncQualificationTestSpecification(SyncQualificationTestSpecificationType value);

} // DocumentRoot
