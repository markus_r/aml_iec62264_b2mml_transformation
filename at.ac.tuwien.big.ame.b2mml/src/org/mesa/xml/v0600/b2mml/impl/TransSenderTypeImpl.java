/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.TransConfirmationCodeType;
import org.mesa.xml.v0600.b2mml.TransSenderType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trans Sender Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransSenderTypeImpl#getLogicalID <em>Logical ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransSenderTypeImpl#getComponentID <em>Component ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransSenderTypeImpl#getTaskID <em>Task ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransSenderTypeImpl#getReferenceID <em>Reference ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransSenderTypeImpl#getConfirmationCode <em>Confirmation Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransSenderTypeImpl#getAuthorizationID <em>Authorization ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransSenderTypeImpl extends MinimalEObjectImpl.Container implements TransSenderType {
	/**
	 * The cached value of the '{@link #getLogicalID() <em>Logical ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogicalID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType logicalID;

	/**
	 * The cached value of the '{@link #getComponentID() <em>Component ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType componentID;

	/**
	 * The cached value of the '{@link #getTaskID() <em>Task ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType taskID;

	/**
	 * The cached value of the '{@link #getReferenceID() <em>Reference ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType referenceID;

	/**
	 * The cached value of the '{@link #getConfirmationCode() <em>Confirmation Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfirmationCode()
	 * @generated
	 * @ordered
	 */
	protected TransConfirmationCodeType confirmationCode;

	/**
	 * The cached value of the '{@link #getAuthorizationID() <em>Authorization ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAuthorizationID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType authorizationID;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransSenderTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTransSenderType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getLogicalID() {
		return logicalID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLogicalID(IdentifierType newLogicalID, NotificationChain msgs) {
		IdentifierType oldLogicalID = logicalID;
		logicalID = newLogicalID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SENDER_TYPE__LOGICAL_ID, oldLogicalID, newLogicalID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLogicalID(IdentifierType newLogicalID) {
		if (newLogicalID != logicalID) {
			NotificationChain msgs = null;
			if (logicalID != null)
				msgs = ((InternalEObject)logicalID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_SENDER_TYPE__LOGICAL_ID, null, msgs);
			if (newLogicalID != null)
				msgs = ((InternalEObject)newLogicalID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_SENDER_TYPE__LOGICAL_ID, null, msgs);
			msgs = basicSetLogicalID(newLogicalID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SENDER_TYPE__LOGICAL_ID, newLogicalID, newLogicalID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getComponentID() {
		return componentID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentID(IdentifierType newComponentID, NotificationChain msgs) {
		IdentifierType oldComponentID = componentID;
		componentID = newComponentID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SENDER_TYPE__COMPONENT_ID, oldComponentID, newComponentID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentID(IdentifierType newComponentID) {
		if (newComponentID != componentID) {
			NotificationChain msgs = null;
			if (componentID != null)
				msgs = ((InternalEObject)componentID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_SENDER_TYPE__COMPONENT_ID, null, msgs);
			if (newComponentID != null)
				msgs = ((InternalEObject)newComponentID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_SENDER_TYPE__COMPONENT_ID, null, msgs);
			msgs = basicSetComponentID(newComponentID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SENDER_TYPE__COMPONENT_ID, newComponentID, newComponentID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getTaskID() {
		return taskID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskID(IdentifierType newTaskID, NotificationChain msgs) {
		IdentifierType oldTaskID = taskID;
		taskID = newTaskID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SENDER_TYPE__TASK_ID, oldTaskID, newTaskID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskID(IdentifierType newTaskID) {
		if (newTaskID != taskID) {
			NotificationChain msgs = null;
			if (taskID != null)
				msgs = ((InternalEObject)taskID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_SENDER_TYPE__TASK_ID, null, msgs);
			if (newTaskID != null)
				msgs = ((InternalEObject)newTaskID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_SENDER_TYPE__TASK_ID, null, msgs);
			msgs = basicSetTaskID(newTaskID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SENDER_TYPE__TASK_ID, newTaskID, newTaskID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getReferenceID() {
		return referenceID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReferenceID(IdentifierType newReferenceID, NotificationChain msgs) {
		IdentifierType oldReferenceID = referenceID;
		referenceID = newReferenceID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SENDER_TYPE__REFERENCE_ID, oldReferenceID, newReferenceID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceID(IdentifierType newReferenceID) {
		if (newReferenceID != referenceID) {
			NotificationChain msgs = null;
			if (referenceID != null)
				msgs = ((InternalEObject)referenceID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_SENDER_TYPE__REFERENCE_ID, null, msgs);
			if (newReferenceID != null)
				msgs = ((InternalEObject)newReferenceID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_SENDER_TYPE__REFERENCE_ID, null, msgs);
			msgs = basicSetReferenceID(newReferenceID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SENDER_TYPE__REFERENCE_ID, newReferenceID, newReferenceID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransConfirmationCodeType getConfirmationCode() {
		return confirmationCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConfirmationCode(TransConfirmationCodeType newConfirmationCode, NotificationChain msgs) {
		TransConfirmationCodeType oldConfirmationCode = confirmationCode;
		confirmationCode = newConfirmationCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SENDER_TYPE__CONFIRMATION_CODE, oldConfirmationCode, newConfirmationCode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfirmationCode(TransConfirmationCodeType newConfirmationCode) {
		if (newConfirmationCode != confirmationCode) {
			NotificationChain msgs = null;
			if (confirmationCode != null)
				msgs = ((InternalEObject)confirmationCode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_SENDER_TYPE__CONFIRMATION_CODE, null, msgs);
			if (newConfirmationCode != null)
				msgs = ((InternalEObject)newConfirmationCode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_SENDER_TYPE__CONFIRMATION_CODE, null, msgs);
			msgs = basicSetConfirmationCode(newConfirmationCode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SENDER_TYPE__CONFIRMATION_CODE, newConfirmationCode, newConfirmationCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getAuthorizationID() {
		return authorizationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAuthorizationID(IdentifierType newAuthorizationID, NotificationChain msgs) {
		IdentifierType oldAuthorizationID = authorizationID;
		authorizationID = newAuthorizationID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SENDER_TYPE__AUTHORIZATION_ID, oldAuthorizationID, newAuthorizationID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAuthorizationID(IdentifierType newAuthorizationID) {
		if (newAuthorizationID != authorizationID) {
			NotificationChain msgs = null;
			if (authorizationID != null)
				msgs = ((InternalEObject)authorizationID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_SENDER_TYPE__AUTHORIZATION_ID, null, msgs);
			if (newAuthorizationID != null)
				msgs = ((InternalEObject)newAuthorizationID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_SENDER_TYPE__AUTHORIZATION_ID, null, msgs);
			msgs = basicSetAuthorizationID(newAuthorizationID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SENDER_TYPE__AUTHORIZATION_ID, newAuthorizationID, newAuthorizationID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TRANS_SENDER_TYPE__LOGICAL_ID:
				return basicSetLogicalID(null, msgs);
			case B2MMLPackage.TRANS_SENDER_TYPE__COMPONENT_ID:
				return basicSetComponentID(null, msgs);
			case B2MMLPackage.TRANS_SENDER_TYPE__TASK_ID:
				return basicSetTaskID(null, msgs);
			case B2MMLPackage.TRANS_SENDER_TYPE__REFERENCE_ID:
				return basicSetReferenceID(null, msgs);
			case B2MMLPackage.TRANS_SENDER_TYPE__CONFIRMATION_CODE:
				return basicSetConfirmationCode(null, msgs);
			case B2MMLPackage.TRANS_SENDER_TYPE__AUTHORIZATION_ID:
				return basicSetAuthorizationID(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TRANS_SENDER_TYPE__LOGICAL_ID:
				return getLogicalID();
			case B2MMLPackage.TRANS_SENDER_TYPE__COMPONENT_ID:
				return getComponentID();
			case B2MMLPackage.TRANS_SENDER_TYPE__TASK_ID:
				return getTaskID();
			case B2MMLPackage.TRANS_SENDER_TYPE__REFERENCE_ID:
				return getReferenceID();
			case B2MMLPackage.TRANS_SENDER_TYPE__CONFIRMATION_CODE:
				return getConfirmationCode();
			case B2MMLPackage.TRANS_SENDER_TYPE__AUTHORIZATION_ID:
				return getAuthorizationID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TRANS_SENDER_TYPE__LOGICAL_ID:
				setLogicalID((IdentifierType)newValue);
				return;
			case B2MMLPackage.TRANS_SENDER_TYPE__COMPONENT_ID:
				setComponentID((IdentifierType)newValue);
				return;
			case B2MMLPackage.TRANS_SENDER_TYPE__TASK_ID:
				setTaskID((IdentifierType)newValue);
				return;
			case B2MMLPackage.TRANS_SENDER_TYPE__REFERENCE_ID:
				setReferenceID((IdentifierType)newValue);
				return;
			case B2MMLPackage.TRANS_SENDER_TYPE__CONFIRMATION_CODE:
				setConfirmationCode((TransConfirmationCodeType)newValue);
				return;
			case B2MMLPackage.TRANS_SENDER_TYPE__AUTHORIZATION_ID:
				setAuthorizationID((IdentifierType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_SENDER_TYPE__LOGICAL_ID:
				setLogicalID((IdentifierType)null);
				return;
			case B2MMLPackage.TRANS_SENDER_TYPE__COMPONENT_ID:
				setComponentID((IdentifierType)null);
				return;
			case B2MMLPackage.TRANS_SENDER_TYPE__TASK_ID:
				setTaskID((IdentifierType)null);
				return;
			case B2MMLPackage.TRANS_SENDER_TYPE__REFERENCE_ID:
				setReferenceID((IdentifierType)null);
				return;
			case B2MMLPackage.TRANS_SENDER_TYPE__CONFIRMATION_CODE:
				setConfirmationCode((TransConfirmationCodeType)null);
				return;
			case B2MMLPackage.TRANS_SENDER_TYPE__AUTHORIZATION_ID:
				setAuthorizationID((IdentifierType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_SENDER_TYPE__LOGICAL_ID:
				return logicalID != null;
			case B2MMLPackage.TRANS_SENDER_TYPE__COMPONENT_ID:
				return componentID != null;
			case B2MMLPackage.TRANS_SENDER_TYPE__TASK_ID:
				return taskID != null;
			case B2MMLPackage.TRANS_SENDER_TYPE__REFERENCE_ID:
				return referenceID != null;
			case B2MMLPackage.TRANS_SENDER_TYPE__CONFIRMATION_CODE:
				return confirmationCode != null;
			case B2MMLPackage.TRANS_SENDER_TYPE__AUTHORIZATION_ID:
				return authorizationID != null;
		}
		return super.eIsSet(featureID);
	}

} //TransSenderTypeImpl
