/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.AssemblyRelationshipType;
import org.mesa.xml.v0600.b2mml.AssemblyTypeType;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.MaterialClassIDType;
import org.mesa.xml.v0600.b2mml.MaterialDefinitionIDType;
import org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationPropertyType;
import org.mesa.xml.v0600.b2mml.MaterialSegmentSpecificationType;
import org.mesa.xml.v0600.b2mml.MaterialUseType;
import org.mesa.xml.v0600.b2mml.QuantityValueType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Segment Specification Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSegmentSpecificationTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSegmentSpecificationTypeImpl#getMaterialClassID <em>Material Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSegmentSpecificationTypeImpl#getMaterialDefinitionID <em>Material Definition ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSegmentSpecificationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSegmentSpecificationTypeImpl#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSegmentSpecificationTypeImpl#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSegmentSpecificationTypeImpl#getAssemblySpecificationID <em>Assembly Specification ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSegmentSpecificationTypeImpl#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSegmentSpecificationTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSegmentSpecificationTypeImpl#getMaterialSegmentSpecificationProperty <em>Material Segment Specification Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MaterialSegmentSpecificationTypeImpl extends MinimalEObjectImpl.Container implements MaterialSegmentSpecificationType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getMaterialClassID() <em>Material Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClassID()
	 * @generated
	 * @ordered
	 */
	protected MaterialClassIDType materialClassID;

	/**
	 * The cached value of the '{@link #getMaterialDefinitionID() <em>Material Definition ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinitionID()
	 * @generated
	 * @ordered
	 */
	protected MaterialDefinitionIDType materialDefinitionID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected AssemblyTypeType assemblyType;

	/**
	 * The cached value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected AssemblyRelationshipType assemblyRelationship;

	/**
	 * The cached value of the '{@link #getAssemblySpecificationID() <em>Assembly Specification ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblySpecificationID()
	 * @generated
	 * @ordered
	 */
	protected EList<IdentifierType> assemblySpecificationID;

	/**
	 * The cached value of the '{@link #getMaterialUse() <em>Material Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialUse()
	 * @generated
	 * @ordered
	 */
	protected MaterialUseType materialUse;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getMaterialSegmentSpecificationProperty() <em>Material Segment Specification Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSegmentSpecificationProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSegmentSpecificationPropertyType> materialSegmentSpecificationProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialSegmentSpecificationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getMaterialSegmentSpecificationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialClassIDType getMaterialClassID() {
		return materialClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialClassID(MaterialClassIDType newMaterialClassID, NotificationChain msgs) {
		MaterialClassIDType oldMaterialClassID = materialClassID;
		materialClassID = newMaterialClassID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_CLASS_ID, oldMaterialClassID, newMaterialClassID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialClassID(MaterialClassIDType newMaterialClassID) {
		if (newMaterialClassID != materialClassID) {
			NotificationChain msgs = null;
			if (materialClassID != null)
				msgs = ((InternalEObject)materialClassID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_CLASS_ID, null, msgs);
			if (newMaterialClassID != null)
				msgs = ((InternalEObject)newMaterialClassID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_CLASS_ID, null, msgs);
			msgs = basicSetMaterialClassID(newMaterialClassID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_CLASS_ID, newMaterialClassID, newMaterialClassID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialDefinitionIDType getMaterialDefinitionID() {
		return materialDefinitionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialDefinitionID(MaterialDefinitionIDType newMaterialDefinitionID, NotificationChain msgs) {
		MaterialDefinitionIDType oldMaterialDefinitionID = materialDefinitionID;
		materialDefinitionID = newMaterialDefinitionID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID, oldMaterialDefinitionID, newMaterialDefinitionID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialDefinitionID(MaterialDefinitionIDType newMaterialDefinitionID) {
		if (newMaterialDefinitionID != materialDefinitionID) {
			NotificationChain msgs = null;
			if (materialDefinitionID != null)
				msgs = ((InternalEObject)materialDefinitionID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID, null, msgs);
			if (newMaterialDefinitionID != null)
				msgs = ((InternalEObject)newMaterialDefinitionID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID, null, msgs);
			msgs = basicSetMaterialDefinitionID(newMaterialDefinitionID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID, newMaterialDefinitionID, newMaterialDefinitionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyTypeType getAssemblyType() {
		return assemblyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyType(AssemblyTypeType newAssemblyType, NotificationChain msgs) {
		AssemblyTypeType oldAssemblyType = assemblyType;
		assemblyType = newAssemblyType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_TYPE, oldAssemblyType, newAssemblyType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyType(AssemblyTypeType newAssemblyType) {
		if (newAssemblyType != assemblyType) {
			NotificationChain msgs = null;
			if (assemblyType != null)
				msgs = ((InternalEObject)assemblyType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_TYPE, null, msgs);
			if (newAssemblyType != null)
				msgs = ((InternalEObject)newAssemblyType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_TYPE, null, msgs);
			msgs = basicSetAssemblyType(newAssemblyType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_TYPE, newAssemblyType, newAssemblyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationshipType getAssemblyRelationship() {
		return assemblyRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship, NotificationChain msgs) {
		AssemblyRelationshipType oldAssemblyRelationship = assemblyRelationship;
		assemblyRelationship = newAssemblyRelationship;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP, oldAssemblyRelationship, newAssemblyRelationship);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship) {
		if (newAssemblyRelationship != assemblyRelationship) {
			NotificationChain msgs = null;
			if (assemblyRelationship != null)
				msgs = ((InternalEObject)assemblyRelationship).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			if (newAssemblyRelationship != null)
				msgs = ((InternalEObject)newAssemblyRelationship).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			msgs = basicSetAssemblyRelationship(newAssemblyRelationship, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP, newAssemblyRelationship, newAssemblyRelationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IdentifierType> getAssemblySpecificationID() {
		if (assemblySpecificationID == null) {
			assemblySpecificationID = new EObjectContainmentEList<IdentifierType>(IdentifierType.class, this, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_SPECIFICATION_ID);
		}
		return assemblySpecificationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialUseType getMaterialUse() {
		return materialUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialUse(MaterialUseType newMaterialUse, NotificationChain msgs) {
		MaterialUseType oldMaterialUse = materialUse;
		materialUse = newMaterialUse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_USE, oldMaterialUse, newMaterialUse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialUse(MaterialUseType newMaterialUse) {
		if (newMaterialUse != materialUse) {
			NotificationChain msgs = null;
			if (materialUse != null)
				msgs = ((InternalEObject)materialUse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_USE, null, msgs);
			if (newMaterialUse != null)
				msgs = ((InternalEObject)newMaterialUse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_USE, null, msgs);
			msgs = basicSetMaterialUse(newMaterialUse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_USE, newMaterialUse, newMaterialUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSegmentSpecificationPropertyType> getMaterialSegmentSpecificationProperty() {
		if (materialSegmentSpecificationProperty == null) {
			materialSegmentSpecificationProperty = new EObjectContainmentEList<MaterialSegmentSpecificationPropertyType>(MaterialSegmentSpecificationPropertyType.class, this, B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_SEGMENT_SPECIFICATION_PROPERTY);
		}
		return materialSegmentSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_CLASS_ID:
				return basicSetMaterialClassID(null, msgs);
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID:
				return basicSetMaterialDefinitionID(null, msgs);
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_TYPE:
				return basicSetAssemblyType(null, msgs);
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP:
				return basicSetAssemblyRelationship(null, msgs);
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_SPECIFICATION_ID:
				return ((InternalEList<?>)getAssemblySpecificationID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_USE:
				return basicSetMaterialUse(null, msgs);
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_SEGMENT_SPECIFICATION_PROPERTY:
				return ((InternalEList<?>)getMaterialSegmentSpecificationProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ID:
				return getID();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_CLASS_ID:
				return getMaterialClassID();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID:
				return getMaterialDefinitionID();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_TYPE:
				return getAssemblyType();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP:
				return getAssemblyRelationship();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_SPECIFICATION_ID:
				return getAssemblySpecificationID();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_USE:
				return getMaterialUse();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_SEGMENT_SPECIFICATION_PROPERTY:
				return getMaterialSegmentSpecificationProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_CLASS_ID:
				setMaterialClassID((MaterialClassIDType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID:
				setMaterialDefinitionID((MaterialDefinitionIDType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_SPECIFICATION_ID:
				getAssemblySpecificationID().clear();
				getAssemblySpecificationID().addAll((Collection<? extends IdentifierType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_USE:
				setMaterialUse((MaterialUseType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_SEGMENT_SPECIFICATION_PROPERTY:
				getMaterialSegmentSpecificationProperty().clear();
				getMaterialSegmentSpecificationProperty().addAll((Collection<? extends MaterialSegmentSpecificationPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_CLASS_ID:
				setMaterialClassID((MaterialClassIDType)null);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID:
				setMaterialDefinitionID((MaterialDefinitionIDType)null);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)null);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)null);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_SPECIFICATION_ID:
				getAssemblySpecificationID().clear();
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_USE:
				setMaterialUse((MaterialUseType)null);
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_SEGMENT_SPECIFICATION_PROPERTY:
				getMaterialSegmentSpecificationProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ID:
				return iD != null;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_CLASS_ID:
				return materialClassID != null;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_DEFINITION_ID:
				return materialDefinitionID != null;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_TYPE:
				return assemblyType != null;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_RELATIONSHIP:
				return assemblyRelationship != null;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__ASSEMBLY_SPECIFICATION_ID:
				return assemblySpecificationID != null && !assemblySpecificationID.isEmpty();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_USE:
				return materialUse != null;
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE__MATERIAL_SEGMENT_SPECIFICATION_PROPERTY:
				return materialSegmentSpecificationProperty != null && !materialSegmentSpecificationProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MaterialSegmentSpecificationTypeImpl
