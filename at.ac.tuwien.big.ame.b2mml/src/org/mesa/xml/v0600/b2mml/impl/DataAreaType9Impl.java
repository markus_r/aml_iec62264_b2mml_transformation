/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType9;
import org.mesa.xml.v0600.b2mml.EquipmentCapabilityTestSpecificationType;
import org.mesa.xml.v0600.b2mml.TransProcessType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type9</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType9Impl#getProcess <em>Process</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType9Impl#getEquipmentCapabilityTestSpec <em>Equipment Capability Test Spec</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType9Impl extends MinimalEObjectImpl.Container implements DataAreaType9 {
	/**
	 * The cached value of the '{@link #getProcess() <em>Process</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcess()
	 * @generated
	 * @ordered
	 */
	protected TransProcessType process;

	/**
	 * The cached value of the '{@link #getEquipmentCapabilityTestSpec() <em>Equipment Capability Test Spec</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentCapabilityTestSpec()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentCapabilityTestSpecificationType> equipmentCapabilityTestSpec;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType9Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType9();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransProcessType getProcess() {
		return process;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcess(TransProcessType newProcess, NotificationChain msgs) {
		TransProcessType oldProcess = process;
		process = newProcess;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE9__PROCESS, oldProcess, newProcess);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcess(TransProcessType newProcess) {
		if (newProcess != process) {
			NotificationChain msgs = null;
			if (process != null)
				msgs = ((InternalEObject)process).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE9__PROCESS, null, msgs);
			if (newProcess != null)
				msgs = ((InternalEObject)newProcess).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE9__PROCESS, null, msgs);
			msgs = basicSetProcess(newProcess, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE9__PROCESS, newProcess, newProcess));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentCapabilityTestSpecificationType> getEquipmentCapabilityTestSpec() {
		if (equipmentCapabilityTestSpec == null) {
			equipmentCapabilityTestSpec = new EObjectContainmentEList<EquipmentCapabilityTestSpecificationType>(EquipmentCapabilityTestSpecificationType.class, this, B2MMLPackage.DATA_AREA_TYPE9__EQUIPMENT_CAPABILITY_TEST_SPEC);
		}
		return equipmentCapabilityTestSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE9__PROCESS:
				return basicSetProcess(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE9__EQUIPMENT_CAPABILITY_TEST_SPEC:
				return ((InternalEList<?>)getEquipmentCapabilityTestSpec()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE9__PROCESS:
				return getProcess();
			case B2MMLPackage.DATA_AREA_TYPE9__EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getEquipmentCapabilityTestSpec();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE9__PROCESS:
				setProcess((TransProcessType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE9__EQUIPMENT_CAPABILITY_TEST_SPEC:
				getEquipmentCapabilityTestSpec().clear();
				getEquipmentCapabilityTestSpec().addAll((Collection<? extends EquipmentCapabilityTestSpecificationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE9__PROCESS:
				setProcess((TransProcessType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE9__EQUIPMENT_CAPABILITY_TEST_SPEC:
				getEquipmentCapabilityTestSpec().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE9__PROCESS:
				return process != null;
			case B2MMLPackage.DATA_AREA_TYPE9__EQUIPMENT_CAPABILITY_TEST_SPEC:
				return equipmentCapabilityTestSpec != null && !equipmentCapabilityTestSpec.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType9Impl
