/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType1;
import org.mesa.xml.v0600.b2mml.OperationsCapabilityInformationType;
import org.mesa.xml.v0600.b2mml.TransGetType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType1Impl#getGet <em>Get</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType1Impl#getOperationsCapabilityInformation <em>Operations Capability Information</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType1Impl extends MinimalEObjectImpl.Container implements DataAreaType1 {
	/**
	 * The cached value of the '{@link #getGet() <em>Get</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGet()
	 * @generated
	 * @ordered
	 */
	protected TransGetType get;

	/**
	 * The cached value of the '{@link #getOperationsCapabilityInformation() <em>Operations Capability Information</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsCapabilityInformation()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsCapabilityInformationType> operationsCapabilityInformation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransGetType getGet() {
		return get;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGet(TransGetType newGet, NotificationChain msgs) {
		TransGetType oldGet = get;
		get = newGet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE1__GET, oldGet, newGet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGet(TransGetType newGet) {
		if (newGet != get) {
			NotificationChain msgs = null;
			if (get != null)
				msgs = ((InternalEObject)get).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE1__GET, null, msgs);
			if (newGet != null)
				msgs = ((InternalEObject)newGet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE1__GET, null, msgs);
			msgs = basicSetGet(newGet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE1__GET, newGet, newGet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsCapabilityInformationType> getOperationsCapabilityInformation() {
		if (operationsCapabilityInformation == null) {
			operationsCapabilityInformation = new EObjectContainmentEList<OperationsCapabilityInformationType>(OperationsCapabilityInformationType.class, this, B2MMLPackage.DATA_AREA_TYPE1__OPERATIONS_CAPABILITY_INFORMATION);
		}
		return operationsCapabilityInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE1__GET:
				return basicSetGet(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE1__OPERATIONS_CAPABILITY_INFORMATION:
				return ((InternalEList<?>)getOperationsCapabilityInformation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE1__GET:
				return getGet();
			case B2MMLPackage.DATA_AREA_TYPE1__OPERATIONS_CAPABILITY_INFORMATION:
				return getOperationsCapabilityInformation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE1__GET:
				setGet((TransGetType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE1__OPERATIONS_CAPABILITY_INFORMATION:
				getOperationsCapabilityInformation().clear();
				getOperationsCapabilityInformation().addAll((Collection<? extends OperationsCapabilityInformationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE1__GET:
				setGet((TransGetType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE1__OPERATIONS_CAPABILITY_INFORMATION:
				getOperationsCapabilityInformation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE1__GET:
				return get != null;
			case B2MMLPackage.DATA_AREA_TYPE1__OPERATIONS_CAPABILITY_INFORMATION:
				return operationsCapabilityInformation != null && !operationsCapabilityInformation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType1Impl
