/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.BillOfMaterialIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bill Of Material ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BillOfMaterialIDTypeImpl extends IdentifierTypeImpl implements BillOfMaterialIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BillOfMaterialIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getBillOfMaterialIDType();
	}

} //BillOfMaterialIDTypeImpl
