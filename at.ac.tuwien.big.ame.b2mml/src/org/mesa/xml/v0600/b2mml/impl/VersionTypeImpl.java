/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.VersionType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Version Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VersionTypeImpl extends IdentifierTypeImpl implements VersionType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VersionTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getVersionType();
	}

} //VersionTypeImpl
