/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.AssemblyType1Type;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assembly Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AssemblyType1TypeImpl extends CodeTypeImpl implements AssemblyType1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssemblyType1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getAssemblyType1Type();
	}

} //AssemblyType1TypeImpl
