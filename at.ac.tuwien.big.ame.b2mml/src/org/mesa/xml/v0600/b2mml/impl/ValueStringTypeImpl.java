/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.ValueStringType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Value String Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ValueStringTypeImpl extends AnyGenericValueTypeImpl implements ValueStringType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ValueStringTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getValueStringType();
	}

} //ValueStringTypeImpl
