/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.EquipmentElementLevelType;
import org.mesa.xml.v0600.b2mml.EquipmentIDType;
import org.mesa.xml.v0600.b2mml.LocationType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Location Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.LocationTypeImpl#getEquipmentID <em>Equipment ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.LocationTypeImpl#getEquipmentElementLevel <em>Equipment Element Level</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.LocationTypeImpl#getLocation <em>Location</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocationTypeImpl extends MinimalEObjectImpl.Container implements LocationType {
	/**
	 * The cached value of the '{@link #getEquipmentID() <em>Equipment ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentID()
	 * @generated
	 * @ordered
	 */
	protected EquipmentIDType equipmentID;

	/**
	 * The cached value of the '{@link #getEquipmentElementLevel() <em>Equipment Element Level</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentElementLevel()
	 * @generated
	 * @ordered
	 */
	protected EquipmentElementLevelType equipmentElementLevel;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected LocationType location;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getLocationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentIDType getEquipmentID() {
		return equipmentID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentID(EquipmentIDType newEquipmentID, NotificationChain msgs) {
		EquipmentIDType oldEquipmentID = equipmentID;
		equipmentID = newEquipmentID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ID, oldEquipmentID, newEquipmentID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentID(EquipmentIDType newEquipmentID) {
		if (newEquipmentID != equipmentID) {
			NotificationChain msgs = null;
			if (equipmentID != null)
				msgs = ((InternalEObject)equipmentID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ID, null, msgs);
			if (newEquipmentID != null)
				msgs = ((InternalEObject)newEquipmentID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ID, null, msgs);
			msgs = basicSetEquipmentID(newEquipmentID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ID, newEquipmentID, newEquipmentID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentElementLevelType getEquipmentElementLevel() {
		return equipmentElementLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentElementLevel(EquipmentElementLevelType newEquipmentElementLevel, NotificationChain msgs) {
		EquipmentElementLevelType oldEquipmentElementLevel = equipmentElementLevel;
		equipmentElementLevel = newEquipmentElementLevel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ELEMENT_LEVEL, oldEquipmentElementLevel, newEquipmentElementLevel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentElementLevel(EquipmentElementLevelType newEquipmentElementLevel) {
		if (newEquipmentElementLevel != equipmentElementLevel) {
			NotificationChain msgs = null;
			if (equipmentElementLevel != null)
				msgs = ((InternalEObject)equipmentElementLevel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ELEMENT_LEVEL, null, msgs);
			if (newEquipmentElementLevel != null)
				msgs = ((InternalEObject)newEquipmentElementLevel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ELEMENT_LEVEL, null, msgs);
			msgs = basicSetEquipmentElementLevel(newEquipmentElementLevel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ELEMENT_LEVEL, newEquipmentElementLevel, newEquipmentElementLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationType getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocation(LocationType newLocation, NotificationChain msgs) {
		LocationType oldLocation = location;
		location = newLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.LOCATION_TYPE__LOCATION, oldLocation, newLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(LocationType newLocation) {
		if (newLocation != location) {
			NotificationChain msgs = null;
			if (location != null)
				msgs = ((InternalEObject)location).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.LOCATION_TYPE__LOCATION, null, msgs);
			if (newLocation != null)
				msgs = ((InternalEObject)newLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.LOCATION_TYPE__LOCATION, null, msgs);
			msgs = basicSetLocation(newLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.LOCATION_TYPE__LOCATION, newLocation, newLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ID:
				return basicSetEquipmentID(null, msgs);
			case B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				return basicSetEquipmentElementLevel(null, msgs);
			case B2MMLPackage.LOCATION_TYPE__LOCATION:
				return basicSetLocation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ID:
				return getEquipmentID();
			case B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				return getEquipmentElementLevel();
			case B2MMLPackage.LOCATION_TYPE__LOCATION:
				return getLocation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ID:
				setEquipmentID((EquipmentIDType)newValue);
				return;
			case B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				setEquipmentElementLevel((EquipmentElementLevelType)newValue);
				return;
			case B2MMLPackage.LOCATION_TYPE__LOCATION:
				setLocation((LocationType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ID:
				setEquipmentID((EquipmentIDType)null);
				return;
			case B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				setEquipmentElementLevel((EquipmentElementLevelType)null);
				return;
			case B2MMLPackage.LOCATION_TYPE__LOCATION:
				setLocation((LocationType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ID:
				return equipmentID != null;
			case B2MMLPackage.LOCATION_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				return equipmentElementLevel != null;
			case B2MMLPackage.LOCATION_TYPE__LOCATION:
				return location != null;
		}
		return super.eIsSet(featureID);
	}

} //LocationTypeImpl
