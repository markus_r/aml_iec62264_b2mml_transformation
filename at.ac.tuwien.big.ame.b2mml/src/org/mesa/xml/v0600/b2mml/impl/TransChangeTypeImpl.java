/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.TransActionCriteriaType;
import org.mesa.xml.v0600.b2mml.TransChangeType;
import org.mesa.xml.v0600.b2mml.TransResponseCodeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trans Change Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransChangeTypeImpl#getActionCriteria <em>Action Criteria</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransChangeTypeImpl#getResponseCode <em>Response Code</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransChangeTypeImpl extends MinimalEObjectImpl.Container implements TransChangeType {
	/**
	 * The cached value of the '{@link #getActionCriteria() <em>Action Criteria</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionCriteria()
	 * @generated
	 * @ordered
	 */
	protected EList<TransActionCriteriaType> actionCriteria;

	/**
	 * The default value of the '{@link #getResponseCode() <em>Response Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseCode()
	 * @generated
	 * @ordered
	 */
	protected static final TransResponseCodeType RESPONSE_CODE_EDEFAULT = TransResponseCodeType.ALWAYS;

	/**
	 * The cached value of the '{@link #getResponseCode() <em>Response Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseCode()
	 * @generated
	 * @ordered
	 */
	protected TransResponseCodeType responseCode = RESPONSE_CODE_EDEFAULT;

	/**
	 * This is true if the Response Code attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean responseCodeESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransChangeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTransChangeType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TransActionCriteriaType> getActionCriteria() {
		if (actionCriteria == null) {
			actionCriteria = new EObjectContainmentEList<TransActionCriteriaType>(TransActionCriteriaType.class, this, B2MMLPackage.TRANS_CHANGE_TYPE__ACTION_CRITERIA);
		}
		return actionCriteria;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransResponseCodeType getResponseCode() {
		return responseCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponseCode(TransResponseCodeType newResponseCode) {
		TransResponseCodeType oldResponseCode = responseCode;
		responseCode = newResponseCode == null ? RESPONSE_CODE_EDEFAULT : newResponseCode;
		boolean oldResponseCodeESet = responseCodeESet;
		responseCodeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_CHANGE_TYPE__RESPONSE_CODE, oldResponseCode, responseCode, !oldResponseCodeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetResponseCode() {
		TransResponseCodeType oldResponseCode = responseCode;
		boolean oldResponseCodeESet = responseCodeESet;
		responseCode = RESPONSE_CODE_EDEFAULT;
		responseCodeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.TRANS_CHANGE_TYPE__RESPONSE_CODE, oldResponseCode, RESPONSE_CODE_EDEFAULT, oldResponseCodeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetResponseCode() {
		return responseCodeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TRANS_CHANGE_TYPE__ACTION_CRITERIA:
				return ((InternalEList<?>)getActionCriteria()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TRANS_CHANGE_TYPE__ACTION_CRITERIA:
				return getActionCriteria();
			case B2MMLPackage.TRANS_CHANGE_TYPE__RESPONSE_CODE:
				return getResponseCode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TRANS_CHANGE_TYPE__ACTION_CRITERIA:
				getActionCriteria().clear();
				getActionCriteria().addAll((Collection<? extends TransActionCriteriaType>)newValue);
				return;
			case B2MMLPackage.TRANS_CHANGE_TYPE__RESPONSE_CODE:
				setResponseCode((TransResponseCodeType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_CHANGE_TYPE__ACTION_CRITERIA:
				getActionCriteria().clear();
				return;
			case B2MMLPackage.TRANS_CHANGE_TYPE__RESPONSE_CODE:
				unsetResponseCode();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_CHANGE_TYPE__ACTION_CRITERIA:
				return actionCriteria != null && !actionCriteria.isEmpty();
			case B2MMLPackage.TRANS_CHANGE_TYPE__RESPONSE_CODE:
				return isSetResponseCode();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (responseCode: ");
		if (responseCodeESet) result.append(responseCode); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //TransChangeTypeImpl
