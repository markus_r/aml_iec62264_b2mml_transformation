/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType21;
import org.mesa.xml.v0600.b2mml.QualificationTestSpecificationType;
import org.mesa.xml.v0600.b2mml.TransProcessType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type21</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType21Impl#getProcess <em>Process</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType21Impl#getQualificationTestSpecification <em>Qualification Test Specification</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType21Impl extends MinimalEObjectImpl.Container implements DataAreaType21 {
	/**
	 * The cached value of the '{@link #getProcess() <em>Process</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcess()
	 * @generated
	 * @ordered
	 */
	protected TransProcessType process;

	/**
	 * The cached value of the '{@link #getQualificationTestSpecification() <em>Qualification Test Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualificationTestSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<QualificationTestSpecificationType> qualificationTestSpecification;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType21Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType21();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransProcessType getProcess() {
		return process;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcess(TransProcessType newProcess, NotificationChain msgs) {
		TransProcessType oldProcess = process;
		process = newProcess;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE21__PROCESS, oldProcess, newProcess);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcess(TransProcessType newProcess) {
		if (newProcess != process) {
			NotificationChain msgs = null;
			if (process != null)
				msgs = ((InternalEObject)process).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE21__PROCESS, null, msgs);
			if (newProcess != null)
				msgs = ((InternalEObject)newProcess).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE21__PROCESS, null, msgs);
			msgs = basicSetProcess(newProcess, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE21__PROCESS, newProcess, newProcess));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QualificationTestSpecificationType> getQualificationTestSpecification() {
		if (qualificationTestSpecification == null) {
			qualificationTestSpecification = new EObjectContainmentEList<QualificationTestSpecificationType>(QualificationTestSpecificationType.class, this, B2MMLPackage.DATA_AREA_TYPE21__QUALIFICATION_TEST_SPECIFICATION);
		}
		return qualificationTestSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE21__PROCESS:
				return basicSetProcess(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE21__QUALIFICATION_TEST_SPECIFICATION:
				return ((InternalEList<?>)getQualificationTestSpecification()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE21__PROCESS:
				return getProcess();
			case B2MMLPackage.DATA_AREA_TYPE21__QUALIFICATION_TEST_SPECIFICATION:
				return getQualificationTestSpecification();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE21__PROCESS:
				setProcess((TransProcessType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE21__QUALIFICATION_TEST_SPECIFICATION:
				getQualificationTestSpecification().clear();
				getQualificationTestSpecification().addAll((Collection<? extends QualificationTestSpecificationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE21__PROCESS:
				setProcess((TransProcessType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE21__QUALIFICATION_TEST_SPECIFICATION:
				getQualificationTestSpecification().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE21__PROCESS:
				return process != null;
			case B2MMLPackage.DATA_AREA_TYPE21__QUALIFICATION_TEST_SPECIFICATION:
				return qualificationTestSpecification != null && !qualificationTestSpecification.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType21Impl
