/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.TransSignatureType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trans Signature Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransSignatureTypeImpl#getAny <em>Any</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransSignatureTypeImpl#getQualifyingAgencyID <em>Qualifying Agency ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransSignatureTypeImpl extends MinimalEObjectImpl.Container implements TransSignatureType {
	/**
	 * The cached value of the '{@link #getAny() <em>Any</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAny()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap any;

	/**
	 * The default value of the '{@link #getQualifyingAgencyID() <em>Qualifying Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualifyingAgencyID()
	 * @generated
	 * @ordered
	 */
	protected static final String QUALIFYING_AGENCY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQualifyingAgencyID() <em>Qualifying Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualifyingAgencyID()
	 * @generated
	 * @ordered
	 */
	protected String qualifyingAgencyID = QUALIFYING_AGENCY_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransSignatureTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTransSignatureType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAny() {
		if (any == null) {
			any = new BasicFeatureMap(this, B2MMLPackage.TRANS_SIGNATURE_TYPE__ANY);
		}
		return any;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQualifyingAgencyID() {
		return qualifyingAgencyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQualifyingAgencyID(String newQualifyingAgencyID) {
		String oldQualifyingAgencyID = qualifyingAgencyID;
		qualifyingAgencyID = newQualifyingAgencyID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_SIGNATURE_TYPE__QUALIFYING_AGENCY_ID, oldQualifyingAgencyID, qualifyingAgencyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TRANS_SIGNATURE_TYPE__ANY:
				return ((InternalEList<?>)getAny()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TRANS_SIGNATURE_TYPE__ANY:
				if (coreType) return getAny();
				return ((FeatureMap.Internal)getAny()).getWrapper();
			case B2MMLPackage.TRANS_SIGNATURE_TYPE__QUALIFYING_AGENCY_ID:
				return getQualifyingAgencyID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TRANS_SIGNATURE_TYPE__ANY:
				((FeatureMap.Internal)getAny()).set(newValue);
				return;
			case B2MMLPackage.TRANS_SIGNATURE_TYPE__QUALIFYING_AGENCY_ID:
				setQualifyingAgencyID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_SIGNATURE_TYPE__ANY:
				getAny().clear();
				return;
			case B2MMLPackage.TRANS_SIGNATURE_TYPE__QUALIFYING_AGENCY_ID:
				setQualifyingAgencyID(QUALIFYING_AGENCY_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_SIGNATURE_TYPE__ANY:
				return any != null && !any.isEmpty();
			case B2MMLPackage.TRANS_SIGNATURE_TYPE__QUALIFYING_AGENCY_ID:
				return QUALIFYING_AGENCY_ID_EDEFAULT == null ? qualifyingAgencyID != null : !QUALIFYING_AGENCY_ID_EDEFAULT.equals(qualifyingAgencyID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (any: ");
		result.append(any);
		result.append(", qualifyingAgencyID: ");
		result.append(qualifyingAgencyID);
		result.append(')');
		return result.toString();
	}

} //TransSignatureTypeImpl
