/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.QuantityStringType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Quantity String Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class QuantityStringTypeImpl extends AnyGenericValueTypeImpl implements QuantityStringType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QuantityStringTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getQuantityStringType();
	}

} //QuantityStringTypeImpl
