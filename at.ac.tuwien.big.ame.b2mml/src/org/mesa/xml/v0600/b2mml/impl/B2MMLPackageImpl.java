/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.io.IOException;

import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.mesa.xml.v0600.b2mml.B2MMLFactory;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;

import org.mesa.xml.v0600.b2mml.util.B2MMLValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class B2MMLPackageImpl extends EPackageImpl implements B2MMLPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected String packageFilename = "b2mml.ecore";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeEquipmentCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeEquipmentClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeEquipmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeEquipmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeMaterialClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeMaterialDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeMaterialInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeMaterialLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeMaterialSubLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeMaterialTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeOperationsCapabilityInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeOperationsCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeOperationsDefinitionInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeOperationsDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeOperationsPerformanceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeOperationsScheduleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgePersonnelClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgePersonnelInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgePersonTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgePhysicalAssetCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgePhysicalAssetClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgePhysicalAssetInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgePhysicalAssetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeProcessSegmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeProcessSegmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acknowledgeQualificationTestSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actualEndTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actualFinishTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actualStartTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass amountTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass anyGenericValueTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assemblyRelationship1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assemblyRelationshipTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assemblyType1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assemblyTypeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass billOfMaterialIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass billOfMaterialsIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass billOfResourcesIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryObjectTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bodTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelEquipmentCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelEquipmentClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelEquipmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelEquipmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelMaterialClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelMaterialDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelMaterialInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelMaterialLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelMaterialSubLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelMaterialTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelOperationsCapabilityInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelOperationsCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelOperationsDefinitionInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelOperationsDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelOperationsPerformanceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelOperationsScheduleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelPersonnelClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelPersonnelInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelPersonTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelPhysicalAssetCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelPhysicalAssetClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelPhysicalAssetInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelPhysicalAssetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelProcessSegmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelProcessSegmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cancelQualificationTestSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass capabilityType1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass capabilityTypeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass causeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass certificateOfAnalysisReferenceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeEquipmentCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeEquipmentClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeEquipmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeEquipmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeMaterialClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeMaterialDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeMaterialInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeMaterialLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeMaterialSubLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeMaterialTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeOperationsCapabilityInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeOperationsCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeOperationsDefinitionInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeOperationsDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeOperationsPerformanceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeOperationsScheduleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changePersonnelClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changePersonnelInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changePersonTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changePhysicalAssetCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changePhysicalAssetClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changePhysicalAssetInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changePhysicalAssetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeProcessSegmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeProcessSegmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeQualificationTestSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass confidenceFactorTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass confirmBODTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass correctionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType4EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType5EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType6EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType7EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType8EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType9EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType10EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType11EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType12EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType13EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType14EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType15EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType16EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType17EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType18EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType19EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType20EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType21EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType22EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType23EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType24EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType25EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType26EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType27EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType28EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType29EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType30EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType31EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType32EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType33EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType34EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType35EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType36EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType37EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType38EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType39EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType40EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType41EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType42EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType43EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType44EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType45EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType46EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType47EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType48EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType49EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType50EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType51EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType52EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType53EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType54EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType55EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType56EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType57EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType58EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType59EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType60EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType61EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType62EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType63EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType64EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType65EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType66EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType67EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType68EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType69EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType70EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType71EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType72EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType73EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType74EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType75EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType76EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType77EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType78EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType79EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType80EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType81EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType82EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType83EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType84EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType85EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType86EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType87EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType88EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType89EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType90EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType91EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType92EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType93EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType94EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType95EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType96EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType97EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType98EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType99EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType100EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType101EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType102EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType103EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType104EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType105EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType106EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType107EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType108EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType109EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType110EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType111EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType112EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType113EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType114EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType115EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType116EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType117EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType118EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType119EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType120EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType121EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType122EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType123EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType124EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType125EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType126EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType127EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType128EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType129EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType130EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType131EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType132EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType133EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType134EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType135EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType136EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType137EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType138EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType139EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType140EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType141EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType142EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType143EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType144EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType145EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType146EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType147EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType148EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType149EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType150EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType151EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType152EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType153EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType154EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType155EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType156EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType157EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType158EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType159EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType160EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType161EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType162EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType163EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType164EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType165EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType166EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType167EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType168EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType169EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType170EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType171EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType172EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType173EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType174EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType175EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType176EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType177EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType178EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType179EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType180EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType181EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType182EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType183EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType184EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType185EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType186EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType187EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType188EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType189EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType190EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType191EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType192EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType193EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType194EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType195EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType196EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType197EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType198EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType199EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType200EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType201EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType202EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType203EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType204EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType205EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType206EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType207EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataAreaType208EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataType1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataTypeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dateTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dependency1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dependencyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass descriptionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earliestStartTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass endTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentAssetMappingTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentCapabilityTestSpecificationIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentCapabilityTestSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentClassIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentClassPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentElementLevel1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentElementLevelTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentSegmentSpecificationPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentSegmentSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equipmentUseTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expirationTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getEquipmentCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getEquipmentClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getEquipmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getEquipmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getMaterialClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getMaterialDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getMaterialInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getMaterialLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getMaterialSubLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getMaterialTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getOperationsCapabilityInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getOperationsCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getOperationsDefinitionInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getOperationsDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getOperationsPerformanceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getOperationsScheduleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getPersonnelClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getPersonnelInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getPersonTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getPhysicalAssetCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getPhysicalAssetClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getPhysicalAssetInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getPhysicalAssetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getProcessSegmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getProcessSegmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getQualificationTestSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hierarchyScopeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identifierTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass jobOrderCommand1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass jobOrderCommandRuleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass jobOrderCommandTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass jobOrderDispatchStatusTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass latestEndTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass locationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass manufacturingBillIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialActualIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialCapabilityIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialClassIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialClassPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialDefinitionIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialDefinitionPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialLotIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialLotPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialRequirementIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialSegmentSpecificationPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialSegmentSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialSpecificationIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialSubLotIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialSubLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialTestSpecificationIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialTestSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialUse1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass materialUseTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass measureTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nameTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass numericTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opEquipmentActualPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opEquipmentActualTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opEquipmentCapabilityPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opEquipmentCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opEquipmentRequirementPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opEquipmentRequirementTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opEquipmentSpecificationPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opEquipmentSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsCapabilityInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsDefinitionIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsDefinitionInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsMaterialBillItemTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsMaterialBillTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsPerformanceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsRequestIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsRequestTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsResponseTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsScheduleIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsScheduleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsSegmentIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsSegmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsType1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationsTypeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opMaterialActualPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opMaterialActualTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opMaterialCapabilityPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opMaterialCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opMaterialRequirementPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opMaterialRequirementTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opMaterialSpecificationPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opMaterialSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPersonnelActualPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPersonnelActualTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPersonnelCapabilityPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPersonnelCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPersonnelRequirementPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPersonnelRequirementTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPersonnelSpecificationPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPersonnelSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPhysicalAssetActualPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPhysicalAssetActualTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPhysicalAssetCapabilityPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPhysicalAssetCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPhysicalAssetRequirementPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPhysicalAssetRequirementTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPhysicalAssetSpecificationPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opPhysicalAssetSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opProcessSegmentCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opSegmentDataTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opSegmentRequirementTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass opSegmentResponseTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass otherDependencyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personNameTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelClassIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelClassPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelSegmentSpecificationPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelSegmentSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personnelUseTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetActualIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetCapabilityTestSpecificationIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetCapabilityTestSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetClassIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetClassPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetSegmentSpecificationPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetSegmentSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalAssetUseTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass plannedFinishTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass plannedStartTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass priorityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass problemTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processEquipmentCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processEquipmentClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processEquipmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processEquipmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processMaterialClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processMaterialDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processMaterialInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processMaterialLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processMaterialSubLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processMaterialTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processOperationsCapabilityInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processOperationsCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processOperationsDefinitionInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processOperationsDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processOperationsPerformanceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processOperationsScheduleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processPersonnelClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processPersonnelInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processPersonTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processPhysicalAssetCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processPhysicalAssetClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processPhysicalAssetInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processPhysicalAssetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processProcessSegmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processProcessSegmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processQualificationTestSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processSegmentIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processSegmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processSegmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass productionRequestIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass productionScheduleIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass productProductionRuleIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass productProductionRuleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass productSegmentIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass publishedDateTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass qualificationTestSpecificationIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass qualificationTestSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass quantityStringTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass quantityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass quantityValueTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reasonTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relationshipForm1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relationshipFormTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relationshipType1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relationshipTypeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requestedCompletionDateTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requestedPriorityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requestState1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requestStateTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requiredByRequestedSegmentResponse1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requiredByRequestedSegmentResponseTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceNetworkConnectionIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceReferenceType1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceReferenceTypeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourcesTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondEquipmentCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondEquipmentClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondEquipmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondEquipmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondMaterialClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondMaterialDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondMaterialInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondMaterialLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondMaterialSubLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondMaterialTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondOperationsCapabilityInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondOperationsCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondOperationsDefinitionInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondOperationsDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondOperationsPerformanceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondOperationsScheduleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondPersonnelClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondPersonnelInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondPersonTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondPhysicalAssetCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondPhysicalAssetClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondPhysicalAssetInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondPhysicalAssetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondProcessSegmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondProcessSegmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass respondQualificationTestSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass responseState1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass responseStateTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resultTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass segmentDependencyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showEquipmentCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showEquipmentClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showEquipmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showEquipmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showMaterialClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showMaterialDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showMaterialInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showMaterialLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showMaterialSubLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showMaterialTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showOperationsCapabilityInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showOperationsCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showOperationsDefinitionInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showOperationsDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showOperationsPerformanceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showOperationsScheduleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showPersonnelClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showPersonnelInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showPersonTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showPhysicalAssetCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showPhysicalAssetClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showPhysicalAssetInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showPhysicalAssetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showProcessSegmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showProcessSegmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showQualificationTestSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass startTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass statusTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass statusTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass storageHierarchyScopeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass storageLocationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncEquipmentCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncEquipmentClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncEquipmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncEquipmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncMaterialClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncMaterialDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncMaterialInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncMaterialLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncMaterialSubLotTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncMaterialTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncOperationsCapabilityInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncOperationsCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncOperationsDefinitionInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncOperationsDefinitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncOperationsPerformanceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncOperationsScheduleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncPersonnelClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncPersonnelInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncPersonTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncPhysicalAssetCapabilityTestSpecTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncPhysicalAssetClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncPhysicalAssetInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncPhysicalAssetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncProcessSegmentInformationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncProcessSegmentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncQualificationTestSpecificationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testDateTimeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testedEquipmentClassPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testedEquipmentPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testedMaterialClassPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testedMaterialDefinitionPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testedMaterialLotPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testedPersonnelClassPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testedPersonPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testedPhysicalAssetClassPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testedPhysicalAssetPropertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testResultTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass textTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transAcknowledgeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transActionCriteriaTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transApplicationAreaTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transCancelTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transChangeStatusTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transChangeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transConfirmationCodeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transConfirmTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transExpression1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transExpressionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transGetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transProcessTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transReceiverTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transRespondTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transResponseCriteriaTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transSenderTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transShowTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transSignatureTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transStateChangeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transSyncTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transUserAreaTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unitOfMeasureTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueStringTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass versionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass workRequestIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass workScheduleIDTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass workType1TypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass workTypeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum assemblyRelationship1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum assemblyType1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum capabilityType1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dataType1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dependency1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum equipmentElementLevel1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum jobOrderCommand1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum materialUse1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum operationsType1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum relationshipForm1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum relationshipType1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum requestState1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum requiredByRequestedSegmentResponse1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum resourceReferenceType1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum responseState1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum transActionCodeEnumerationTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum transResponseCodeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum workType1TypeBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType assemblyRelationship1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType assemblyType1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType capabilityType1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType dataType1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType dependency1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType durationTypeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType equipmentElementLevel1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType indicatorTypeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType indicatorTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType jobOrderCommand1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType materialUse1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType operationsType1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType relationshipForm1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType relationshipType1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType requestState1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType requiredByRequestedSegmentResponse1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType resourceReferenceType1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType responseState1TypeBaseObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType transActionCodeEnumerationTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType transActionCodeTypeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType transResponseCodeTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType workType1TypeBaseObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private B2MMLPackageImpl() {
		super(eNS_URI, B2MMLFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link B2MMLPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @generated
	 */
	public static B2MMLPackage init() {
		if (isInited) return (B2MMLPackage)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI);

		// Obtain or create and register package
		B2MMLPackageImpl theB2MMLPackage = (B2MMLPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof B2MMLPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new B2MMLPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Load packages
		theB2MMLPackage.loadPackage();

		// Fix loaded packages
		theB2MMLPackage.fixPackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theB2MMLPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return B2MMLValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theB2MMLPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(B2MMLPackage.eNS_URI, theB2MMLPackage);
		return theB2MMLPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeEquipmentCapabilityTestSpecType() {
		if (acknowledgeEquipmentCapabilityTestSpecTypeEClass == null) {
			acknowledgeEquipmentCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(0);
		}
		return acknowledgeEquipmentCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeEquipmentCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getAcknowledgeEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeEquipmentCapabilityTestSpecType_DataArea() {
        return (EReference)getAcknowledgeEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeEquipmentCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getAcknowledgeEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeEquipmentCapabilityTestSpecType_VersionID() {
        return (EAttribute)getAcknowledgeEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeEquipmentClassType() {
		if (acknowledgeEquipmentClassTypeEClass == null) {
			acknowledgeEquipmentClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(1);
		}
		return acknowledgeEquipmentClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeEquipmentClassType_ApplicationArea() {
        return (EReference)getAcknowledgeEquipmentClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeEquipmentClassType_DataArea() {
        return (EReference)getAcknowledgeEquipmentClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeEquipmentClassType_ReleaseID() {
        return (EAttribute)getAcknowledgeEquipmentClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeEquipmentClassType_VersionID() {
        return (EAttribute)getAcknowledgeEquipmentClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeEquipmentInformationType() {
		if (acknowledgeEquipmentInformationTypeEClass == null) {
			acknowledgeEquipmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(2);
		}
		return acknowledgeEquipmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeEquipmentInformationType_ApplicationArea() {
        return (EReference)getAcknowledgeEquipmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeEquipmentInformationType_DataArea() {
        return (EReference)getAcknowledgeEquipmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeEquipmentInformationType_ReleaseID() {
        return (EAttribute)getAcknowledgeEquipmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeEquipmentInformationType_VersionID() {
        return (EAttribute)getAcknowledgeEquipmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeEquipmentType() {
		if (acknowledgeEquipmentTypeEClass == null) {
			acknowledgeEquipmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(3);
		}
		return acknowledgeEquipmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeEquipmentType_ApplicationArea() {
        return (EReference)getAcknowledgeEquipmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeEquipmentType_DataArea() {
        return (EReference)getAcknowledgeEquipmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeEquipmentType_ReleaseID() {
        return (EAttribute)getAcknowledgeEquipmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeEquipmentType_VersionID() {
        return (EAttribute)getAcknowledgeEquipmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeMaterialClassType() {
		if (acknowledgeMaterialClassTypeEClass == null) {
			acknowledgeMaterialClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(4);
		}
		return acknowledgeMaterialClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeMaterialClassType_ApplicationArea() {
        return (EReference)getAcknowledgeMaterialClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeMaterialClassType_DataArea() {
        return (EReference)getAcknowledgeMaterialClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeMaterialClassType_ReleaseID() {
        return (EAttribute)getAcknowledgeMaterialClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeMaterialClassType_VersionID() {
        return (EAttribute)getAcknowledgeMaterialClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeMaterialDefinitionType() {
		if (acknowledgeMaterialDefinitionTypeEClass == null) {
			acknowledgeMaterialDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(5);
		}
		return acknowledgeMaterialDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeMaterialDefinitionType_ApplicationArea() {
        return (EReference)getAcknowledgeMaterialDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeMaterialDefinitionType_DataArea() {
        return (EReference)getAcknowledgeMaterialDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeMaterialDefinitionType_ReleaseID() {
        return (EAttribute)getAcknowledgeMaterialDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeMaterialDefinitionType_VersionID() {
        return (EAttribute)getAcknowledgeMaterialDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeMaterialInformationType() {
		if (acknowledgeMaterialInformationTypeEClass == null) {
			acknowledgeMaterialInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(6);
		}
		return acknowledgeMaterialInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeMaterialInformationType_ApplicationArea() {
        return (EReference)getAcknowledgeMaterialInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeMaterialInformationType_DataArea() {
        return (EReference)getAcknowledgeMaterialInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeMaterialInformationType_ReleaseID() {
        return (EAttribute)getAcknowledgeMaterialInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeMaterialInformationType_VersionID() {
        return (EAttribute)getAcknowledgeMaterialInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeMaterialLotType() {
		if (acknowledgeMaterialLotTypeEClass == null) {
			acknowledgeMaterialLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(7);
		}
		return acknowledgeMaterialLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeMaterialLotType_ApplicationArea() {
        return (EReference)getAcknowledgeMaterialLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeMaterialLotType_DataArea() {
        return (EReference)getAcknowledgeMaterialLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeMaterialLotType_ReleaseID() {
        return (EAttribute)getAcknowledgeMaterialLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeMaterialLotType_VersionID() {
        return (EAttribute)getAcknowledgeMaterialLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeMaterialSubLotType() {
		if (acknowledgeMaterialSubLotTypeEClass == null) {
			acknowledgeMaterialSubLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(8);
		}
		return acknowledgeMaterialSubLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeMaterialSubLotType_ApplicationArea() {
        return (EReference)getAcknowledgeMaterialSubLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeMaterialSubLotType_DataArea() {
        return (EReference)getAcknowledgeMaterialSubLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeMaterialSubLotType_ReleaseID() {
        return (EAttribute)getAcknowledgeMaterialSubLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeMaterialSubLotType_VersionID() {
        return (EAttribute)getAcknowledgeMaterialSubLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeMaterialTestSpecType() {
		if (acknowledgeMaterialTestSpecTypeEClass == null) {
			acknowledgeMaterialTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(9);
		}
		return acknowledgeMaterialTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeMaterialTestSpecType_ApplicationArea() {
        return (EReference)getAcknowledgeMaterialTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeMaterialTestSpecType_DataArea() {
        return (EReference)getAcknowledgeMaterialTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeMaterialTestSpecType_ReleaseID() {
        return (EAttribute)getAcknowledgeMaterialTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeMaterialTestSpecType_VersionID() {
        return (EAttribute)getAcknowledgeMaterialTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeOperationsCapabilityInformationType() {
		if (acknowledgeOperationsCapabilityInformationTypeEClass == null) {
			acknowledgeOperationsCapabilityInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(10);
		}
		return acknowledgeOperationsCapabilityInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeOperationsCapabilityInformationType_ApplicationArea() {
        return (EReference)getAcknowledgeOperationsCapabilityInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeOperationsCapabilityInformationType_DataArea() {
        return (EReference)getAcknowledgeOperationsCapabilityInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeOperationsCapabilityInformationType_ReleaseID() {
        return (EAttribute)getAcknowledgeOperationsCapabilityInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeOperationsCapabilityInformationType_VersionID() {
        return (EAttribute)getAcknowledgeOperationsCapabilityInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeOperationsCapabilityType() {
		if (acknowledgeOperationsCapabilityTypeEClass == null) {
			acknowledgeOperationsCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(11);
		}
		return acknowledgeOperationsCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeOperationsCapabilityType_ApplicationArea() {
        return (EReference)getAcknowledgeOperationsCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeOperationsCapabilityType_DataArea() {
        return (EReference)getAcknowledgeOperationsCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeOperationsCapabilityType_ReleaseID() {
        return (EAttribute)getAcknowledgeOperationsCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeOperationsCapabilityType_VersionID() {
        return (EAttribute)getAcknowledgeOperationsCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeOperationsDefinitionInformationType() {
		if (acknowledgeOperationsDefinitionInformationTypeEClass == null) {
			acknowledgeOperationsDefinitionInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(12);
		}
		return acknowledgeOperationsDefinitionInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeOperationsDefinitionInformationType_ApplicationArea() {
        return (EReference)getAcknowledgeOperationsDefinitionInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeOperationsDefinitionInformationType_DataArea() {
        return (EReference)getAcknowledgeOperationsDefinitionInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeOperationsDefinitionInformationType_ReleaseID() {
        return (EAttribute)getAcknowledgeOperationsDefinitionInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeOperationsDefinitionInformationType_VersionID() {
        return (EAttribute)getAcknowledgeOperationsDefinitionInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeOperationsDefinitionType() {
		if (acknowledgeOperationsDefinitionTypeEClass == null) {
			acknowledgeOperationsDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(13);
		}
		return acknowledgeOperationsDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeOperationsDefinitionType_ApplicationArea() {
        return (EReference)getAcknowledgeOperationsDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeOperationsDefinitionType_DataArea() {
        return (EReference)getAcknowledgeOperationsDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeOperationsDefinitionType_ReleaseID() {
        return (EAttribute)getAcknowledgeOperationsDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeOperationsDefinitionType_VersionID() {
        return (EAttribute)getAcknowledgeOperationsDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeOperationsPerformanceType() {
		if (acknowledgeOperationsPerformanceTypeEClass == null) {
			acknowledgeOperationsPerformanceTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(14);
		}
		return acknowledgeOperationsPerformanceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeOperationsPerformanceType_ApplicationArea() {
        return (EReference)getAcknowledgeOperationsPerformanceType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeOperationsPerformanceType_DataArea() {
        return (EReference)getAcknowledgeOperationsPerformanceType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeOperationsPerformanceType_ReleaseID() {
        return (EAttribute)getAcknowledgeOperationsPerformanceType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeOperationsPerformanceType_VersionID() {
        return (EAttribute)getAcknowledgeOperationsPerformanceType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeOperationsScheduleType() {
		if (acknowledgeOperationsScheduleTypeEClass == null) {
			acknowledgeOperationsScheduleTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(15);
		}
		return acknowledgeOperationsScheduleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeOperationsScheduleType_ApplicationArea() {
        return (EReference)getAcknowledgeOperationsScheduleType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeOperationsScheduleType_DataArea() {
        return (EReference)getAcknowledgeOperationsScheduleType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeOperationsScheduleType_ReleaseID() {
        return (EAttribute)getAcknowledgeOperationsScheduleType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeOperationsScheduleType_VersionID() {
        return (EAttribute)getAcknowledgeOperationsScheduleType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgePersonnelClassType() {
		if (acknowledgePersonnelClassTypeEClass == null) {
			acknowledgePersonnelClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(16);
		}
		return acknowledgePersonnelClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePersonnelClassType_ApplicationArea() {
        return (EReference)getAcknowledgePersonnelClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePersonnelClassType_DataArea() {
        return (EReference)getAcknowledgePersonnelClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePersonnelClassType_ReleaseID() {
        return (EAttribute)getAcknowledgePersonnelClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePersonnelClassType_VersionID() {
        return (EAttribute)getAcknowledgePersonnelClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgePersonnelInformationType() {
		if (acknowledgePersonnelInformationTypeEClass == null) {
			acknowledgePersonnelInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(17);
		}
		return acknowledgePersonnelInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePersonnelInformationType_ApplicationArea() {
        return (EReference)getAcknowledgePersonnelInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePersonnelInformationType_DataArea() {
        return (EReference)getAcknowledgePersonnelInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePersonnelInformationType_ReleaseID() {
        return (EAttribute)getAcknowledgePersonnelInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePersonnelInformationType_VersionID() {
        return (EAttribute)getAcknowledgePersonnelInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgePersonType() {
		if (acknowledgePersonTypeEClass == null) {
			acknowledgePersonTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(18);
		}
		return acknowledgePersonTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePersonType_ApplicationArea() {
        return (EReference)getAcknowledgePersonType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePersonType_DataArea() {
        return (EReference)getAcknowledgePersonType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePersonType_ReleaseID() {
        return (EAttribute)getAcknowledgePersonType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePersonType_VersionID() {
        return (EAttribute)getAcknowledgePersonType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgePhysicalAssetCapabilityTestSpecType() {
		if (acknowledgePhysicalAssetCapabilityTestSpecTypeEClass == null) {
			acknowledgePhysicalAssetCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(19);
		}
		return acknowledgePhysicalAssetCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePhysicalAssetCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getAcknowledgePhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePhysicalAssetCapabilityTestSpecType_DataArea() {
        return (EReference)getAcknowledgePhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePhysicalAssetCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getAcknowledgePhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePhysicalAssetCapabilityTestSpecType_VersionID() {
        return (EAttribute)getAcknowledgePhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgePhysicalAssetClassType() {
		if (acknowledgePhysicalAssetClassTypeEClass == null) {
			acknowledgePhysicalAssetClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(20);
		}
		return acknowledgePhysicalAssetClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePhysicalAssetClassType_ApplicationArea() {
        return (EReference)getAcknowledgePhysicalAssetClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePhysicalAssetClassType_DataArea() {
        return (EReference)getAcknowledgePhysicalAssetClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePhysicalAssetClassType_ReleaseID() {
        return (EAttribute)getAcknowledgePhysicalAssetClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePhysicalAssetClassType_VersionID() {
        return (EAttribute)getAcknowledgePhysicalAssetClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgePhysicalAssetInformationType() {
		if (acknowledgePhysicalAssetInformationTypeEClass == null) {
			acknowledgePhysicalAssetInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(21);
		}
		return acknowledgePhysicalAssetInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePhysicalAssetInformationType_ApplicationArea() {
        return (EReference)getAcknowledgePhysicalAssetInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePhysicalAssetInformationType_DataArea() {
        return (EReference)getAcknowledgePhysicalAssetInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePhysicalAssetInformationType_ReleaseID() {
        return (EAttribute)getAcknowledgePhysicalAssetInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePhysicalAssetInformationType_VersionID() {
        return (EAttribute)getAcknowledgePhysicalAssetInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgePhysicalAssetType() {
		if (acknowledgePhysicalAssetTypeEClass == null) {
			acknowledgePhysicalAssetTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(22);
		}
		return acknowledgePhysicalAssetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePhysicalAssetType_ApplicationArea() {
        return (EReference)getAcknowledgePhysicalAssetType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgePhysicalAssetType_DataArea() {
        return (EReference)getAcknowledgePhysicalAssetType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePhysicalAssetType_ReleaseID() {
        return (EAttribute)getAcknowledgePhysicalAssetType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgePhysicalAssetType_VersionID() {
        return (EAttribute)getAcknowledgePhysicalAssetType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeProcessSegmentInformationType() {
		if (acknowledgeProcessSegmentInformationTypeEClass == null) {
			acknowledgeProcessSegmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(23);
		}
		return acknowledgeProcessSegmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeProcessSegmentInformationType_ApplicationArea() {
        return (EReference)getAcknowledgeProcessSegmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeProcessSegmentInformationType_DataArea() {
        return (EReference)getAcknowledgeProcessSegmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeProcessSegmentInformationType_ReleaseID() {
        return (EAttribute)getAcknowledgeProcessSegmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeProcessSegmentInformationType_VersionID() {
        return (EAttribute)getAcknowledgeProcessSegmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeProcessSegmentType() {
		if (acknowledgeProcessSegmentTypeEClass == null) {
			acknowledgeProcessSegmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(24);
		}
		return acknowledgeProcessSegmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeProcessSegmentType_ApplicationArea() {
        return (EReference)getAcknowledgeProcessSegmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeProcessSegmentType_DataArea() {
        return (EReference)getAcknowledgeProcessSegmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeProcessSegmentType_ReleaseID() {
        return (EAttribute)getAcknowledgeProcessSegmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeProcessSegmentType_VersionID() {
        return (EAttribute)getAcknowledgeProcessSegmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAcknowledgeQualificationTestSpecificationType() {
		if (acknowledgeQualificationTestSpecificationTypeEClass == null) {
			acknowledgeQualificationTestSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(25);
		}
		return acknowledgeQualificationTestSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeQualificationTestSpecificationType_ApplicationArea() {
        return (EReference)getAcknowledgeQualificationTestSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAcknowledgeQualificationTestSpecificationType_DataArea() {
        return (EReference)getAcknowledgeQualificationTestSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeQualificationTestSpecificationType_ReleaseID() {
        return (EAttribute)getAcknowledgeQualificationTestSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAcknowledgeQualificationTestSpecificationType_VersionID() {
        return (EAttribute)getAcknowledgeQualificationTestSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActualEndTimeType() {
		if (actualEndTimeTypeEClass == null) {
			actualEndTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(26);
		}
		return actualEndTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActualFinishTimeType() {
		if (actualFinishTimeTypeEClass == null) {
			actualFinishTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(27);
		}
		return actualFinishTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActualStartTimeType() {
		if (actualStartTimeTypeEClass == null) {
			actualStartTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(28);
		}
		return actualStartTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAmountType() {
		if (amountTypeEClass == null) {
			amountTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(29);
		}
		return amountTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAmountType_Value() {
        return (EAttribute)getAmountType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAmountType_CurrencyCodeListVersionID() {
        return (EAttribute)getAmountType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAmountType_CurrencyID() {
        return (EAttribute)getAmountType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnyGenericValueType() {
		if (anyGenericValueTypeEClass == null) {
			anyGenericValueTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(30);
		}
		return anyGenericValueTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_Value() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_CharacterSetCode() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_CurrencyCodeListVersionID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_CurrencyID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_EncodingCode() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_Filename() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_Format() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_LanguageID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_LanguageLocaleID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_ListAgencyID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_ListAgencyName() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_ListID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_ListName() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_ListSchemaURI() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_ListURI() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_ListVersionID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_MimeCode() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_Name() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_SchemaAgencyID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_SchemaAgencyName() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_SchemaDataURI() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_SchemaID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_SchemaName() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_SchemaURI() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_SchemaVersionID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_UnitCode() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_UnitCodeListAgencyID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_UnitCodeListAgencyName() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_UnitCodeListID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_UnitCodeListVersionID() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(29);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnyGenericValueType_Uri() {
        return (EAttribute)getAnyGenericValueType().getEStructuralFeatures().get(30);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssemblyRelationship1Type() {
		if (assemblyRelationship1TypeEClass == null) {
			assemblyRelationship1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(31);
		}
		return assemblyRelationship1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssemblyRelationshipType() {
		if (assemblyRelationshipTypeEClass == null) {
			assemblyRelationshipTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(34);
		}
		return assemblyRelationshipTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssemblyRelationshipType_OtherValue() {
        return (EAttribute)getAssemblyRelationshipType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssemblyType1Type() {
		if (assemblyType1TypeEClass == null) {
			assemblyType1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(35);
		}
		return assemblyType1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssemblyTypeType() {
		if (assemblyTypeTypeEClass == null) {
			assemblyTypeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(38);
		}
		return assemblyTypeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssemblyTypeType_OtherValue() {
        return (EAttribute)getAssemblyTypeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBillOfMaterialIDType() {
		if (billOfMaterialIDTypeEClass == null) {
			billOfMaterialIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(39);
		}
		return billOfMaterialIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBillOfMaterialsIDType() {
		if (billOfMaterialsIDTypeEClass == null) {
			billOfMaterialsIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(40);
		}
		return billOfMaterialsIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBillOfResourcesIDType() {
		if (billOfResourcesIDTypeEClass == null) {
			billOfResourcesIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(41);
		}
		return billOfResourcesIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBinaryObjectType() {
		if (binaryObjectTypeEClass == null) {
			binaryObjectTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(42);
		}
		return binaryObjectTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryObjectType_Value() {
        return (EAttribute)getBinaryObjectType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryObjectType_CharacterSetCode() {
        return (EAttribute)getBinaryObjectType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryObjectType_EncodingCode() {
        return (EAttribute)getBinaryObjectType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryObjectType_Filename() {
        return (EAttribute)getBinaryObjectType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryObjectType_Format() {
        return (EAttribute)getBinaryObjectType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryObjectType_MimeCode() {
        return (EAttribute)getBinaryObjectType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryObjectType_Uri() {
        return (EAttribute)getBinaryObjectType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBODType() {
		if (bodTypeEClass == null) {
			bodTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(43);
		}
		return bodTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBODType_OriginalApplicationArea() {
        return (EReference)getBODType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBODType_Description() {
        return (EReference)getBODType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBODType_Note() {
        return (EReference)getBODType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBODType_UserArea() {
        return (EReference)getBODType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelEquipmentCapabilityTestSpecType() {
		if (cancelEquipmentCapabilityTestSpecTypeEClass == null) {
			cancelEquipmentCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(44);
		}
		return cancelEquipmentCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelEquipmentCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getCancelEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelEquipmentCapabilityTestSpecType_DataArea() {
        return (EReference)getCancelEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelEquipmentCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getCancelEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelEquipmentCapabilityTestSpecType_VersionID() {
        return (EAttribute)getCancelEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelEquipmentClassType() {
		if (cancelEquipmentClassTypeEClass == null) {
			cancelEquipmentClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(45);
		}
		return cancelEquipmentClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelEquipmentClassType_ApplicationArea() {
        return (EReference)getCancelEquipmentClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelEquipmentClassType_DataArea() {
        return (EReference)getCancelEquipmentClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelEquipmentClassType_ReleaseID() {
        return (EAttribute)getCancelEquipmentClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelEquipmentClassType_VersionID() {
        return (EAttribute)getCancelEquipmentClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelEquipmentInformationType() {
		if (cancelEquipmentInformationTypeEClass == null) {
			cancelEquipmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(46);
		}
		return cancelEquipmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelEquipmentInformationType_ApplicationArea() {
        return (EReference)getCancelEquipmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelEquipmentInformationType_DataArea() {
        return (EReference)getCancelEquipmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelEquipmentInformationType_ReleaseID() {
        return (EAttribute)getCancelEquipmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelEquipmentInformationType_VersionID() {
        return (EAttribute)getCancelEquipmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelEquipmentType() {
		if (cancelEquipmentTypeEClass == null) {
			cancelEquipmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(47);
		}
		return cancelEquipmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelEquipmentType_ApplicationArea() {
        return (EReference)getCancelEquipmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelEquipmentType_DataArea() {
        return (EReference)getCancelEquipmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelEquipmentType_ReleaseID() {
        return (EAttribute)getCancelEquipmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelEquipmentType_VersionID() {
        return (EAttribute)getCancelEquipmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelMaterialClassType() {
		if (cancelMaterialClassTypeEClass == null) {
			cancelMaterialClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(48);
		}
		return cancelMaterialClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelMaterialClassType_ApplicationArea() {
        return (EReference)getCancelMaterialClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelMaterialClassType_DataArea() {
        return (EReference)getCancelMaterialClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelMaterialClassType_ReleaseID() {
        return (EAttribute)getCancelMaterialClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelMaterialClassType_VersionID() {
        return (EAttribute)getCancelMaterialClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelMaterialDefinitionType() {
		if (cancelMaterialDefinitionTypeEClass == null) {
			cancelMaterialDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(49);
		}
		return cancelMaterialDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelMaterialDefinitionType_ApplicationArea() {
        return (EReference)getCancelMaterialDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelMaterialDefinitionType_DataArea() {
        return (EReference)getCancelMaterialDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelMaterialDefinitionType_ReleaseID() {
        return (EAttribute)getCancelMaterialDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelMaterialDefinitionType_VersionID() {
        return (EAttribute)getCancelMaterialDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelMaterialInformationType() {
		if (cancelMaterialInformationTypeEClass == null) {
			cancelMaterialInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(50);
		}
		return cancelMaterialInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelMaterialInformationType_ApplicationArea() {
        return (EReference)getCancelMaterialInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelMaterialInformationType_DataArea() {
        return (EReference)getCancelMaterialInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelMaterialInformationType_ReleaseID() {
        return (EAttribute)getCancelMaterialInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelMaterialInformationType_VersionID() {
        return (EAttribute)getCancelMaterialInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelMaterialLotType() {
		if (cancelMaterialLotTypeEClass == null) {
			cancelMaterialLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(51);
		}
		return cancelMaterialLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelMaterialLotType_ApplicationArea() {
        return (EReference)getCancelMaterialLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelMaterialLotType_DataArea() {
        return (EReference)getCancelMaterialLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelMaterialLotType_ReleaseID() {
        return (EAttribute)getCancelMaterialLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelMaterialLotType_VersionID() {
        return (EAttribute)getCancelMaterialLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelMaterialSubLotType() {
		if (cancelMaterialSubLotTypeEClass == null) {
			cancelMaterialSubLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(52);
		}
		return cancelMaterialSubLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelMaterialSubLotType_ApplicationArea() {
        return (EReference)getCancelMaterialSubLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelMaterialSubLotType_DataArea() {
        return (EReference)getCancelMaterialSubLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelMaterialSubLotType_ReleaseID() {
        return (EAttribute)getCancelMaterialSubLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelMaterialSubLotType_VersionID() {
        return (EAttribute)getCancelMaterialSubLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelMaterialTestSpecType() {
		if (cancelMaterialTestSpecTypeEClass == null) {
			cancelMaterialTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(53);
		}
		return cancelMaterialTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelMaterialTestSpecType_ApplicationArea() {
        return (EReference)getCancelMaterialTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelMaterialTestSpecType_DataArea() {
        return (EReference)getCancelMaterialTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelMaterialTestSpecType_ReleaseID() {
        return (EAttribute)getCancelMaterialTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelMaterialTestSpecType_VersionID() {
        return (EAttribute)getCancelMaterialTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelOperationsCapabilityInformationType() {
		if (cancelOperationsCapabilityInformationTypeEClass == null) {
			cancelOperationsCapabilityInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(54);
		}
		return cancelOperationsCapabilityInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelOperationsCapabilityInformationType_ApplicationArea() {
        return (EReference)getCancelOperationsCapabilityInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelOperationsCapabilityInformationType_DataArea() {
        return (EReference)getCancelOperationsCapabilityInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelOperationsCapabilityInformationType_ReleaseID() {
        return (EAttribute)getCancelOperationsCapabilityInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelOperationsCapabilityInformationType_VersionID() {
        return (EAttribute)getCancelOperationsCapabilityInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelOperationsCapabilityType() {
		if (cancelOperationsCapabilityTypeEClass == null) {
			cancelOperationsCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(55);
		}
		return cancelOperationsCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelOperationsCapabilityType_ApplicationArea() {
        return (EReference)getCancelOperationsCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelOperationsCapabilityType_DataArea() {
        return (EReference)getCancelOperationsCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelOperationsCapabilityType_ReleaseID() {
        return (EAttribute)getCancelOperationsCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelOperationsCapabilityType_VersionID() {
        return (EAttribute)getCancelOperationsCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelOperationsDefinitionInformationType() {
		if (cancelOperationsDefinitionInformationTypeEClass == null) {
			cancelOperationsDefinitionInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(56);
		}
		return cancelOperationsDefinitionInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelOperationsDefinitionInformationType_ApplicationArea() {
        return (EReference)getCancelOperationsDefinitionInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelOperationsDefinitionInformationType_DataArea() {
        return (EReference)getCancelOperationsDefinitionInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelOperationsDefinitionInformationType_ReleaseID() {
        return (EAttribute)getCancelOperationsDefinitionInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelOperationsDefinitionInformationType_VersionID() {
        return (EAttribute)getCancelOperationsDefinitionInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelOperationsDefinitionType() {
		if (cancelOperationsDefinitionTypeEClass == null) {
			cancelOperationsDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(57);
		}
		return cancelOperationsDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelOperationsDefinitionType_ApplicationArea() {
        return (EReference)getCancelOperationsDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelOperationsDefinitionType_DataArea() {
        return (EReference)getCancelOperationsDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelOperationsDefinitionType_ReleaseID() {
        return (EAttribute)getCancelOperationsDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelOperationsDefinitionType_VersionID() {
        return (EAttribute)getCancelOperationsDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelOperationsPerformanceType() {
		if (cancelOperationsPerformanceTypeEClass == null) {
			cancelOperationsPerformanceTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(58);
		}
		return cancelOperationsPerformanceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelOperationsPerformanceType_ApplicationArea() {
        return (EReference)getCancelOperationsPerformanceType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelOperationsPerformanceType_DataArea() {
        return (EReference)getCancelOperationsPerformanceType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelOperationsPerformanceType_ReleaseID() {
        return (EAttribute)getCancelOperationsPerformanceType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelOperationsPerformanceType_VersionID() {
        return (EAttribute)getCancelOperationsPerformanceType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelOperationsScheduleType() {
		if (cancelOperationsScheduleTypeEClass == null) {
			cancelOperationsScheduleTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(59);
		}
		return cancelOperationsScheduleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelOperationsScheduleType_ApplicationArea() {
        return (EReference)getCancelOperationsScheduleType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelOperationsScheduleType_DataArea() {
        return (EReference)getCancelOperationsScheduleType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelOperationsScheduleType_ReleaseID() {
        return (EAttribute)getCancelOperationsScheduleType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelOperationsScheduleType_VersionID() {
        return (EAttribute)getCancelOperationsScheduleType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelPersonnelClassType() {
		if (cancelPersonnelClassTypeEClass == null) {
			cancelPersonnelClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(60);
		}
		return cancelPersonnelClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPersonnelClassType_ApplicationArea() {
        return (EReference)getCancelPersonnelClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPersonnelClassType_DataArea() {
        return (EReference)getCancelPersonnelClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelPersonnelClassType_ReleaseID() {
        return (EAttribute)getCancelPersonnelClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelPersonnelClassType_VersionID() {
        return (EAttribute)getCancelPersonnelClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelPersonnelInformationType() {
		if (cancelPersonnelInformationTypeEClass == null) {
			cancelPersonnelInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(61);
		}
		return cancelPersonnelInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPersonnelInformationType_ApplicationArea() {
        return (EReference)getCancelPersonnelInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPersonnelInformationType_DataArea() {
        return (EReference)getCancelPersonnelInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelPersonType() {
		if (cancelPersonTypeEClass == null) {
			cancelPersonTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(62);
		}
		return cancelPersonTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPersonType_ApplicationArea() {
        return (EReference)getCancelPersonType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPersonType_DataArea() {
        return (EReference)getCancelPersonType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelPersonType_ReleaseID() {
        return (EAttribute)getCancelPersonType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelPersonType_VersionID() {
        return (EAttribute)getCancelPersonType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelPhysicalAssetCapabilityTestSpecType() {
		if (cancelPhysicalAssetCapabilityTestSpecTypeEClass == null) {
			cancelPhysicalAssetCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(63);
		}
		return cancelPhysicalAssetCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPhysicalAssetCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getCancelPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPhysicalAssetCapabilityTestSpecType_DataArea() {
        return (EReference)getCancelPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelPhysicalAssetCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getCancelPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelPhysicalAssetCapabilityTestSpecType_VersionID() {
        return (EAttribute)getCancelPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelPhysicalAssetClassType() {
		if (cancelPhysicalAssetClassTypeEClass == null) {
			cancelPhysicalAssetClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(64);
		}
		return cancelPhysicalAssetClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPhysicalAssetClassType_ApplicationArea() {
        return (EReference)getCancelPhysicalAssetClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPhysicalAssetClassType_DataArea() {
        return (EReference)getCancelPhysicalAssetClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelPhysicalAssetClassType_ReleaseID() {
        return (EAttribute)getCancelPhysicalAssetClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelPhysicalAssetClassType_VersionID() {
        return (EAttribute)getCancelPhysicalAssetClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelPhysicalAssetInformationType() {
		if (cancelPhysicalAssetInformationTypeEClass == null) {
			cancelPhysicalAssetInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(65);
		}
		return cancelPhysicalAssetInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPhysicalAssetInformationType_ApplicationArea() {
        return (EReference)getCancelPhysicalAssetInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPhysicalAssetInformationType_DataArea() {
        return (EReference)getCancelPhysicalAssetInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelPhysicalAssetInformationType_ReleaseID() {
        return (EAttribute)getCancelPhysicalAssetInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelPhysicalAssetInformationType_VersionID() {
        return (EAttribute)getCancelPhysicalAssetInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelPhysicalAssetType() {
		if (cancelPhysicalAssetTypeEClass == null) {
			cancelPhysicalAssetTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(66);
		}
		return cancelPhysicalAssetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPhysicalAssetType_ApplicationArea() {
        return (EReference)getCancelPhysicalAssetType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelPhysicalAssetType_DataArea() {
        return (EReference)getCancelPhysicalAssetType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelPhysicalAssetType_ReleaseID() {
        return (EAttribute)getCancelPhysicalAssetType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelPhysicalAssetType_VersionID() {
        return (EAttribute)getCancelPhysicalAssetType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelProcessSegmentInformationType() {
		if (cancelProcessSegmentInformationTypeEClass == null) {
			cancelProcessSegmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(67);
		}
		return cancelProcessSegmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelProcessSegmentInformationType_ApplicationArea() {
        return (EReference)getCancelProcessSegmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelProcessSegmentInformationType_DataArea() {
        return (EReference)getCancelProcessSegmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelProcessSegmentInformationType_ReleaseID() {
        return (EAttribute)getCancelProcessSegmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelProcessSegmentInformationType_VersionID() {
        return (EAttribute)getCancelProcessSegmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelProcessSegmentType() {
		if (cancelProcessSegmentTypeEClass == null) {
			cancelProcessSegmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(68);
		}
		return cancelProcessSegmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelProcessSegmentType_ApplicationArea() {
        return (EReference)getCancelProcessSegmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelProcessSegmentType_DataArea() {
        return (EReference)getCancelProcessSegmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelProcessSegmentType_ReleaseID() {
        return (EAttribute)getCancelProcessSegmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelProcessSegmentType_VersionID() {
        return (EAttribute)getCancelProcessSegmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCancelQualificationTestSpecificationType() {
		if (cancelQualificationTestSpecificationTypeEClass == null) {
			cancelQualificationTestSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(69);
		}
		return cancelQualificationTestSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelQualificationTestSpecificationType_ApplicationArea() {
        return (EReference)getCancelQualificationTestSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCancelQualificationTestSpecificationType_DataArea() {
        return (EReference)getCancelQualificationTestSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelQualificationTestSpecificationType_ReleaseID() {
        return (EAttribute)getCancelQualificationTestSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCancelQualificationTestSpecificationType_VersionID() {
        return (EAttribute)getCancelQualificationTestSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCapabilityType1Type() {
		if (capabilityType1TypeEClass == null) {
			capabilityType1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(70);
		}
		return capabilityType1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCapabilityTypeType() {
		if (capabilityTypeTypeEClass == null) {
			capabilityTypeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(73);
		}
		return capabilityTypeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCapabilityTypeType_OtherValue() {
        return (EAttribute)getCapabilityTypeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCauseType() {
		if (causeTypeEClass == null) {
			causeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(74);
		}
		return causeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCertificateOfAnalysisReferenceType() {
		if (certificateOfAnalysisReferenceTypeEClass == null) {
			certificateOfAnalysisReferenceTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(75);
		}
		return certificateOfAnalysisReferenceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeEquipmentCapabilityTestSpecType() {
		if (changeEquipmentCapabilityTestSpecTypeEClass == null) {
			changeEquipmentCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(76);
		}
		return changeEquipmentCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeEquipmentCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getChangeEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeEquipmentCapabilityTestSpecType_DataArea() {
        return (EReference)getChangeEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeEquipmentCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getChangeEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeEquipmentCapabilityTestSpecType_VersionID() {
        return (EAttribute)getChangeEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeEquipmentClassType() {
		if (changeEquipmentClassTypeEClass == null) {
			changeEquipmentClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(77);
		}
		return changeEquipmentClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeEquipmentClassType_ApplicationArea() {
        return (EReference)getChangeEquipmentClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeEquipmentClassType_DataArea() {
        return (EReference)getChangeEquipmentClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeEquipmentClassType_ReleaseID() {
        return (EAttribute)getChangeEquipmentClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeEquipmentClassType_VersionID() {
        return (EAttribute)getChangeEquipmentClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeEquipmentInformationType() {
		if (changeEquipmentInformationTypeEClass == null) {
			changeEquipmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(78);
		}
		return changeEquipmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeEquipmentInformationType_ApplicationArea() {
        return (EReference)getChangeEquipmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeEquipmentInformationType_DataArea() {
        return (EReference)getChangeEquipmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeEquipmentInformationType_ReleaseID() {
        return (EAttribute)getChangeEquipmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeEquipmentInformationType_VersionID() {
        return (EAttribute)getChangeEquipmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeEquipmentType() {
		if (changeEquipmentTypeEClass == null) {
			changeEquipmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(79);
		}
		return changeEquipmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeEquipmentType_ApplicationArea() {
        return (EReference)getChangeEquipmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeEquipmentType_DataArea() {
        return (EReference)getChangeEquipmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeEquipmentType_ReleaseID() {
        return (EAttribute)getChangeEquipmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeEquipmentType_VersionID() {
        return (EAttribute)getChangeEquipmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeMaterialClassType() {
		if (changeMaterialClassTypeEClass == null) {
			changeMaterialClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(80);
		}
		return changeMaterialClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeMaterialClassType_ApplicationArea() {
        return (EReference)getChangeMaterialClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeMaterialClassType_DataArea() {
        return (EReference)getChangeMaterialClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeMaterialClassType_ReleaseID() {
        return (EAttribute)getChangeMaterialClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeMaterialClassType_VersionID() {
        return (EAttribute)getChangeMaterialClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeMaterialDefinitionType() {
		if (changeMaterialDefinitionTypeEClass == null) {
			changeMaterialDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(81);
		}
		return changeMaterialDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeMaterialDefinitionType_ApplicationArea() {
        return (EReference)getChangeMaterialDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeMaterialDefinitionType_DataArea() {
        return (EReference)getChangeMaterialDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeMaterialDefinitionType_ReleaseID() {
        return (EAttribute)getChangeMaterialDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeMaterialDefinitionType_VersionID() {
        return (EAttribute)getChangeMaterialDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeMaterialInformationType() {
		if (changeMaterialInformationTypeEClass == null) {
			changeMaterialInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(82);
		}
		return changeMaterialInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeMaterialInformationType_ApplicationArea() {
        return (EReference)getChangeMaterialInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeMaterialInformationType_DataArea() {
        return (EReference)getChangeMaterialInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeMaterialInformationType_ReleaseID() {
        return (EAttribute)getChangeMaterialInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeMaterialInformationType_VersionID() {
        return (EAttribute)getChangeMaterialInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeMaterialLotType() {
		if (changeMaterialLotTypeEClass == null) {
			changeMaterialLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(83);
		}
		return changeMaterialLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeMaterialLotType_ApplicationArea() {
        return (EReference)getChangeMaterialLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeMaterialLotType_DataArea() {
        return (EReference)getChangeMaterialLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeMaterialLotType_ReleaseID() {
        return (EAttribute)getChangeMaterialLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeMaterialLotType_VersionID() {
        return (EAttribute)getChangeMaterialLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeMaterialSubLotType() {
		if (changeMaterialSubLotTypeEClass == null) {
			changeMaterialSubLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(84);
		}
		return changeMaterialSubLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeMaterialSubLotType_ApplicationArea() {
        return (EReference)getChangeMaterialSubLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeMaterialSubLotType_DataArea() {
        return (EReference)getChangeMaterialSubLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeMaterialSubLotType_ReleaseID() {
        return (EAttribute)getChangeMaterialSubLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeMaterialSubLotType_VersionID() {
        return (EAttribute)getChangeMaterialSubLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeMaterialTestSpecType() {
		if (changeMaterialTestSpecTypeEClass == null) {
			changeMaterialTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(85);
		}
		return changeMaterialTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeMaterialTestSpecType_ApplicationArea() {
        return (EReference)getChangeMaterialTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeMaterialTestSpecType_DataArea() {
        return (EReference)getChangeMaterialTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeMaterialTestSpecType_ReleaseID() {
        return (EAttribute)getChangeMaterialTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeMaterialTestSpecType_VersionID() {
        return (EAttribute)getChangeMaterialTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeOperationsCapabilityInformationType() {
		if (changeOperationsCapabilityInformationTypeEClass == null) {
			changeOperationsCapabilityInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(86);
		}
		return changeOperationsCapabilityInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeOperationsCapabilityInformationType_ApplicationArea() {
        return (EReference)getChangeOperationsCapabilityInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeOperationsCapabilityInformationType_DataArea() {
        return (EReference)getChangeOperationsCapabilityInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeOperationsCapabilityInformationType_ReleaseID() {
        return (EAttribute)getChangeOperationsCapabilityInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeOperationsCapabilityInformationType_VersionID() {
        return (EAttribute)getChangeOperationsCapabilityInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeOperationsCapabilityType() {
		if (changeOperationsCapabilityTypeEClass == null) {
			changeOperationsCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(87);
		}
		return changeOperationsCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeOperationsCapabilityType_ApplicationArea() {
        return (EReference)getChangeOperationsCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeOperationsCapabilityType_DataArea() {
        return (EReference)getChangeOperationsCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeOperationsCapabilityType_ReleaseID() {
        return (EAttribute)getChangeOperationsCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeOperationsCapabilityType_VersionID() {
        return (EAttribute)getChangeOperationsCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeOperationsDefinitionInformationType() {
		if (changeOperationsDefinitionInformationTypeEClass == null) {
			changeOperationsDefinitionInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(88);
		}
		return changeOperationsDefinitionInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeOperationsDefinitionInformationType_ApplicationArea() {
        return (EReference)getChangeOperationsDefinitionInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeOperationsDefinitionInformationType_DataArea() {
        return (EReference)getChangeOperationsDefinitionInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeOperationsDefinitionInformationType_ReleaseID() {
        return (EAttribute)getChangeOperationsDefinitionInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeOperationsDefinitionInformationType_VersionID() {
        return (EAttribute)getChangeOperationsDefinitionInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeOperationsDefinitionType() {
		if (changeOperationsDefinitionTypeEClass == null) {
			changeOperationsDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(89);
		}
		return changeOperationsDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeOperationsDefinitionType_ApplicationArea() {
        return (EReference)getChangeOperationsDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeOperationsDefinitionType_DataArea() {
        return (EReference)getChangeOperationsDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeOperationsDefinitionType_ReleaseID() {
        return (EAttribute)getChangeOperationsDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeOperationsDefinitionType_VersionID() {
        return (EAttribute)getChangeOperationsDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeOperationsPerformanceType() {
		if (changeOperationsPerformanceTypeEClass == null) {
			changeOperationsPerformanceTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(90);
		}
		return changeOperationsPerformanceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeOperationsPerformanceType_ApplicationArea() {
        return (EReference)getChangeOperationsPerformanceType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeOperationsPerformanceType_DataArea() {
        return (EReference)getChangeOperationsPerformanceType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeOperationsPerformanceType_ReleaseID() {
        return (EAttribute)getChangeOperationsPerformanceType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeOperationsPerformanceType_VersionID() {
        return (EAttribute)getChangeOperationsPerformanceType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeOperationsScheduleType() {
		if (changeOperationsScheduleTypeEClass == null) {
			changeOperationsScheduleTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(91);
		}
		return changeOperationsScheduleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeOperationsScheduleType_ApplicationArea() {
        return (EReference)getChangeOperationsScheduleType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeOperationsScheduleType_DataArea() {
        return (EReference)getChangeOperationsScheduleType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeOperationsScheduleType_ReleaseID() {
        return (EAttribute)getChangeOperationsScheduleType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeOperationsScheduleType_VersionID() {
        return (EAttribute)getChangeOperationsScheduleType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangePersonnelClassType() {
		if (changePersonnelClassTypeEClass == null) {
			changePersonnelClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(92);
		}
		return changePersonnelClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePersonnelClassType_ApplicationArea() {
        return (EReference)getChangePersonnelClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePersonnelClassType_DataArea() {
        return (EReference)getChangePersonnelClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePersonnelClassType_ReleaseID() {
        return (EAttribute)getChangePersonnelClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePersonnelClassType_VersionID() {
        return (EAttribute)getChangePersonnelClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangePersonnelInformationType() {
		if (changePersonnelInformationTypeEClass == null) {
			changePersonnelInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(93);
		}
		return changePersonnelInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePersonnelInformationType_ApplicationArea() {
        return (EReference)getChangePersonnelInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePersonnelInformationType_DataArea() {
        return (EReference)getChangePersonnelInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePersonnelInformationType_ReleaseID() {
        return (EAttribute)getChangePersonnelInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePersonnelInformationType_VersionID() {
        return (EAttribute)getChangePersonnelInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangePersonType() {
		if (changePersonTypeEClass == null) {
			changePersonTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(94);
		}
		return changePersonTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePersonType_ApplicationArea() {
        return (EReference)getChangePersonType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePersonType_DataArea() {
        return (EReference)getChangePersonType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePersonType_ReleaseID() {
        return (EAttribute)getChangePersonType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePersonType_VersionID() {
        return (EAttribute)getChangePersonType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangePhysicalAssetCapabilityTestSpecType() {
		if (changePhysicalAssetCapabilityTestSpecTypeEClass == null) {
			changePhysicalAssetCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(95);
		}
		return changePhysicalAssetCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePhysicalAssetCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getChangePhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePhysicalAssetCapabilityTestSpecType_DataArea() {
        return (EReference)getChangePhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePhysicalAssetCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getChangePhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePhysicalAssetCapabilityTestSpecType_VersionID() {
        return (EAttribute)getChangePhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangePhysicalAssetClassType() {
		if (changePhysicalAssetClassTypeEClass == null) {
			changePhysicalAssetClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(96);
		}
		return changePhysicalAssetClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePhysicalAssetClassType_ApplicationArea() {
        return (EReference)getChangePhysicalAssetClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePhysicalAssetClassType_DataArea() {
        return (EReference)getChangePhysicalAssetClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePhysicalAssetClassType_ReleaseID() {
        return (EAttribute)getChangePhysicalAssetClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePhysicalAssetClassType_VersionID() {
        return (EAttribute)getChangePhysicalAssetClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangePhysicalAssetInformationType() {
		if (changePhysicalAssetInformationTypeEClass == null) {
			changePhysicalAssetInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(97);
		}
		return changePhysicalAssetInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePhysicalAssetInformationType_ApplicationArea() {
        return (EReference)getChangePhysicalAssetInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePhysicalAssetInformationType_DataArea() {
        return (EReference)getChangePhysicalAssetInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePhysicalAssetInformationType_ReleaseID() {
        return (EAttribute)getChangePhysicalAssetInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePhysicalAssetInformationType_VersionID() {
        return (EAttribute)getChangePhysicalAssetInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangePhysicalAssetType() {
		if (changePhysicalAssetTypeEClass == null) {
			changePhysicalAssetTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(98);
		}
		return changePhysicalAssetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePhysicalAssetType_ApplicationArea() {
        return (EReference)getChangePhysicalAssetType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangePhysicalAssetType_DataArea() {
        return (EReference)getChangePhysicalAssetType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePhysicalAssetType_ReleaseID() {
        return (EAttribute)getChangePhysicalAssetType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangePhysicalAssetType_VersionID() {
        return (EAttribute)getChangePhysicalAssetType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeProcessSegmentInformationType() {
		if (changeProcessSegmentInformationTypeEClass == null) {
			changeProcessSegmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(99);
		}
		return changeProcessSegmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeProcessSegmentInformationType_ApplicationArea() {
        return (EReference)getChangeProcessSegmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeProcessSegmentInformationType_DataArea() {
        return (EReference)getChangeProcessSegmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeProcessSegmentInformationType_ReleaseID() {
        return (EAttribute)getChangeProcessSegmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeProcessSegmentInformationType_VersionID() {
        return (EAttribute)getChangeProcessSegmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeProcessSegmentType() {
		if (changeProcessSegmentTypeEClass == null) {
			changeProcessSegmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(100);
		}
		return changeProcessSegmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeProcessSegmentType_ApplicationArea() {
        return (EReference)getChangeProcessSegmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeProcessSegmentType_DataArea() {
        return (EReference)getChangeProcessSegmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeProcessSegmentType_ReleaseID() {
        return (EAttribute)getChangeProcessSegmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeProcessSegmentType_VersionID() {
        return (EAttribute)getChangeProcessSegmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeQualificationTestSpecificationType() {
		if (changeQualificationTestSpecificationTypeEClass == null) {
			changeQualificationTestSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(101);
		}
		return changeQualificationTestSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeQualificationTestSpecificationType_ApplicationArea() {
        return (EReference)getChangeQualificationTestSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeQualificationTestSpecificationType_DataArea() {
        return (EReference)getChangeQualificationTestSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeQualificationTestSpecificationType_ReleaseID() {
        return (EAttribute)getChangeQualificationTestSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeQualificationTestSpecificationType_VersionID() {
        return (EAttribute)getChangeQualificationTestSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeType() {
		if (codeTypeEClass == null) {
			codeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(102);
		}
		return codeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeType_Value() {
        return (EAttribute)getCodeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeType_LanguageID() {
        return (EAttribute)getCodeType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeType_ListAgencyID() {
        return (EAttribute)getCodeType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeType_ListAgencyName() {
        return (EAttribute)getCodeType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeType_ListID() {
        return (EAttribute)getCodeType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeType_ListName() {
        return (EAttribute)getCodeType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeType_ListSchemeURI() {
        return (EAttribute)getCodeType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeType_ListURI() {
        return (EAttribute)getCodeType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeType_ListVersionID() {
        return (EAttribute)getCodeType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeType_Name() {
        return (EAttribute)getCodeType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfidenceFactorType() {
		if (confidenceFactorTypeEClass == null) {
			confidenceFactorTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(103);
		}
		return confidenceFactorTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfirmBODType() {
		if (confirmBODTypeEClass == null) {
			confirmBODTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(104);
		}
		return confirmBODTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfirmBODType_ApplicationArea() {
        return (EReference)getConfirmBODType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfirmBODType_DataArea() {
        return (EReference)getConfirmBODType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrectionType() {
		if (correctionTypeEClass == null) {
			correctionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(105);
		}
		return correctionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType() {
		if (dataAreaTypeEClass == null) {
			dataAreaTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(106);
		}
		return dataAreaTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType_Get() {
        return (EReference)getDataAreaType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType_MaterialTestSpec() {
        return (EReference)getDataAreaType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType1() {
		if (dataAreaType1EClass == null) {
			dataAreaType1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(107);
		}
		return dataAreaType1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType1_Get() {
        return (EReference)getDataAreaType1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType1_OperationsCapabilityInformation() {
        return (EReference)getDataAreaType1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType2() {
		if (dataAreaType2EClass == null) {
			dataAreaType2EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(108);
		}
		return dataAreaType2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType2_Get() {
        return (EReference)getDataAreaType2().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType2_OperationsDefinitionInformation() {
        return (EReference)getDataAreaType2().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType3() {
		if (dataAreaType3EClass == null) {
			dataAreaType3EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(109);
		}
		return dataAreaType3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType3_Get() {
        return (EReference)getDataAreaType3().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType3_OperationsSchedule() {
        return (EReference)getDataAreaType3().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType4() {
		if (dataAreaType4EClass == null) {
			dataAreaType4EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(110);
		}
		return dataAreaType4EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType4_Get() {
        return (EReference)getDataAreaType4().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType4_PersonnelClass() {
        return (EReference)getDataAreaType4().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType5() {
		if (dataAreaType5EClass == null) {
			dataAreaType5EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(111);
		}
		return dataAreaType5EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType5_Get() {
        return (EReference)getDataAreaType5().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType5_PhysicalAsset() {
        return (EReference)getDataAreaType5().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType6() {
		if (dataAreaType6EClass == null) {
			dataAreaType6EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(112);
		}
		return dataAreaType6EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType6_Get() {
        return (EReference)getDataAreaType6().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType6_PhysicalAssetClass() {
        return (EReference)getDataAreaType6().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType7() {
		if (dataAreaType7EClass == null) {
			dataAreaType7EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(113);
		}
		return dataAreaType7EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType7_Get() {
        return (EReference)getDataAreaType7().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType7_ProcessSegment() {
        return (EReference)getDataAreaType7().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType8() {
		if (dataAreaType8EClass == null) {
			dataAreaType8EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(114);
		}
		return dataAreaType8EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType8_Get() {
        return (EReference)getDataAreaType8().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType8_QualificationTestSpecification() {
        return (EReference)getDataAreaType8().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType9() {
		if (dataAreaType9EClass == null) {
			dataAreaType9EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(115);
		}
		return dataAreaType9EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType9_Process() {
        return (EReference)getDataAreaType9().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType9_EquipmentCapabilityTestSpec() {
        return (EReference)getDataAreaType9().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType10() {
		if (dataAreaType10EClass == null) {
			dataAreaType10EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(116);
		}
		return dataAreaType10EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType10_Process() {
        return (EReference)getDataAreaType10().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType10_EquipmentInformation() {
        return (EReference)getDataAreaType10().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType11() {
		if (dataAreaType11EClass == null) {
			dataAreaType11EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(117);
		}
		return dataAreaType11EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType11_Process() {
        return (EReference)getDataAreaType11().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType11_MaterialDefinition() {
        return (EReference)getDataAreaType11().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType12() {
		if (dataAreaType12EClass == null) {
			dataAreaType12EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(118);
		}
		return dataAreaType12EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType12_Process() {
        return (EReference)getDataAreaType12().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType12_MaterialLot() {
        return (EReference)getDataAreaType12().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType13() {
		if (dataAreaType13EClass == null) {
			dataAreaType13EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(119);
		}
		return dataAreaType13EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType13_Process() {
        return (EReference)getDataAreaType13().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType13_MaterialTestSpec() {
        return (EReference)getDataAreaType13().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType14() {
		if (dataAreaType14EClass == null) {
			dataAreaType14EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(120);
		}
		return dataAreaType14EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType14_Process() {
        return (EReference)getDataAreaType14().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType14_OperationsCapabilityInformation() {
        return (EReference)getDataAreaType14().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType15() {
		if (dataAreaType15EClass == null) {
			dataAreaType15EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(121);
		}
		return dataAreaType15EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType15_Process() {
        return (EReference)getDataAreaType15().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType15_OperationsDefinitionInformation() {
        return (EReference)getDataAreaType15().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType16() {
		if (dataAreaType16EClass == null) {
			dataAreaType16EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(122);
		}
		return dataAreaType16EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType16_Process() {
        return (EReference)getDataAreaType16().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType16_OperationsSchedule() {
        return (EReference)getDataAreaType16().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType17() {
		if (dataAreaType17EClass == null) {
			dataAreaType17EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(123);
		}
		return dataAreaType17EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType17_Process() {
        return (EReference)getDataAreaType17().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType17_PersonnelClass() {
        return (EReference)getDataAreaType17().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType18() {
		if (dataAreaType18EClass == null) {
			dataAreaType18EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(124);
		}
		return dataAreaType18EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType18_Process() {
        return (EReference)getDataAreaType18().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType18_PhysicalAsset() {
        return (EReference)getDataAreaType18().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType19() {
		if (dataAreaType19EClass == null) {
			dataAreaType19EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(125);
		}
		return dataAreaType19EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType19_Process() {
        return (EReference)getDataAreaType19().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType19_PhysicalAssetClass() {
        return (EReference)getDataAreaType19().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType20() {
		if (dataAreaType20EClass == null) {
			dataAreaType20EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(126);
		}
		return dataAreaType20EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType20_Process() {
        return (EReference)getDataAreaType20().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType20_ProcessSegment() {
        return (EReference)getDataAreaType20().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType21() {
		if (dataAreaType21EClass == null) {
			dataAreaType21EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(127);
		}
		return dataAreaType21EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType21_Process() {
        return (EReference)getDataAreaType21().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType21_QualificationTestSpecification() {
        return (EReference)getDataAreaType21().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType22() {
		if (dataAreaType22EClass == null) {
			dataAreaType22EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(128);
		}
		return dataAreaType22EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType22_Respond() {
        return (EReference)getDataAreaType22().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType22_Equipment() {
        return (EReference)getDataAreaType22().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType23() {
		if (dataAreaType23EClass == null) {
			dataAreaType23EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(129);
		}
		return dataAreaType23EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType23_Respond() {
        return (EReference)getDataAreaType23().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType23_EquipmentClass() {
        return (EReference)getDataAreaType23().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType24() {
		if (dataAreaType24EClass == null) {
			dataAreaType24EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(130);
		}
		return dataAreaType24EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType24_Respond() {
        return (EReference)getDataAreaType24().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType24_MaterialClass() {
        return (EReference)getDataAreaType24().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType25() {
		if (dataAreaType25EClass == null) {
			dataAreaType25EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(131);
		}
		return dataAreaType25EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType25_Respond() {
        return (EReference)getDataAreaType25().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType25_MaterialInformation() {
        return (EReference)getDataAreaType25().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType26() {
		if (dataAreaType26EClass == null) {
			dataAreaType26EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(132);
		}
		return dataAreaType26EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType26_Respond() {
        return (EReference)getDataAreaType26().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType26_MaterialSubLot() {
        return (EReference)getDataAreaType26().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType27() {
		if (dataAreaType27EClass == null) {
			dataAreaType27EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(133);
		}
		return dataAreaType27EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType27_Respond() {
        return (EReference)getDataAreaType27().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType27_OperationsCapability() {
        return (EReference)getDataAreaType27().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType28() {
		if (dataAreaType28EClass == null) {
			dataAreaType28EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(134);
		}
		return dataAreaType28EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType28_Respond() {
        return (EReference)getDataAreaType28().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType28_OperationsDefinition() {
        return (EReference)getDataAreaType28().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType29() {
		if (dataAreaType29EClass == null) {
			dataAreaType29EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(135);
		}
		return dataAreaType29EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType29_Respond() {
        return (EReference)getDataAreaType29().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType29_OperationsPerformance() {
        return (EReference)getDataAreaType29().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType30() {
		if (dataAreaType30EClass == null) {
			dataAreaType30EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(136);
		}
		return dataAreaType30EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType30_Respond() {
        return (EReference)getDataAreaType30().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType30_Person() {
        return (EReference)getDataAreaType30().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType31() {
		if (dataAreaType31EClass == null) {
			dataAreaType31EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(137);
		}
		return dataAreaType31EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType31_Respond() {
        return (EReference)getDataAreaType31().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType31_PersonnelInformation() {
        return (EReference)getDataAreaType31().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType32() {
		if (dataAreaType32EClass == null) {
			dataAreaType32EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(138);
		}
		return dataAreaType32EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType32_Respond() {
        return (EReference)getDataAreaType32().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType32_PhysicalAssetCapabilityTestSpec() {
        return (EReference)getDataAreaType32().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType33() {
		if (dataAreaType33EClass == null) {
			dataAreaType33EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(139);
		}
		return dataAreaType33EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType33_Respond() {
        return (EReference)getDataAreaType33().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType33_PhysicalAssetInformation() {
        return (EReference)getDataAreaType33().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType34() {
		if (dataAreaType34EClass == null) {
			dataAreaType34EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(140);
		}
		return dataAreaType34EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType34_Respond() {
        return (EReference)getDataAreaType34().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType34_ProcessSegmentInformation() {
        return (EReference)getDataAreaType34().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType35() {
		if (dataAreaType35EClass == null) {
			dataAreaType35EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(141);
		}
		return dataAreaType35EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType35_Show() {
        return (EReference)getDataAreaType35().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType35_Equipment() {
        return (EReference)getDataAreaType35().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType36() {
		if (dataAreaType36EClass == null) {
			dataAreaType36EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(142);
		}
		return dataAreaType36EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType36_Show() {
        return (EReference)getDataAreaType36().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType36_EquipmentClass() {
        return (EReference)getDataAreaType36().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType37() {
		if (dataAreaType37EClass == null) {
			dataAreaType37EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(143);
		}
		return dataAreaType37EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType37_Show() {
        return (EReference)getDataAreaType37().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType37_MaterialClass() {
        return (EReference)getDataAreaType37().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType38() {
		if (dataAreaType38EClass == null) {
			dataAreaType38EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(144);
		}
		return dataAreaType38EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType38_Show() {
        return (EReference)getDataAreaType38().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType38_MaterialInformation() {
        return (EReference)getDataAreaType38().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType39() {
		if (dataAreaType39EClass == null) {
			dataAreaType39EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(145);
		}
		return dataAreaType39EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType39_Show() {
        return (EReference)getDataAreaType39().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType39_MaterialSubLot() {
        return (EReference)getDataAreaType39().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType40() {
		if (dataAreaType40EClass == null) {
			dataAreaType40EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(146);
		}
		return dataAreaType40EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType40_Show() {
        return (EReference)getDataAreaType40().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType40_OperationsCapability() {
        return (EReference)getDataAreaType40().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType41() {
		if (dataAreaType41EClass == null) {
			dataAreaType41EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(147);
		}
		return dataAreaType41EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType41_Show() {
        return (EReference)getDataAreaType41().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType41_OperationsDefinition() {
        return (EReference)getDataAreaType41().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType42() {
		if (dataAreaType42EClass == null) {
			dataAreaType42EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(148);
		}
		return dataAreaType42EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType42_Show() {
        return (EReference)getDataAreaType42().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType42_OperationsPerformance() {
        return (EReference)getDataAreaType42().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType43() {
		if (dataAreaType43EClass == null) {
			dataAreaType43EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(149);
		}
		return dataAreaType43EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType43_Show() {
        return (EReference)getDataAreaType43().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType43_Person() {
        return (EReference)getDataAreaType43().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType44() {
		if (dataAreaType44EClass == null) {
			dataAreaType44EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(150);
		}
		return dataAreaType44EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType44_Show() {
        return (EReference)getDataAreaType44().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType44_PersonnelInformation() {
        return (EReference)getDataAreaType44().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType45() {
		if (dataAreaType45EClass == null) {
			dataAreaType45EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(151);
		}
		return dataAreaType45EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType45_Show() {
        return (EReference)getDataAreaType45().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType45_PhysicalAssetCapabilityTestSpec() {
        return (EReference)getDataAreaType45().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType46() {
		if (dataAreaType46EClass == null) {
			dataAreaType46EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(152);
		}
		return dataAreaType46EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType46_Show() {
        return (EReference)getDataAreaType46().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType46_PhysicalAssetInformation() {
        return (EReference)getDataAreaType46().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType47() {
		if (dataAreaType47EClass == null) {
			dataAreaType47EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(153);
		}
		return dataAreaType47EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType47_Show() {
        return (EReference)getDataAreaType47().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType47_ProcessSegmentInformation() {
        return (EReference)getDataAreaType47().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType48() {
		if (dataAreaType48EClass == null) {
			dataAreaType48EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(154);
		}
		return dataAreaType48EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType48_Sync() {
        return (EReference)getDataAreaType48().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType48_Equipment() {
        return (EReference)getDataAreaType48().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType49() {
		if (dataAreaType49EClass == null) {
			dataAreaType49EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(155);
		}
		return dataAreaType49EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType49_Sync() {
        return (EReference)getDataAreaType49().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType49_EquipmentCapabilityTestSpec() {
        return (EReference)getDataAreaType49().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType50() {
		if (dataAreaType50EClass == null) {
			dataAreaType50EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(156);
		}
		return dataAreaType50EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType50_Sync() {
        return (EReference)getDataAreaType50().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType50_EquipmentInformation() {
        return (EReference)getDataAreaType50().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType51() {
		if (dataAreaType51EClass == null) {
			dataAreaType51EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(157);
		}
		return dataAreaType51EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType51_Sync() {
        return (EReference)getDataAreaType51().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType51_MaterialDefinition() {
        return (EReference)getDataAreaType51().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType52() {
		if (dataAreaType52EClass == null) {
			dataAreaType52EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(158);
		}
		return dataAreaType52EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType52_Sync() {
        return (EReference)getDataAreaType52().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType52_MaterialLot() {
        return (EReference)getDataAreaType52().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType53() {
		if (dataAreaType53EClass == null) {
			dataAreaType53EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(159);
		}
		return dataAreaType53EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType53_Sync() {
        return (EReference)getDataAreaType53().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType53_MaterialTestSpec() {
        return (EReference)getDataAreaType53().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType54() {
		if (dataAreaType54EClass == null) {
			dataAreaType54EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(160);
		}
		return dataAreaType54EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType54_Sync() {
        return (EReference)getDataAreaType54().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType54_OperationsCapabilityInformation() {
        return (EReference)getDataAreaType54().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType55() {
		if (dataAreaType55EClass == null) {
			dataAreaType55EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(161);
		}
		return dataAreaType55EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType55_Sync() {
        return (EReference)getDataAreaType55().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType55_OperationsDefinitionInformation() {
        return (EReference)getDataAreaType55().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType56() {
		if (dataAreaType56EClass == null) {
			dataAreaType56EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(162);
		}
		return dataAreaType56EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType56_Sync() {
        return (EReference)getDataAreaType56().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType56_OperationsSchedule() {
        return (EReference)getDataAreaType56().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType57() {
		if (dataAreaType57EClass == null) {
			dataAreaType57EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(163);
		}
		return dataAreaType57EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType57_Sync() {
        return (EReference)getDataAreaType57().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType57_PersonnelClass() {
        return (EReference)getDataAreaType57().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType58() {
		if (dataAreaType58EClass == null) {
			dataAreaType58EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(164);
		}
		return dataAreaType58EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType58_Sync() {
        return (EReference)getDataAreaType58().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType58_PhysicalAsset() {
        return (EReference)getDataAreaType58().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType59() {
		if (dataAreaType59EClass == null) {
			dataAreaType59EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(165);
		}
		return dataAreaType59EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType59_Sync() {
        return (EReference)getDataAreaType59().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType59_PhysicalAssetClass() {
        return (EReference)getDataAreaType59().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType60() {
		if (dataAreaType60EClass == null) {
			dataAreaType60EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(166);
		}
		return dataAreaType60EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType60_Sync() {
        return (EReference)getDataAreaType60().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType60_ProcessSegment() {
        return (EReference)getDataAreaType60().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType61() {
		if (dataAreaType61EClass == null) {
			dataAreaType61EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(167);
		}
		return dataAreaType61EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType61_Sync() {
        return (EReference)getDataAreaType61().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType61_QualificationTestSpecification() {
        return (EReference)getDataAreaType61().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType62() {
		if (dataAreaType62EClass == null) {
			dataAreaType62EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(168);
		}
		return dataAreaType62EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType62_Sync() {
        return (EReference)getDataAreaType62().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType62_ProcessSegmentInformation() {
        return (EReference)getDataAreaType62().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType63() {
		if (dataAreaType63EClass == null) {
			dataAreaType63EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(169);
		}
		return dataAreaType63EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType63_Sync() {
        return (EReference)getDataAreaType63().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType63_PhysicalAssetInformation() {
        return (EReference)getDataAreaType63().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType64() {
		if (dataAreaType64EClass == null) {
			dataAreaType64EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(170);
		}
		return dataAreaType64EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType64_Sync() {
        return (EReference)getDataAreaType64().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType64_PhysicalAssetCapabilityTestSpec() {
        return (EReference)getDataAreaType64().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType65() {
		if (dataAreaType65EClass == null) {
			dataAreaType65EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(171);
		}
		return dataAreaType65EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType65_Sync() {
        return (EReference)getDataAreaType65().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType65_PersonnelInformation() {
        return (EReference)getDataAreaType65().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType66() {
		if (dataAreaType66EClass == null) {
			dataAreaType66EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(172);
		}
		return dataAreaType66EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType66_Sync() {
        return (EReference)getDataAreaType66().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType66_Person() {
        return (EReference)getDataAreaType66().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType67() {
		if (dataAreaType67EClass == null) {
			dataAreaType67EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(173);
		}
		return dataAreaType67EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType67_Sync() {
        return (EReference)getDataAreaType67().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType67_OperationsPerformance() {
        return (EReference)getDataAreaType67().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType68() {
		if (dataAreaType68EClass == null) {
			dataAreaType68EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(174);
		}
		return dataAreaType68EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType68_Sync() {
        return (EReference)getDataAreaType68().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType68_OperationsDefinition() {
        return (EReference)getDataAreaType68().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType69() {
		if (dataAreaType69EClass == null) {
			dataAreaType69EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(175);
		}
		return dataAreaType69EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType69_Sync() {
        return (EReference)getDataAreaType69().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType69_OperationsCapability() {
        return (EReference)getDataAreaType69().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType70() {
		if (dataAreaType70EClass == null) {
			dataAreaType70EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(176);
		}
		return dataAreaType70EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType70_Sync() {
        return (EReference)getDataAreaType70().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType70_MaterialSubLot() {
        return (EReference)getDataAreaType70().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType71() {
		if (dataAreaType71EClass == null) {
			dataAreaType71EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(177);
		}
		return dataAreaType71EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType71_Sync() {
        return (EReference)getDataAreaType71().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType71_MaterialInformation() {
        return (EReference)getDataAreaType71().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType72() {
		if (dataAreaType72EClass == null) {
			dataAreaType72EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(178);
		}
		return dataAreaType72EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType72_Sync() {
        return (EReference)getDataAreaType72().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType72_MaterialClass() {
        return (EReference)getDataAreaType72().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType73() {
		if (dataAreaType73EClass == null) {
			dataAreaType73EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(179);
		}
		return dataAreaType73EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType73_Sync() {
        return (EReference)getDataAreaType73().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType73_EquipmentClass() {
        return (EReference)getDataAreaType73().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType74() {
		if (dataAreaType74EClass == null) {
			dataAreaType74EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(180);
		}
		return dataAreaType74EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType74_Show() {
        return (EReference)getDataAreaType74().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType74_QualificationTestSpecification() {
        return (EReference)getDataAreaType74().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType75() {
		if (dataAreaType75EClass == null) {
			dataAreaType75EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(181);
		}
		return dataAreaType75EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType75_Show() {
        return (EReference)getDataAreaType75().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType75_ProcessSegment() {
        return (EReference)getDataAreaType75().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType76() {
		if (dataAreaType76EClass == null) {
			dataAreaType76EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(182);
		}
		return dataAreaType76EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType76_Show() {
        return (EReference)getDataAreaType76().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType76_PhysicalAssetClass() {
        return (EReference)getDataAreaType76().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType77() {
		if (dataAreaType77EClass == null) {
			dataAreaType77EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(183);
		}
		return dataAreaType77EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType77_Show() {
        return (EReference)getDataAreaType77().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType77_PhysicalAsset() {
        return (EReference)getDataAreaType77().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType78() {
		if (dataAreaType78EClass == null) {
			dataAreaType78EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(184);
		}
		return dataAreaType78EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType78_Show() {
        return (EReference)getDataAreaType78().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType78_PersonnelClass() {
        return (EReference)getDataAreaType78().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType79() {
		if (dataAreaType79EClass == null) {
			dataAreaType79EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(185);
		}
		return dataAreaType79EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType79_Show() {
        return (EReference)getDataAreaType79().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType79_OperationsSchedule() {
        return (EReference)getDataAreaType79().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType80() {
		if (dataAreaType80EClass == null) {
			dataAreaType80EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(186);
		}
		return dataAreaType80EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType80_Show() {
        return (EReference)getDataAreaType80().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType80_OperationsDefinitionInformation() {
        return (EReference)getDataAreaType80().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType81() {
		if (dataAreaType81EClass == null) {
			dataAreaType81EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(187);
		}
		return dataAreaType81EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType81_Show() {
        return (EReference)getDataAreaType81().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType81_OperationsCapabilityInformation() {
        return (EReference)getDataAreaType81().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType82() {
		if (dataAreaType82EClass == null) {
			dataAreaType82EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(188);
		}
		return dataAreaType82EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType82_Show() {
        return (EReference)getDataAreaType82().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType82_MaterialTestSpec() {
        return (EReference)getDataAreaType82().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType83() {
		if (dataAreaType83EClass == null) {
			dataAreaType83EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(189);
		}
		return dataAreaType83EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType83_Show() {
        return (EReference)getDataAreaType83().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType83_MaterialLot() {
        return (EReference)getDataAreaType83().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType84() {
		if (dataAreaType84EClass == null) {
			dataAreaType84EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(190);
		}
		return dataAreaType84EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType84_Show() {
        return (EReference)getDataAreaType84().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType84_MaterialDefinition() {
        return (EReference)getDataAreaType84().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType85() {
		if (dataAreaType85EClass == null) {
			dataAreaType85EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(191);
		}
		return dataAreaType85EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType85_Show() {
        return (EReference)getDataAreaType85().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType85_EquipmentInformation() {
        return (EReference)getDataAreaType85().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType86() {
		if (dataAreaType86EClass == null) {
			dataAreaType86EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(192);
		}
		return dataAreaType86EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType86_Show() {
        return (EReference)getDataAreaType86().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType86_EquipmentCapabilityTestSpec() {
        return (EReference)getDataAreaType86().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType87() {
		if (dataAreaType87EClass == null) {
			dataAreaType87EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(193);
		}
		return dataAreaType87EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType87_Respond() {
        return (EReference)getDataAreaType87().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType87_QualificationTestSpecification() {
        return (EReference)getDataAreaType87().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType88() {
		if (dataAreaType88EClass == null) {
			dataAreaType88EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(194);
		}
		return dataAreaType88EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType88_Respond() {
        return (EReference)getDataAreaType88().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType88_ProcessSegment() {
        return (EReference)getDataAreaType88().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType89() {
		if (dataAreaType89EClass == null) {
			dataAreaType89EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(195);
		}
		return dataAreaType89EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType89_Respond() {
        return (EReference)getDataAreaType89().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType89_PhysicalAssetClass() {
        return (EReference)getDataAreaType89().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType90() {
		if (dataAreaType90EClass == null) {
			dataAreaType90EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(196);
		}
		return dataAreaType90EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType90_Respond() {
        return (EReference)getDataAreaType90().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType90_PhysicalAsset() {
        return (EReference)getDataAreaType90().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType91() {
		if (dataAreaType91EClass == null) {
			dataAreaType91EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(197);
		}
		return dataAreaType91EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType91_Respond() {
        return (EReference)getDataAreaType91().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType91_PersonnelClass() {
        return (EReference)getDataAreaType91().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType92() {
		if (dataAreaType92EClass == null) {
			dataAreaType92EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(198);
		}
		return dataAreaType92EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType92_Respond() {
        return (EReference)getDataAreaType92().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType92_OperationsSchedule() {
        return (EReference)getDataAreaType92().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType93() {
		if (dataAreaType93EClass == null) {
			dataAreaType93EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(199);
		}
		return dataAreaType93EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType93_Respond() {
        return (EReference)getDataAreaType93().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType93_OperationsDefinitionInformation() {
        return (EReference)getDataAreaType93().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType94() {
		if (dataAreaType94EClass == null) {
			dataAreaType94EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(200);
		}
		return dataAreaType94EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType94_Respond() {
        return (EReference)getDataAreaType94().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType94_OperationsCapabilityInformation() {
        return (EReference)getDataAreaType94().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType95() {
		if (dataAreaType95EClass == null) {
			dataAreaType95EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(201);
		}
		return dataAreaType95EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType95_Respond() {
        return (EReference)getDataAreaType95().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType95_MaterialTestSpec() {
        return (EReference)getDataAreaType95().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType96() {
		if (dataAreaType96EClass == null) {
			dataAreaType96EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(202);
		}
		return dataAreaType96EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType96_Respond() {
        return (EReference)getDataAreaType96().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType96_MaterialLot() {
        return (EReference)getDataAreaType96().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType97() {
		if (dataAreaType97EClass == null) {
			dataAreaType97EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(203);
		}
		return dataAreaType97EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType97_Respond() {
        return (EReference)getDataAreaType97().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType97_MaterialDefinition() {
        return (EReference)getDataAreaType97().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType98() {
		if (dataAreaType98EClass == null) {
			dataAreaType98EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(204);
		}
		return dataAreaType98EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType98_Respond() {
        return (EReference)getDataAreaType98().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType98_EquipmentInformation() {
        return (EReference)getDataAreaType98().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType99() {
		if (dataAreaType99EClass == null) {
			dataAreaType99EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(205);
		}
		return dataAreaType99EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType99_Respond() {
        return (EReference)getDataAreaType99().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType99_EquipmentCapabilityTestSpec() {
        return (EReference)getDataAreaType99().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType100() {
		if (dataAreaType100EClass == null) {
			dataAreaType100EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(206);
		}
		return dataAreaType100EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType100_Process() {
        return (EReference)getDataAreaType100().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType100_ProcessSegmentInformation() {
        return (EReference)getDataAreaType100().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType101() {
		if (dataAreaType101EClass == null) {
			dataAreaType101EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(207);
		}
		return dataAreaType101EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType101_Process() {
        return (EReference)getDataAreaType101().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType101_PhysicalAssetInformation() {
        return (EReference)getDataAreaType101().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType102() {
		if (dataAreaType102EClass == null) {
			dataAreaType102EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(208);
		}
		return dataAreaType102EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType102_Process() {
        return (EReference)getDataAreaType102().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType102_PhysicalAssetCapabilityTestSpec() {
        return (EReference)getDataAreaType102().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType103() {
		if (dataAreaType103EClass == null) {
			dataAreaType103EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(209);
		}
		return dataAreaType103EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType103_Process() {
        return (EReference)getDataAreaType103().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType103_PersonnelInformation() {
        return (EReference)getDataAreaType103().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType104() {
		if (dataAreaType104EClass == null) {
			dataAreaType104EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(210);
		}
		return dataAreaType104EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType104_Process() {
        return (EReference)getDataAreaType104().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType104_Person() {
        return (EReference)getDataAreaType104().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType105() {
		if (dataAreaType105EClass == null) {
			dataAreaType105EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(211);
		}
		return dataAreaType105EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType105_Process() {
        return (EReference)getDataAreaType105().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType105_OperationsPerformance() {
        return (EReference)getDataAreaType105().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType106() {
		if (dataAreaType106EClass == null) {
			dataAreaType106EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(212);
		}
		return dataAreaType106EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType106_Process() {
        return (EReference)getDataAreaType106().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType106_OperationsDefinition() {
        return (EReference)getDataAreaType106().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType107() {
		if (dataAreaType107EClass == null) {
			dataAreaType107EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(213);
		}
		return dataAreaType107EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType107_Process() {
        return (EReference)getDataAreaType107().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType107_OperationsCapability() {
        return (EReference)getDataAreaType107().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType108() {
		if (dataAreaType108EClass == null) {
			dataAreaType108EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(214);
		}
		return dataAreaType108EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType108_Process() {
        return (EReference)getDataAreaType108().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType108_MaterialSubLot() {
        return (EReference)getDataAreaType108().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType109() {
		if (dataAreaType109EClass == null) {
			dataAreaType109EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(215);
		}
		return dataAreaType109EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType109_Process() {
        return (EReference)getDataAreaType109().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType109_MaterialInformation() {
        return (EReference)getDataAreaType109().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType110() {
		if (dataAreaType110EClass == null) {
			dataAreaType110EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(216);
		}
		return dataAreaType110EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType110_Process() {
        return (EReference)getDataAreaType110().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType110_MaterialClass() {
        return (EReference)getDataAreaType110().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType111() {
		if (dataAreaType111EClass == null) {
			dataAreaType111EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(217);
		}
		return dataAreaType111EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType111_Process() {
        return (EReference)getDataAreaType111().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType111_EquipmentClass() {
        return (EReference)getDataAreaType111().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType112() {
		if (dataAreaType112EClass == null) {
			dataAreaType112EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(218);
		}
		return dataAreaType112EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType112_Process() {
        return (EReference)getDataAreaType112().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType112_Equipment() {
        return (EReference)getDataAreaType112().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType113() {
		if (dataAreaType113EClass == null) {
			dataAreaType113EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(219);
		}
		return dataAreaType113EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType113_Get() {
        return (EReference)getDataAreaType113().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType113_ProcessSegmentInformation() {
        return (EReference)getDataAreaType113().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType114() {
		if (dataAreaType114EClass == null) {
			dataAreaType114EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(220);
		}
		return dataAreaType114EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType114_Get() {
        return (EReference)getDataAreaType114().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType114_PhysicalAssetInformation() {
        return (EReference)getDataAreaType114().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType115() {
		if (dataAreaType115EClass == null) {
			dataAreaType115EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(221);
		}
		return dataAreaType115EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType115_Get() {
        return (EReference)getDataAreaType115().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType115_PhysicalAssetCapabilityTestSpec() {
        return (EReference)getDataAreaType115().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType116() {
		if (dataAreaType116EClass == null) {
			dataAreaType116EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(222);
		}
		return dataAreaType116EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType116_Get() {
        return (EReference)getDataAreaType116().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType116_PersonnelInformation() {
        return (EReference)getDataAreaType116().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType117() {
		if (dataAreaType117EClass == null) {
			dataAreaType117EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(223);
		}
		return dataAreaType117EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType117_Get() {
        return (EReference)getDataAreaType117().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType117_Person() {
        return (EReference)getDataAreaType117().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType118() {
		if (dataAreaType118EClass == null) {
			dataAreaType118EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(224);
		}
		return dataAreaType118EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType118_Get() {
        return (EReference)getDataAreaType118().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType118_OperationsPerformance() {
        return (EReference)getDataAreaType118().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType119() {
		if (dataAreaType119EClass == null) {
			dataAreaType119EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(225);
		}
		return dataAreaType119EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType119_Get() {
        return (EReference)getDataAreaType119().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType119_OperationsDefinition() {
        return (EReference)getDataAreaType119().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType120() {
		if (dataAreaType120EClass == null) {
			dataAreaType120EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(226);
		}
		return dataAreaType120EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType120_Get() {
        return (EReference)getDataAreaType120().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType120_OperationsCapability() {
        return (EReference)getDataAreaType120().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType121() {
		if (dataAreaType121EClass == null) {
			dataAreaType121EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(227);
		}
		return dataAreaType121EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType121_Get() {
        return (EReference)getDataAreaType121().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType121_MaterialSubLot() {
        return (EReference)getDataAreaType121().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType122() {
		if (dataAreaType122EClass == null) {
			dataAreaType122EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(228);
		}
		return dataAreaType122EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType122_Change() {
        return (EReference)getDataAreaType122().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType122_PersonnelClass() {
        return (EReference)getDataAreaType122().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType123() {
		if (dataAreaType123EClass == null) {
			dataAreaType123EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(229);
		}
		return dataAreaType123EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType123_Change() {
        return (EReference)getDataAreaType123().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType123_Person() {
        return (EReference)getDataAreaType123().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType124() {
		if (dataAreaType124EClass == null) {
			dataAreaType124EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(230);
		}
		return dataAreaType124EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType124_Acknowledge() {
        return (EReference)getDataAreaType124().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType124_MaterialClass() {
        return (EReference)getDataAreaType124().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType125() {
		if (dataAreaType125EClass == null) {
			dataAreaType125EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(231);
		}
		return dataAreaType125EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType125_Change() {
        return (EReference)getDataAreaType125().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType125_OperationsSchedule() {
        return (EReference)getDataAreaType125().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType126() {
		if (dataAreaType126EClass == null) {
			dataAreaType126EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(232);
		}
		return dataAreaType126EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType126_Change() {
        return (EReference)getDataAreaType126().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType126_OperationsPerformance() {
        return (EReference)getDataAreaType126().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType127() {
		if (dataAreaType127EClass == null) {
			dataAreaType127EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(233);
		}
		return dataAreaType127EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType127_Acknowledge() {
        return (EReference)getDataAreaType127().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType127_EquipmentInformation() {
        return (EReference)getDataAreaType127().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType128() {
		if (dataAreaType128EClass == null) {
			dataAreaType128EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(234);
		}
		return dataAreaType128EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType128_Change() {
        return (EReference)getDataAreaType128().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType128_OperationsDefinitionInformation() {
        return (EReference)getDataAreaType128().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType129() {
		if (dataAreaType129EClass == null) {
			dataAreaType129EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(235);
		}
		return dataAreaType129EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType129_Change() {
        return (EReference)getDataAreaType129().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType129_OperationsDefinition() {
        return (EReference)getDataAreaType129().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType130() {
		if (dataAreaType130EClass == null) {
			dataAreaType130EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(236);
		}
		return dataAreaType130EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType130_Acknowledge() {
        return (EReference)getDataAreaType130().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType130_EquipmentCapabilityTestSpec() {
        return (EReference)getDataAreaType130().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType131() {
		if (dataAreaType131EClass == null) {
			dataAreaType131EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(237);
		}
		return dataAreaType131EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType131_Change() {
        return (EReference)getDataAreaType131().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType131_OperationsCapabilityInformation() {
        return (EReference)getDataAreaType131().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType132() {
		if (dataAreaType132EClass == null) {
			dataAreaType132EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(238);
		}
		return dataAreaType132EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType132_Change() {
        return (EReference)getDataAreaType132().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType132_OperationsCapability() {
        return (EReference)getDataAreaType132().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType133() {
		if (dataAreaType133EClass == null) {
			dataAreaType133EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(239);
		}
		return dataAreaType133EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType133_Acknowledge() {
        return (EReference)getDataAreaType133().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType133_PersonnelInformation() {
        return (EReference)getDataAreaType133().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType134() {
		if (dataAreaType134EClass == null) {
			dataAreaType134EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(240);
		}
		return dataAreaType134EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType134_Change() {
        return (EReference)getDataAreaType134().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType134_MaterialTestSpec() {
        return (EReference)getDataAreaType134().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType135() {
		if (dataAreaType135EClass == null) {
			dataAreaType135EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(241);
		}
		return dataAreaType135EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType135_Change() {
        return (EReference)getDataAreaType135().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType135_MaterialSubLot() {
        return (EReference)getDataAreaType135().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType136() {
		if (dataAreaType136EClass == null) {
			dataAreaType136EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(242);
		}
		return dataAreaType136EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType136_Acknowledge() {
        return (EReference)getDataAreaType136().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType136_PhysicalAsset() {
        return (EReference)getDataAreaType136().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType137() {
		if (dataAreaType137EClass == null) {
			dataAreaType137EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(243);
		}
		return dataAreaType137EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType137_Change() {
        return (EReference)getDataAreaType137().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType137_MaterialLot() {
        return (EReference)getDataAreaType137().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType138() {
		if (dataAreaType138EClass == null) {
			dataAreaType138EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(244);
		}
		return dataAreaType138EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType138_Change() {
        return (EReference)getDataAreaType138().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType138_MaterialInformation() {
        return (EReference)getDataAreaType138().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType139() {
		if (dataAreaType139EClass == null) {
			dataAreaType139EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(245);
		}
		return dataAreaType139EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType139_Acknowledge() {
        return (EReference)getDataAreaType139().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType139_MaterialInformation() {
        return (EReference)getDataAreaType139().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType140() {
		if (dataAreaType140EClass == null) {
			dataAreaType140EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(246);
		}
		return dataAreaType140EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType140_Change() {
        return (EReference)getDataAreaType140().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType140_MaterialDefinition() {
        return (EReference)getDataAreaType140().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType141() {
		if (dataAreaType141EClass == null) {
			dataAreaType141EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(247);
		}
		return dataAreaType141EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType141_Change() {
        return (EReference)getDataAreaType141().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType141_MaterialClass() {
        return (EReference)getDataAreaType141().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType142() {
		if (dataAreaType142EClass == null) {
			dataAreaType142EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(248);
		}
		return dataAreaType142EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType142_Acknowledge() {
        return (EReference)getDataAreaType142().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType142_PersonnelClass() {
        return (EReference)getDataAreaType142().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType143() {
		if (dataAreaType143EClass == null) {
			dataAreaType143EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(249);
		}
		return dataAreaType143EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType143_Change() {
        return (EReference)getDataAreaType143().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType143_EquipmentInformation() {
        return (EReference)getDataAreaType143().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType144() {
		if (dataAreaType144EClass == null) {
			dataAreaType144EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(250);
		}
		return dataAreaType144EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType144_Change() {
        return (EReference)getDataAreaType144().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType144_EquipmentClass() {
        return (EReference)getDataAreaType144().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType145() {
		if (dataAreaType145EClass == null) {
			dataAreaType145EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(251);
		}
		return dataAreaType145EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType145_Acknowledge() {
        return (EReference)getDataAreaType145().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType145_Person() {
        return (EReference)getDataAreaType145().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType146() {
		if (dataAreaType146EClass == null) {
			dataAreaType146EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(252);
		}
		return dataAreaType146EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType146_Change() {
        return (EReference)getDataAreaType146().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType146_EquipmentCapabilityTestSpec() {
        return (EReference)getDataAreaType146().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType147() {
		if (dataAreaType147EClass == null) {
			dataAreaType147EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(253);
		}
		return dataAreaType147EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType147_Change() {
        return (EReference)getDataAreaType147().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType147_Equipment() {
        return (EReference)getDataAreaType147().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType148() {
		if (dataAreaType148EClass == null) {
			dataAreaType148EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(254);
		}
		return dataAreaType148EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType148_Cancel() {
        return (EReference)getDataAreaType148().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType148_QualificationTestSpecification() {
        return (EReference)getDataAreaType148().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType149() {
		if (dataAreaType149EClass == null) {
			dataAreaType149EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(255);
		}
		return dataAreaType149EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType149_Acknowledge() {
        return (EReference)getDataAreaType149().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType149_OperationsCapability() {
        return (EReference)getDataAreaType149().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType150() {
		if (dataAreaType150EClass == null) {
			dataAreaType150EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(256);
		}
		return dataAreaType150EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType150_Cancel() {
        return (EReference)getDataAreaType150().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType150_ProcessSegmentInformation() {
        return (EReference)getDataAreaType150().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType151() {
		if (dataAreaType151EClass == null) {
			dataAreaType151EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(257);
		}
		return dataAreaType151EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType151_Cancel() {
        return (EReference)getDataAreaType151().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType151_ProcessSegment() {
        return (EReference)getDataAreaType151().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType152() {
		if (dataAreaType152EClass == null) {
			dataAreaType152EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(258);
		}
		return dataAreaType152EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType152_Acknowledge() {
        return (EReference)getDataAreaType152().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType152_MaterialDefinition() {
        return (EReference)getDataAreaType152().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType153() {
		if (dataAreaType153EClass == null) {
			dataAreaType153EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(259);
		}
		return dataAreaType153EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType153_Cancel() {
        return (EReference)getDataAreaType153().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType153_PhysicalAssetInformation() {
        return (EReference)getDataAreaType153().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType154() {
		if (dataAreaType154EClass == null) {
			dataAreaType154EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(260);
		}
		return dataAreaType154EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType154_Cancel() {
        return (EReference)getDataAreaType154().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType154_PhysicalAssetClass() {
        return (EReference)getDataAreaType154().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType155() {
		if (dataAreaType155EClass == null) {
			dataAreaType155EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(261);
		}
		return dataAreaType155EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType155_Acknowledge() {
        return (EReference)getDataAreaType155().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType155_OperationsDefinition() {
        return (EReference)getDataAreaType155().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType156() {
		if (dataAreaType156EClass == null) {
			dataAreaType156EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(262);
		}
		return dataAreaType156EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType156_Cancel() {
        return (EReference)getDataAreaType156().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType156_PhysicalAssetCapabilityTestSpec() {
        return (EReference)getDataAreaType156().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType157() {
		if (dataAreaType157EClass == null) {
			dataAreaType157EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(263);
		}
		return dataAreaType157EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType157_Cancel() {
        return (EReference)getDataAreaType157().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType157_PhysicalAsset() {
        return (EReference)getDataAreaType157().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType158() {
		if (dataAreaType158EClass == null) {
			dataAreaType158EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(264);
		}
		return dataAreaType158EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType158_Acknowledge() {
        return (EReference)getDataAreaType158().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType158_OperationsCapabilityInformation() {
        return (EReference)getDataAreaType158().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType159() {
		if (dataAreaType159EClass == null) {
			dataAreaType159EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(265);
		}
		return dataAreaType159EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType159_Cancel() {
        return (EReference)getDataAreaType159().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType159_PersonnelInformation() {
        return (EReference)getDataAreaType159().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType160() {
		if (dataAreaType160EClass == null) {
			dataAreaType160EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(266);
		}
		return dataAreaType160EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType160_Cancel() {
        return (EReference)getDataAreaType160().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType160_PersonnelClass() {
        return (EReference)getDataAreaType160().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType161() {
		if (dataAreaType161EClass == null) {
			dataAreaType161EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(267);
		}
		return dataAreaType161EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType161_Acknowledge() {
        return (EReference)getDataAreaType161().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType161_MaterialTestSpec() {
        return (EReference)getDataAreaType161().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType162() {
		if (dataAreaType162EClass == null) {
			dataAreaType162EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(268);
		}
		return dataAreaType162EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType162_Cancel() {
        return (EReference)getDataAreaType162().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType162_Person() {
        return (EReference)getDataAreaType162().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType163() {
		if (dataAreaType163EClass == null) {
			dataAreaType163EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(269);
		}
		return dataAreaType163EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType163_Cancel() {
        return (EReference)getDataAreaType163().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType163_OperationsSchedule() {
        return (EReference)getDataAreaType163().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType164() {
		if (dataAreaType164EClass == null) {
			dataAreaType164EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(270);
		}
		return dataAreaType164EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType164_Confirm() {
        return (EReference)getDataAreaType164().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType164_BOD() {
        return (EReference)getDataAreaType164().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType165() {
		if (dataAreaType165EClass == null) {
			dataAreaType165EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(271);
		}
		return dataAreaType165EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType165_Get() {
        return (EReference)getDataAreaType165().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType165_Equipment() {
        return (EReference)getDataAreaType165().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType166() {
		if (dataAreaType166EClass == null) {
			dataAreaType166EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(272);
		}
		return dataAreaType166EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType166_Get() {
        return (EReference)getDataAreaType166().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType166_EquipmentCapabilityTestSpec() {
        return (EReference)getDataAreaType166().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType167() {
		if (dataAreaType167EClass == null) {
			dataAreaType167EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(273);
		}
		return dataAreaType167EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType167_Get() {
        return (EReference)getDataAreaType167().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType167_EquipmentClass() {
        return (EReference)getDataAreaType167().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType168() {
		if (dataAreaType168EClass == null) {
			dataAreaType168EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(274);
		}
		return dataAreaType168EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType168_Get() {
        return (EReference)getDataAreaType168().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType168_EquipmentInformation() {
        return (EReference)getDataAreaType168().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType169() {
		if (dataAreaType169EClass == null) {
			dataAreaType169EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(275);
		}
		return dataAreaType169EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType169_Get() {
        return (EReference)getDataAreaType169().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType169_MaterialDefinition() {
        return (EReference)getDataAreaType169().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType170() {
		if (dataAreaType170EClass == null) {
			dataAreaType170EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(276);
		}
		return dataAreaType170EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType170_Get() {
        return (EReference)getDataAreaType170().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType170_MaterialInformation() {
        return (EReference)getDataAreaType170().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType171() {
		if (dataAreaType171EClass == null) {
			dataAreaType171EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(277);
		}
		return dataAreaType171EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType171_Get() {
        return (EReference)getDataAreaType171().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType171_MaterialLot() {
        return (EReference)getDataAreaType171().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType172() {
		if (dataAreaType172EClass == null) {
			dataAreaType172EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(278);
		}
		return dataAreaType172EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType172_Get() {
        return (EReference)getDataAreaType172().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType172_MaterialClass() {
        return (EReference)getDataAreaType172().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType173() {
		if (dataAreaType173EClass == null) {
			dataAreaType173EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(279);
		}
		return dataAreaType173EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType173_Acknowledge() {
        return (EReference)getDataAreaType173().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType173_MaterialSubLot() {
        return (EReference)getDataAreaType173().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType174() {
		if (dataAreaType174EClass == null) {
			dataAreaType174EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(280);
		}
		return dataAreaType174EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType174_Change() {
        return (EReference)getDataAreaType174().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType174_QualificationTestSpecification() {
        return (EReference)getDataAreaType174().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType175() {
		if (dataAreaType175EClass == null) {
			dataAreaType175EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(281);
		}
		return dataAreaType175EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType175_Change() {
        return (EReference)getDataAreaType175().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType175_ProcessSegmentInformation() {
        return (EReference)getDataAreaType175().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType176() {
		if (dataAreaType176EClass == null) {
			dataAreaType176EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(282);
		}
		return dataAreaType176EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType176_Change() {
        return (EReference)getDataAreaType176().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType176_ProcessSegment() {
        return (EReference)getDataAreaType176().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType177() {
		if (dataAreaType177EClass == null) {
			dataAreaType177EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(283);
		}
		return dataAreaType177EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType177_Change() {
        return (EReference)getDataAreaType177().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType177_PhysicalAssetInformation() {
        return (EReference)getDataAreaType177().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType178() {
		if (dataAreaType178EClass == null) {
			dataAreaType178EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(284);
		}
		return dataAreaType178EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType178_Cancel() {
        return (EReference)getDataAreaType178().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType178_OperationsPerformance() {
        return (EReference)getDataAreaType178().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType179() {
		if (dataAreaType179EClass == null) {
			dataAreaType179EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(285);
		}
		return dataAreaType179EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType179_Change() {
        return (EReference)getDataAreaType179().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType179_PhysicalAssetClass() {
        return (EReference)getDataAreaType179().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType180() {
		if (dataAreaType180EClass == null) {
			dataAreaType180EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(286);
		}
		return dataAreaType180EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType180_Change() {
        return (EReference)getDataAreaType180().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType180_PhysicalAssetCapabilityTestSpec() {
        return (EReference)getDataAreaType180().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType181() {
		if (dataAreaType181EClass == null) {
			dataAreaType181EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(287);
		}
		return dataAreaType181EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType181_Change() {
        return (EReference)getDataAreaType181().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType181_PhysicalAsset() {
        return (EReference)getDataAreaType181().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType182() {
		if (dataAreaType182EClass == null) {
			dataAreaType182EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(288);
		}
		return dataAreaType182EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType182_Change() {
        return (EReference)getDataAreaType182().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType182_PersonnelInformation() {
        return (EReference)getDataAreaType182().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType183() {
		if (dataAreaType183EClass == null) {
			dataAreaType183EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(289);
		}
		return dataAreaType183EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType183_Cancel() {
        return (EReference)getDataAreaType183().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType183_OperationsDefinitionInformation() {
        return (EReference)getDataAreaType183().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType184() {
		if (dataAreaType184EClass == null) {
			dataAreaType184EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(290);
		}
		return dataAreaType184EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType184_Acknowledge() {
        return (EReference)getDataAreaType184().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType184_OperationsPerformance() {
        return (EReference)getDataAreaType184().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType185() {
		if (dataAreaType185EClass == null) {
			dataAreaType185EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(291);
		}
		return dataAreaType185EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType185_Cancel() {
        return (EReference)getDataAreaType185().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType185_OperationsDefinition() {
        return (EReference)getDataAreaType185().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType186() {
		if (dataAreaType186EClass == null) {
			dataAreaType186EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(292);
		}
		return dataAreaType186EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType186_Cancel() {
        return (EReference)getDataAreaType186().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType186_OperationsCapabilityInformation() {
        return (EReference)getDataAreaType186().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType187() {
		if (dataAreaType187EClass == null) {
			dataAreaType187EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(293);
		}
		return dataAreaType187EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType187_Acknowledge() {
        return (EReference)getDataAreaType187().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType187_OperationsDefinitionInformation() {
        return (EReference)getDataAreaType187().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType188() {
		if (dataAreaType188EClass == null) {
			dataAreaType188EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(294);
		}
		return dataAreaType188EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType188_Cancel() {
        return (EReference)getDataAreaType188().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType188_OperationsCapability() {
        return (EReference)getDataAreaType188().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType189() {
		if (dataAreaType189EClass == null) {
			dataAreaType189EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(295);
		}
		return dataAreaType189EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType189_Cancel() {
        return (EReference)getDataAreaType189().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType189_MaterialTestSpec() {
        return (EReference)getDataAreaType189().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType190() {
		if (dataAreaType190EClass == null) {
			dataAreaType190EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(296);
		}
		return dataAreaType190EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType190_Acknowledge() {
        return (EReference)getDataAreaType190().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType190_EquipmentClass() {
        return (EReference)getDataAreaType190().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType191() {
		if (dataAreaType191EClass == null) {
			dataAreaType191EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(297);
		}
		return dataAreaType191EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType191_Cancel() {
        return (EReference)getDataAreaType191().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType191_MaterialSubLot() {
        return (EReference)getDataAreaType191().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType192() {
		if (dataAreaType192EClass == null) {
			dataAreaType192EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(298);
		}
		return dataAreaType192EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType192_Cancel() {
        return (EReference)getDataAreaType192().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType192_MaterialLot() {
        return (EReference)getDataAreaType192().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType193() {
		if (dataAreaType193EClass == null) {
			dataAreaType193EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(299);
		}
		return dataAreaType193EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType193_Acknowledge() {
        return (EReference)getDataAreaType193().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType193_MaterialLot() {
        return (EReference)getDataAreaType193().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType194() {
		if (dataAreaType194EClass == null) {
			dataAreaType194EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(300);
		}
		return dataAreaType194EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType194_Cancel() {
        return (EReference)getDataAreaType194().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType194_MaterialInformation() {
        return (EReference)getDataAreaType194().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType195() {
		if (dataAreaType195EClass == null) {
			dataAreaType195EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(301);
		}
		return dataAreaType195EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType195_Cancel() {
        return (EReference)getDataAreaType195().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType195_MaterialDefinition() {
        return (EReference)getDataAreaType195().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType196() {
		if (dataAreaType196EClass == null) {
			dataAreaType196EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(302);
		}
		return dataAreaType196EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType196_Acknowledge() {
        return (EReference)getDataAreaType196().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType196_OperationsSchedule() {
        return (EReference)getDataAreaType196().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType197() {
		if (dataAreaType197EClass == null) {
			dataAreaType197EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(303);
		}
		return dataAreaType197EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType197_Cancel() {
        return (EReference)getDataAreaType197().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType197_MaterialClass() {
        return (EReference)getDataAreaType197().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType198() {
		if (dataAreaType198EClass == null) {
			dataAreaType198EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(304);
		}
		return dataAreaType198EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType198_Cancel() {
        return (EReference)getDataAreaType198().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType198_EquipmentInformation() {
        return (EReference)getDataAreaType198().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType199() {
		if (dataAreaType199EClass == null) {
			dataAreaType199EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(305);
		}
		return dataAreaType199EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType199_Acknowledge() {
        return (EReference)getDataAreaType199().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType199_ProcessSegment() {
        return (EReference)getDataAreaType199().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType200() {
		if (dataAreaType200EClass == null) {
			dataAreaType200EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(306);
		}
		return dataAreaType200EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType200_Cancel() {
        return (EReference)getDataAreaType200().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType200_EquipmentClass() {
        return (EReference)getDataAreaType200().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType201() {
		if (dataAreaType201EClass == null) {
			dataAreaType201EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(307);
		}
		return dataAreaType201EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType201_Cancel() {
        return (EReference)getDataAreaType201().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType201_EquipmentCapabilityTestSpec() {
        return (EReference)getDataAreaType201().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType202() {
		if (dataAreaType202EClass == null) {
			dataAreaType202EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(308);
		}
		return dataAreaType202EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType202_Cancel() {
        return (EReference)getDataAreaType202().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType202_Equipment() {
        return (EReference)getDataAreaType202().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType203() {
		if (dataAreaType203EClass == null) {
			dataAreaType203EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(309);
		}
		return dataAreaType203EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType203_Acknowledge() {
        return (EReference)getDataAreaType203().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType203_PhysicalAssetInformation() {
        return (EReference)getDataAreaType203().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType204() {
		if (dataAreaType204EClass == null) {
			dataAreaType204EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(310);
		}
		return dataAreaType204EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType204_Acknowledge() {
        return (EReference)getDataAreaType204().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType204_QualificationTestSpecification() {
        return (EReference)getDataAreaType204().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType205() {
		if (dataAreaType205EClass == null) {
			dataAreaType205EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(311);
		}
		return dataAreaType205EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType205_Acknowledge() {
        return (EReference)getDataAreaType205().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType205_ProcessSegmentInformation() {
        return (EReference)getDataAreaType205().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType206() {
		if (dataAreaType206EClass == null) {
			dataAreaType206EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(312);
		}
		return dataAreaType206EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType206_Acknowledge() {
        return (EReference)getDataAreaType206().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType206_PhysicalAssetClass() {
        return (EReference)getDataAreaType206().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType207() {
		if (dataAreaType207EClass == null) {
			dataAreaType207EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(313);
		}
		return dataAreaType207EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType207_Acknowledge() {
        return (EReference)getDataAreaType207().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType207_PhysicalAssetCapabilityTestSpec() {
        return (EReference)getDataAreaType207().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataAreaType208() {
		if (dataAreaType208EClass == null) {
			dataAreaType208EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(314);
		}
		return dataAreaType208EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType208_Acknowledge() {
        return (EReference)getDataAreaType208().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataAreaType208_Equipment() {
        return (EReference)getDataAreaType208().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataType1Type() {
		if (dataType1TypeEClass == null) {
			dataType1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(315);
		}
		return dataType1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataTypeType() {
		if (dataTypeTypeEClass == null) {
			dataTypeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(318);
		}
		return dataTypeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataTypeType_OtherValue() {
        return (EAttribute)getDataTypeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDateTimeType() {
		if (dateTimeTypeEClass == null) {
			dateTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(319);
		}
		return dateTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDateTimeType_Value() {
        return (EAttribute)getDateTimeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDateTimeType_Format() {
        return (EAttribute)getDateTimeType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDependency1Type() {
		if (dependency1TypeEClass == null) {
			dependency1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(320);
		}
		return dependency1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDependencyType() {
		if (dependencyTypeEClass == null) {
			dependencyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(323);
		}
		return dependencyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDependencyType_OtherValue() {
        return (EAttribute)getDependencyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDescriptionType() {
		if (descriptionTypeEClass == null) {
			descriptionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(324);
		}
		return descriptionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDocumentRoot() {
		if (documentRootEClass == null) {
			documentRootEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(325);
		}
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_Mixed() {
        return (EAttribute)getDocumentRoot().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XMLNSPrefixMap() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XSISchemaLocation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeEquipment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeEquipmentCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeEquipmentClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeEquipmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeMaterialClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeMaterialDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeMaterialInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeMaterialLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeMaterialSubLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeMaterialTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeOperationsCapability() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeOperationsCapabilityInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeOperationsDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeOperationsDefinitionInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeOperationsPerformance() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeOperationsSchedule() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgePerson() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgePersonnelClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgePersonnelInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgePhysicalAsset() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgePhysicalAssetCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgePhysicalAssetClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgePhysicalAssetInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeProcessSegment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeProcessSegmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AcknowledgeQualificationTestSpecification() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelEquipment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(29);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelEquipmentCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(30);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelEquipmentClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(31);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelEquipmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(32);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelMaterialClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(33);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelMaterialDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(34);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelMaterialInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(35);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelMaterialLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(36);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelMaterialSubLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(37);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelMaterialTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(38);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelOperationsCapability() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(39);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelOperationsCapabilityInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(40);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelOperationsDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(41);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelOperationsDefinitionInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(42);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelOperationsPerformance() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(43);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelOperationsSchedule() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(44);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelPerson() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(45);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelPersonnelClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(46);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelPersonnelInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(47);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelPhysicalAsset() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(48);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelPhysicalAssetCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(49);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelPhysicalAssetClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(50);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelPhysicalAssetInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(51);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelProcessSegment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(52);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelProcessSegmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(53);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_CancelQualificationTestSpecification() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(54);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeEquipment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(55);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeEquipmentCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(56);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeEquipmentClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(57);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeEquipmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(58);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeMaterialClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(59);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeMaterialDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(60);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeMaterialInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(61);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeMaterialLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(62);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeMaterialSubLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(63);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeMaterialTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(64);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeOperationsCapability() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(65);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeOperationsCapabilityInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(66);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeOperationsDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(67);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeOperationsDefinitionInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(68);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeOperationsPerformance() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(69);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeOperationsSchedule() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(70);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangePerson() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(71);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangePersonnelClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(72);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangePersonnelInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(73);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangePhysicalAsset() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(74);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangePhysicalAssetCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(75);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangePhysicalAssetClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(76);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangePhysicalAssetInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(77);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeProcessSegment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(78);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeProcessSegmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(79);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ChangeQualificationTestSpecification() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(80);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ConfirmBOD() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(81);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Equipment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(82);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_EquipmentCapabilityTestSpecification() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(83);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_EquipmentClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(84);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_EquipmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(85);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetEquipment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(86);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetEquipmentCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(87);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetEquipmentClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(88);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetEquipmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(89);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetMaterialClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(90);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetMaterialDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(91);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetMaterialInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(92);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetMaterialLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(93);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetMaterialSubLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(94);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetMaterialTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(95);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetOperationsCapability() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(96);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetOperationsCapabilityInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(97);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetOperationsDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(98);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetOperationsDefinitionInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(99);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetOperationsPerformance() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(100);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetOperationsSchedule() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(101);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetPerson() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(102);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetPersonnelClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(103);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetPersonnelInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(104);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetPhysicalAsset() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(105);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetPhysicalAssetCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(106);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetPhysicalAssetClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(107);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetPhysicalAssetInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(108);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetProcessSegment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(109);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetProcessSegmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(110);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_GetQualificationTestSpecification() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(111);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_MaterialClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(112);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_MaterialDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(113);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_MaterialInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(114);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_MaterialLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(115);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_MaterialSubLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(116);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_MaterialTestSpecification() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(117);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_OperationsCapability() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(118);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_OperationsCapabilityInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(119);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_OperationsDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(120);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_OperationsDefinitionInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(121);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_OperationsPerformance() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(122);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_OperationsRequest() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(123);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_OperationsResponse() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(124);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_OperationsSchedule() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(125);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Person() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(126);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_PersonnelClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(127);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_PersonnelInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(128);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_PhysicalAsset() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(129);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_PhysicalAssetCapabilityTestSpecification() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(130);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_PhysicalAssetClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(131);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_PhysicalAssetInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(132);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessEquipment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(133);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessEquipmentCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(134);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessEquipmentClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(135);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessEquipmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(136);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessMaterialClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(137);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessMaterialDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(138);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessMaterialInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(139);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessMaterialLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(140);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessMaterialSubLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(141);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessMaterialTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(142);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessOperationsCapability() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(143);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessOperationsCapabilityInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(144);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessOperationsDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(145);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessOperationsDefinitionInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(146);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessOperationsPerformance() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(147);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessOperationsSchedule() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(148);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessPerson() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(149);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessPersonnelClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(150);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessPersonnelInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(151);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessPhysicalAsset() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(152);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessPhysicalAssetCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(153);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessPhysicalAssetClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(154);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessPhysicalAssetInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(155);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessProcessSegment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(156);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessProcessSegmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(157);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessQualificationTestSpecification() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(158);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessSegment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(159);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ProcessSegmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(160);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_QualificationTestSpecification() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(161);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondEquipment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(162);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondEquipmentCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(163);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondEquipmentClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(164);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondEquipmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(165);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondMaterialClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(166);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondMaterialDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(167);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondMaterialInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(168);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondMaterialLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(169);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondMaterialSubLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(170);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondMaterialTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(171);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondOperationsCapability() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(172);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondOperationsCapabilityInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(173);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondOperationsDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(174);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondOperationsDefinitionInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(175);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondOperationsPerformance() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(176);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondOperationsSchedule() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(177);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondPerson() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(178);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondPersonnelClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(179);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondPersonnelInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(180);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondPhysicalAsset() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(181);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondPhysicalAssetCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(182);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondPhysicalAssetClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(183);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondPhysicalAssetInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(184);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondProcessSegment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(185);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondProcessSegmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(186);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_RespondQualificationTestSpecification() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(187);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowEquipment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(188);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowEquipmentCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(189);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowEquipmentClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(190);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowEquipmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(191);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowMaterialClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(192);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowMaterialDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(193);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowMaterialInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(194);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowMaterialLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(195);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowMaterialSubLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(196);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowMaterialTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(197);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowOperationsCapability() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(198);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowOperationsCapabilityInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(199);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowOperationsDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(200);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowOperationsDefinitionInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(201);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowOperationsPerformance() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(202);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowOperationsSchedule() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(203);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowPerson() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(204);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowPersonnelClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(205);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowPersonnelInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(206);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowPhysicalAsset() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(207);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowPhysicalAssetCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(208);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowPhysicalAssetClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(209);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowPhysicalAssetInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(210);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowProcessSegment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(211);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowProcessSegmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(212);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ShowQualificationTestSpecification() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(213);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncEquipment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(214);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncEquipmentCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(215);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncEquipmentClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(216);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncEquipmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(217);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncMaterialClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(218);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncMaterialDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(219);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncMaterialInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(220);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncMaterialLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(221);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncMaterialSubLot() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(222);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncMaterialTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(223);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncOperationsCapability() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(224);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncOperationsCapabilityInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(225);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncOperationsDefinition() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(226);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncOperationsDefinitionInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(227);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncOperationsPerformance() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(228);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncOperationsSchedule() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(229);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncPerson() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(230);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncPersonnelClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(231);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncPersonnelInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(232);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncPhysicalAsset() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(233);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncPhysicalAssetCapabilityTestSpec() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(234);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncPhysicalAssetClass() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(235);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncPhysicalAssetInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(236);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncProcessSegment() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(237);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncProcessSegmentInformation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(238);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SyncQualificationTestSpecification() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(239);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarliestStartTimeType() {
		if (earliestStartTimeTypeEClass == null) {
			earliestStartTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(327);
		}
		return earliestStartTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEndTimeType() {
		if (endTimeTypeEClass == null) {
			endTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(328);
		}
		return endTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentAssetMappingType() {
		if (equipmentAssetMappingTypeEClass == null) {
			equipmentAssetMappingTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(329);
		}
		return equipmentAssetMappingTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentAssetMappingType_EquipmentID() {
        return (EReference)getEquipmentAssetMappingType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentAssetMappingType_PhysicalAssetID() {
        return (EReference)getEquipmentAssetMappingType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentAssetMappingType_StartTime() {
        return (EReference)getEquipmentAssetMappingType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentAssetMappingType_EndTime() {
        return (EReference)getEquipmentAssetMappingType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentCapabilityTestSpecificationIDType() {
		if (equipmentCapabilityTestSpecificationIDTypeEClass == null) {
			equipmentCapabilityTestSpecificationIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(330);
		}
		return equipmentCapabilityTestSpecificationIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentCapabilityTestSpecificationType() {
		if (equipmentCapabilityTestSpecificationTypeEClass == null) {
			equipmentCapabilityTestSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(331);
		}
		return equipmentCapabilityTestSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapabilityTestSpecificationType_Name() {
        return (EReference)getEquipmentCapabilityTestSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapabilityTestSpecificationType_Description() {
        return (EReference)getEquipmentCapabilityTestSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapabilityTestSpecificationType_Version() {
        return (EReference)getEquipmentCapabilityTestSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapabilityTestSpecificationType_Location() {
        return (EReference)getEquipmentCapabilityTestSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapabilityTestSpecificationType_HierarchyScope() {
        return (EReference)getEquipmentCapabilityTestSpecificationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapabilityTestSpecificationType_TestedEquipmentProperty() {
        return (EReference)getEquipmentCapabilityTestSpecificationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentCapabilityTestSpecificationType_TestedEquipmentClassProperty() {
        return (EReference)getEquipmentCapabilityTestSpecificationType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentClassIDType() {
		if (equipmentClassIDTypeEClass == null) {
			equipmentClassIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(332);
		}
		return equipmentClassIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentClassPropertyType() {
		if (equipmentClassPropertyTypeEClass == null) {
			equipmentClassPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(333);
		}
		return equipmentClassPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassPropertyType_ID() {
        return (EReference)getEquipmentClassPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassPropertyType_Description() {
        return (EReference)getEquipmentClassPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassPropertyType_Value() {
        return (EReference)getEquipmentClassPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassPropertyType_EquipmentClassProperty() {
        return (EReference)getEquipmentClassPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassPropertyType_EquipmentCapabilityTestSpecificationID() {
        return (EReference)getEquipmentClassPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentClassType() {
		if (equipmentClassTypeEClass == null) {
			equipmentClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(334);
		}
		return equipmentClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassType_ID() {
        return (EReference)getEquipmentClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassType_Description() {
        return (EReference)getEquipmentClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassType_Location() {
        return (EReference)getEquipmentClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassType_HierarchyScope() {
        return (EReference)getEquipmentClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassType_EquipmentLevel() {
        return (EReference)getEquipmentClassType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassType_EquipmentClassProperty() {
        return (EReference)getEquipmentClassType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassType_EquipmentID() {
        return (EReference)getEquipmentClassType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentClassType_EquipmentCapabilityTestSpecificationID() {
        return (EReference)getEquipmentClassType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentElementLevel1Type() {
		if (equipmentElementLevel1TypeEClass == null) {
			equipmentElementLevel1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(335);
		}
		return equipmentElementLevel1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentElementLevelType() {
		if (equipmentElementLevelTypeEClass == null) {
			equipmentElementLevelTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(338);
		}
		return equipmentElementLevelTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEquipmentElementLevelType_OtherValue() {
        return (EAttribute)getEquipmentElementLevelType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentIDType() {
		if (equipmentIDTypeEClass == null) {
			equipmentIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(339);
		}
		return equipmentIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentInformationType() {
		if (equipmentInformationTypeEClass == null) {
			equipmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(340);
		}
		return equipmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentInformationType_ID() {
        return (EReference)getEquipmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentInformationType_Description() {
        return (EReference)getEquipmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentInformationType_Location() {
        return (EReference)getEquipmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentInformationType_HierarchyScope() {
        return (EReference)getEquipmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentInformationType_PublishedDate() {
        return (EReference)getEquipmentInformationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentInformationType_Equipment() {
        return (EReference)getEquipmentInformationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentInformationType_EquipmentClass() {
        return (EReference)getEquipmentInformationType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentInformationType_EquipmentCapabilityTestSpecification() {
        return (EReference)getEquipmentInformationType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentPropertyType() {
		if (equipmentPropertyTypeEClass == null) {
			equipmentPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(341);
		}
		return equipmentPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentPropertyType_ID() {
        return (EReference)getEquipmentPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentPropertyType_Description() {
        return (EReference)getEquipmentPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentPropertyType_Value() {
        return (EReference)getEquipmentPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentPropertyType_EquipmentProperty() {
        return (EReference)getEquipmentPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentPropertyType_EquipmentCapabilityTestSpecificationID() {
        return (EReference)getEquipmentPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentPropertyType_TestResult() {
        return (EReference)getEquipmentPropertyType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentSegmentSpecificationPropertyType() {
		if (equipmentSegmentSpecificationPropertyTypeEClass == null) {
			equipmentSegmentSpecificationPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(342);
		}
		return equipmentSegmentSpecificationPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecificationPropertyType_ID() {
        return (EReference)getEquipmentSegmentSpecificationPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecificationPropertyType_Description() {
        return (EReference)getEquipmentSegmentSpecificationPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecificationPropertyType_Value() {
        return (EReference)getEquipmentSegmentSpecificationPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecificationPropertyType_Quantity() {
        return (EReference)getEquipmentSegmentSpecificationPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentSegmentSpecificationType() {
		if (equipmentSegmentSpecificationTypeEClass == null) {
			equipmentSegmentSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(343);
		}
		return equipmentSegmentSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecificationType_EquipmentClassID() {
        return (EReference)getEquipmentSegmentSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecificationType_EquipmentID() {
        return (EReference)getEquipmentSegmentSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecificationType_Description() {
        return (EReference)getEquipmentSegmentSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecificationType_EquipmentUse() {
        return (EReference)getEquipmentSegmentSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecificationType_Quantity() {
        return (EReference)getEquipmentSegmentSpecificationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentSegmentSpecificationType_EquipmentSegmentSpecificationProperty() {
        return (EReference)getEquipmentSegmentSpecificationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentType() {
		if (equipmentTypeEClass == null) {
			equipmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(344);
		}
		return equipmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentType_ID() {
        return (EReference)getEquipmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentType_Description() {
        return (EReference)getEquipmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentType_Location() {
        return (EReference)getEquipmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentType_HierarchyScope() {
        return (EReference)getEquipmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentType_EquipmentLevel() {
        return (EReference)getEquipmentType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentType_EquipmentAssetMapping() {
        return (EReference)getEquipmentType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentType_EquipmentProperty() {
        return (EReference)getEquipmentType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentType_Equipment() {
        return (EReference)getEquipmentType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentType_EquipmentClassID() {
        return (EReference)getEquipmentType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEquipmentType_EquipmentCapabilityTestSpecificationID() {
        return (EReference)getEquipmentType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquipmentUseType() {
		if (equipmentUseTypeEClass == null) {
			equipmentUseTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(345);
		}
		return equipmentUseTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpirationTimeType() {
		if (expirationTimeTypeEClass == null) {
			expirationTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(346);
		}
		return expirationTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetEquipmentCapabilityTestSpecType() {
		if (getEquipmentCapabilityTestSpecTypeEClass == null) {
			getEquipmentCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(347);
		}
		return getEquipmentCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetEquipmentCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getGetEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetEquipmentCapabilityTestSpecType_DataArea() {
        return (EReference)getGetEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetEquipmentCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getGetEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetEquipmentCapabilityTestSpecType_VersionID() {
        return (EAttribute)getGetEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetEquipmentClassType() {
		if (getEquipmentClassTypeEClass == null) {
			getEquipmentClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(348);
		}
		return getEquipmentClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetEquipmentClassType_ApplicationArea() {
        return (EReference)getGetEquipmentClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetEquipmentClassType_DataArea() {
        return (EReference)getGetEquipmentClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetEquipmentClassType_ReleaseID() {
        return (EAttribute)getGetEquipmentClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetEquipmentClassType_VersionID() {
        return (EAttribute)getGetEquipmentClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetEquipmentInformationType() {
		if (getEquipmentInformationTypeEClass == null) {
			getEquipmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(349);
		}
		return getEquipmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetEquipmentInformationType_ApplicationArea() {
        return (EReference)getGetEquipmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetEquipmentInformationType_DataArea() {
        return (EReference)getGetEquipmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetEquipmentInformationType_ReleaseID() {
        return (EAttribute)getGetEquipmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetEquipmentInformationType_VersionID() {
        return (EAttribute)getGetEquipmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetEquipmentType() {
		if (getEquipmentTypeEClass == null) {
			getEquipmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(350);
		}
		return getEquipmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetEquipmentType_ApplicationArea() {
        return (EReference)getGetEquipmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetEquipmentType_DataArea() {
        return (EReference)getGetEquipmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetEquipmentType_ReleaseID() {
        return (EAttribute)getGetEquipmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetEquipmentType_VersionID() {
        return (EAttribute)getGetEquipmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetMaterialClassType() {
		if (getMaterialClassTypeEClass == null) {
			getMaterialClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(351);
		}
		return getMaterialClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetMaterialClassType_ApplicationArea() {
        return (EReference)getGetMaterialClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetMaterialClassType_DataArea() {
        return (EReference)getGetMaterialClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetMaterialClassType_ReleaseID() {
        return (EAttribute)getGetMaterialClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetMaterialClassType_VersionID() {
        return (EAttribute)getGetMaterialClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetMaterialDefinitionType() {
		if (getMaterialDefinitionTypeEClass == null) {
			getMaterialDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(352);
		}
		return getMaterialDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetMaterialDefinitionType_ApplicationArea() {
        return (EReference)getGetMaterialDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetMaterialDefinitionType_DataArea() {
        return (EReference)getGetMaterialDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetMaterialDefinitionType_ReleaseID() {
        return (EAttribute)getGetMaterialDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetMaterialDefinitionType_VersionID() {
        return (EAttribute)getGetMaterialDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetMaterialInformationType() {
		if (getMaterialInformationTypeEClass == null) {
			getMaterialInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(353);
		}
		return getMaterialInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetMaterialInformationType_ApplicationArea() {
        return (EReference)getGetMaterialInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetMaterialInformationType_DataArea() {
        return (EReference)getGetMaterialInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetMaterialInformationType_ReleaseID() {
        return (EAttribute)getGetMaterialInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetMaterialInformationType_VersionID() {
        return (EAttribute)getGetMaterialInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetMaterialLotType() {
		if (getMaterialLotTypeEClass == null) {
			getMaterialLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(354);
		}
		return getMaterialLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetMaterialLotType_ApplicationArea() {
        return (EReference)getGetMaterialLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetMaterialLotType_DataArea() {
        return (EReference)getGetMaterialLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetMaterialLotType_ReleaseID() {
        return (EAttribute)getGetMaterialLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetMaterialLotType_VersionID() {
        return (EAttribute)getGetMaterialLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetMaterialSubLotType() {
		if (getMaterialSubLotTypeEClass == null) {
			getMaterialSubLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(355);
		}
		return getMaterialSubLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetMaterialSubLotType_ApplicationArea() {
        return (EReference)getGetMaterialSubLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetMaterialSubLotType_DataArea() {
        return (EReference)getGetMaterialSubLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetMaterialSubLotType_ReleaseID() {
        return (EAttribute)getGetMaterialSubLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetMaterialSubLotType_VersionID() {
        return (EAttribute)getGetMaterialSubLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetMaterialTestSpecType() {
		if (getMaterialTestSpecTypeEClass == null) {
			getMaterialTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(356);
		}
		return getMaterialTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetMaterialTestSpecType_ApplicationArea() {
        return (EReference)getGetMaterialTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetMaterialTestSpecType_DataArea() {
        return (EReference)getGetMaterialTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetMaterialTestSpecType_ReleaseID() {
        return (EAttribute)getGetMaterialTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetMaterialTestSpecType_VersionID() {
        return (EAttribute)getGetMaterialTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetOperationsCapabilityInformationType() {
		if (getOperationsCapabilityInformationTypeEClass == null) {
			getOperationsCapabilityInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(357);
		}
		return getOperationsCapabilityInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetOperationsCapabilityInformationType_ApplicationArea() {
        return (EReference)getGetOperationsCapabilityInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetOperationsCapabilityInformationType_DataArea() {
        return (EReference)getGetOperationsCapabilityInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetOperationsCapabilityInformationType_ReleaseID() {
        return (EAttribute)getGetOperationsCapabilityInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetOperationsCapabilityInformationType_VersionID() {
        return (EAttribute)getGetOperationsCapabilityInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetOperationsCapabilityType() {
		if (getOperationsCapabilityTypeEClass == null) {
			getOperationsCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(358);
		}
		return getOperationsCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetOperationsCapabilityType_ApplicationArea() {
        return (EReference)getGetOperationsCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetOperationsCapabilityType_DataArea() {
        return (EReference)getGetOperationsCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetOperationsCapabilityType_ReleaseID() {
        return (EAttribute)getGetOperationsCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetOperationsCapabilityType_VersionID() {
        return (EAttribute)getGetOperationsCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetOperationsDefinitionInformationType() {
		if (getOperationsDefinitionInformationTypeEClass == null) {
			getOperationsDefinitionInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(359);
		}
		return getOperationsDefinitionInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetOperationsDefinitionInformationType_ApplicationArea() {
        return (EReference)getGetOperationsDefinitionInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetOperationsDefinitionInformationType_DataArea() {
        return (EReference)getGetOperationsDefinitionInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetOperationsDefinitionInformationType_ReleaseID() {
        return (EAttribute)getGetOperationsDefinitionInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetOperationsDefinitionInformationType_VersionID() {
        return (EAttribute)getGetOperationsDefinitionInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetOperationsDefinitionType() {
		if (getOperationsDefinitionTypeEClass == null) {
			getOperationsDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(360);
		}
		return getOperationsDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetOperationsDefinitionType_ApplicationArea() {
        return (EReference)getGetOperationsDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetOperationsDefinitionType_DataArea() {
        return (EReference)getGetOperationsDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetOperationsDefinitionType_ReleaseID() {
        return (EAttribute)getGetOperationsDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetOperationsDefinitionType_VersionID() {
        return (EAttribute)getGetOperationsDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetOperationsPerformanceType() {
		if (getOperationsPerformanceTypeEClass == null) {
			getOperationsPerformanceTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(361);
		}
		return getOperationsPerformanceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetOperationsPerformanceType_ApplicationArea() {
        return (EReference)getGetOperationsPerformanceType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetOperationsPerformanceType_DataArea() {
        return (EReference)getGetOperationsPerformanceType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetOperationsPerformanceType_ReleaseID() {
        return (EAttribute)getGetOperationsPerformanceType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetOperationsPerformanceType_VersionID() {
        return (EAttribute)getGetOperationsPerformanceType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetOperationsScheduleType() {
		if (getOperationsScheduleTypeEClass == null) {
			getOperationsScheduleTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(362);
		}
		return getOperationsScheduleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetOperationsScheduleType_ApplicationArea() {
        return (EReference)getGetOperationsScheduleType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetOperationsScheduleType_DataArea() {
        return (EReference)getGetOperationsScheduleType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetOperationsScheduleType_ReleaseID() {
        return (EAttribute)getGetOperationsScheduleType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetOperationsScheduleType_VersionID() {
        return (EAttribute)getGetOperationsScheduleType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetPersonnelClassType() {
		if (getPersonnelClassTypeEClass == null) {
			getPersonnelClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(363);
		}
		return getPersonnelClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPersonnelClassType_ApplicationArea() {
        return (EReference)getGetPersonnelClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPersonnelClassType_DataArea() {
        return (EReference)getGetPersonnelClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPersonnelClassType_ReleaseID() {
        return (EAttribute)getGetPersonnelClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPersonnelClassType_VersionID() {
        return (EAttribute)getGetPersonnelClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetPersonnelInformationType() {
		if (getPersonnelInformationTypeEClass == null) {
			getPersonnelInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(364);
		}
		return getPersonnelInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPersonnelInformationType_ApplicationArea() {
        return (EReference)getGetPersonnelInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPersonnelInformationType_DataArea() {
        return (EReference)getGetPersonnelInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPersonnelInformationType_ReleaseID() {
        return (EAttribute)getGetPersonnelInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPersonnelInformationType_VersionID() {
        return (EAttribute)getGetPersonnelInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetPersonType() {
		if (getPersonTypeEClass == null) {
			getPersonTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(365);
		}
		return getPersonTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPersonType_ApplicationArea() {
        return (EReference)getGetPersonType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPersonType_DataArea() {
        return (EReference)getGetPersonType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPersonType_ReleaseID() {
        return (EAttribute)getGetPersonType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPersonType_VersionID() {
        return (EAttribute)getGetPersonType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetPhysicalAssetCapabilityTestSpecType() {
		if (getPhysicalAssetCapabilityTestSpecTypeEClass == null) {
			getPhysicalAssetCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(366);
		}
		return getPhysicalAssetCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPhysicalAssetCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getGetPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPhysicalAssetCapabilityTestSpecType_DataArea() {
        return (EReference)getGetPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPhysicalAssetCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getGetPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPhysicalAssetCapabilityTestSpecType_VersionID() {
        return (EAttribute)getGetPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetPhysicalAssetClassType() {
		if (getPhysicalAssetClassTypeEClass == null) {
			getPhysicalAssetClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(367);
		}
		return getPhysicalAssetClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPhysicalAssetClassType_ApplicationArea() {
        return (EReference)getGetPhysicalAssetClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPhysicalAssetClassType_DataArea() {
        return (EReference)getGetPhysicalAssetClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPhysicalAssetClassType_ReleaseID() {
        return (EAttribute)getGetPhysicalAssetClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPhysicalAssetClassType_VersionID() {
        return (EAttribute)getGetPhysicalAssetClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetPhysicalAssetInformationType() {
		if (getPhysicalAssetInformationTypeEClass == null) {
			getPhysicalAssetInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(368);
		}
		return getPhysicalAssetInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPhysicalAssetInformationType_ApplicationArea() {
        return (EReference)getGetPhysicalAssetInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPhysicalAssetInformationType_DataArea() {
        return (EReference)getGetPhysicalAssetInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPhysicalAssetInformationType_ReleaseID() {
        return (EAttribute)getGetPhysicalAssetInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPhysicalAssetInformationType_VersionID() {
        return (EAttribute)getGetPhysicalAssetInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetPhysicalAssetType() {
		if (getPhysicalAssetTypeEClass == null) {
			getPhysicalAssetTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(369);
		}
		return getPhysicalAssetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPhysicalAssetType_ApplicationArea() {
        return (EReference)getGetPhysicalAssetType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetPhysicalAssetType_DataArea() {
        return (EReference)getGetPhysicalAssetType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPhysicalAssetType_ReleaseID() {
        return (EAttribute)getGetPhysicalAssetType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetPhysicalAssetType_VersionID() {
        return (EAttribute)getGetPhysicalAssetType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetProcessSegmentInformationType() {
		if (getProcessSegmentInformationTypeEClass == null) {
			getProcessSegmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(370);
		}
		return getProcessSegmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetProcessSegmentInformationType_ApplicationArea() {
        return (EReference)getGetProcessSegmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetProcessSegmentInformationType_DataArea() {
        return (EReference)getGetProcessSegmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetProcessSegmentInformationType_ReleaseID() {
        return (EAttribute)getGetProcessSegmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetProcessSegmentInformationType_VersionID() {
        return (EAttribute)getGetProcessSegmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetProcessSegmentType() {
		if (getProcessSegmentTypeEClass == null) {
			getProcessSegmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(371);
		}
		return getProcessSegmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetProcessSegmentType_ApplicationArea() {
        return (EReference)getGetProcessSegmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetProcessSegmentType_DataArea() {
        return (EReference)getGetProcessSegmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetProcessSegmentType_ReleaseID() {
        return (EAttribute)getGetProcessSegmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetProcessSegmentType_VersionID() {
        return (EAttribute)getGetProcessSegmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetQualificationTestSpecificationType() {
		if (getQualificationTestSpecificationTypeEClass == null) {
			getQualificationTestSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(372);
		}
		return getQualificationTestSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetQualificationTestSpecificationType_ApplicationArea() {
        return (EReference)getGetQualificationTestSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGetQualificationTestSpecificationType_DataArea() {
        return (EReference)getGetQualificationTestSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetQualificationTestSpecificationType_ReleaseID() {
        return (EAttribute)getGetQualificationTestSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGetQualificationTestSpecificationType_VersionID() {
        return (EAttribute)getGetQualificationTestSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHierarchyScopeType() {
		if (hierarchyScopeTypeEClass == null) {
			hierarchyScopeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(373);
		}
		return hierarchyScopeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHierarchyScopeType_EquipmentID() {
        return (EReference)getHierarchyScopeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHierarchyScopeType_EquipmentElementLevel() {
        return (EReference)getHierarchyScopeType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHierarchyScopeType_HierarchyScope() {
        return (EReference)getHierarchyScopeType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIdentifierType() {
		if (identifierTypeEClass == null) {
			identifierTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(374);
		}
		return identifierTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifierType_Value() {
        return (EAttribute)getIdentifierType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifierType_SchemeAgencyID() {
        return (EAttribute)getIdentifierType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifierType_SchemeAgencyName() {
        return (EAttribute)getIdentifierType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifierType_SchemeDataURI() {
        return (EAttribute)getIdentifierType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifierType_SchemeID() {
        return (EAttribute)getIdentifierType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifierType_SchemeName() {
        return (EAttribute)getIdentifierType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifierType_SchemeURI() {
        return (EAttribute)getIdentifierType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifierType_SchemeVersionID() {
        return (EAttribute)getIdentifierType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJobOrderCommand1Type() {
		if (jobOrderCommand1TypeEClass == null) {
			jobOrderCommand1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(377);
		}
		return jobOrderCommand1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJobOrderCommandRuleType() {
		if (jobOrderCommandRuleTypeEClass == null) {
			jobOrderCommandRuleTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(380);
		}
		return jobOrderCommandRuleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJobOrderCommandType() {
		if (jobOrderCommandTypeEClass == null) {
			jobOrderCommandTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(381);
		}
		return jobOrderCommandTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJobOrderCommandType_OtherValue() {
        return (EAttribute)getJobOrderCommandType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJobOrderDispatchStatusType() {
		if (jobOrderDispatchStatusTypeEClass == null) {
			jobOrderDispatchStatusTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(382);
		}
		return jobOrderDispatchStatusTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLatestEndTimeType() {
		if (latestEndTimeTypeEClass == null) {
			latestEndTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(383);
		}
		return latestEndTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLocationType() {
		if (locationTypeEClass == null) {
			locationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(384);
		}
		return locationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocationType_EquipmentID() {
        return (EReference)getLocationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocationType_EquipmentElementLevel() {
        return (EReference)getLocationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocationType_Location() {
        return (EReference)getLocationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getManufacturingBillIDType() {
		if (manufacturingBillIDTypeEClass == null) {
			manufacturingBillIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(385);
		}
		return manufacturingBillIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialActualIDType() {
		if (materialActualIDTypeEClass == null) {
			materialActualIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(386);
		}
		return materialActualIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialCapabilityIDType() {
		if (materialCapabilityIDTypeEClass == null) {
			materialCapabilityIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(387);
		}
		return materialCapabilityIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialClassIDType() {
		if (materialClassIDTypeEClass == null) {
			materialClassIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(388);
		}
		return materialClassIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialClassPropertyType() {
		if (materialClassPropertyTypeEClass == null) {
			materialClassPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(389);
		}
		return materialClassPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassPropertyType_ID() {
        return (EReference)getMaterialClassPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassPropertyType_Description() {
        return (EReference)getMaterialClassPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassPropertyType_Value() {
        return (EReference)getMaterialClassPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassPropertyType_MaterialClassProperty() {
        return (EReference)getMaterialClassPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassPropertyType_MaterialTestSpecificationID() {
        return (EReference)getMaterialClassPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialClassType() {
		if (materialClassTypeEClass == null) {
			materialClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(390);
		}
		return materialClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassType_ID() {
        return (EReference)getMaterialClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassType_Description() {
        return (EReference)getMaterialClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassType_Location() {
        return (EReference)getMaterialClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassType_HierarchyScope() {
        return (EReference)getMaterialClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassType_MaterialClassProperty() {
        return (EReference)getMaterialClassType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassType_MaterialDefinitionID() {
        return (EReference)getMaterialClassType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassType_MaterialTestSpecificationID() {
        return (EReference)getMaterialClassType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassType_AssemblyClassID() {
        return (EReference)getMaterialClassType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassType_AssemblyType() {
        return (EReference)getMaterialClassType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialClassType_AssemblyRelationship() {
        return (EReference)getMaterialClassType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialDefinitionIDType() {
		if (materialDefinitionIDTypeEClass == null) {
			materialDefinitionIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(391);
		}
		return materialDefinitionIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialDefinitionPropertyType() {
		if (materialDefinitionPropertyTypeEClass == null) {
			materialDefinitionPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(392);
		}
		return materialDefinitionPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionPropertyType_ID() {
        return (EReference)getMaterialDefinitionPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionPropertyType_Description() {
        return (EReference)getMaterialDefinitionPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionPropertyType_Value() {
        return (EReference)getMaterialDefinitionPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionPropertyType_MaterialDefinitionProperty() {
        return (EReference)getMaterialDefinitionPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionPropertyType_MaterialTestSpecificationID() {
        return (EReference)getMaterialDefinitionPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialDefinitionType() {
		if (materialDefinitionTypeEClass == null) {
			materialDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(393);
		}
		return materialDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionType_ID() {
        return (EReference)getMaterialDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionType_Description() {
        return (EReference)getMaterialDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionType_Location() {
        return (EReference)getMaterialDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionType_HierarchyScope() {
        return (EReference)getMaterialDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionType_MaterialDefinitionProperty() {
        return (EReference)getMaterialDefinitionType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionType_MaterialClassID() {
        return (EReference)getMaterialDefinitionType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionType_MaterialLotID() {
        return (EReference)getMaterialDefinitionType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionType_MaterialTestSpecificationID() {
        return (EReference)getMaterialDefinitionType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionType_AssemblylDefinitionID() {
        return (EReference)getMaterialDefinitionType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionType_AssemblyType() {
        return (EReference)getMaterialDefinitionType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialDefinitionType_AssemblyRelationship() {
        return (EReference)getMaterialDefinitionType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialInformationType() {
		if (materialInformationTypeEClass == null) {
			materialInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(394);
		}
		return materialInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialInformationType_ID() {
        return (EReference)getMaterialInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialInformationType_Description() {
        return (EReference)getMaterialInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialInformationType_Location() {
        return (EReference)getMaterialInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialInformationType_HierarchyScope() {
        return (EReference)getMaterialInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialInformationType_PublishedDate() {
        return (EReference)getMaterialInformationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialInformationType_MaterialClass() {
        return (EReference)getMaterialInformationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialInformationType_MaterialDefinition() {
        return (EReference)getMaterialInformationType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialInformationType_MaterialLot() {
        return (EReference)getMaterialInformationType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialInformationType_MaterialSubLot() {
        return (EReference)getMaterialInformationType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialInformationType_MaterialTestSpecification() {
        return (EReference)getMaterialInformationType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialLotIDType() {
		if (materialLotIDTypeEClass == null) {
			materialLotIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(395);
		}
		return materialLotIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialLotPropertyType() {
		if (materialLotPropertyTypeEClass == null) {
			materialLotPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(396);
		}
		return materialLotPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotPropertyType_ID() {
        return (EReference)getMaterialLotPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotPropertyType_Description() {
        return (EReference)getMaterialLotPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotPropertyType_Value() {
        return (EReference)getMaterialLotPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotPropertyType_MaterialLotProperty() {
        return (EReference)getMaterialLotPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotPropertyType_MaterialTestSpecificationID() {
        return (EReference)getMaterialLotPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotPropertyType_TestResult() {
        return (EReference)getMaterialLotPropertyType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialLotType() {
		if (materialLotTypeEClass == null) {
			materialLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(397);
		}
		return materialLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_ID() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_Description() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_Location() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_HierarchyScope() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_MaterialDefinitionID() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_Status() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_MaterialLotProperty() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_MaterialSubLot() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_StorageLocation() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_Quantity() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_MaterialTestSpecificationID() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_AssemblyLotID() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_AssemblySubLotID() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_AssemblyType() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialLotType_AssemblyRelationship() {
        return (EReference)getMaterialLotType().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialRequirementIDType() {
		if (materialRequirementIDTypeEClass == null) {
			materialRequirementIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(398);
		}
		return materialRequirementIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialSegmentSpecificationPropertyType() {
		if (materialSegmentSpecificationPropertyTypeEClass == null) {
			materialSegmentSpecificationPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(399);
		}
		return materialSegmentSpecificationPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationPropertyType_ID() {
        return (EReference)getMaterialSegmentSpecificationPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationPropertyType_Description() {
        return (EReference)getMaterialSegmentSpecificationPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationPropertyType_Value() {
        return (EReference)getMaterialSegmentSpecificationPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationPropertyType_Quantity() {
        return (EReference)getMaterialSegmentSpecificationPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialSegmentSpecificationType() {
		if (materialSegmentSpecificationTypeEClass == null) {
			materialSegmentSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(400);
		}
		return materialSegmentSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationType_ID() {
        return (EReference)getMaterialSegmentSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationType_MaterialClassID() {
        return (EReference)getMaterialSegmentSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationType_MaterialDefinitionID() {
        return (EReference)getMaterialSegmentSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationType_Description() {
        return (EReference)getMaterialSegmentSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationType_AssemblyType() {
        return (EReference)getMaterialSegmentSpecificationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationType_AssemblyRelationship() {
        return (EReference)getMaterialSegmentSpecificationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationType_AssemblySpecificationID() {
        return (EReference)getMaterialSegmentSpecificationType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationType_MaterialUse() {
        return (EReference)getMaterialSegmentSpecificationType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationType_Quantity() {
        return (EReference)getMaterialSegmentSpecificationType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSegmentSpecificationType_MaterialSegmentSpecificationProperty() {
        return (EReference)getMaterialSegmentSpecificationType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialSpecificationIDType() {
		if (materialSpecificationIDTypeEClass == null) {
			materialSpecificationIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(401);
		}
		return materialSpecificationIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialSubLotIDType() {
		if (materialSubLotIDTypeEClass == null) {
			materialSubLotIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(402);
		}
		return materialSubLotIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialSubLotType() {
		if (materialSubLotTypeEClass == null) {
			materialSubLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(403);
		}
		return materialSubLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_ID() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_Description() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_Location() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_HierarchyScope() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_Status() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_MaterialSublotProperty() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_StorageLocation() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_Quantity() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_MaterialSubLot() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_MaterialLotID() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_AssemblyLotID() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_AssemblySubLotID() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_AssemblyType() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialSubLotType_AssemblyRelationship() {
        return (EReference)getMaterialSubLotType().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialTestSpecificationIDType() {
		if (materialTestSpecificationIDTypeEClass == null) {
			materialTestSpecificationIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(404);
		}
		return materialTestSpecificationIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialTestSpecificationType() {
		if (materialTestSpecificationTypeEClass == null) {
			materialTestSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(405);
		}
		return materialTestSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialTestSpecificationType_Name() {
        return (EReference)getMaterialTestSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialTestSpecificationType_Description() {
        return (EReference)getMaterialTestSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialTestSpecificationType_Version() {
        return (EReference)getMaterialTestSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialTestSpecificationType_Location() {
        return (EReference)getMaterialTestSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialTestSpecificationType_HierarchyScope() {
        return (EReference)getMaterialTestSpecificationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialTestSpecificationType_TestedMaterialClassProperty() {
        return (EReference)getMaterialTestSpecificationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialTestSpecificationType_TestedMaterialDefinitionProperty() {
        return (EReference)getMaterialTestSpecificationType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMaterialTestSpecificationType_TestedMaterialLotProperty() {
        return (EReference)getMaterialTestSpecificationType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialUse1Type() {
		if (materialUse1TypeEClass == null) {
			materialUse1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(406);
		}
		return materialUse1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMaterialUseType() {
		if (materialUseTypeEClass == null) {
			materialUseTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(409);
		}
		return materialUseTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMaterialUseType_OtherValue() {
        return (EAttribute)getMaterialUseType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMeasureType() {
		if (measureTypeEClass == null) {
			measureTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(410);
		}
		return measureTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMeasureType_Value() {
        return (EAttribute)getMeasureType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMeasureType_UnitCode() {
        return (EAttribute)getMeasureType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMeasureType_UnitCodeListVersionID() {
        return (EAttribute)getMeasureType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNameType() {
		if (nameTypeEClass == null) {
			nameTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(411);
		}
		return nameTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNameType_Value() {
        return (EAttribute)getNameType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNameType_LanguageID() {
        return (EAttribute)getNameType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNumericType() {
		if (numericTypeEClass == null) {
			numericTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(412);
		}
		return numericTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumericType_Value() {
        return (EAttribute)getNumericType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumericType_Format() {
        return (EAttribute)getNumericType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpEquipmentActualPropertyType() {
		if (opEquipmentActualPropertyTypeEClass == null) {
			opEquipmentActualPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(413);
		}
		return opEquipmentActualPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualPropertyType_ID() {
        return (EReference)getOpEquipmentActualPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualPropertyType_Description() {
        return (EReference)getOpEquipmentActualPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualPropertyType_Value() {
        return (EReference)getOpEquipmentActualPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualPropertyType_Quantity() {
        return (EReference)getOpEquipmentActualPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualPropertyType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpEquipmentActualPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpEquipmentActualType() {
		if (opEquipmentActualTypeEClass == null) {
			opEquipmentActualTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(414);
		}
		return opEquipmentActualTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualType_EquipmentClassID() {
        return (EReference)getOpEquipmentActualType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualType_EquipmentID() {
        return (EReference)getOpEquipmentActualType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualType_Description() {
        return (EReference)getOpEquipmentActualType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualType_EquipmentUse() {
        return (EReference)getOpEquipmentActualType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualType_Quantity() {
        return (EReference)getOpEquipmentActualType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualType_HierarchyScope() {
        return (EReference)getOpEquipmentActualType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualType_EquipmentActualProperty() {
        return (EReference)getOpEquipmentActualType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentActualType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpEquipmentActualType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpEquipmentCapabilityPropertyType() {
		if (opEquipmentCapabilityPropertyTypeEClass == null) {
			opEquipmentCapabilityPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(415);
		}
		return opEquipmentCapabilityPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityPropertyType_ID() {
        return (EReference)getOpEquipmentCapabilityPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityPropertyType_Description() {
        return (EReference)getOpEquipmentCapabilityPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityPropertyType_Value() {
        return (EReference)getOpEquipmentCapabilityPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityPropertyType_Quantity() {
        return (EReference)getOpEquipmentCapabilityPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpEquipmentCapabilityType() {
		if (opEquipmentCapabilityTypeEClass == null) {
			opEquipmentCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(416);
		}
		return opEquipmentCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityType_EquipmentClassID() {
        return (EReference)getOpEquipmentCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityType_EquipmentID() {
        return (EReference)getOpEquipmentCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityType_Description() {
        return (EReference)getOpEquipmentCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityType_CapabilityType() {
        return (EReference)getOpEquipmentCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityType_Reason() {
        return (EReference)getOpEquipmentCapabilityType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityType_ConfidenceFactor() {
        return (EReference)getOpEquipmentCapabilityType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityType_HierarchyScope() {
        return (EReference)getOpEquipmentCapabilityType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityType_EquipmentUse() {
        return (EReference)getOpEquipmentCapabilityType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityType_StartTime() {
        return (EReference)getOpEquipmentCapabilityType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityType_EndTime() {
        return (EReference)getOpEquipmentCapabilityType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityType_Quantity() {
        return (EReference)getOpEquipmentCapabilityType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentCapabilityType_EquipmentCapabilityProperty() {
        return (EReference)getOpEquipmentCapabilityType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpEquipmentRequirementPropertyType() {
		if (opEquipmentRequirementPropertyTypeEClass == null) {
			opEquipmentRequirementPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(417);
		}
		return opEquipmentRequirementPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementPropertyType_ID() {
        return (EReference)getOpEquipmentRequirementPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementPropertyType_Description() {
        return (EReference)getOpEquipmentRequirementPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementPropertyType_Value() {
        return (EReference)getOpEquipmentRequirementPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementPropertyType_Quantity() {
        return (EReference)getOpEquipmentRequirementPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpEquipmentRequirementType() {
		if (opEquipmentRequirementTypeEClass == null) {
			opEquipmentRequirementTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(418);
		}
		return opEquipmentRequirementTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementType_EquipmentClassID() {
        return (EReference)getOpEquipmentRequirementType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementType_EquipmentID() {
        return (EReference)getOpEquipmentRequirementType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementType_Description() {
        return (EReference)getOpEquipmentRequirementType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementType_EquipmentUse() {
        return (EReference)getOpEquipmentRequirementType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementType_Quantity() {
        return (EReference)getOpEquipmentRequirementType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementType_HierarchyScope() {
        return (EReference)getOpEquipmentRequirementType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementType_EquipmentLevel() {
        return (EReference)getOpEquipmentRequirementType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementType_EquipmentRequirementProperty() {
        return (EReference)getOpEquipmentRequirementType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentRequirementType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpEquipmentRequirementType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpEquipmentSpecificationPropertyType() {
		if (opEquipmentSpecificationPropertyTypeEClass == null) {
			opEquipmentSpecificationPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(419);
		}
		return opEquipmentSpecificationPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentSpecificationPropertyType_ID() {
        return (EReference)getOpEquipmentSpecificationPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentSpecificationPropertyType_Description() {
        return (EReference)getOpEquipmentSpecificationPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentSpecificationPropertyType_Value() {
        return (EReference)getOpEquipmentSpecificationPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentSpecificationPropertyType_Quantity() {
        return (EReference)getOpEquipmentSpecificationPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpEquipmentSpecificationType() {
		if (opEquipmentSpecificationTypeEClass == null) {
			opEquipmentSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(420);
		}
		return opEquipmentSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentSpecificationType_EquipmentClassID() {
        return (EReference)getOpEquipmentSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentSpecificationType_EquipmentID() {
        return (EReference)getOpEquipmentSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentSpecificationType_Description() {
        return (EReference)getOpEquipmentSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentSpecificationType_EquipmentUse() {
        return (EReference)getOpEquipmentSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentSpecificationType_Quantity() {
        return (EReference)getOpEquipmentSpecificationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpEquipmentSpecificationType_EquipmentSpecificationProperty() {
        return (EReference)getOpEquipmentSpecificationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsCapabilityInformationType() {
		if (operationsCapabilityInformationTypeEClass == null) {
			operationsCapabilityInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(421);
		}
		return operationsCapabilityInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityInformationType_ID() {
        return (EReference)getOperationsCapabilityInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityInformationType_Description() {
        return (EReference)getOperationsCapabilityInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityInformationType_HierarchyScope() {
        return (EReference)getOperationsCapabilityInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityInformationType_PublishedDate() {
        return (EReference)getOperationsCapabilityInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityInformationType_OperationsCapability() {
        return (EReference)getOperationsCapabilityInformationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsCapabilityType() {
		if (operationsCapabilityTypeEClass == null) {
			operationsCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(422);
		}
		return operationsCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_ID() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_Description() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_HierarchyScope() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_CapabilityType() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_Reason() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_ConfidenceFactor() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_StartTime() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_EndTime() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_PublishedDate() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_PersonnelCapability() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_EquipmentCapability() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_PhysicalAssetCapability() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_MaterialCapability() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsCapabilityType_ProcessSegmentCapability() {
        return (EReference)getOperationsCapabilityType().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsDefinitionIDType() {
		if (operationsDefinitionIDTypeEClass == null) {
			operationsDefinitionIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(423);
		}
		return operationsDefinitionIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsDefinitionInformationType() {
		if (operationsDefinitionInformationTypeEClass == null) {
			operationsDefinitionInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(424);
		}
		return operationsDefinitionInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionInformationType_ID() {
        return (EReference)getOperationsDefinitionInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionInformationType_Description() {
        return (EReference)getOperationsDefinitionInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionInformationType_HierarchyScope() {
        return (EReference)getOperationsDefinitionInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionInformationType_PublishedDate() {
        return (EReference)getOperationsDefinitionInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionInformationType_OperationsDefinition() {
        return (EReference)getOperationsDefinitionInformationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsDefinitionType() {
		if (operationsDefinitionTypeEClass == null) {
			operationsDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(425);
		}
		return operationsDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionType_ID() {
        return (EReference)getOperationsDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionType_Version() {
        return (EReference)getOperationsDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionType_Description() {
        return (EReference)getOperationsDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionType_HierarchyScope() {
        return (EReference)getOperationsDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionType_OperationsType() {
        return (EReference)getOperationsDefinitionType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionType_PublishedDate() {
        return (EReference)getOperationsDefinitionType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionType_BillOfMaterialsID() {
        return (EReference)getOperationsDefinitionType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionType_WorkDefinitionID() {
        return (EReference)getOperationsDefinitionType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionType_BillOfResourcesID() {
        return (EReference)getOperationsDefinitionType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionType_OperationsMaterialBill() {
        return (EReference)getOperationsDefinitionType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsDefinitionType_OperationsSegment() {
        return (EReference)getOperationsDefinitionType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsMaterialBillItemType() {
		if (operationsMaterialBillItemTypeEClass == null) {
			operationsMaterialBillItemTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(426);
		}
		return operationsMaterialBillItemTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItemType_ID() {
        return (EReference)getOperationsMaterialBillItemType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItemType_Description() {
        return (EReference)getOperationsMaterialBillItemType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItemType_MaterialClassID() {
        return (EReference)getOperationsMaterialBillItemType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItemType_MaterialDefinitionID() {
        return (EReference)getOperationsMaterialBillItemType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItemType_UseType() {
        return (EReference)getOperationsMaterialBillItemType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItemType_AssemblyBillOfMaterialItem() {
        return (EReference)getOperationsMaterialBillItemType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItemType_AssemblyType() {
        return (EReference)getOperationsMaterialBillItemType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItemType_AssemblyRelationship() {
        return (EReference)getOperationsMaterialBillItemType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItemType_MaterialSpecificationID() {
        return (EReference)getOperationsMaterialBillItemType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillItemType_Quantity() {
        return (EReference)getOperationsMaterialBillItemType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsMaterialBillType() {
		if (operationsMaterialBillTypeEClass == null) {
			operationsMaterialBillTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(427);
		}
		return operationsMaterialBillTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillType_ID() {
        return (EReference)getOperationsMaterialBillType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillType_Description() {
        return (EReference)getOperationsMaterialBillType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsMaterialBillType_OperationsMaterialBillItem() {
        return (EReference)getOperationsMaterialBillType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsPerformanceType() {
		if (operationsPerformanceTypeEClass == null) {
			operationsPerformanceTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(428);
		}
		return operationsPerformanceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformanceType_ID() {
        return (EReference)getOperationsPerformanceType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformanceType_Description() {
        return (EReference)getOperationsPerformanceType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformanceType_HierarchyScope() {
        return (EReference)getOperationsPerformanceType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformanceType_OperationsType() {
        return (EReference)getOperationsPerformanceType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformanceType_OperationsScheduleID() {
        return (EReference)getOperationsPerformanceType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformanceType_StartTime() {
        return (EReference)getOperationsPerformanceType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformanceType_EndTime() {
        return (EReference)getOperationsPerformanceType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformanceType_PerformanceState() {
        return (EReference)getOperationsPerformanceType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformanceType_PublishedDate() {
        return (EReference)getOperationsPerformanceType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsPerformanceType_OperationsResponse() {
        return (EReference)getOperationsPerformanceType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsRequestIDType() {
		if (operationsRequestIDTypeEClass == null) {
			operationsRequestIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(429);
		}
		return operationsRequestIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsRequestType() {
		if (operationsRequestTypeEClass == null) {
			operationsRequestTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(430);
		}
		return operationsRequestTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequestType_ID() {
        return (EReference)getOperationsRequestType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequestType_Description() {
        return (EReference)getOperationsRequestType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequestType_HierarchyScope() {
        return (EReference)getOperationsRequestType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequestType_OperationsType() {
        return (EReference)getOperationsRequestType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequestType_StartTime() {
        return (EReference)getOperationsRequestType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequestType_EndTime() {
        return (EReference)getOperationsRequestType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequestType_Priority() {
        return (EReference)getOperationsRequestType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequestType_OperationsDefinitionID() {
        return (EReference)getOperationsRequestType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequestType_RequestState() {
        return (EReference)getOperationsRequestType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequestType_SegmentRequirement() {
        return (EReference)getOperationsRequestType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsRequestType_SegmentResponse() {
        return (EReference)getOperationsRequestType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsResponseType() {
		if (operationsResponseTypeEClass == null) {
			operationsResponseTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(431);
		}
		return operationsResponseTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponseType_ID() {
        return (EReference)getOperationsResponseType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponseType_Description() {
        return (EReference)getOperationsResponseType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponseType_HierarchyScope() {
        return (EReference)getOperationsResponseType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponseType_OperationsType() {
        return (EReference)getOperationsResponseType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponseType_OperationsRequestID() {
        return (EReference)getOperationsResponseType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponseType_StartTime() {
        return (EReference)getOperationsResponseType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponseType_EndTime() {
        return (EReference)getOperationsResponseType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponseType_OperationsDefinitionID() {
        return (EReference)getOperationsResponseType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponseType_ResponseState() {
        return (EReference)getOperationsResponseType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsResponseType_SegmentResponse() {
        return (EReference)getOperationsResponseType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsScheduleIDType() {
		if (operationsScheduleIDTypeEClass == null) {
			operationsScheduleIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(432);
		}
		return operationsScheduleIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsScheduleType() {
		if (operationsScheduleTypeEClass == null) {
			operationsScheduleTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(433);
		}
		return operationsScheduleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsScheduleType_ID() {
        return (EReference)getOperationsScheduleType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsScheduleType_Description() {
        return (EReference)getOperationsScheduleType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsScheduleType_HierarchyScope() {
        return (EReference)getOperationsScheduleType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsScheduleType_OperationsType() {
        return (EReference)getOperationsScheduleType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsScheduleType_StartTime() {
        return (EReference)getOperationsScheduleType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsScheduleType_EndTime() {
        return (EReference)getOperationsScheduleType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsScheduleType_ScheduleState() {
        return (EReference)getOperationsScheduleType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsScheduleType_PublishedDate() {
        return (EReference)getOperationsScheduleType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsScheduleType_OperationsRequest() {
        return (EReference)getOperationsScheduleType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsSegmentIDType() {
		if (operationsSegmentIDTypeEClass == null) {
			operationsSegmentIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(434);
		}
		return operationsSegmentIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsSegmentType() {
		if (operationsSegmentTypeEClass == null) {
			operationsSegmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(435);
		}
		return operationsSegmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentType_ID() {
        return (EReference)getOperationsSegmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentType_Description() {
        return (EReference)getOperationsSegmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentType_OperationsType() {
        return (EReference)getOperationsSegmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentType_HierarchyScope() {
        return (EReference)getOperationsSegmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsSegmentType_Duration() {
        return (EAttribute)getOperationsSegmentType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentType_ProcessSegmentID() {
        return (EReference)getOperationsSegmentType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentType_Parameter() {
        return (EReference)getOperationsSegmentType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentType_PersonnelSpecification() {
        return (EReference)getOperationsSegmentType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentType_EquipmentSpecification() {
        return (EReference)getOperationsSegmentType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentType_PhysicalAssetSpecification() {
        return (EReference)getOperationsSegmentType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentType_MaterialSpecification() {
        return (EReference)getOperationsSegmentType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentType_SegmentDependency() {
        return (EReference)getOperationsSegmentType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationsSegmentType_OperationsSegment() {
        return (EReference)getOperationsSegmentType().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsType1Type() {
		if (operationsType1TypeEClass == null) {
			operationsType1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(436);
		}
		return operationsType1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationsTypeType() {
		if (operationsTypeTypeEClass == null) {
			operationsTypeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(439);
		}
		return operationsTypeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationsTypeType_OtherValue() {
        return (EAttribute)getOperationsTypeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpMaterialActualPropertyType() {
		if (opMaterialActualPropertyTypeEClass == null) {
			opMaterialActualPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(440);
		}
		return opMaterialActualPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualPropertyType_ID() {
        return (EReference)getOpMaterialActualPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualPropertyType_Description() {
        return (EReference)getOpMaterialActualPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualPropertyType_Value() {
        return (EReference)getOpMaterialActualPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualPropertyType_Quantity() {
        return (EReference)getOpMaterialActualPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualPropertyType_MaterialActualProperty() {
        return (EReference)getOpMaterialActualPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualPropertyType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpMaterialActualPropertyType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpMaterialActualType() {
		if (opMaterialActualTypeEClass == null) {
			opMaterialActualTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(441);
		}
		return opMaterialActualTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_MaterialClassID() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_MaterialDefinitionID() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_MaterialLotID() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_MaterialSubLotID() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_Description() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_MaterialUse() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_StorageLocation() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_Quantity() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_AssemblyActual() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_AssemblyType() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_AssemblyRelationship() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_HierarchyScope() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_MaterialActualProperty() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialActualType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpMaterialActualType().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpMaterialCapabilityPropertyType() {
		if (opMaterialCapabilityPropertyTypeEClass == null) {
			opMaterialCapabilityPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(442);
		}
		return opMaterialCapabilityPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityPropertyType_ID() {
        return (EReference)getOpMaterialCapabilityPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityPropertyType_Description() {
        return (EReference)getOpMaterialCapabilityPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityPropertyType_Value() {
        return (EReference)getOpMaterialCapabilityPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityPropertyType_Quantity() {
        return (EReference)getOpMaterialCapabilityPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityPropertyType_MaterialUse() {
        return (EReference)getOpMaterialCapabilityPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpMaterialCapabilityType() {
		if (opMaterialCapabilityTypeEClass == null) {
			opMaterialCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(443);
		}
		return opMaterialCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_MaterialClassID() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_MaterialDefinitionID() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_MaterialLotID() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_MaterialSubLotID() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_Description() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_CapabilityType() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_Reason() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_ConfidenceFactor() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_HierarchyScope() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_MaterialUse() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_StartTime() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_EndTime() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_AssemblyCapability() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_AssemblyType() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_AssemblyRelationship() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_Quantity() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialCapabilityType_MaterialCapabilityProperty() {
        return (EReference)getOpMaterialCapabilityType().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpMaterialRequirementPropertyType() {
		if (opMaterialRequirementPropertyTypeEClass == null) {
			opMaterialRequirementPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(444);
		}
		return opMaterialRequirementPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementPropertyType_ID() {
        return (EReference)getOpMaterialRequirementPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementPropertyType_Description() {
        return (EReference)getOpMaterialRequirementPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementPropertyType_Value() {
        return (EReference)getOpMaterialRequirementPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementPropertyType_Quantity() {
        return (EReference)getOpMaterialRequirementPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementPropertyType_MaterialRequirementProperty() {
        return (EReference)getOpMaterialRequirementPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpMaterialRequirementType() {
		if (opMaterialRequirementTypeEClass == null) {
			opMaterialRequirementTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(445);
		}
		return opMaterialRequirementTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_MaterialClassID() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_MaterialDefinitionID() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_MaterialLotID() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_MaterialSubLotID() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_Description() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_MaterialUse() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_StorageLocation() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_Quantity() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_AssemblyRequirement() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_AssemblyType() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_AssemblyRelationship() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_HierarchyScope() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_MaterialRequirementProperty() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialRequirementType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpMaterialRequirementType().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpMaterialSpecificationPropertyType() {
		if (opMaterialSpecificationPropertyTypeEClass == null) {
			opMaterialSpecificationPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(446);
		}
		return opMaterialSpecificationPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationPropertyType_ID() {
        return (EReference)getOpMaterialSpecificationPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationPropertyType_Description() {
        return (EReference)getOpMaterialSpecificationPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationPropertyType_Value() {
        return (EReference)getOpMaterialSpecificationPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationPropertyType_Quantity() {
        return (EReference)getOpMaterialSpecificationPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpMaterialSpecificationType() {
		if (opMaterialSpecificationTypeEClass == null) {
			opMaterialSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(447);
		}
		return opMaterialSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationType_ID() {
        return (EReference)getOpMaterialSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationType_MaterialClassID() {
        return (EReference)getOpMaterialSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationType_MaterialDefinitionID() {
        return (EReference)getOpMaterialSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationType_Description() {
        return (EReference)getOpMaterialSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationType_MaterialUse() {
        return (EReference)getOpMaterialSpecificationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationType_Quantity() {
        return (EReference)getOpMaterialSpecificationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationType_AssemblySpecification() {
        return (EReference)getOpMaterialSpecificationType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationType_AssemblyType() {
        return (EReference)getOpMaterialSpecificationType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationType_AssemblyRelationship() {
        return (EReference)getOpMaterialSpecificationType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpMaterialSpecificationType_MaterialSpecificationProperty() {
        return (EReference)getOpMaterialSpecificationType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPersonnelActualPropertyType() {
		if (opPersonnelActualPropertyTypeEClass == null) {
			opPersonnelActualPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(448);
		}
		return opPersonnelActualPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualPropertyType_ID() {
        return (EReference)getOpPersonnelActualPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualPropertyType_Description() {
        return (EReference)getOpPersonnelActualPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualPropertyType_Value() {
        return (EReference)getOpPersonnelActualPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualPropertyType_Quantity() {
        return (EReference)getOpPersonnelActualPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualPropertyType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpPersonnelActualPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPersonnelActualType() {
		if (opPersonnelActualTypeEClass == null) {
			opPersonnelActualTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(449);
		}
		return opPersonnelActualTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualType_PersonnelClassID() {
        return (EReference)getOpPersonnelActualType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualType_PersonID() {
        return (EReference)getOpPersonnelActualType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualType_Description() {
        return (EReference)getOpPersonnelActualType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualType_PersonnelUse() {
        return (EReference)getOpPersonnelActualType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualType_Quantity() {
        return (EReference)getOpPersonnelActualType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualType_HierarchyScope() {
        return (EReference)getOpPersonnelActualType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualType_PersonnelActualProperty() {
        return (EReference)getOpPersonnelActualType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelActualType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpPersonnelActualType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPersonnelCapabilityPropertyType() {
		if (opPersonnelCapabilityPropertyTypeEClass == null) {
			opPersonnelCapabilityPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(450);
		}
		return opPersonnelCapabilityPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityPropertyType_ID() {
        return (EReference)getOpPersonnelCapabilityPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityPropertyType_Description() {
        return (EReference)getOpPersonnelCapabilityPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityPropertyType_Value() {
        return (EReference)getOpPersonnelCapabilityPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityPropertyType_Quantity() {
        return (EReference)getOpPersonnelCapabilityPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPersonnelCapabilityType() {
		if (opPersonnelCapabilityTypeEClass == null) {
			opPersonnelCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(451);
		}
		return opPersonnelCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityType_PersonnelClassID() {
        return (EReference)getOpPersonnelCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityType_PersonID() {
        return (EReference)getOpPersonnelCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityType_Description() {
        return (EReference)getOpPersonnelCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityType_CapabilityType() {
        return (EReference)getOpPersonnelCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityType_Reason() {
        return (EReference)getOpPersonnelCapabilityType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityType_ConfidenceFactor() {
        return (EReference)getOpPersonnelCapabilityType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityType_HierarchyScope() {
        return (EReference)getOpPersonnelCapabilityType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityType_PersonnelUse() {
        return (EReference)getOpPersonnelCapabilityType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityType_StartTime() {
        return (EReference)getOpPersonnelCapabilityType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityType_EndTime() {
        return (EReference)getOpPersonnelCapabilityType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityType_Quantity() {
        return (EReference)getOpPersonnelCapabilityType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelCapabilityType_PersonnelCapabilityProperty() {
        return (EReference)getOpPersonnelCapabilityType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPersonnelRequirementPropertyType() {
		if (opPersonnelRequirementPropertyTypeEClass == null) {
			opPersonnelRequirementPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(452);
		}
		return opPersonnelRequirementPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelRequirementPropertyType_ID() {
        return (EReference)getOpPersonnelRequirementPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelRequirementPropertyType_Description() {
        return (EReference)getOpPersonnelRequirementPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelRequirementPropertyType_Value() {
        return (EReference)getOpPersonnelRequirementPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelRequirementPropertyType_Quantity() {
        return (EReference)getOpPersonnelRequirementPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPersonnelRequirementType() {
		if (opPersonnelRequirementTypeEClass == null) {
			opPersonnelRequirementTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(453);
		}
		return opPersonnelRequirementTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelRequirementType_PersonnelClassID() {
        return (EReference)getOpPersonnelRequirementType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelRequirementType_PersonID() {
        return (EReference)getOpPersonnelRequirementType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelRequirementType_Description() {
        return (EReference)getOpPersonnelRequirementType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelRequirementType_PersonnelUse() {
        return (EReference)getOpPersonnelRequirementType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelRequirementType_Quantity() {
        return (EReference)getOpPersonnelRequirementType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelRequirementType_HierarchyScope() {
        return (EReference)getOpPersonnelRequirementType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelRequirementType_PersonnelRequirementProperty() {
        return (EReference)getOpPersonnelRequirementType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelRequirementType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpPersonnelRequirementType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPersonnelSpecificationPropertyType() {
		if (opPersonnelSpecificationPropertyTypeEClass == null) {
			opPersonnelSpecificationPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(454);
		}
		return opPersonnelSpecificationPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelSpecificationPropertyType_ID() {
        return (EReference)getOpPersonnelSpecificationPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelSpecificationPropertyType_Description() {
        return (EReference)getOpPersonnelSpecificationPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelSpecificationPropertyType_Value() {
        return (EReference)getOpPersonnelSpecificationPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelSpecificationPropertyType_Quantity() {
        return (EReference)getOpPersonnelSpecificationPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPersonnelSpecificationType() {
		if (opPersonnelSpecificationTypeEClass == null) {
			opPersonnelSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(455);
		}
		return opPersonnelSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelSpecificationType_PersonnelClassID() {
        return (EReference)getOpPersonnelSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelSpecificationType_PersonID() {
        return (EReference)getOpPersonnelSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelSpecificationType_Description() {
        return (EReference)getOpPersonnelSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelSpecificationType_PersonnelUse() {
        return (EReference)getOpPersonnelSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelSpecificationType_Quantity() {
        return (EReference)getOpPersonnelSpecificationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPersonnelSpecificationType_PersonnelSpecificationProperty() {
        return (EReference)getOpPersonnelSpecificationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPhysicalAssetActualPropertyType() {
		if (opPhysicalAssetActualPropertyTypeEClass == null) {
			opPhysicalAssetActualPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(456);
		}
		return opPhysicalAssetActualPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetActualPropertyType_ID() {
        return (EReference)getOpPhysicalAssetActualPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetActualPropertyType_Description() {
        return (EReference)getOpPhysicalAssetActualPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetActualPropertyType_Value() {
        return (EReference)getOpPhysicalAssetActualPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetActualPropertyType_Quantity() {
        return (EReference)getOpPhysicalAssetActualPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetActualPropertyType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpPhysicalAssetActualPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPhysicalAssetActualType() {
		if (opPhysicalAssetActualTypeEClass == null) {
			opPhysicalAssetActualTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(457);
		}
		return opPhysicalAssetActualTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetActualType_PhysicalAssetClassID() {
        return (EReference)getOpPhysicalAssetActualType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetActualType_PhysicalAssetID() {
        return (EReference)getOpPhysicalAssetActualType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetActualType_Description() {
        return (EReference)getOpPhysicalAssetActualType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetActualType_Quantity() {
        return (EReference)getOpPhysicalAssetActualType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetActualType_HierarchyScope() {
        return (EReference)getOpPhysicalAssetActualType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetActualType_PhysicalAssetActualProperty() {
        return (EReference)getOpPhysicalAssetActualType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetActualType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpPhysicalAssetActualType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPhysicalAssetCapabilityPropertyType() {
		if (opPhysicalAssetCapabilityPropertyTypeEClass == null) {
			opPhysicalAssetCapabilityPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(458);
		}
		return opPhysicalAssetCapabilityPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityPropertyType_ID() {
        return (EReference)getOpPhysicalAssetCapabilityPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityPropertyType_Description() {
        return (EReference)getOpPhysicalAssetCapabilityPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityPropertyType_Value() {
        return (EReference)getOpPhysicalAssetCapabilityPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityPropertyType_Quantity() {
        return (EReference)getOpPhysicalAssetCapabilityPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPhysicalAssetCapabilityType() {
		if (opPhysicalAssetCapabilityTypeEClass == null) {
			opPhysicalAssetCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(459);
		}
		return opPhysicalAssetCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityType_PhysicalAssetClassID() {
        return (EReference)getOpPhysicalAssetCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityType_PhysicalAssetID() {
        return (EReference)getOpPhysicalAssetCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityType_Description() {
        return (EReference)getOpPhysicalAssetCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityType_CapabilityType() {
        return (EReference)getOpPhysicalAssetCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityType_Reason() {
        return (EReference)getOpPhysicalAssetCapabilityType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityType_ConfidenceFactor() {
        return (EReference)getOpPhysicalAssetCapabilityType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityType_HierarchyScope() {
        return (EReference)getOpPhysicalAssetCapabilityType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityType_PhysicalAssetUse() {
        return (EReference)getOpPhysicalAssetCapabilityType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityType_StartTime() {
        return (EReference)getOpPhysicalAssetCapabilityType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityType_EndTime() {
        return (EReference)getOpPhysicalAssetCapabilityType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityType_Quantity() {
        return (EReference)getOpPhysicalAssetCapabilityType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetCapabilityType_PhysicalAssetCapabilityProperty() {
        return (EReference)getOpPhysicalAssetCapabilityType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPhysicalAssetRequirementPropertyType() {
		if (opPhysicalAssetRequirementPropertyTypeEClass == null) {
			opPhysicalAssetRequirementPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(460);
		}
		return opPhysicalAssetRequirementPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementPropertyType_ID() {
        return (EReference)getOpPhysicalAssetRequirementPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementPropertyType_Description() {
        return (EReference)getOpPhysicalAssetRequirementPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementPropertyType_Value() {
        return (EReference)getOpPhysicalAssetRequirementPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementPropertyType_Quantity() {
        return (EReference)getOpPhysicalAssetRequirementPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPhysicalAssetRequirementType() {
		if (opPhysicalAssetRequirementTypeEClass == null) {
			opPhysicalAssetRequirementTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(461);
		}
		return opPhysicalAssetRequirementTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementType_PhysicalAssetClassID() {
        return (EReference)getOpPhysicalAssetRequirementType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementType_PhysicalAssetID() {
        return (EReference)getOpPhysicalAssetRequirementType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementType_Description() {
        return (EReference)getOpPhysicalAssetRequirementType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementType_PhysicalAssetUse() {
        return (EReference)getOpPhysicalAssetRequirementType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementType_Quantity() {
        return (EReference)getOpPhysicalAssetRequirementType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementType_HierarchyScope() {
        return (EReference)getOpPhysicalAssetRequirementType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementType_EquipmentLevel() {
        return (EReference)getOpPhysicalAssetRequirementType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementType_PhysicalAssetRequirementProperty() {
        return (EReference)getOpPhysicalAssetRequirementType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetRequirementType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpPhysicalAssetRequirementType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPhysicalAssetSpecificationPropertyType() {
		if (opPhysicalAssetSpecificationPropertyTypeEClass == null) {
			opPhysicalAssetSpecificationPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(462);
		}
		return opPhysicalAssetSpecificationPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetSpecificationPropertyType_ID() {
        return (EReference)getOpPhysicalAssetSpecificationPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetSpecificationPropertyType_Description() {
        return (EReference)getOpPhysicalAssetSpecificationPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetSpecificationPropertyType_Value() {
        return (EReference)getOpPhysicalAssetSpecificationPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetSpecificationPropertyType_Quantity() {
        return (EReference)getOpPhysicalAssetSpecificationPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpPhysicalAssetSpecificationType() {
		if (opPhysicalAssetSpecificationTypeEClass == null) {
			opPhysicalAssetSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(463);
		}
		return opPhysicalAssetSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetSpecificationType_PhysicalAssetClassID() {
        return (EReference)getOpPhysicalAssetSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetSpecificationType_PhysicalAssetID() {
        return (EReference)getOpPhysicalAssetSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetSpecificationType_Description() {
        return (EReference)getOpPhysicalAssetSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetSpecificationType_PhysicalAssetUse() {
        return (EReference)getOpPhysicalAssetSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetSpecificationType_Quantity() {
        return (EReference)getOpPhysicalAssetSpecificationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpPhysicalAssetSpecificationType_PhysicalAssetSpecificationProperty() {
        return (EReference)getOpPhysicalAssetSpecificationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpProcessSegmentCapabilityType() {
		if (opProcessSegmentCapabilityTypeEClass == null) {
			opProcessSegmentCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(464);
		}
		return opProcessSegmentCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_ID() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_Description() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_ProcessSegmentID() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_CapabilityType() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_Reason() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_HierarchyScope() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_EquipmentElementLevel() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_StartTime() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_EndTime() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_PersonnelCapability() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_EquipmentCapability() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_PhysicalAssetCapability() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_MaterialCapability() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpProcessSegmentCapabilityType_ProcessSegmentCapability() {
        return (EReference)getOpProcessSegmentCapabilityType().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpSegmentDataType() {
		if (opSegmentDataTypeEClass == null) {
			opSegmentDataTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(465);
		}
		return opSegmentDataTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentDataType_ID() {
        return (EReference)getOpSegmentDataType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentDataType_Description() {
        return (EReference)getOpSegmentDataType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentDataType_Value() {
        return (EReference)getOpSegmentDataType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentDataType_SegmentData() {
        return (EReference)getOpSegmentDataType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentDataType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpSegmentDataType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpSegmentRequirementType() {
		if (opSegmentRequirementTypeEClass == null) {
			opSegmentRequirementTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(466);
		}
		return opSegmentRequirementTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_ID() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_Description() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_HierarchyScope() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_OperationsType() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_ProcessSegmentID() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_EarliestStartTime() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_LatestEndTime() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOpSegmentRequirementType_Duration() {
        return (EAttribute)getOpSegmentRequirementType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_OperationsDefinitionID() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_SegmentState() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_SegmentParameter() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_PersonnelRequirement() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_EquipmentRequirement() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_PhysicalAssetRequirement() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_MaterialRequirement() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_SegmentRequirement() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentRequirementType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpSegmentRequirementType().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpSegmentResponseType() {
		if (opSegmentResponseTypeEClass == null) {
			opSegmentResponseTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(467);
		}
		return opSegmentResponseTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_ID() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_Description() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_HierarchyScope() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_OperationsType() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_ProcessSegmentID() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_ActualStartTime() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_ActualEndTime() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_OperationsDefinitionID() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_SegmentState() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_SegmentData() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_PersonnelActual() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_EquipmentActual() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_PhysicalAssetActual() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_MaterialActual() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_SegmentResponse() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOpSegmentResponseType_RequiredByRequestedSegmentResponse() {
        return (EReference)getOpSegmentResponseType().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOtherDependencyType() {
		if (otherDependencyTypeEClass == null) {
			otherDependencyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(468);
		}
		return otherDependencyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterIDType() {
		if (parameterIDTypeEClass == null) {
			parameterIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(469);
		}
		return parameterIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterType() {
		if (parameterTypeEClass == null) {
			parameterTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(470);
		}
		return parameterTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterType_ID() {
        return (EReference)getParameterType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterType_Value() {
        return (EReference)getParameterType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterType_Description() {
        return (EReference)getParameterType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterType_Parameter() {
        return (EReference)getParameterType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonIDType() {
		if (personIDTypeEClass == null) {
			personIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(471);
		}
		return personIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonNameType() {
		if (personNameTypeEClass == null) {
			personNameTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(472);
		}
		return personNameTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelClassIDType() {
		if (personnelClassIDTypeEClass == null) {
			personnelClassIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(473);
		}
		return personnelClassIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelClassPropertyType() {
		if (personnelClassPropertyTypeEClass == null) {
			personnelClassPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(474);
		}
		return personnelClassPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassPropertyType_ID() {
        return (EReference)getPersonnelClassPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassPropertyType_Description() {
        return (EReference)getPersonnelClassPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassPropertyType_Value() {
        return (EReference)getPersonnelClassPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassPropertyType_PersonnelClassProperty() {
        return (EReference)getPersonnelClassPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassPropertyType_QualificationTestSpecificationID() {
        return (EReference)getPersonnelClassPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelClassType() {
		if (personnelClassTypeEClass == null) {
			personnelClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(475);
		}
		return personnelClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassType_ID() {
        return (EReference)getPersonnelClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassType_Description() {
        return (EReference)getPersonnelClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassType_Location() {
        return (EReference)getPersonnelClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassType_HierarchyScope() {
        return (EReference)getPersonnelClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassType_PersonnelClassProperty() {
        return (EReference)getPersonnelClassType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassType_PersonID() {
        return (EReference)getPersonnelClassType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelClassType_QualificationTestSpecificationID() {
        return (EReference)getPersonnelClassType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelInformationType() {
		if (personnelInformationTypeEClass == null) {
			personnelInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(476);
		}
		return personnelInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelInformationType_ID() {
        return (EReference)getPersonnelInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelInformationType_Description() {
        return (EReference)getPersonnelInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelInformationType_Location() {
        return (EReference)getPersonnelInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelInformationType_HierarchyScope() {
        return (EReference)getPersonnelInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelInformationType_PublishedDate() {
        return (EReference)getPersonnelInformationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelInformationType_Person() {
        return (EReference)getPersonnelInformationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelInformationType_PersonnelClass() {
        return (EReference)getPersonnelInformationType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelInformationType_QualificationTestSpecification() {
        return (EReference)getPersonnelInformationType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelSegmentSpecificationPropertyType() {
		if (personnelSegmentSpecificationPropertyTypeEClass == null) {
			personnelSegmentSpecificationPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(477);
		}
		return personnelSegmentSpecificationPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecificationPropertyType_ID() {
        return (EReference)getPersonnelSegmentSpecificationPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecificationPropertyType_Description() {
        return (EReference)getPersonnelSegmentSpecificationPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecificationPropertyType_Value() {
        return (EReference)getPersonnelSegmentSpecificationPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecificationPropertyType_Quantity() {
        return (EReference)getPersonnelSegmentSpecificationPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelSegmentSpecificationType() {
		if (personnelSegmentSpecificationTypeEClass == null) {
			personnelSegmentSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(478);
		}
		return personnelSegmentSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecificationType_PersonnelClassID() {
        return (EReference)getPersonnelSegmentSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecificationType_PersonID() {
        return (EReference)getPersonnelSegmentSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecificationType_Description() {
        return (EReference)getPersonnelSegmentSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecificationType_PersonnelUse() {
        return (EReference)getPersonnelSegmentSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecificationType_Quantity() {
        return (EReference)getPersonnelSegmentSpecificationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonnelSegmentSpecificationType_PersonnelSegmentSpecificationProperty() {
        return (EReference)getPersonnelSegmentSpecificationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonnelUseType() {
		if (personnelUseTypeEClass == null) {
			personnelUseTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(479);
		}
		return personnelUseTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonPropertyType() {
		if (personPropertyTypeEClass == null) {
			personPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(480);
		}
		return personPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonPropertyType_ID() {
        return (EReference)getPersonPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonPropertyType_Description() {
        return (EReference)getPersonPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonPropertyType_Value() {
        return (EReference)getPersonPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonPropertyType_PersonProperty() {
        return (EReference)getPersonPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonPropertyType_QualificationTestSpecificationID() {
        return (EReference)getPersonPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonPropertyType_TestResult() {
        return (EReference)getPersonPropertyType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonType() {
		if (personTypeEClass == null) {
			personTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(481);
		}
		return personTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonType_ID() {
        return (EReference)getPersonType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonType_Description() {
        return (EReference)getPersonType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonType_Location() {
        return (EReference)getPersonType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonType_HierarchyScope() {
        return (EReference)getPersonType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonType_PersonName() {
        return (EReference)getPersonType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonType_PersonProperty() {
        return (EReference)getPersonType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonType_PersonnelClassID() {
        return (EReference)getPersonType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPersonType_QualificationTestSpecificationID() {
        return (EReference)getPersonType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetActualIDType() {
		if (physicalAssetActualIDTypeEClass == null) {
			physicalAssetActualIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(482);
		}
		return physicalAssetActualIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetCapabilityTestSpecificationIDType() {
		if (physicalAssetCapabilityTestSpecificationIDTypeEClass == null) {
			physicalAssetCapabilityTestSpecificationIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(483);
		}
		return physicalAssetCapabilityTestSpecificationIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetCapabilityTestSpecificationType() {
		if (physicalAssetCapabilityTestSpecificationTypeEClass == null) {
			physicalAssetCapabilityTestSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(484);
		}
		return physicalAssetCapabilityTestSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapabilityTestSpecificationType_Name() {
        return (EReference)getPhysicalAssetCapabilityTestSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapabilityTestSpecificationType_Description() {
        return (EReference)getPhysicalAssetCapabilityTestSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapabilityTestSpecificationType_Version() {
        return (EReference)getPhysicalAssetCapabilityTestSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapabilityTestSpecificationType_HierarchyScope() {
        return (EReference)getPhysicalAssetCapabilityTestSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapabilityTestSpecificationType_TestedPhysicalAssetProperty() {
        return (EReference)getPhysicalAssetCapabilityTestSpecificationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetCapabilityTestSpecificationType_TestedPhysicalAssetClassProperty() {
        return (EReference)getPhysicalAssetCapabilityTestSpecificationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetClassIDType() {
		if (physicalAssetClassIDTypeEClass == null) {
			physicalAssetClassIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(485);
		}
		return physicalAssetClassIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetClassPropertyType() {
		if (physicalAssetClassPropertyTypeEClass == null) {
			physicalAssetClassPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(486);
		}
		return physicalAssetClassPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassPropertyType_ID() {
        return (EReference)getPhysicalAssetClassPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassPropertyType_Description() {
        return (EReference)getPhysicalAssetClassPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassPropertyType_Value() {
        return (EReference)getPhysicalAssetClassPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassPropertyType_PhysicalAssetClassProperty() {
        return (EReference)getPhysicalAssetClassPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassPropertyType_PhysicalAssetCapabilityTestSpecificationID() {
        return (EReference)getPhysicalAssetClassPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetClassType() {
		if (physicalAssetClassTypeEClass == null) {
			physicalAssetClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(487);
		}
		return physicalAssetClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassType_ID() {
        return (EReference)getPhysicalAssetClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassType_Description() {
        return (EReference)getPhysicalAssetClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassType_HierarchyScope() {
        return (EReference)getPhysicalAssetClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassType_Manufacturer() {
        return (EReference)getPhysicalAssetClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassType_PhysicalAssetClassProperty() {
        return (EReference)getPhysicalAssetClassType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassType_PhysicalAssetID() {
        return (EReference)getPhysicalAssetClassType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetClassType_PhysicalAssetCapabilityTestSpecificationID() {
        return (EReference)getPhysicalAssetClassType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetIDType() {
		if (physicalAssetIDTypeEClass == null) {
			physicalAssetIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(488);
		}
		return physicalAssetIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetInformationType() {
		if (physicalAssetInformationTypeEClass == null) {
			physicalAssetInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(489);
		}
		return physicalAssetInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetInformationType_ID() {
        return (EReference)getPhysicalAssetInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetInformationType_Description() {
        return (EReference)getPhysicalAssetInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetInformationType_HierarchyScope() {
        return (EReference)getPhysicalAssetInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetInformationType_PublishedDate() {
        return (EReference)getPhysicalAssetInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetInformationType_PhysicalAsset() {
        return (EReference)getPhysicalAssetInformationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetInformationType_PhysicalAssetClass() {
        return (EReference)getPhysicalAssetInformationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetInformationType_PhysicalAssetCapabilityTestSpecification() {
        return (EReference)getPhysicalAssetInformationType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetPropertyType() {
		if (physicalAssetPropertyTypeEClass == null) {
			physicalAssetPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(490);
		}
		return physicalAssetPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetPropertyType_ID() {
        return (EReference)getPhysicalAssetPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetPropertyType_Description() {
        return (EReference)getPhysicalAssetPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetPropertyType_Value() {
        return (EReference)getPhysicalAssetPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetPropertyType_PhysicalAssetProperty() {
        return (EReference)getPhysicalAssetPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetPropertyType_PhysicalAssetCapabilityTestSpecificationID() {
        return (EReference)getPhysicalAssetPropertyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetPropertyType_TestResult() {
        return (EReference)getPhysicalAssetPropertyType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetSegmentSpecificationPropertyType() {
		if (physicalAssetSegmentSpecificationPropertyTypeEClass == null) {
			physicalAssetSegmentSpecificationPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(491);
		}
		return physicalAssetSegmentSpecificationPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecificationPropertyType_ID() {
        return (EReference)getPhysicalAssetSegmentSpecificationPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecificationPropertyType_Description() {
        return (EReference)getPhysicalAssetSegmentSpecificationPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecificationPropertyType_Value() {
        return (EReference)getPhysicalAssetSegmentSpecificationPropertyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecificationPropertyType_Quantity() {
        return (EReference)getPhysicalAssetSegmentSpecificationPropertyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetSegmentSpecificationType() {
		if (physicalAssetSegmentSpecificationTypeEClass == null) {
			physicalAssetSegmentSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(492);
		}
		return physicalAssetSegmentSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecificationType_PhysicalAssetClassID() {
        return (EReference)getPhysicalAssetSegmentSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecificationType_PhysicalAssetID() {
        return (EReference)getPhysicalAssetSegmentSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecificationType_Description() {
        return (EReference)getPhysicalAssetSegmentSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecificationType_PhysicalAssetUse() {
        return (EReference)getPhysicalAssetSegmentSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecificationType_Quantity() {
        return (EReference)getPhysicalAssetSegmentSpecificationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetSegmentSpecificationType_PhysicalAssetSegmentSpecificationProperty() {
        return (EReference)getPhysicalAssetSegmentSpecificationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetType() {
		if (physicalAssetTypeEClass == null) {
			physicalAssetTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(493);
		}
		return physicalAssetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetType_ID() {
        return (EReference)getPhysicalAssetType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetType_Description() {
        return (EReference)getPhysicalAssetType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetType_HierarchyScope() {
        return (EReference)getPhysicalAssetType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetType_PhysicalLocation() {
        return (EReference)getPhysicalAssetType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetType_FixedAssetID() {
        return (EReference)getPhysicalAssetType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetType_VendorID() {
        return (EReference)getPhysicalAssetType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetType_EquipmentLevel() {
        return (EReference)getPhysicalAssetType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetType_EquipmentAssetMapping() {
        return (EReference)getPhysicalAssetType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetType_PhysicalAssetProperty() {
        return (EReference)getPhysicalAssetType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetType_PhysicalAsset() {
        return (EReference)getPhysicalAssetType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetType_PhysicalAssetClassID() {
        return (EReference)getPhysicalAssetType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalAssetType_PhysicalAssetCapabilityTestSpecificationID() {
        return (EReference)getPhysicalAssetType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalAssetUseType() {
		if (physicalAssetUseTypeEClass == null) {
			physicalAssetUseTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(494);
		}
		return physicalAssetUseTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPlannedFinishTimeType() {
		if (plannedFinishTimeTypeEClass == null) {
			plannedFinishTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(495);
		}
		return plannedFinishTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPlannedStartTimeType() {
		if (plannedStartTimeTypeEClass == null) {
			plannedStartTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(496);
		}
		return plannedStartTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPriorityType() {
		if (priorityTypeEClass == null) {
			priorityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(497);
		}
		return priorityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProblemType() {
		if (problemTypeEClass == null) {
			problemTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(498);
		}
		return problemTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessEquipmentCapabilityTestSpecType() {
		if (processEquipmentCapabilityTestSpecTypeEClass == null) {
			processEquipmentCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(499);
		}
		return processEquipmentCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessEquipmentCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getProcessEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessEquipmentCapabilityTestSpecType_DataArea() {
        return (EReference)getProcessEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessEquipmentCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getProcessEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessEquipmentCapabilityTestSpecType_VersionID() {
        return (EAttribute)getProcessEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessEquipmentClassType() {
		if (processEquipmentClassTypeEClass == null) {
			processEquipmentClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(500);
		}
		return processEquipmentClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessEquipmentClassType_ApplicationArea() {
        return (EReference)getProcessEquipmentClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessEquipmentClassType_DataArea() {
        return (EReference)getProcessEquipmentClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessEquipmentClassType_ReleaseID() {
        return (EAttribute)getProcessEquipmentClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessEquipmentClassType_VersionID() {
        return (EAttribute)getProcessEquipmentClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessEquipmentInformationType() {
		if (processEquipmentInformationTypeEClass == null) {
			processEquipmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(501);
		}
		return processEquipmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessEquipmentInformationType_ApplicationArea() {
        return (EReference)getProcessEquipmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessEquipmentInformationType_DataArea() {
        return (EReference)getProcessEquipmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessEquipmentInformationType_ReleaseID() {
        return (EAttribute)getProcessEquipmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessEquipmentInformationType_VersionID() {
        return (EAttribute)getProcessEquipmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessEquipmentType() {
		if (processEquipmentTypeEClass == null) {
			processEquipmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(502);
		}
		return processEquipmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessEquipmentType_ApplicationArea() {
        return (EReference)getProcessEquipmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessEquipmentType_DataArea() {
        return (EReference)getProcessEquipmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessEquipmentType_ReleaseID() {
        return (EAttribute)getProcessEquipmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessEquipmentType_VersionID() {
        return (EAttribute)getProcessEquipmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessMaterialClassType() {
		if (processMaterialClassTypeEClass == null) {
			processMaterialClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(503);
		}
		return processMaterialClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessMaterialClassType_ApplicationArea() {
        return (EReference)getProcessMaterialClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessMaterialClassType_DataArea() {
        return (EReference)getProcessMaterialClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessMaterialClassType_ReleaseID() {
        return (EAttribute)getProcessMaterialClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessMaterialClassType_VersionID() {
        return (EAttribute)getProcessMaterialClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessMaterialDefinitionType() {
		if (processMaterialDefinitionTypeEClass == null) {
			processMaterialDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(504);
		}
		return processMaterialDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessMaterialDefinitionType_ApplicationArea() {
        return (EReference)getProcessMaterialDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessMaterialDefinitionType_DataArea() {
        return (EReference)getProcessMaterialDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessMaterialDefinitionType_ReleaseID() {
        return (EAttribute)getProcessMaterialDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessMaterialDefinitionType_VersionID() {
        return (EAttribute)getProcessMaterialDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessMaterialInformationType() {
		if (processMaterialInformationTypeEClass == null) {
			processMaterialInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(505);
		}
		return processMaterialInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessMaterialInformationType_ApplicationArea() {
        return (EReference)getProcessMaterialInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessMaterialInformationType_DataArea() {
        return (EReference)getProcessMaterialInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessMaterialInformationType_ReleaseID() {
        return (EAttribute)getProcessMaterialInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessMaterialInformationType_VersionID() {
        return (EAttribute)getProcessMaterialInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessMaterialLotType() {
		if (processMaterialLotTypeEClass == null) {
			processMaterialLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(506);
		}
		return processMaterialLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessMaterialLotType_ApplicationArea() {
        return (EReference)getProcessMaterialLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessMaterialLotType_DataArea() {
        return (EReference)getProcessMaterialLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessMaterialLotType_ReleaseID() {
        return (EAttribute)getProcessMaterialLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessMaterialLotType_VersionID() {
        return (EAttribute)getProcessMaterialLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessMaterialSubLotType() {
		if (processMaterialSubLotTypeEClass == null) {
			processMaterialSubLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(507);
		}
		return processMaterialSubLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessMaterialSubLotType_ApplicationArea() {
        return (EReference)getProcessMaterialSubLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessMaterialSubLotType_DataArea() {
        return (EReference)getProcessMaterialSubLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessMaterialSubLotType_ReleaseID() {
        return (EAttribute)getProcessMaterialSubLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessMaterialSubLotType_VersionID() {
        return (EAttribute)getProcessMaterialSubLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessMaterialTestSpecType() {
		if (processMaterialTestSpecTypeEClass == null) {
			processMaterialTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(508);
		}
		return processMaterialTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessMaterialTestSpecType_ApplicationArea() {
        return (EReference)getProcessMaterialTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessMaterialTestSpecType_DataArea() {
        return (EReference)getProcessMaterialTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessMaterialTestSpecType_ReleaseID() {
        return (EAttribute)getProcessMaterialTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessMaterialTestSpecType_VersionID() {
        return (EAttribute)getProcessMaterialTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessOperationsCapabilityInformationType() {
		if (processOperationsCapabilityInformationTypeEClass == null) {
			processOperationsCapabilityInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(509);
		}
		return processOperationsCapabilityInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessOperationsCapabilityInformationType_ApplicationArea() {
        return (EReference)getProcessOperationsCapabilityInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessOperationsCapabilityInformationType_DataArea() {
        return (EReference)getProcessOperationsCapabilityInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessOperationsCapabilityInformationType_ReleaseID() {
        return (EAttribute)getProcessOperationsCapabilityInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessOperationsCapabilityInformationType_VersionID() {
        return (EAttribute)getProcessOperationsCapabilityInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessOperationsCapabilityType() {
		if (processOperationsCapabilityTypeEClass == null) {
			processOperationsCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(510);
		}
		return processOperationsCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessOperationsCapabilityType_ApplicationArea() {
        return (EReference)getProcessOperationsCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessOperationsCapabilityType_DataArea() {
        return (EReference)getProcessOperationsCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessOperationsCapabilityType_ReleaseID() {
        return (EAttribute)getProcessOperationsCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessOperationsCapabilityType_VersionID() {
        return (EAttribute)getProcessOperationsCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessOperationsDefinitionInformationType() {
		if (processOperationsDefinitionInformationTypeEClass == null) {
			processOperationsDefinitionInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(511);
		}
		return processOperationsDefinitionInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessOperationsDefinitionInformationType_ApplicationArea() {
        return (EReference)getProcessOperationsDefinitionInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessOperationsDefinitionInformationType_DataArea() {
        return (EReference)getProcessOperationsDefinitionInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessOperationsDefinitionInformationType_ReleaseID() {
        return (EAttribute)getProcessOperationsDefinitionInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessOperationsDefinitionInformationType_VersionID() {
        return (EAttribute)getProcessOperationsDefinitionInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessOperationsDefinitionType() {
		if (processOperationsDefinitionTypeEClass == null) {
			processOperationsDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(512);
		}
		return processOperationsDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessOperationsDefinitionType_ApplicationArea() {
        return (EReference)getProcessOperationsDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessOperationsDefinitionType_DataArea() {
        return (EReference)getProcessOperationsDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessOperationsDefinitionType_ReleaseID() {
        return (EAttribute)getProcessOperationsDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessOperationsDefinitionType_VersionID() {
        return (EAttribute)getProcessOperationsDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessOperationsPerformanceType() {
		if (processOperationsPerformanceTypeEClass == null) {
			processOperationsPerformanceTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(513);
		}
		return processOperationsPerformanceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessOperationsPerformanceType_ApplicationArea() {
        return (EReference)getProcessOperationsPerformanceType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessOperationsPerformanceType_DataArea() {
        return (EReference)getProcessOperationsPerformanceType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessOperationsPerformanceType_ReleaseID() {
        return (EAttribute)getProcessOperationsPerformanceType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessOperationsPerformanceType_VersionID() {
        return (EAttribute)getProcessOperationsPerformanceType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessOperationsScheduleType() {
		if (processOperationsScheduleTypeEClass == null) {
			processOperationsScheduleTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(514);
		}
		return processOperationsScheduleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessOperationsScheduleType_ApplicationArea() {
        return (EReference)getProcessOperationsScheduleType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessOperationsScheduleType_DataArea() {
        return (EReference)getProcessOperationsScheduleType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessOperationsScheduleType_ReleaseID() {
        return (EAttribute)getProcessOperationsScheduleType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessOperationsScheduleType_VersionID() {
        return (EAttribute)getProcessOperationsScheduleType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessPersonnelClassType() {
		if (processPersonnelClassTypeEClass == null) {
			processPersonnelClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(515);
		}
		return processPersonnelClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPersonnelClassType_ApplicationArea() {
        return (EReference)getProcessPersonnelClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPersonnelClassType_DataArea() {
        return (EReference)getProcessPersonnelClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPersonnelClassType_ReleaseID() {
        return (EAttribute)getProcessPersonnelClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPersonnelClassType_VersionID() {
        return (EAttribute)getProcessPersonnelClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessPersonnelInformationType() {
		if (processPersonnelInformationTypeEClass == null) {
			processPersonnelInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(516);
		}
		return processPersonnelInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPersonnelInformationType_ApplicationArea() {
        return (EReference)getProcessPersonnelInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPersonnelInformationType_DataArea() {
        return (EReference)getProcessPersonnelInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPersonnelInformationType_ReleaseID() {
        return (EAttribute)getProcessPersonnelInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPersonnelInformationType_VersionID() {
        return (EAttribute)getProcessPersonnelInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessPersonType() {
		if (processPersonTypeEClass == null) {
			processPersonTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(517);
		}
		return processPersonTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPersonType_ApplicationArea() {
        return (EReference)getProcessPersonType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPersonType_DataArea() {
        return (EReference)getProcessPersonType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPersonType_ReleaseID() {
        return (EAttribute)getProcessPersonType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPersonType_VersionID() {
        return (EAttribute)getProcessPersonType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessPhysicalAssetCapabilityTestSpecType() {
		if (processPhysicalAssetCapabilityTestSpecTypeEClass == null) {
			processPhysicalAssetCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(518);
		}
		return processPhysicalAssetCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPhysicalAssetCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getProcessPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPhysicalAssetCapabilityTestSpecType_DataArea() {
        return (EReference)getProcessPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPhysicalAssetCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getProcessPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPhysicalAssetCapabilityTestSpecType_VersionID() {
        return (EAttribute)getProcessPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessPhysicalAssetClassType() {
		if (processPhysicalAssetClassTypeEClass == null) {
			processPhysicalAssetClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(519);
		}
		return processPhysicalAssetClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPhysicalAssetClassType_ApplicationArea() {
        return (EReference)getProcessPhysicalAssetClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPhysicalAssetClassType_DataArea() {
        return (EReference)getProcessPhysicalAssetClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPhysicalAssetClassType_ReleaseID() {
        return (EAttribute)getProcessPhysicalAssetClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPhysicalAssetClassType_VersionID() {
        return (EAttribute)getProcessPhysicalAssetClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessPhysicalAssetInformationType() {
		if (processPhysicalAssetInformationTypeEClass == null) {
			processPhysicalAssetInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(520);
		}
		return processPhysicalAssetInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPhysicalAssetInformationType_ApplicationArea() {
        return (EReference)getProcessPhysicalAssetInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPhysicalAssetInformationType_DataArea() {
        return (EReference)getProcessPhysicalAssetInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPhysicalAssetInformationType_ReleaseID() {
        return (EAttribute)getProcessPhysicalAssetInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPhysicalAssetInformationType_VersionID() {
        return (EAttribute)getProcessPhysicalAssetInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessPhysicalAssetType() {
		if (processPhysicalAssetTypeEClass == null) {
			processPhysicalAssetTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(521);
		}
		return processPhysicalAssetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPhysicalAssetType_ApplicationArea() {
        return (EReference)getProcessPhysicalAssetType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessPhysicalAssetType_DataArea() {
        return (EReference)getProcessPhysicalAssetType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPhysicalAssetType_ReleaseID() {
        return (EAttribute)getProcessPhysicalAssetType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessPhysicalAssetType_VersionID() {
        return (EAttribute)getProcessPhysicalAssetType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessProcessSegmentInformationType() {
		if (processProcessSegmentInformationTypeEClass == null) {
			processProcessSegmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(522);
		}
		return processProcessSegmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessProcessSegmentInformationType_ApplicationArea() {
        return (EReference)getProcessProcessSegmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessProcessSegmentInformationType_DataArea() {
        return (EReference)getProcessProcessSegmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessProcessSegmentInformationType_ReleaseID() {
        return (EAttribute)getProcessProcessSegmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessProcessSegmentInformationType_VersionID() {
        return (EAttribute)getProcessProcessSegmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessProcessSegmentType() {
		if (processProcessSegmentTypeEClass == null) {
			processProcessSegmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(523);
		}
		return processProcessSegmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessProcessSegmentType_ApplicationArea() {
        return (EReference)getProcessProcessSegmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessProcessSegmentType_DataArea() {
        return (EReference)getProcessProcessSegmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessProcessSegmentType_ReleaseID() {
        return (EAttribute)getProcessProcessSegmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessProcessSegmentType_VersionID() {
        return (EAttribute)getProcessProcessSegmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessQualificationTestSpecificationType() {
		if (processQualificationTestSpecificationTypeEClass == null) {
			processQualificationTestSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(524);
		}
		return processQualificationTestSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessQualificationTestSpecificationType_ApplicationArea() {
        return (EReference)getProcessQualificationTestSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessQualificationTestSpecificationType_DataArea() {
        return (EReference)getProcessQualificationTestSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessQualificationTestSpecificationType_ReleaseID() {
        return (EAttribute)getProcessQualificationTestSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessQualificationTestSpecificationType_VersionID() {
        return (EAttribute)getProcessQualificationTestSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessSegmentIDType() {
		if (processSegmentIDTypeEClass == null) {
			processSegmentIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(525);
		}
		return processSegmentIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessSegmentInformationType() {
		if (processSegmentInformationTypeEClass == null) {
			processSegmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(526);
		}
		return processSegmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentInformationType_ID() {
        return (EReference)getProcessSegmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentInformationType_Description() {
        return (EReference)getProcessSegmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentInformationType_Location() {
        return (EReference)getProcessSegmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentInformationType_HierarchyScope() {
        return (EReference)getProcessSegmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentInformationType_PublishedDate() {
        return (EReference)getProcessSegmentInformationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentInformationType_ProcessSegment() {
        return (EReference)getProcessSegmentInformationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessSegmentType() {
		if (processSegmentTypeEClass == null) {
			processSegmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(527);
		}
		return processSegmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_ID() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_Description() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_OperationsType() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_Location() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_HierarchyScope() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_PublishedDate() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessSegmentType_Duration() {
        return (EAttribute)getProcessSegmentType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_PersonnelSegmentSpecification() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_EquipmentSegmentSpecification() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_PhysicalAssetSegmentSpecification() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_MaterialSegmentSpecification() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_Parameter() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_SegmentDependency() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessSegmentType_ProcessSegment() {
        return (EReference)getProcessSegmentType().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProductionRequestIDType() {
		if (productionRequestIDTypeEClass == null) {
			productionRequestIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(528);
		}
		return productionRequestIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProductionScheduleIDType() {
		if (productionScheduleIDTypeEClass == null) {
			productionScheduleIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(529);
		}
		return productionScheduleIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProductProductionRuleIDType() {
		if (productProductionRuleIDTypeEClass == null) {
			productProductionRuleIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(530);
		}
		return productProductionRuleIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProductProductionRuleType() {
		if (productProductionRuleTypeEClass == null) {
			productProductionRuleTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(531);
		}
		return productProductionRuleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProductSegmentIDType() {
		if (productSegmentIDTypeEClass == null) {
			productSegmentIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(532);
		}
		return productSegmentIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPropertyIDType() {
		if (propertyIDTypeEClass == null) {
			propertyIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(533);
		}
		return propertyIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPublishedDateType() {
		if (publishedDateTypeEClass == null) {
			publishedDateTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(534);
		}
		return publishedDateTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQualificationTestSpecificationIDType() {
		if (qualificationTestSpecificationIDTypeEClass == null) {
			qualificationTestSpecificationIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(535);
		}
		return qualificationTestSpecificationIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQualificationTestSpecificationType() {
		if (qualificationTestSpecificationTypeEClass == null) {
			qualificationTestSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(536);
		}
		return qualificationTestSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualificationTestSpecificationType_ID() {
        return (EReference)getQualificationTestSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualificationTestSpecificationType_Description() {
        return (EReference)getQualificationTestSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualificationTestSpecificationType_Version() {
        return (EReference)getQualificationTestSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualificationTestSpecificationType_Location() {
        return (EReference)getQualificationTestSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualificationTestSpecificationType_HierarchyScope() {
        return (EReference)getQualificationTestSpecificationType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualificationTestSpecificationType_TestedPersonProperty() {
        return (EReference)getQualificationTestSpecificationType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualificationTestSpecificationType_TestedPersonnelClassProperty() {
        return (EReference)getQualificationTestSpecificationType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQuantityStringType() {
		if (quantityStringTypeEClass == null) {
			quantityStringTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(537);
		}
		return quantityStringTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQuantityType() {
		if (quantityTypeEClass == null) {
			quantityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(538);
		}
		return quantityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuantityType_Value() {
        return (EAttribute)getQuantityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuantityType_UnitCode() {
        return (EAttribute)getQuantityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuantityType_UnitCodeListAgencyID() {
        return (EAttribute)getQuantityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuantityType_UnitCodeListAgencyName() {
        return (EAttribute)getQuantityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuantityType_UnitCodeListID() {
        return (EAttribute)getQuantityType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQuantityValueType() {
		if (quantityValueTypeEClass == null) {
			quantityValueTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(539);
		}
		return quantityValueTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQuantityValueType_QuantityString() {
        return (EReference)getQuantityValueType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQuantityValueType_DataType() {
        return (EReference)getQuantityValueType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQuantityValueType_UnitOfMeasure() {
        return (EReference)getQuantityValueType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQuantityValueType_Key() {
        return (EReference)getQuantityValueType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReasonType() {
		if (reasonTypeEClass == null) {
			reasonTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(540);
		}
		return reasonTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRelationshipForm1Type() {
		if (relationshipForm1TypeEClass == null) {
			relationshipForm1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(541);
		}
		return relationshipForm1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRelationshipFormType() {
		if (relationshipFormTypeEClass == null) {
			relationshipFormTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(544);
		}
		return relationshipFormTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRelationshipFormType_OtherValue() {
        return (EAttribute)getRelationshipFormType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRelationshipType1Type() {
		if (relationshipType1TypeEClass == null) {
			relationshipType1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(545);
		}
		return relationshipType1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRelationshipTypeType() {
		if (relationshipTypeTypeEClass == null) {
			relationshipTypeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(548);
		}
		return relationshipTypeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRelationshipTypeType_OtherValue() {
        return (EAttribute)getRelationshipTypeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequestedCompletionDateType() {
		if (requestedCompletionDateTypeEClass == null) {
			requestedCompletionDateTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(549);
		}
		return requestedCompletionDateTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequestedPriorityType() {
		if (requestedPriorityTypeEClass == null) {
			requestedPriorityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(550);
		}
		return requestedPriorityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequestState1Type() {
		if (requestState1TypeEClass == null) {
			requestState1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(551);
		}
		return requestState1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequestStateType() {
		if (requestStateTypeEClass == null) {
			requestStateTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(554);
		}
		return requestStateTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRequestStateType_OtherValue() {
        return (EAttribute)getRequestStateType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequiredByRequestedSegmentResponse1Type() {
		if (requiredByRequestedSegmentResponse1TypeEClass == null) {
			requiredByRequestedSegmentResponse1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(555);
		}
		return requiredByRequestedSegmentResponse1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequiredByRequestedSegmentResponseType() {
		if (requiredByRequestedSegmentResponseTypeEClass == null) {
			requiredByRequestedSegmentResponseTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(558);
		}
		return requiredByRequestedSegmentResponseTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRequiredByRequestedSegmentResponseType_OtherValue() {
        return (EAttribute)getRequiredByRequestedSegmentResponseType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourceIDType() {
		if (resourceIDTypeEClass == null) {
			resourceIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(559);
		}
		return resourceIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourceNetworkConnectionIDType() {
		if (resourceNetworkConnectionIDTypeEClass == null) {
			resourceNetworkConnectionIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(560);
		}
		return resourceNetworkConnectionIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourceReferenceType1Type() {
		if (resourceReferenceType1TypeEClass == null) {
			resourceReferenceType1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(561);
		}
		return resourceReferenceType1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourceReferenceTypeType() {
		if (resourceReferenceTypeTypeEClass == null) {
			resourceReferenceTypeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(564);
		}
		return resourceReferenceTypeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResourceReferenceTypeType_OtherValue() {
        return (EAttribute)getResourceReferenceTypeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResourcesType() {
		if (resourcesTypeEClass == null) {
			resourcesTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(565);
		}
		return resourcesTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondEquipmentCapabilityTestSpecType() {
		if (respondEquipmentCapabilityTestSpecTypeEClass == null) {
			respondEquipmentCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(566);
		}
		return respondEquipmentCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondEquipmentCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getRespondEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondEquipmentCapabilityTestSpecType_DataArea() {
        return (EReference)getRespondEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondEquipmentCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getRespondEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondEquipmentCapabilityTestSpecType_VersionID() {
        return (EAttribute)getRespondEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondEquipmentClassType() {
		if (respondEquipmentClassTypeEClass == null) {
			respondEquipmentClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(567);
		}
		return respondEquipmentClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondEquipmentClassType_ApplicationArea() {
        return (EReference)getRespondEquipmentClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondEquipmentClassType_DataArea() {
        return (EReference)getRespondEquipmentClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondEquipmentClassType_ReleaseID() {
        return (EAttribute)getRespondEquipmentClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondEquipmentClassType_VersionID() {
        return (EAttribute)getRespondEquipmentClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondEquipmentInformationType() {
		if (respondEquipmentInformationTypeEClass == null) {
			respondEquipmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(568);
		}
		return respondEquipmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondEquipmentInformationType_ApplicationArea() {
        return (EReference)getRespondEquipmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondEquipmentInformationType_DataArea() {
        return (EReference)getRespondEquipmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondEquipmentInformationType_ReleaseID() {
        return (EAttribute)getRespondEquipmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondEquipmentInformationType_VersionID() {
        return (EAttribute)getRespondEquipmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondEquipmentType() {
		if (respondEquipmentTypeEClass == null) {
			respondEquipmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(569);
		}
		return respondEquipmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondEquipmentType_ApplicationArea() {
        return (EReference)getRespondEquipmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondEquipmentType_DataArea() {
        return (EReference)getRespondEquipmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondEquipmentType_ReleaseID() {
        return (EAttribute)getRespondEquipmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondEquipmentType_VersionID() {
        return (EAttribute)getRespondEquipmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondMaterialClassType() {
		if (respondMaterialClassTypeEClass == null) {
			respondMaterialClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(570);
		}
		return respondMaterialClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondMaterialClassType_ApplicationArea() {
        return (EReference)getRespondMaterialClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondMaterialClassType_DataArea() {
        return (EReference)getRespondMaterialClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondMaterialClassType_ReleaseID() {
        return (EAttribute)getRespondMaterialClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondMaterialClassType_VersionID() {
        return (EAttribute)getRespondMaterialClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondMaterialDefinitionType() {
		if (respondMaterialDefinitionTypeEClass == null) {
			respondMaterialDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(571);
		}
		return respondMaterialDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondMaterialDefinitionType_ApplicationArea() {
        return (EReference)getRespondMaterialDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondMaterialDefinitionType_DataArea() {
        return (EReference)getRespondMaterialDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondMaterialDefinitionType_ReleaseID() {
        return (EAttribute)getRespondMaterialDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondMaterialDefinitionType_VersionID() {
        return (EAttribute)getRespondMaterialDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondMaterialInformationType() {
		if (respondMaterialInformationTypeEClass == null) {
			respondMaterialInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(572);
		}
		return respondMaterialInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondMaterialInformationType_ApplicationArea() {
        return (EReference)getRespondMaterialInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondMaterialInformationType_DataArea() {
        return (EReference)getRespondMaterialInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondMaterialInformationType_ReleaseID() {
        return (EAttribute)getRespondMaterialInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondMaterialInformationType_VersionID() {
        return (EAttribute)getRespondMaterialInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondMaterialLotType() {
		if (respondMaterialLotTypeEClass == null) {
			respondMaterialLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(573);
		}
		return respondMaterialLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondMaterialLotType_ApplicationArea() {
        return (EReference)getRespondMaterialLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondMaterialLotType_DataArea() {
        return (EReference)getRespondMaterialLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondMaterialLotType_ReleaseID() {
        return (EAttribute)getRespondMaterialLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondMaterialLotType_VersionID() {
        return (EAttribute)getRespondMaterialLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondMaterialSubLotType() {
		if (respondMaterialSubLotTypeEClass == null) {
			respondMaterialSubLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(574);
		}
		return respondMaterialSubLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondMaterialSubLotType_ApplicationArea() {
        return (EReference)getRespondMaterialSubLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondMaterialSubLotType_DataArea() {
        return (EReference)getRespondMaterialSubLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondMaterialSubLotType_ReleaseID() {
        return (EAttribute)getRespondMaterialSubLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondMaterialSubLotType_VersionID() {
        return (EAttribute)getRespondMaterialSubLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondMaterialTestSpecType() {
		if (respondMaterialTestSpecTypeEClass == null) {
			respondMaterialTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(575);
		}
		return respondMaterialTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondMaterialTestSpecType_ApplicationArea() {
        return (EReference)getRespondMaterialTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondMaterialTestSpecType_DataArea() {
        return (EReference)getRespondMaterialTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondMaterialTestSpecType_ReleaseID() {
        return (EAttribute)getRespondMaterialTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondMaterialTestSpecType_VersionID() {
        return (EAttribute)getRespondMaterialTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondOperationsCapabilityInformationType() {
		if (respondOperationsCapabilityInformationTypeEClass == null) {
			respondOperationsCapabilityInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(576);
		}
		return respondOperationsCapabilityInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondOperationsCapabilityInformationType_ApplicationArea() {
        return (EReference)getRespondOperationsCapabilityInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondOperationsCapabilityInformationType_DataArea() {
        return (EReference)getRespondOperationsCapabilityInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondOperationsCapabilityInformationType_ReleaseID() {
        return (EAttribute)getRespondOperationsCapabilityInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondOperationsCapabilityInformationType_VersionID() {
        return (EAttribute)getRespondOperationsCapabilityInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondOperationsCapabilityType() {
		if (respondOperationsCapabilityTypeEClass == null) {
			respondOperationsCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(577);
		}
		return respondOperationsCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondOperationsCapabilityType_ApplicationArea() {
        return (EReference)getRespondOperationsCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondOperationsCapabilityType_DataArea() {
        return (EReference)getRespondOperationsCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondOperationsCapabilityType_ReleaseID() {
        return (EAttribute)getRespondOperationsCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondOperationsCapabilityType_VersionID() {
        return (EAttribute)getRespondOperationsCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondOperationsDefinitionInformationType() {
		if (respondOperationsDefinitionInformationTypeEClass == null) {
			respondOperationsDefinitionInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(578);
		}
		return respondOperationsDefinitionInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondOperationsDefinitionInformationType_ApplicationArea() {
        return (EReference)getRespondOperationsDefinitionInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondOperationsDefinitionInformationType_DataArea() {
        return (EReference)getRespondOperationsDefinitionInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondOperationsDefinitionInformationType_ReleaseID() {
        return (EAttribute)getRespondOperationsDefinitionInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondOperationsDefinitionInformationType_VersionID() {
        return (EAttribute)getRespondOperationsDefinitionInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondOperationsDefinitionType() {
		if (respondOperationsDefinitionTypeEClass == null) {
			respondOperationsDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(579);
		}
		return respondOperationsDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondOperationsDefinitionType_ApplicationArea() {
        return (EReference)getRespondOperationsDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondOperationsDefinitionType_DataArea() {
        return (EReference)getRespondOperationsDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondOperationsDefinitionType_ReleaseID() {
        return (EAttribute)getRespondOperationsDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondOperationsDefinitionType_VersionID() {
        return (EAttribute)getRespondOperationsDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondOperationsPerformanceType() {
		if (respondOperationsPerformanceTypeEClass == null) {
			respondOperationsPerformanceTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(580);
		}
		return respondOperationsPerformanceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondOperationsPerformanceType_ApplicationArea() {
        return (EReference)getRespondOperationsPerformanceType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondOperationsPerformanceType_DataArea() {
        return (EReference)getRespondOperationsPerformanceType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondOperationsPerformanceType_ReleaseID() {
        return (EAttribute)getRespondOperationsPerformanceType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondOperationsPerformanceType_VersionID() {
        return (EAttribute)getRespondOperationsPerformanceType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondOperationsScheduleType() {
		if (respondOperationsScheduleTypeEClass == null) {
			respondOperationsScheduleTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(581);
		}
		return respondOperationsScheduleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondOperationsScheduleType_ApplicationArea() {
        return (EReference)getRespondOperationsScheduleType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondOperationsScheduleType_DataArea() {
        return (EReference)getRespondOperationsScheduleType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondOperationsScheduleType_ReleaseID() {
        return (EAttribute)getRespondOperationsScheduleType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondOperationsScheduleType_VersionID() {
        return (EAttribute)getRespondOperationsScheduleType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondPersonnelClassType() {
		if (respondPersonnelClassTypeEClass == null) {
			respondPersonnelClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(582);
		}
		return respondPersonnelClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPersonnelClassType_ApplicationArea() {
        return (EReference)getRespondPersonnelClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPersonnelClassType_DataArea() {
        return (EReference)getRespondPersonnelClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPersonnelClassType_ReleaseID() {
        return (EAttribute)getRespondPersonnelClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPersonnelClassType_VersionID() {
        return (EAttribute)getRespondPersonnelClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondPersonnelInformationType() {
		if (respondPersonnelInformationTypeEClass == null) {
			respondPersonnelInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(583);
		}
		return respondPersonnelInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPersonnelInformationType_ApplicationArea() {
        return (EReference)getRespondPersonnelInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPersonnelInformationType_DataArea() {
        return (EReference)getRespondPersonnelInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPersonnelInformationType_ReleaseID() {
        return (EAttribute)getRespondPersonnelInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPersonnelInformationType_VersionID() {
        return (EAttribute)getRespondPersonnelInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondPersonType() {
		if (respondPersonTypeEClass == null) {
			respondPersonTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(584);
		}
		return respondPersonTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPersonType_ApplicationArea() {
        return (EReference)getRespondPersonType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPersonType_DataArea() {
        return (EReference)getRespondPersonType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPersonType_ReleaseID() {
        return (EAttribute)getRespondPersonType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPersonType_VersionID() {
        return (EAttribute)getRespondPersonType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondPhysicalAssetCapabilityTestSpecType() {
		if (respondPhysicalAssetCapabilityTestSpecTypeEClass == null) {
			respondPhysicalAssetCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(585);
		}
		return respondPhysicalAssetCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPhysicalAssetCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getRespondPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPhysicalAssetCapabilityTestSpecType_DataArea() {
        return (EReference)getRespondPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPhysicalAssetCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getRespondPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPhysicalAssetCapabilityTestSpecType_VersionID() {
        return (EAttribute)getRespondPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondPhysicalAssetClassType() {
		if (respondPhysicalAssetClassTypeEClass == null) {
			respondPhysicalAssetClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(586);
		}
		return respondPhysicalAssetClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPhysicalAssetClassType_ApplicationArea() {
        return (EReference)getRespondPhysicalAssetClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPhysicalAssetClassType_DataArea() {
        return (EReference)getRespondPhysicalAssetClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPhysicalAssetClassType_ReleaseID() {
        return (EAttribute)getRespondPhysicalAssetClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPhysicalAssetClassType_VersionID() {
        return (EAttribute)getRespondPhysicalAssetClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondPhysicalAssetInformationType() {
		if (respondPhysicalAssetInformationTypeEClass == null) {
			respondPhysicalAssetInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(587);
		}
		return respondPhysicalAssetInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPhysicalAssetInformationType_ApplicationArea() {
        return (EReference)getRespondPhysicalAssetInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPhysicalAssetInformationType_DataArea() {
        return (EReference)getRespondPhysicalAssetInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPhysicalAssetInformationType_ReleaseID() {
        return (EAttribute)getRespondPhysicalAssetInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPhysicalAssetInformationType_VersionID() {
        return (EAttribute)getRespondPhysicalAssetInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondPhysicalAssetType() {
		if (respondPhysicalAssetTypeEClass == null) {
			respondPhysicalAssetTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(588);
		}
		return respondPhysicalAssetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPhysicalAssetType_ApplicationArea() {
        return (EReference)getRespondPhysicalAssetType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondPhysicalAssetType_DataArea() {
        return (EReference)getRespondPhysicalAssetType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPhysicalAssetType_ReleaseID() {
        return (EAttribute)getRespondPhysicalAssetType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondPhysicalAssetType_VersionID() {
        return (EAttribute)getRespondPhysicalAssetType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondProcessSegmentInformationType() {
		if (respondProcessSegmentInformationTypeEClass == null) {
			respondProcessSegmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(589);
		}
		return respondProcessSegmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondProcessSegmentInformationType_ApplicationArea() {
        return (EReference)getRespondProcessSegmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondProcessSegmentInformationType_DataArea() {
        return (EReference)getRespondProcessSegmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondProcessSegmentInformationType_ReleaseID() {
        return (EAttribute)getRespondProcessSegmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondProcessSegmentInformationType_VersionID() {
        return (EAttribute)getRespondProcessSegmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondProcessSegmentType() {
		if (respondProcessSegmentTypeEClass == null) {
			respondProcessSegmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(590);
		}
		return respondProcessSegmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondProcessSegmentType_ApplicationArea() {
        return (EReference)getRespondProcessSegmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondProcessSegmentType_DataArea() {
        return (EReference)getRespondProcessSegmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondProcessSegmentType_ReleaseID() {
        return (EAttribute)getRespondProcessSegmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondProcessSegmentType_VersionID() {
        return (EAttribute)getRespondProcessSegmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRespondQualificationTestSpecificationType() {
		if (respondQualificationTestSpecificationTypeEClass == null) {
			respondQualificationTestSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(591);
		}
		return respondQualificationTestSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondQualificationTestSpecificationType_ApplicationArea() {
        return (EReference)getRespondQualificationTestSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRespondQualificationTestSpecificationType_DataArea() {
        return (EReference)getRespondQualificationTestSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondQualificationTestSpecificationType_ReleaseID() {
        return (EAttribute)getRespondQualificationTestSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRespondQualificationTestSpecificationType_VersionID() {
        return (EAttribute)getRespondQualificationTestSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResponseState1Type() {
		if (responseState1TypeEClass == null) {
			responseState1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(592);
		}
		return responseState1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResponseStateType() {
		if (responseStateTypeEClass == null) {
			responseStateTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(595);
		}
		return responseStateTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResponseStateType_OtherValue() {
        return (EAttribute)getResponseStateType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResultType() {
		if (resultTypeEClass == null) {
			resultTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(596);
		}
		return resultTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResultType_ValueString() {
        return (EReference)getResultType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResultType_DataType() {
        return (EReference)getResultType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResultType_UnitOfMeasure() {
        return (EReference)getResultType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResultType_Key() {
        return (EReference)getResultType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSegmentDependencyType() {
		if (segmentDependencyTypeEClass == null) {
			segmentDependencyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(597);
		}
		return segmentDependencyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentDependencyType_ID() {
        return (EReference)getSegmentDependencyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentDependencyType_Description() {
        return (EReference)getSegmentDependencyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentDependencyType_Dependency() {
        return (EReference)getSegmentDependencyType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentDependencyType_TimingFactor() {
        return (EReference)getSegmentDependencyType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSegmentDependencyType_Group() {
        return (EAttribute)getSegmentDependencyType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentDependencyType_ProductSegmentID() {
        return (EReference)getSegmentDependencyType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentDependencyType_ProcessSegmentID() {
        return (EReference)getSegmentDependencyType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSegmentDependencyType_SegmentID() {
        return (EReference)getSegmentDependencyType().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowEquipmentCapabilityTestSpecType() {
		if (showEquipmentCapabilityTestSpecTypeEClass == null) {
			showEquipmentCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(598);
		}
		return showEquipmentCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowEquipmentCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getShowEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowEquipmentCapabilityTestSpecType_DataArea() {
        return (EReference)getShowEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowEquipmentCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getShowEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowEquipmentCapabilityTestSpecType_VersionID() {
        return (EAttribute)getShowEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowEquipmentClassType() {
		if (showEquipmentClassTypeEClass == null) {
			showEquipmentClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(599);
		}
		return showEquipmentClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowEquipmentClassType_ApplicationArea() {
        return (EReference)getShowEquipmentClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowEquipmentClassType_DataArea() {
        return (EReference)getShowEquipmentClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowEquipmentClassType_ReleaseID() {
        return (EAttribute)getShowEquipmentClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowEquipmentClassType_VersionID() {
        return (EAttribute)getShowEquipmentClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowEquipmentInformationType() {
		if (showEquipmentInformationTypeEClass == null) {
			showEquipmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(600);
		}
		return showEquipmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowEquipmentInformationType_ApplicationArea() {
        return (EReference)getShowEquipmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowEquipmentInformationType_DataArea() {
        return (EReference)getShowEquipmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowEquipmentInformationType_ReleaseID() {
        return (EAttribute)getShowEquipmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowEquipmentInformationType_VersionID() {
        return (EAttribute)getShowEquipmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowEquipmentType() {
		if (showEquipmentTypeEClass == null) {
			showEquipmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(601);
		}
		return showEquipmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowEquipmentType_ApplicationArea() {
        return (EReference)getShowEquipmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowEquipmentType_DataArea() {
        return (EReference)getShowEquipmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowEquipmentType_ReleaseID() {
        return (EAttribute)getShowEquipmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowEquipmentType_VersionID() {
        return (EAttribute)getShowEquipmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowMaterialClassType() {
		if (showMaterialClassTypeEClass == null) {
			showMaterialClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(602);
		}
		return showMaterialClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowMaterialClassType_ApplicationArea() {
        return (EReference)getShowMaterialClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowMaterialClassType_DataArea() {
        return (EReference)getShowMaterialClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowMaterialClassType_ReleaseID() {
        return (EAttribute)getShowMaterialClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowMaterialClassType_VersionID() {
        return (EAttribute)getShowMaterialClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowMaterialDefinitionType() {
		if (showMaterialDefinitionTypeEClass == null) {
			showMaterialDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(603);
		}
		return showMaterialDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowMaterialDefinitionType_ApplicationArea() {
        return (EReference)getShowMaterialDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowMaterialDefinitionType_DataArea() {
        return (EReference)getShowMaterialDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowMaterialDefinitionType_ReleaseID() {
        return (EAttribute)getShowMaterialDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowMaterialDefinitionType_VersionID() {
        return (EAttribute)getShowMaterialDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowMaterialInformationType() {
		if (showMaterialInformationTypeEClass == null) {
			showMaterialInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(604);
		}
		return showMaterialInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowMaterialInformationType_ApplicationArea() {
        return (EReference)getShowMaterialInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowMaterialInformationType_DataArea() {
        return (EReference)getShowMaterialInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowMaterialInformationType_ReleaseID() {
        return (EAttribute)getShowMaterialInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowMaterialInformationType_VersionID() {
        return (EAttribute)getShowMaterialInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowMaterialLotType() {
		if (showMaterialLotTypeEClass == null) {
			showMaterialLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(605);
		}
		return showMaterialLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowMaterialLotType_ApplicationArea() {
        return (EReference)getShowMaterialLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowMaterialLotType_DataArea() {
        return (EReference)getShowMaterialLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowMaterialLotType_ReleaseID() {
        return (EAttribute)getShowMaterialLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowMaterialLotType_VersionID() {
        return (EAttribute)getShowMaterialLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowMaterialSubLotType() {
		if (showMaterialSubLotTypeEClass == null) {
			showMaterialSubLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(606);
		}
		return showMaterialSubLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowMaterialSubLotType_ApplicationArea() {
        return (EReference)getShowMaterialSubLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowMaterialSubLotType_DataArea() {
        return (EReference)getShowMaterialSubLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowMaterialSubLotType_ReleaseID() {
        return (EAttribute)getShowMaterialSubLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowMaterialSubLotType_VersionID() {
        return (EAttribute)getShowMaterialSubLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowMaterialTestSpecType() {
		if (showMaterialTestSpecTypeEClass == null) {
			showMaterialTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(607);
		}
		return showMaterialTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowMaterialTestSpecType_ApplicationArea() {
        return (EReference)getShowMaterialTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowMaterialTestSpecType_DataArea() {
        return (EReference)getShowMaterialTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowMaterialTestSpecType_ReleaseID() {
        return (EAttribute)getShowMaterialTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowMaterialTestSpecType_VersionID() {
        return (EAttribute)getShowMaterialTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowOperationsCapabilityInformationType() {
		if (showOperationsCapabilityInformationTypeEClass == null) {
			showOperationsCapabilityInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(608);
		}
		return showOperationsCapabilityInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowOperationsCapabilityInformationType_ApplicationArea() {
        return (EReference)getShowOperationsCapabilityInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowOperationsCapabilityInformationType_DataArea() {
        return (EReference)getShowOperationsCapabilityInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowOperationsCapabilityInformationType_ReleaseID() {
        return (EAttribute)getShowOperationsCapabilityInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowOperationsCapabilityInformationType_VersionID() {
        return (EAttribute)getShowOperationsCapabilityInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowOperationsCapabilityType() {
		if (showOperationsCapabilityTypeEClass == null) {
			showOperationsCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(609);
		}
		return showOperationsCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowOperationsCapabilityType_ApplicationArea() {
        return (EReference)getShowOperationsCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowOperationsCapabilityType_DataArea() {
        return (EReference)getShowOperationsCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowOperationsCapabilityType_ReleaseID() {
        return (EAttribute)getShowOperationsCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowOperationsCapabilityType_VersionID() {
        return (EAttribute)getShowOperationsCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowOperationsDefinitionInformationType() {
		if (showOperationsDefinitionInformationTypeEClass == null) {
			showOperationsDefinitionInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(610);
		}
		return showOperationsDefinitionInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowOperationsDefinitionInformationType_ApplicationArea() {
        return (EReference)getShowOperationsDefinitionInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowOperationsDefinitionInformationType_DataArea() {
        return (EReference)getShowOperationsDefinitionInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowOperationsDefinitionInformationType_ReleaseID() {
        return (EAttribute)getShowOperationsDefinitionInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowOperationsDefinitionInformationType_VersionID() {
        return (EAttribute)getShowOperationsDefinitionInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowOperationsDefinitionType() {
		if (showOperationsDefinitionTypeEClass == null) {
			showOperationsDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(611);
		}
		return showOperationsDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowOperationsDefinitionType_ApplicationArea() {
        return (EReference)getShowOperationsDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowOperationsDefinitionType_DataArea() {
        return (EReference)getShowOperationsDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowOperationsDefinitionType_ReleaseID() {
        return (EAttribute)getShowOperationsDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowOperationsDefinitionType_VersionID() {
        return (EAttribute)getShowOperationsDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowOperationsPerformanceType() {
		if (showOperationsPerformanceTypeEClass == null) {
			showOperationsPerformanceTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(612);
		}
		return showOperationsPerformanceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowOperationsPerformanceType_ApplicationArea() {
        return (EReference)getShowOperationsPerformanceType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowOperationsPerformanceType_DataArea() {
        return (EReference)getShowOperationsPerformanceType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowOperationsPerformanceType_ReleaseID() {
        return (EAttribute)getShowOperationsPerformanceType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowOperationsPerformanceType_VersionID() {
        return (EAttribute)getShowOperationsPerformanceType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowOperationsScheduleType() {
		if (showOperationsScheduleTypeEClass == null) {
			showOperationsScheduleTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(613);
		}
		return showOperationsScheduleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowOperationsScheduleType_ApplicationArea() {
        return (EReference)getShowOperationsScheduleType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowOperationsScheduleType_DataArea() {
        return (EReference)getShowOperationsScheduleType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowOperationsScheduleType_ReleaseID() {
        return (EAttribute)getShowOperationsScheduleType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowOperationsScheduleType_VersionID() {
        return (EAttribute)getShowOperationsScheduleType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowPersonnelClassType() {
		if (showPersonnelClassTypeEClass == null) {
			showPersonnelClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(614);
		}
		return showPersonnelClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPersonnelClassType_ApplicationArea() {
        return (EReference)getShowPersonnelClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPersonnelClassType_DataArea() {
        return (EReference)getShowPersonnelClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPersonnelClassType_ReleaseID() {
        return (EAttribute)getShowPersonnelClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPersonnelClassType_VersionID() {
        return (EAttribute)getShowPersonnelClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowPersonnelInformationType() {
		if (showPersonnelInformationTypeEClass == null) {
			showPersonnelInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(615);
		}
		return showPersonnelInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPersonnelInformationType_ApplicationArea() {
        return (EReference)getShowPersonnelInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPersonnelInformationType_DataArea() {
        return (EReference)getShowPersonnelInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPersonnelInformationType_ReleaseID() {
        return (EAttribute)getShowPersonnelInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPersonnelInformationType_VersionID() {
        return (EAttribute)getShowPersonnelInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowPersonType() {
		if (showPersonTypeEClass == null) {
			showPersonTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(616);
		}
		return showPersonTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPersonType_ApplicationArea() {
        return (EReference)getShowPersonType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPersonType_DataArea() {
        return (EReference)getShowPersonType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPersonType_ReleaseID() {
        return (EAttribute)getShowPersonType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPersonType_VersionID() {
        return (EAttribute)getShowPersonType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowPhysicalAssetCapabilityTestSpecType() {
		if (showPhysicalAssetCapabilityTestSpecTypeEClass == null) {
			showPhysicalAssetCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(617);
		}
		return showPhysicalAssetCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPhysicalAssetCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getShowPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPhysicalAssetCapabilityTestSpecType_DataArea() {
        return (EReference)getShowPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPhysicalAssetCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getShowPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPhysicalAssetCapabilityTestSpecType_VersionID() {
        return (EAttribute)getShowPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowPhysicalAssetClassType() {
		if (showPhysicalAssetClassTypeEClass == null) {
			showPhysicalAssetClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(618);
		}
		return showPhysicalAssetClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPhysicalAssetClassType_ApplicationArea() {
        return (EReference)getShowPhysicalAssetClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPhysicalAssetClassType_DataArea() {
        return (EReference)getShowPhysicalAssetClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPhysicalAssetClassType_ReleaseID() {
        return (EAttribute)getShowPhysicalAssetClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPhysicalAssetClassType_VersionID() {
        return (EAttribute)getShowPhysicalAssetClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowPhysicalAssetInformationType() {
		if (showPhysicalAssetInformationTypeEClass == null) {
			showPhysicalAssetInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(619);
		}
		return showPhysicalAssetInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPhysicalAssetInformationType_ApplicationArea() {
        return (EReference)getShowPhysicalAssetInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPhysicalAssetInformationType_DataArea() {
        return (EReference)getShowPhysicalAssetInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPhysicalAssetInformationType_ReleaseID() {
        return (EAttribute)getShowPhysicalAssetInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPhysicalAssetInformationType_VersionID() {
        return (EAttribute)getShowPhysicalAssetInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowPhysicalAssetType() {
		if (showPhysicalAssetTypeEClass == null) {
			showPhysicalAssetTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(620);
		}
		return showPhysicalAssetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPhysicalAssetType_ApplicationArea() {
        return (EReference)getShowPhysicalAssetType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowPhysicalAssetType_DataArea() {
        return (EReference)getShowPhysicalAssetType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPhysicalAssetType_ReleaseID() {
        return (EAttribute)getShowPhysicalAssetType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowPhysicalAssetType_VersionID() {
        return (EAttribute)getShowPhysicalAssetType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowProcessSegmentInformationType() {
		if (showProcessSegmentInformationTypeEClass == null) {
			showProcessSegmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(621);
		}
		return showProcessSegmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowProcessSegmentInformationType_ApplicationArea() {
        return (EReference)getShowProcessSegmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowProcessSegmentInformationType_DataArea() {
        return (EReference)getShowProcessSegmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowProcessSegmentInformationType_ReleaseID() {
        return (EAttribute)getShowProcessSegmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowProcessSegmentInformationType_VersionID() {
        return (EAttribute)getShowProcessSegmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowProcessSegmentType() {
		if (showProcessSegmentTypeEClass == null) {
			showProcessSegmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(622);
		}
		return showProcessSegmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowProcessSegmentType_ApplicationArea() {
        return (EReference)getShowProcessSegmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowProcessSegmentType_DataArea() {
        return (EReference)getShowProcessSegmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowProcessSegmentType_ReleaseID() {
        return (EAttribute)getShowProcessSegmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowProcessSegmentType_VersionID() {
        return (EAttribute)getShowProcessSegmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowQualificationTestSpecificationType() {
		if (showQualificationTestSpecificationTypeEClass == null) {
			showQualificationTestSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(623);
		}
		return showQualificationTestSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowQualificationTestSpecificationType_ApplicationArea() {
        return (EReference)getShowQualificationTestSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShowQualificationTestSpecificationType_DataArea() {
        return (EReference)getShowQualificationTestSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowQualificationTestSpecificationType_ReleaseID() {
        return (EAttribute)getShowQualificationTestSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowQualificationTestSpecificationType_VersionID() {
        return (EAttribute)getShowQualificationTestSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStartTimeType() {
		if (startTimeTypeEClass == null) {
			startTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(624);
		}
		return startTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStatusTimeType() {
		if (statusTimeTypeEClass == null) {
			statusTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(625);
		}
		return statusTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStatusType() {
		if (statusTypeEClass == null) {
			statusTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(626);
		}
		return statusTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStorageHierarchyScopeType() {
		if (storageHierarchyScopeTypeEClass == null) {
			storageHierarchyScopeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(627);
		}
		return storageHierarchyScopeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStorageLocationType() {
		if (storageLocationTypeEClass == null) {
			storageLocationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(628);
		}
		return storageLocationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncEquipmentCapabilityTestSpecType() {
		if (syncEquipmentCapabilityTestSpecTypeEClass == null) {
			syncEquipmentCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(629);
		}
		return syncEquipmentCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncEquipmentCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getSyncEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncEquipmentCapabilityTestSpecType_DataArea() {
        return (EReference)getSyncEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncEquipmentCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getSyncEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncEquipmentCapabilityTestSpecType_VersionID() {
        return (EAttribute)getSyncEquipmentCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncEquipmentClassType() {
		if (syncEquipmentClassTypeEClass == null) {
			syncEquipmentClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(630);
		}
		return syncEquipmentClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncEquipmentClassType_ApplicationArea() {
        return (EReference)getSyncEquipmentClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncEquipmentClassType_DataArea() {
        return (EReference)getSyncEquipmentClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncEquipmentClassType_ReleaseID() {
        return (EAttribute)getSyncEquipmentClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncEquipmentClassType_VersionID() {
        return (EAttribute)getSyncEquipmentClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncEquipmentInformationType() {
		if (syncEquipmentInformationTypeEClass == null) {
			syncEquipmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(631);
		}
		return syncEquipmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncEquipmentInformationType_ApplicationArea() {
        return (EReference)getSyncEquipmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncEquipmentInformationType_DataArea() {
        return (EReference)getSyncEquipmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncEquipmentInformationType_ReleaseID() {
        return (EAttribute)getSyncEquipmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncEquipmentInformationType_VersionID() {
        return (EAttribute)getSyncEquipmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncEquipmentType() {
		if (syncEquipmentTypeEClass == null) {
			syncEquipmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(632);
		}
		return syncEquipmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncEquipmentType_ApplicationArea() {
        return (EReference)getSyncEquipmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncEquipmentType_DataArea() {
        return (EReference)getSyncEquipmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncEquipmentType_ReleaseID() {
        return (EAttribute)getSyncEquipmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncEquipmentType_VersionID() {
        return (EAttribute)getSyncEquipmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncMaterialClassType() {
		if (syncMaterialClassTypeEClass == null) {
			syncMaterialClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(633);
		}
		return syncMaterialClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncMaterialClassType_ApplicationArea() {
        return (EReference)getSyncMaterialClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncMaterialClassType_DataArea() {
        return (EReference)getSyncMaterialClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncMaterialClassType_ReleaseID() {
        return (EAttribute)getSyncMaterialClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncMaterialClassType_VersionID() {
        return (EAttribute)getSyncMaterialClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncMaterialDefinitionType() {
		if (syncMaterialDefinitionTypeEClass == null) {
			syncMaterialDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(634);
		}
		return syncMaterialDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncMaterialDefinitionType_ApplicationArea() {
        return (EReference)getSyncMaterialDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncMaterialDefinitionType_DataArea() {
        return (EReference)getSyncMaterialDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncMaterialDefinitionType_ReleaseID() {
        return (EAttribute)getSyncMaterialDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncMaterialDefinitionType_VersionID() {
        return (EAttribute)getSyncMaterialDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncMaterialInformationType() {
		if (syncMaterialInformationTypeEClass == null) {
			syncMaterialInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(635);
		}
		return syncMaterialInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncMaterialInformationType_ApplicationArea() {
        return (EReference)getSyncMaterialInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncMaterialInformationType_DataArea() {
        return (EReference)getSyncMaterialInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncMaterialInformationType_ReleaseID() {
        return (EAttribute)getSyncMaterialInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncMaterialInformationType_VersionID() {
        return (EAttribute)getSyncMaterialInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncMaterialLotType() {
		if (syncMaterialLotTypeEClass == null) {
			syncMaterialLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(636);
		}
		return syncMaterialLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncMaterialLotType_ApplicationArea() {
        return (EReference)getSyncMaterialLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncMaterialLotType_DataArea() {
        return (EReference)getSyncMaterialLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncMaterialLotType_ReleaseID() {
        return (EAttribute)getSyncMaterialLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncMaterialLotType_VersionID() {
        return (EAttribute)getSyncMaterialLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncMaterialSubLotType() {
		if (syncMaterialSubLotTypeEClass == null) {
			syncMaterialSubLotTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(637);
		}
		return syncMaterialSubLotTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncMaterialSubLotType_ApplicationArea() {
        return (EReference)getSyncMaterialSubLotType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncMaterialSubLotType_DataArea() {
        return (EReference)getSyncMaterialSubLotType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncMaterialSubLotType_ReleaseID() {
        return (EAttribute)getSyncMaterialSubLotType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncMaterialSubLotType_VersionID() {
        return (EAttribute)getSyncMaterialSubLotType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncMaterialTestSpecType() {
		if (syncMaterialTestSpecTypeEClass == null) {
			syncMaterialTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(638);
		}
		return syncMaterialTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncMaterialTestSpecType_ApplicationArea() {
        return (EReference)getSyncMaterialTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncMaterialTestSpecType_DataArea() {
        return (EReference)getSyncMaterialTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncMaterialTestSpecType_ReleaseID() {
        return (EAttribute)getSyncMaterialTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncMaterialTestSpecType_VersionID() {
        return (EAttribute)getSyncMaterialTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncOperationsCapabilityInformationType() {
		if (syncOperationsCapabilityInformationTypeEClass == null) {
			syncOperationsCapabilityInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(639);
		}
		return syncOperationsCapabilityInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncOperationsCapabilityInformationType_ApplicationArea() {
        return (EReference)getSyncOperationsCapabilityInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncOperationsCapabilityInformationType_DataArea() {
        return (EReference)getSyncOperationsCapabilityInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncOperationsCapabilityInformationType_ReleaseID() {
        return (EAttribute)getSyncOperationsCapabilityInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncOperationsCapabilityInformationType_VersionID() {
        return (EAttribute)getSyncOperationsCapabilityInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncOperationsCapabilityType() {
		if (syncOperationsCapabilityTypeEClass == null) {
			syncOperationsCapabilityTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(640);
		}
		return syncOperationsCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncOperationsCapabilityType_ApplicationArea() {
        return (EReference)getSyncOperationsCapabilityType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncOperationsCapabilityType_DataArea() {
        return (EReference)getSyncOperationsCapabilityType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncOperationsCapabilityType_ReleaseID() {
        return (EAttribute)getSyncOperationsCapabilityType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncOperationsCapabilityType_VersionID() {
        return (EAttribute)getSyncOperationsCapabilityType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncOperationsDefinitionInformationType() {
		if (syncOperationsDefinitionInformationTypeEClass == null) {
			syncOperationsDefinitionInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(641);
		}
		return syncOperationsDefinitionInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncOperationsDefinitionInformationType_ApplicationArea() {
        return (EReference)getSyncOperationsDefinitionInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncOperationsDefinitionInformationType_DataArea() {
        return (EReference)getSyncOperationsDefinitionInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncOperationsDefinitionInformationType_ReleaseID() {
        return (EAttribute)getSyncOperationsDefinitionInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncOperationsDefinitionInformationType_VersionID() {
        return (EAttribute)getSyncOperationsDefinitionInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncOperationsDefinitionType() {
		if (syncOperationsDefinitionTypeEClass == null) {
			syncOperationsDefinitionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(642);
		}
		return syncOperationsDefinitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncOperationsDefinitionType_ApplicationArea() {
        return (EReference)getSyncOperationsDefinitionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncOperationsDefinitionType_DataArea() {
        return (EReference)getSyncOperationsDefinitionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncOperationsDefinitionType_ReleaseID() {
        return (EAttribute)getSyncOperationsDefinitionType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncOperationsDefinitionType_VersionID() {
        return (EAttribute)getSyncOperationsDefinitionType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncOperationsPerformanceType() {
		if (syncOperationsPerformanceTypeEClass == null) {
			syncOperationsPerformanceTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(643);
		}
		return syncOperationsPerformanceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncOperationsPerformanceType_ApplicationArea() {
        return (EReference)getSyncOperationsPerformanceType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncOperationsPerformanceType_DataArea() {
        return (EReference)getSyncOperationsPerformanceType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncOperationsPerformanceType_ReleaseID() {
        return (EAttribute)getSyncOperationsPerformanceType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncOperationsPerformanceType_VersionID() {
        return (EAttribute)getSyncOperationsPerformanceType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncOperationsScheduleType() {
		if (syncOperationsScheduleTypeEClass == null) {
			syncOperationsScheduleTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(644);
		}
		return syncOperationsScheduleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncOperationsScheduleType_ApplicationArea() {
        return (EReference)getSyncOperationsScheduleType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncOperationsScheduleType_DataArea() {
        return (EReference)getSyncOperationsScheduleType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncOperationsScheduleType_ReleaseID() {
        return (EAttribute)getSyncOperationsScheduleType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncOperationsScheduleType_VersionID() {
        return (EAttribute)getSyncOperationsScheduleType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncPersonnelClassType() {
		if (syncPersonnelClassTypeEClass == null) {
			syncPersonnelClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(645);
		}
		return syncPersonnelClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPersonnelClassType_ApplicationArea() {
        return (EReference)getSyncPersonnelClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPersonnelClassType_DataArea() {
        return (EReference)getSyncPersonnelClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPersonnelClassType_ReleaseID() {
        return (EAttribute)getSyncPersonnelClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPersonnelClassType_VersionID() {
        return (EAttribute)getSyncPersonnelClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncPersonnelInformationType() {
		if (syncPersonnelInformationTypeEClass == null) {
			syncPersonnelInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(646);
		}
		return syncPersonnelInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPersonnelInformationType_ApplicationArea() {
        return (EReference)getSyncPersonnelInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPersonnelInformationType_DataArea() {
        return (EReference)getSyncPersonnelInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPersonnelInformationType_ReleaseID() {
        return (EAttribute)getSyncPersonnelInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPersonnelInformationType_VersionID() {
        return (EAttribute)getSyncPersonnelInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncPersonType() {
		if (syncPersonTypeEClass == null) {
			syncPersonTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(647);
		}
		return syncPersonTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPersonType_ApplicationArea() {
        return (EReference)getSyncPersonType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPersonType_DataArea() {
        return (EReference)getSyncPersonType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPersonType_ReleaseID() {
        return (EAttribute)getSyncPersonType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPersonType_VersionID() {
        return (EAttribute)getSyncPersonType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncPhysicalAssetCapabilityTestSpecType() {
		if (syncPhysicalAssetCapabilityTestSpecTypeEClass == null) {
			syncPhysicalAssetCapabilityTestSpecTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(648);
		}
		return syncPhysicalAssetCapabilityTestSpecTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPhysicalAssetCapabilityTestSpecType_ApplicationArea() {
        return (EReference)getSyncPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPhysicalAssetCapabilityTestSpecType_DataArea() {
        return (EReference)getSyncPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPhysicalAssetCapabilityTestSpecType_ReleaseID() {
        return (EAttribute)getSyncPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPhysicalAssetCapabilityTestSpecType_VersionID() {
        return (EAttribute)getSyncPhysicalAssetCapabilityTestSpecType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncPhysicalAssetClassType() {
		if (syncPhysicalAssetClassTypeEClass == null) {
			syncPhysicalAssetClassTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(649);
		}
		return syncPhysicalAssetClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPhysicalAssetClassType_ApplicationArea() {
        return (EReference)getSyncPhysicalAssetClassType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPhysicalAssetClassType_DataArea() {
        return (EReference)getSyncPhysicalAssetClassType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPhysicalAssetClassType_ReleaseID() {
        return (EAttribute)getSyncPhysicalAssetClassType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPhysicalAssetClassType_VersionID() {
        return (EAttribute)getSyncPhysicalAssetClassType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncPhysicalAssetInformationType() {
		if (syncPhysicalAssetInformationTypeEClass == null) {
			syncPhysicalAssetInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(650);
		}
		return syncPhysicalAssetInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPhysicalAssetInformationType_ApplicationArea() {
        return (EReference)getSyncPhysicalAssetInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPhysicalAssetInformationType_DataArea() {
        return (EReference)getSyncPhysicalAssetInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPhysicalAssetInformationType_ReleaseID() {
        return (EAttribute)getSyncPhysicalAssetInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPhysicalAssetInformationType_VersionID() {
        return (EAttribute)getSyncPhysicalAssetInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncPhysicalAssetType() {
		if (syncPhysicalAssetTypeEClass == null) {
			syncPhysicalAssetTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(651);
		}
		return syncPhysicalAssetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPhysicalAssetType_ApplicationArea() {
        return (EReference)getSyncPhysicalAssetType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncPhysicalAssetType_DataArea() {
        return (EReference)getSyncPhysicalAssetType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPhysicalAssetType_ReleaseID() {
        return (EAttribute)getSyncPhysicalAssetType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncPhysicalAssetType_VersionID() {
        return (EAttribute)getSyncPhysicalAssetType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncProcessSegmentInformationType() {
		if (syncProcessSegmentInformationTypeEClass == null) {
			syncProcessSegmentInformationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(652);
		}
		return syncProcessSegmentInformationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncProcessSegmentInformationType_ApplicationArea() {
        return (EReference)getSyncProcessSegmentInformationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncProcessSegmentInformationType_DataArea() {
        return (EReference)getSyncProcessSegmentInformationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncProcessSegmentInformationType_ReleaseID() {
        return (EAttribute)getSyncProcessSegmentInformationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncProcessSegmentInformationType_VersionID() {
        return (EAttribute)getSyncProcessSegmentInformationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncProcessSegmentType() {
		if (syncProcessSegmentTypeEClass == null) {
			syncProcessSegmentTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(653);
		}
		return syncProcessSegmentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncProcessSegmentType_ApplicationArea() {
        return (EReference)getSyncProcessSegmentType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncProcessSegmentType_DataArea() {
        return (EReference)getSyncProcessSegmentType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncProcessSegmentType_ReleaseID() {
        return (EAttribute)getSyncProcessSegmentType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncProcessSegmentType_VersionID() {
        return (EAttribute)getSyncProcessSegmentType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncQualificationTestSpecificationType() {
		if (syncQualificationTestSpecificationTypeEClass == null) {
			syncQualificationTestSpecificationTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(654);
		}
		return syncQualificationTestSpecificationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncQualificationTestSpecificationType_ApplicationArea() {
        return (EReference)getSyncQualificationTestSpecificationType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncQualificationTestSpecificationType_DataArea() {
        return (EReference)getSyncQualificationTestSpecificationType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncQualificationTestSpecificationType_ReleaseID() {
        return (EAttribute)getSyncQualificationTestSpecificationType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncQualificationTestSpecificationType_VersionID() {
        return (EAttribute)getSyncQualificationTestSpecificationType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestDateTimeType() {
		if (testDateTimeTypeEClass == null) {
			testDateTimeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(655);
		}
		return testDateTimeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestedEquipmentClassPropertyType() {
		if (testedEquipmentClassPropertyTypeEClass == null) {
			testedEquipmentClassPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(656);
		}
		return testedEquipmentClassPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedEquipmentClassPropertyType_EquipmentClassID() {
        return (EReference)getTestedEquipmentClassPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedEquipmentClassPropertyType_PropertyID() {
        return (EReference)getTestedEquipmentClassPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestedEquipmentPropertyType() {
		if (testedEquipmentPropertyTypeEClass == null) {
			testedEquipmentPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(657);
		}
		return testedEquipmentPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedEquipmentPropertyType_EquipmentID() {
        return (EReference)getTestedEquipmentPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedEquipmentPropertyType_PropertyID() {
        return (EReference)getTestedEquipmentPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestedMaterialClassPropertyType() {
		if (testedMaterialClassPropertyTypeEClass == null) {
			testedMaterialClassPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(658);
		}
		return testedMaterialClassPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedMaterialClassPropertyType_MaterialClassID() {
        return (EReference)getTestedMaterialClassPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedMaterialClassPropertyType_PropertyID() {
        return (EReference)getTestedMaterialClassPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestedMaterialDefinitionPropertyType() {
		if (testedMaterialDefinitionPropertyTypeEClass == null) {
			testedMaterialDefinitionPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(659);
		}
		return testedMaterialDefinitionPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedMaterialDefinitionPropertyType_MaterialDefinitionID() {
        return (EReference)getTestedMaterialDefinitionPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedMaterialDefinitionPropertyType_PropertyID() {
        return (EReference)getTestedMaterialDefinitionPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestedMaterialLotPropertyType() {
		if (testedMaterialLotPropertyTypeEClass == null) {
			testedMaterialLotPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(660);
		}
		return testedMaterialLotPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedMaterialLotPropertyType_MaterialLotID() {
        return (EReference)getTestedMaterialLotPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedMaterialLotPropertyType_PropertyID() {
        return (EReference)getTestedMaterialLotPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestedPersonnelClassPropertyType() {
		if (testedPersonnelClassPropertyTypeEClass == null) {
			testedPersonnelClassPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(661);
		}
		return testedPersonnelClassPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedPersonnelClassPropertyType_PersonnelClassID() {
        return (EReference)getTestedPersonnelClassPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedPersonnelClassPropertyType_PropertyID() {
        return (EReference)getTestedPersonnelClassPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestedPersonPropertyType() {
		if (testedPersonPropertyTypeEClass == null) {
			testedPersonPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(662);
		}
		return testedPersonPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedPersonPropertyType_PersonID() {
        return (EReference)getTestedPersonPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedPersonPropertyType_PropertyID() {
        return (EReference)getTestedPersonPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestedPhysicalAssetClassPropertyType() {
		if (testedPhysicalAssetClassPropertyTypeEClass == null) {
			testedPhysicalAssetClassPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(663);
		}
		return testedPhysicalAssetClassPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedPhysicalAssetClassPropertyType_PhysicalAssetClassID() {
        return (EReference)getTestedPhysicalAssetClassPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedPhysicalAssetClassPropertyType_PropertyID() {
        return (EReference)getTestedPhysicalAssetClassPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestedPhysicalAssetPropertyType() {
		if (testedPhysicalAssetPropertyTypeEClass == null) {
			testedPhysicalAssetPropertyTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(664);
		}
		return testedPhysicalAssetPropertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedPhysicalAssetPropertyType_PhysicalAssetID() {
        return (EReference)getTestedPhysicalAssetPropertyType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestedPhysicalAssetPropertyType_PropertyID() {
        return (EReference)getTestedPhysicalAssetPropertyType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestResultType() {
		if (testResultTypeEClass == null) {
			testResultTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(665);
		}
		return testResultTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestResultType_ID() {
        return (EReference)getTestResultType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestResultType_Description() {
        return (EReference)getTestResultType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestResultType_TestDateTime() {
        return (EReference)getTestResultType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestResultType_Result() {
        return (EReference)getTestResultType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestResultType_ExpirationTime() {
        return (EReference)getTestResultType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTextType() {
		if (textTypeEClass == null) {
			textTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(666);
		}
		return textTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTextType_Value() {
        return (EAttribute)getTextType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTextType_LanguageID() {
        return (EAttribute)getTextType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransAcknowledgeType() {
		if (transAcknowledgeTypeEClass == null) {
			transAcknowledgeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(667);
		}
		return transAcknowledgeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransAcknowledgeType_OriginalApplicationArea() {
        return (EReference)getTransAcknowledgeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransAcknowledgeType_ResponseCriteria() {
        return (EReference)getTransAcknowledgeType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransActionCriteriaType() {
		if (transActionCriteriaTypeEClass == null) {
			transActionCriteriaTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(671);
		}
		return transActionCriteriaTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransActionCriteriaType_ActionExpression() {
        return (EReference)getTransActionCriteriaType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransActionCriteriaType_ChangeStatus() {
        return (EReference)getTransActionCriteriaType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransApplicationAreaType() {
		if (transApplicationAreaTypeEClass == null) {
			transApplicationAreaTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(672);
		}
		return transApplicationAreaTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransApplicationAreaType_Sender() {
        return (EReference)getTransApplicationAreaType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransApplicationAreaType_Receiver() {
        return (EReference)getTransApplicationAreaType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransApplicationAreaType_CreationDateTime() {
        return (EReference)getTransApplicationAreaType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransApplicationAreaType_Signature() {
        return (EReference)getTransApplicationAreaType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransApplicationAreaType_BODID() {
        return (EReference)getTransApplicationAreaType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransApplicationAreaType_UserArea() {
        return (EReference)getTransApplicationAreaType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransCancelType() {
		if (transCancelTypeEClass == null) {
			transCancelTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(673);
		}
		return transCancelTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransCancelType_ActionCriteria() {
        return (EReference)getTransCancelType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransChangeStatusType() {
		if (transChangeStatusTypeEClass == null) {
			transChangeStatusTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(674);
		}
		return transChangeStatusTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransChangeStatusType_Code() {
        return (EReference)getTransChangeStatusType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransChangeStatusType_Description() {
        return (EReference)getTransChangeStatusType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransChangeStatusType_EffectiveDateTime() {
        return (EReference)getTransChangeStatusType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransChangeStatusType_ReasonCode() {
        return (EReference)getTransChangeStatusType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransChangeStatusType_Reason() {
        return (EReference)getTransChangeStatusType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransChangeStatusType_StateChange() {
        return (EReference)getTransChangeStatusType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransChangeStatusType_UserArea() {
        return (EReference)getTransChangeStatusType().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransChangeType() {
		if (transChangeTypeEClass == null) {
			transChangeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(675);
		}
		return transChangeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransChangeType_ActionCriteria() {
        return (EReference)getTransChangeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransChangeType_ResponseCode() {
        return (EAttribute)getTransChangeType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransConfirmationCodeType() {
		if (transConfirmationCodeTypeEClass == null) {
			transConfirmationCodeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(676);
		}
		return transConfirmationCodeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransConfirmType() {
		if (transConfirmTypeEClass == null) {
			transConfirmTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(677);
		}
		return transConfirmTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransConfirmType_OriginalApplicationArea() {
        return (EReference)getTransConfirmType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransConfirmType_ResponseCriteria() {
        return (EReference)getTransConfirmType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransExpression1Type() {
		if (transExpression1TypeEClass == null) {
			transExpression1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(678);
		}
		return transExpression1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransExpression1Type_Value() {
        return (EAttribute)getTransExpression1Type().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransExpressionType() {
		if (transExpressionTypeEClass == null) {
			transExpressionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(679);
		}
		return transExpressionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransExpressionType_ActionCode() {
        return (EAttribute)getTransExpressionType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransExpressionType_ExpressionLanguage() {
        return (EAttribute)getTransExpressionType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransGetType() {
		if (transGetTypeEClass == null) {
			transGetTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(680);
		}
		return transGetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransGetType_Expression() {
        return (EAttribute)getTransGetType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransProcessType() {
		if (transProcessTypeEClass == null) {
			transProcessTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(681);
		}
		return transProcessTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransProcessType_ActionCriteria() {
        return (EReference)getTransProcessType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransProcessType_AcknowledgeCode() {
        return (EAttribute)getTransProcessType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransReceiverType() {
		if (transReceiverTypeEClass == null) {
			transReceiverTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(682);
		}
		return transReceiverTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransReceiverType_LogicalID() {
        return (EReference)getTransReceiverType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransReceiverType_ComponentID() {
        return (EReference)getTransReceiverType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransReceiverType_ID() {
        return (EReference)getTransReceiverType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransRespondType() {
		if (transRespondTypeEClass == null) {
			transRespondTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(683);
		}
		return transRespondTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransRespondType_OriginalApplicationArea() {
        return (EReference)getTransRespondType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransRespondType_ResponseCriteria() {
        return (EReference)getTransRespondType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransResponseCriteriaType() {
		if (transResponseCriteriaTypeEClass == null) {
			transResponseCriteriaTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(686);
		}
		return transResponseCriteriaTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransResponseCriteriaType_ResponseExpression() {
        return (EReference)getTransResponseCriteriaType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransResponseCriteriaType_ChangeStatus() {
        return (EReference)getTransResponseCriteriaType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransSenderType() {
		if (transSenderTypeEClass == null) {
			transSenderTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(687);
		}
		return transSenderTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransSenderType_LogicalID() {
        return (EReference)getTransSenderType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransSenderType_ComponentID() {
        return (EReference)getTransSenderType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransSenderType_TaskID() {
        return (EReference)getTransSenderType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransSenderType_ReferenceID() {
        return (EReference)getTransSenderType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransSenderType_ConfirmationCode() {
        return (EReference)getTransSenderType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransSenderType_AuthorizationID() {
        return (EReference)getTransSenderType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransShowType() {
		if (transShowTypeEClass == null) {
			transShowTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(688);
		}
		return transShowTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransShowType_OriginalApplicationArea() {
        return (EReference)getTransShowType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransShowType_ResponseCriteria() {
        return (EReference)getTransShowType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransSignatureType() {
		if (transSignatureTypeEClass == null) {
			transSignatureTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(689);
		}
		return transSignatureTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransSignatureType_Any() {
        return (EAttribute)getTransSignatureType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransSignatureType_QualifyingAgencyID() {
        return (EAttribute)getTransSignatureType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransStateChangeType() {
		if (transStateChangeTypeEClass == null) {
			transStateChangeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(690);
		}
		return transStateChangeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransStateChangeType_FromStateCode() {
        return (EReference)getTransStateChangeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransStateChangeType_ToStateCode() {
        return (EReference)getTransStateChangeType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransStateChangeType_ChangeDateTime() {
        return (EReference)getTransStateChangeType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransStateChangeType_Description() {
        return (EReference)getTransStateChangeType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransStateChangeType_Note() {
        return (EReference)getTransStateChangeType().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransStateChangeType_UserArea() {
        return (EReference)getTransStateChangeType().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransSyncType() {
		if (transSyncTypeEClass == null) {
			transSyncTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(691);
		}
		return transSyncTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransSyncType_ActionCriteria() {
        return (EReference)getTransSyncType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransUserAreaType() {
		if (transUserAreaTypeEClass == null) {
			transUserAreaTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(692);
		}
		return transUserAreaTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransUserAreaType_Any() {
        return (EAttribute)getTransUserAreaType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnitOfMeasureType() {
		if (unitOfMeasureTypeEClass == null) {
			unitOfMeasureTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(693);
		}
		return unitOfMeasureTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueStringType() {
		if (valueStringTypeEClass == null) {
			valueStringTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(694);
		}
		return valueStringTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueType() {
		if (valueTypeEClass == null) {
			valueTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(695);
		}
		return valueTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getValueType_ValueString() {
        return (EReference)getValueType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getValueType_DataType() {
        return (EReference)getValueType().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getValueType_UnitOfMeasure() {
        return (EReference)getValueType().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getValueType_Key() {
        return (EReference)getValueType().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVersionType() {
		if (versionTypeEClass == null) {
			versionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(696);
		}
		return versionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWorkRequestIDType() {
		if (workRequestIDTypeEClass == null) {
			workRequestIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(697);
		}
		return workRequestIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWorkScheduleIDType() {
		if (workScheduleIDTypeEClass == null) {
			workScheduleIDTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(698);
		}
		return workScheduleIDTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWorkType1Type() {
		if (workType1TypeEClass == null) {
			workType1TypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(699);
		}
		return workType1TypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWorkTypeType() {
		if (workTypeTypeEClass == null) {
			workTypeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(702);
		}
		return workTypeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWorkTypeType_OtherValue() {
        return (EAttribute)getWorkTypeType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAssemblyRelationship1TypeBase() {
		if (assemblyRelationship1TypeBaseEEnum == null) {
			assemblyRelationship1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(32);
		}
		return assemblyRelationship1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAssemblyType1TypeBase() {
		if (assemblyType1TypeBaseEEnum == null) {
			assemblyType1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(36);
		}
		return assemblyType1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCapabilityType1TypeBase() {
		if (capabilityType1TypeBaseEEnum == null) {
			capabilityType1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(71);
		}
		return capabilityType1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDataType1TypeBase() {
		if (dataType1TypeBaseEEnum == null) {
			dataType1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(316);
		}
		return dataType1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDependency1TypeBase() {
		if (dependency1TypeBaseEEnum == null) {
			dependency1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(321);
		}
		return dependency1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEquipmentElementLevel1TypeBase() {
		if (equipmentElementLevel1TypeBaseEEnum == null) {
			equipmentElementLevel1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(336);
		}
		return equipmentElementLevel1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getJobOrderCommand1TypeBase() {
		if (jobOrderCommand1TypeBaseEEnum == null) {
			jobOrderCommand1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(378);
		}
		return jobOrderCommand1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMaterialUse1TypeBase() {
		if (materialUse1TypeBaseEEnum == null) {
			materialUse1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(407);
		}
		return materialUse1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOperationsType1TypeBase() {
		if (operationsType1TypeBaseEEnum == null) {
			operationsType1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(437);
		}
		return operationsType1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRelationshipForm1TypeBase() {
		if (relationshipForm1TypeBaseEEnum == null) {
			relationshipForm1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(542);
		}
		return relationshipForm1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRelationshipType1TypeBase() {
		if (relationshipType1TypeBaseEEnum == null) {
			relationshipType1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(546);
		}
		return relationshipType1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRequestState1TypeBase() {
		if (requestState1TypeBaseEEnum == null) {
			requestState1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(552);
		}
		return requestState1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRequiredByRequestedSegmentResponse1TypeBase() {
		if (requiredByRequestedSegmentResponse1TypeBaseEEnum == null) {
			requiredByRequestedSegmentResponse1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(556);
		}
		return requiredByRequestedSegmentResponse1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getResourceReferenceType1TypeBase() {
		if (resourceReferenceType1TypeBaseEEnum == null) {
			resourceReferenceType1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(562);
		}
		return resourceReferenceType1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getResponseState1TypeBase() {
		if (responseState1TypeBaseEEnum == null) {
			responseState1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(593);
		}
		return responseState1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTransActionCodeEnumerationType() {
		if (transActionCodeEnumerationTypeEEnum == null) {
			transActionCodeEnumerationTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(668);
		}
		return transActionCodeEnumerationTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTransResponseCodeType() {
		if (transResponseCodeTypeEEnum == null) {
			transResponseCodeTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(684);
		}
		return transResponseCodeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getWorkType1TypeBase() {
		if (workType1TypeBaseEEnum == null) {
			workType1TypeBaseEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(700);
		}
		return workType1TypeBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAssemblyRelationship1TypeBaseObject() {
		if (assemblyRelationship1TypeBaseObjectEDataType == null) {
			assemblyRelationship1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(33);
		}
		return assemblyRelationship1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAssemblyType1TypeBaseObject() {
		if (assemblyType1TypeBaseObjectEDataType == null) {
			assemblyType1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(37);
		}
		return assemblyType1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getCapabilityType1TypeBaseObject() {
		if (capabilityType1TypeBaseObjectEDataType == null) {
			capabilityType1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(72);
		}
		return capabilityType1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDataType1TypeBaseObject() {
		if (dataType1TypeBaseObjectEDataType == null) {
			dataType1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(317);
		}
		return dataType1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDependency1TypeBaseObject() {
		if (dependency1TypeBaseObjectEDataType == null) {
			dependency1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(322);
		}
		return dependency1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDurationType() {
		if (durationTypeEDataType == null) {
			durationTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(326);
		}
		return durationTypeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEquipmentElementLevel1TypeBaseObject() {
		if (equipmentElementLevel1TypeBaseObjectEDataType == null) {
			equipmentElementLevel1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(337);
		}
		return equipmentElementLevel1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIndicatorType() {
		if (indicatorTypeEDataType == null) {
			indicatorTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(375);
		}
		return indicatorTypeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIndicatorTypeObject() {
		if (indicatorTypeObjectEDataType == null) {
			indicatorTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(376);
		}
		return indicatorTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getJobOrderCommand1TypeBaseObject() {
		if (jobOrderCommand1TypeBaseObjectEDataType == null) {
			jobOrderCommand1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(379);
		}
		return jobOrderCommand1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getMaterialUse1TypeBaseObject() {
		if (materialUse1TypeBaseObjectEDataType == null) {
			materialUse1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(408);
		}
		return materialUse1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getOperationsType1TypeBaseObject() {
		if (operationsType1TypeBaseObjectEDataType == null) {
			operationsType1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(438);
		}
		return operationsType1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getRelationshipForm1TypeBaseObject() {
		if (relationshipForm1TypeBaseObjectEDataType == null) {
			relationshipForm1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(543);
		}
		return relationshipForm1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getRelationshipType1TypeBaseObject() {
		if (relationshipType1TypeBaseObjectEDataType == null) {
			relationshipType1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(547);
		}
		return relationshipType1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getRequestState1TypeBaseObject() {
		if (requestState1TypeBaseObjectEDataType == null) {
			requestState1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(553);
		}
		return requestState1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getRequiredByRequestedSegmentResponse1TypeBaseObject() {
		if (requiredByRequestedSegmentResponse1TypeBaseObjectEDataType == null) {
			requiredByRequestedSegmentResponse1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(557);
		}
		return requiredByRequestedSegmentResponse1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getResourceReferenceType1TypeBaseObject() {
		if (resourceReferenceType1TypeBaseObjectEDataType == null) {
			resourceReferenceType1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(563);
		}
		return resourceReferenceType1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getResponseState1TypeBaseObject() {
		if (responseState1TypeBaseObjectEDataType == null) {
			responseState1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(594);
		}
		return responseState1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getTransActionCodeEnumerationTypeObject() {
		if (transActionCodeEnumerationTypeObjectEDataType == null) {
			transActionCodeEnumerationTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(669);
		}
		return transActionCodeEnumerationTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getTransActionCodeType() {
		if (transActionCodeTypeEDataType == null) {
			transActionCodeTypeEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(670);
		}
		return transActionCodeTypeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getTransResponseCodeTypeObject() {
		if (transResponseCodeTypeObjectEDataType == null) {
			transResponseCodeTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(685);
		}
		return transResponseCodeTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getWorkType1TypeBaseObject() {
		if (workType1TypeBaseObjectEDataType == null) {
			workType1TypeBaseObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(B2MMLPackage.eNS_URI).getEClassifiers().get(701);
		}
		return workType1TypeBaseObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public B2MMLFactory getB2MMLFactory() {
		return (B2MMLFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isLoaded = false;

	/**
	 * Laods the package and any sub-packages from their serialized form.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void loadPackage() {
		if (isLoaded) return;
		isLoaded = true;

		URL url = getClass().getResource(packageFilename);
		if (url == null) {
			throw new RuntimeException("Missing serialized package: " + packageFilename);
		}
		URI uri = URI.createURI(url.toString());
		Resource resource = new EcoreResourceFactoryImpl().createResource(uri);
		try {
			resource.load(null);
		}
		catch (IOException exception) {
			throw new WrappedException(exception);
		}
		initializeFromLoadedEPackage(this, (EPackage)resource.getContents().get(0));
		createResource(eNS_URI);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isFixed = false;

	/**
	 * Fixes up the loaded package, to make it appear as if it had been programmatically built.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fixPackageContents() {
		if (isFixed) return;
		isFixed = true;
		fixEClassifiers();
	}

	/**
	 * Sets the instance class on the given classifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void fixInstanceClass(EClassifier eClassifier) {
		if (eClassifier.getInstanceClassName() == null) {
			eClassifier.setInstanceClassName("org.mesa.xml.v0600.b2mml." + eClassifier.getName());
			setGeneratedClassName(eClassifier);
		}
	}

} //B2MMLPackageImpl
