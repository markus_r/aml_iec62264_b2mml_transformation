/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.AssemblyRelationshipType;
import org.mesa.xml.v0600.b2mml.AssemblyTypeType;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.CodeType;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.MaterialClassIDType;
import org.mesa.xml.v0600.b2mml.MaterialDefinitionIDType;
import org.mesa.xml.v0600.b2mml.OperationsMaterialBillItemType;
import org.mesa.xml.v0600.b2mml.QuantityValueType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Material Bill Item Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsMaterialBillItemTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsMaterialBillItemTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsMaterialBillItemTypeImpl#getMaterialClassID <em>Material Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsMaterialBillItemTypeImpl#getMaterialDefinitionID <em>Material Definition ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsMaterialBillItemTypeImpl#getUseType <em>Use Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsMaterialBillItemTypeImpl#getAssemblyBillOfMaterialItem <em>Assembly Bill Of Material Item</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsMaterialBillItemTypeImpl#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsMaterialBillItemTypeImpl#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsMaterialBillItemTypeImpl#getMaterialSpecificationID <em>Material Specification ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsMaterialBillItemTypeImpl#getQuantity <em>Quantity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsMaterialBillItemTypeImpl extends MinimalEObjectImpl.Container implements OperationsMaterialBillItemType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected DescriptionType description;

	/**
	 * The cached value of the '{@link #getMaterialClassID() <em>Material Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialClassIDType> materialClassID;

	/**
	 * The cached value of the '{@link #getMaterialDefinitionID() <em>Material Definition ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinitionID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialDefinitionIDType> materialDefinitionID;

	/**
	 * The cached value of the '{@link #getUseType() <em>Use Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseType()
	 * @generated
	 * @ordered
	 */
	protected CodeType useType;

	/**
	 * The cached value of the '{@link #getAssemblyBillOfMaterialItem() <em>Assembly Bill Of Material Item</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyBillOfMaterialItem()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsMaterialBillItemType> assemblyBillOfMaterialItem;

	/**
	 * The cached value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected AssemblyTypeType assemblyType;

	/**
	 * The cached value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected AssemblyRelationshipType assemblyRelationship;

	/**
	 * The cached value of the '{@link #getMaterialSpecificationID() <em>Material Specification ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSpecificationID()
	 * @generated
	 * @ordered
	 */
	protected EList<IdentifierType> materialSpecificationID;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsMaterialBillItemTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOperationsMaterialBillItemType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DescriptionType getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescription(DescriptionType newDescription, NotificationChain msgs) {
		DescriptionType oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__DESCRIPTION, oldDescription, newDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(DescriptionType newDescription) {
		if (newDescription != description) {
			NotificationChain msgs = null;
			if (description != null)
				msgs = ((InternalEObject)description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__DESCRIPTION, null, msgs);
			if (newDescription != null)
				msgs = ((InternalEObject)newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__DESCRIPTION, null, msgs);
			msgs = basicSetDescription(newDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__DESCRIPTION, newDescription, newDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialClassIDType> getMaterialClassID() {
		if (materialClassID == null) {
			materialClassID = new EObjectContainmentEList<MaterialClassIDType>(MaterialClassIDType.class, this, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_CLASS_ID);
		}
		return materialClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialDefinitionIDType> getMaterialDefinitionID() {
		if (materialDefinitionID == null) {
			materialDefinitionID = new EObjectContainmentEList<MaterialDefinitionIDType>(MaterialDefinitionIDType.class, this, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_DEFINITION_ID);
		}
		return materialDefinitionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeType getUseType() {
		return useType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUseType(CodeType newUseType, NotificationChain msgs) {
		CodeType oldUseType = useType;
		useType = newUseType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__USE_TYPE, oldUseType, newUseType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseType(CodeType newUseType) {
		if (newUseType != useType) {
			NotificationChain msgs = null;
			if (useType != null)
				msgs = ((InternalEObject)useType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__USE_TYPE, null, msgs);
			if (newUseType != null)
				msgs = ((InternalEObject)newUseType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__USE_TYPE, null, msgs);
			msgs = basicSetUseType(newUseType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__USE_TYPE, newUseType, newUseType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsMaterialBillItemType> getAssemblyBillOfMaterialItem() {
		if (assemblyBillOfMaterialItem == null) {
			assemblyBillOfMaterialItem = new EObjectContainmentEList<OperationsMaterialBillItemType>(OperationsMaterialBillItemType.class, this, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_BILL_OF_MATERIAL_ITEM);
		}
		return assemblyBillOfMaterialItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyTypeType getAssemblyType() {
		return assemblyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyType(AssemblyTypeType newAssemblyType, NotificationChain msgs) {
		AssemblyTypeType oldAssemblyType = assemblyType;
		assemblyType = newAssemblyType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_TYPE, oldAssemblyType, newAssemblyType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyType(AssemblyTypeType newAssemblyType) {
		if (newAssemblyType != assemblyType) {
			NotificationChain msgs = null;
			if (assemblyType != null)
				msgs = ((InternalEObject)assemblyType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_TYPE, null, msgs);
			if (newAssemblyType != null)
				msgs = ((InternalEObject)newAssemblyType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_TYPE, null, msgs);
			msgs = basicSetAssemblyType(newAssemblyType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_TYPE, newAssemblyType, newAssemblyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationshipType getAssemblyRelationship() {
		return assemblyRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship, NotificationChain msgs) {
		AssemblyRelationshipType oldAssemblyRelationship = assemblyRelationship;
		assemblyRelationship = newAssemblyRelationship;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_RELATIONSHIP, oldAssemblyRelationship, newAssemblyRelationship);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship) {
		if (newAssemblyRelationship != assemblyRelationship) {
			NotificationChain msgs = null;
			if (assemblyRelationship != null)
				msgs = ((InternalEObject)assemblyRelationship).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			if (newAssemblyRelationship != null)
				msgs = ((InternalEObject)newAssemblyRelationship).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			msgs = basicSetAssemblyRelationship(newAssemblyRelationship, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_RELATIONSHIP, newAssemblyRelationship, newAssemblyRelationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IdentifierType> getMaterialSpecificationID() {
		if (materialSpecificationID == null) {
			materialSpecificationID = new EObjectContainmentEList<IdentifierType>(IdentifierType.class, this, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_SPECIFICATION_ID);
		}
		return materialSpecificationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__DESCRIPTION:
				return basicSetDescription(null, msgs);
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_CLASS_ID:
				return ((InternalEList<?>)getMaterialClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_DEFINITION_ID:
				return ((InternalEList<?>)getMaterialDefinitionID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__USE_TYPE:
				return basicSetUseType(null, msgs);
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_BILL_OF_MATERIAL_ITEM:
				return ((InternalEList<?>)getAssemblyBillOfMaterialItem()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_TYPE:
				return basicSetAssemblyType(null, msgs);
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_RELATIONSHIP:
				return basicSetAssemblyRelationship(null, msgs);
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_SPECIFICATION_ID:
				return ((InternalEList<?>)getMaterialSpecificationID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ID:
				return getID();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_CLASS_ID:
				return getMaterialClassID();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_DEFINITION_ID:
				return getMaterialDefinitionID();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__USE_TYPE:
				return getUseType();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_BILL_OF_MATERIAL_ITEM:
				return getAssemblyBillOfMaterialItem();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_TYPE:
				return getAssemblyType();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_RELATIONSHIP:
				return getAssemblyRelationship();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_SPECIFICATION_ID:
				return getMaterialSpecificationID();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__QUANTITY:
				return getQuantity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__DESCRIPTION:
				setDescription((DescriptionType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_CLASS_ID:
				getMaterialClassID().clear();
				getMaterialClassID().addAll((Collection<? extends MaterialClassIDType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_DEFINITION_ID:
				getMaterialDefinitionID().clear();
				getMaterialDefinitionID().addAll((Collection<? extends MaterialDefinitionIDType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__USE_TYPE:
				setUseType((CodeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_BILL_OF_MATERIAL_ITEM:
				getAssemblyBillOfMaterialItem().clear();
				getAssemblyBillOfMaterialItem().addAll((Collection<? extends OperationsMaterialBillItemType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_SPECIFICATION_ID:
				getMaterialSpecificationID().clear();
				getMaterialSpecificationID().addAll((Collection<? extends IdentifierType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__DESCRIPTION:
				setDescription((DescriptionType)null);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_CLASS_ID:
				getMaterialClassID().clear();
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_DEFINITION_ID:
				getMaterialDefinitionID().clear();
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__USE_TYPE:
				setUseType((CodeType)null);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_BILL_OF_MATERIAL_ITEM:
				getAssemblyBillOfMaterialItem().clear();
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)null);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)null);
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_SPECIFICATION_ID:
				getMaterialSpecificationID().clear();
				return;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__QUANTITY:
				getQuantity().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ID:
				return iD != null;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__DESCRIPTION:
				return description != null;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_CLASS_ID:
				return materialClassID != null && !materialClassID.isEmpty();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_DEFINITION_ID:
				return materialDefinitionID != null && !materialDefinitionID.isEmpty();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__USE_TYPE:
				return useType != null;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_BILL_OF_MATERIAL_ITEM:
				return assemblyBillOfMaterialItem != null && !assemblyBillOfMaterialItem.isEmpty();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_TYPE:
				return assemblyType != null;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__ASSEMBLY_RELATIONSHIP:
				return assemblyRelationship != null;
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__MATERIAL_SPECIFICATION_ID:
				return materialSpecificationID != null && !materialSpecificationID.isEmpty();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OperationsMaterialBillItemTypeImpl
