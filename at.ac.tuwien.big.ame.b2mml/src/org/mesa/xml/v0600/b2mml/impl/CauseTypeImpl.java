/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.CauseType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cause Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CauseTypeImpl extends CodeTypeImpl implements CauseType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CauseTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getCauseType();
	}

} //CauseTypeImpl
