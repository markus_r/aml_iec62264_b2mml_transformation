/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.OperationsDefinitionIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Definition ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OperationsDefinitionIDTypeImpl extends IdentifierTypeImpl implements OperationsDefinitionIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsDefinitionIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOperationsDefinitionIDType();
	}

} //OperationsDefinitionIDTypeImpl
