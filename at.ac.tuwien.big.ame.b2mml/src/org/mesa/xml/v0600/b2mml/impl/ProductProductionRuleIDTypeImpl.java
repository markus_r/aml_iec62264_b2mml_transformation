/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.ProductProductionRuleIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Product Production Rule ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ProductProductionRuleIDTypeImpl extends IdentifierTypeImpl implements ProductProductionRuleIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProductProductionRuleIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getProductProductionRuleIDType();
	}

} //ProductProductionRuleIDTypeImpl
