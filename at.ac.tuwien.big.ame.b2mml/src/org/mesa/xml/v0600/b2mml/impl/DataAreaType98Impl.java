/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType98;
import org.mesa.xml.v0600.b2mml.EquipmentInformationType;
import org.mesa.xml.v0600.b2mml.TransRespondType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type98</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType98Impl#getRespond <em>Respond</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType98Impl#getEquipmentInformation <em>Equipment Information</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType98Impl extends MinimalEObjectImpl.Container implements DataAreaType98 {
	/**
	 * The cached value of the '{@link #getRespond() <em>Respond</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRespond()
	 * @generated
	 * @ordered
	 */
	protected TransRespondType respond;

	/**
	 * The cached value of the '{@link #getEquipmentInformation() <em>Equipment Information</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentInformation()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentInformationType> equipmentInformation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType98Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType98();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransRespondType getRespond() {
		return respond;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespond(TransRespondType newRespond, NotificationChain msgs) {
		TransRespondType oldRespond = respond;
		respond = newRespond;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE98__RESPOND, oldRespond, newRespond);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespond(TransRespondType newRespond) {
		if (newRespond != respond) {
			NotificationChain msgs = null;
			if (respond != null)
				msgs = ((InternalEObject)respond).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE98__RESPOND, null, msgs);
			if (newRespond != null)
				msgs = ((InternalEObject)newRespond).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE98__RESPOND, null, msgs);
			msgs = basicSetRespond(newRespond, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE98__RESPOND, newRespond, newRespond));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentInformationType> getEquipmentInformation() {
		if (equipmentInformation == null) {
			equipmentInformation = new EObjectContainmentEList<EquipmentInformationType>(EquipmentInformationType.class, this, B2MMLPackage.DATA_AREA_TYPE98__EQUIPMENT_INFORMATION);
		}
		return equipmentInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE98__RESPOND:
				return basicSetRespond(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE98__EQUIPMENT_INFORMATION:
				return ((InternalEList<?>)getEquipmentInformation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE98__RESPOND:
				return getRespond();
			case B2MMLPackage.DATA_AREA_TYPE98__EQUIPMENT_INFORMATION:
				return getEquipmentInformation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE98__RESPOND:
				setRespond((TransRespondType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE98__EQUIPMENT_INFORMATION:
				getEquipmentInformation().clear();
				getEquipmentInformation().addAll((Collection<? extends EquipmentInformationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE98__RESPOND:
				setRespond((TransRespondType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE98__EQUIPMENT_INFORMATION:
				getEquipmentInformation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE98__RESPOND:
				return respond != null;
			case B2MMLPackage.DATA_AREA_TYPE98__EQUIPMENT_INFORMATION:
				return equipmentInformation != null && !equipmentInformation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType98Impl
