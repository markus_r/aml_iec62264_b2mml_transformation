/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.EndTimeType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.OpSegmentResponseType;
import org.mesa.xml.v0600.b2mml.OperationsDefinitionIDType;
import org.mesa.xml.v0600.b2mml.OperationsRequestIDType;
import org.mesa.xml.v0600.b2mml.OperationsResponseType;
import org.mesa.xml.v0600.b2mml.OperationsTypeType;
import org.mesa.xml.v0600.b2mml.ResponseStateType;
import org.mesa.xml.v0600.b2mml.StartTimeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Response Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsResponseTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsResponseTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsResponseTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsResponseTypeImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsResponseTypeImpl#getOperationsRequestID <em>Operations Request ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsResponseTypeImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsResponseTypeImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsResponseTypeImpl#getOperationsDefinitionID <em>Operations Definition ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsResponseTypeImpl#getResponseState <em>Response State</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsResponseTypeImpl#getSegmentResponse <em>Segment Response</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsResponseTypeImpl extends MinimalEObjectImpl.Container implements OperationsResponseType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsTypeType operationsType;

	/**
	 * The cached value of the '{@link #getOperationsRequestID() <em>Operations Request ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsRequestID()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsRequestIDType> operationsRequestID;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected StartTimeType startTime;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected EndTimeType endTime;

	/**
	 * The cached value of the '{@link #getOperationsDefinitionID() <em>Operations Definition ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsDefinitionID()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsDefinitionIDType> operationsDefinitionID;

	/**
	 * The cached value of the '{@link #getResponseState() <em>Response State</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseState()
	 * @generated
	 * @ordered
	 */
	protected ResponseStateType responseState;

	/**
	 * The cached value of the '{@link #getSegmentResponse() <em>Segment Response</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentResponse()
	 * @generated
	 * @ordered
	 */
	protected EList<OpSegmentResponseType> segmentResponse;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsResponseTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOperationsResponseType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_RESPONSE_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_RESPONSE_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_RESPONSE_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_RESPONSE_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsTypeType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsType(OperationsTypeType newOperationsType, NotificationChain msgs) {
		OperationsTypeType oldOperationsType = operationsType;
		operationsType = newOperationsType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_TYPE, oldOperationsType, newOperationsType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsTypeType newOperationsType) {
		if (newOperationsType != operationsType) {
			NotificationChain msgs = null;
			if (operationsType != null)
				msgs = ((InternalEObject)operationsType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_TYPE, null, msgs);
			if (newOperationsType != null)
				msgs = ((InternalEObject)newOperationsType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_TYPE, null, msgs);
			msgs = basicSetOperationsType(newOperationsType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_TYPE, newOperationsType, newOperationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsRequestIDType> getOperationsRequestID() {
		if (operationsRequestID == null) {
			operationsRequestID = new EObjectContainmentEList<OperationsRequestIDType>(OperationsRequestIDType.class, this, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_REQUEST_ID);
		}
		return operationsRequestID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StartTimeType getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStartTime(StartTimeType newStartTime, NotificationChain msgs) {
		StartTimeType oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__START_TIME, oldStartTime, newStartTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(StartTimeType newStartTime) {
		if (newStartTime != startTime) {
			NotificationChain msgs = null;
			if (startTime != null)
				msgs = ((InternalEObject)startTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_RESPONSE_TYPE__START_TIME, null, msgs);
			if (newStartTime != null)
				msgs = ((InternalEObject)newStartTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_RESPONSE_TYPE__START_TIME, null, msgs);
			msgs = basicSetStartTime(newStartTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__START_TIME, newStartTime, newStartTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EndTimeType getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEndTime(EndTimeType newEndTime, NotificationChain msgs) {
		EndTimeType oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__END_TIME, oldEndTime, newEndTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(EndTimeType newEndTime) {
		if (newEndTime != endTime) {
			NotificationChain msgs = null;
			if (endTime != null)
				msgs = ((InternalEObject)endTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_RESPONSE_TYPE__END_TIME, null, msgs);
			if (newEndTime != null)
				msgs = ((InternalEObject)newEndTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_RESPONSE_TYPE__END_TIME, null, msgs);
			msgs = basicSetEndTime(newEndTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__END_TIME, newEndTime, newEndTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsDefinitionIDType> getOperationsDefinitionID() {
		if (operationsDefinitionID == null) {
			operationsDefinitionID = new EObjectContainmentEList<OperationsDefinitionIDType>(OperationsDefinitionIDType.class, this, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_DEFINITION_ID);
		}
		return operationsDefinitionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseStateType getResponseState() {
		return responseState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResponseState(ResponseStateType newResponseState, NotificationChain msgs) {
		ResponseStateType oldResponseState = responseState;
		responseState = newResponseState;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__RESPONSE_STATE, oldResponseState, newResponseState);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponseState(ResponseStateType newResponseState) {
		if (newResponseState != responseState) {
			NotificationChain msgs = null;
			if (responseState != null)
				msgs = ((InternalEObject)responseState).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_RESPONSE_TYPE__RESPONSE_STATE, null, msgs);
			if (newResponseState != null)
				msgs = ((InternalEObject)newResponseState).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_RESPONSE_TYPE__RESPONSE_STATE, null, msgs);
			msgs = basicSetResponseState(newResponseState, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__RESPONSE_STATE, newResponseState, newResponseState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpSegmentResponseType> getSegmentResponse() {
		if (segmentResponse == null) {
			segmentResponse = new EObjectContainmentEList<OpSegmentResponseType>(OpSegmentResponseType.class, this, B2MMLPackage.OPERATIONS_RESPONSE_TYPE__SEGMENT_RESPONSE);
		}
		return segmentResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_TYPE:
				return basicSetOperationsType(null, msgs);
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_REQUEST_ID:
				return ((InternalEList<?>)getOperationsRequestID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__START_TIME:
				return basicSetStartTime(null, msgs);
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__END_TIME:
				return basicSetEndTime(null, msgs);
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_DEFINITION_ID:
				return ((InternalEList<?>)getOperationsDefinitionID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__RESPONSE_STATE:
				return basicSetResponseState(null, msgs);
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__SEGMENT_RESPONSE:
				return ((InternalEList<?>)getSegmentResponse()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__ID:
				return getID();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_TYPE:
				return getOperationsType();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_REQUEST_ID:
				return getOperationsRequestID();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__START_TIME:
				return getStartTime();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__END_TIME:
				return getEndTime();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_DEFINITION_ID:
				return getOperationsDefinitionID();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__RESPONSE_STATE:
				return getResponseState();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__SEGMENT_RESPONSE:
				return getSegmentResponse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_REQUEST_ID:
				getOperationsRequestID().clear();
				getOperationsRequestID().addAll((Collection<? extends OperationsRequestIDType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__START_TIME:
				setStartTime((StartTimeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__END_TIME:
				setEndTime((EndTimeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_DEFINITION_ID:
				getOperationsDefinitionID().clear();
				getOperationsDefinitionID().addAll((Collection<? extends OperationsDefinitionIDType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__RESPONSE_STATE:
				setResponseState((ResponseStateType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__SEGMENT_RESPONSE:
				getSegmentResponse().clear();
				getSegmentResponse().addAll((Collection<? extends OpSegmentResponseType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)null);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_REQUEST_ID:
				getOperationsRequestID().clear();
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__START_TIME:
				setStartTime((StartTimeType)null);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__END_TIME:
				setEndTime((EndTimeType)null);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_DEFINITION_ID:
				getOperationsDefinitionID().clear();
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__RESPONSE_STATE:
				setResponseState((ResponseStateType)null);
				return;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__SEGMENT_RESPONSE:
				getSegmentResponse().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__ID:
				return iD != null;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_TYPE:
				return operationsType != null;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_REQUEST_ID:
				return operationsRequestID != null && !operationsRequestID.isEmpty();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__START_TIME:
				return startTime != null;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__END_TIME:
				return endTime != null;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__OPERATIONS_DEFINITION_ID:
				return operationsDefinitionID != null && !operationsDefinitionID.isEmpty();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__RESPONSE_STATE:
				return responseState != null;
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE__SEGMENT_RESPONSE:
				return segmentResponse != null && !segmentResponse.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OperationsResponseTypeImpl
