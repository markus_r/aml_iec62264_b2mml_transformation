/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.BillOfMaterialsIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bill Of Materials ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BillOfMaterialsIDTypeImpl extends IdentifierTypeImpl implements BillOfMaterialsIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BillOfMaterialsIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getBillOfMaterialsIDType();
	}

} //BillOfMaterialsIDTypeImpl
