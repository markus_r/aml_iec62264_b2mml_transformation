/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.ResponseState1Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Response State1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ResponseState1TypeImpl extends CodeTypeImpl implements ResponseState1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResponseState1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getResponseState1Type();
	}

} //ResponseState1TypeImpl
