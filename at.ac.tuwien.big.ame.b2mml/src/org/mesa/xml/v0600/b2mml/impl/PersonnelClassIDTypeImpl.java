/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.PersonnelClassIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Personnel Class ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PersonnelClassIDTypeImpl extends IdentifierTypeImpl implements PersonnelClassIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonnelClassIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPersonnelClassIDType();
	}

} //PersonnelClassIDTypeImpl
