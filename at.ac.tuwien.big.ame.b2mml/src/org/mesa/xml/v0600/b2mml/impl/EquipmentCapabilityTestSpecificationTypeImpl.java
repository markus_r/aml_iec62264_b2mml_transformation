/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.EquipmentCapabilityTestSpecificationType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.LocationType;
import org.mesa.xml.v0600.b2mml.NameType;
import org.mesa.xml.v0600.b2mml.TestedEquipmentClassPropertyType;
import org.mesa.xml.v0600.b2mml.TestedEquipmentPropertyType;
import org.mesa.xml.v0600.b2mml.VersionType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Capability Test Specification Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentCapabilityTestSpecificationTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentCapabilityTestSpecificationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentCapabilityTestSpecificationTypeImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentCapabilityTestSpecificationTypeImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentCapabilityTestSpecificationTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentCapabilityTestSpecificationTypeImpl#getTestedEquipmentProperty <em>Tested Equipment Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentCapabilityTestSpecificationTypeImpl#getTestedEquipmentClassProperty <em>Tested Equipment Class Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EquipmentCapabilityTestSpecificationTypeImpl extends MinimalEObjectImpl.Container implements EquipmentCapabilityTestSpecificationType {
	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected NameType name;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected VersionType version;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected LocationType location;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getTestedEquipmentProperty() <em>Tested Equipment Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestedEquipmentProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<TestedEquipmentPropertyType> testedEquipmentProperty;

	/**
	 * The cached value of the '{@link #getTestedEquipmentClassProperty() <em>Tested Equipment Class Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestedEquipmentClassProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<TestedEquipmentClassPropertyType> testedEquipmentClassProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentCapabilityTestSpecificationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getEquipmentCapabilityTestSpecificationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameType getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetName(NameType newName, NotificationChain msgs) {
		NameType oldName = name;
		name = newName;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME, oldName, newName);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(NameType newName) {
		if (newName != name) {
			NotificationChain msgs = null;
			if (name != null)
				msgs = ((InternalEObject)name).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME, null, msgs);
			if (newName != null)
				msgs = ((InternalEObject)newName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME, null, msgs);
			msgs = basicSetName(newName, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME, newName, newName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VersionType getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVersion(VersionType newVersion, NotificationChain msgs) {
		VersionType oldVersion = version;
		version = newVersion;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION, oldVersion, newVersion);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(VersionType newVersion) {
		if (newVersion != version) {
			NotificationChain msgs = null;
			if (version != null)
				msgs = ((InternalEObject)version).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION, null, msgs);
			if (newVersion != null)
				msgs = ((InternalEObject)newVersion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION, null, msgs);
			msgs = basicSetVersion(newVersion, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION, newVersion, newVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationType getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocation(LocationType newLocation, NotificationChain msgs) {
		LocationType oldLocation = location;
		location = newLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__LOCATION, oldLocation, newLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(LocationType newLocation) {
		if (newLocation != location) {
			NotificationChain msgs = null;
			if (location != null)
				msgs = ((InternalEObject)location).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__LOCATION, null, msgs);
			if (newLocation != null)
				msgs = ((InternalEObject)newLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__LOCATION, null, msgs);
			msgs = basicSetLocation(newLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__LOCATION, newLocation, newLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestedEquipmentPropertyType> getTestedEquipmentProperty() {
		if (testedEquipmentProperty == null) {
			testedEquipmentProperty = new EObjectContainmentEList<TestedEquipmentPropertyType>(TestedEquipmentPropertyType.class, this, B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_EQUIPMENT_PROPERTY);
		}
		return testedEquipmentProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestedEquipmentClassPropertyType> getTestedEquipmentClassProperty() {
		if (testedEquipmentClassProperty == null) {
			testedEquipmentClassProperty = new EObjectContainmentEList<TestedEquipmentClassPropertyType>(TestedEquipmentClassPropertyType.class, this, B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_EQUIPMENT_CLASS_PROPERTY);
		}
		return testedEquipmentClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME:
				return basicSetName(null, msgs);
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION:
				return basicSetVersion(null, msgs);
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__LOCATION:
				return basicSetLocation(null, msgs);
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_EQUIPMENT_PROPERTY:
				return ((InternalEList<?>)getTestedEquipmentProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_EQUIPMENT_CLASS_PROPERTY:
				return ((InternalEList<?>)getTestedEquipmentClassProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME:
				return getName();
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION:
				return getVersion();
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__LOCATION:
				return getLocation();
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_EQUIPMENT_PROPERTY:
				return getTestedEquipmentProperty();
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_EQUIPMENT_CLASS_PROPERTY:
				return getTestedEquipmentClassProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME:
				setName((NameType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION:
				setVersion((VersionType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__LOCATION:
				setLocation((LocationType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_EQUIPMENT_PROPERTY:
				getTestedEquipmentProperty().clear();
				getTestedEquipmentProperty().addAll((Collection<? extends TestedEquipmentPropertyType>)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_EQUIPMENT_CLASS_PROPERTY:
				getTestedEquipmentClassProperty().clear();
				getTestedEquipmentClassProperty().addAll((Collection<? extends TestedEquipmentClassPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME:
				setName((NameType)null);
				return;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION:
				setVersion((VersionType)null);
				return;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__LOCATION:
				setLocation((LocationType)null);
				return;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_EQUIPMENT_PROPERTY:
				getTestedEquipmentProperty().clear();
				return;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_EQUIPMENT_CLASS_PROPERTY:
				getTestedEquipmentClassProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME:
				return name != null;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION:
				return version != null;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__LOCATION:
				return location != null;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_EQUIPMENT_PROPERTY:
				return testedEquipmentProperty != null && !testedEquipmentProperty.isEmpty();
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_EQUIPMENT_CLASS_PROPERTY:
				return testedEquipmentClassProperty != null && !testedEquipmentClassProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EquipmentCapabilityTestSpecificationTypeImpl
