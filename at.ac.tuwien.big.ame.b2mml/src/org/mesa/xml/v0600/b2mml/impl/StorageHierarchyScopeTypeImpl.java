/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.StorageHierarchyScopeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Storage Hierarchy Scope Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StorageHierarchyScopeTypeImpl extends IdentifierTypeImpl implements StorageHierarchyScopeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StorageHierarchyScopeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getStorageHierarchyScopeType();
	}

} //StorageHierarchyScopeTypeImpl
