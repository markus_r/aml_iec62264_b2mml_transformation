/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.AssemblyRelationshipType;
import org.mesa.xml.v0600.b2mml.AssemblyTypeType;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.LocationType;
import org.mesa.xml.v0600.b2mml.MaterialClassIDType;
import org.mesa.xml.v0600.b2mml.MaterialClassPropertyType;
import org.mesa.xml.v0600.b2mml.MaterialClassType;
import org.mesa.xml.v0600.b2mml.MaterialDefinitionIDType;
import org.mesa.xml.v0600.b2mml.MaterialTestSpecificationIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Class Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialClassTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialClassTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialClassTypeImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialClassTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialClassTypeImpl#getMaterialClassProperty <em>Material Class Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialClassTypeImpl#getMaterialDefinitionID <em>Material Definition ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialClassTypeImpl#getMaterialTestSpecificationID <em>Material Test Specification ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialClassTypeImpl#getAssemblyClassID <em>Assembly Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialClassTypeImpl#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialClassTypeImpl#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MaterialClassTypeImpl extends MinimalEObjectImpl.Container implements MaterialClassType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected LocationType location;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getMaterialClassProperty() <em>Material Class Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClassProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialClassPropertyType> materialClassProperty;

	/**
	 * The cached value of the '{@link #getMaterialDefinitionID() <em>Material Definition ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinitionID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialDefinitionIDType> materialDefinitionID;

	/**
	 * The cached value of the '{@link #getMaterialTestSpecificationID() <em>Material Test Specification ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialTestSpecificationID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialTestSpecificationIDType> materialTestSpecificationID;

	/**
	 * The cached value of the '{@link #getAssemblyClassID() <em>Assembly Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialClassIDType> assemblyClassID;

	/**
	 * The cached value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected AssemblyTypeType assemblyType;

	/**
	 * The cached value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected AssemblyRelationshipType assemblyRelationship;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialClassTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getMaterialClassType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_CLASS_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_CLASS_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_CLASS_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_CLASS_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.MATERIAL_CLASS_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationType getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocation(LocationType newLocation, NotificationChain msgs) {
		LocationType oldLocation = location;
		location = newLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_CLASS_TYPE__LOCATION, oldLocation, newLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(LocationType newLocation) {
		if (newLocation != location) {
			NotificationChain msgs = null;
			if (location != null)
				msgs = ((InternalEObject)location).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_CLASS_TYPE__LOCATION, null, msgs);
			if (newLocation != null)
				msgs = ((InternalEObject)newLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_CLASS_TYPE__LOCATION, null, msgs);
			msgs = basicSetLocation(newLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_CLASS_TYPE__LOCATION, newLocation, newLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_CLASS_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_CLASS_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_CLASS_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_CLASS_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialClassPropertyType> getMaterialClassProperty() {
		if (materialClassProperty == null) {
			materialClassProperty = new EObjectContainmentEList<MaterialClassPropertyType>(MaterialClassPropertyType.class, this, B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_CLASS_PROPERTY);
		}
		return materialClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialDefinitionIDType> getMaterialDefinitionID() {
		if (materialDefinitionID == null) {
			materialDefinitionID = new EObjectContainmentEList<MaterialDefinitionIDType>(MaterialDefinitionIDType.class, this, B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_DEFINITION_ID);
		}
		return materialDefinitionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialTestSpecificationIDType> getMaterialTestSpecificationID() {
		if (materialTestSpecificationID == null) {
			materialTestSpecificationID = new EObjectContainmentEList<MaterialTestSpecificationIDType>(MaterialTestSpecificationIDType.class, this, B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_TEST_SPECIFICATION_ID);
		}
		return materialTestSpecificationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialClassIDType> getAssemblyClassID() {
		if (assemblyClassID == null) {
			assemblyClassID = new EObjectContainmentEList<MaterialClassIDType>(MaterialClassIDType.class, this, B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_CLASS_ID);
		}
		return assemblyClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyTypeType getAssemblyType() {
		return assemblyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyType(AssemblyTypeType newAssemblyType, NotificationChain msgs) {
		AssemblyTypeType oldAssemblyType = assemblyType;
		assemblyType = newAssemblyType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_TYPE, oldAssemblyType, newAssemblyType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyType(AssemblyTypeType newAssemblyType) {
		if (newAssemblyType != assemblyType) {
			NotificationChain msgs = null;
			if (assemblyType != null)
				msgs = ((InternalEObject)assemblyType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_TYPE, null, msgs);
			if (newAssemblyType != null)
				msgs = ((InternalEObject)newAssemblyType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_TYPE, null, msgs);
			msgs = basicSetAssemblyType(newAssemblyType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_TYPE, newAssemblyType, newAssemblyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationshipType getAssemblyRelationship() {
		return assemblyRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship, NotificationChain msgs) {
		AssemblyRelationshipType oldAssemblyRelationship = assemblyRelationship;
		assemblyRelationship = newAssemblyRelationship;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_RELATIONSHIP, oldAssemblyRelationship, newAssemblyRelationship);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship) {
		if (newAssemblyRelationship != assemblyRelationship) {
			NotificationChain msgs = null;
			if (assemblyRelationship != null)
				msgs = ((InternalEObject)assemblyRelationship).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			if (newAssemblyRelationship != null)
				msgs = ((InternalEObject)newAssemblyRelationship).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			msgs = basicSetAssemblyRelationship(newAssemblyRelationship, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_RELATIONSHIP, newAssemblyRelationship, newAssemblyRelationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.MATERIAL_CLASS_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_CLASS_TYPE__LOCATION:
				return basicSetLocation(null, msgs);
			case B2MMLPackage.MATERIAL_CLASS_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_CLASS_PROPERTY:
				return ((InternalEList<?>)getMaterialClassProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_DEFINITION_ID:
				return ((InternalEList<?>)getMaterialDefinitionID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_TEST_SPECIFICATION_ID:
				return ((InternalEList<?>)getMaterialTestSpecificationID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_CLASS_ID:
				return ((InternalEList<?>)getAssemblyClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_TYPE:
				return basicSetAssemblyType(null, msgs);
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_RELATIONSHIP:
				return basicSetAssemblyRelationship(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ID:
				return getID();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__LOCATION:
				return getLocation();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_CLASS_PROPERTY:
				return getMaterialClassProperty();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_DEFINITION_ID:
				return getMaterialDefinitionID();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_TEST_SPECIFICATION_ID:
				return getMaterialTestSpecificationID();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_CLASS_ID:
				return getAssemblyClassID();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_TYPE:
				return getAssemblyType();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_RELATIONSHIP:
				return getAssemblyRelationship();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__LOCATION:
				setLocation((LocationType)newValue);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_CLASS_PROPERTY:
				getMaterialClassProperty().clear();
				getMaterialClassProperty().addAll((Collection<? extends MaterialClassPropertyType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_DEFINITION_ID:
				getMaterialDefinitionID().clear();
				getMaterialDefinitionID().addAll((Collection<? extends MaterialDefinitionIDType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_TEST_SPECIFICATION_ID:
				getMaterialTestSpecificationID().clear();
				getMaterialTestSpecificationID().addAll((Collection<? extends MaterialTestSpecificationIDType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_CLASS_ID:
				getAssemblyClassID().clear();
				getAssemblyClassID().addAll((Collection<? extends MaterialClassIDType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)newValue);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__LOCATION:
				setLocation((LocationType)null);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_CLASS_PROPERTY:
				getMaterialClassProperty().clear();
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_DEFINITION_ID:
				getMaterialDefinitionID().clear();
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_TEST_SPECIFICATION_ID:
				getMaterialTestSpecificationID().clear();
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_CLASS_ID:
				getAssemblyClassID().clear();
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)null);
				return;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ID:
				return iD != null;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__LOCATION:
				return location != null;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_CLASS_PROPERTY:
				return materialClassProperty != null && !materialClassProperty.isEmpty();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_DEFINITION_ID:
				return materialDefinitionID != null && !materialDefinitionID.isEmpty();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__MATERIAL_TEST_SPECIFICATION_ID:
				return materialTestSpecificationID != null && !materialTestSpecificationID.isEmpty();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_CLASS_ID:
				return assemblyClassID != null && !assemblyClassID.isEmpty();
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_TYPE:
				return assemblyType != null;
			case B2MMLPackage.MATERIAL_CLASS_TYPE__ASSEMBLY_RELATIONSHIP:
				return assemblyRelationship != null;
		}
		return super.eIsSet(featureID);
	}

} //MaterialClassTypeImpl
