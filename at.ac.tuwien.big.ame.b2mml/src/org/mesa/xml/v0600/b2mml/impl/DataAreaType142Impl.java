/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType142;
import org.mesa.xml.v0600.b2mml.PersonnelClassType;
import org.mesa.xml.v0600.b2mml.TransAcknowledgeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type142</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType142Impl#getAcknowledge <em>Acknowledge</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType142Impl#getPersonnelClass <em>Personnel Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType142Impl extends MinimalEObjectImpl.Container implements DataAreaType142 {
	/**
	 * The cached value of the '{@link #getAcknowledge() <em>Acknowledge</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcknowledge()
	 * @generated
	 * @ordered
	 */
	protected TransAcknowledgeType acknowledge;

	/**
	 * The cached value of the '{@link #getPersonnelClass() <em>Personnel Class</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelClass()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelClassType> personnelClass;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType142Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType142();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransAcknowledgeType getAcknowledge() {
		return acknowledge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledge(TransAcknowledgeType newAcknowledge, NotificationChain msgs) {
		TransAcknowledgeType oldAcknowledge = acknowledge;
		acknowledge = newAcknowledge;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE142__ACKNOWLEDGE, oldAcknowledge, newAcknowledge);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledge(TransAcknowledgeType newAcknowledge) {
		if (newAcknowledge != acknowledge) {
			NotificationChain msgs = null;
			if (acknowledge != null)
				msgs = ((InternalEObject)acknowledge).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE142__ACKNOWLEDGE, null, msgs);
			if (newAcknowledge != null)
				msgs = ((InternalEObject)newAcknowledge).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE142__ACKNOWLEDGE, null, msgs);
			msgs = basicSetAcknowledge(newAcknowledge, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE142__ACKNOWLEDGE, newAcknowledge, newAcknowledge));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelClassType> getPersonnelClass() {
		if (personnelClass == null) {
			personnelClass = new EObjectContainmentEList<PersonnelClassType>(PersonnelClassType.class, this, B2MMLPackage.DATA_AREA_TYPE142__PERSONNEL_CLASS);
		}
		return personnelClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE142__ACKNOWLEDGE:
				return basicSetAcknowledge(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE142__PERSONNEL_CLASS:
				return ((InternalEList<?>)getPersonnelClass()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE142__ACKNOWLEDGE:
				return getAcknowledge();
			case B2MMLPackage.DATA_AREA_TYPE142__PERSONNEL_CLASS:
				return getPersonnelClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE142__ACKNOWLEDGE:
				setAcknowledge((TransAcknowledgeType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE142__PERSONNEL_CLASS:
				getPersonnelClass().clear();
				getPersonnelClass().addAll((Collection<? extends PersonnelClassType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE142__ACKNOWLEDGE:
				setAcknowledge((TransAcknowledgeType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE142__PERSONNEL_CLASS:
				getPersonnelClass().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE142__ACKNOWLEDGE:
				return acknowledge != null;
			case B2MMLPackage.DATA_AREA_TYPE142__PERSONNEL_CLASS:
				return personnelClass != null && !personnelClass.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType142Impl
