/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.TestDateTimeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Date Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TestDateTimeTypeImpl extends DateTimeTypeImpl implements TestDateTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestDateTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTestDateTimeType();
	}

} //TestDateTimeTypeImpl
