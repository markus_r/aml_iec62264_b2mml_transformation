/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.CapabilityTypeType;
import org.mesa.xml.v0600.b2mml.ConfidenceFactorType;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.EndTimeType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityPropertyType;
import org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType;
import org.mesa.xml.v0600.b2mml.PersonIDType;
import org.mesa.xml.v0600.b2mml.PersonnelClassIDType;
import org.mesa.xml.v0600.b2mml.PersonnelUseType;
import org.mesa.xml.v0600.b2mml.QuantityValueType;
import org.mesa.xml.v0600.b2mml.ReasonType;
import org.mesa.xml.v0600.b2mml.StartTimeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Personnel Capability Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPersonnelCapabilityTypeImpl#getPersonnelClassID <em>Personnel Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPersonnelCapabilityTypeImpl#getPersonID <em>Person ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPersonnelCapabilityTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPersonnelCapabilityTypeImpl#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPersonnelCapabilityTypeImpl#getReason <em>Reason</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPersonnelCapabilityTypeImpl#getConfidenceFactor <em>Confidence Factor</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPersonnelCapabilityTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPersonnelCapabilityTypeImpl#getPersonnelUse <em>Personnel Use</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPersonnelCapabilityTypeImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPersonnelCapabilityTypeImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPersonnelCapabilityTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPersonnelCapabilityTypeImpl#getPersonnelCapabilityProperty <em>Personnel Capability Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpPersonnelCapabilityTypeImpl extends MinimalEObjectImpl.Container implements OpPersonnelCapabilityType {
	/**
	 * The cached value of the '{@link #getPersonnelClassID() <em>Personnel Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelClassIDType> personnelClassID;

	/**
	 * The cached value of the '{@link #getPersonID() <em>Person ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonID()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonIDType> personID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getCapabilityType() <em>Capability Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilityType()
	 * @generated
	 * @ordered
	 */
	protected CapabilityTypeType capabilityType;

	/**
	 * The cached value of the '{@link #getReason() <em>Reason</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected ReasonType reason;

	/**
	 * The cached value of the '{@link #getConfidenceFactor() <em>Confidence Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceFactor()
	 * @generated
	 * @ordered
	 */
	protected ConfidenceFactorType confidenceFactor;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getPersonnelUse() <em>Personnel Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelUse()
	 * @generated
	 * @ordered
	 */
	protected PersonnelUseType personnelUse;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected StartTimeType startTime;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected EndTimeType endTime;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getPersonnelCapabilityProperty() <em>Personnel Capability Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelCapabilityProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPersonnelCapabilityPropertyType> personnelCapabilityProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpPersonnelCapabilityTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpPersonnelCapabilityType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelClassIDType> getPersonnelClassID() {
		if (personnelClassID == null) {
			personnelClassID = new EObjectContainmentEList<PersonnelClassIDType>(PersonnelClassIDType.class, this, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_CLASS_ID);
		}
		return personnelClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonIDType> getPersonID() {
		if (personID == null) {
			personID = new EObjectContainmentEList<PersonIDType>(PersonIDType.class, this, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSON_ID);
		}
		return personID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityTypeType getCapabilityType() {
		return capabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCapabilityType(CapabilityTypeType newCapabilityType, NotificationChain msgs) {
		CapabilityTypeType oldCapabilityType = capabilityType;
		capabilityType = newCapabilityType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CAPABILITY_TYPE, oldCapabilityType, newCapabilityType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCapabilityType(CapabilityTypeType newCapabilityType) {
		if (newCapabilityType != capabilityType) {
			NotificationChain msgs = null;
			if (capabilityType != null)
				msgs = ((InternalEObject)capabilityType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CAPABILITY_TYPE, null, msgs);
			if (newCapabilityType != null)
				msgs = ((InternalEObject)newCapabilityType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CAPABILITY_TYPE, null, msgs);
			msgs = basicSetCapabilityType(newCapabilityType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CAPABILITY_TYPE, newCapabilityType, newCapabilityType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReasonType getReason() {
		return reason;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReason(ReasonType newReason, NotificationChain msgs) {
		ReasonType oldReason = reason;
		reason = newReason;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__REASON, oldReason, newReason);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReason(ReasonType newReason) {
		if (newReason != reason) {
			NotificationChain msgs = null;
			if (reason != null)
				msgs = ((InternalEObject)reason).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__REASON, null, msgs);
			if (newReason != null)
				msgs = ((InternalEObject)newReason).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__REASON, null, msgs);
			msgs = basicSetReason(newReason, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__REASON, newReason, newReason));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfidenceFactorType getConfidenceFactor() {
		return confidenceFactor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConfidenceFactor(ConfidenceFactorType newConfidenceFactor, NotificationChain msgs) {
		ConfidenceFactorType oldConfidenceFactor = confidenceFactor;
		confidenceFactor = newConfidenceFactor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CONFIDENCE_FACTOR, oldConfidenceFactor, newConfidenceFactor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfidenceFactor(ConfidenceFactorType newConfidenceFactor) {
		if (newConfidenceFactor != confidenceFactor) {
			NotificationChain msgs = null;
			if (confidenceFactor != null)
				msgs = ((InternalEObject)confidenceFactor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CONFIDENCE_FACTOR, null, msgs);
			if (newConfidenceFactor != null)
				msgs = ((InternalEObject)newConfidenceFactor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CONFIDENCE_FACTOR, null, msgs);
			msgs = basicSetConfidenceFactor(newConfidenceFactor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CONFIDENCE_FACTOR, newConfidenceFactor, newConfidenceFactor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelUseType getPersonnelUse() {
		return personnelUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPersonnelUse(PersonnelUseType newPersonnelUse, NotificationChain msgs) {
		PersonnelUseType oldPersonnelUse = personnelUse;
		personnelUse = newPersonnelUse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_USE, oldPersonnelUse, newPersonnelUse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonnelUse(PersonnelUseType newPersonnelUse) {
		if (newPersonnelUse != personnelUse) {
			NotificationChain msgs = null;
			if (personnelUse != null)
				msgs = ((InternalEObject)personnelUse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_USE, null, msgs);
			if (newPersonnelUse != null)
				msgs = ((InternalEObject)newPersonnelUse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_USE, null, msgs);
			msgs = basicSetPersonnelUse(newPersonnelUse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_USE, newPersonnelUse, newPersonnelUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StartTimeType getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStartTime(StartTimeType newStartTime, NotificationChain msgs) {
		StartTimeType oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__START_TIME, oldStartTime, newStartTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(StartTimeType newStartTime) {
		if (newStartTime != startTime) {
			NotificationChain msgs = null;
			if (startTime != null)
				msgs = ((InternalEObject)startTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__START_TIME, null, msgs);
			if (newStartTime != null)
				msgs = ((InternalEObject)newStartTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__START_TIME, null, msgs);
			msgs = basicSetStartTime(newStartTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__START_TIME, newStartTime, newStartTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EndTimeType getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEndTime(EndTimeType newEndTime, NotificationChain msgs) {
		EndTimeType oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__END_TIME, oldEndTime, newEndTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(EndTimeType newEndTime) {
		if (newEndTime != endTime) {
			NotificationChain msgs = null;
			if (endTime != null)
				msgs = ((InternalEObject)endTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__END_TIME, null, msgs);
			if (newEndTime != null)
				msgs = ((InternalEObject)newEndTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__END_TIME, null, msgs);
			msgs = basicSetEndTime(newEndTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__END_TIME, newEndTime, newEndTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPersonnelCapabilityPropertyType> getPersonnelCapabilityProperty() {
		if (personnelCapabilityProperty == null) {
			personnelCapabilityProperty = new EObjectContainmentEList<OpPersonnelCapabilityPropertyType>(OpPersonnelCapabilityPropertyType.class, this, B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_CAPABILITY_PROPERTY);
		}
		return personnelCapabilityProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_CLASS_ID:
				return ((InternalEList<?>)getPersonnelClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSON_ID:
				return ((InternalEList<?>)getPersonID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CAPABILITY_TYPE:
				return basicSetCapabilityType(null, msgs);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__REASON:
				return basicSetReason(null, msgs);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				return basicSetConfidenceFactor(null, msgs);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_USE:
				return basicSetPersonnelUse(null, msgs);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__START_TIME:
				return basicSetStartTime(null, msgs);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__END_TIME:
				return basicSetEndTime(null, msgs);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_CAPABILITY_PROPERTY:
				return ((InternalEList<?>)getPersonnelCapabilityProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_CLASS_ID:
				return getPersonnelClassID();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSON_ID:
				return getPersonID();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CAPABILITY_TYPE:
				return getCapabilityType();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__REASON:
				return getReason();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				return getConfidenceFactor();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_USE:
				return getPersonnelUse();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__START_TIME:
				return getStartTime();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__END_TIME:
				return getEndTime();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_CAPABILITY_PROPERTY:
				return getPersonnelCapabilityProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_CLASS_ID:
				getPersonnelClassID().clear();
				getPersonnelClassID().addAll((Collection<? extends PersonnelClassIDType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSON_ID:
				getPersonID().clear();
				getPersonID().addAll((Collection<? extends PersonIDType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CAPABILITY_TYPE:
				setCapabilityType((CapabilityTypeType)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__REASON:
				setReason((ReasonType)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				setConfidenceFactor((ConfidenceFactorType)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_USE:
				setPersonnelUse((PersonnelUseType)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__START_TIME:
				setStartTime((StartTimeType)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__END_TIME:
				setEndTime((EndTimeType)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_CAPABILITY_PROPERTY:
				getPersonnelCapabilityProperty().clear();
				getPersonnelCapabilityProperty().addAll((Collection<? extends OpPersonnelCapabilityPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_CLASS_ID:
				getPersonnelClassID().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSON_ID:
				getPersonID().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CAPABILITY_TYPE:
				setCapabilityType((CapabilityTypeType)null);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__REASON:
				setReason((ReasonType)null);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				setConfidenceFactor((ConfidenceFactorType)null);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_USE:
				setPersonnelUse((PersonnelUseType)null);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__START_TIME:
				setStartTime((StartTimeType)null);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__END_TIME:
				setEndTime((EndTimeType)null);
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_CAPABILITY_PROPERTY:
				getPersonnelCapabilityProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_CLASS_ID:
				return personnelClassID != null && !personnelClassID.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSON_ID:
				return personID != null && !personID.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CAPABILITY_TYPE:
				return capabilityType != null;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__REASON:
				return reason != null;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__CONFIDENCE_FACTOR:
				return confidenceFactor != null;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_USE:
				return personnelUse != null;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__START_TIME:
				return startTime != null;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__END_TIME:
				return endTime != null;
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE__PERSONNEL_CAPABILITY_PROPERTY:
				return personnelCapabilityProperty != null && !personnelCapabilityProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OpPersonnelCapabilityTypeImpl
