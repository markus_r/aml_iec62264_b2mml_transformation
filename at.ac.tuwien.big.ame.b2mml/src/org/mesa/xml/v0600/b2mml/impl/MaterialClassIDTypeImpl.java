/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.MaterialClassIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Class ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MaterialClassIDTypeImpl extends IdentifierTypeImpl implements MaterialClassIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialClassIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getMaterialClassIDType();
	}

} //MaterialClassIDTypeImpl
