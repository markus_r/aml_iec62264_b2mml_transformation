/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import javax.xml.datatype.Duration;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.Diagnostician;

import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.mesa.xml.v0600.b2mml.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class B2MMLFactoryImpl extends EFactoryImpl implements B2MMLFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static B2MMLFactory init() {
		try {
			B2MMLFactory theB2MMLFactory = (B2MMLFactory)EPackage.Registry.INSTANCE.getEFactory(B2MMLPackage.eNS_URI);
			if (theB2MMLFactory != null) {
				return theB2MMLFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new B2MMLFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public B2MMLFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case B2MMLPackage.ACKNOWLEDGE_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: return createAcknowledgeEquipmentCapabilityTestSpecType();
			case B2MMLPackage.ACKNOWLEDGE_EQUIPMENT_CLASS_TYPE: return createAcknowledgeEquipmentClassType();
			case B2MMLPackage.ACKNOWLEDGE_EQUIPMENT_INFORMATION_TYPE: return createAcknowledgeEquipmentInformationType();
			case B2MMLPackage.ACKNOWLEDGE_EQUIPMENT_TYPE: return createAcknowledgeEquipmentType();
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_CLASS_TYPE: return createAcknowledgeMaterialClassType();
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_DEFINITION_TYPE: return createAcknowledgeMaterialDefinitionType();
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_INFORMATION_TYPE: return createAcknowledgeMaterialInformationType();
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_LOT_TYPE: return createAcknowledgeMaterialLotType();
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_SUB_LOT_TYPE: return createAcknowledgeMaterialSubLotType();
			case B2MMLPackage.ACKNOWLEDGE_MATERIAL_TEST_SPEC_TYPE: return createAcknowledgeMaterialTestSpecType();
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_CAPABILITY_INFORMATION_TYPE: return createAcknowledgeOperationsCapabilityInformationType();
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_CAPABILITY_TYPE: return createAcknowledgeOperationsCapabilityType();
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_DEFINITION_INFORMATION_TYPE: return createAcknowledgeOperationsDefinitionInformationType();
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_DEFINITION_TYPE: return createAcknowledgeOperationsDefinitionType();
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_PERFORMANCE_TYPE: return createAcknowledgeOperationsPerformanceType();
			case B2MMLPackage.ACKNOWLEDGE_OPERATIONS_SCHEDULE_TYPE: return createAcknowledgeOperationsScheduleType();
			case B2MMLPackage.ACKNOWLEDGE_PERSONNEL_CLASS_TYPE: return createAcknowledgePersonnelClassType();
			case B2MMLPackage.ACKNOWLEDGE_PERSONNEL_INFORMATION_TYPE: return createAcknowledgePersonnelInformationType();
			case B2MMLPackage.ACKNOWLEDGE_PERSON_TYPE: return createAcknowledgePersonType();
			case B2MMLPackage.ACKNOWLEDGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: return createAcknowledgePhysicalAssetCapabilityTestSpecType();
			case B2MMLPackage.ACKNOWLEDGE_PHYSICAL_ASSET_CLASS_TYPE: return createAcknowledgePhysicalAssetClassType();
			case B2MMLPackage.ACKNOWLEDGE_PHYSICAL_ASSET_INFORMATION_TYPE: return createAcknowledgePhysicalAssetInformationType();
			case B2MMLPackage.ACKNOWLEDGE_PHYSICAL_ASSET_TYPE: return createAcknowledgePhysicalAssetType();
			case B2MMLPackage.ACKNOWLEDGE_PROCESS_SEGMENT_INFORMATION_TYPE: return createAcknowledgeProcessSegmentInformationType();
			case B2MMLPackage.ACKNOWLEDGE_PROCESS_SEGMENT_TYPE: return createAcknowledgeProcessSegmentType();
			case B2MMLPackage.ACKNOWLEDGE_QUALIFICATION_TEST_SPECIFICATION_TYPE: return createAcknowledgeQualificationTestSpecificationType();
			case B2MMLPackage.ACTUAL_END_TIME_TYPE: return createActualEndTimeType();
			case B2MMLPackage.ACTUAL_FINISH_TIME_TYPE: return createActualFinishTimeType();
			case B2MMLPackage.ACTUAL_START_TIME_TYPE: return createActualStartTimeType();
			case B2MMLPackage.AMOUNT_TYPE: return createAmountType();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE: return createAnyGenericValueType();
			case B2MMLPackage.ASSEMBLY_RELATIONSHIP1_TYPE: return createAssemblyRelationship1Type();
			case B2MMLPackage.ASSEMBLY_RELATIONSHIP_TYPE: return createAssemblyRelationshipType();
			case B2MMLPackage.ASSEMBLY_TYPE1_TYPE: return createAssemblyType1Type();
			case B2MMLPackage.ASSEMBLY_TYPE_TYPE: return createAssemblyTypeType();
			case B2MMLPackage.BILL_OF_MATERIAL_ID_TYPE: return createBillOfMaterialIDType();
			case B2MMLPackage.BILL_OF_MATERIALS_ID_TYPE: return createBillOfMaterialsIDType();
			case B2MMLPackage.BILL_OF_RESOURCES_ID_TYPE: return createBillOfResourcesIDType();
			case B2MMLPackage.BINARY_OBJECT_TYPE: return createBinaryObjectType();
			case B2MMLPackage.BOD_TYPE: return createBODType();
			case B2MMLPackage.CANCEL_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: return createCancelEquipmentCapabilityTestSpecType();
			case B2MMLPackage.CANCEL_EQUIPMENT_CLASS_TYPE: return createCancelEquipmentClassType();
			case B2MMLPackage.CANCEL_EQUIPMENT_INFORMATION_TYPE: return createCancelEquipmentInformationType();
			case B2MMLPackage.CANCEL_EQUIPMENT_TYPE: return createCancelEquipmentType();
			case B2MMLPackage.CANCEL_MATERIAL_CLASS_TYPE: return createCancelMaterialClassType();
			case B2MMLPackage.CANCEL_MATERIAL_DEFINITION_TYPE: return createCancelMaterialDefinitionType();
			case B2MMLPackage.CANCEL_MATERIAL_INFORMATION_TYPE: return createCancelMaterialInformationType();
			case B2MMLPackage.CANCEL_MATERIAL_LOT_TYPE: return createCancelMaterialLotType();
			case B2MMLPackage.CANCEL_MATERIAL_SUB_LOT_TYPE: return createCancelMaterialSubLotType();
			case B2MMLPackage.CANCEL_MATERIAL_TEST_SPEC_TYPE: return createCancelMaterialTestSpecType();
			case B2MMLPackage.CANCEL_OPERATIONS_CAPABILITY_INFORMATION_TYPE: return createCancelOperationsCapabilityInformationType();
			case B2MMLPackage.CANCEL_OPERATIONS_CAPABILITY_TYPE: return createCancelOperationsCapabilityType();
			case B2MMLPackage.CANCEL_OPERATIONS_DEFINITION_INFORMATION_TYPE: return createCancelOperationsDefinitionInformationType();
			case B2MMLPackage.CANCEL_OPERATIONS_DEFINITION_TYPE: return createCancelOperationsDefinitionType();
			case B2MMLPackage.CANCEL_OPERATIONS_PERFORMANCE_TYPE: return createCancelOperationsPerformanceType();
			case B2MMLPackage.CANCEL_OPERATIONS_SCHEDULE_TYPE: return createCancelOperationsScheduleType();
			case B2MMLPackage.CANCEL_PERSONNEL_CLASS_TYPE: return createCancelPersonnelClassType();
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE: return createCancelPersonnelInformationType();
			case B2MMLPackage.CANCEL_PERSON_TYPE: return createCancelPersonType();
			case B2MMLPackage.CANCEL_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: return createCancelPhysicalAssetCapabilityTestSpecType();
			case B2MMLPackage.CANCEL_PHYSICAL_ASSET_CLASS_TYPE: return createCancelPhysicalAssetClassType();
			case B2MMLPackage.CANCEL_PHYSICAL_ASSET_INFORMATION_TYPE: return createCancelPhysicalAssetInformationType();
			case B2MMLPackage.CANCEL_PHYSICAL_ASSET_TYPE: return createCancelPhysicalAssetType();
			case B2MMLPackage.CANCEL_PROCESS_SEGMENT_INFORMATION_TYPE: return createCancelProcessSegmentInformationType();
			case B2MMLPackage.CANCEL_PROCESS_SEGMENT_TYPE: return createCancelProcessSegmentType();
			case B2MMLPackage.CANCEL_QUALIFICATION_TEST_SPECIFICATION_TYPE: return createCancelQualificationTestSpecificationType();
			case B2MMLPackage.CAPABILITY_TYPE1_TYPE: return createCapabilityType1Type();
			case B2MMLPackage.CAPABILITY_TYPE_TYPE: return createCapabilityTypeType();
			case B2MMLPackage.CAUSE_TYPE: return createCauseType();
			case B2MMLPackage.CERTIFICATE_OF_ANALYSIS_REFERENCE_TYPE: return createCertificateOfAnalysisReferenceType();
			case B2MMLPackage.CHANGE_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: return createChangeEquipmentCapabilityTestSpecType();
			case B2MMLPackage.CHANGE_EQUIPMENT_CLASS_TYPE: return createChangeEquipmentClassType();
			case B2MMLPackage.CHANGE_EQUIPMENT_INFORMATION_TYPE: return createChangeEquipmentInformationType();
			case B2MMLPackage.CHANGE_EQUIPMENT_TYPE: return createChangeEquipmentType();
			case B2MMLPackage.CHANGE_MATERIAL_CLASS_TYPE: return createChangeMaterialClassType();
			case B2MMLPackage.CHANGE_MATERIAL_DEFINITION_TYPE: return createChangeMaterialDefinitionType();
			case B2MMLPackage.CHANGE_MATERIAL_INFORMATION_TYPE: return createChangeMaterialInformationType();
			case B2MMLPackage.CHANGE_MATERIAL_LOT_TYPE: return createChangeMaterialLotType();
			case B2MMLPackage.CHANGE_MATERIAL_SUB_LOT_TYPE: return createChangeMaterialSubLotType();
			case B2MMLPackage.CHANGE_MATERIAL_TEST_SPEC_TYPE: return createChangeMaterialTestSpecType();
			case B2MMLPackage.CHANGE_OPERATIONS_CAPABILITY_INFORMATION_TYPE: return createChangeOperationsCapabilityInformationType();
			case B2MMLPackage.CHANGE_OPERATIONS_CAPABILITY_TYPE: return createChangeOperationsCapabilityType();
			case B2MMLPackage.CHANGE_OPERATIONS_DEFINITION_INFORMATION_TYPE: return createChangeOperationsDefinitionInformationType();
			case B2MMLPackage.CHANGE_OPERATIONS_DEFINITION_TYPE: return createChangeOperationsDefinitionType();
			case B2MMLPackage.CHANGE_OPERATIONS_PERFORMANCE_TYPE: return createChangeOperationsPerformanceType();
			case B2MMLPackage.CHANGE_OPERATIONS_SCHEDULE_TYPE: return createChangeOperationsScheduleType();
			case B2MMLPackage.CHANGE_PERSONNEL_CLASS_TYPE: return createChangePersonnelClassType();
			case B2MMLPackage.CHANGE_PERSONNEL_INFORMATION_TYPE: return createChangePersonnelInformationType();
			case B2MMLPackage.CHANGE_PERSON_TYPE: return createChangePersonType();
			case B2MMLPackage.CHANGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: return createChangePhysicalAssetCapabilityTestSpecType();
			case B2MMLPackage.CHANGE_PHYSICAL_ASSET_CLASS_TYPE: return createChangePhysicalAssetClassType();
			case B2MMLPackage.CHANGE_PHYSICAL_ASSET_INFORMATION_TYPE: return createChangePhysicalAssetInformationType();
			case B2MMLPackage.CHANGE_PHYSICAL_ASSET_TYPE: return createChangePhysicalAssetType();
			case B2MMLPackage.CHANGE_PROCESS_SEGMENT_INFORMATION_TYPE: return createChangeProcessSegmentInformationType();
			case B2MMLPackage.CHANGE_PROCESS_SEGMENT_TYPE: return createChangeProcessSegmentType();
			case B2MMLPackage.CHANGE_QUALIFICATION_TEST_SPECIFICATION_TYPE: return createChangeQualificationTestSpecificationType();
			case B2MMLPackage.CODE_TYPE: return createCodeType();
			case B2MMLPackage.CONFIDENCE_FACTOR_TYPE: return createConfidenceFactorType();
			case B2MMLPackage.CONFIRM_BOD_TYPE: return createConfirmBODType();
			case B2MMLPackage.CORRECTION_TYPE: return createCorrectionType();
			case B2MMLPackage.DATA_AREA_TYPE: return createDataAreaType();
			case B2MMLPackage.DATA_AREA_TYPE1: return createDataAreaType1();
			case B2MMLPackage.DATA_AREA_TYPE2: return createDataAreaType2();
			case B2MMLPackage.DATA_AREA_TYPE3: return createDataAreaType3();
			case B2MMLPackage.DATA_AREA_TYPE4: return createDataAreaType4();
			case B2MMLPackage.DATA_AREA_TYPE5: return createDataAreaType5();
			case B2MMLPackage.DATA_AREA_TYPE6: return createDataAreaType6();
			case B2MMLPackage.DATA_AREA_TYPE7: return createDataAreaType7();
			case B2MMLPackage.DATA_AREA_TYPE8: return createDataAreaType8();
			case B2MMLPackage.DATA_AREA_TYPE9: return createDataAreaType9();
			case B2MMLPackage.DATA_AREA_TYPE10: return createDataAreaType10();
			case B2MMLPackage.DATA_AREA_TYPE11: return createDataAreaType11();
			case B2MMLPackage.DATA_AREA_TYPE12: return createDataAreaType12();
			case B2MMLPackage.DATA_AREA_TYPE13: return createDataAreaType13();
			case B2MMLPackage.DATA_AREA_TYPE14: return createDataAreaType14();
			case B2MMLPackage.DATA_AREA_TYPE15: return createDataAreaType15();
			case B2MMLPackage.DATA_AREA_TYPE16: return createDataAreaType16();
			case B2MMLPackage.DATA_AREA_TYPE17: return createDataAreaType17();
			case B2MMLPackage.DATA_AREA_TYPE18: return createDataAreaType18();
			case B2MMLPackage.DATA_AREA_TYPE19: return createDataAreaType19();
			case B2MMLPackage.DATA_AREA_TYPE20: return createDataAreaType20();
			case B2MMLPackage.DATA_AREA_TYPE21: return createDataAreaType21();
			case B2MMLPackage.DATA_AREA_TYPE22: return createDataAreaType22();
			case B2MMLPackage.DATA_AREA_TYPE23: return createDataAreaType23();
			case B2MMLPackage.DATA_AREA_TYPE24: return createDataAreaType24();
			case B2MMLPackage.DATA_AREA_TYPE25: return createDataAreaType25();
			case B2MMLPackage.DATA_AREA_TYPE26: return createDataAreaType26();
			case B2MMLPackage.DATA_AREA_TYPE27: return createDataAreaType27();
			case B2MMLPackage.DATA_AREA_TYPE28: return createDataAreaType28();
			case B2MMLPackage.DATA_AREA_TYPE29: return createDataAreaType29();
			case B2MMLPackage.DATA_AREA_TYPE30: return createDataAreaType30();
			case B2MMLPackage.DATA_AREA_TYPE31: return createDataAreaType31();
			case B2MMLPackage.DATA_AREA_TYPE32: return createDataAreaType32();
			case B2MMLPackage.DATA_AREA_TYPE33: return createDataAreaType33();
			case B2MMLPackage.DATA_AREA_TYPE34: return createDataAreaType34();
			case B2MMLPackage.DATA_AREA_TYPE35: return createDataAreaType35();
			case B2MMLPackage.DATA_AREA_TYPE36: return createDataAreaType36();
			case B2MMLPackage.DATA_AREA_TYPE37: return createDataAreaType37();
			case B2MMLPackage.DATA_AREA_TYPE38: return createDataAreaType38();
			case B2MMLPackage.DATA_AREA_TYPE39: return createDataAreaType39();
			case B2MMLPackage.DATA_AREA_TYPE40: return createDataAreaType40();
			case B2MMLPackage.DATA_AREA_TYPE41: return createDataAreaType41();
			case B2MMLPackage.DATA_AREA_TYPE42: return createDataAreaType42();
			case B2MMLPackage.DATA_AREA_TYPE43: return createDataAreaType43();
			case B2MMLPackage.DATA_AREA_TYPE44: return createDataAreaType44();
			case B2MMLPackage.DATA_AREA_TYPE45: return createDataAreaType45();
			case B2MMLPackage.DATA_AREA_TYPE46: return createDataAreaType46();
			case B2MMLPackage.DATA_AREA_TYPE47: return createDataAreaType47();
			case B2MMLPackage.DATA_AREA_TYPE48: return createDataAreaType48();
			case B2MMLPackage.DATA_AREA_TYPE49: return createDataAreaType49();
			case B2MMLPackage.DATA_AREA_TYPE50: return createDataAreaType50();
			case B2MMLPackage.DATA_AREA_TYPE51: return createDataAreaType51();
			case B2MMLPackage.DATA_AREA_TYPE52: return createDataAreaType52();
			case B2MMLPackage.DATA_AREA_TYPE53: return createDataAreaType53();
			case B2MMLPackage.DATA_AREA_TYPE54: return createDataAreaType54();
			case B2MMLPackage.DATA_AREA_TYPE55: return createDataAreaType55();
			case B2MMLPackage.DATA_AREA_TYPE56: return createDataAreaType56();
			case B2MMLPackage.DATA_AREA_TYPE57: return createDataAreaType57();
			case B2MMLPackage.DATA_AREA_TYPE58: return createDataAreaType58();
			case B2MMLPackage.DATA_AREA_TYPE59: return createDataAreaType59();
			case B2MMLPackage.DATA_AREA_TYPE60: return createDataAreaType60();
			case B2MMLPackage.DATA_AREA_TYPE61: return createDataAreaType61();
			case B2MMLPackage.DATA_AREA_TYPE62: return createDataAreaType62();
			case B2MMLPackage.DATA_AREA_TYPE63: return createDataAreaType63();
			case B2MMLPackage.DATA_AREA_TYPE64: return createDataAreaType64();
			case B2MMLPackage.DATA_AREA_TYPE65: return createDataAreaType65();
			case B2MMLPackage.DATA_AREA_TYPE66: return createDataAreaType66();
			case B2MMLPackage.DATA_AREA_TYPE67: return createDataAreaType67();
			case B2MMLPackage.DATA_AREA_TYPE68: return createDataAreaType68();
			case B2MMLPackage.DATA_AREA_TYPE69: return createDataAreaType69();
			case B2MMLPackage.DATA_AREA_TYPE70: return createDataAreaType70();
			case B2MMLPackage.DATA_AREA_TYPE71: return createDataAreaType71();
			case B2MMLPackage.DATA_AREA_TYPE72: return createDataAreaType72();
			case B2MMLPackage.DATA_AREA_TYPE73: return createDataAreaType73();
			case B2MMLPackage.DATA_AREA_TYPE74: return createDataAreaType74();
			case B2MMLPackage.DATA_AREA_TYPE75: return createDataAreaType75();
			case B2MMLPackage.DATA_AREA_TYPE76: return createDataAreaType76();
			case B2MMLPackage.DATA_AREA_TYPE77: return createDataAreaType77();
			case B2MMLPackage.DATA_AREA_TYPE78: return createDataAreaType78();
			case B2MMLPackage.DATA_AREA_TYPE79: return createDataAreaType79();
			case B2MMLPackage.DATA_AREA_TYPE80: return createDataAreaType80();
			case B2MMLPackage.DATA_AREA_TYPE81: return createDataAreaType81();
			case B2MMLPackage.DATA_AREA_TYPE82: return createDataAreaType82();
			case B2MMLPackage.DATA_AREA_TYPE83: return createDataAreaType83();
			case B2MMLPackage.DATA_AREA_TYPE84: return createDataAreaType84();
			case B2MMLPackage.DATA_AREA_TYPE85: return createDataAreaType85();
			case B2MMLPackage.DATA_AREA_TYPE86: return createDataAreaType86();
			case B2MMLPackage.DATA_AREA_TYPE87: return createDataAreaType87();
			case B2MMLPackage.DATA_AREA_TYPE88: return createDataAreaType88();
			case B2MMLPackage.DATA_AREA_TYPE89: return createDataAreaType89();
			case B2MMLPackage.DATA_AREA_TYPE90: return createDataAreaType90();
			case B2MMLPackage.DATA_AREA_TYPE91: return createDataAreaType91();
			case B2MMLPackage.DATA_AREA_TYPE92: return createDataAreaType92();
			case B2MMLPackage.DATA_AREA_TYPE93: return createDataAreaType93();
			case B2MMLPackage.DATA_AREA_TYPE94: return createDataAreaType94();
			case B2MMLPackage.DATA_AREA_TYPE95: return createDataAreaType95();
			case B2MMLPackage.DATA_AREA_TYPE96: return createDataAreaType96();
			case B2MMLPackage.DATA_AREA_TYPE97: return createDataAreaType97();
			case B2MMLPackage.DATA_AREA_TYPE98: return createDataAreaType98();
			case B2MMLPackage.DATA_AREA_TYPE99: return createDataAreaType99();
			case B2MMLPackage.DATA_AREA_TYPE100: return createDataAreaType100();
			case B2MMLPackage.DATA_AREA_TYPE101: return createDataAreaType101();
			case B2MMLPackage.DATA_AREA_TYPE102: return createDataAreaType102();
			case B2MMLPackage.DATA_AREA_TYPE103: return createDataAreaType103();
			case B2MMLPackage.DATA_AREA_TYPE104: return createDataAreaType104();
			case B2MMLPackage.DATA_AREA_TYPE105: return createDataAreaType105();
			case B2MMLPackage.DATA_AREA_TYPE106: return createDataAreaType106();
			case B2MMLPackage.DATA_AREA_TYPE107: return createDataAreaType107();
			case B2MMLPackage.DATA_AREA_TYPE108: return createDataAreaType108();
			case B2MMLPackage.DATA_AREA_TYPE109: return createDataAreaType109();
			case B2MMLPackage.DATA_AREA_TYPE110: return createDataAreaType110();
			case B2MMLPackage.DATA_AREA_TYPE111: return createDataAreaType111();
			case B2MMLPackage.DATA_AREA_TYPE112: return createDataAreaType112();
			case B2MMLPackage.DATA_AREA_TYPE113: return createDataAreaType113();
			case B2MMLPackage.DATA_AREA_TYPE114: return createDataAreaType114();
			case B2MMLPackage.DATA_AREA_TYPE115: return createDataAreaType115();
			case B2MMLPackage.DATA_AREA_TYPE116: return createDataAreaType116();
			case B2MMLPackage.DATA_AREA_TYPE117: return createDataAreaType117();
			case B2MMLPackage.DATA_AREA_TYPE118: return createDataAreaType118();
			case B2MMLPackage.DATA_AREA_TYPE119: return createDataAreaType119();
			case B2MMLPackage.DATA_AREA_TYPE120: return createDataAreaType120();
			case B2MMLPackage.DATA_AREA_TYPE121: return createDataAreaType121();
			case B2MMLPackage.DATA_AREA_TYPE122: return createDataAreaType122();
			case B2MMLPackage.DATA_AREA_TYPE123: return createDataAreaType123();
			case B2MMLPackage.DATA_AREA_TYPE124: return createDataAreaType124();
			case B2MMLPackage.DATA_AREA_TYPE125: return createDataAreaType125();
			case B2MMLPackage.DATA_AREA_TYPE126: return createDataAreaType126();
			case B2MMLPackage.DATA_AREA_TYPE127: return createDataAreaType127();
			case B2MMLPackage.DATA_AREA_TYPE128: return createDataAreaType128();
			case B2MMLPackage.DATA_AREA_TYPE129: return createDataAreaType129();
			case B2MMLPackage.DATA_AREA_TYPE130: return createDataAreaType130();
			case B2MMLPackage.DATA_AREA_TYPE131: return createDataAreaType131();
			case B2MMLPackage.DATA_AREA_TYPE132: return createDataAreaType132();
			case B2MMLPackage.DATA_AREA_TYPE133: return createDataAreaType133();
			case B2MMLPackage.DATA_AREA_TYPE134: return createDataAreaType134();
			case B2MMLPackage.DATA_AREA_TYPE135: return createDataAreaType135();
			case B2MMLPackage.DATA_AREA_TYPE136: return createDataAreaType136();
			case B2MMLPackage.DATA_AREA_TYPE137: return createDataAreaType137();
			case B2MMLPackage.DATA_AREA_TYPE138: return createDataAreaType138();
			case B2MMLPackage.DATA_AREA_TYPE139: return createDataAreaType139();
			case B2MMLPackage.DATA_AREA_TYPE140: return createDataAreaType140();
			case B2MMLPackage.DATA_AREA_TYPE141: return createDataAreaType141();
			case B2MMLPackage.DATA_AREA_TYPE142: return createDataAreaType142();
			case B2MMLPackage.DATA_AREA_TYPE143: return createDataAreaType143();
			case B2MMLPackage.DATA_AREA_TYPE144: return createDataAreaType144();
			case B2MMLPackage.DATA_AREA_TYPE145: return createDataAreaType145();
			case B2MMLPackage.DATA_AREA_TYPE146: return createDataAreaType146();
			case B2MMLPackage.DATA_AREA_TYPE147: return createDataAreaType147();
			case B2MMLPackage.DATA_AREA_TYPE148: return createDataAreaType148();
			case B2MMLPackage.DATA_AREA_TYPE149: return createDataAreaType149();
			case B2MMLPackage.DATA_AREA_TYPE150: return createDataAreaType150();
			case B2MMLPackage.DATA_AREA_TYPE151: return createDataAreaType151();
			case B2MMLPackage.DATA_AREA_TYPE152: return createDataAreaType152();
			case B2MMLPackage.DATA_AREA_TYPE153: return createDataAreaType153();
			case B2MMLPackage.DATA_AREA_TYPE154: return createDataAreaType154();
			case B2MMLPackage.DATA_AREA_TYPE155: return createDataAreaType155();
			case B2MMLPackage.DATA_AREA_TYPE156: return createDataAreaType156();
			case B2MMLPackage.DATA_AREA_TYPE157: return createDataAreaType157();
			case B2MMLPackage.DATA_AREA_TYPE158: return createDataAreaType158();
			case B2MMLPackage.DATA_AREA_TYPE159: return createDataAreaType159();
			case B2MMLPackage.DATA_AREA_TYPE160: return createDataAreaType160();
			case B2MMLPackage.DATA_AREA_TYPE161: return createDataAreaType161();
			case B2MMLPackage.DATA_AREA_TYPE162: return createDataAreaType162();
			case B2MMLPackage.DATA_AREA_TYPE163: return createDataAreaType163();
			case B2MMLPackage.DATA_AREA_TYPE164: return createDataAreaType164();
			case B2MMLPackage.DATA_AREA_TYPE165: return createDataAreaType165();
			case B2MMLPackage.DATA_AREA_TYPE166: return createDataAreaType166();
			case B2MMLPackage.DATA_AREA_TYPE167: return createDataAreaType167();
			case B2MMLPackage.DATA_AREA_TYPE168: return createDataAreaType168();
			case B2MMLPackage.DATA_AREA_TYPE169: return createDataAreaType169();
			case B2MMLPackage.DATA_AREA_TYPE170: return createDataAreaType170();
			case B2MMLPackage.DATA_AREA_TYPE171: return createDataAreaType171();
			case B2MMLPackage.DATA_AREA_TYPE172: return createDataAreaType172();
			case B2MMLPackage.DATA_AREA_TYPE173: return createDataAreaType173();
			case B2MMLPackage.DATA_AREA_TYPE174: return createDataAreaType174();
			case B2MMLPackage.DATA_AREA_TYPE175: return createDataAreaType175();
			case B2MMLPackage.DATA_AREA_TYPE176: return createDataAreaType176();
			case B2MMLPackage.DATA_AREA_TYPE177: return createDataAreaType177();
			case B2MMLPackage.DATA_AREA_TYPE178: return createDataAreaType178();
			case B2MMLPackage.DATA_AREA_TYPE179: return createDataAreaType179();
			case B2MMLPackage.DATA_AREA_TYPE180: return createDataAreaType180();
			case B2MMLPackage.DATA_AREA_TYPE181: return createDataAreaType181();
			case B2MMLPackage.DATA_AREA_TYPE182: return createDataAreaType182();
			case B2MMLPackage.DATA_AREA_TYPE183: return createDataAreaType183();
			case B2MMLPackage.DATA_AREA_TYPE184: return createDataAreaType184();
			case B2MMLPackage.DATA_AREA_TYPE185: return createDataAreaType185();
			case B2MMLPackage.DATA_AREA_TYPE186: return createDataAreaType186();
			case B2MMLPackage.DATA_AREA_TYPE187: return createDataAreaType187();
			case B2MMLPackage.DATA_AREA_TYPE188: return createDataAreaType188();
			case B2MMLPackage.DATA_AREA_TYPE189: return createDataAreaType189();
			case B2MMLPackage.DATA_AREA_TYPE190: return createDataAreaType190();
			case B2MMLPackage.DATA_AREA_TYPE191: return createDataAreaType191();
			case B2MMLPackage.DATA_AREA_TYPE192: return createDataAreaType192();
			case B2MMLPackage.DATA_AREA_TYPE193: return createDataAreaType193();
			case B2MMLPackage.DATA_AREA_TYPE194: return createDataAreaType194();
			case B2MMLPackage.DATA_AREA_TYPE195: return createDataAreaType195();
			case B2MMLPackage.DATA_AREA_TYPE196: return createDataAreaType196();
			case B2MMLPackage.DATA_AREA_TYPE197: return createDataAreaType197();
			case B2MMLPackage.DATA_AREA_TYPE198: return createDataAreaType198();
			case B2MMLPackage.DATA_AREA_TYPE199: return createDataAreaType199();
			case B2MMLPackage.DATA_AREA_TYPE200: return createDataAreaType200();
			case B2MMLPackage.DATA_AREA_TYPE201: return createDataAreaType201();
			case B2MMLPackage.DATA_AREA_TYPE202: return createDataAreaType202();
			case B2MMLPackage.DATA_AREA_TYPE203: return createDataAreaType203();
			case B2MMLPackage.DATA_AREA_TYPE204: return createDataAreaType204();
			case B2MMLPackage.DATA_AREA_TYPE205: return createDataAreaType205();
			case B2MMLPackage.DATA_AREA_TYPE206: return createDataAreaType206();
			case B2MMLPackage.DATA_AREA_TYPE207: return createDataAreaType207();
			case B2MMLPackage.DATA_AREA_TYPE208: return createDataAreaType208();
			case B2MMLPackage.DATA_TYPE1_TYPE: return createDataType1Type();
			case B2MMLPackage.DATA_TYPE_TYPE: return createDataTypeType();
			case B2MMLPackage.DATE_TIME_TYPE: return createDateTimeType();
			case B2MMLPackage.DEPENDENCY1_TYPE: return createDependency1Type();
			case B2MMLPackage.DEPENDENCY_TYPE: return createDependencyType();
			case B2MMLPackage.DESCRIPTION_TYPE: return createDescriptionType();
			case B2MMLPackage.DOCUMENT_ROOT: return createDocumentRoot();
			case B2MMLPackage.EARLIEST_START_TIME_TYPE: return createEarliestStartTimeType();
			case B2MMLPackage.END_TIME_TYPE: return createEndTimeType();
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE: return createEquipmentAssetMappingType();
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID_TYPE: return createEquipmentCapabilityTestSpecificationIDType();
			case B2MMLPackage.EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_TYPE: return createEquipmentCapabilityTestSpecificationType();
			case B2MMLPackage.EQUIPMENT_CLASS_ID_TYPE: return createEquipmentClassIDType();
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE: return createEquipmentClassPropertyType();
			case B2MMLPackage.EQUIPMENT_CLASS_TYPE: return createEquipmentClassType();
			case B2MMLPackage.EQUIPMENT_ELEMENT_LEVEL1_TYPE: return createEquipmentElementLevel1Type();
			case B2MMLPackage.EQUIPMENT_ELEMENT_LEVEL_TYPE: return createEquipmentElementLevelType();
			case B2MMLPackage.EQUIPMENT_ID_TYPE: return createEquipmentIDType();
			case B2MMLPackage.EQUIPMENT_INFORMATION_TYPE: return createEquipmentInformationType();
			case B2MMLPackage.EQUIPMENT_PROPERTY_TYPE: return createEquipmentPropertyType();
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_PROPERTY_TYPE: return createEquipmentSegmentSpecificationPropertyType();
			case B2MMLPackage.EQUIPMENT_SEGMENT_SPECIFICATION_TYPE: return createEquipmentSegmentSpecificationType();
			case B2MMLPackage.EQUIPMENT_TYPE: return createEquipmentType();
			case B2MMLPackage.EQUIPMENT_USE_TYPE: return createEquipmentUseType();
			case B2MMLPackage.EXPIRATION_TIME_TYPE: return createExpirationTimeType();
			case B2MMLPackage.GET_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: return createGetEquipmentCapabilityTestSpecType();
			case B2MMLPackage.GET_EQUIPMENT_CLASS_TYPE: return createGetEquipmentClassType();
			case B2MMLPackage.GET_EQUIPMENT_INFORMATION_TYPE: return createGetEquipmentInformationType();
			case B2MMLPackage.GET_EQUIPMENT_TYPE: return createGetEquipmentType();
			case B2MMLPackage.GET_MATERIAL_CLASS_TYPE: return createGetMaterialClassType();
			case B2MMLPackage.GET_MATERIAL_DEFINITION_TYPE: return createGetMaterialDefinitionType();
			case B2MMLPackage.GET_MATERIAL_INFORMATION_TYPE: return createGetMaterialInformationType();
			case B2MMLPackage.GET_MATERIAL_LOT_TYPE: return createGetMaterialLotType();
			case B2MMLPackage.GET_MATERIAL_SUB_LOT_TYPE: return createGetMaterialSubLotType();
			case B2MMLPackage.GET_MATERIAL_TEST_SPEC_TYPE: return createGetMaterialTestSpecType();
			case B2MMLPackage.GET_OPERATIONS_CAPABILITY_INFORMATION_TYPE: return createGetOperationsCapabilityInformationType();
			case B2MMLPackage.GET_OPERATIONS_CAPABILITY_TYPE: return createGetOperationsCapabilityType();
			case B2MMLPackage.GET_OPERATIONS_DEFINITION_INFORMATION_TYPE: return createGetOperationsDefinitionInformationType();
			case B2MMLPackage.GET_OPERATIONS_DEFINITION_TYPE: return createGetOperationsDefinitionType();
			case B2MMLPackage.GET_OPERATIONS_PERFORMANCE_TYPE: return createGetOperationsPerformanceType();
			case B2MMLPackage.GET_OPERATIONS_SCHEDULE_TYPE: return createGetOperationsScheduleType();
			case B2MMLPackage.GET_PERSONNEL_CLASS_TYPE: return createGetPersonnelClassType();
			case B2MMLPackage.GET_PERSONNEL_INFORMATION_TYPE: return createGetPersonnelInformationType();
			case B2MMLPackage.GET_PERSON_TYPE: return createGetPersonType();
			case B2MMLPackage.GET_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: return createGetPhysicalAssetCapabilityTestSpecType();
			case B2MMLPackage.GET_PHYSICAL_ASSET_CLASS_TYPE: return createGetPhysicalAssetClassType();
			case B2MMLPackage.GET_PHYSICAL_ASSET_INFORMATION_TYPE: return createGetPhysicalAssetInformationType();
			case B2MMLPackage.GET_PHYSICAL_ASSET_TYPE: return createGetPhysicalAssetType();
			case B2MMLPackage.GET_PROCESS_SEGMENT_INFORMATION_TYPE: return createGetProcessSegmentInformationType();
			case B2MMLPackage.GET_PROCESS_SEGMENT_TYPE: return createGetProcessSegmentType();
			case B2MMLPackage.GET_QUALIFICATION_TEST_SPECIFICATION_TYPE: return createGetQualificationTestSpecificationType();
			case B2MMLPackage.HIERARCHY_SCOPE_TYPE: return createHierarchyScopeType();
			case B2MMLPackage.IDENTIFIER_TYPE: return createIdentifierType();
			case B2MMLPackage.JOB_ORDER_COMMAND1_TYPE: return createJobOrderCommand1Type();
			case B2MMLPackage.JOB_ORDER_COMMAND_RULE_TYPE: return createJobOrderCommandRuleType();
			case B2MMLPackage.JOB_ORDER_COMMAND_TYPE: return createJobOrderCommandType();
			case B2MMLPackage.JOB_ORDER_DISPATCH_STATUS_TYPE: return createJobOrderDispatchStatusType();
			case B2MMLPackage.LATEST_END_TIME_TYPE: return createLatestEndTimeType();
			case B2MMLPackage.LOCATION_TYPE: return createLocationType();
			case B2MMLPackage.MANUFACTURING_BILL_ID_TYPE: return createManufacturingBillIDType();
			case B2MMLPackage.MATERIAL_ACTUAL_ID_TYPE: return createMaterialActualIDType();
			case B2MMLPackage.MATERIAL_CAPABILITY_ID_TYPE: return createMaterialCapabilityIDType();
			case B2MMLPackage.MATERIAL_CLASS_ID_TYPE: return createMaterialClassIDType();
			case B2MMLPackage.MATERIAL_CLASS_PROPERTY_TYPE: return createMaterialClassPropertyType();
			case B2MMLPackage.MATERIAL_CLASS_TYPE: return createMaterialClassType();
			case B2MMLPackage.MATERIAL_DEFINITION_ID_TYPE: return createMaterialDefinitionIDType();
			case B2MMLPackage.MATERIAL_DEFINITION_PROPERTY_TYPE: return createMaterialDefinitionPropertyType();
			case B2MMLPackage.MATERIAL_DEFINITION_TYPE: return createMaterialDefinitionType();
			case B2MMLPackage.MATERIAL_INFORMATION_TYPE: return createMaterialInformationType();
			case B2MMLPackage.MATERIAL_LOT_ID_TYPE: return createMaterialLotIDType();
			case B2MMLPackage.MATERIAL_LOT_PROPERTY_TYPE: return createMaterialLotPropertyType();
			case B2MMLPackage.MATERIAL_LOT_TYPE: return createMaterialLotType();
			case B2MMLPackage.MATERIAL_REQUIREMENT_ID_TYPE: return createMaterialRequirementIDType();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_PROPERTY_TYPE: return createMaterialSegmentSpecificationPropertyType();
			case B2MMLPackage.MATERIAL_SEGMENT_SPECIFICATION_TYPE: return createMaterialSegmentSpecificationType();
			case B2MMLPackage.MATERIAL_SPECIFICATION_ID_TYPE: return createMaterialSpecificationIDType();
			case B2MMLPackage.MATERIAL_SUB_LOT_ID_TYPE: return createMaterialSubLotIDType();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE: return createMaterialSubLotType();
			case B2MMLPackage.MATERIAL_TEST_SPECIFICATION_ID_TYPE: return createMaterialTestSpecificationIDType();
			case B2MMLPackage.MATERIAL_TEST_SPECIFICATION_TYPE: return createMaterialTestSpecificationType();
			case B2MMLPackage.MATERIAL_USE1_TYPE: return createMaterialUse1Type();
			case B2MMLPackage.MATERIAL_USE_TYPE: return createMaterialUseType();
			case B2MMLPackage.MEASURE_TYPE: return createMeasureType();
			case B2MMLPackage.NAME_TYPE: return createNameType();
			case B2MMLPackage.NUMERIC_TYPE: return createNumericType();
			case B2MMLPackage.OP_EQUIPMENT_ACTUAL_PROPERTY_TYPE: return createOpEquipmentActualPropertyType();
			case B2MMLPackage.OP_EQUIPMENT_ACTUAL_TYPE: return createOpEquipmentActualType();
			case B2MMLPackage.OP_EQUIPMENT_CAPABILITY_PROPERTY_TYPE: return createOpEquipmentCapabilityPropertyType();
			case B2MMLPackage.OP_EQUIPMENT_CAPABILITY_TYPE: return createOpEquipmentCapabilityType();
			case B2MMLPackage.OP_EQUIPMENT_REQUIREMENT_PROPERTY_TYPE: return createOpEquipmentRequirementPropertyType();
			case B2MMLPackage.OP_EQUIPMENT_REQUIREMENT_TYPE: return createOpEquipmentRequirementType();
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_PROPERTY_TYPE: return createOpEquipmentSpecificationPropertyType();
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE: return createOpEquipmentSpecificationType();
			case B2MMLPackage.OPERATIONS_CAPABILITY_INFORMATION_TYPE: return createOperationsCapabilityInformationType();
			case B2MMLPackage.OPERATIONS_CAPABILITY_TYPE: return createOperationsCapabilityType();
			case B2MMLPackage.OPERATIONS_DEFINITION_ID_TYPE: return createOperationsDefinitionIDType();
			case B2MMLPackage.OPERATIONS_DEFINITION_INFORMATION_TYPE: return createOperationsDefinitionInformationType();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE: return createOperationsDefinitionType();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_ITEM_TYPE: return createOperationsMaterialBillItemType();
			case B2MMLPackage.OPERATIONS_MATERIAL_BILL_TYPE: return createOperationsMaterialBillType();
			case B2MMLPackage.OPERATIONS_PERFORMANCE_TYPE: return createOperationsPerformanceType();
			case B2MMLPackage.OPERATIONS_REQUEST_ID_TYPE: return createOperationsRequestIDType();
			case B2MMLPackage.OPERATIONS_REQUEST_TYPE: return createOperationsRequestType();
			case B2MMLPackage.OPERATIONS_RESPONSE_TYPE: return createOperationsResponseType();
			case B2MMLPackage.OPERATIONS_SCHEDULE_ID_TYPE: return createOperationsScheduleIDType();
			case B2MMLPackage.OPERATIONS_SCHEDULE_TYPE: return createOperationsScheduleType();
			case B2MMLPackage.OPERATIONS_SEGMENT_ID_TYPE: return createOperationsSegmentIDType();
			case B2MMLPackage.OPERATIONS_SEGMENT_TYPE: return createOperationsSegmentType();
			case B2MMLPackage.OPERATIONS_TYPE1_TYPE: return createOperationsType1Type();
			case B2MMLPackage.OPERATIONS_TYPE_TYPE: return createOperationsTypeType();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE: return createOpMaterialActualPropertyType();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE: return createOpMaterialActualType();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_PROPERTY_TYPE: return createOpMaterialCapabilityPropertyType();
			case B2MMLPackage.OP_MATERIAL_CAPABILITY_TYPE: return createOpMaterialCapabilityType();
			case B2MMLPackage.OP_MATERIAL_REQUIREMENT_PROPERTY_TYPE: return createOpMaterialRequirementPropertyType();
			case B2MMLPackage.OP_MATERIAL_REQUIREMENT_TYPE: return createOpMaterialRequirementType();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_PROPERTY_TYPE: return createOpMaterialSpecificationPropertyType();
			case B2MMLPackage.OP_MATERIAL_SPECIFICATION_TYPE: return createOpMaterialSpecificationType();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_PROPERTY_TYPE: return createOpPersonnelActualPropertyType();
			case B2MMLPackage.OP_PERSONNEL_ACTUAL_TYPE: return createOpPersonnelActualType();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_PROPERTY_TYPE: return createOpPersonnelCapabilityPropertyType();
			case B2MMLPackage.OP_PERSONNEL_CAPABILITY_TYPE: return createOpPersonnelCapabilityType();
			case B2MMLPackage.OP_PERSONNEL_REQUIREMENT_PROPERTY_TYPE: return createOpPersonnelRequirementPropertyType();
			case B2MMLPackage.OP_PERSONNEL_REQUIREMENT_TYPE: return createOpPersonnelRequirementType();
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_PROPERTY_TYPE: return createOpPersonnelSpecificationPropertyType();
			case B2MMLPackage.OP_PERSONNEL_SPECIFICATION_TYPE: return createOpPersonnelSpecificationType();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_PROPERTY_TYPE: return createOpPhysicalAssetActualPropertyType();
			case B2MMLPackage.OP_PHYSICAL_ASSET_ACTUAL_TYPE: return createOpPhysicalAssetActualType();
			case B2MMLPackage.OP_PHYSICAL_ASSET_CAPABILITY_PROPERTY_TYPE: return createOpPhysicalAssetCapabilityPropertyType();
			case B2MMLPackage.OP_PHYSICAL_ASSET_CAPABILITY_TYPE: return createOpPhysicalAssetCapabilityType();
			case B2MMLPackage.OP_PHYSICAL_ASSET_REQUIREMENT_PROPERTY_TYPE: return createOpPhysicalAssetRequirementPropertyType();
			case B2MMLPackage.OP_PHYSICAL_ASSET_REQUIREMENT_TYPE: return createOpPhysicalAssetRequirementType();
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_PROPERTY_TYPE: return createOpPhysicalAssetSpecificationPropertyType();
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE: return createOpPhysicalAssetSpecificationType();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE: return createOpProcessSegmentCapabilityType();
			case B2MMLPackage.OP_SEGMENT_DATA_TYPE: return createOpSegmentDataType();
			case B2MMLPackage.OP_SEGMENT_REQUIREMENT_TYPE: return createOpSegmentRequirementType();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE: return createOpSegmentResponseType();
			case B2MMLPackage.OTHER_DEPENDENCY_TYPE: return createOtherDependencyType();
			case B2MMLPackage.PARAMETER_ID_TYPE: return createParameterIDType();
			case B2MMLPackage.PARAMETER_TYPE: return createParameterType();
			case B2MMLPackage.PERSON_ID_TYPE: return createPersonIDType();
			case B2MMLPackage.PERSON_NAME_TYPE: return createPersonNameType();
			case B2MMLPackage.PERSONNEL_CLASS_ID_TYPE: return createPersonnelClassIDType();
			case B2MMLPackage.PERSONNEL_CLASS_PROPERTY_TYPE: return createPersonnelClassPropertyType();
			case B2MMLPackage.PERSONNEL_CLASS_TYPE: return createPersonnelClassType();
			case B2MMLPackage.PERSONNEL_INFORMATION_TYPE: return createPersonnelInformationType();
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_PROPERTY_TYPE: return createPersonnelSegmentSpecificationPropertyType();
			case B2MMLPackage.PERSONNEL_SEGMENT_SPECIFICATION_TYPE: return createPersonnelSegmentSpecificationType();
			case B2MMLPackage.PERSONNEL_USE_TYPE: return createPersonnelUseType();
			case B2MMLPackage.PERSON_PROPERTY_TYPE: return createPersonPropertyType();
			case B2MMLPackage.PERSON_TYPE: return createPersonType();
			case B2MMLPackage.PHYSICAL_ASSET_ACTUAL_ID_TYPE: return createPhysicalAssetActualIDType();
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID_TYPE: return createPhysicalAssetCapabilityTestSpecificationIDType();
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE: return createPhysicalAssetCapabilityTestSpecificationType();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_ID_TYPE: return createPhysicalAssetClassIDType();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_PROPERTY_TYPE: return createPhysicalAssetClassPropertyType();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE: return createPhysicalAssetClassType();
			case B2MMLPackage.PHYSICAL_ASSET_ID_TYPE: return createPhysicalAssetIDType();
			case B2MMLPackage.PHYSICAL_ASSET_INFORMATION_TYPE: return createPhysicalAssetInformationType();
			case B2MMLPackage.PHYSICAL_ASSET_PROPERTY_TYPE: return createPhysicalAssetPropertyType();
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY_TYPE: return createPhysicalAssetSegmentSpecificationPropertyType();
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE: return createPhysicalAssetSegmentSpecificationType();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE: return createPhysicalAssetType();
			case B2MMLPackage.PHYSICAL_ASSET_USE_TYPE: return createPhysicalAssetUseType();
			case B2MMLPackage.PLANNED_FINISH_TIME_TYPE: return createPlannedFinishTimeType();
			case B2MMLPackage.PLANNED_START_TIME_TYPE: return createPlannedStartTimeType();
			case B2MMLPackage.PRIORITY_TYPE: return createPriorityType();
			case B2MMLPackage.PROBLEM_TYPE: return createProblemType();
			case B2MMLPackage.PROCESS_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: return createProcessEquipmentCapabilityTestSpecType();
			case B2MMLPackage.PROCESS_EQUIPMENT_CLASS_TYPE: return createProcessEquipmentClassType();
			case B2MMLPackage.PROCESS_EQUIPMENT_INFORMATION_TYPE: return createProcessEquipmentInformationType();
			case B2MMLPackage.PROCESS_EQUIPMENT_TYPE: return createProcessEquipmentType();
			case B2MMLPackage.PROCESS_MATERIAL_CLASS_TYPE: return createProcessMaterialClassType();
			case B2MMLPackage.PROCESS_MATERIAL_DEFINITION_TYPE: return createProcessMaterialDefinitionType();
			case B2MMLPackage.PROCESS_MATERIAL_INFORMATION_TYPE: return createProcessMaterialInformationType();
			case B2MMLPackage.PROCESS_MATERIAL_LOT_TYPE: return createProcessMaterialLotType();
			case B2MMLPackage.PROCESS_MATERIAL_SUB_LOT_TYPE: return createProcessMaterialSubLotType();
			case B2MMLPackage.PROCESS_MATERIAL_TEST_SPEC_TYPE: return createProcessMaterialTestSpecType();
			case B2MMLPackage.PROCESS_OPERATIONS_CAPABILITY_INFORMATION_TYPE: return createProcessOperationsCapabilityInformationType();
			case B2MMLPackage.PROCESS_OPERATIONS_CAPABILITY_TYPE: return createProcessOperationsCapabilityType();
			case B2MMLPackage.PROCESS_OPERATIONS_DEFINITION_INFORMATION_TYPE: return createProcessOperationsDefinitionInformationType();
			case B2MMLPackage.PROCESS_OPERATIONS_DEFINITION_TYPE: return createProcessOperationsDefinitionType();
			case B2MMLPackage.PROCESS_OPERATIONS_PERFORMANCE_TYPE: return createProcessOperationsPerformanceType();
			case B2MMLPackage.PROCESS_OPERATIONS_SCHEDULE_TYPE: return createProcessOperationsScheduleType();
			case B2MMLPackage.PROCESS_PERSONNEL_CLASS_TYPE: return createProcessPersonnelClassType();
			case B2MMLPackage.PROCESS_PERSONNEL_INFORMATION_TYPE: return createProcessPersonnelInformationType();
			case B2MMLPackage.PROCESS_PERSON_TYPE: return createProcessPersonType();
			case B2MMLPackage.PROCESS_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: return createProcessPhysicalAssetCapabilityTestSpecType();
			case B2MMLPackage.PROCESS_PHYSICAL_ASSET_CLASS_TYPE: return createProcessPhysicalAssetClassType();
			case B2MMLPackage.PROCESS_PHYSICAL_ASSET_INFORMATION_TYPE: return createProcessPhysicalAssetInformationType();
			case B2MMLPackage.PROCESS_PHYSICAL_ASSET_TYPE: return createProcessPhysicalAssetType();
			case B2MMLPackage.PROCESS_PROCESS_SEGMENT_INFORMATION_TYPE: return createProcessProcessSegmentInformationType();
			case B2MMLPackage.PROCESS_PROCESS_SEGMENT_TYPE: return createProcessProcessSegmentType();
			case B2MMLPackage.PROCESS_QUALIFICATION_TEST_SPECIFICATION_TYPE: return createProcessQualificationTestSpecificationType();
			case B2MMLPackage.PROCESS_SEGMENT_ID_TYPE: return createProcessSegmentIDType();
			case B2MMLPackage.PROCESS_SEGMENT_INFORMATION_TYPE: return createProcessSegmentInformationType();
			case B2MMLPackage.PROCESS_SEGMENT_TYPE: return createProcessSegmentType();
			case B2MMLPackage.PRODUCTION_REQUEST_ID_TYPE: return createProductionRequestIDType();
			case B2MMLPackage.PRODUCTION_SCHEDULE_ID_TYPE: return createProductionScheduleIDType();
			case B2MMLPackage.PRODUCT_PRODUCTION_RULE_ID_TYPE: return createProductProductionRuleIDType();
			case B2MMLPackage.PRODUCT_PRODUCTION_RULE_TYPE: return createProductProductionRuleType();
			case B2MMLPackage.PRODUCT_SEGMENT_ID_TYPE: return createProductSegmentIDType();
			case B2MMLPackage.PROPERTY_ID_TYPE: return createPropertyIDType();
			case B2MMLPackage.PUBLISHED_DATE_TYPE: return createPublishedDateType();
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_ID_TYPE: return createQualificationTestSpecificationIDType();
			case B2MMLPackage.QUALIFICATION_TEST_SPECIFICATION_TYPE: return createQualificationTestSpecificationType();
			case B2MMLPackage.QUANTITY_STRING_TYPE: return createQuantityStringType();
			case B2MMLPackage.QUANTITY_TYPE: return createQuantityType();
			case B2MMLPackage.QUANTITY_VALUE_TYPE: return createQuantityValueType();
			case B2MMLPackage.REASON_TYPE: return createReasonType();
			case B2MMLPackage.RELATIONSHIP_FORM1_TYPE: return createRelationshipForm1Type();
			case B2MMLPackage.RELATIONSHIP_FORM_TYPE: return createRelationshipFormType();
			case B2MMLPackage.RELATIONSHIP_TYPE1_TYPE: return createRelationshipType1Type();
			case B2MMLPackage.RELATIONSHIP_TYPE_TYPE: return createRelationshipTypeType();
			case B2MMLPackage.REQUESTED_COMPLETION_DATE_TYPE: return createRequestedCompletionDateType();
			case B2MMLPackage.REQUESTED_PRIORITY_TYPE: return createRequestedPriorityType();
			case B2MMLPackage.REQUEST_STATE1_TYPE: return createRequestState1Type();
			case B2MMLPackage.REQUEST_STATE_TYPE: return createRequestStateType();
			case B2MMLPackage.REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE1_TYPE: return createRequiredByRequestedSegmentResponse1Type();
			case B2MMLPackage.REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE_TYPE: return createRequiredByRequestedSegmentResponseType();
			case B2MMLPackage.RESOURCE_ID_TYPE: return createResourceIDType();
			case B2MMLPackage.RESOURCE_NETWORK_CONNECTION_ID_TYPE: return createResourceNetworkConnectionIDType();
			case B2MMLPackage.RESOURCE_REFERENCE_TYPE1_TYPE: return createResourceReferenceType1Type();
			case B2MMLPackage.RESOURCE_REFERENCE_TYPE_TYPE: return createResourceReferenceTypeType();
			case B2MMLPackage.RESOURCES_TYPE: return createResourcesType();
			case B2MMLPackage.RESPOND_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: return createRespondEquipmentCapabilityTestSpecType();
			case B2MMLPackage.RESPOND_EQUIPMENT_CLASS_TYPE: return createRespondEquipmentClassType();
			case B2MMLPackage.RESPOND_EQUIPMENT_INFORMATION_TYPE: return createRespondEquipmentInformationType();
			case B2MMLPackage.RESPOND_EQUIPMENT_TYPE: return createRespondEquipmentType();
			case B2MMLPackage.RESPOND_MATERIAL_CLASS_TYPE: return createRespondMaterialClassType();
			case B2MMLPackage.RESPOND_MATERIAL_DEFINITION_TYPE: return createRespondMaterialDefinitionType();
			case B2MMLPackage.RESPOND_MATERIAL_INFORMATION_TYPE: return createRespondMaterialInformationType();
			case B2MMLPackage.RESPOND_MATERIAL_LOT_TYPE: return createRespondMaterialLotType();
			case B2MMLPackage.RESPOND_MATERIAL_SUB_LOT_TYPE: return createRespondMaterialSubLotType();
			case B2MMLPackage.RESPOND_MATERIAL_TEST_SPEC_TYPE: return createRespondMaterialTestSpecType();
			case B2MMLPackage.RESPOND_OPERATIONS_CAPABILITY_INFORMATION_TYPE: return createRespondOperationsCapabilityInformationType();
			case B2MMLPackage.RESPOND_OPERATIONS_CAPABILITY_TYPE: return createRespondOperationsCapabilityType();
			case B2MMLPackage.RESPOND_OPERATIONS_DEFINITION_INFORMATION_TYPE: return createRespondOperationsDefinitionInformationType();
			case B2MMLPackage.RESPOND_OPERATIONS_DEFINITION_TYPE: return createRespondOperationsDefinitionType();
			case B2MMLPackage.RESPOND_OPERATIONS_PERFORMANCE_TYPE: return createRespondOperationsPerformanceType();
			case B2MMLPackage.RESPOND_OPERATIONS_SCHEDULE_TYPE: return createRespondOperationsScheduleType();
			case B2MMLPackage.RESPOND_PERSONNEL_CLASS_TYPE: return createRespondPersonnelClassType();
			case B2MMLPackage.RESPOND_PERSONNEL_INFORMATION_TYPE: return createRespondPersonnelInformationType();
			case B2MMLPackage.RESPOND_PERSON_TYPE: return createRespondPersonType();
			case B2MMLPackage.RESPOND_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: return createRespondPhysicalAssetCapabilityTestSpecType();
			case B2MMLPackage.RESPOND_PHYSICAL_ASSET_CLASS_TYPE: return createRespondPhysicalAssetClassType();
			case B2MMLPackage.RESPOND_PHYSICAL_ASSET_INFORMATION_TYPE: return createRespondPhysicalAssetInformationType();
			case B2MMLPackage.RESPOND_PHYSICAL_ASSET_TYPE: return createRespondPhysicalAssetType();
			case B2MMLPackage.RESPOND_PROCESS_SEGMENT_INFORMATION_TYPE: return createRespondProcessSegmentInformationType();
			case B2MMLPackage.RESPOND_PROCESS_SEGMENT_TYPE: return createRespondProcessSegmentType();
			case B2MMLPackage.RESPOND_QUALIFICATION_TEST_SPECIFICATION_TYPE: return createRespondQualificationTestSpecificationType();
			case B2MMLPackage.RESPONSE_STATE1_TYPE: return createResponseState1Type();
			case B2MMLPackage.RESPONSE_STATE_TYPE: return createResponseStateType();
			case B2MMLPackage.RESULT_TYPE: return createResultType();
			case B2MMLPackage.SEGMENT_DEPENDENCY_TYPE: return createSegmentDependencyType();
			case B2MMLPackage.SHOW_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: return createShowEquipmentCapabilityTestSpecType();
			case B2MMLPackage.SHOW_EQUIPMENT_CLASS_TYPE: return createShowEquipmentClassType();
			case B2MMLPackage.SHOW_EQUIPMENT_INFORMATION_TYPE: return createShowEquipmentInformationType();
			case B2MMLPackage.SHOW_EQUIPMENT_TYPE: return createShowEquipmentType();
			case B2MMLPackage.SHOW_MATERIAL_CLASS_TYPE: return createShowMaterialClassType();
			case B2MMLPackage.SHOW_MATERIAL_DEFINITION_TYPE: return createShowMaterialDefinitionType();
			case B2MMLPackage.SHOW_MATERIAL_INFORMATION_TYPE: return createShowMaterialInformationType();
			case B2MMLPackage.SHOW_MATERIAL_LOT_TYPE: return createShowMaterialLotType();
			case B2MMLPackage.SHOW_MATERIAL_SUB_LOT_TYPE: return createShowMaterialSubLotType();
			case B2MMLPackage.SHOW_MATERIAL_TEST_SPEC_TYPE: return createShowMaterialTestSpecType();
			case B2MMLPackage.SHOW_OPERATIONS_CAPABILITY_INFORMATION_TYPE: return createShowOperationsCapabilityInformationType();
			case B2MMLPackage.SHOW_OPERATIONS_CAPABILITY_TYPE: return createShowOperationsCapabilityType();
			case B2MMLPackage.SHOW_OPERATIONS_DEFINITION_INFORMATION_TYPE: return createShowOperationsDefinitionInformationType();
			case B2MMLPackage.SHOW_OPERATIONS_DEFINITION_TYPE: return createShowOperationsDefinitionType();
			case B2MMLPackage.SHOW_OPERATIONS_PERFORMANCE_TYPE: return createShowOperationsPerformanceType();
			case B2MMLPackage.SHOW_OPERATIONS_SCHEDULE_TYPE: return createShowOperationsScheduleType();
			case B2MMLPackage.SHOW_PERSONNEL_CLASS_TYPE: return createShowPersonnelClassType();
			case B2MMLPackage.SHOW_PERSONNEL_INFORMATION_TYPE: return createShowPersonnelInformationType();
			case B2MMLPackage.SHOW_PERSON_TYPE: return createShowPersonType();
			case B2MMLPackage.SHOW_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: return createShowPhysicalAssetCapabilityTestSpecType();
			case B2MMLPackage.SHOW_PHYSICAL_ASSET_CLASS_TYPE: return createShowPhysicalAssetClassType();
			case B2MMLPackage.SHOW_PHYSICAL_ASSET_INFORMATION_TYPE: return createShowPhysicalAssetInformationType();
			case B2MMLPackage.SHOW_PHYSICAL_ASSET_TYPE: return createShowPhysicalAssetType();
			case B2MMLPackage.SHOW_PROCESS_SEGMENT_INFORMATION_TYPE: return createShowProcessSegmentInformationType();
			case B2MMLPackage.SHOW_PROCESS_SEGMENT_TYPE: return createShowProcessSegmentType();
			case B2MMLPackage.SHOW_QUALIFICATION_TEST_SPECIFICATION_TYPE: return createShowQualificationTestSpecificationType();
			case B2MMLPackage.START_TIME_TYPE: return createStartTimeType();
			case B2MMLPackage.STATUS_TIME_TYPE: return createStatusTimeType();
			case B2MMLPackage.STATUS_TYPE: return createStatusType();
			case B2MMLPackage.STORAGE_HIERARCHY_SCOPE_TYPE: return createStorageHierarchyScopeType();
			case B2MMLPackage.STORAGE_LOCATION_TYPE: return createStorageLocationType();
			case B2MMLPackage.SYNC_EQUIPMENT_CAPABILITY_TEST_SPEC_TYPE: return createSyncEquipmentCapabilityTestSpecType();
			case B2MMLPackage.SYNC_EQUIPMENT_CLASS_TYPE: return createSyncEquipmentClassType();
			case B2MMLPackage.SYNC_EQUIPMENT_INFORMATION_TYPE: return createSyncEquipmentInformationType();
			case B2MMLPackage.SYNC_EQUIPMENT_TYPE: return createSyncEquipmentType();
			case B2MMLPackage.SYNC_MATERIAL_CLASS_TYPE: return createSyncMaterialClassType();
			case B2MMLPackage.SYNC_MATERIAL_DEFINITION_TYPE: return createSyncMaterialDefinitionType();
			case B2MMLPackage.SYNC_MATERIAL_INFORMATION_TYPE: return createSyncMaterialInformationType();
			case B2MMLPackage.SYNC_MATERIAL_LOT_TYPE: return createSyncMaterialLotType();
			case B2MMLPackage.SYNC_MATERIAL_SUB_LOT_TYPE: return createSyncMaterialSubLotType();
			case B2MMLPackage.SYNC_MATERIAL_TEST_SPEC_TYPE: return createSyncMaterialTestSpecType();
			case B2MMLPackage.SYNC_OPERATIONS_CAPABILITY_INFORMATION_TYPE: return createSyncOperationsCapabilityInformationType();
			case B2MMLPackage.SYNC_OPERATIONS_CAPABILITY_TYPE: return createSyncOperationsCapabilityType();
			case B2MMLPackage.SYNC_OPERATIONS_DEFINITION_INFORMATION_TYPE: return createSyncOperationsDefinitionInformationType();
			case B2MMLPackage.SYNC_OPERATIONS_DEFINITION_TYPE: return createSyncOperationsDefinitionType();
			case B2MMLPackage.SYNC_OPERATIONS_PERFORMANCE_TYPE: return createSyncOperationsPerformanceType();
			case B2MMLPackage.SYNC_OPERATIONS_SCHEDULE_TYPE: return createSyncOperationsScheduleType();
			case B2MMLPackage.SYNC_PERSONNEL_CLASS_TYPE: return createSyncPersonnelClassType();
			case B2MMLPackage.SYNC_PERSONNEL_INFORMATION_TYPE: return createSyncPersonnelInformationType();
			case B2MMLPackage.SYNC_PERSON_TYPE: return createSyncPersonType();
			case B2MMLPackage.SYNC_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC_TYPE: return createSyncPhysicalAssetCapabilityTestSpecType();
			case B2MMLPackage.SYNC_PHYSICAL_ASSET_CLASS_TYPE: return createSyncPhysicalAssetClassType();
			case B2MMLPackage.SYNC_PHYSICAL_ASSET_INFORMATION_TYPE: return createSyncPhysicalAssetInformationType();
			case B2MMLPackage.SYNC_PHYSICAL_ASSET_TYPE: return createSyncPhysicalAssetType();
			case B2MMLPackage.SYNC_PROCESS_SEGMENT_INFORMATION_TYPE: return createSyncProcessSegmentInformationType();
			case B2MMLPackage.SYNC_PROCESS_SEGMENT_TYPE: return createSyncProcessSegmentType();
			case B2MMLPackage.SYNC_QUALIFICATION_TEST_SPECIFICATION_TYPE: return createSyncQualificationTestSpecificationType();
			case B2MMLPackage.TEST_DATE_TIME_TYPE: return createTestDateTimeType();
			case B2MMLPackage.TESTED_EQUIPMENT_CLASS_PROPERTY_TYPE: return createTestedEquipmentClassPropertyType();
			case B2MMLPackage.TESTED_EQUIPMENT_PROPERTY_TYPE: return createTestedEquipmentPropertyType();
			case B2MMLPackage.TESTED_MATERIAL_CLASS_PROPERTY_TYPE: return createTestedMaterialClassPropertyType();
			case B2MMLPackage.TESTED_MATERIAL_DEFINITION_PROPERTY_TYPE: return createTestedMaterialDefinitionPropertyType();
			case B2MMLPackage.TESTED_MATERIAL_LOT_PROPERTY_TYPE: return createTestedMaterialLotPropertyType();
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE: return createTestedPersonnelClassPropertyType();
			case B2MMLPackage.TESTED_PERSON_PROPERTY_TYPE: return createTestedPersonPropertyType();
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_CLASS_PROPERTY_TYPE: return createTestedPhysicalAssetClassPropertyType();
			case B2MMLPackage.TESTED_PHYSICAL_ASSET_PROPERTY_TYPE: return createTestedPhysicalAssetPropertyType();
			case B2MMLPackage.TEST_RESULT_TYPE: return createTestResultType();
			case B2MMLPackage.TEXT_TYPE: return createTextType();
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE: return createTransAcknowledgeType();
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE: return createTransActionCriteriaType();
			case B2MMLPackage.TRANS_APPLICATION_AREA_TYPE: return createTransApplicationAreaType();
			case B2MMLPackage.TRANS_CANCEL_TYPE: return createTransCancelType();
			case B2MMLPackage.TRANS_CHANGE_STATUS_TYPE: return createTransChangeStatusType();
			case B2MMLPackage.TRANS_CHANGE_TYPE: return createTransChangeType();
			case B2MMLPackage.TRANS_CONFIRMATION_CODE_TYPE: return createTransConfirmationCodeType();
			case B2MMLPackage.TRANS_CONFIRM_TYPE: return createTransConfirmType();
			case B2MMLPackage.TRANS_EXPRESSION1_TYPE: return createTransExpression1Type();
			case B2MMLPackage.TRANS_EXPRESSION_TYPE: return createTransExpressionType();
			case B2MMLPackage.TRANS_GET_TYPE: return createTransGetType();
			case B2MMLPackage.TRANS_PROCESS_TYPE: return createTransProcessType();
			case B2MMLPackage.TRANS_RECEIVER_TYPE: return createTransReceiverType();
			case B2MMLPackage.TRANS_RESPOND_TYPE: return createTransRespondType();
			case B2MMLPackage.TRANS_RESPONSE_CRITERIA_TYPE: return createTransResponseCriteriaType();
			case B2MMLPackage.TRANS_SENDER_TYPE: return createTransSenderType();
			case B2MMLPackage.TRANS_SHOW_TYPE: return createTransShowType();
			case B2MMLPackage.TRANS_SIGNATURE_TYPE: return createTransSignatureType();
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE: return createTransStateChangeType();
			case B2MMLPackage.TRANS_SYNC_TYPE: return createTransSyncType();
			case B2MMLPackage.TRANS_USER_AREA_TYPE: return createTransUserAreaType();
			case B2MMLPackage.UNIT_OF_MEASURE_TYPE: return createUnitOfMeasureType();
			case B2MMLPackage.VALUE_STRING_TYPE: return createValueStringType();
			case B2MMLPackage.VALUE_TYPE: return createValueType();
			case B2MMLPackage.VERSION_TYPE: return createVersionType();
			case B2MMLPackage.WORK_REQUEST_ID_TYPE: return createWorkRequestIDType();
			case B2MMLPackage.WORK_SCHEDULE_ID_TYPE: return createWorkScheduleIDType();
			case B2MMLPackage.WORK_TYPE1_TYPE: return createWorkType1Type();
			case B2MMLPackage.WORK_TYPE_TYPE: return createWorkTypeType();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case B2MMLPackage.ASSEMBLY_RELATIONSHIP1_TYPE_BASE:
				return createAssemblyRelationship1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.ASSEMBLY_TYPE1_TYPE_BASE:
				return createAssemblyType1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.CAPABILITY_TYPE1_TYPE_BASE:
				return createCapabilityType1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.DATA_TYPE1_TYPE_BASE:
				return createDataType1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.DEPENDENCY1_TYPE_BASE:
				return createDependency1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.EQUIPMENT_ELEMENT_LEVEL1_TYPE_BASE:
				return createEquipmentElementLevel1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.JOB_ORDER_COMMAND1_TYPE_BASE:
				return createJobOrderCommand1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.MATERIAL_USE1_TYPE_BASE:
				return createMaterialUse1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.OPERATIONS_TYPE1_TYPE_BASE:
				return createOperationsType1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.RELATIONSHIP_FORM1_TYPE_BASE:
				return createRelationshipForm1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.RELATIONSHIP_TYPE1_TYPE_BASE:
				return createRelationshipType1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.REQUEST_STATE1_TYPE_BASE:
				return createRequestState1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE1_TYPE_BASE:
				return createRequiredByRequestedSegmentResponse1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.RESOURCE_REFERENCE_TYPE1_TYPE_BASE:
				return createResourceReferenceType1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.RESPONSE_STATE1_TYPE_BASE:
				return createResponseState1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.TRANS_ACTION_CODE_ENUMERATION_TYPE:
				return createTransActionCodeEnumerationTypeFromString(eDataType, initialValue);
			case B2MMLPackage.TRANS_RESPONSE_CODE_TYPE:
				return createTransResponseCodeTypeFromString(eDataType, initialValue);
			case B2MMLPackage.WORK_TYPE1_TYPE_BASE:
				return createWorkType1TypeBaseFromString(eDataType, initialValue);
			case B2MMLPackage.ASSEMBLY_RELATIONSHIP1_TYPE_BASE_OBJECT:
				return createAssemblyRelationship1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.ASSEMBLY_TYPE1_TYPE_BASE_OBJECT:
				return createAssemblyType1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.CAPABILITY_TYPE1_TYPE_BASE_OBJECT:
				return createCapabilityType1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.DATA_TYPE1_TYPE_BASE_OBJECT:
				return createDataType1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.DEPENDENCY1_TYPE_BASE_OBJECT:
				return createDependency1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.DURATION_TYPE:
				return createDurationTypeFromString(eDataType, initialValue);
			case B2MMLPackage.EQUIPMENT_ELEMENT_LEVEL1_TYPE_BASE_OBJECT:
				return createEquipmentElementLevel1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.INDICATOR_TYPE:
				return createIndicatorTypeFromString(eDataType, initialValue);
			case B2MMLPackage.INDICATOR_TYPE_OBJECT:
				return createIndicatorTypeObjectFromString(eDataType, initialValue);
			case B2MMLPackage.JOB_ORDER_COMMAND1_TYPE_BASE_OBJECT:
				return createJobOrderCommand1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.MATERIAL_USE1_TYPE_BASE_OBJECT:
				return createMaterialUse1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.OPERATIONS_TYPE1_TYPE_BASE_OBJECT:
				return createOperationsType1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.RELATIONSHIP_FORM1_TYPE_BASE_OBJECT:
				return createRelationshipForm1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.RELATIONSHIP_TYPE1_TYPE_BASE_OBJECT:
				return createRelationshipType1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.REQUEST_STATE1_TYPE_BASE_OBJECT:
				return createRequestState1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE1_TYPE_BASE_OBJECT:
				return createRequiredByRequestedSegmentResponse1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.RESOURCE_REFERENCE_TYPE1_TYPE_BASE_OBJECT:
				return createResourceReferenceType1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.RESPONSE_STATE1_TYPE_BASE_OBJECT:
				return createResponseState1TypeBaseObjectFromString(eDataType, initialValue);
			case B2MMLPackage.TRANS_ACTION_CODE_ENUMERATION_TYPE_OBJECT:
				return createTransActionCodeEnumerationTypeObjectFromString(eDataType, initialValue);
			case B2MMLPackage.TRANS_ACTION_CODE_TYPE:
				return createTransActionCodeTypeFromString(eDataType, initialValue);
			case B2MMLPackage.TRANS_RESPONSE_CODE_TYPE_OBJECT:
				return createTransResponseCodeTypeObjectFromString(eDataType, initialValue);
			case B2MMLPackage.WORK_TYPE1_TYPE_BASE_OBJECT:
				return createWorkType1TypeBaseObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case B2MMLPackage.ASSEMBLY_RELATIONSHIP1_TYPE_BASE:
				return convertAssemblyRelationship1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.ASSEMBLY_TYPE1_TYPE_BASE:
				return convertAssemblyType1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.CAPABILITY_TYPE1_TYPE_BASE:
				return convertCapabilityType1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.DATA_TYPE1_TYPE_BASE:
				return convertDataType1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.DEPENDENCY1_TYPE_BASE:
				return convertDependency1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.EQUIPMENT_ELEMENT_LEVEL1_TYPE_BASE:
				return convertEquipmentElementLevel1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.JOB_ORDER_COMMAND1_TYPE_BASE:
				return convertJobOrderCommand1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.MATERIAL_USE1_TYPE_BASE:
				return convertMaterialUse1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.OPERATIONS_TYPE1_TYPE_BASE:
				return convertOperationsType1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.RELATIONSHIP_FORM1_TYPE_BASE:
				return convertRelationshipForm1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.RELATIONSHIP_TYPE1_TYPE_BASE:
				return convertRelationshipType1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.REQUEST_STATE1_TYPE_BASE:
				return convertRequestState1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE1_TYPE_BASE:
				return convertRequiredByRequestedSegmentResponse1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.RESOURCE_REFERENCE_TYPE1_TYPE_BASE:
				return convertResourceReferenceType1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.RESPONSE_STATE1_TYPE_BASE:
				return convertResponseState1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.TRANS_ACTION_CODE_ENUMERATION_TYPE:
				return convertTransActionCodeEnumerationTypeToString(eDataType, instanceValue);
			case B2MMLPackage.TRANS_RESPONSE_CODE_TYPE:
				return convertTransResponseCodeTypeToString(eDataType, instanceValue);
			case B2MMLPackage.WORK_TYPE1_TYPE_BASE:
				return convertWorkType1TypeBaseToString(eDataType, instanceValue);
			case B2MMLPackage.ASSEMBLY_RELATIONSHIP1_TYPE_BASE_OBJECT:
				return convertAssemblyRelationship1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.ASSEMBLY_TYPE1_TYPE_BASE_OBJECT:
				return convertAssemblyType1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.CAPABILITY_TYPE1_TYPE_BASE_OBJECT:
				return convertCapabilityType1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.DATA_TYPE1_TYPE_BASE_OBJECT:
				return convertDataType1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.DEPENDENCY1_TYPE_BASE_OBJECT:
				return convertDependency1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.DURATION_TYPE:
				return convertDurationTypeToString(eDataType, instanceValue);
			case B2MMLPackage.EQUIPMENT_ELEMENT_LEVEL1_TYPE_BASE_OBJECT:
				return convertEquipmentElementLevel1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.INDICATOR_TYPE:
				return convertIndicatorTypeToString(eDataType, instanceValue);
			case B2MMLPackage.INDICATOR_TYPE_OBJECT:
				return convertIndicatorTypeObjectToString(eDataType, instanceValue);
			case B2MMLPackage.JOB_ORDER_COMMAND1_TYPE_BASE_OBJECT:
				return convertJobOrderCommand1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.MATERIAL_USE1_TYPE_BASE_OBJECT:
				return convertMaterialUse1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.OPERATIONS_TYPE1_TYPE_BASE_OBJECT:
				return convertOperationsType1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.RELATIONSHIP_FORM1_TYPE_BASE_OBJECT:
				return convertRelationshipForm1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.RELATIONSHIP_TYPE1_TYPE_BASE_OBJECT:
				return convertRelationshipType1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.REQUEST_STATE1_TYPE_BASE_OBJECT:
				return convertRequestState1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE1_TYPE_BASE_OBJECT:
				return convertRequiredByRequestedSegmentResponse1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.RESOURCE_REFERENCE_TYPE1_TYPE_BASE_OBJECT:
				return convertResourceReferenceType1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.RESPONSE_STATE1_TYPE_BASE_OBJECT:
				return convertResponseState1TypeBaseObjectToString(eDataType, instanceValue);
			case B2MMLPackage.TRANS_ACTION_CODE_ENUMERATION_TYPE_OBJECT:
				return convertTransActionCodeEnumerationTypeObjectToString(eDataType, instanceValue);
			case B2MMLPackage.TRANS_ACTION_CODE_TYPE:
				return convertTransActionCodeTypeToString(eDataType, instanceValue);
			case B2MMLPackage.TRANS_RESPONSE_CODE_TYPE_OBJECT:
				return convertTransResponseCodeTypeObjectToString(eDataType, instanceValue);
			case B2MMLPackage.WORK_TYPE1_TYPE_BASE_OBJECT:
				return convertWorkType1TypeBaseObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeEquipmentCapabilityTestSpecType createAcknowledgeEquipmentCapabilityTestSpecType() {
		AcknowledgeEquipmentCapabilityTestSpecTypeImpl acknowledgeEquipmentCapabilityTestSpecType = new AcknowledgeEquipmentCapabilityTestSpecTypeImpl();
		return acknowledgeEquipmentCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeEquipmentClassType createAcknowledgeEquipmentClassType() {
		AcknowledgeEquipmentClassTypeImpl acknowledgeEquipmentClassType = new AcknowledgeEquipmentClassTypeImpl();
		return acknowledgeEquipmentClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeEquipmentInformationType createAcknowledgeEquipmentInformationType() {
		AcknowledgeEquipmentInformationTypeImpl acknowledgeEquipmentInformationType = new AcknowledgeEquipmentInformationTypeImpl();
		return acknowledgeEquipmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeEquipmentType createAcknowledgeEquipmentType() {
		AcknowledgeEquipmentTypeImpl acknowledgeEquipmentType = new AcknowledgeEquipmentTypeImpl();
		return acknowledgeEquipmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeMaterialClassType createAcknowledgeMaterialClassType() {
		AcknowledgeMaterialClassTypeImpl acknowledgeMaterialClassType = new AcknowledgeMaterialClassTypeImpl();
		return acknowledgeMaterialClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeMaterialDefinitionType createAcknowledgeMaterialDefinitionType() {
		AcknowledgeMaterialDefinitionTypeImpl acknowledgeMaterialDefinitionType = new AcknowledgeMaterialDefinitionTypeImpl();
		return acknowledgeMaterialDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeMaterialInformationType createAcknowledgeMaterialInformationType() {
		AcknowledgeMaterialInformationTypeImpl acknowledgeMaterialInformationType = new AcknowledgeMaterialInformationTypeImpl();
		return acknowledgeMaterialInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeMaterialLotType createAcknowledgeMaterialLotType() {
		AcknowledgeMaterialLotTypeImpl acknowledgeMaterialLotType = new AcknowledgeMaterialLotTypeImpl();
		return acknowledgeMaterialLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeMaterialSubLotType createAcknowledgeMaterialSubLotType() {
		AcknowledgeMaterialSubLotTypeImpl acknowledgeMaterialSubLotType = new AcknowledgeMaterialSubLotTypeImpl();
		return acknowledgeMaterialSubLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeMaterialTestSpecType createAcknowledgeMaterialTestSpecType() {
		AcknowledgeMaterialTestSpecTypeImpl acknowledgeMaterialTestSpecType = new AcknowledgeMaterialTestSpecTypeImpl();
		return acknowledgeMaterialTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeOperationsCapabilityInformationType createAcknowledgeOperationsCapabilityInformationType() {
		AcknowledgeOperationsCapabilityInformationTypeImpl acknowledgeOperationsCapabilityInformationType = new AcknowledgeOperationsCapabilityInformationTypeImpl();
		return acknowledgeOperationsCapabilityInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeOperationsCapabilityType createAcknowledgeOperationsCapabilityType() {
		AcknowledgeOperationsCapabilityTypeImpl acknowledgeOperationsCapabilityType = new AcknowledgeOperationsCapabilityTypeImpl();
		return acknowledgeOperationsCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeOperationsDefinitionInformationType createAcknowledgeOperationsDefinitionInformationType() {
		AcknowledgeOperationsDefinitionInformationTypeImpl acknowledgeOperationsDefinitionInformationType = new AcknowledgeOperationsDefinitionInformationTypeImpl();
		return acknowledgeOperationsDefinitionInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeOperationsDefinitionType createAcknowledgeOperationsDefinitionType() {
		AcknowledgeOperationsDefinitionTypeImpl acknowledgeOperationsDefinitionType = new AcknowledgeOperationsDefinitionTypeImpl();
		return acknowledgeOperationsDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeOperationsPerformanceType createAcknowledgeOperationsPerformanceType() {
		AcknowledgeOperationsPerformanceTypeImpl acknowledgeOperationsPerformanceType = new AcknowledgeOperationsPerformanceTypeImpl();
		return acknowledgeOperationsPerformanceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeOperationsScheduleType createAcknowledgeOperationsScheduleType() {
		AcknowledgeOperationsScheduleTypeImpl acknowledgeOperationsScheduleType = new AcknowledgeOperationsScheduleTypeImpl();
		return acknowledgeOperationsScheduleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePersonnelClassType createAcknowledgePersonnelClassType() {
		AcknowledgePersonnelClassTypeImpl acknowledgePersonnelClassType = new AcknowledgePersonnelClassTypeImpl();
		return acknowledgePersonnelClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePersonnelInformationType createAcknowledgePersonnelInformationType() {
		AcknowledgePersonnelInformationTypeImpl acknowledgePersonnelInformationType = new AcknowledgePersonnelInformationTypeImpl();
		return acknowledgePersonnelInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePersonType createAcknowledgePersonType() {
		AcknowledgePersonTypeImpl acknowledgePersonType = new AcknowledgePersonTypeImpl();
		return acknowledgePersonType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePhysicalAssetCapabilityTestSpecType createAcknowledgePhysicalAssetCapabilityTestSpecType() {
		AcknowledgePhysicalAssetCapabilityTestSpecTypeImpl acknowledgePhysicalAssetCapabilityTestSpecType = new AcknowledgePhysicalAssetCapabilityTestSpecTypeImpl();
		return acknowledgePhysicalAssetCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePhysicalAssetClassType createAcknowledgePhysicalAssetClassType() {
		AcknowledgePhysicalAssetClassTypeImpl acknowledgePhysicalAssetClassType = new AcknowledgePhysicalAssetClassTypeImpl();
		return acknowledgePhysicalAssetClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePhysicalAssetInformationType createAcknowledgePhysicalAssetInformationType() {
		AcknowledgePhysicalAssetInformationTypeImpl acknowledgePhysicalAssetInformationType = new AcknowledgePhysicalAssetInformationTypeImpl();
		return acknowledgePhysicalAssetInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePhysicalAssetType createAcknowledgePhysicalAssetType() {
		AcknowledgePhysicalAssetTypeImpl acknowledgePhysicalAssetType = new AcknowledgePhysicalAssetTypeImpl();
		return acknowledgePhysicalAssetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeProcessSegmentInformationType createAcknowledgeProcessSegmentInformationType() {
		AcknowledgeProcessSegmentInformationTypeImpl acknowledgeProcessSegmentInformationType = new AcknowledgeProcessSegmentInformationTypeImpl();
		return acknowledgeProcessSegmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeProcessSegmentType createAcknowledgeProcessSegmentType() {
		AcknowledgeProcessSegmentTypeImpl acknowledgeProcessSegmentType = new AcknowledgeProcessSegmentTypeImpl();
		return acknowledgeProcessSegmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeQualificationTestSpecificationType createAcknowledgeQualificationTestSpecificationType() {
		AcknowledgeQualificationTestSpecificationTypeImpl acknowledgeQualificationTestSpecificationType = new AcknowledgeQualificationTestSpecificationTypeImpl();
		return acknowledgeQualificationTestSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActualEndTimeType createActualEndTimeType() {
		ActualEndTimeTypeImpl actualEndTimeType = new ActualEndTimeTypeImpl();
		return actualEndTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActualFinishTimeType createActualFinishTimeType() {
		ActualFinishTimeTypeImpl actualFinishTimeType = new ActualFinishTimeTypeImpl();
		return actualFinishTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActualStartTimeType createActualStartTimeType() {
		ActualStartTimeTypeImpl actualStartTimeType = new ActualStartTimeTypeImpl();
		return actualStartTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AmountType createAmountType() {
		AmountTypeImpl amountType = new AmountTypeImpl();
		return amountType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnyGenericValueType createAnyGenericValueType() {
		AnyGenericValueTypeImpl anyGenericValueType = new AnyGenericValueTypeImpl();
		return anyGenericValueType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationship1Type createAssemblyRelationship1Type() {
		AssemblyRelationship1TypeImpl assemblyRelationship1Type = new AssemblyRelationship1TypeImpl();
		return assemblyRelationship1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationshipType createAssemblyRelationshipType() {
		AssemblyRelationshipTypeImpl assemblyRelationshipType = new AssemblyRelationshipTypeImpl();
		return assemblyRelationshipType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyType1Type createAssemblyType1Type() {
		AssemblyType1TypeImpl assemblyType1Type = new AssemblyType1TypeImpl();
		return assemblyType1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyTypeType createAssemblyTypeType() {
		AssemblyTypeTypeImpl assemblyTypeType = new AssemblyTypeTypeImpl();
		return assemblyTypeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BillOfMaterialIDType createBillOfMaterialIDType() {
		BillOfMaterialIDTypeImpl billOfMaterialIDType = new BillOfMaterialIDTypeImpl();
		return billOfMaterialIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BillOfMaterialsIDType createBillOfMaterialsIDType() {
		BillOfMaterialsIDTypeImpl billOfMaterialsIDType = new BillOfMaterialsIDTypeImpl();
		return billOfMaterialsIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BillOfResourcesIDType createBillOfResourcesIDType() {
		BillOfResourcesIDTypeImpl billOfResourcesIDType = new BillOfResourcesIDTypeImpl();
		return billOfResourcesIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryObjectType createBinaryObjectType() {
		BinaryObjectTypeImpl binaryObjectType = new BinaryObjectTypeImpl();
		return binaryObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BODType createBODType() {
		BODTypeImpl bodType = new BODTypeImpl();
		return bodType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelEquipmentCapabilityTestSpecType createCancelEquipmentCapabilityTestSpecType() {
		CancelEquipmentCapabilityTestSpecTypeImpl cancelEquipmentCapabilityTestSpecType = new CancelEquipmentCapabilityTestSpecTypeImpl();
		return cancelEquipmentCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelEquipmentClassType createCancelEquipmentClassType() {
		CancelEquipmentClassTypeImpl cancelEquipmentClassType = new CancelEquipmentClassTypeImpl();
		return cancelEquipmentClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelEquipmentInformationType createCancelEquipmentInformationType() {
		CancelEquipmentInformationTypeImpl cancelEquipmentInformationType = new CancelEquipmentInformationTypeImpl();
		return cancelEquipmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelEquipmentType createCancelEquipmentType() {
		CancelEquipmentTypeImpl cancelEquipmentType = new CancelEquipmentTypeImpl();
		return cancelEquipmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelMaterialClassType createCancelMaterialClassType() {
		CancelMaterialClassTypeImpl cancelMaterialClassType = new CancelMaterialClassTypeImpl();
		return cancelMaterialClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelMaterialDefinitionType createCancelMaterialDefinitionType() {
		CancelMaterialDefinitionTypeImpl cancelMaterialDefinitionType = new CancelMaterialDefinitionTypeImpl();
		return cancelMaterialDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelMaterialInformationType createCancelMaterialInformationType() {
		CancelMaterialInformationTypeImpl cancelMaterialInformationType = new CancelMaterialInformationTypeImpl();
		return cancelMaterialInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelMaterialLotType createCancelMaterialLotType() {
		CancelMaterialLotTypeImpl cancelMaterialLotType = new CancelMaterialLotTypeImpl();
		return cancelMaterialLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelMaterialSubLotType createCancelMaterialSubLotType() {
		CancelMaterialSubLotTypeImpl cancelMaterialSubLotType = new CancelMaterialSubLotTypeImpl();
		return cancelMaterialSubLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelMaterialTestSpecType createCancelMaterialTestSpecType() {
		CancelMaterialTestSpecTypeImpl cancelMaterialTestSpecType = new CancelMaterialTestSpecTypeImpl();
		return cancelMaterialTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelOperationsCapabilityInformationType createCancelOperationsCapabilityInformationType() {
		CancelOperationsCapabilityInformationTypeImpl cancelOperationsCapabilityInformationType = new CancelOperationsCapabilityInformationTypeImpl();
		return cancelOperationsCapabilityInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelOperationsCapabilityType createCancelOperationsCapabilityType() {
		CancelOperationsCapabilityTypeImpl cancelOperationsCapabilityType = new CancelOperationsCapabilityTypeImpl();
		return cancelOperationsCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelOperationsDefinitionInformationType createCancelOperationsDefinitionInformationType() {
		CancelOperationsDefinitionInformationTypeImpl cancelOperationsDefinitionInformationType = new CancelOperationsDefinitionInformationTypeImpl();
		return cancelOperationsDefinitionInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelOperationsDefinitionType createCancelOperationsDefinitionType() {
		CancelOperationsDefinitionTypeImpl cancelOperationsDefinitionType = new CancelOperationsDefinitionTypeImpl();
		return cancelOperationsDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelOperationsPerformanceType createCancelOperationsPerformanceType() {
		CancelOperationsPerformanceTypeImpl cancelOperationsPerformanceType = new CancelOperationsPerformanceTypeImpl();
		return cancelOperationsPerformanceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelOperationsScheduleType createCancelOperationsScheduleType() {
		CancelOperationsScheduleTypeImpl cancelOperationsScheduleType = new CancelOperationsScheduleTypeImpl();
		return cancelOperationsScheduleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPersonnelClassType createCancelPersonnelClassType() {
		CancelPersonnelClassTypeImpl cancelPersonnelClassType = new CancelPersonnelClassTypeImpl();
		return cancelPersonnelClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPersonnelInformationType createCancelPersonnelInformationType() {
		CancelPersonnelInformationTypeImpl cancelPersonnelInformationType = new CancelPersonnelInformationTypeImpl();
		return cancelPersonnelInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPersonType createCancelPersonType() {
		CancelPersonTypeImpl cancelPersonType = new CancelPersonTypeImpl();
		return cancelPersonType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPhysicalAssetCapabilityTestSpecType createCancelPhysicalAssetCapabilityTestSpecType() {
		CancelPhysicalAssetCapabilityTestSpecTypeImpl cancelPhysicalAssetCapabilityTestSpecType = new CancelPhysicalAssetCapabilityTestSpecTypeImpl();
		return cancelPhysicalAssetCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPhysicalAssetClassType createCancelPhysicalAssetClassType() {
		CancelPhysicalAssetClassTypeImpl cancelPhysicalAssetClassType = new CancelPhysicalAssetClassTypeImpl();
		return cancelPhysicalAssetClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPhysicalAssetInformationType createCancelPhysicalAssetInformationType() {
		CancelPhysicalAssetInformationTypeImpl cancelPhysicalAssetInformationType = new CancelPhysicalAssetInformationTypeImpl();
		return cancelPhysicalAssetInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPhysicalAssetType createCancelPhysicalAssetType() {
		CancelPhysicalAssetTypeImpl cancelPhysicalAssetType = new CancelPhysicalAssetTypeImpl();
		return cancelPhysicalAssetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelProcessSegmentInformationType createCancelProcessSegmentInformationType() {
		CancelProcessSegmentInformationTypeImpl cancelProcessSegmentInformationType = new CancelProcessSegmentInformationTypeImpl();
		return cancelProcessSegmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelProcessSegmentType createCancelProcessSegmentType() {
		CancelProcessSegmentTypeImpl cancelProcessSegmentType = new CancelProcessSegmentTypeImpl();
		return cancelProcessSegmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelQualificationTestSpecificationType createCancelQualificationTestSpecificationType() {
		CancelQualificationTestSpecificationTypeImpl cancelQualificationTestSpecificationType = new CancelQualificationTestSpecificationTypeImpl();
		return cancelQualificationTestSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityType1Type createCapabilityType1Type() {
		CapabilityType1TypeImpl capabilityType1Type = new CapabilityType1TypeImpl();
		return capabilityType1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityTypeType createCapabilityTypeType() {
		CapabilityTypeTypeImpl capabilityTypeType = new CapabilityTypeTypeImpl();
		return capabilityTypeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CauseType createCauseType() {
		CauseTypeImpl causeType = new CauseTypeImpl();
		return causeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CertificateOfAnalysisReferenceType createCertificateOfAnalysisReferenceType() {
		CertificateOfAnalysisReferenceTypeImpl certificateOfAnalysisReferenceType = new CertificateOfAnalysisReferenceTypeImpl();
		return certificateOfAnalysisReferenceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEquipmentCapabilityTestSpecType createChangeEquipmentCapabilityTestSpecType() {
		ChangeEquipmentCapabilityTestSpecTypeImpl changeEquipmentCapabilityTestSpecType = new ChangeEquipmentCapabilityTestSpecTypeImpl();
		return changeEquipmentCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEquipmentClassType createChangeEquipmentClassType() {
		ChangeEquipmentClassTypeImpl changeEquipmentClassType = new ChangeEquipmentClassTypeImpl();
		return changeEquipmentClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEquipmentInformationType createChangeEquipmentInformationType() {
		ChangeEquipmentInformationTypeImpl changeEquipmentInformationType = new ChangeEquipmentInformationTypeImpl();
		return changeEquipmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEquipmentType createChangeEquipmentType() {
		ChangeEquipmentTypeImpl changeEquipmentType = new ChangeEquipmentTypeImpl();
		return changeEquipmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeMaterialClassType createChangeMaterialClassType() {
		ChangeMaterialClassTypeImpl changeMaterialClassType = new ChangeMaterialClassTypeImpl();
		return changeMaterialClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeMaterialDefinitionType createChangeMaterialDefinitionType() {
		ChangeMaterialDefinitionTypeImpl changeMaterialDefinitionType = new ChangeMaterialDefinitionTypeImpl();
		return changeMaterialDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeMaterialInformationType createChangeMaterialInformationType() {
		ChangeMaterialInformationTypeImpl changeMaterialInformationType = new ChangeMaterialInformationTypeImpl();
		return changeMaterialInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeMaterialLotType createChangeMaterialLotType() {
		ChangeMaterialLotTypeImpl changeMaterialLotType = new ChangeMaterialLotTypeImpl();
		return changeMaterialLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeMaterialSubLotType createChangeMaterialSubLotType() {
		ChangeMaterialSubLotTypeImpl changeMaterialSubLotType = new ChangeMaterialSubLotTypeImpl();
		return changeMaterialSubLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeMaterialTestSpecType createChangeMaterialTestSpecType() {
		ChangeMaterialTestSpecTypeImpl changeMaterialTestSpecType = new ChangeMaterialTestSpecTypeImpl();
		return changeMaterialTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeOperationsCapabilityInformationType createChangeOperationsCapabilityInformationType() {
		ChangeOperationsCapabilityInformationTypeImpl changeOperationsCapabilityInformationType = new ChangeOperationsCapabilityInformationTypeImpl();
		return changeOperationsCapabilityInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeOperationsCapabilityType createChangeOperationsCapabilityType() {
		ChangeOperationsCapabilityTypeImpl changeOperationsCapabilityType = new ChangeOperationsCapabilityTypeImpl();
		return changeOperationsCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeOperationsDefinitionInformationType createChangeOperationsDefinitionInformationType() {
		ChangeOperationsDefinitionInformationTypeImpl changeOperationsDefinitionInformationType = new ChangeOperationsDefinitionInformationTypeImpl();
		return changeOperationsDefinitionInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeOperationsDefinitionType createChangeOperationsDefinitionType() {
		ChangeOperationsDefinitionTypeImpl changeOperationsDefinitionType = new ChangeOperationsDefinitionTypeImpl();
		return changeOperationsDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeOperationsPerformanceType createChangeOperationsPerformanceType() {
		ChangeOperationsPerformanceTypeImpl changeOperationsPerformanceType = new ChangeOperationsPerformanceTypeImpl();
		return changeOperationsPerformanceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeOperationsScheduleType createChangeOperationsScheduleType() {
		ChangeOperationsScheduleTypeImpl changeOperationsScheduleType = new ChangeOperationsScheduleTypeImpl();
		return changeOperationsScheduleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePersonnelClassType createChangePersonnelClassType() {
		ChangePersonnelClassTypeImpl changePersonnelClassType = new ChangePersonnelClassTypeImpl();
		return changePersonnelClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePersonnelInformationType createChangePersonnelInformationType() {
		ChangePersonnelInformationTypeImpl changePersonnelInformationType = new ChangePersonnelInformationTypeImpl();
		return changePersonnelInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePersonType createChangePersonType() {
		ChangePersonTypeImpl changePersonType = new ChangePersonTypeImpl();
		return changePersonType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePhysicalAssetCapabilityTestSpecType createChangePhysicalAssetCapabilityTestSpecType() {
		ChangePhysicalAssetCapabilityTestSpecTypeImpl changePhysicalAssetCapabilityTestSpecType = new ChangePhysicalAssetCapabilityTestSpecTypeImpl();
		return changePhysicalAssetCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePhysicalAssetClassType createChangePhysicalAssetClassType() {
		ChangePhysicalAssetClassTypeImpl changePhysicalAssetClassType = new ChangePhysicalAssetClassTypeImpl();
		return changePhysicalAssetClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePhysicalAssetInformationType createChangePhysicalAssetInformationType() {
		ChangePhysicalAssetInformationTypeImpl changePhysicalAssetInformationType = new ChangePhysicalAssetInformationTypeImpl();
		return changePhysicalAssetInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePhysicalAssetType createChangePhysicalAssetType() {
		ChangePhysicalAssetTypeImpl changePhysicalAssetType = new ChangePhysicalAssetTypeImpl();
		return changePhysicalAssetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeProcessSegmentInformationType createChangeProcessSegmentInformationType() {
		ChangeProcessSegmentInformationTypeImpl changeProcessSegmentInformationType = new ChangeProcessSegmentInformationTypeImpl();
		return changeProcessSegmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeProcessSegmentType createChangeProcessSegmentType() {
		ChangeProcessSegmentTypeImpl changeProcessSegmentType = new ChangeProcessSegmentTypeImpl();
		return changeProcessSegmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeQualificationTestSpecificationType createChangeQualificationTestSpecificationType() {
		ChangeQualificationTestSpecificationTypeImpl changeQualificationTestSpecificationType = new ChangeQualificationTestSpecificationTypeImpl();
		return changeQualificationTestSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeType createCodeType() {
		CodeTypeImpl codeType = new CodeTypeImpl();
		return codeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfidenceFactorType createConfidenceFactorType() {
		ConfidenceFactorTypeImpl confidenceFactorType = new ConfidenceFactorTypeImpl();
		return confidenceFactorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfirmBODType createConfirmBODType() {
		ConfirmBODTypeImpl confirmBODType = new ConfirmBODTypeImpl();
		return confirmBODType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrectionType createCorrectionType() {
		CorrectionTypeImpl correctionType = new CorrectionTypeImpl();
		return correctionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType createDataAreaType() {
		DataAreaTypeImpl dataAreaType = new DataAreaTypeImpl();
		return dataAreaType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType1 createDataAreaType1() {
		DataAreaType1Impl dataAreaType1 = new DataAreaType1Impl();
		return dataAreaType1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType2 createDataAreaType2() {
		DataAreaType2Impl dataAreaType2 = new DataAreaType2Impl();
		return dataAreaType2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType3 createDataAreaType3() {
		DataAreaType3Impl dataAreaType3 = new DataAreaType3Impl();
		return dataAreaType3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType4 createDataAreaType4() {
		DataAreaType4Impl dataAreaType4 = new DataAreaType4Impl();
		return dataAreaType4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType5 createDataAreaType5() {
		DataAreaType5Impl dataAreaType5 = new DataAreaType5Impl();
		return dataAreaType5;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType6 createDataAreaType6() {
		DataAreaType6Impl dataAreaType6 = new DataAreaType6Impl();
		return dataAreaType6;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType7 createDataAreaType7() {
		DataAreaType7Impl dataAreaType7 = new DataAreaType7Impl();
		return dataAreaType7;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType8 createDataAreaType8() {
		DataAreaType8Impl dataAreaType8 = new DataAreaType8Impl();
		return dataAreaType8;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType9 createDataAreaType9() {
		DataAreaType9Impl dataAreaType9 = new DataAreaType9Impl();
		return dataAreaType9;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType10 createDataAreaType10() {
		DataAreaType10Impl dataAreaType10 = new DataAreaType10Impl();
		return dataAreaType10;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType11 createDataAreaType11() {
		DataAreaType11Impl dataAreaType11 = new DataAreaType11Impl();
		return dataAreaType11;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType12 createDataAreaType12() {
		DataAreaType12Impl dataAreaType12 = new DataAreaType12Impl();
		return dataAreaType12;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType13 createDataAreaType13() {
		DataAreaType13Impl dataAreaType13 = new DataAreaType13Impl();
		return dataAreaType13;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType14 createDataAreaType14() {
		DataAreaType14Impl dataAreaType14 = new DataAreaType14Impl();
		return dataAreaType14;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType15 createDataAreaType15() {
		DataAreaType15Impl dataAreaType15 = new DataAreaType15Impl();
		return dataAreaType15;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType16 createDataAreaType16() {
		DataAreaType16Impl dataAreaType16 = new DataAreaType16Impl();
		return dataAreaType16;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType17 createDataAreaType17() {
		DataAreaType17Impl dataAreaType17 = new DataAreaType17Impl();
		return dataAreaType17;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType18 createDataAreaType18() {
		DataAreaType18Impl dataAreaType18 = new DataAreaType18Impl();
		return dataAreaType18;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType19 createDataAreaType19() {
		DataAreaType19Impl dataAreaType19 = new DataAreaType19Impl();
		return dataAreaType19;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType20 createDataAreaType20() {
		DataAreaType20Impl dataAreaType20 = new DataAreaType20Impl();
		return dataAreaType20;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType21 createDataAreaType21() {
		DataAreaType21Impl dataAreaType21 = new DataAreaType21Impl();
		return dataAreaType21;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType22 createDataAreaType22() {
		DataAreaType22Impl dataAreaType22 = new DataAreaType22Impl();
		return dataAreaType22;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType23 createDataAreaType23() {
		DataAreaType23Impl dataAreaType23 = new DataAreaType23Impl();
		return dataAreaType23;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType24 createDataAreaType24() {
		DataAreaType24Impl dataAreaType24 = new DataAreaType24Impl();
		return dataAreaType24;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType25 createDataAreaType25() {
		DataAreaType25Impl dataAreaType25 = new DataAreaType25Impl();
		return dataAreaType25;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType26 createDataAreaType26() {
		DataAreaType26Impl dataAreaType26 = new DataAreaType26Impl();
		return dataAreaType26;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType27 createDataAreaType27() {
		DataAreaType27Impl dataAreaType27 = new DataAreaType27Impl();
		return dataAreaType27;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType28 createDataAreaType28() {
		DataAreaType28Impl dataAreaType28 = new DataAreaType28Impl();
		return dataAreaType28;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType29 createDataAreaType29() {
		DataAreaType29Impl dataAreaType29 = new DataAreaType29Impl();
		return dataAreaType29;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType30 createDataAreaType30() {
		DataAreaType30Impl dataAreaType30 = new DataAreaType30Impl();
		return dataAreaType30;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType31 createDataAreaType31() {
		DataAreaType31Impl dataAreaType31 = new DataAreaType31Impl();
		return dataAreaType31;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType32 createDataAreaType32() {
		DataAreaType32Impl dataAreaType32 = new DataAreaType32Impl();
		return dataAreaType32;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType33 createDataAreaType33() {
		DataAreaType33Impl dataAreaType33 = new DataAreaType33Impl();
		return dataAreaType33;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType34 createDataAreaType34() {
		DataAreaType34Impl dataAreaType34 = new DataAreaType34Impl();
		return dataAreaType34;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType35 createDataAreaType35() {
		DataAreaType35Impl dataAreaType35 = new DataAreaType35Impl();
		return dataAreaType35;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType36 createDataAreaType36() {
		DataAreaType36Impl dataAreaType36 = new DataAreaType36Impl();
		return dataAreaType36;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType37 createDataAreaType37() {
		DataAreaType37Impl dataAreaType37 = new DataAreaType37Impl();
		return dataAreaType37;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType38 createDataAreaType38() {
		DataAreaType38Impl dataAreaType38 = new DataAreaType38Impl();
		return dataAreaType38;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType39 createDataAreaType39() {
		DataAreaType39Impl dataAreaType39 = new DataAreaType39Impl();
		return dataAreaType39;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType40 createDataAreaType40() {
		DataAreaType40Impl dataAreaType40 = new DataAreaType40Impl();
		return dataAreaType40;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType41 createDataAreaType41() {
		DataAreaType41Impl dataAreaType41 = new DataAreaType41Impl();
		return dataAreaType41;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType42 createDataAreaType42() {
		DataAreaType42Impl dataAreaType42 = new DataAreaType42Impl();
		return dataAreaType42;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType43 createDataAreaType43() {
		DataAreaType43Impl dataAreaType43 = new DataAreaType43Impl();
		return dataAreaType43;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType44 createDataAreaType44() {
		DataAreaType44Impl dataAreaType44 = new DataAreaType44Impl();
		return dataAreaType44;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType45 createDataAreaType45() {
		DataAreaType45Impl dataAreaType45 = new DataAreaType45Impl();
		return dataAreaType45;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType46 createDataAreaType46() {
		DataAreaType46Impl dataAreaType46 = new DataAreaType46Impl();
		return dataAreaType46;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType47 createDataAreaType47() {
		DataAreaType47Impl dataAreaType47 = new DataAreaType47Impl();
		return dataAreaType47;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType48 createDataAreaType48() {
		DataAreaType48Impl dataAreaType48 = new DataAreaType48Impl();
		return dataAreaType48;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType49 createDataAreaType49() {
		DataAreaType49Impl dataAreaType49 = new DataAreaType49Impl();
		return dataAreaType49;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType50 createDataAreaType50() {
		DataAreaType50Impl dataAreaType50 = new DataAreaType50Impl();
		return dataAreaType50;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType51 createDataAreaType51() {
		DataAreaType51Impl dataAreaType51 = new DataAreaType51Impl();
		return dataAreaType51;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType52 createDataAreaType52() {
		DataAreaType52Impl dataAreaType52 = new DataAreaType52Impl();
		return dataAreaType52;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType53 createDataAreaType53() {
		DataAreaType53Impl dataAreaType53 = new DataAreaType53Impl();
		return dataAreaType53;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType54 createDataAreaType54() {
		DataAreaType54Impl dataAreaType54 = new DataAreaType54Impl();
		return dataAreaType54;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType55 createDataAreaType55() {
		DataAreaType55Impl dataAreaType55 = new DataAreaType55Impl();
		return dataAreaType55;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType56 createDataAreaType56() {
		DataAreaType56Impl dataAreaType56 = new DataAreaType56Impl();
		return dataAreaType56;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType57 createDataAreaType57() {
		DataAreaType57Impl dataAreaType57 = new DataAreaType57Impl();
		return dataAreaType57;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType58 createDataAreaType58() {
		DataAreaType58Impl dataAreaType58 = new DataAreaType58Impl();
		return dataAreaType58;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType59 createDataAreaType59() {
		DataAreaType59Impl dataAreaType59 = new DataAreaType59Impl();
		return dataAreaType59;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType60 createDataAreaType60() {
		DataAreaType60Impl dataAreaType60 = new DataAreaType60Impl();
		return dataAreaType60;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType61 createDataAreaType61() {
		DataAreaType61Impl dataAreaType61 = new DataAreaType61Impl();
		return dataAreaType61;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType62 createDataAreaType62() {
		DataAreaType62Impl dataAreaType62 = new DataAreaType62Impl();
		return dataAreaType62;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType63 createDataAreaType63() {
		DataAreaType63Impl dataAreaType63 = new DataAreaType63Impl();
		return dataAreaType63;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType64 createDataAreaType64() {
		DataAreaType64Impl dataAreaType64 = new DataAreaType64Impl();
		return dataAreaType64;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType65 createDataAreaType65() {
		DataAreaType65Impl dataAreaType65 = new DataAreaType65Impl();
		return dataAreaType65;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType66 createDataAreaType66() {
		DataAreaType66Impl dataAreaType66 = new DataAreaType66Impl();
		return dataAreaType66;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType67 createDataAreaType67() {
		DataAreaType67Impl dataAreaType67 = new DataAreaType67Impl();
		return dataAreaType67;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType68 createDataAreaType68() {
		DataAreaType68Impl dataAreaType68 = new DataAreaType68Impl();
		return dataAreaType68;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType69 createDataAreaType69() {
		DataAreaType69Impl dataAreaType69 = new DataAreaType69Impl();
		return dataAreaType69;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType70 createDataAreaType70() {
		DataAreaType70Impl dataAreaType70 = new DataAreaType70Impl();
		return dataAreaType70;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType71 createDataAreaType71() {
		DataAreaType71Impl dataAreaType71 = new DataAreaType71Impl();
		return dataAreaType71;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType72 createDataAreaType72() {
		DataAreaType72Impl dataAreaType72 = new DataAreaType72Impl();
		return dataAreaType72;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType73 createDataAreaType73() {
		DataAreaType73Impl dataAreaType73 = new DataAreaType73Impl();
		return dataAreaType73;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType74 createDataAreaType74() {
		DataAreaType74Impl dataAreaType74 = new DataAreaType74Impl();
		return dataAreaType74;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType75 createDataAreaType75() {
		DataAreaType75Impl dataAreaType75 = new DataAreaType75Impl();
		return dataAreaType75;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType76 createDataAreaType76() {
		DataAreaType76Impl dataAreaType76 = new DataAreaType76Impl();
		return dataAreaType76;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType77 createDataAreaType77() {
		DataAreaType77Impl dataAreaType77 = new DataAreaType77Impl();
		return dataAreaType77;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType78 createDataAreaType78() {
		DataAreaType78Impl dataAreaType78 = new DataAreaType78Impl();
		return dataAreaType78;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType79 createDataAreaType79() {
		DataAreaType79Impl dataAreaType79 = new DataAreaType79Impl();
		return dataAreaType79;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType80 createDataAreaType80() {
		DataAreaType80Impl dataAreaType80 = new DataAreaType80Impl();
		return dataAreaType80;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType81 createDataAreaType81() {
		DataAreaType81Impl dataAreaType81 = new DataAreaType81Impl();
		return dataAreaType81;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType82 createDataAreaType82() {
		DataAreaType82Impl dataAreaType82 = new DataAreaType82Impl();
		return dataAreaType82;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType83 createDataAreaType83() {
		DataAreaType83Impl dataAreaType83 = new DataAreaType83Impl();
		return dataAreaType83;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType84 createDataAreaType84() {
		DataAreaType84Impl dataAreaType84 = new DataAreaType84Impl();
		return dataAreaType84;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType85 createDataAreaType85() {
		DataAreaType85Impl dataAreaType85 = new DataAreaType85Impl();
		return dataAreaType85;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType86 createDataAreaType86() {
		DataAreaType86Impl dataAreaType86 = new DataAreaType86Impl();
		return dataAreaType86;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType87 createDataAreaType87() {
		DataAreaType87Impl dataAreaType87 = new DataAreaType87Impl();
		return dataAreaType87;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType88 createDataAreaType88() {
		DataAreaType88Impl dataAreaType88 = new DataAreaType88Impl();
		return dataAreaType88;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType89 createDataAreaType89() {
		DataAreaType89Impl dataAreaType89 = new DataAreaType89Impl();
		return dataAreaType89;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType90 createDataAreaType90() {
		DataAreaType90Impl dataAreaType90 = new DataAreaType90Impl();
		return dataAreaType90;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType91 createDataAreaType91() {
		DataAreaType91Impl dataAreaType91 = new DataAreaType91Impl();
		return dataAreaType91;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType92 createDataAreaType92() {
		DataAreaType92Impl dataAreaType92 = new DataAreaType92Impl();
		return dataAreaType92;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType93 createDataAreaType93() {
		DataAreaType93Impl dataAreaType93 = new DataAreaType93Impl();
		return dataAreaType93;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType94 createDataAreaType94() {
		DataAreaType94Impl dataAreaType94 = new DataAreaType94Impl();
		return dataAreaType94;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType95 createDataAreaType95() {
		DataAreaType95Impl dataAreaType95 = new DataAreaType95Impl();
		return dataAreaType95;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType96 createDataAreaType96() {
		DataAreaType96Impl dataAreaType96 = new DataAreaType96Impl();
		return dataAreaType96;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType97 createDataAreaType97() {
		DataAreaType97Impl dataAreaType97 = new DataAreaType97Impl();
		return dataAreaType97;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType98 createDataAreaType98() {
		DataAreaType98Impl dataAreaType98 = new DataAreaType98Impl();
		return dataAreaType98;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType99 createDataAreaType99() {
		DataAreaType99Impl dataAreaType99 = new DataAreaType99Impl();
		return dataAreaType99;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType100 createDataAreaType100() {
		DataAreaType100Impl dataAreaType100 = new DataAreaType100Impl();
		return dataAreaType100;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType101 createDataAreaType101() {
		DataAreaType101Impl dataAreaType101 = new DataAreaType101Impl();
		return dataAreaType101;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType102 createDataAreaType102() {
		DataAreaType102Impl dataAreaType102 = new DataAreaType102Impl();
		return dataAreaType102;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType103 createDataAreaType103() {
		DataAreaType103Impl dataAreaType103 = new DataAreaType103Impl();
		return dataAreaType103;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType104 createDataAreaType104() {
		DataAreaType104Impl dataAreaType104 = new DataAreaType104Impl();
		return dataAreaType104;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType105 createDataAreaType105() {
		DataAreaType105Impl dataAreaType105 = new DataAreaType105Impl();
		return dataAreaType105;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType106 createDataAreaType106() {
		DataAreaType106Impl dataAreaType106 = new DataAreaType106Impl();
		return dataAreaType106;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType107 createDataAreaType107() {
		DataAreaType107Impl dataAreaType107 = new DataAreaType107Impl();
		return dataAreaType107;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType108 createDataAreaType108() {
		DataAreaType108Impl dataAreaType108 = new DataAreaType108Impl();
		return dataAreaType108;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType109 createDataAreaType109() {
		DataAreaType109Impl dataAreaType109 = new DataAreaType109Impl();
		return dataAreaType109;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType110 createDataAreaType110() {
		DataAreaType110Impl dataAreaType110 = new DataAreaType110Impl();
		return dataAreaType110;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType111 createDataAreaType111() {
		DataAreaType111Impl dataAreaType111 = new DataAreaType111Impl();
		return dataAreaType111;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType112 createDataAreaType112() {
		DataAreaType112Impl dataAreaType112 = new DataAreaType112Impl();
		return dataAreaType112;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType113 createDataAreaType113() {
		DataAreaType113Impl dataAreaType113 = new DataAreaType113Impl();
		return dataAreaType113;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType114 createDataAreaType114() {
		DataAreaType114Impl dataAreaType114 = new DataAreaType114Impl();
		return dataAreaType114;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType115 createDataAreaType115() {
		DataAreaType115Impl dataAreaType115 = new DataAreaType115Impl();
		return dataAreaType115;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType116 createDataAreaType116() {
		DataAreaType116Impl dataAreaType116 = new DataAreaType116Impl();
		return dataAreaType116;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType117 createDataAreaType117() {
		DataAreaType117Impl dataAreaType117 = new DataAreaType117Impl();
		return dataAreaType117;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType118 createDataAreaType118() {
		DataAreaType118Impl dataAreaType118 = new DataAreaType118Impl();
		return dataAreaType118;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType119 createDataAreaType119() {
		DataAreaType119Impl dataAreaType119 = new DataAreaType119Impl();
		return dataAreaType119;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType120 createDataAreaType120() {
		DataAreaType120Impl dataAreaType120 = new DataAreaType120Impl();
		return dataAreaType120;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType121 createDataAreaType121() {
		DataAreaType121Impl dataAreaType121 = new DataAreaType121Impl();
		return dataAreaType121;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType122 createDataAreaType122() {
		DataAreaType122Impl dataAreaType122 = new DataAreaType122Impl();
		return dataAreaType122;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType123 createDataAreaType123() {
		DataAreaType123Impl dataAreaType123 = new DataAreaType123Impl();
		return dataAreaType123;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType124 createDataAreaType124() {
		DataAreaType124Impl dataAreaType124 = new DataAreaType124Impl();
		return dataAreaType124;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType125 createDataAreaType125() {
		DataAreaType125Impl dataAreaType125 = new DataAreaType125Impl();
		return dataAreaType125;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType126 createDataAreaType126() {
		DataAreaType126Impl dataAreaType126 = new DataAreaType126Impl();
		return dataAreaType126;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType127 createDataAreaType127() {
		DataAreaType127Impl dataAreaType127 = new DataAreaType127Impl();
		return dataAreaType127;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType128 createDataAreaType128() {
		DataAreaType128Impl dataAreaType128 = new DataAreaType128Impl();
		return dataAreaType128;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType129 createDataAreaType129() {
		DataAreaType129Impl dataAreaType129 = new DataAreaType129Impl();
		return dataAreaType129;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType130 createDataAreaType130() {
		DataAreaType130Impl dataAreaType130 = new DataAreaType130Impl();
		return dataAreaType130;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType131 createDataAreaType131() {
		DataAreaType131Impl dataAreaType131 = new DataAreaType131Impl();
		return dataAreaType131;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType132 createDataAreaType132() {
		DataAreaType132Impl dataAreaType132 = new DataAreaType132Impl();
		return dataAreaType132;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType133 createDataAreaType133() {
		DataAreaType133Impl dataAreaType133 = new DataAreaType133Impl();
		return dataAreaType133;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType134 createDataAreaType134() {
		DataAreaType134Impl dataAreaType134 = new DataAreaType134Impl();
		return dataAreaType134;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType135 createDataAreaType135() {
		DataAreaType135Impl dataAreaType135 = new DataAreaType135Impl();
		return dataAreaType135;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType136 createDataAreaType136() {
		DataAreaType136Impl dataAreaType136 = new DataAreaType136Impl();
		return dataAreaType136;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType137 createDataAreaType137() {
		DataAreaType137Impl dataAreaType137 = new DataAreaType137Impl();
		return dataAreaType137;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType138 createDataAreaType138() {
		DataAreaType138Impl dataAreaType138 = new DataAreaType138Impl();
		return dataAreaType138;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType139 createDataAreaType139() {
		DataAreaType139Impl dataAreaType139 = new DataAreaType139Impl();
		return dataAreaType139;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType140 createDataAreaType140() {
		DataAreaType140Impl dataAreaType140 = new DataAreaType140Impl();
		return dataAreaType140;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType141 createDataAreaType141() {
		DataAreaType141Impl dataAreaType141 = new DataAreaType141Impl();
		return dataAreaType141;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType142 createDataAreaType142() {
		DataAreaType142Impl dataAreaType142 = new DataAreaType142Impl();
		return dataAreaType142;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType143 createDataAreaType143() {
		DataAreaType143Impl dataAreaType143 = new DataAreaType143Impl();
		return dataAreaType143;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType144 createDataAreaType144() {
		DataAreaType144Impl dataAreaType144 = new DataAreaType144Impl();
		return dataAreaType144;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType145 createDataAreaType145() {
		DataAreaType145Impl dataAreaType145 = new DataAreaType145Impl();
		return dataAreaType145;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType146 createDataAreaType146() {
		DataAreaType146Impl dataAreaType146 = new DataAreaType146Impl();
		return dataAreaType146;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType147 createDataAreaType147() {
		DataAreaType147Impl dataAreaType147 = new DataAreaType147Impl();
		return dataAreaType147;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType148 createDataAreaType148() {
		DataAreaType148Impl dataAreaType148 = new DataAreaType148Impl();
		return dataAreaType148;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType149 createDataAreaType149() {
		DataAreaType149Impl dataAreaType149 = new DataAreaType149Impl();
		return dataAreaType149;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType150 createDataAreaType150() {
		DataAreaType150Impl dataAreaType150 = new DataAreaType150Impl();
		return dataAreaType150;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType151 createDataAreaType151() {
		DataAreaType151Impl dataAreaType151 = new DataAreaType151Impl();
		return dataAreaType151;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType152 createDataAreaType152() {
		DataAreaType152Impl dataAreaType152 = new DataAreaType152Impl();
		return dataAreaType152;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType153 createDataAreaType153() {
		DataAreaType153Impl dataAreaType153 = new DataAreaType153Impl();
		return dataAreaType153;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType154 createDataAreaType154() {
		DataAreaType154Impl dataAreaType154 = new DataAreaType154Impl();
		return dataAreaType154;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType155 createDataAreaType155() {
		DataAreaType155Impl dataAreaType155 = new DataAreaType155Impl();
		return dataAreaType155;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType156 createDataAreaType156() {
		DataAreaType156Impl dataAreaType156 = new DataAreaType156Impl();
		return dataAreaType156;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType157 createDataAreaType157() {
		DataAreaType157Impl dataAreaType157 = new DataAreaType157Impl();
		return dataAreaType157;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType158 createDataAreaType158() {
		DataAreaType158Impl dataAreaType158 = new DataAreaType158Impl();
		return dataAreaType158;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType159 createDataAreaType159() {
		DataAreaType159Impl dataAreaType159 = new DataAreaType159Impl();
		return dataAreaType159;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType160 createDataAreaType160() {
		DataAreaType160Impl dataAreaType160 = new DataAreaType160Impl();
		return dataAreaType160;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType161 createDataAreaType161() {
		DataAreaType161Impl dataAreaType161 = new DataAreaType161Impl();
		return dataAreaType161;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType162 createDataAreaType162() {
		DataAreaType162Impl dataAreaType162 = new DataAreaType162Impl();
		return dataAreaType162;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType163 createDataAreaType163() {
		DataAreaType163Impl dataAreaType163 = new DataAreaType163Impl();
		return dataAreaType163;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType164 createDataAreaType164() {
		DataAreaType164Impl dataAreaType164 = new DataAreaType164Impl();
		return dataAreaType164;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType165 createDataAreaType165() {
		DataAreaType165Impl dataAreaType165 = new DataAreaType165Impl();
		return dataAreaType165;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType166 createDataAreaType166() {
		DataAreaType166Impl dataAreaType166 = new DataAreaType166Impl();
		return dataAreaType166;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType167 createDataAreaType167() {
		DataAreaType167Impl dataAreaType167 = new DataAreaType167Impl();
		return dataAreaType167;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType168 createDataAreaType168() {
		DataAreaType168Impl dataAreaType168 = new DataAreaType168Impl();
		return dataAreaType168;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType169 createDataAreaType169() {
		DataAreaType169Impl dataAreaType169 = new DataAreaType169Impl();
		return dataAreaType169;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType170 createDataAreaType170() {
		DataAreaType170Impl dataAreaType170 = new DataAreaType170Impl();
		return dataAreaType170;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType171 createDataAreaType171() {
		DataAreaType171Impl dataAreaType171 = new DataAreaType171Impl();
		return dataAreaType171;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType172 createDataAreaType172() {
		DataAreaType172Impl dataAreaType172 = new DataAreaType172Impl();
		return dataAreaType172;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType173 createDataAreaType173() {
		DataAreaType173Impl dataAreaType173 = new DataAreaType173Impl();
		return dataAreaType173;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType174 createDataAreaType174() {
		DataAreaType174Impl dataAreaType174 = new DataAreaType174Impl();
		return dataAreaType174;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType175 createDataAreaType175() {
		DataAreaType175Impl dataAreaType175 = new DataAreaType175Impl();
		return dataAreaType175;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType176 createDataAreaType176() {
		DataAreaType176Impl dataAreaType176 = new DataAreaType176Impl();
		return dataAreaType176;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType177 createDataAreaType177() {
		DataAreaType177Impl dataAreaType177 = new DataAreaType177Impl();
		return dataAreaType177;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType178 createDataAreaType178() {
		DataAreaType178Impl dataAreaType178 = new DataAreaType178Impl();
		return dataAreaType178;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType179 createDataAreaType179() {
		DataAreaType179Impl dataAreaType179 = new DataAreaType179Impl();
		return dataAreaType179;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType180 createDataAreaType180() {
		DataAreaType180Impl dataAreaType180 = new DataAreaType180Impl();
		return dataAreaType180;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType181 createDataAreaType181() {
		DataAreaType181Impl dataAreaType181 = new DataAreaType181Impl();
		return dataAreaType181;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType182 createDataAreaType182() {
		DataAreaType182Impl dataAreaType182 = new DataAreaType182Impl();
		return dataAreaType182;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType183 createDataAreaType183() {
		DataAreaType183Impl dataAreaType183 = new DataAreaType183Impl();
		return dataAreaType183;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType184 createDataAreaType184() {
		DataAreaType184Impl dataAreaType184 = new DataAreaType184Impl();
		return dataAreaType184;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType185 createDataAreaType185() {
		DataAreaType185Impl dataAreaType185 = new DataAreaType185Impl();
		return dataAreaType185;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType186 createDataAreaType186() {
		DataAreaType186Impl dataAreaType186 = new DataAreaType186Impl();
		return dataAreaType186;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType187 createDataAreaType187() {
		DataAreaType187Impl dataAreaType187 = new DataAreaType187Impl();
		return dataAreaType187;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType188 createDataAreaType188() {
		DataAreaType188Impl dataAreaType188 = new DataAreaType188Impl();
		return dataAreaType188;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType189 createDataAreaType189() {
		DataAreaType189Impl dataAreaType189 = new DataAreaType189Impl();
		return dataAreaType189;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType190 createDataAreaType190() {
		DataAreaType190Impl dataAreaType190 = new DataAreaType190Impl();
		return dataAreaType190;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType191 createDataAreaType191() {
		DataAreaType191Impl dataAreaType191 = new DataAreaType191Impl();
		return dataAreaType191;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType192 createDataAreaType192() {
		DataAreaType192Impl dataAreaType192 = new DataAreaType192Impl();
		return dataAreaType192;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType193 createDataAreaType193() {
		DataAreaType193Impl dataAreaType193 = new DataAreaType193Impl();
		return dataAreaType193;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType194 createDataAreaType194() {
		DataAreaType194Impl dataAreaType194 = new DataAreaType194Impl();
		return dataAreaType194;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType195 createDataAreaType195() {
		DataAreaType195Impl dataAreaType195 = new DataAreaType195Impl();
		return dataAreaType195;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType196 createDataAreaType196() {
		DataAreaType196Impl dataAreaType196 = new DataAreaType196Impl();
		return dataAreaType196;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType197 createDataAreaType197() {
		DataAreaType197Impl dataAreaType197 = new DataAreaType197Impl();
		return dataAreaType197;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType198 createDataAreaType198() {
		DataAreaType198Impl dataAreaType198 = new DataAreaType198Impl();
		return dataAreaType198;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType199 createDataAreaType199() {
		DataAreaType199Impl dataAreaType199 = new DataAreaType199Impl();
		return dataAreaType199;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType200 createDataAreaType200() {
		DataAreaType200Impl dataAreaType200 = new DataAreaType200Impl();
		return dataAreaType200;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType201 createDataAreaType201() {
		DataAreaType201Impl dataAreaType201 = new DataAreaType201Impl();
		return dataAreaType201;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType202 createDataAreaType202() {
		DataAreaType202Impl dataAreaType202 = new DataAreaType202Impl();
		return dataAreaType202;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType203 createDataAreaType203() {
		DataAreaType203Impl dataAreaType203 = new DataAreaType203Impl();
		return dataAreaType203;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType204 createDataAreaType204() {
		DataAreaType204Impl dataAreaType204 = new DataAreaType204Impl();
		return dataAreaType204;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType205 createDataAreaType205() {
		DataAreaType205Impl dataAreaType205 = new DataAreaType205Impl();
		return dataAreaType205;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType206 createDataAreaType206() {
		DataAreaType206Impl dataAreaType206 = new DataAreaType206Impl();
		return dataAreaType206;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType207 createDataAreaType207() {
		DataAreaType207Impl dataAreaType207 = new DataAreaType207Impl();
		return dataAreaType207;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType208 createDataAreaType208() {
		DataAreaType208Impl dataAreaType208 = new DataAreaType208Impl();
		return dataAreaType208;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType1Type createDataType1Type() {
		DataType1TypeImpl dataType1Type = new DataType1TypeImpl();
		return dataType1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataTypeType createDataTypeType() {
		DataTypeTypeImpl dataTypeType = new DataTypeTypeImpl();
		return dataTypeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTimeType createDateTimeType() {
		DateTimeTypeImpl dateTimeType = new DateTimeTypeImpl();
		return dateTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dependency1Type createDependency1Type() {
		Dependency1TypeImpl dependency1Type = new Dependency1TypeImpl();
		return dependency1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DependencyType createDependencyType() {
		DependencyTypeImpl dependencyType = new DependencyTypeImpl();
		return dependencyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DescriptionType createDescriptionType() {
		DescriptionTypeImpl descriptionType = new DescriptionTypeImpl();
		return descriptionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarliestStartTimeType createEarliestStartTimeType() {
		EarliestStartTimeTypeImpl earliestStartTimeType = new EarliestStartTimeTypeImpl();
		return earliestStartTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EndTimeType createEndTimeType() {
		EndTimeTypeImpl endTimeType = new EndTimeTypeImpl();
		return endTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentAssetMappingType createEquipmentAssetMappingType() {
		EquipmentAssetMappingTypeImpl equipmentAssetMappingType = new EquipmentAssetMappingTypeImpl();
		return equipmentAssetMappingType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentCapabilityTestSpecificationIDType createEquipmentCapabilityTestSpecificationIDType() {
		EquipmentCapabilityTestSpecificationIDTypeImpl equipmentCapabilityTestSpecificationIDType = new EquipmentCapabilityTestSpecificationIDTypeImpl();
		return equipmentCapabilityTestSpecificationIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentCapabilityTestSpecificationType createEquipmentCapabilityTestSpecificationType() {
		EquipmentCapabilityTestSpecificationTypeImpl equipmentCapabilityTestSpecificationType = new EquipmentCapabilityTestSpecificationTypeImpl();
		return equipmentCapabilityTestSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClassIDType createEquipmentClassIDType() {
		EquipmentClassIDTypeImpl equipmentClassIDType = new EquipmentClassIDTypeImpl();
		return equipmentClassIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClassPropertyType createEquipmentClassPropertyType() {
		EquipmentClassPropertyTypeImpl equipmentClassPropertyType = new EquipmentClassPropertyTypeImpl();
		return equipmentClassPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClassType createEquipmentClassType() {
		EquipmentClassTypeImpl equipmentClassType = new EquipmentClassTypeImpl();
		return equipmentClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentElementLevel1Type createEquipmentElementLevel1Type() {
		EquipmentElementLevel1TypeImpl equipmentElementLevel1Type = new EquipmentElementLevel1TypeImpl();
		return equipmentElementLevel1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentElementLevelType createEquipmentElementLevelType() {
		EquipmentElementLevelTypeImpl equipmentElementLevelType = new EquipmentElementLevelTypeImpl();
		return equipmentElementLevelType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentIDType createEquipmentIDType() {
		EquipmentIDTypeImpl equipmentIDType = new EquipmentIDTypeImpl();
		return equipmentIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentInformationType createEquipmentInformationType() {
		EquipmentInformationTypeImpl equipmentInformationType = new EquipmentInformationTypeImpl();
		return equipmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentPropertyType createEquipmentPropertyType() {
		EquipmentPropertyTypeImpl equipmentPropertyType = new EquipmentPropertyTypeImpl();
		return equipmentPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentSegmentSpecificationPropertyType createEquipmentSegmentSpecificationPropertyType() {
		EquipmentSegmentSpecificationPropertyTypeImpl equipmentSegmentSpecificationPropertyType = new EquipmentSegmentSpecificationPropertyTypeImpl();
		return equipmentSegmentSpecificationPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentSegmentSpecificationType createEquipmentSegmentSpecificationType() {
		EquipmentSegmentSpecificationTypeImpl equipmentSegmentSpecificationType = new EquipmentSegmentSpecificationTypeImpl();
		return equipmentSegmentSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentType createEquipmentType() {
		EquipmentTypeImpl equipmentType = new EquipmentTypeImpl();
		return equipmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentUseType createEquipmentUseType() {
		EquipmentUseTypeImpl equipmentUseType = new EquipmentUseTypeImpl();
		return equipmentUseType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpirationTimeType createExpirationTimeType() {
		ExpirationTimeTypeImpl expirationTimeType = new ExpirationTimeTypeImpl();
		return expirationTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetEquipmentCapabilityTestSpecType createGetEquipmentCapabilityTestSpecType() {
		GetEquipmentCapabilityTestSpecTypeImpl getEquipmentCapabilityTestSpecType = new GetEquipmentCapabilityTestSpecTypeImpl();
		return getEquipmentCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetEquipmentClassType createGetEquipmentClassType() {
		GetEquipmentClassTypeImpl getEquipmentClassType = new GetEquipmentClassTypeImpl();
		return getEquipmentClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetEquipmentInformationType createGetEquipmentInformationType() {
		GetEquipmentInformationTypeImpl getEquipmentInformationType = new GetEquipmentInformationTypeImpl();
		return getEquipmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetEquipmentType createGetEquipmentType() {
		GetEquipmentTypeImpl getEquipmentType = new GetEquipmentTypeImpl();
		return getEquipmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetMaterialClassType createGetMaterialClassType() {
		GetMaterialClassTypeImpl getMaterialClassType = new GetMaterialClassTypeImpl();
		return getMaterialClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetMaterialDefinitionType createGetMaterialDefinitionType() {
		GetMaterialDefinitionTypeImpl getMaterialDefinitionType = new GetMaterialDefinitionTypeImpl();
		return getMaterialDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetMaterialInformationType createGetMaterialInformationType() {
		GetMaterialInformationTypeImpl getMaterialInformationType = new GetMaterialInformationTypeImpl();
		return getMaterialInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetMaterialLotType createGetMaterialLotType() {
		GetMaterialLotTypeImpl getMaterialLotType = new GetMaterialLotTypeImpl();
		return getMaterialLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetMaterialSubLotType createGetMaterialSubLotType() {
		GetMaterialSubLotTypeImpl getMaterialSubLotType = new GetMaterialSubLotTypeImpl();
		return getMaterialSubLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetMaterialTestSpecType createGetMaterialTestSpecType() {
		GetMaterialTestSpecTypeImpl getMaterialTestSpecType = new GetMaterialTestSpecTypeImpl();
		return getMaterialTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetOperationsCapabilityInformationType createGetOperationsCapabilityInformationType() {
		GetOperationsCapabilityInformationTypeImpl getOperationsCapabilityInformationType = new GetOperationsCapabilityInformationTypeImpl();
		return getOperationsCapabilityInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetOperationsCapabilityType createGetOperationsCapabilityType() {
		GetOperationsCapabilityTypeImpl getOperationsCapabilityType = new GetOperationsCapabilityTypeImpl();
		return getOperationsCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetOperationsDefinitionInformationType createGetOperationsDefinitionInformationType() {
		GetOperationsDefinitionInformationTypeImpl getOperationsDefinitionInformationType = new GetOperationsDefinitionInformationTypeImpl();
		return getOperationsDefinitionInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetOperationsDefinitionType createGetOperationsDefinitionType() {
		GetOperationsDefinitionTypeImpl getOperationsDefinitionType = new GetOperationsDefinitionTypeImpl();
		return getOperationsDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetOperationsPerformanceType createGetOperationsPerformanceType() {
		GetOperationsPerformanceTypeImpl getOperationsPerformanceType = new GetOperationsPerformanceTypeImpl();
		return getOperationsPerformanceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetOperationsScheduleType createGetOperationsScheduleType() {
		GetOperationsScheduleTypeImpl getOperationsScheduleType = new GetOperationsScheduleTypeImpl();
		return getOperationsScheduleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPersonnelClassType createGetPersonnelClassType() {
		GetPersonnelClassTypeImpl getPersonnelClassType = new GetPersonnelClassTypeImpl();
		return getPersonnelClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPersonnelInformationType createGetPersonnelInformationType() {
		GetPersonnelInformationTypeImpl getPersonnelInformationType = new GetPersonnelInformationTypeImpl();
		return getPersonnelInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPersonType createGetPersonType() {
		GetPersonTypeImpl getPersonType = new GetPersonTypeImpl();
		return getPersonType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPhysicalAssetCapabilityTestSpecType createGetPhysicalAssetCapabilityTestSpecType() {
		GetPhysicalAssetCapabilityTestSpecTypeImpl getPhysicalAssetCapabilityTestSpecType = new GetPhysicalAssetCapabilityTestSpecTypeImpl();
		return getPhysicalAssetCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPhysicalAssetClassType createGetPhysicalAssetClassType() {
		GetPhysicalAssetClassTypeImpl getPhysicalAssetClassType = new GetPhysicalAssetClassTypeImpl();
		return getPhysicalAssetClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPhysicalAssetInformationType createGetPhysicalAssetInformationType() {
		GetPhysicalAssetInformationTypeImpl getPhysicalAssetInformationType = new GetPhysicalAssetInformationTypeImpl();
		return getPhysicalAssetInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPhysicalAssetType createGetPhysicalAssetType() {
		GetPhysicalAssetTypeImpl getPhysicalAssetType = new GetPhysicalAssetTypeImpl();
		return getPhysicalAssetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetProcessSegmentInformationType createGetProcessSegmentInformationType() {
		GetProcessSegmentInformationTypeImpl getProcessSegmentInformationType = new GetProcessSegmentInformationTypeImpl();
		return getProcessSegmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetProcessSegmentType createGetProcessSegmentType() {
		GetProcessSegmentTypeImpl getProcessSegmentType = new GetProcessSegmentTypeImpl();
		return getProcessSegmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetQualificationTestSpecificationType createGetQualificationTestSpecificationType() {
		GetQualificationTestSpecificationTypeImpl getQualificationTestSpecificationType = new GetQualificationTestSpecificationTypeImpl();
		return getQualificationTestSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType createHierarchyScopeType() {
		HierarchyScopeTypeImpl hierarchyScopeType = new HierarchyScopeTypeImpl();
		return hierarchyScopeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType createIdentifierType() {
		IdentifierTypeImpl identifierType = new IdentifierTypeImpl();
		return identifierType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JobOrderCommand1Type createJobOrderCommand1Type() {
		JobOrderCommand1TypeImpl jobOrderCommand1Type = new JobOrderCommand1TypeImpl();
		return jobOrderCommand1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JobOrderCommandRuleType createJobOrderCommandRuleType() {
		JobOrderCommandRuleTypeImpl jobOrderCommandRuleType = new JobOrderCommandRuleTypeImpl();
		return jobOrderCommandRuleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JobOrderCommandType createJobOrderCommandType() {
		JobOrderCommandTypeImpl jobOrderCommandType = new JobOrderCommandTypeImpl();
		return jobOrderCommandType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JobOrderDispatchStatusType createJobOrderDispatchStatusType() {
		JobOrderDispatchStatusTypeImpl jobOrderDispatchStatusType = new JobOrderDispatchStatusTypeImpl();
		return jobOrderDispatchStatusType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LatestEndTimeType createLatestEndTimeType() {
		LatestEndTimeTypeImpl latestEndTimeType = new LatestEndTimeTypeImpl();
		return latestEndTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationType createLocationType() {
		LocationTypeImpl locationType = new LocationTypeImpl();
		return locationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ManufacturingBillIDType createManufacturingBillIDType() {
		ManufacturingBillIDTypeImpl manufacturingBillIDType = new ManufacturingBillIDTypeImpl();
		return manufacturingBillIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialActualIDType createMaterialActualIDType() {
		MaterialActualIDTypeImpl materialActualIDType = new MaterialActualIDTypeImpl();
		return materialActualIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialCapabilityIDType createMaterialCapabilityIDType() {
		MaterialCapabilityIDTypeImpl materialCapabilityIDType = new MaterialCapabilityIDTypeImpl();
		return materialCapabilityIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialClassIDType createMaterialClassIDType() {
		MaterialClassIDTypeImpl materialClassIDType = new MaterialClassIDTypeImpl();
		return materialClassIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialClassPropertyType createMaterialClassPropertyType() {
		MaterialClassPropertyTypeImpl materialClassPropertyType = new MaterialClassPropertyTypeImpl();
		return materialClassPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialClassType createMaterialClassType() {
		MaterialClassTypeImpl materialClassType = new MaterialClassTypeImpl();
		return materialClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialDefinitionIDType createMaterialDefinitionIDType() {
		MaterialDefinitionIDTypeImpl materialDefinitionIDType = new MaterialDefinitionIDTypeImpl();
		return materialDefinitionIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialDefinitionPropertyType createMaterialDefinitionPropertyType() {
		MaterialDefinitionPropertyTypeImpl materialDefinitionPropertyType = new MaterialDefinitionPropertyTypeImpl();
		return materialDefinitionPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialDefinitionType createMaterialDefinitionType() {
		MaterialDefinitionTypeImpl materialDefinitionType = new MaterialDefinitionTypeImpl();
		return materialDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialInformationType createMaterialInformationType() {
		MaterialInformationTypeImpl materialInformationType = new MaterialInformationTypeImpl();
		return materialInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialLotIDType createMaterialLotIDType() {
		MaterialLotIDTypeImpl materialLotIDType = new MaterialLotIDTypeImpl();
		return materialLotIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialLotPropertyType createMaterialLotPropertyType() {
		MaterialLotPropertyTypeImpl materialLotPropertyType = new MaterialLotPropertyTypeImpl();
		return materialLotPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialLotType createMaterialLotType() {
		MaterialLotTypeImpl materialLotType = new MaterialLotTypeImpl();
		return materialLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialRequirementIDType createMaterialRequirementIDType() {
		MaterialRequirementIDTypeImpl materialRequirementIDType = new MaterialRequirementIDTypeImpl();
		return materialRequirementIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSegmentSpecificationPropertyType createMaterialSegmentSpecificationPropertyType() {
		MaterialSegmentSpecificationPropertyTypeImpl materialSegmentSpecificationPropertyType = new MaterialSegmentSpecificationPropertyTypeImpl();
		return materialSegmentSpecificationPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSegmentSpecificationType createMaterialSegmentSpecificationType() {
		MaterialSegmentSpecificationTypeImpl materialSegmentSpecificationType = new MaterialSegmentSpecificationTypeImpl();
		return materialSegmentSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSpecificationIDType createMaterialSpecificationIDType() {
		MaterialSpecificationIDTypeImpl materialSpecificationIDType = new MaterialSpecificationIDTypeImpl();
		return materialSpecificationIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSubLotIDType createMaterialSubLotIDType() {
		MaterialSubLotIDTypeImpl materialSubLotIDType = new MaterialSubLotIDTypeImpl();
		return materialSubLotIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSubLotType createMaterialSubLotType() {
		MaterialSubLotTypeImpl materialSubLotType = new MaterialSubLotTypeImpl();
		return materialSubLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialTestSpecificationIDType createMaterialTestSpecificationIDType() {
		MaterialTestSpecificationIDTypeImpl materialTestSpecificationIDType = new MaterialTestSpecificationIDTypeImpl();
		return materialTestSpecificationIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialTestSpecificationType createMaterialTestSpecificationType() {
		MaterialTestSpecificationTypeImpl materialTestSpecificationType = new MaterialTestSpecificationTypeImpl();
		return materialTestSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialUse1Type createMaterialUse1Type() {
		MaterialUse1TypeImpl materialUse1Type = new MaterialUse1TypeImpl();
		return materialUse1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialUseType createMaterialUseType() {
		MaterialUseTypeImpl materialUseType = new MaterialUseTypeImpl();
		return materialUseType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MeasureType createMeasureType() {
		MeasureTypeImpl measureType = new MeasureTypeImpl();
		return measureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameType createNameType() {
		NameTypeImpl nameType = new NameTypeImpl();
		return nameType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumericType createNumericType() {
		NumericTypeImpl numericType = new NumericTypeImpl();
		return numericType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpEquipmentActualPropertyType createOpEquipmentActualPropertyType() {
		OpEquipmentActualPropertyTypeImpl opEquipmentActualPropertyType = new OpEquipmentActualPropertyTypeImpl();
		return opEquipmentActualPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpEquipmentActualType createOpEquipmentActualType() {
		OpEquipmentActualTypeImpl opEquipmentActualType = new OpEquipmentActualTypeImpl();
		return opEquipmentActualType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpEquipmentCapabilityPropertyType createOpEquipmentCapabilityPropertyType() {
		OpEquipmentCapabilityPropertyTypeImpl opEquipmentCapabilityPropertyType = new OpEquipmentCapabilityPropertyTypeImpl();
		return opEquipmentCapabilityPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpEquipmentCapabilityType createOpEquipmentCapabilityType() {
		OpEquipmentCapabilityTypeImpl opEquipmentCapabilityType = new OpEquipmentCapabilityTypeImpl();
		return opEquipmentCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpEquipmentRequirementPropertyType createOpEquipmentRequirementPropertyType() {
		OpEquipmentRequirementPropertyTypeImpl opEquipmentRequirementPropertyType = new OpEquipmentRequirementPropertyTypeImpl();
		return opEquipmentRequirementPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpEquipmentRequirementType createOpEquipmentRequirementType() {
		OpEquipmentRequirementTypeImpl opEquipmentRequirementType = new OpEquipmentRequirementTypeImpl();
		return opEquipmentRequirementType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpEquipmentSpecificationPropertyType createOpEquipmentSpecificationPropertyType() {
		OpEquipmentSpecificationPropertyTypeImpl opEquipmentSpecificationPropertyType = new OpEquipmentSpecificationPropertyTypeImpl();
		return opEquipmentSpecificationPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpEquipmentSpecificationType createOpEquipmentSpecificationType() {
		OpEquipmentSpecificationTypeImpl opEquipmentSpecificationType = new OpEquipmentSpecificationTypeImpl();
		return opEquipmentSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsCapabilityInformationType createOperationsCapabilityInformationType() {
		OperationsCapabilityInformationTypeImpl operationsCapabilityInformationType = new OperationsCapabilityInformationTypeImpl();
		return operationsCapabilityInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsCapabilityType createOperationsCapabilityType() {
		OperationsCapabilityTypeImpl operationsCapabilityType = new OperationsCapabilityTypeImpl();
		return operationsCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinitionIDType createOperationsDefinitionIDType() {
		OperationsDefinitionIDTypeImpl operationsDefinitionIDType = new OperationsDefinitionIDTypeImpl();
		return operationsDefinitionIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinitionInformationType createOperationsDefinitionInformationType() {
		OperationsDefinitionInformationTypeImpl operationsDefinitionInformationType = new OperationsDefinitionInformationTypeImpl();
		return operationsDefinitionInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinitionType createOperationsDefinitionType() {
		OperationsDefinitionTypeImpl operationsDefinitionType = new OperationsDefinitionTypeImpl();
		return operationsDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsMaterialBillItemType createOperationsMaterialBillItemType() {
		OperationsMaterialBillItemTypeImpl operationsMaterialBillItemType = new OperationsMaterialBillItemTypeImpl();
		return operationsMaterialBillItemType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsMaterialBillType createOperationsMaterialBillType() {
		OperationsMaterialBillTypeImpl operationsMaterialBillType = new OperationsMaterialBillTypeImpl();
		return operationsMaterialBillType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsPerformanceType createOperationsPerformanceType() {
		OperationsPerformanceTypeImpl operationsPerformanceType = new OperationsPerformanceTypeImpl();
		return operationsPerformanceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsRequestIDType createOperationsRequestIDType() {
		OperationsRequestIDTypeImpl operationsRequestIDType = new OperationsRequestIDTypeImpl();
		return operationsRequestIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsRequestType createOperationsRequestType() {
		OperationsRequestTypeImpl operationsRequestType = new OperationsRequestTypeImpl();
		return operationsRequestType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsResponseType createOperationsResponseType() {
		OperationsResponseTypeImpl operationsResponseType = new OperationsResponseTypeImpl();
		return operationsResponseType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsScheduleIDType createOperationsScheduleIDType() {
		OperationsScheduleIDTypeImpl operationsScheduleIDType = new OperationsScheduleIDTypeImpl();
		return operationsScheduleIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsScheduleType createOperationsScheduleType() {
		OperationsScheduleTypeImpl operationsScheduleType = new OperationsScheduleTypeImpl();
		return operationsScheduleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsSegmentIDType createOperationsSegmentIDType() {
		OperationsSegmentIDTypeImpl operationsSegmentIDType = new OperationsSegmentIDTypeImpl();
		return operationsSegmentIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsSegmentType createOperationsSegmentType() {
		OperationsSegmentTypeImpl operationsSegmentType = new OperationsSegmentTypeImpl();
		return operationsSegmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsType1Type createOperationsType1Type() {
		OperationsType1TypeImpl operationsType1Type = new OperationsType1TypeImpl();
		return operationsType1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsTypeType createOperationsTypeType() {
		OperationsTypeTypeImpl operationsTypeType = new OperationsTypeTypeImpl();
		return operationsTypeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpMaterialActualPropertyType createOpMaterialActualPropertyType() {
		OpMaterialActualPropertyTypeImpl opMaterialActualPropertyType = new OpMaterialActualPropertyTypeImpl();
		return opMaterialActualPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpMaterialActualType createOpMaterialActualType() {
		OpMaterialActualTypeImpl opMaterialActualType = new OpMaterialActualTypeImpl();
		return opMaterialActualType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpMaterialCapabilityPropertyType createOpMaterialCapabilityPropertyType() {
		OpMaterialCapabilityPropertyTypeImpl opMaterialCapabilityPropertyType = new OpMaterialCapabilityPropertyTypeImpl();
		return opMaterialCapabilityPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpMaterialCapabilityType createOpMaterialCapabilityType() {
		OpMaterialCapabilityTypeImpl opMaterialCapabilityType = new OpMaterialCapabilityTypeImpl();
		return opMaterialCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpMaterialRequirementPropertyType createOpMaterialRequirementPropertyType() {
		OpMaterialRequirementPropertyTypeImpl opMaterialRequirementPropertyType = new OpMaterialRequirementPropertyTypeImpl();
		return opMaterialRequirementPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpMaterialRequirementType createOpMaterialRequirementType() {
		OpMaterialRequirementTypeImpl opMaterialRequirementType = new OpMaterialRequirementTypeImpl();
		return opMaterialRequirementType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpMaterialSpecificationPropertyType createOpMaterialSpecificationPropertyType() {
		OpMaterialSpecificationPropertyTypeImpl opMaterialSpecificationPropertyType = new OpMaterialSpecificationPropertyTypeImpl();
		return opMaterialSpecificationPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpMaterialSpecificationType createOpMaterialSpecificationType() {
		OpMaterialSpecificationTypeImpl opMaterialSpecificationType = new OpMaterialSpecificationTypeImpl();
		return opMaterialSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPersonnelActualPropertyType createOpPersonnelActualPropertyType() {
		OpPersonnelActualPropertyTypeImpl opPersonnelActualPropertyType = new OpPersonnelActualPropertyTypeImpl();
		return opPersonnelActualPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPersonnelActualType createOpPersonnelActualType() {
		OpPersonnelActualTypeImpl opPersonnelActualType = new OpPersonnelActualTypeImpl();
		return opPersonnelActualType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPersonnelCapabilityPropertyType createOpPersonnelCapabilityPropertyType() {
		OpPersonnelCapabilityPropertyTypeImpl opPersonnelCapabilityPropertyType = new OpPersonnelCapabilityPropertyTypeImpl();
		return opPersonnelCapabilityPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPersonnelCapabilityType createOpPersonnelCapabilityType() {
		OpPersonnelCapabilityTypeImpl opPersonnelCapabilityType = new OpPersonnelCapabilityTypeImpl();
		return opPersonnelCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPersonnelRequirementPropertyType createOpPersonnelRequirementPropertyType() {
		OpPersonnelRequirementPropertyTypeImpl opPersonnelRequirementPropertyType = new OpPersonnelRequirementPropertyTypeImpl();
		return opPersonnelRequirementPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPersonnelRequirementType createOpPersonnelRequirementType() {
		OpPersonnelRequirementTypeImpl opPersonnelRequirementType = new OpPersonnelRequirementTypeImpl();
		return opPersonnelRequirementType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPersonnelSpecificationPropertyType createOpPersonnelSpecificationPropertyType() {
		OpPersonnelSpecificationPropertyTypeImpl opPersonnelSpecificationPropertyType = new OpPersonnelSpecificationPropertyTypeImpl();
		return opPersonnelSpecificationPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPersonnelSpecificationType createOpPersonnelSpecificationType() {
		OpPersonnelSpecificationTypeImpl opPersonnelSpecificationType = new OpPersonnelSpecificationTypeImpl();
		return opPersonnelSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPhysicalAssetActualPropertyType createOpPhysicalAssetActualPropertyType() {
		OpPhysicalAssetActualPropertyTypeImpl opPhysicalAssetActualPropertyType = new OpPhysicalAssetActualPropertyTypeImpl();
		return opPhysicalAssetActualPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPhysicalAssetActualType createOpPhysicalAssetActualType() {
		OpPhysicalAssetActualTypeImpl opPhysicalAssetActualType = new OpPhysicalAssetActualTypeImpl();
		return opPhysicalAssetActualType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPhysicalAssetCapabilityPropertyType createOpPhysicalAssetCapabilityPropertyType() {
		OpPhysicalAssetCapabilityPropertyTypeImpl opPhysicalAssetCapabilityPropertyType = new OpPhysicalAssetCapabilityPropertyTypeImpl();
		return opPhysicalAssetCapabilityPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPhysicalAssetCapabilityType createOpPhysicalAssetCapabilityType() {
		OpPhysicalAssetCapabilityTypeImpl opPhysicalAssetCapabilityType = new OpPhysicalAssetCapabilityTypeImpl();
		return opPhysicalAssetCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPhysicalAssetRequirementPropertyType createOpPhysicalAssetRequirementPropertyType() {
		OpPhysicalAssetRequirementPropertyTypeImpl opPhysicalAssetRequirementPropertyType = new OpPhysicalAssetRequirementPropertyTypeImpl();
		return opPhysicalAssetRequirementPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPhysicalAssetRequirementType createOpPhysicalAssetRequirementType() {
		OpPhysicalAssetRequirementTypeImpl opPhysicalAssetRequirementType = new OpPhysicalAssetRequirementTypeImpl();
		return opPhysicalAssetRequirementType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPhysicalAssetSpecificationPropertyType createOpPhysicalAssetSpecificationPropertyType() {
		OpPhysicalAssetSpecificationPropertyTypeImpl opPhysicalAssetSpecificationPropertyType = new OpPhysicalAssetSpecificationPropertyTypeImpl();
		return opPhysicalAssetSpecificationPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpPhysicalAssetSpecificationType createOpPhysicalAssetSpecificationType() {
		OpPhysicalAssetSpecificationTypeImpl opPhysicalAssetSpecificationType = new OpPhysicalAssetSpecificationTypeImpl();
		return opPhysicalAssetSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpProcessSegmentCapabilityType createOpProcessSegmentCapabilityType() {
		OpProcessSegmentCapabilityTypeImpl opProcessSegmentCapabilityType = new OpProcessSegmentCapabilityTypeImpl();
		return opProcessSegmentCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpSegmentDataType createOpSegmentDataType() {
		OpSegmentDataTypeImpl opSegmentDataType = new OpSegmentDataTypeImpl();
		return opSegmentDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpSegmentRequirementType createOpSegmentRequirementType() {
		OpSegmentRequirementTypeImpl opSegmentRequirementType = new OpSegmentRequirementTypeImpl();
		return opSegmentRequirementType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OpSegmentResponseType createOpSegmentResponseType() {
		OpSegmentResponseTypeImpl opSegmentResponseType = new OpSegmentResponseTypeImpl();
		return opSegmentResponseType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OtherDependencyType createOtherDependencyType() {
		OtherDependencyTypeImpl otherDependencyType = new OtherDependencyTypeImpl();
		return otherDependencyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterIDType createParameterIDType() {
		ParameterIDTypeImpl parameterIDType = new ParameterIDTypeImpl();
		return parameterIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterType createParameterType() {
		ParameterTypeImpl parameterType = new ParameterTypeImpl();
		return parameterType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonIDType createPersonIDType() {
		PersonIDTypeImpl personIDType = new PersonIDTypeImpl();
		return personIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonNameType createPersonNameType() {
		PersonNameTypeImpl personNameType = new PersonNameTypeImpl();
		return personNameType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelClassIDType createPersonnelClassIDType() {
		PersonnelClassIDTypeImpl personnelClassIDType = new PersonnelClassIDTypeImpl();
		return personnelClassIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelClassPropertyType createPersonnelClassPropertyType() {
		PersonnelClassPropertyTypeImpl personnelClassPropertyType = new PersonnelClassPropertyTypeImpl();
		return personnelClassPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelClassType createPersonnelClassType() {
		PersonnelClassTypeImpl personnelClassType = new PersonnelClassTypeImpl();
		return personnelClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelInformationType createPersonnelInformationType() {
		PersonnelInformationTypeImpl personnelInformationType = new PersonnelInformationTypeImpl();
		return personnelInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelSegmentSpecificationPropertyType createPersonnelSegmentSpecificationPropertyType() {
		PersonnelSegmentSpecificationPropertyTypeImpl personnelSegmentSpecificationPropertyType = new PersonnelSegmentSpecificationPropertyTypeImpl();
		return personnelSegmentSpecificationPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelSegmentSpecificationType createPersonnelSegmentSpecificationType() {
		PersonnelSegmentSpecificationTypeImpl personnelSegmentSpecificationType = new PersonnelSegmentSpecificationTypeImpl();
		return personnelSegmentSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelUseType createPersonnelUseType() {
		PersonnelUseTypeImpl personnelUseType = new PersonnelUseTypeImpl();
		return personnelUseType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonPropertyType createPersonPropertyType() {
		PersonPropertyTypeImpl personPropertyType = new PersonPropertyTypeImpl();
		return personPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonType createPersonType() {
		PersonTypeImpl personType = new PersonTypeImpl();
		return personType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetActualIDType createPhysicalAssetActualIDType() {
		PhysicalAssetActualIDTypeImpl physicalAssetActualIDType = new PhysicalAssetActualIDTypeImpl();
		return physicalAssetActualIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetCapabilityTestSpecificationIDType createPhysicalAssetCapabilityTestSpecificationIDType() {
		PhysicalAssetCapabilityTestSpecificationIDTypeImpl physicalAssetCapabilityTestSpecificationIDType = new PhysicalAssetCapabilityTestSpecificationIDTypeImpl();
		return physicalAssetCapabilityTestSpecificationIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetCapabilityTestSpecificationType createPhysicalAssetCapabilityTestSpecificationType() {
		PhysicalAssetCapabilityTestSpecificationTypeImpl physicalAssetCapabilityTestSpecificationType = new PhysicalAssetCapabilityTestSpecificationTypeImpl();
		return physicalAssetCapabilityTestSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClassIDType createPhysicalAssetClassIDType() {
		PhysicalAssetClassIDTypeImpl physicalAssetClassIDType = new PhysicalAssetClassIDTypeImpl();
		return physicalAssetClassIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClassPropertyType createPhysicalAssetClassPropertyType() {
		PhysicalAssetClassPropertyTypeImpl physicalAssetClassPropertyType = new PhysicalAssetClassPropertyTypeImpl();
		return physicalAssetClassPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClassType createPhysicalAssetClassType() {
		PhysicalAssetClassTypeImpl physicalAssetClassType = new PhysicalAssetClassTypeImpl();
		return physicalAssetClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetIDType createPhysicalAssetIDType() {
		PhysicalAssetIDTypeImpl physicalAssetIDType = new PhysicalAssetIDTypeImpl();
		return physicalAssetIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetInformationType createPhysicalAssetInformationType() {
		PhysicalAssetInformationTypeImpl physicalAssetInformationType = new PhysicalAssetInformationTypeImpl();
		return physicalAssetInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetPropertyType createPhysicalAssetPropertyType() {
		PhysicalAssetPropertyTypeImpl physicalAssetPropertyType = new PhysicalAssetPropertyTypeImpl();
		return physicalAssetPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetSegmentSpecificationPropertyType createPhysicalAssetSegmentSpecificationPropertyType() {
		PhysicalAssetSegmentSpecificationPropertyTypeImpl physicalAssetSegmentSpecificationPropertyType = new PhysicalAssetSegmentSpecificationPropertyTypeImpl();
		return physicalAssetSegmentSpecificationPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetSegmentSpecificationType createPhysicalAssetSegmentSpecificationType() {
		PhysicalAssetSegmentSpecificationTypeImpl physicalAssetSegmentSpecificationType = new PhysicalAssetSegmentSpecificationTypeImpl();
		return physicalAssetSegmentSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetType createPhysicalAssetType() {
		PhysicalAssetTypeImpl physicalAssetType = new PhysicalAssetTypeImpl();
		return physicalAssetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetUseType createPhysicalAssetUseType() {
		PhysicalAssetUseTypeImpl physicalAssetUseType = new PhysicalAssetUseTypeImpl();
		return physicalAssetUseType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlannedFinishTimeType createPlannedFinishTimeType() {
		PlannedFinishTimeTypeImpl plannedFinishTimeType = new PlannedFinishTimeTypeImpl();
		return plannedFinishTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlannedStartTimeType createPlannedStartTimeType() {
		PlannedStartTimeTypeImpl plannedStartTimeType = new PlannedStartTimeTypeImpl();
		return plannedStartTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PriorityType createPriorityType() {
		PriorityTypeImpl priorityType = new PriorityTypeImpl();
		return priorityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProblemType createProblemType() {
		ProblemTypeImpl problemType = new ProblemTypeImpl();
		return problemType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessEquipmentCapabilityTestSpecType createProcessEquipmentCapabilityTestSpecType() {
		ProcessEquipmentCapabilityTestSpecTypeImpl processEquipmentCapabilityTestSpecType = new ProcessEquipmentCapabilityTestSpecTypeImpl();
		return processEquipmentCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessEquipmentClassType createProcessEquipmentClassType() {
		ProcessEquipmentClassTypeImpl processEquipmentClassType = new ProcessEquipmentClassTypeImpl();
		return processEquipmentClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessEquipmentInformationType createProcessEquipmentInformationType() {
		ProcessEquipmentInformationTypeImpl processEquipmentInformationType = new ProcessEquipmentInformationTypeImpl();
		return processEquipmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessEquipmentType createProcessEquipmentType() {
		ProcessEquipmentTypeImpl processEquipmentType = new ProcessEquipmentTypeImpl();
		return processEquipmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessMaterialClassType createProcessMaterialClassType() {
		ProcessMaterialClassTypeImpl processMaterialClassType = new ProcessMaterialClassTypeImpl();
		return processMaterialClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessMaterialDefinitionType createProcessMaterialDefinitionType() {
		ProcessMaterialDefinitionTypeImpl processMaterialDefinitionType = new ProcessMaterialDefinitionTypeImpl();
		return processMaterialDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessMaterialInformationType createProcessMaterialInformationType() {
		ProcessMaterialInformationTypeImpl processMaterialInformationType = new ProcessMaterialInformationTypeImpl();
		return processMaterialInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessMaterialLotType createProcessMaterialLotType() {
		ProcessMaterialLotTypeImpl processMaterialLotType = new ProcessMaterialLotTypeImpl();
		return processMaterialLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessMaterialSubLotType createProcessMaterialSubLotType() {
		ProcessMaterialSubLotTypeImpl processMaterialSubLotType = new ProcessMaterialSubLotTypeImpl();
		return processMaterialSubLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessMaterialTestSpecType createProcessMaterialTestSpecType() {
		ProcessMaterialTestSpecTypeImpl processMaterialTestSpecType = new ProcessMaterialTestSpecTypeImpl();
		return processMaterialTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessOperationsCapabilityInformationType createProcessOperationsCapabilityInformationType() {
		ProcessOperationsCapabilityInformationTypeImpl processOperationsCapabilityInformationType = new ProcessOperationsCapabilityInformationTypeImpl();
		return processOperationsCapabilityInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessOperationsCapabilityType createProcessOperationsCapabilityType() {
		ProcessOperationsCapabilityTypeImpl processOperationsCapabilityType = new ProcessOperationsCapabilityTypeImpl();
		return processOperationsCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessOperationsDefinitionInformationType createProcessOperationsDefinitionInformationType() {
		ProcessOperationsDefinitionInformationTypeImpl processOperationsDefinitionInformationType = new ProcessOperationsDefinitionInformationTypeImpl();
		return processOperationsDefinitionInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessOperationsDefinitionType createProcessOperationsDefinitionType() {
		ProcessOperationsDefinitionTypeImpl processOperationsDefinitionType = new ProcessOperationsDefinitionTypeImpl();
		return processOperationsDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessOperationsPerformanceType createProcessOperationsPerformanceType() {
		ProcessOperationsPerformanceTypeImpl processOperationsPerformanceType = new ProcessOperationsPerformanceTypeImpl();
		return processOperationsPerformanceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessOperationsScheduleType createProcessOperationsScheduleType() {
		ProcessOperationsScheduleTypeImpl processOperationsScheduleType = new ProcessOperationsScheduleTypeImpl();
		return processOperationsScheduleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPersonnelClassType createProcessPersonnelClassType() {
		ProcessPersonnelClassTypeImpl processPersonnelClassType = new ProcessPersonnelClassTypeImpl();
		return processPersonnelClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPersonnelInformationType createProcessPersonnelInformationType() {
		ProcessPersonnelInformationTypeImpl processPersonnelInformationType = new ProcessPersonnelInformationTypeImpl();
		return processPersonnelInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPersonType createProcessPersonType() {
		ProcessPersonTypeImpl processPersonType = new ProcessPersonTypeImpl();
		return processPersonType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPhysicalAssetCapabilityTestSpecType createProcessPhysicalAssetCapabilityTestSpecType() {
		ProcessPhysicalAssetCapabilityTestSpecTypeImpl processPhysicalAssetCapabilityTestSpecType = new ProcessPhysicalAssetCapabilityTestSpecTypeImpl();
		return processPhysicalAssetCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPhysicalAssetClassType createProcessPhysicalAssetClassType() {
		ProcessPhysicalAssetClassTypeImpl processPhysicalAssetClassType = new ProcessPhysicalAssetClassTypeImpl();
		return processPhysicalAssetClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPhysicalAssetInformationType createProcessPhysicalAssetInformationType() {
		ProcessPhysicalAssetInformationTypeImpl processPhysicalAssetInformationType = new ProcessPhysicalAssetInformationTypeImpl();
		return processPhysicalAssetInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPhysicalAssetType createProcessPhysicalAssetType() {
		ProcessPhysicalAssetTypeImpl processPhysicalAssetType = new ProcessPhysicalAssetTypeImpl();
		return processPhysicalAssetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessProcessSegmentInformationType createProcessProcessSegmentInformationType() {
		ProcessProcessSegmentInformationTypeImpl processProcessSegmentInformationType = new ProcessProcessSegmentInformationTypeImpl();
		return processProcessSegmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessProcessSegmentType createProcessProcessSegmentType() {
		ProcessProcessSegmentTypeImpl processProcessSegmentType = new ProcessProcessSegmentTypeImpl();
		return processProcessSegmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessQualificationTestSpecificationType createProcessQualificationTestSpecificationType() {
		ProcessQualificationTestSpecificationTypeImpl processQualificationTestSpecificationType = new ProcessQualificationTestSpecificationTypeImpl();
		return processQualificationTestSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentIDType createProcessSegmentIDType() {
		ProcessSegmentIDTypeImpl processSegmentIDType = new ProcessSegmentIDTypeImpl();
		return processSegmentIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentInformationType createProcessSegmentInformationType() {
		ProcessSegmentInformationTypeImpl processSegmentInformationType = new ProcessSegmentInformationTypeImpl();
		return processSegmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentType createProcessSegmentType() {
		ProcessSegmentTypeImpl processSegmentType = new ProcessSegmentTypeImpl();
		return processSegmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProductionRequestIDType createProductionRequestIDType() {
		ProductionRequestIDTypeImpl productionRequestIDType = new ProductionRequestIDTypeImpl();
		return productionRequestIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProductionScheduleIDType createProductionScheduleIDType() {
		ProductionScheduleIDTypeImpl productionScheduleIDType = new ProductionScheduleIDTypeImpl();
		return productionScheduleIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProductProductionRuleIDType createProductProductionRuleIDType() {
		ProductProductionRuleIDTypeImpl productProductionRuleIDType = new ProductProductionRuleIDTypeImpl();
		return productProductionRuleIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProductProductionRuleType createProductProductionRuleType() {
		ProductProductionRuleTypeImpl productProductionRuleType = new ProductProductionRuleTypeImpl();
		return productProductionRuleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProductSegmentIDType createProductSegmentIDType() {
		ProductSegmentIDTypeImpl productSegmentIDType = new ProductSegmentIDTypeImpl();
		return productSegmentIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyIDType createPropertyIDType() {
		PropertyIDTypeImpl propertyIDType = new PropertyIDTypeImpl();
		return propertyIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PublishedDateType createPublishedDateType() {
		PublishedDateTypeImpl publishedDateType = new PublishedDateTypeImpl();
		return publishedDateType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualificationTestSpecificationIDType createQualificationTestSpecificationIDType() {
		QualificationTestSpecificationIDTypeImpl qualificationTestSpecificationIDType = new QualificationTestSpecificationIDTypeImpl();
		return qualificationTestSpecificationIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualificationTestSpecificationType createQualificationTestSpecificationType() {
		QualificationTestSpecificationTypeImpl qualificationTestSpecificationType = new QualificationTestSpecificationTypeImpl();
		return qualificationTestSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuantityStringType createQuantityStringType() {
		QuantityStringTypeImpl quantityStringType = new QuantityStringTypeImpl();
		return quantityStringType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuantityType createQuantityType() {
		QuantityTypeImpl quantityType = new QuantityTypeImpl();
		return quantityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuantityValueType createQuantityValueType() {
		QuantityValueTypeImpl quantityValueType = new QuantityValueTypeImpl();
		return quantityValueType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReasonType createReasonType() {
		ReasonTypeImpl reasonType = new ReasonTypeImpl();
		return reasonType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipForm1Type createRelationshipForm1Type() {
		RelationshipForm1TypeImpl relationshipForm1Type = new RelationshipForm1TypeImpl();
		return relationshipForm1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipFormType createRelationshipFormType() {
		RelationshipFormTypeImpl relationshipFormType = new RelationshipFormTypeImpl();
		return relationshipFormType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipType1Type createRelationshipType1Type() {
		RelationshipType1TypeImpl relationshipType1Type = new RelationshipType1TypeImpl();
		return relationshipType1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipTypeType createRelationshipTypeType() {
		RelationshipTypeTypeImpl relationshipTypeType = new RelationshipTypeTypeImpl();
		return relationshipTypeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequestedCompletionDateType createRequestedCompletionDateType() {
		RequestedCompletionDateTypeImpl requestedCompletionDateType = new RequestedCompletionDateTypeImpl();
		return requestedCompletionDateType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequestedPriorityType createRequestedPriorityType() {
		RequestedPriorityTypeImpl requestedPriorityType = new RequestedPriorityTypeImpl();
		return requestedPriorityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequestState1Type createRequestState1Type() {
		RequestState1TypeImpl requestState1Type = new RequestState1TypeImpl();
		return requestState1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequestStateType createRequestStateType() {
		RequestStateTypeImpl requestStateType = new RequestStateTypeImpl();
		return requestStateType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredByRequestedSegmentResponse1Type createRequiredByRequestedSegmentResponse1Type() {
		RequiredByRequestedSegmentResponse1TypeImpl requiredByRequestedSegmentResponse1Type = new RequiredByRequestedSegmentResponse1TypeImpl();
		return requiredByRequestedSegmentResponse1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredByRequestedSegmentResponseType createRequiredByRequestedSegmentResponseType() {
		RequiredByRequestedSegmentResponseTypeImpl requiredByRequestedSegmentResponseType = new RequiredByRequestedSegmentResponseTypeImpl();
		return requiredByRequestedSegmentResponseType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceIDType createResourceIDType() {
		ResourceIDTypeImpl resourceIDType = new ResourceIDTypeImpl();
		return resourceIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceNetworkConnectionIDType createResourceNetworkConnectionIDType() {
		ResourceNetworkConnectionIDTypeImpl resourceNetworkConnectionIDType = new ResourceNetworkConnectionIDTypeImpl();
		return resourceNetworkConnectionIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceReferenceType1Type createResourceReferenceType1Type() {
		ResourceReferenceType1TypeImpl resourceReferenceType1Type = new ResourceReferenceType1TypeImpl();
		return resourceReferenceType1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceReferenceTypeType createResourceReferenceTypeType() {
		ResourceReferenceTypeTypeImpl resourceReferenceTypeType = new ResourceReferenceTypeTypeImpl();
		return resourceReferenceTypeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourcesType createResourcesType() {
		ResourcesTypeImpl resourcesType = new ResourcesTypeImpl();
		return resourcesType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondEquipmentCapabilityTestSpecType createRespondEquipmentCapabilityTestSpecType() {
		RespondEquipmentCapabilityTestSpecTypeImpl respondEquipmentCapabilityTestSpecType = new RespondEquipmentCapabilityTestSpecTypeImpl();
		return respondEquipmentCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondEquipmentClassType createRespondEquipmentClassType() {
		RespondEquipmentClassTypeImpl respondEquipmentClassType = new RespondEquipmentClassTypeImpl();
		return respondEquipmentClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondEquipmentInformationType createRespondEquipmentInformationType() {
		RespondEquipmentInformationTypeImpl respondEquipmentInformationType = new RespondEquipmentInformationTypeImpl();
		return respondEquipmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondEquipmentType createRespondEquipmentType() {
		RespondEquipmentTypeImpl respondEquipmentType = new RespondEquipmentTypeImpl();
		return respondEquipmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondMaterialClassType createRespondMaterialClassType() {
		RespondMaterialClassTypeImpl respondMaterialClassType = new RespondMaterialClassTypeImpl();
		return respondMaterialClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondMaterialDefinitionType createRespondMaterialDefinitionType() {
		RespondMaterialDefinitionTypeImpl respondMaterialDefinitionType = new RespondMaterialDefinitionTypeImpl();
		return respondMaterialDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondMaterialInformationType createRespondMaterialInformationType() {
		RespondMaterialInformationTypeImpl respondMaterialInformationType = new RespondMaterialInformationTypeImpl();
		return respondMaterialInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondMaterialLotType createRespondMaterialLotType() {
		RespondMaterialLotTypeImpl respondMaterialLotType = new RespondMaterialLotTypeImpl();
		return respondMaterialLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondMaterialSubLotType createRespondMaterialSubLotType() {
		RespondMaterialSubLotTypeImpl respondMaterialSubLotType = new RespondMaterialSubLotTypeImpl();
		return respondMaterialSubLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondMaterialTestSpecType createRespondMaterialTestSpecType() {
		RespondMaterialTestSpecTypeImpl respondMaterialTestSpecType = new RespondMaterialTestSpecTypeImpl();
		return respondMaterialTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondOperationsCapabilityInformationType createRespondOperationsCapabilityInformationType() {
		RespondOperationsCapabilityInformationTypeImpl respondOperationsCapabilityInformationType = new RespondOperationsCapabilityInformationTypeImpl();
		return respondOperationsCapabilityInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondOperationsCapabilityType createRespondOperationsCapabilityType() {
		RespondOperationsCapabilityTypeImpl respondOperationsCapabilityType = new RespondOperationsCapabilityTypeImpl();
		return respondOperationsCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondOperationsDefinitionInformationType createRespondOperationsDefinitionInformationType() {
		RespondOperationsDefinitionInformationTypeImpl respondOperationsDefinitionInformationType = new RespondOperationsDefinitionInformationTypeImpl();
		return respondOperationsDefinitionInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondOperationsDefinitionType createRespondOperationsDefinitionType() {
		RespondOperationsDefinitionTypeImpl respondOperationsDefinitionType = new RespondOperationsDefinitionTypeImpl();
		return respondOperationsDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondOperationsPerformanceType createRespondOperationsPerformanceType() {
		RespondOperationsPerformanceTypeImpl respondOperationsPerformanceType = new RespondOperationsPerformanceTypeImpl();
		return respondOperationsPerformanceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondOperationsScheduleType createRespondOperationsScheduleType() {
		RespondOperationsScheduleTypeImpl respondOperationsScheduleType = new RespondOperationsScheduleTypeImpl();
		return respondOperationsScheduleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPersonnelClassType createRespondPersonnelClassType() {
		RespondPersonnelClassTypeImpl respondPersonnelClassType = new RespondPersonnelClassTypeImpl();
		return respondPersonnelClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPersonnelInformationType createRespondPersonnelInformationType() {
		RespondPersonnelInformationTypeImpl respondPersonnelInformationType = new RespondPersonnelInformationTypeImpl();
		return respondPersonnelInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPersonType createRespondPersonType() {
		RespondPersonTypeImpl respondPersonType = new RespondPersonTypeImpl();
		return respondPersonType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPhysicalAssetCapabilityTestSpecType createRespondPhysicalAssetCapabilityTestSpecType() {
		RespondPhysicalAssetCapabilityTestSpecTypeImpl respondPhysicalAssetCapabilityTestSpecType = new RespondPhysicalAssetCapabilityTestSpecTypeImpl();
		return respondPhysicalAssetCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPhysicalAssetClassType createRespondPhysicalAssetClassType() {
		RespondPhysicalAssetClassTypeImpl respondPhysicalAssetClassType = new RespondPhysicalAssetClassTypeImpl();
		return respondPhysicalAssetClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPhysicalAssetInformationType createRespondPhysicalAssetInformationType() {
		RespondPhysicalAssetInformationTypeImpl respondPhysicalAssetInformationType = new RespondPhysicalAssetInformationTypeImpl();
		return respondPhysicalAssetInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPhysicalAssetType createRespondPhysicalAssetType() {
		RespondPhysicalAssetTypeImpl respondPhysicalAssetType = new RespondPhysicalAssetTypeImpl();
		return respondPhysicalAssetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondProcessSegmentInformationType createRespondProcessSegmentInformationType() {
		RespondProcessSegmentInformationTypeImpl respondProcessSegmentInformationType = new RespondProcessSegmentInformationTypeImpl();
		return respondProcessSegmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondProcessSegmentType createRespondProcessSegmentType() {
		RespondProcessSegmentTypeImpl respondProcessSegmentType = new RespondProcessSegmentTypeImpl();
		return respondProcessSegmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondQualificationTestSpecificationType createRespondQualificationTestSpecificationType() {
		RespondQualificationTestSpecificationTypeImpl respondQualificationTestSpecificationType = new RespondQualificationTestSpecificationTypeImpl();
		return respondQualificationTestSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseState1Type createResponseState1Type() {
		ResponseState1TypeImpl responseState1Type = new ResponseState1TypeImpl();
		return responseState1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseStateType createResponseStateType() {
		ResponseStateTypeImpl responseStateType = new ResponseStateTypeImpl();
		return responseStateType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResultType createResultType() {
		ResultTypeImpl resultType = new ResultTypeImpl();
		return resultType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SegmentDependencyType createSegmentDependencyType() {
		SegmentDependencyTypeImpl segmentDependencyType = new SegmentDependencyTypeImpl();
		return segmentDependencyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowEquipmentCapabilityTestSpecType createShowEquipmentCapabilityTestSpecType() {
		ShowEquipmentCapabilityTestSpecTypeImpl showEquipmentCapabilityTestSpecType = new ShowEquipmentCapabilityTestSpecTypeImpl();
		return showEquipmentCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowEquipmentClassType createShowEquipmentClassType() {
		ShowEquipmentClassTypeImpl showEquipmentClassType = new ShowEquipmentClassTypeImpl();
		return showEquipmentClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowEquipmentInformationType createShowEquipmentInformationType() {
		ShowEquipmentInformationTypeImpl showEquipmentInformationType = new ShowEquipmentInformationTypeImpl();
		return showEquipmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowEquipmentType createShowEquipmentType() {
		ShowEquipmentTypeImpl showEquipmentType = new ShowEquipmentTypeImpl();
		return showEquipmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowMaterialClassType createShowMaterialClassType() {
		ShowMaterialClassTypeImpl showMaterialClassType = new ShowMaterialClassTypeImpl();
		return showMaterialClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowMaterialDefinitionType createShowMaterialDefinitionType() {
		ShowMaterialDefinitionTypeImpl showMaterialDefinitionType = new ShowMaterialDefinitionTypeImpl();
		return showMaterialDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowMaterialInformationType createShowMaterialInformationType() {
		ShowMaterialInformationTypeImpl showMaterialInformationType = new ShowMaterialInformationTypeImpl();
		return showMaterialInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowMaterialLotType createShowMaterialLotType() {
		ShowMaterialLotTypeImpl showMaterialLotType = new ShowMaterialLotTypeImpl();
		return showMaterialLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowMaterialSubLotType createShowMaterialSubLotType() {
		ShowMaterialSubLotTypeImpl showMaterialSubLotType = new ShowMaterialSubLotTypeImpl();
		return showMaterialSubLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowMaterialTestSpecType createShowMaterialTestSpecType() {
		ShowMaterialTestSpecTypeImpl showMaterialTestSpecType = new ShowMaterialTestSpecTypeImpl();
		return showMaterialTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowOperationsCapabilityInformationType createShowOperationsCapabilityInformationType() {
		ShowOperationsCapabilityInformationTypeImpl showOperationsCapabilityInformationType = new ShowOperationsCapabilityInformationTypeImpl();
		return showOperationsCapabilityInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowOperationsCapabilityType createShowOperationsCapabilityType() {
		ShowOperationsCapabilityTypeImpl showOperationsCapabilityType = new ShowOperationsCapabilityTypeImpl();
		return showOperationsCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowOperationsDefinitionInformationType createShowOperationsDefinitionInformationType() {
		ShowOperationsDefinitionInformationTypeImpl showOperationsDefinitionInformationType = new ShowOperationsDefinitionInformationTypeImpl();
		return showOperationsDefinitionInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowOperationsDefinitionType createShowOperationsDefinitionType() {
		ShowOperationsDefinitionTypeImpl showOperationsDefinitionType = new ShowOperationsDefinitionTypeImpl();
		return showOperationsDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowOperationsPerformanceType createShowOperationsPerformanceType() {
		ShowOperationsPerformanceTypeImpl showOperationsPerformanceType = new ShowOperationsPerformanceTypeImpl();
		return showOperationsPerformanceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowOperationsScheduleType createShowOperationsScheduleType() {
		ShowOperationsScheduleTypeImpl showOperationsScheduleType = new ShowOperationsScheduleTypeImpl();
		return showOperationsScheduleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPersonnelClassType createShowPersonnelClassType() {
		ShowPersonnelClassTypeImpl showPersonnelClassType = new ShowPersonnelClassTypeImpl();
		return showPersonnelClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPersonnelInformationType createShowPersonnelInformationType() {
		ShowPersonnelInformationTypeImpl showPersonnelInformationType = new ShowPersonnelInformationTypeImpl();
		return showPersonnelInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPersonType createShowPersonType() {
		ShowPersonTypeImpl showPersonType = new ShowPersonTypeImpl();
		return showPersonType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPhysicalAssetCapabilityTestSpecType createShowPhysicalAssetCapabilityTestSpecType() {
		ShowPhysicalAssetCapabilityTestSpecTypeImpl showPhysicalAssetCapabilityTestSpecType = new ShowPhysicalAssetCapabilityTestSpecTypeImpl();
		return showPhysicalAssetCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPhysicalAssetClassType createShowPhysicalAssetClassType() {
		ShowPhysicalAssetClassTypeImpl showPhysicalAssetClassType = new ShowPhysicalAssetClassTypeImpl();
		return showPhysicalAssetClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPhysicalAssetInformationType createShowPhysicalAssetInformationType() {
		ShowPhysicalAssetInformationTypeImpl showPhysicalAssetInformationType = new ShowPhysicalAssetInformationTypeImpl();
		return showPhysicalAssetInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPhysicalAssetType createShowPhysicalAssetType() {
		ShowPhysicalAssetTypeImpl showPhysicalAssetType = new ShowPhysicalAssetTypeImpl();
		return showPhysicalAssetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowProcessSegmentInformationType createShowProcessSegmentInformationType() {
		ShowProcessSegmentInformationTypeImpl showProcessSegmentInformationType = new ShowProcessSegmentInformationTypeImpl();
		return showProcessSegmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowProcessSegmentType createShowProcessSegmentType() {
		ShowProcessSegmentTypeImpl showProcessSegmentType = new ShowProcessSegmentTypeImpl();
		return showProcessSegmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowQualificationTestSpecificationType createShowQualificationTestSpecificationType() {
		ShowQualificationTestSpecificationTypeImpl showQualificationTestSpecificationType = new ShowQualificationTestSpecificationTypeImpl();
		return showQualificationTestSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StartTimeType createStartTimeType() {
		StartTimeTypeImpl startTimeType = new StartTimeTypeImpl();
		return startTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatusTimeType createStatusTimeType() {
		StatusTimeTypeImpl statusTimeType = new StatusTimeTypeImpl();
		return statusTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatusType createStatusType() {
		StatusTypeImpl statusType = new StatusTypeImpl();
		return statusType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageHierarchyScopeType createStorageHierarchyScopeType() {
		StorageHierarchyScopeTypeImpl storageHierarchyScopeType = new StorageHierarchyScopeTypeImpl();
		return storageHierarchyScopeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageLocationType createStorageLocationType() {
		StorageLocationTypeImpl storageLocationType = new StorageLocationTypeImpl();
		return storageLocationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncEquipmentCapabilityTestSpecType createSyncEquipmentCapabilityTestSpecType() {
		SyncEquipmentCapabilityTestSpecTypeImpl syncEquipmentCapabilityTestSpecType = new SyncEquipmentCapabilityTestSpecTypeImpl();
		return syncEquipmentCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncEquipmentClassType createSyncEquipmentClassType() {
		SyncEquipmentClassTypeImpl syncEquipmentClassType = new SyncEquipmentClassTypeImpl();
		return syncEquipmentClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncEquipmentInformationType createSyncEquipmentInformationType() {
		SyncEquipmentInformationTypeImpl syncEquipmentInformationType = new SyncEquipmentInformationTypeImpl();
		return syncEquipmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncEquipmentType createSyncEquipmentType() {
		SyncEquipmentTypeImpl syncEquipmentType = new SyncEquipmentTypeImpl();
		return syncEquipmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncMaterialClassType createSyncMaterialClassType() {
		SyncMaterialClassTypeImpl syncMaterialClassType = new SyncMaterialClassTypeImpl();
		return syncMaterialClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncMaterialDefinitionType createSyncMaterialDefinitionType() {
		SyncMaterialDefinitionTypeImpl syncMaterialDefinitionType = new SyncMaterialDefinitionTypeImpl();
		return syncMaterialDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncMaterialInformationType createSyncMaterialInformationType() {
		SyncMaterialInformationTypeImpl syncMaterialInformationType = new SyncMaterialInformationTypeImpl();
		return syncMaterialInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncMaterialLotType createSyncMaterialLotType() {
		SyncMaterialLotTypeImpl syncMaterialLotType = new SyncMaterialLotTypeImpl();
		return syncMaterialLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncMaterialSubLotType createSyncMaterialSubLotType() {
		SyncMaterialSubLotTypeImpl syncMaterialSubLotType = new SyncMaterialSubLotTypeImpl();
		return syncMaterialSubLotType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncMaterialTestSpecType createSyncMaterialTestSpecType() {
		SyncMaterialTestSpecTypeImpl syncMaterialTestSpecType = new SyncMaterialTestSpecTypeImpl();
		return syncMaterialTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncOperationsCapabilityInformationType createSyncOperationsCapabilityInformationType() {
		SyncOperationsCapabilityInformationTypeImpl syncOperationsCapabilityInformationType = new SyncOperationsCapabilityInformationTypeImpl();
		return syncOperationsCapabilityInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncOperationsCapabilityType createSyncOperationsCapabilityType() {
		SyncOperationsCapabilityTypeImpl syncOperationsCapabilityType = new SyncOperationsCapabilityTypeImpl();
		return syncOperationsCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncOperationsDefinitionInformationType createSyncOperationsDefinitionInformationType() {
		SyncOperationsDefinitionInformationTypeImpl syncOperationsDefinitionInformationType = new SyncOperationsDefinitionInformationTypeImpl();
		return syncOperationsDefinitionInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncOperationsDefinitionType createSyncOperationsDefinitionType() {
		SyncOperationsDefinitionTypeImpl syncOperationsDefinitionType = new SyncOperationsDefinitionTypeImpl();
		return syncOperationsDefinitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncOperationsPerformanceType createSyncOperationsPerformanceType() {
		SyncOperationsPerformanceTypeImpl syncOperationsPerformanceType = new SyncOperationsPerformanceTypeImpl();
		return syncOperationsPerformanceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncOperationsScheduleType createSyncOperationsScheduleType() {
		SyncOperationsScheduleTypeImpl syncOperationsScheduleType = new SyncOperationsScheduleTypeImpl();
		return syncOperationsScheduleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPersonnelClassType createSyncPersonnelClassType() {
		SyncPersonnelClassTypeImpl syncPersonnelClassType = new SyncPersonnelClassTypeImpl();
		return syncPersonnelClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPersonnelInformationType createSyncPersonnelInformationType() {
		SyncPersonnelInformationTypeImpl syncPersonnelInformationType = new SyncPersonnelInformationTypeImpl();
		return syncPersonnelInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPersonType createSyncPersonType() {
		SyncPersonTypeImpl syncPersonType = new SyncPersonTypeImpl();
		return syncPersonType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPhysicalAssetCapabilityTestSpecType createSyncPhysicalAssetCapabilityTestSpecType() {
		SyncPhysicalAssetCapabilityTestSpecTypeImpl syncPhysicalAssetCapabilityTestSpecType = new SyncPhysicalAssetCapabilityTestSpecTypeImpl();
		return syncPhysicalAssetCapabilityTestSpecType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPhysicalAssetClassType createSyncPhysicalAssetClassType() {
		SyncPhysicalAssetClassTypeImpl syncPhysicalAssetClassType = new SyncPhysicalAssetClassTypeImpl();
		return syncPhysicalAssetClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPhysicalAssetInformationType createSyncPhysicalAssetInformationType() {
		SyncPhysicalAssetInformationTypeImpl syncPhysicalAssetInformationType = new SyncPhysicalAssetInformationTypeImpl();
		return syncPhysicalAssetInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPhysicalAssetType createSyncPhysicalAssetType() {
		SyncPhysicalAssetTypeImpl syncPhysicalAssetType = new SyncPhysicalAssetTypeImpl();
		return syncPhysicalAssetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncProcessSegmentInformationType createSyncProcessSegmentInformationType() {
		SyncProcessSegmentInformationTypeImpl syncProcessSegmentInformationType = new SyncProcessSegmentInformationTypeImpl();
		return syncProcessSegmentInformationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncProcessSegmentType createSyncProcessSegmentType() {
		SyncProcessSegmentTypeImpl syncProcessSegmentType = new SyncProcessSegmentTypeImpl();
		return syncProcessSegmentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncQualificationTestSpecificationType createSyncQualificationTestSpecificationType() {
		SyncQualificationTestSpecificationTypeImpl syncQualificationTestSpecificationType = new SyncQualificationTestSpecificationTypeImpl();
		return syncQualificationTestSpecificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestDateTimeType createTestDateTimeType() {
		TestDateTimeTypeImpl testDateTimeType = new TestDateTimeTypeImpl();
		return testDateTimeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestedEquipmentClassPropertyType createTestedEquipmentClassPropertyType() {
		TestedEquipmentClassPropertyTypeImpl testedEquipmentClassPropertyType = new TestedEquipmentClassPropertyTypeImpl();
		return testedEquipmentClassPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestedEquipmentPropertyType createTestedEquipmentPropertyType() {
		TestedEquipmentPropertyTypeImpl testedEquipmentPropertyType = new TestedEquipmentPropertyTypeImpl();
		return testedEquipmentPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestedMaterialClassPropertyType createTestedMaterialClassPropertyType() {
		TestedMaterialClassPropertyTypeImpl testedMaterialClassPropertyType = new TestedMaterialClassPropertyTypeImpl();
		return testedMaterialClassPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestedMaterialDefinitionPropertyType createTestedMaterialDefinitionPropertyType() {
		TestedMaterialDefinitionPropertyTypeImpl testedMaterialDefinitionPropertyType = new TestedMaterialDefinitionPropertyTypeImpl();
		return testedMaterialDefinitionPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestedMaterialLotPropertyType createTestedMaterialLotPropertyType() {
		TestedMaterialLotPropertyTypeImpl testedMaterialLotPropertyType = new TestedMaterialLotPropertyTypeImpl();
		return testedMaterialLotPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestedPersonnelClassPropertyType createTestedPersonnelClassPropertyType() {
		TestedPersonnelClassPropertyTypeImpl testedPersonnelClassPropertyType = new TestedPersonnelClassPropertyTypeImpl();
		return testedPersonnelClassPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestedPersonPropertyType createTestedPersonPropertyType() {
		TestedPersonPropertyTypeImpl testedPersonPropertyType = new TestedPersonPropertyTypeImpl();
		return testedPersonPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestedPhysicalAssetClassPropertyType createTestedPhysicalAssetClassPropertyType() {
		TestedPhysicalAssetClassPropertyTypeImpl testedPhysicalAssetClassPropertyType = new TestedPhysicalAssetClassPropertyTypeImpl();
		return testedPhysicalAssetClassPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestedPhysicalAssetPropertyType createTestedPhysicalAssetPropertyType() {
		TestedPhysicalAssetPropertyTypeImpl testedPhysicalAssetPropertyType = new TestedPhysicalAssetPropertyTypeImpl();
		return testedPhysicalAssetPropertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestResultType createTestResultType() {
		TestResultTypeImpl testResultType = new TestResultTypeImpl();
		return testResultType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TextType createTextType() {
		TextTypeImpl textType = new TextTypeImpl();
		return textType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransAcknowledgeType createTransAcknowledgeType() {
		TransAcknowledgeTypeImpl transAcknowledgeType = new TransAcknowledgeTypeImpl();
		return transAcknowledgeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransActionCriteriaType createTransActionCriteriaType() {
		TransActionCriteriaTypeImpl transActionCriteriaType = new TransActionCriteriaTypeImpl();
		return transActionCriteriaType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransApplicationAreaType createTransApplicationAreaType() {
		TransApplicationAreaTypeImpl transApplicationAreaType = new TransApplicationAreaTypeImpl();
		return transApplicationAreaType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransCancelType createTransCancelType() {
		TransCancelTypeImpl transCancelType = new TransCancelTypeImpl();
		return transCancelType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransChangeStatusType createTransChangeStatusType() {
		TransChangeStatusTypeImpl transChangeStatusType = new TransChangeStatusTypeImpl();
		return transChangeStatusType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransChangeType createTransChangeType() {
		TransChangeTypeImpl transChangeType = new TransChangeTypeImpl();
		return transChangeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransConfirmationCodeType createTransConfirmationCodeType() {
		TransConfirmationCodeTypeImpl transConfirmationCodeType = new TransConfirmationCodeTypeImpl();
		return transConfirmationCodeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransConfirmType createTransConfirmType() {
		TransConfirmTypeImpl transConfirmType = new TransConfirmTypeImpl();
		return transConfirmType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransExpression1Type createTransExpression1Type() {
		TransExpression1TypeImpl transExpression1Type = new TransExpression1TypeImpl();
		return transExpression1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransExpressionType createTransExpressionType() {
		TransExpressionTypeImpl transExpressionType = new TransExpressionTypeImpl();
		return transExpressionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransGetType createTransGetType() {
		TransGetTypeImpl transGetType = new TransGetTypeImpl();
		return transGetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransProcessType createTransProcessType() {
		TransProcessTypeImpl transProcessType = new TransProcessTypeImpl();
		return transProcessType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransReceiverType createTransReceiverType() {
		TransReceiverTypeImpl transReceiverType = new TransReceiverTypeImpl();
		return transReceiverType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransRespondType createTransRespondType() {
		TransRespondTypeImpl transRespondType = new TransRespondTypeImpl();
		return transRespondType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransResponseCriteriaType createTransResponseCriteriaType() {
		TransResponseCriteriaTypeImpl transResponseCriteriaType = new TransResponseCriteriaTypeImpl();
		return transResponseCriteriaType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransSenderType createTransSenderType() {
		TransSenderTypeImpl transSenderType = new TransSenderTypeImpl();
		return transSenderType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransShowType createTransShowType() {
		TransShowTypeImpl transShowType = new TransShowTypeImpl();
		return transShowType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransSignatureType createTransSignatureType() {
		TransSignatureTypeImpl transSignatureType = new TransSignatureTypeImpl();
		return transSignatureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransStateChangeType createTransStateChangeType() {
		TransStateChangeTypeImpl transStateChangeType = new TransStateChangeTypeImpl();
		return transStateChangeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransSyncType createTransSyncType() {
		TransSyncTypeImpl transSyncType = new TransSyncTypeImpl();
		return transSyncType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransUserAreaType createTransUserAreaType() {
		TransUserAreaTypeImpl transUserAreaType = new TransUserAreaTypeImpl();
		return transUserAreaType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitOfMeasureType createUnitOfMeasureType() {
		UnitOfMeasureTypeImpl unitOfMeasureType = new UnitOfMeasureTypeImpl();
		return unitOfMeasureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueStringType createValueStringType() {
		ValueStringTypeImpl valueStringType = new ValueStringTypeImpl();
		return valueStringType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueType createValueType() {
		ValueTypeImpl valueType = new ValueTypeImpl();
		return valueType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VersionType createVersionType() {
		VersionTypeImpl versionType = new VersionTypeImpl();
		return versionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkRequestIDType createWorkRequestIDType() {
		WorkRequestIDTypeImpl workRequestIDType = new WorkRequestIDTypeImpl();
		return workRequestIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkScheduleIDType createWorkScheduleIDType() {
		WorkScheduleIDTypeImpl workScheduleIDType = new WorkScheduleIDTypeImpl();
		return workScheduleIDType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkType1Type createWorkType1Type() {
		WorkType1TypeImpl workType1Type = new WorkType1TypeImpl();
		return workType1Type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkTypeType createWorkTypeType() {
		WorkTypeTypeImpl workTypeType = new WorkTypeTypeImpl();
		return workTypeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationship1TypeBase createAssemblyRelationship1TypeBaseFromString(EDataType eDataType, String initialValue) {
		AssemblyRelationship1TypeBase result = AssemblyRelationship1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssemblyRelationship1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyType1TypeBase createAssemblyType1TypeBaseFromString(EDataType eDataType, String initialValue) {
		AssemblyType1TypeBase result = AssemblyType1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssemblyType1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityType1TypeBase createCapabilityType1TypeBaseFromString(EDataType eDataType, String initialValue) {
		CapabilityType1TypeBase result = CapabilityType1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCapabilityType1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType1TypeBase createDataType1TypeBaseFromString(EDataType eDataType, String initialValue) {
		DataType1TypeBase result = DataType1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDataType1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dependency1TypeBase createDependency1TypeBaseFromString(EDataType eDataType, String initialValue) {
		Dependency1TypeBase result = Dependency1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDependency1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentElementLevel1TypeBase createEquipmentElementLevel1TypeBaseFromString(EDataType eDataType, String initialValue) {
		EquipmentElementLevel1TypeBase result = EquipmentElementLevel1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEquipmentElementLevel1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JobOrderCommand1TypeBase createJobOrderCommand1TypeBaseFromString(EDataType eDataType, String initialValue) {
		JobOrderCommand1TypeBase result = JobOrderCommand1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertJobOrderCommand1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialUse1TypeBase createMaterialUse1TypeBaseFromString(EDataType eDataType, String initialValue) {
		MaterialUse1TypeBase result = MaterialUse1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMaterialUse1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsType1TypeBase createOperationsType1TypeBaseFromString(EDataType eDataType, String initialValue) {
		OperationsType1TypeBase result = OperationsType1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperationsType1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipForm1TypeBase createRelationshipForm1TypeBaseFromString(EDataType eDataType, String initialValue) {
		RelationshipForm1TypeBase result = RelationshipForm1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRelationshipForm1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipType1TypeBase createRelationshipType1TypeBaseFromString(EDataType eDataType, String initialValue) {
		RelationshipType1TypeBase result = RelationshipType1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRelationshipType1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequestState1TypeBase createRequestState1TypeBaseFromString(EDataType eDataType, String initialValue) {
		RequestState1TypeBase result = RequestState1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRequestState1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredByRequestedSegmentResponse1TypeBase createRequiredByRequestedSegmentResponse1TypeBaseFromString(EDataType eDataType, String initialValue) {
		RequiredByRequestedSegmentResponse1TypeBase result = RequiredByRequestedSegmentResponse1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRequiredByRequestedSegmentResponse1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceReferenceType1TypeBase createResourceReferenceType1TypeBaseFromString(EDataType eDataType, String initialValue) {
		ResourceReferenceType1TypeBase result = ResourceReferenceType1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertResourceReferenceType1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseState1TypeBase createResponseState1TypeBaseFromString(EDataType eDataType, String initialValue) {
		ResponseState1TypeBase result = ResponseState1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertResponseState1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransActionCodeEnumerationType createTransActionCodeEnumerationTypeFromString(EDataType eDataType, String initialValue) {
		TransActionCodeEnumerationType result = TransActionCodeEnumerationType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTransActionCodeEnumerationTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransResponseCodeType createTransResponseCodeTypeFromString(EDataType eDataType, String initialValue) {
		TransResponseCodeType result = TransResponseCodeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTransResponseCodeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkType1TypeBase createWorkType1TypeBaseFromString(EDataType eDataType, String initialValue) {
		WorkType1TypeBase result = WorkType1TypeBase.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertWorkType1TypeBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationship1TypeBase createAssemblyRelationship1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createAssemblyRelationship1TypeBaseFromString(B2MMLPackage.eINSTANCE.getAssemblyRelationship1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssemblyRelationship1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertAssemblyRelationship1TypeBaseToString(B2MMLPackage.eINSTANCE.getAssemblyRelationship1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyType1TypeBase createAssemblyType1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createAssemblyType1TypeBaseFromString(B2MMLPackage.eINSTANCE.getAssemblyType1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssemblyType1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertAssemblyType1TypeBaseToString(B2MMLPackage.eINSTANCE.getAssemblyType1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityType1TypeBase createCapabilityType1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createCapabilityType1TypeBaseFromString(B2MMLPackage.eINSTANCE.getCapabilityType1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCapabilityType1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertCapabilityType1TypeBaseToString(B2MMLPackage.eINSTANCE.getCapabilityType1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType1TypeBase createDataType1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createDataType1TypeBaseFromString(B2MMLPackage.eINSTANCE.getDataType1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDataType1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertDataType1TypeBaseToString(B2MMLPackage.eINSTANCE.getDataType1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dependency1TypeBase createDependency1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createDependency1TypeBaseFromString(B2MMLPackage.eINSTANCE.getDependency1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDependency1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertDependency1TypeBaseToString(B2MMLPackage.eINSTANCE.getDependency1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Duration createDurationTypeFromString(EDataType eDataType, String initialValue) {
		return (Duration)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.DURATION, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDurationTypeToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.DURATION, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentElementLevel1TypeBase createEquipmentElementLevel1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createEquipmentElementLevel1TypeBaseFromString(B2MMLPackage.eINSTANCE.getEquipmentElementLevel1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEquipmentElementLevel1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertEquipmentElementLevel1TypeBaseToString(B2MMLPackage.eINSTANCE.getEquipmentElementLevel1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean createIndicatorTypeFromString(EDataType eDataType, String initialValue) {
		return (Boolean)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.BOOLEAN, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIndicatorTypeToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.BOOLEAN, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean createIndicatorTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createIndicatorTypeFromString(B2MMLPackage.eINSTANCE.getIndicatorType(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIndicatorTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertIndicatorTypeToString(B2MMLPackage.eINSTANCE.getIndicatorType(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JobOrderCommand1TypeBase createJobOrderCommand1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createJobOrderCommand1TypeBaseFromString(B2MMLPackage.eINSTANCE.getJobOrderCommand1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertJobOrderCommand1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertJobOrderCommand1TypeBaseToString(B2MMLPackage.eINSTANCE.getJobOrderCommand1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialUse1TypeBase createMaterialUse1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createMaterialUse1TypeBaseFromString(B2MMLPackage.eINSTANCE.getMaterialUse1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMaterialUse1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertMaterialUse1TypeBaseToString(B2MMLPackage.eINSTANCE.getMaterialUse1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsType1TypeBase createOperationsType1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createOperationsType1TypeBaseFromString(B2MMLPackage.eINSTANCE.getOperationsType1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperationsType1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertOperationsType1TypeBaseToString(B2MMLPackage.eINSTANCE.getOperationsType1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipForm1TypeBase createRelationshipForm1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createRelationshipForm1TypeBaseFromString(B2MMLPackage.eINSTANCE.getRelationshipForm1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRelationshipForm1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertRelationshipForm1TypeBaseToString(B2MMLPackage.eINSTANCE.getRelationshipForm1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipType1TypeBase createRelationshipType1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createRelationshipType1TypeBaseFromString(B2MMLPackage.eINSTANCE.getRelationshipType1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRelationshipType1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertRelationshipType1TypeBaseToString(B2MMLPackage.eINSTANCE.getRelationshipType1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequestState1TypeBase createRequestState1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createRequestState1TypeBaseFromString(B2MMLPackage.eINSTANCE.getRequestState1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRequestState1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertRequestState1TypeBaseToString(B2MMLPackage.eINSTANCE.getRequestState1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredByRequestedSegmentResponse1TypeBase createRequiredByRequestedSegmentResponse1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createRequiredByRequestedSegmentResponse1TypeBaseFromString(B2MMLPackage.eINSTANCE.getRequiredByRequestedSegmentResponse1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRequiredByRequestedSegmentResponse1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertRequiredByRequestedSegmentResponse1TypeBaseToString(B2MMLPackage.eINSTANCE.getRequiredByRequestedSegmentResponse1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceReferenceType1TypeBase createResourceReferenceType1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createResourceReferenceType1TypeBaseFromString(B2MMLPackage.eINSTANCE.getResourceReferenceType1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertResourceReferenceType1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertResourceReferenceType1TypeBaseToString(B2MMLPackage.eINSTANCE.getResourceReferenceType1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseState1TypeBase createResponseState1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createResponseState1TypeBaseFromString(B2MMLPackage.eINSTANCE.getResponseState1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertResponseState1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertResponseState1TypeBaseToString(B2MMLPackage.eINSTANCE.getResponseState1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransActionCodeEnumerationType createTransActionCodeEnumerationTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createTransActionCodeEnumerationTypeFromString(B2MMLPackage.eINSTANCE.getTransActionCodeEnumerationType(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTransActionCodeEnumerationTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertTransActionCodeEnumerationTypeToString(B2MMLPackage.eINSTANCE.getTransActionCodeEnumerationType(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object createTransActionCodeTypeFromString(EDataType eDataType, String initialValue) {
		if (initialValue == null) return null;
		Object result = null;
		RuntimeException exception = null;
		try {
			result = createTransActionCodeEnumerationTypeFromString(B2MMLPackage.eINSTANCE.getTransActionCodeEnumerationType(), initialValue);
			if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
				return result;
			}
		}
		catch (RuntimeException e) {
			exception = e;
		}
		try {
			result = XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NORMALIZED_STRING, initialValue);
			if (result != null && Diagnostician.INSTANCE.validate(eDataType, result, null, null)) {
				return result;
			}
		}
		catch (RuntimeException e) {
			exception = e;
		}
		if (result != null || exception == null) return result;
    
		throw exception;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTransActionCodeTypeToString(EDataType eDataType, Object instanceValue) {
		if (instanceValue == null) return null;
		if (B2MMLPackage.eINSTANCE.getTransActionCodeEnumerationType().isInstance(instanceValue)) {
			try {
				String value = convertTransActionCodeEnumerationTypeToString(B2MMLPackage.eINSTANCE.getTransActionCodeEnumerationType(), instanceValue);
				if (value != null) return value;
			}
			catch (Exception e) {
				// Keep trying other member types until all have failed.
			}
		}
		if (XMLTypePackage.Literals.NORMALIZED_STRING.isInstance(instanceValue)) {
			try {
				String value = XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NORMALIZED_STRING, instanceValue);
				if (value != null) return value;
			}
			catch (Exception e) {
				// Keep trying other member types until all have failed.
			}
		}
		throw new IllegalArgumentException("Invalid value: '"+instanceValue+"' for datatype :"+eDataType.getName());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransResponseCodeType createTransResponseCodeTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createTransResponseCodeTypeFromString(B2MMLPackage.eINSTANCE.getTransResponseCodeType(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTransResponseCodeTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertTransResponseCodeTypeToString(B2MMLPackage.eINSTANCE.getTransResponseCodeType(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkType1TypeBase createWorkType1TypeBaseObjectFromString(EDataType eDataType, String initialValue) {
		return createWorkType1TypeBaseFromString(B2MMLPackage.eINSTANCE.getWorkType1TypeBase(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertWorkType1TypeBaseObjectToString(EDataType eDataType, Object instanceValue) {
		return convertWorkType1TypeBaseToString(B2MMLPackage.eINSTANCE.getWorkType1TypeBase(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public B2MMLPackage getB2MMLPackage() {
		return (B2MMLPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static B2MMLPackage getPackage() {
		return B2MMLPackage.eINSTANCE;
	}

} //B2MMLFactoryImpl
