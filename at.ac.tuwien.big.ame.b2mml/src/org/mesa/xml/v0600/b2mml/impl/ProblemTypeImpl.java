/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.ProblemType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Problem Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ProblemTypeImpl extends CodeTypeImpl implements ProblemType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProblemTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getProblemType();
	}

} //ProblemTypeImpl
