/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataTypeType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.ResultType;
import org.mesa.xml.v0600.b2mml.UnitOfMeasureType;
import org.mesa.xml.v0600.b2mml.ValueStringType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Result Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.ResultTypeImpl#getValueString <em>Value String</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.ResultTypeImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.ResultTypeImpl#getUnitOfMeasure <em>Unit Of Measure</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.ResultTypeImpl#getKey <em>Key</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResultTypeImpl extends MinimalEObjectImpl.Container implements ResultType {
	/**
	 * The cached value of the '{@link #getValueString() <em>Value String</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueString()
	 * @generated
	 * @ordered
	 */
	protected ValueStringType valueString;

	/**
	 * This is true if the Value String containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean valueStringESet;

	/**
	 * The cached value of the '{@link #getDataType() <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected DataTypeType dataType;

	/**
	 * This is true if the Data Type containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dataTypeESet;

	/**
	 * The cached value of the '{@link #getUnitOfMeasure() <em>Unit Of Measure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected UnitOfMeasureType unitOfMeasure;

	/**
	 * This is true if the Unit Of Measure containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean unitOfMeasureESet;

	/**
	 * The cached value of the '{@link #getKey() <em>Key</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKey()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType key;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResultTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getResultType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueStringType getValueString() {
		return valueString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValueString(ValueStringType newValueString, NotificationChain msgs) {
		ValueStringType oldValueString = valueString;
		valueString = newValueString;
		boolean oldValueStringESet = valueStringESet;
		valueStringESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESULT_TYPE__VALUE_STRING, oldValueString, newValueString, !oldValueStringESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueString(ValueStringType newValueString) {
		if (newValueString != valueString) {
			NotificationChain msgs = null;
			if (valueString != null)
				msgs = ((InternalEObject)valueString).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESULT_TYPE__VALUE_STRING, null, msgs);
			if (newValueString != null)
				msgs = ((InternalEObject)newValueString).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESULT_TYPE__VALUE_STRING, null, msgs);
			msgs = basicSetValueString(newValueString, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldValueStringESet = valueStringESet;
			valueStringESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESULT_TYPE__VALUE_STRING, newValueString, newValueString, !oldValueStringESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetValueString(NotificationChain msgs) {
		ValueStringType oldValueString = valueString;
		valueString = null;
		boolean oldValueStringESet = valueStringESet;
		valueStringESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.RESULT_TYPE__VALUE_STRING, oldValueString, null, oldValueStringESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetValueString() {
		if (valueString != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)valueString).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESULT_TYPE__VALUE_STRING, null, msgs);
			msgs = basicUnsetValueString(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldValueStringESet = valueStringESet;
			valueStringESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.RESULT_TYPE__VALUE_STRING, null, null, oldValueStringESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetValueString() {
		return valueStringESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataTypeType getDataType() {
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataType(DataTypeType newDataType, NotificationChain msgs) {
		DataTypeType oldDataType = dataType;
		dataType = newDataType;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESULT_TYPE__DATA_TYPE, oldDataType, newDataType, !oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataType(DataTypeType newDataType) {
		if (newDataType != dataType) {
			NotificationChain msgs = null;
			if (dataType != null)
				msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESULT_TYPE__DATA_TYPE, null, msgs);
			if (newDataType != null)
				msgs = ((InternalEObject)newDataType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESULT_TYPE__DATA_TYPE, null, msgs);
			msgs = basicSetDataType(newDataType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESULT_TYPE__DATA_TYPE, newDataType, newDataType, !oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetDataType(NotificationChain msgs) {
		DataTypeType oldDataType = dataType;
		dataType = null;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.RESULT_TYPE__DATA_TYPE, oldDataType, null, oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDataType() {
		if (dataType != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESULT_TYPE__DATA_TYPE, null, msgs);
			msgs = basicUnsetDataType(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.RESULT_TYPE__DATA_TYPE, null, null, oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDataType() {
		return dataTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitOfMeasureType getUnitOfMeasure() {
		return unitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnitOfMeasure(UnitOfMeasureType newUnitOfMeasure, NotificationChain msgs) {
		UnitOfMeasureType oldUnitOfMeasure = unitOfMeasure;
		unitOfMeasure = newUnitOfMeasure;
		boolean oldUnitOfMeasureESet = unitOfMeasureESet;
		unitOfMeasureESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESULT_TYPE__UNIT_OF_MEASURE, oldUnitOfMeasure, newUnitOfMeasure, !oldUnitOfMeasureESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitOfMeasure(UnitOfMeasureType newUnitOfMeasure) {
		if (newUnitOfMeasure != unitOfMeasure) {
			NotificationChain msgs = null;
			if (unitOfMeasure != null)
				msgs = ((InternalEObject)unitOfMeasure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESULT_TYPE__UNIT_OF_MEASURE, null, msgs);
			if (newUnitOfMeasure != null)
				msgs = ((InternalEObject)newUnitOfMeasure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESULT_TYPE__UNIT_OF_MEASURE, null, msgs);
			msgs = basicSetUnitOfMeasure(newUnitOfMeasure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldUnitOfMeasureESet = unitOfMeasureESet;
			unitOfMeasureESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESULT_TYPE__UNIT_OF_MEASURE, newUnitOfMeasure, newUnitOfMeasure, !oldUnitOfMeasureESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetUnitOfMeasure(NotificationChain msgs) {
		UnitOfMeasureType oldUnitOfMeasure = unitOfMeasure;
		unitOfMeasure = null;
		boolean oldUnitOfMeasureESet = unitOfMeasureESet;
		unitOfMeasureESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.RESULT_TYPE__UNIT_OF_MEASURE, oldUnitOfMeasure, null, oldUnitOfMeasureESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUnitOfMeasure() {
		if (unitOfMeasure != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)unitOfMeasure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESULT_TYPE__UNIT_OF_MEASURE, null, msgs);
			msgs = basicUnsetUnitOfMeasure(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldUnitOfMeasureESet = unitOfMeasureESet;
			unitOfMeasureESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.RESULT_TYPE__UNIT_OF_MEASURE, null, null, oldUnitOfMeasureESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUnitOfMeasure() {
		return unitOfMeasureESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getKey() {
		return key;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetKey(IdentifierType newKey, NotificationChain msgs) {
		IdentifierType oldKey = key;
		key = newKey;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESULT_TYPE__KEY, oldKey, newKey);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKey(IdentifierType newKey) {
		if (newKey != key) {
			NotificationChain msgs = null;
			if (key != null)
				msgs = ((InternalEObject)key).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESULT_TYPE__KEY, null, msgs);
			if (newKey != null)
				msgs = ((InternalEObject)newKey).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.RESULT_TYPE__KEY, null, msgs);
			msgs = basicSetKey(newKey, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.RESULT_TYPE__KEY, newKey, newKey));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.RESULT_TYPE__VALUE_STRING:
				return basicUnsetValueString(msgs);
			case B2MMLPackage.RESULT_TYPE__DATA_TYPE:
				return basicUnsetDataType(msgs);
			case B2MMLPackage.RESULT_TYPE__UNIT_OF_MEASURE:
				return basicUnsetUnitOfMeasure(msgs);
			case B2MMLPackage.RESULT_TYPE__KEY:
				return basicSetKey(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.RESULT_TYPE__VALUE_STRING:
				return getValueString();
			case B2MMLPackage.RESULT_TYPE__DATA_TYPE:
				return getDataType();
			case B2MMLPackage.RESULT_TYPE__UNIT_OF_MEASURE:
				return getUnitOfMeasure();
			case B2MMLPackage.RESULT_TYPE__KEY:
				return getKey();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.RESULT_TYPE__VALUE_STRING:
				setValueString((ValueStringType)newValue);
				return;
			case B2MMLPackage.RESULT_TYPE__DATA_TYPE:
				setDataType((DataTypeType)newValue);
				return;
			case B2MMLPackage.RESULT_TYPE__UNIT_OF_MEASURE:
				setUnitOfMeasure((UnitOfMeasureType)newValue);
				return;
			case B2MMLPackage.RESULT_TYPE__KEY:
				setKey((IdentifierType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.RESULT_TYPE__VALUE_STRING:
				unsetValueString();
				return;
			case B2MMLPackage.RESULT_TYPE__DATA_TYPE:
				unsetDataType();
				return;
			case B2MMLPackage.RESULT_TYPE__UNIT_OF_MEASURE:
				unsetUnitOfMeasure();
				return;
			case B2MMLPackage.RESULT_TYPE__KEY:
				setKey((IdentifierType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.RESULT_TYPE__VALUE_STRING:
				return isSetValueString();
			case B2MMLPackage.RESULT_TYPE__DATA_TYPE:
				return isSetDataType();
			case B2MMLPackage.RESULT_TYPE__UNIT_OF_MEASURE:
				return isSetUnitOfMeasure();
			case B2MMLPackage.RESULT_TYPE__KEY:
				return key != null;
		}
		return super.eIsSet(featureID);
	}

} //ResultTypeImpl
