/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.AssemblyRelationshipType;
import org.mesa.xml.v0600.b2mml.AssemblyTypeType;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.LocationType;
import org.mesa.xml.v0600.b2mml.MaterialLotIDType;
import org.mesa.xml.v0600.b2mml.MaterialLotPropertyType;
import org.mesa.xml.v0600.b2mml.MaterialLotType;
import org.mesa.xml.v0600.b2mml.MaterialSubLotType;
import org.mesa.xml.v0600.b2mml.QuantityValueType;
import org.mesa.xml.v0600.b2mml.StatusType;
import org.mesa.xml.v0600.b2mml.StorageHierarchyScopeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Sub Lot Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getMaterialSublotProperty <em>Material Sublot Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getStorageLocation <em>Storage Location</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getMaterialSubLot <em>Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getMaterialLotID <em>Material Lot ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getAssemblyLotID <em>Assembly Lot ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getAssemblySubLotID <em>Assembly Sub Lot ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.MaterialSubLotTypeImpl#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MaterialSubLotTypeImpl extends MinimalEObjectImpl.Container implements MaterialSubLotType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected LocationType location;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected StatusType status;

	/**
	 * The cached value of the '{@link #getMaterialSublotProperty() <em>Material Sublot Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSublotProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialLotPropertyType> materialSublotProperty;

	/**
	 * The cached value of the '{@link #getStorageLocation() <em>Storage Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageLocation()
	 * @generated
	 * @ordered
	 */
	protected StorageHierarchyScopeType storageLocation;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getMaterialSubLot() <em>Material Sub Lot</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSubLot()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSubLotType> materialSubLot;

	/**
	 * The cached value of the '{@link #getMaterialLotID() <em>Material Lot ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialLotID()
	 * @generated
	 * @ordered
	 */
	protected MaterialLotIDType materialLotID;

	/**
	 * The cached value of the '{@link #getAssemblyLotID() <em>Assembly Lot ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyLotID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialLotType> assemblyLotID;

	/**
	 * The cached value of the '{@link #getAssemblySubLotID() <em>Assembly Sub Lot ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblySubLotID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSubLotType> assemblySubLotID;

	/**
	 * The cached value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected AssemblyTypeType assemblyType;

	/**
	 * The cached value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected AssemblyRelationshipType assemblyRelationship;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialSubLotTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getMaterialSubLotType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationType getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocation(LocationType newLocation, NotificationChain msgs) {
		LocationType oldLocation = location;
		location = newLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__LOCATION, oldLocation, newLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(LocationType newLocation) {
		if (newLocation != location) {
			NotificationChain msgs = null;
			if (location != null)
				msgs = ((InternalEObject)location).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__LOCATION, null, msgs);
			if (newLocation != null)
				msgs = ((InternalEObject)newLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__LOCATION, null, msgs);
			msgs = basicSetLocation(newLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__LOCATION, newLocation, newLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatusType getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStatus(StatusType newStatus, NotificationChain msgs) {
		StatusType oldStatus = status;
		status = newStatus;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STATUS, oldStatus, newStatus);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(StatusType newStatus) {
		if (newStatus != status) {
			NotificationChain msgs = null;
			if (status != null)
				msgs = ((InternalEObject)status).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STATUS, null, msgs);
			if (newStatus != null)
				msgs = ((InternalEObject)newStatus).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STATUS, null, msgs);
			msgs = basicSetStatus(newStatus, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STATUS, newStatus, newStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialLotPropertyType> getMaterialSublotProperty() {
		if (materialSublotProperty == null) {
			materialSublotProperty = new EObjectContainmentEList<MaterialLotPropertyType>(MaterialLotPropertyType.class, this, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_SUBLOT_PROPERTY);
		}
		return materialSublotProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageHierarchyScopeType getStorageLocation() {
		return storageLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStorageLocation(StorageHierarchyScopeType newStorageLocation, NotificationChain msgs) {
		StorageHierarchyScopeType oldStorageLocation = storageLocation;
		storageLocation = newStorageLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STORAGE_LOCATION, oldStorageLocation, newStorageLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageLocation(StorageHierarchyScopeType newStorageLocation) {
		if (newStorageLocation != storageLocation) {
			NotificationChain msgs = null;
			if (storageLocation != null)
				msgs = ((InternalEObject)storageLocation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STORAGE_LOCATION, null, msgs);
			if (newStorageLocation != null)
				msgs = ((InternalEObject)newStorageLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STORAGE_LOCATION, null, msgs);
			msgs = basicSetStorageLocation(newStorageLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STORAGE_LOCATION, newStorageLocation, newStorageLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSubLotType> getMaterialSubLot() {
		if (materialSubLot == null) {
			materialSubLot = new EObjectContainmentEList<MaterialSubLotType>(MaterialSubLotType.class, this, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_SUB_LOT);
		}
		return materialSubLot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialLotIDType getMaterialLotID() {
		return materialLotID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialLotID(MaterialLotIDType newMaterialLotID, NotificationChain msgs) {
		MaterialLotIDType oldMaterialLotID = materialLotID;
		materialLotID = newMaterialLotID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_LOT_ID, oldMaterialLotID, newMaterialLotID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialLotID(MaterialLotIDType newMaterialLotID) {
		if (newMaterialLotID != materialLotID) {
			NotificationChain msgs = null;
			if (materialLotID != null)
				msgs = ((InternalEObject)materialLotID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_LOT_ID, null, msgs);
			if (newMaterialLotID != null)
				msgs = ((InternalEObject)newMaterialLotID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_LOT_ID, null, msgs);
			msgs = basicSetMaterialLotID(newMaterialLotID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_LOT_ID, newMaterialLotID, newMaterialLotID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialLotType> getAssemblyLotID() {
		if (assemblyLotID == null) {
			assemblyLotID = new EObjectContainmentEList<MaterialLotType>(MaterialLotType.class, this, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_LOT_ID);
		}
		return assemblyLotID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSubLotType> getAssemblySubLotID() {
		if (assemblySubLotID == null) {
			assemblySubLotID = new EObjectContainmentEList<MaterialSubLotType>(MaterialSubLotType.class, this, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_SUB_LOT_ID);
		}
		return assemblySubLotID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyTypeType getAssemblyType() {
		return assemblyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyType(AssemblyTypeType newAssemblyType, NotificationChain msgs) {
		AssemblyTypeType oldAssemblyType = assemblyType;
		assemblyType = newAssemblyType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_TYPE, oldAssemblyType, newAssemblyType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyType(AssemblyTypeType newAssemblyType) {
		if (newAssemblyType != assemblyType) {
			NotificationChain msgs = null;
			if (assemblyType != null)
				msgs = ((InternalEObject)assemblyType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_TYPE, null, msgs);
			if (newAssemblyType != null)
				msgs = ((InternalEObject)newAssemblyType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_TYPE, null, msgs);
			msgs = basicSetAssemblyType(newAssemblyType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_TYPE, newAssemblyType, newAssemblyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationshipType getAssemblyRelationship() {
		return assemblyRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship, NotificationChain msgs) {
		AssemblyRelationshipType oldAssemblyRelationship = assemblyRelationship;
		assemblyRelationship = newAssemblyRelationship;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_RELATIONSHIP, oldAssemblyRelationship, newAssemblyRelationship);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship) {
		if (newAssemblyRelationship != assemblyRelationship) {
			NotificationChain msgs = null;
			if (assemblyRelationship != null)
				msgs = ((InternalEObject)assemblyRelationship).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			if (newAssemblyRelationship != null)
				msgs = ((InternalEObject)newAssemblyRelationship).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			msgs = basicSetAssemblyRelationship(newAssemblyRelationship, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_RELATIONSHIP, newAssemblyRelationship, newAssemblyRelationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__LOCATION:
				return basicSetLocation(null, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STATUS:
				return basicSetStatus(null, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_SUBLOT_PROPERTY:
				return ((InternalEList<?>)getMaterialSublotProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STORAGE_LOCATION:
				return basicSetStorageLocation(null, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_SUB_LOT:
				return ((InternalEList<?>)getMaterialSubLot()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_LOT_ID:
				return basicSetMaterialLotID(null, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_LOT_ID:
				return ((InternalEList<?>)getAssemblyLotID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_SUB_LOT_ID:
				return ((InternalEList<?>)getAssemblySubLotID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_TYPE:
				return basicSetAssemblyType(null, msgs);
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_RELATIONSHIP:
				return basicSetAssemblyRelationship(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ID:
				return getID();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__LOCATION:
				return getLocation();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STATUS:
				return getStatus();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_SUBLOT_PROPERTY:
				return getMaterialSublotProperty();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STORAGE_LOCATION:
				return getStorageLocation();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_SUB_LOT:
				return getMaterialSubLot();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_LOT_ID:
				return getMaterialLotID();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_LOT_ID:
				return getAssemblyLotID();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_SUB_LOT_ID:
				return getAssemblySubLotID();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_TYPE:
				return getAssemblyType();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_RELATIONSHIP:
				return getAssemblyRelationship();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__LOCATION:
				setLocation((LocationType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STATUS:
				setStatus((StatusType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_SUBLOT_PROPERTY:
				getMaterialSublotProperty().clear();
				getMaterialSublotProperty().addAll((Collection<? extends MaterialLotPropertyType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STORAGE_LOCATION:
				setStorageLocation((StorageHierarchyScopeType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_SUB_LOT:
				getMaterialSubLot().clear();
				getMaterialSubLot().addAll((Collection<? extends MaterialSubLotType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_LOT_ID:
				setMaterialLotID((MaterialLotIDType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_LOT_ID:
				getAssemblyLotID().clear();
				getAssemblyLotID().addAll((Collection<? extends MaterialLotType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_SUB_LOT_ID:
				getAssemblySubLotID().clear();
				getAssemblySubLotID().addAll((Collection<? extends MaterialSubLotType>)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)newValue);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__LOCATION:
				setLocation((LocationType)null);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STATUS:
				setStatus((StatusType)null);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_SUBLOT_PROPERTY:
				getMaterialSublotProperty().clear();
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STORAGE_LOCATION:
				setStorageLocation((StorageHierarchyScopeType)null);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_SUB_LOT:
				getMaterialSubLot().clear();
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_LOT_ID:
				setMaterialLotID((MaterialLotIDType)null);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_LOT_ID:
				getAssemblyLotID().clear();
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_SUB_LOT_ID:
				getAssemblySubLotID().clear();
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)null);
				return;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ID:
				return iD != null;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__LOCATION:
				return location != null;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STATUS:
				return status != null;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_SUBLOT_PROPERTY:
				return materialSublotProperty != null && !materialSublotProperty.isEmpty();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__STORAGE_LOCATION:
				return storageLocation != null;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_SUB_LOT:
				return materialSubLot != null && !materialSubLot.isEmpty();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__MATERIAL_LOT_ID:
				return materialLotID != null;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_LOT_ID:
				return assemblyLotID != null && !assemblyLotID.isEmpty();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_SUB_LOT_ID:
				return assemblySubLotID != null && !assemblySubLotID.isEmpty();
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_TYPE:
				return assemblyType != null;
			case B2MMLPackage.MATERIAL_SUB_LOT_TYPE__ASSEMBLY_RELATIONSHIP:
				return assemblyRelationship != null;
		}
		return super.eIsSet(featureID);
	}

} //MaterialSubLotTypeImpl
