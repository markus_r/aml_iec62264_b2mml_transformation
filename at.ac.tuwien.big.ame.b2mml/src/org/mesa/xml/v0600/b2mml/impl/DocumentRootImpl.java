/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.AcknowledgeEquipmentCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.AcknowledgeEquipmentClassType;
import org.mesa.xml.v0600.b2mml.AcknowledgeEquipmentInformationType;
import org.mesa.xml.v0600.b2mml.AcknowledgeEquipmentType;
import org.mesa.xml.v0600.b2mml.AcknowledgeMaterialClassType;
import org.mesa.xml.v0600.b2mml.AcknowledgeMaterialDefinitionType;
import org.mesa.xml.v0600.b2mml.AcknowledgeMaterialInformationType;
import org.mesa.xml.v0600.b2mml.AcknowledgeMaterialLotType;
import org.mesa.xml.v0600.b2mml.AcknowledgeMaterialSubLotType;
import org.mesa.xml.v0600.b2mml.AcknowledgeMaterialTestSpecType;
import org.mesa.xml.v0600.b2mml.AcknowledgeOperationsCapabilityInformationType;
import org.mesa.xml.v0600.b2mml.AcknowledgeOperationsCapabilityType;
import org.mesa.xml.v0600.b2mml.AcknowledgeOperationsDefinitionInformationType;
import org.mesa.xml.v0600.b2mml.AcknowledgeOperationsDefinitionType;
import org.mesa.xml.v0600.b2mml.AcknowledgeOperationsPerformanceType;
import org.mesa.xml.v0600.b2mml.AcknowledgeOperationsScheduleType;
import org.mesa.xml.v0600.b2mml.AcknowledgePersonType;
import org.mesa.xml.v0600.b2mml.AcknowledgePersonnelClassType;
import org.mesa.xml.v0600.b2mml.AcknowledgePersonnelInformationType;
import org.mesa.xml.v0600.b2mml.AcknowledgePhysicalAssetCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.AcknowledgePhysicalAssetClassType;
import org.mesa.xml.v0600.b2mml.AcknowledgePhysicalAssetInformationType;
import org.mesa.xml.v0600.b2mml.AcknowledgePhysicalAssetType;
import org.mesa.xml.v0600.b2mml.AcknowledgeProcessSegmentInformationType;
import org.mesa.xml.v0600.b2mml.AcknowledgeProcessSegmentType;
import org.mesa.xml.v0600.b2mml.AcknowledgeQualificationTestSpecificationType;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.CancelEquipmentCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.CancelEquipmentClassType;
import org.mesa.xml.v0600.b2mml.CancelEquipmentInformationType;
import org.mesa.xml.v0600.b2mml.CancelEquipmentType;
import org.mesa.xml.v0600.b2mml.CancelMaterialClassType;
import org.mesa.xml.v0600.b2mml.CancelMaterialDefinitionType;
import org.mesa.xml.v0600.b2mml.CancelMaterialInformationType;
import org.mesa.xml.v0600.b2mml.CancelMaterialLotType;
import org.mesa.xml.v0600.b2mml.CancelMaterialSubLotType;
import org.mesa.xml.v0600.b2mml.CancelMaterialTestSpecType;
import org.mesa.xml.v0600.b2mml.CancelOperationsCapabilityInformationType;
import org.mesa.xml.v0600.b2mml.CancelOperationsCapabilityType;
import org.mesa.xml.v0600.b2mml.CancelOperationsDefinitionInformationType;
import org.mesa.xml.v0600.b2mml.CancelOperationsDefinitionType;
import org.mesa.xml.v0600.b2mml.CancelOperationsPerformanceType;
import org.mesa.xml.v0600.b2mml.CancelOperationsScheduleType;
import org.mesa.xml.v0600.b2mml.CancelPersonType;
import org.mesa.xml.v0600.b2mml.CancelPersonnelClassType;
import org.mesa.xml.v0600.b2mml.CancelPersonnelInformationType;
import org.mesa.xml.v0600.b2mml.CancelPhysicalAssetCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.CancelPhysicalAssetClassType;
import org.mesa.xml.v0600.b2mml.CancelPhysicalAssetInformationType;
import org.mesa.xml.v0600.b2mml.CancelPhysicalAssetType;
import org.mesa.xml.v0600.b2mml.CancelProcessSegmentInformationType;
import org.mesa.xml.v0600.b2mml.CancelProcessSegmentType;
import org.mesa.xml.v0600.b2mml.CancelQualificationTestSpecificationType;
import org.mesa.xml.v0600.b2mml.ChangeEquipmentCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.ChangeEquipmentClassType;
import org.mesa.xml.v0600.b2mml.ChangeEquipmentInformationType;
import org.mesa.xml.v0600.b2mml.ChangeEquipmentType;
import org.mesa.xml.v0600.b2mml.ChangeMaterialClassType;
import org.mesa.xml.v0600.b2mml.ChangeMaterialDefinitionType;
import org.mesa.xml.v0600.b2mml.ChangeMaterialInformationType;
import org.mesa.xml.v0600.b2mml.ChangeMaterialLotType;
import org.mesa.xml.v0600.b2mml.ChangeMaterialSubLotType;
import org.mesa.xml.v0600.b2mml.ChangeMaterialTestSpecType;
import org.mesa.xml.v0600.b2mml.ChangeOperationsCapabilityInformationType;
import org.mesa.xml.v0600.b2mml.ChangeOperationsCapabilityType;
import org.mesa.xml.v0600.b2mml.ChangeOperationsDefinitionInformationType;
import org.mesa.xml.v0600.b2mml.ChangeOperationsDefinitionType;
import org.mesa.xml.v0600.b2mml.ChangeOperationsPerformanceType;
import org.mesa.xml.v0600.b2mml.ChangeOperationsScheduleType;
import org.mesa.xml.v0600.b2mml.ChangePersonType;
import org.mesa.xml.v0600.b2mml.ChangePersonnelClassType;
import org.mesa.xml.v0600.b2mml.ChangePersonnelInformationType;
import org.mesa.xml.v0600.b2mml.ChangePhysicalAssetCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.ChangePhysicalAssetClassType;
import org.mesa.xml.v0600.b2mml.ChangePhysicalAssetInformationType;
import org.mesa.xml.v0600.b2mml.ChangePhysicalAssetType;
import org.mesa.xml.v0600.b2mml.ChangeProcessSegmentInformationType;
import org.mesa.xml.v0600.b2mml.ChangeProcessSegmentType;
import org.mesa.xml.v0600.b2mml.ChangeQualificationTestSpecificationType;
import org.mesa.xml.v0600.b2mml.ConfirmBODType;
import org.mesa.xml.v0600.b2mml.DocumentRoot;
import org.mesa.xml.v0600.b2mml.EquipmentCapabilityTestSpecificationType;
import org.mesa.xml.v0600.b2mml.EquipmentClassType;
import org.mesa.xml.v0600.b2mml.EquipmentInformationType;
import org.mesa.xml.v0600.b2mml.EquipmentType;
import org.mesa.xml.v0600.b2mml.GetEquipmentCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.GetEquipmentClassType;
import org.mesa.xml.v0600.b2mml.GetEquipmentInformationType;
import org.mesa.xml.v0600.b2mml.GetEquipmentType;
import org.mesa.xml.v0600.b2mml.GetMaterialClassType;
import org.mesa.xml.v0600.b2mml.GetMaterialDefinitionType;
import org.mesa.xml.v0600.b2mml.GetMaterialInformationType;
import org.mesa.xml.v0600.b2mml.GetMaterialLotType;
import org.mesa.xml.v0600.b2mml.GetMaterialSubLotType;
import org.mesa.xml.v0600.b2mml.GetMaterialTestSpecType;
import org.mesa.xml.v0600.b2mml.GetOperationsCapabilityInformationType;
import org.mesa.xml.v0600.b2mml.GetOperationsCapabilityType;
import org.mesa.xml.v0600.b2mml.GetOperationsDefinitionInformationType;
import org.mesa.xml.v0600.b2mml.GetOperationsDefinitionType;
import org.mesa.xml.v0600.b2mml.GetOperationsPerformanceType;
import org.mesa.xml.v0600.b2mml.GetOperationsScheduleType;
import org.mesa.xml.v0600.b2mml.GetPersonType;
import org.mesa.xml.v0600.b2mml.GetPersonnelClassType;
import org.mesa.xml.v0600.b2mml.GetPersonnelInformationType;
import org.mesa.xml.v0600.b2mml.GetPhysicalAssetCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.GetPhysicalAssetClassType;
import org.mesa.xml.v0600.b2mml.GetPhysicalAssetInformationType;
import org.mesa.xml.v0600.b2mml.GetPhysicalAssetType;
import org.mesa.xml.v0600.b2mml.GetProcessSegmentInformationType;
import org.mesa.xml.v0600.b2mml.GetProcessSegmentType;
import org.mesa.xml.v0600.b2mml.GetQualificationTestSpecificationType;
import org.mesa.xml.v0600.b2mml.MaterialClassType;
import org.mesa.xml.v0600.b2mml.MaterialDefinitionType;
import org.mesa.xml.v0600.b2mml.MaterialInformationType;
import org.mesa.xml.v0600.b2mml.MaterialLotType;
import org.mesa.xml.v0600.b2mml.MaterialSubLotType;
import org.mesa.xml.v0600.b2mml.MaterialTestSpecificationType;
import org.mesa.xml.v0600.b2mml.OperationsCapabilityInformationType;
import org.mesa.xml.v0600.b2mml.OperationsCapabilityType;
import org.mesa.xml.v0600.b2mml.OperationsDefinitionInformationType;
import org.mesa.xml.v0600.b2mml.OperationsDefinitionType;
import org.mesa.xml.v0600.b2mml.OperationsPerformanceType;
import org.mesa.xml.v0600.b2mml.OperationsRequestType;
import org.mesa.xml.v0600.b2mml.OperationsResponseType;
import org.mesa.xml.v0600.b2mml.OperationsScheduleType;
import org.mesa.xml.v0600.b2mml.PersonType;
import org.mesa.xml.v0600.b2mml.PersonnelClassType;
import org.mesa.xml.v0600.b2mml.PersonnelInformationType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetCapabilityTestSpecificationType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetClassType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetInformationType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetType;
import org.mesa.xml.v0600.b2mml.ProcessEquipmentCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.ProcessEquipmentClassType;
import org.mesa.xml.v0600.b2mml.ProcessEquipmentInformationType;
import org.mesa.xml.v0600.b2mml.ProcessEquipmentType;
import org.mesa.xml.v0600.b2mml.ProcessMaterialClassType;
import org.mesa.xml.v0600.b2mml.ProcessMaterialDefinitionType;
import org.mesa.xml.v0600.b2mml.ProcessMaterialInformationType;
import org.mesa.xml.v0600.b2mml.ProcessMaterialLotType;
import org.mesa.xml.v0600.b2mml.ProcessMaterialSubLotType;
import org.mesa.xml.v0600.b2mml.ProcessMaterialTestSpecType;
import org.mesa.xml.v0600.b2mml.ProcessOperationsCapabilityInformationType;
import org.mesa.xml.v0600.b2mml.ProcessOperationsCapabilityType;
import org.mesa.xml.v0600.b2mml.ProcessOperationsDefinitionInformationType;
import org.mesa.xml.v0600.b2mml.ProcessOperationsDefinitionType;
import org.mesa.xml.v0600.b2mml.ProcessOperationsPerformanceType;
import org.mesa.xml.v0600.b2mml.ProcessOperationsScheduleType;
import org.mesa.xml.v0600.b2mml.ProcessPersonType;
import org.mesa.xml.v0600.b2mml.ProcessPersonnelClassType;
import org.mesa.xml.v0600.b2mml.ProcessPersonnelInformationType;
import org.mesa.xml.v0600.b2mml.ProcessPhysicalAssetCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.ProcessPhysicalAssetClassType;
import org.mesa.xml.v0600.b2mml.ProcessPhysicalAssetInformationType;
import org.mesa.xml.v0600.b2mml.ProcessPhysicalAssetType;
import org.mesa.xml.v0600.b2mml.ProcessProcessSegmentInformationType;
import org.mesa.xml.v0600.b2mml.ProcessProcessSegmentType;
import org.mesa.xml.v0600.b2mml.ProcessQualificationTestSpecificationType;
import org.mesa.xml.v0600.b2mml.ProcessSegmentInformationType;
import org.mesa.xml.v0600.b2mml.ProcessSegmentType;
import org.mesa.xml.v0600.b2mml.QualificationTestSpecificationType;
import org.mesa.xml.v0600.b2mml.RespondEquipmentCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.RespondEquipmentClassType;
import org.mesa.xml.v0600.b2mml.RespondEquipmentInformationType;
import org.mesa.xml.v0600.b2mml.RespondEquipmentType;
import org.mesa.xml.v0600.b2mml.RespondMaterialClassType;
import org.mesa.xml.v0600.b2mml.RespondMaterialDefinitionType;
import org.mesa.xml.v0600.b2mml.RespondMaterialInformationType;
import org.mesa.xml.v0600.b2mml.RespondMaterialLotType;
import org.mesa.xml.v0600.b2mml.RespondMaterialSubLotType;
import org.mesa.xml.v0600.b2mml.RespondMaterialTestSpecType;
import org.mesa.xml.v0600.b2mml.RespondOperationsCapabilityInformationType;
import org.mesa.xml.v0600.b2mml.RespondOperationsCapabilityType;
import org.mesa.xml.v0600.b2mml.RespondOperationsDefinitionInformationType;
import org.mesa.xml.v0600.b2mml.RespondOperationsDefinitionType;
import org.mesa.xml.v0600.b2mml.RespondOperationsPerformanceType;
import org.mesa.xml.v0600.b2mml.RespondOperationsScheduleType;
import org.mesa.xml.v0600.b2mml.RespondPersonType;
import org.mesa.xml.v0600.b2mml.RespondPersonnelClassType;
import org.mesa.xml.v0600.b2mml.RespondPersonnelInformationType;
import org.mesa.xml.v0600.b2mml.RespondPhysicalAssetCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.RespondPhysicalAssetClassType;
import org.mesa.xml.v0600.b2mml.RespondPhysicalAssetInformationType;
import org.mesa.xml.v0600.b2mml.RespondPhysicalAssetType;
import org.mesa.xml.v0600.b2mml.RespondProcessSegmentInformationType;
import org.mesa.xml.v0600.b2mml.RespondProcessSegmentType;
import org.mesa.xml.v0600.b2mml.RespondQualificationTestSpecificationType;
import org.mesa.xml.v0600.b2mml.ShowEquipmentCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.ShowEquipmentClassType;
import org.mesa.xml.v0600.b2mml.ShowEquipmentInformationType;
import org.mesa.xml.v0600.b2mml.ShowEquipmentType;
import org.mesa.xml.v0600.b2mml.ShowMaterialClassType;
import org.mesa.xml.v0600.b2mml.ShowMaterialDefinitionType;
import org.mesa.xml.v0600.b2mml.ShowMaterialInformationType;
import org.mesa.xml.v0600.b2mml.ShowMaterialLotType;
import org.mesa.xml.v0600.b2mml.ShowMaterialSubLotType;
import org.mesa.xml.v0600.b2mml.ShowMaterialTestSpecType;
import org.mesa.xml.v0600.b2mml.ShowOperationsCapabilityInformationType;
import org.mesa.xml.v0600.b2mml.ShowOperationsCapabilityType;
import org.mesa.xml.v0600.b2mml.ShowOperationsDefinitionInformationType;
import org.mesa.xml.v0600.b2mml.ShowOperationsDefinitionType;
import org.mesa.xml.v0600.b2mml.ShowOperationsPerformanceType;
import org.mesa.xml.v0600.b2mml.ShowOperationsScheduleType;
import org.mesa.xml.v0600.b2mml.ShowPersonType;
import org.mesa.xml.v0600.b2mml.ShowPersonnelClassType;
import org.mesa.xml.v0600.b2mml.ShowPersonnelInformationType;
import org.mesa.xml.v0600.b2mml.ShowPhysicalAssetCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.ShowPhysicalAssetClassType;
import org.mesa.xml.v0600.b2mml.ShowPhysicalAssetInformationType;
import org.mesa.xml.v0600.b2mml.ShowPhysicalAssetType;
import org.mesa.xml.v0600.b2mml.ShowProcessSegmentInformationType;
import org.mesa.xml.v0600.b2mml.ShowProcessSegmentType;
import org.mesa.xml.v0600.b2mml.ShowQualificationTestSpecificationType;
import org.mesa.xml.v0600.b2mml.SyncEquipmentCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.SyncEquipmentClassType;
import org.mesa.xml.v0600.b2mml.SyncEquipmentInformationType;
import org.mesa.xml.v0600.b2mml.SyncEquipmentType;
import org.mesa.xml.v0600.b2mml.SyncMaterialClassType;
import org.mesa.xml.v0600.b2mml.SyncMaterialDefinitionType;
import org.mesa.xml.v0600.b2mml.SyncMaterialInformationType;
import org.mesa.xml.v0600.b2mml.SyncMaterialLotType;
import org.mesa.xml.v0600.b2mml.SyncMaterialSubLotType;
import org.mesa.xml.v0600.b2mml.SyncMaterialTestSpecType;
import org.mesa.xml.v0600.b2mml.SyncOperationsCapabilityInformationType;
import org.mesa.xml.v0600.b2mml.SyncOperationsCapabilityType;
import org.mesa.xml.v0600.b2mml.SyncOperationsDefinitionInformationType;
import org.mesa.xml.v0600.b2mml.SyncOperationsDefinitionType;
import org.mesa.xml.v0600.b2mml.SyncOperationsPerformanceType;
import org.mesa.xml.v0600.b2mml.SyncOperationsScheduleType;
import org.mesa.xml.v0600.b2mml.SyncPersonType;
import org.mesa.xml.v0600.b2mml.SyncPersonnelClassType;
import org.mesa.xml.v0600.b2mml.SyncPersonnelInformationType;
import org.mesa.xml.v0600.b2mml.SyncPhysicalAssetCapabilityTestSpecType;
import org.mesa.xml.v0600.b2mml.SyncPhysicalAssetClassType;
import org.mesa.xml.v0600.b2mml.SyncPhysicalAssetInformationType;
import org.mesa.xml.v0600.b2mml.SyncPhysicalAssetType;
import org.mesa.xml.v0600.b2mml.SyncProcessSegmentInformationType;
import org.mesa.xml.v0600.b2mml.SyncProcessSegmentType;
import org.mesa.xml.v0600.b2mml.SyncQualificationTestSpecificationType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeEquipment <em>Acknowledge Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeEquipmentCapabilityTestSpec <em>Acknowledge Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeEquipmentClass <em>Acknowledge Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeEquipmentInformation <em>Acknowledge Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeMaterialClass <em>Acknowledge Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeMaterialDefinition <em>Acknowledge Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeMaterialInformation <em>Acknowledge Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeMaterialLot <em>Acknowledge Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeMaterialSubLot <em>Acknowledge Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeMaterialTestSpec <em>Acknowledge Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeOperationsCapability <em>Acknowledge Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeOperationsCapabilityInformation <em>Acknowledge Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeOperationsDefinition <em>Acknowledge Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeOperationsDefinitionInformation <em>Acknowledge Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeOperationsPerformance <em>Acknowledge Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeOperationsSchedule <em>Acknowledge Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgePerson <em>Acknowledge Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgePersonnelClass <em>Acknowledge Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgePersonnelInformation <em>Acknowledge Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgePhysicalAsset <em>Acknowledge Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgePhysicalAssetCapabilityTestSpec <em>Acknowledge Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgePhysicalAssetClass <em>Acknowledge Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgePhysicalAssetInformation <em>Acknowledge Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeProcessSegment <em>Acknowledge Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeProcessSegmentInformation <em>Acknowledge Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getAcknowledgeQualificationTestSpecification <em>Acknowledge Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelEquipment <em>Cancel Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelEquipmentCapabilityTestSpec <em>Cancel Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelEquipmentClass <em>Cancel Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelEquipmentInformation <em>Cancel Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelMaterialClass <em>Cancel Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelMaterialDefinition <em>Cancel Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelMaterialInformation <em>Cancel Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelMaterialLot <em>Cancel Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelMaterialSubLot <em>Cancel Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelMaterialTestSpec <em>Cancel Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelOperationsCapability <em>Cancel Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelOperationsCapabilityInformation <em>Cancel Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelOperationsDefinition <em>Cancel Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelOperationsDefinitionInformation <em>Cancel Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelOperationsPerformance <em>Cancel Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelOperationsSchedule <em>Cancel Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelPerson <em>Cancel Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelPersonnelClass <em>Cancel Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelPersonnelInformation <em>Cancel Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelPhysicalAsset <em>Cancel Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelPhysicalAssetCapabilityTestSpec <em>Cancel Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelPhysicalAssetClass <em>Cancel Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelPhysicalAssetInformation <em>Cancel Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelProcessSegment <em>Cancel Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelProcessSegmentInformation <em>Cancel Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getCancelQualificationTestSpecification <em>Cancel Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeEquipment <em>Change Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeEquipmentCapabilityTestSpec <em>Change Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeEquipmentClass <em>Change Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeEquipmentInformation <em>Change Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeMaterialClass <em>Change Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeMaterialDefinition <em>Change Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeMaterialInformation <em>Change Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeMaterialLot <em>Change Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeMaterialSubLot <em>Change Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeMaterialTestSpec <em>Change Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeOperationsCapability <em>Change Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeOperationsCapabilityInformation <em>Change Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeOperationsDefinition <em>Change Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeOperationsDefinitionInformation <em>Change Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeOperationsPerformance <em>Change Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeOperationsSchedule <em>Change Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangePerson <em>Change Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangePersonnelClass <em>Change Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangePersonnelInformation <em>Change Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangePhysicalAsset <em>Change Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangePhysicalAssetCapabilityTestSpec <em>Change Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangePhysicalAssetClass <em>Change Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangePhysicalAssetInformation <em>Change Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeProcessSegment <em>Change Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeProcessSegmentInformation <em>Change Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getChangeQualificationTestSpecification <em>Change Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getConfirmBOD <em>Confirm BOD</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getEquipment <em>Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getEquipmentCapabilityTestSpecification <em>Equipment Capability Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getEquipmentClass <em>Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getEquipmentInformation <em>Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetEquipment <em>Get Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetEquipmentCapabilityTestSpec <em>Get Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetEquipmentClass <em>Get Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetEquipmentInformation <em>Get Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetMaterialClass <em>Get Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetMaterialDefinition <em>Get Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetMaterialInformation <em>Get Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetMaterialLot <em>Get Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetMaterialSubLot <em>Get Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetMaterialTestSpec <em>Get Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetOperationsCapability <em>Get Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetOperationsCapabilityInformation <em>Get Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetOperationsDefinition <em>Get Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetOperationsDefinitionInformation <em>Get Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetOperationsPerformance <em>Get Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetOperationsSchedule <em>Get Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetPerson <em>Get Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetPersonnelClass <em>Get Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetPersonnelInformation <em>Get Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetPhysicalAsset <em>Get Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetPhysicalAssetCapabilityTestSpec <em>Get Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetPhysicalAssetClass <em>Get Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetPhysicalAssetInformation <em>Get Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetProcessSegment <em>Get Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetProcessSegmentInformation <em>Get Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getGetQualificationTestSpecification <em>Get Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getMaterialClass <em>Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getMaterialDefinition <em>Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getMaterialInformation <em>Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getMaterialLot <em>Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getMaterialSubLot <em>Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getMaterialTestSpecification <em>Material Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getOperationsCapability <em>Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getOperationsCapabilityInformation <em>Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getOperationsDefinition <em>Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getOperationsDefinitionInformation <em>Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getOperationsPerformance <em>Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getOperationsRequest <em>Operations Request</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getOperationsResponse <em>Operations Response</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getOperationsSchedule <em>Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getPerson <em>Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getPersonnelClass <em>Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getPersonnelInformation <em>Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getPhysicalAsset <em>Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getPhysicalAssetCapabilityTestSpecification <em>Physical Asset Capability Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getPhysicalAssetClass <em>Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getPhysicalAssetInformation <em>Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessEquipment <em>Process Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessEquipmentCapabilityTestSpec <em>Process Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessEquipmentClass <em>Process Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessEquipmentInformation <em>Process Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessMaterialClass <em>Process Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessMaterialDefinition <em>Process Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessMaterialInformation <em>Process Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessMaterialLot <em>Process Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessMaterialSubLot <em>Process Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessMaterialTestSpec <em>Process Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessOperationsCapability <em>Process Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessOperationsCapabilityInformation <em>Process Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessOperationsDefinition <em>Process Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessOperationsDefinitionInformation <em>Process Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessOperationsPerformance <em>Process Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessOperationsSchedule <em>Process Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessPerson <em>Process Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessPersonnelClass <em>Process Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessPersonnelInformation <em>Process Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessPhysicalAsset <em>Process Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessPhysicalAssetCapabilityTestSpec <em>Process Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessPhysicalAssetClass <em>Process Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessPhysicalAssetInformation <em>Process Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessProcessSegment <em>Process Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessProcessSegmentInformation <em>Process Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessQualificationTestSpecification <em>Process Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessSegment <em>Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getProcessSegmentInformation <em>Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getQualificationTestSpecification <em>Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondEquipment <em>Respond Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondEquipmentCapabilityTestSpec <em>Respond Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondEquipmentClass <em>Respond Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondEquipmentInformation <em>Respond Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondMaterialClass <em>Respond Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondMaterialDefinition <em>Respond Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondMaterialInformation <em>Respond Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondMaterialLot <em>Respond Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondMaterialSubLot <em>Respond Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondMaterialTestSpec <em>Respond Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondOperationsCapability <em>Respond Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondOperationsCapabilityInformation <em>Respond Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondOperationsDefinition <em>Respond Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondOperationsDefinitionInformation <em>Respond Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondOperationsPerformance <em>Respond Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondOperationsSchedule <em>Respond Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondPerson <em>Respond Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondPersonnelClass <em>Respond Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondPersonnelInformation <em>Respond Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondPhysicalAsset <em>Respond Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondPhysicalAssetCapabilityTestSpec <em>Respond Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondPhysicalAssetClass <em>Respond Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondPhysicalAssetInformation <em>Respond Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondProcessSegment <em>Respond Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondProcessSegmentInformation <em>Respond Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getRespondQualificationTestSpecification <em>Respond Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowEquipment <em>Show Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowEquipmentCapabilityTestSpec <em>Show Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowEquipmentClass <em>Show Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowEquipmentInformation <em>Show Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowMaterialClass <em>Show Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowMaterialDefinition <em>Show Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowMaterialInformation <em>Show Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowMaterialLot <em>Show Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowMaterialSubLot <em>Show Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowMaterialTestSpec <em>Show Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowOperationsCapability <em>Show Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowOperationsCapabilityInformation <em>Show Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowOperationsDefinition <em>Show Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowOperationsDefinitionInformation <em>Show Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowOperationsPerformance <em>Show Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowOperationsSchedule <em>Show Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowPerson <em>Show Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowPersonnelClass <em>Show Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowPersonnelInformation <em>Show Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowPhysicalAsset <em>Show Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowPhysicalAssetCapabilityTestSpec <em>Show Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowPhysicalAssetClass <em>Show Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowPhysicalAssetInformation <em>Show Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowProcessSegment <em>Show Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowProcessSegmentInformation <em>Show Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getShowQualificationTestSpecification <em>Show Qualification Test Specification</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncEquipment <em>Sync Equipment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncEquipmentCapabilityTestSpec <em>Sync Equipment Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncEquipmentClass <em>Sync Equipment Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncEquipmentInformation <em>Sync Equipment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncMaterialClass <em>Sync Material Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncMaterialDefinition <em>Sync Material Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncMaterialInformation <em>Sync Material Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncMaterialLot <em>Sync Material Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncMaterialSubLot <em>Sync Material Sub Lot</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncMaterialTestSpec <em>Sync Material Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncOperationsCapability <em>Sync Operations Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncOperationsCapabilityInformation <em>Sync Operations Capability Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncOperationsDefinition <em>Sync Operations Definition</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncOperationsDefinitionInformation <em>Sync Operations Definition Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncOperationsPerformance <em>Sync Operations Performance</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncOperationsSchedule <em>Sync Operations Schedule</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncPerson <em>Sync Person</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncPersonnelClass <em>Sync Personnel Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncPersonnelInformation <em>Sync Personnel Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncPhysicalAsset <em>Sync Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncPhysicalAssetCapabilityTestSpec <em>Sync Physical Asset Capability Test Spec</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncPhysicalAssetClass <em>Sync Physical Asset Class</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncPhysicalAssetInformation <em>Sync Physical Asset Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncProcessSegment <em>Sync Process Segment</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncProcessSegmentInformation <em>Sync Process Segment Information</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DocumentRootImpl#getSyncQualificationTestSpecification <em>Sync Qualification Test Specification</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DocumentRootImpl extends MinimalEObjectImpl.Container implements DocumentRoot {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * The cached value of the '{@link #getXMLNSPrefixMap() <em>XMLNS Prefix Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXMLNSPrefixMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> xMLNSPrefixMap;

	/**
	 * The cached value of the '{@link #getXSISchemaLocation() <em>XSI Schema Location</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXSISchemaLocation()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> xSISchemaLocation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DocumentRootImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDocumentRoot();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, B2MMLPackage.DOCUMENT_ROOT__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getXMLNSPrefixMap() {
		if (xMLNSPrefixMap == null) {
			xMLNSPrefixMap = new EcoreEMap<String,String>(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, B2MMLPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		}
		return xMLNSPrefixMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getXSISchemaLocation() {
		if (xSISchemaLocation == null) {
			xSISchemaLocation = new EcoreEMap<String,String>(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, B2MMLPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		}
		return xSISchemaLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeEquipmentType getAcknowledgeEquipment() {
		return (AcknowledgeEquipmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeEquipment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeEquipment(AcknowledgeEquipmentType newAcknowledgeEquipment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeEquipment(), newAcknowledgeEquipment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeEquipment(AcknowledgeEquipmentType newAcknowledgeEquipment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeEquipment(), newAcknowledgeEquipment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeEquipmentCapabilityTestSpecType getAcknowledgeEquipmentCapabilityTestSpec() {
		return (AcknowledgeEquipmentCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeEquipmentCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeEquipmentCapabilityTestSpec(AcknowledgeEquipmentCapabilityTestSpecType newAcknowledgeEquipmentCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeEquipmentCapabilityTestSpec(), newAcknowledgeEquipmentCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeEquipmentCapabilityTestSpec(AcknowledgeEquipmentCapabilityTestSpecType newAcknowledgeEquipmentCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeEquipmentCapabilityTestSpec(), newAcknowledgeEquipmentCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeEquipmentClassType getAcknowledgeEquipmentClass() {
		return (AcknowledgeEquipmentClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeEquipmentClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeEquipmentClass(AcknowledgeEquipmentClassType newAcknowledgeEquipmentClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeEquipmentClass(), newAcknowledgeEquipmentClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeEquipmentClass(AcknowledgeEquipmentClassType newAcknowledgeEquipmentClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeEquipmentClass(), newAcknowledgeEquipmentClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeEquipmentInformationType getAcknowledgeEquipmentInformation() {
		return (AcknowledgeEquipmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeEquipmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeEquipmentInformation(AcknowledgeEquipmentInformationType newAcknowledgeEquipmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeEquipmentInformation(), newAcknowledgeEquipmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeEquipmentInformation(AcknowledgeEquipmentInformationType newAcknowledgeEquipmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeEquipmentInformation(), newAcknowledgeEquipmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeMaterialClassType getAcknowledgeMaterialClass() {
		return (AcknowledgeMaterialClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeMaterialClass(AcknowledgeMaterialClassType newAcknowledgeMaterialClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialClass(), newAcknowledgeMaterialClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeMaterialClass(AcknowledgeMaterialClassType newAcknowledgeMaterialClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialClass(), newAcknowledgeMaterialClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeMaterialDefinitionType getAcknowledgeMaterialDefinition() {
		return (AcknowledgeMaterialDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeMaterialDefinition(AcknowledgeMaterialDefinitionType newAcknowledgeMaterialDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialDefinition(), newAcknowledgeMaterialDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeMaterialDefinition(AcknowledgeMaterialDefinitionType newAcknowledgeMaterialDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialDefinition(), newAcknowledgeMaterialDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeMaterialInformationType getAcknowledgeMaterialInformation() {
		return (AcknowledgeMaterialInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeMaterialInformation(AcknowledgeMaterialInformationType newAcknowledgeMaterialInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialInformation(), newAcknowledgeMaterialInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeMaterialInformation(AcknowledgeMaterialInformationType newAcknowledgeMaterialInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialInformation(), newAcknowledgeMaterialInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeMaterialLotType getAcknowledgeMaterialLot() {
		return (AcknowledgeMaterialLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeMaterialLot(AcknowledgeMaterialLotType newAcknowledgeMaterialLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialLot(), newAcknowledgeMaterialLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeMaterialLot(AcknowledgeMaterialLotType newAcknowledgeMaterialLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialLot(), newAcknowledgeMaterialLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeMaterialSubLotType getAcknowledgeMaterialSubLot() {
		return (AcknowledgeMaterialSubLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialSubLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeMaterialSubLot(AcknowledgeMaterialSubLotType newAcknowledgeMaterialSubLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialSubLot(), newAcknowledgeMaterialSubLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeMaterialSubLot(AcknowledgeMaterialSubLotType newAcknowledgeMaterialSubLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialSubLot(), newAcknowledgeMaterialSubLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeMaterialTestSpecType getAcknowledgeMaterialTestSpec() {
		return (AcknowledgeMaterialTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeMaterialTestSpec(AcknowledgeMaterialTestSpecType newAcknowledgeMaterialTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialTestSpec(), newAcknowledgeMaterialTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeMaterialTestSpec(AcknowledgeMaterialTestSpecType newAcknowledgeMaterialTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeMaterialTestSpec(), newAcknowledgeMaterialTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeOperationsCapabilityType getAcknowledgeOperationsCapability() {
		return (AcknowledgeOperationsCapabilityType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsCapability(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeOperationsCapability(AcknowledgeOperationsCapabilityType newAcknowledgeOperationsCapability, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsCapability(), newAcknowledgeOperationsCapability, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeOperationsCapability(AcknowledgeOperationsCapabilityType newAcknowledgeOperationsCapability) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsCapability(), newAcknowledgeOperationsCapability);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeOperationsCapabilityInformationType getAcknowledgeOperationsCapabilityInformation() {
		return (AcknowledgeOperationsCapabilityInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsCapabilityInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeOperationsCapabilityInformation(AcknowledgeOperationsCapabilityInformationType newAcknowledgeOperationsCapabilityInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsCapabilityInformation(), newAcknowledgeOperationsCapabilityInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeOperationsCapabilityInformation(AcknowledgeOperationsCapabilityInformationType newAcknowledgeOperationsCapabilityInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsCapabilityInformation(), newAcknowledgeOperationsCapabilityInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeOperationsDefinitionType getAcknowledgeOperationsDefinition() {
		return (AcknowledgeOperationsDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeOperationsDefinition(AcknowledgeOperationsDefinitionType newAcknowledgeOperationsDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsDefinition(), newAcknowledgeOperationsDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeOperationsDefinition(AcknowledgeOperationsDefinitionType newAcknowledgeOperationsDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsDefinition(), newAcknowledgeOperationsDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeOperationsDefinitionInformationType getAcknowledgeOperationsDefinitionInformation() {
		return (AcknowledgeOperationsDefinitionInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsDefinitionInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeOperationsDefinitionInformation(AcknowledgeOperationsDefinitionInformationType newAcknowledgeOperationsDefinitionInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsDefinitionInformation(), newAcknowledgeOperationsDefinitionInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeOperationsDefinitionInformation(AcknowledgeOperationsDefinitionInformationType newAcknowledgeOperationsDefinitionInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsDefinitionInformation(), newAcknowledgeOperationsDefinitionInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeOperationsPerformanceType getAcknowledgeOperationsPerformance() {
		return (AcknowledgeOperationsPerformanceType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsPerformance(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeOperationsPerformance(AcknowledgeOperationsPerformanceType newAcknowledgeOperationsPerformance, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsPerformance(), newAcknowledgeOperationsPerformance, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeOperationsPerformance(AcknowledgeOperationsPerformanceType newAcknowledgeOperationsPerformance) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsPerformance(), newAcknowledgeOperationsPerformance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeOperationsScheduleType getAcknowledgeOperationsSchedule() {
		return (AcknowledgeOperationsScheduleType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsSchedule(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeOperationsSchedule(AcknowledgeOperationsScheduleType newAcknowledgeOperationsSchedule, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsSchedule(), newAcknowledgeOperationsSchedule, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeOperationsSchedule(AcknowledgeOperationsScheduleType newAcknowledgeOperationsSchedule) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeOperationsSchedule(), newAcknowledgeOperationsSchedule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePersonType getAcknowledgePerson() {
		return (AcknowledgePersonType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePerson(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgePerson(AcknowledgePersonType newAcknowledgePerson, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePerson(), newAcknowledgePerson, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgePerson(AcknowledgePersonType newAcknowledgePerson) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePerson(), newAcknowledgePerson);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePersonnelClassType getAcknowledgePersonnelClass() {
		return (AcknowledgePersonnelClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePersonnelClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgePersonnelClass(AcknowledgePersonnelClassType newAcknowledgePersonnelClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePersonnelClass(), newAcknowledgePersonnelClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgePersonnelClass(AcknowledgePersonnelClassType newAcknowledgePersonnelClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePersonnelClass(), newAcknowledgePersonnelClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePersonnelInformationType getAcknowledgePersonnelInformation() {
		return (AcknowledgePersonnelInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePersonnelInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgePersonnelInformation(AcknowledgePersonnelInformationType newAcknowledgePersonnelInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePersonnelInformation(), newAcknowledgePersonnelInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgePersonnelInformation(AcknowledgePersonnelInformationType newAcknowledgePersonnelInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePersonnelInformation(), newAcknowledgePersonnelInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePhysicalAssetType getAcknowledgePhysicalAsset() {
		return (AcknowledgePhysicalAssetType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePhysicalAsset(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgePhysicalAsset(AcknowledgePhysicalAssetType newAcknowledgePhysicalAsset, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePhysicalAsset(), newAcknowledgePhysicalAsset, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgePhysicalAsset(AcknowledgePhysicalAssetType newAcknowledgePhysicalAsset) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePhysicalAsset(), newAcknowledgePhysicalAsset);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePhysicalAssetCapabilityTestSpecType getAcknowledgePhysicalAssetCapabilityTestSpec() {
		return (AcknowledgePhysicalAssetCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePhysicalAssetCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgePhysicalAssetCapabilityTestSpec(AcknowledgePhysicalAssetCapabilityTestSpecType newAcknowledgePhysicalAssetCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePhysicalAssetCapabilityTestSpec(), newAcknowledgePhysicalAssetCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgePhysicalAssetCapabilityTestSpec(AcknowledgePhysicalAssetCapabilityTestSpecType newAcknowledgePhysicalAssetCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePhysicalAssetCapabilityTestSpec(), newAcknowledgePhysicalAssetCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePhysicalAssetClassType getAcknowledgePhysicalAssetClass() {
		return (AcknowledgePhysicalAssetClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePhysicalAssetClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgePhysicalAssetClass(AcknowledgePhysicalAssetClassType newAcknowledgePhysicalAssetClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePhysicalAssetClass(), newAcknowledgePhysicalAssetClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgePhysicalAssetClass(AcknowledgePhysicalAssetClassType newAcknowledgePhysicalAssetClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePhysicalAssetClass(), newAcknowledgePhysicalAssetClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgePhysicalAssetInformationType getAcknowledgePhysicalAssetInformation() {
		return (AcknowledgePhysicalAssetInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePhysicalAssetInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgePhysicalAssetInformation(AcknowledgePhysicalAssetInformationType newAcknowledgePhysicalAssetInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePhysicalAssetInformation(), newAcknowledgePhysicalAssetInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgePhysicalAssetInformation(AcknowledgePhysicalAssetInformationType newAcknowledgePhysicalAssetInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgePhysicalAssetInformation(), newAcknowledgePhysicalAssetInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeProcessSegmentType getAcknowledgeProcessSegment() {
		return (AcknowledgeProcessSegmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeProcessSegment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeProcessSegment(AcknowledgeProcessSegmentType newAcknowledgeProcessSegment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeProcessSegment(), newAcknowledgeProcessSegment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeProcessSegment(AcknowledgeProcessSegmentType newAcknowledgeProcessSegment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeProcessSegment(), newAcknowledgeProcessSegment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeProcessSegmentInformationType getAcknowledgeProcessSegmentInformation() {
		return (AcknowledgeProcessSegmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeProcessSegmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeProcessSegmentInformation(AcknowledgeProcessSegmentInformationType newAcknowledgeProcessSegmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeProcessSegmentInformation(), newAcknowledgeProcessSegmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeProcessSegmentInformation(AcknowledgeProcessSegmentInformationType newAcknowledgeProcessSegmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeProcessSegmentInformation(), newAcknowledgeProcessSegmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcknowledgeQualificationTestSpecificationType getAcknowledgeQualificationTestSpecification() {
		return (AcknowledgeQualificationTestSpecificationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeQualificationTestSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledgeQualificationTestSpecification(AcknowledgeQualificationTestSpecificationType newAcknowledgeQualificationTestSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeQualificationTestSpecification(), newAcknowledgeQualificationTestSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledgeQualificationTestSpecification(AcknowledgeQualificationTestSpecificationType newAcknowledgeQualificationTestSpecification) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_AcknowledgeQualificationTestSpecification(), newAcknowledgeQualificationTestSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelEquipmentType getCancelEquipment() {
		return (CancelEquipmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelEquipment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelEquipment(CancelEquipmentType newCancelEquipment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelEquipment(), newCancelEquipment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelEquipment(CancelEquipmentType newCancelEquipment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelEquipment(), newCancelEquipment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelEquipmentCapabilityTestSpecType getCancelEquipmentCapabilityTestSpec() {
		return (CancelEquipmentCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelEquipmentCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelEquipmentCapabilityTestSpec(CancelEquipmentCapabilityTestSpecType newCancelEquipmentCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelEquipmentCapabilityTestSpec(), newCancelEquipmentCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelEquipmentCapabilityTestSpec(CancelEquipmentCapabilityTestSpecType newCancelEquipmentCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelEquipmentCapabilityTestSpec(), newCancelEquipmentCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelEquipmentClassType getCancelEquipmentClass() {
		return (CancelEquipmentClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelEquipmentClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelEquipmentClass(CancelEquipmentClassType newCancelEquipmentClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelEquipmentClass(), newCancelEquipmentClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelEquipmentClass(CancelEquipmentClassType newCancelEquipmentClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelEquipmentClass(), newCancelEquipmentClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelEquipmentInformationType getCancelEquipmentInformation() {
		return (CancelEquipmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelEquipmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelEquipmentInformation(CancelEquipmentInformationType newCancelEquipmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelEquipmentInformation(), newCancelEquipmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelEquipmentInformation(CancelEquipmentInformationType newCancelEquipmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelEquipmentInformation(), newCancelEquipmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelMaterialClassType getCancelMaterialClass() {
		return (CancelMaterialClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelMaterialClass(CancelMaterialClassType newCancelMaterialClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialClass(), newCancelMaterialClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelMaterialClass(CancelMaterialClassType newCancelMaterialClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialClass(), newCancelMaterialClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelMaterialDefinitionType getCancelMaterialDefinition() {
		return (CancelMaterialDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelMaterialDefinition(CancelMaterialDefinitionType newCancelMaterialDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialDefinition(), newCancelMaterialDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelMaterialDefinition(CancelMaterialDefinitionType newCancelMaterialDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialDefinition(), newCancelMaterialDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelMaterialInformationType getCancelMaterialInformation() {
		return (CancelMaterialInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelMaterialInformation(CancelMaterialInformationType newCancelMaterialInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialInformation(), newCancelMaterialInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelMaterialInformation(CancelMaterialInformationType newCancelMaterialInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialInformation(), newCancelMaterialInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelMaterialLotType getCancelMaterialLot() {
		return (CancelMaterialLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelMaterialLot(CancelMaterialLotType newCancelMaterialLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialLot(), newCancelMaterialLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelMaterialLot(CancelMaterialLotType newCancelMaterialLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialLot(), newCancelMaterialLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelMaterialSubLotType getCancelMaterialSubLot() {
		return (CancelMaterialSubLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialSubLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelMaterialSubLot(CancelMaterialSubLotType newCancelMaterialSubLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialSubLot(), newCancelMaterialSubLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelMaterialSubLot(CancelMaterialSubLotType newCancelMaterialSubLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialSubLot(), newCancelMaterialSubLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelMaterialTestSpecType getCancelMaterialTestSpec() {
		return (CancelMaterialTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelMaterialTestSpec(CancelMaterialTestSpecType newCancelMaterialTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialTestSpec(), newCancelMaterialTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelMaterialTestSpec(CancelMaterialTestSpecType newCancelMaterialTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelMaterialTestSpec(), newCancelMaterialTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelOperationsCapabilityType getCancelOperationsCapability() {
		return (CancelOperationsCapabilityType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsCapability(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelOperationsCapability(CancelOperationsCapabilityType newCancelOperationsCapability, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsCapability(), newCancelOperationsCapability, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelOperationsCapability(CancelOperationsCapabilityType newCancelOperationsCapability) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsCapability(), newCancelOperationsCapability);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelOperationsCapabilityInformationType getCancelOperationsCapabilityInformation() {
		return (CancelOperationsCapabilityInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsCapabilityInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelOperationsCapabilityInformation(CancelOperationsCapabilityInformationType newCancelOperationsCapabilityInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsCapabilityInformation(), newCancelOperationsCapabilityInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelOperationsCapabilityInformation(CancelOperationsCapabilityInformationType newCancelOperationsCapabilityInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsCapabilityInformation(), newCancelOperationsCapabilityInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelOperationsDefinitionType getCancelOperationsDefinition() {
		return (CancelOperationsDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelOperationsDefinition(CancelOperationsDefinitionType newCancelOperationsDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsDefinition(), newCancelOperationsDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelOperationsDefinition(CancelOperationsDefinitionType newCancelOperationsDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsDefinition(), newCancelOperationsDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelOperationsDefinitionInformationType getCancelOperationsDefinitionInformation() {
		return (CancelOperationsDefinitionInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsDefinitionInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelOperationsDefinitionInformation(CancelOperationsDefinitionInformationType newCancelOperationsDefinitionInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsDefinitionInformation(), newCancelOperationsDefinitionInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelOperationsDefinitionInformation(CancelOperationsDefinitionInformationType newCancelOperationsDefinitionInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsDefinitionInformation(), newCancelOperationsDefinitionInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelOperationsPerformanceType getCancelOperationsPerformance() {
		return (CancelOperationsPerformanceType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsPerformance(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelOperationsPerformance(CancelOperationsPerformanceType newCancelOperationsPerformance, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsPerformance(), newCancelOperationsPerformance, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelOperationsPerformance(CancelOperationsPerformanceType newCancelOperationsPerformance) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsPerformance(), newCancelOperationsPerformance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelOperationsScheduleType getCancelOperationsSchedule() {
		return (CancelOperationsScheduleType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsSchedule(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelOperationsSchedule(CancelOperationsScheduleType newCancelOperationsSchedule, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsSchedule(), newCancelOperationsSchedule, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelOperationsSchedule(CancelOperationsScheduleType newCancelOperationsSchedule) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelOperationsSchedule(), newCancelOperationsSchedule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPersonType getCancelPerson() {
		return (CancelPersonType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPerson(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelPerson(CancelPersonType newCancelPerson, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPerson(), newCancelPerson, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelPerson(CancelPersonType newCancelPerson) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPerson(), newCancelPerson);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPersonnelClassType getCancelPersonnelClass() {
		return (CancelPersonnelClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPersonnelClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelPersonnelClass(CancelPersonnelClassType newCancelPersonnelClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPersonnelClass(), newCancelPersonnelClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelPersonnelClass(CancelPersonnelClassType newCancelPersonnelClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPersonnelClass(), newCancelPersonnelClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPersonnelInformationType getCancelPersonnelInformation() {
		return (CancelPersonnelInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPersonnelInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelPersonnelInformation(CancelPersonnelInformationType newCancelPersonnelInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPersonnelInformation(), newCancelPersonnelInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelPersonnelInformation(CancelPersonnelInformationType newCancelPersonnelInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPersonnelInformation(), newCancelPersonnelInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPhysicalAssetType getCancelPhysicalAsset() {
		return (CancelPhysicalAssetType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPhysicalAsset(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelPhysicalAsset(CancelPhysicalAssetType newCancelPhysicalAsset, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPhysicalAsset(), newCancelPhysicalAsset, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelPhysicalAsset(CancelPhysicalAssetType newCancelPhysicalAsset) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPhysicalAsset(), newCancelPhysicalAsset);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPhysicalAssetCapabilityTestSpecType getCancelPhysicalAssetCapabilityTestSpec() {
		return (CancelPhysicalAssetCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPhysicalAssetCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelPhysicalAssetCapabilityTestSpec(CancelPhysicalAssetCapabilityTestSpecType newCancelPhysicalAssetCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPhysicalAssetCapabilityTestSpec(), newCancelPhysicalAssetCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelPhysicalAssetCapabilityTestSpec(CancelPhysicalAssetCapabilityTestSpecType newCancelPhysicalAssetCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPhysicalAssetCapabilityTestSpec(), newCancelPhysicalAssetCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPhysicalAssetClassType getCancelPhysicalAssetClass() {
		return (CancelPhysicalAssetClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPhysicalAssetClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelPhysicalAssetClass(CancelPhysicalAssetClassType newCancelPhysicalAssetClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPhysicalAssetClass(), newCancelPhysicalAssetClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelPhysicalAssetClass(CancelPhysicalAssetClassType newCancelPhysicalAssetClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPhysicalAssetClass(), newCancelPhysicalAssetClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelPhysicalAssetInformationType getCancelPhysicalAssetInformation() {
		return (CancelPhysicalAssetInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPhysicalAssetInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelPhysicalAssetInformation(CancelPhysicalAssetInformationType newCancelPhysicalAssetInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPhysicalAssetInformation(), newCancelPhysicalAssetInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelPhysicalAssetInformation(CancelPhysicalAssetInformationType newCancelPhysicalAssetInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelPhysicalAssetInformation(), newCancelPhysicalAssetInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelProcessSegmentType getCancelProcessSegment() {
		return (CancelProcessSegmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelProcessSegment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelProcessSegment(CancelProcessSegmentType newCancelProcessSegment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelProcessSegment(), newCancelProcessSegment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelProcessSegment(CancelProcessSegmentType newCancelProcessSegment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelProcessSegment(), newCancelProcessSegment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelProcessSegmentInformationType getCancelProcessSegmentInformation() {
		return (CancelProcessSegmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelProcessSegmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelProcessSegmentInformation(CancelProcessSegmentInformationType newCancelProcessSegmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelProcessSegmentInformation(), newCancelProcessSegmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelProcessSegmentInformation(CancelProcessSegmentInformationType newCancelProcessSegmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelProcessSegmentInformation(), newCancelProcessSegmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CancelQualificationTestSpecificationType getCancelQualificationTestSpecification() {
		return (CancelQualificationTestSpecificationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelQualificationTestSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancelQualificationTestSpecification(CancelQualificationTestSpecificationType newCancelQualificationTestSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelQualificationTestSpecification(), newCancelQualificationTestSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancelQualificationTestSpecification(CancelQualificationTestSpecificationType newCancelQualificationTestSpecification) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_CancelQualificationTestSpecification(), newCancelQualificationTestSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEquipmentType getChangeEquipment() {
		return (ChangeEquipmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeEquipment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeEquipment(ChangeEquipmentType newChangeEquipment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeEquipment(), newChangeEquipment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeEquipment(ChangeEquipmentType newChangeEquipment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeEquipment(), newChangeEquipment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEquipmentCapabilityTestSpecType getChangeEquipmentCapabilityTestSpec() {
		return (ChangeEquipmentCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeEquipmentCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeEquipmentCapabilityTestSpec(ChangeEquipmentCapabilityTestSpecType newChangeEquipmentCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeEquipmentCapabilityTestSpec(), newChangeEquipmentCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeEquipmentCapabilityTestSpec(ChangeEquipmentCapabilityTestSpecType newChangeEquipmentCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeEquipmentCapabilityTestSpec(), newChangeEquipmentCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEquipmentClassType getChangeEquipmentClass() {
		return (ChangeEquipmentClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeEquipmentClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeEquipmentClass(ChangeEquipmentClassType newChangeEquipmentClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeEquipmentClass(), newChangeEquipmentClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeEquipmentClass(ChangeEquipmentClassType newChangeEquipmentClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeEquipmentClass(), newChangeEquipmentClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeEquipmentInformationType getChangeEquipmentInformation() {
		return (ChangeEquipmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeEquipmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeEquipmentInformation(ChangeEquipmentInformationType newChangeEquipmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeEquipmentInformation(), newChangeEquipmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeEquipmentInformation(ChangeEquipmentInformationType newChangeEquipmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeEquipmentInformation(), newChangeEquipmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeMaterialClassType getChangeMaterialClass() {
		return (ChangeMaterialClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeMaterialClass(ChangeMaterialClassType newChangeMaterialClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialClass(), newChangeMaterialClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeMaterialClass(ChangeMaterialClassType newChangeMaterialClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialClass(), newChangeMaterialClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeMaterialDefinitionType getChangeMaterialDefinition() {
		return (ChangeMaterialDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeMaterialDefinition(ChangeMaterialDefinitionType newChangeMaterialDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialDefinition(), newChangeMaterialDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeMaterialDefinition(ChangeMaterialDefinitionType newChangeMaterialDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialDefinition(), newChangeMaterialDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeMaterialInformationType getChangeMaterialInformation() {
		return (ChangeMaterialInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeMaterialInformation(ChangeMaterialInformationType newChangeMaterialInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialInformation(), newChangeMaterialInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeMaterialInformation(ChangeMaterialInformationType newChangeMaterialInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialInformation(), newChangeMaterialInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeMaterialLotType getChangeMaterialLot() {
		return (ChangeMaterialLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeMaterialLot(ChangeMaterialLotType newChangeMaterialLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialLot(), newChangeMaterialLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeMaterialLot(ChangeMaterialLotType newChangeMaterialLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialLot(), newChangeMaterialLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeMaterialSubLotType getChangeMaterialSubLot() {
		return (ChangeMaterialSubLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialSubLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeMaterialSubLot(ChangeMaterialSubLotType newChangeMaterialSubLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialSubLot(), newChangeMaterialSubLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeMaterialSubLot(ChangeMaterialSubLotType newChangeMaterialSubLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialSubLot(), newChangeMaterialSubLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeMaterialTestSpecType getChangeMaterialTestSpec() {
		return (ChangeMaterialTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeMaterialTestSpec(ChangeMaterialTestSpecType newChangeMaterialTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialTestSpec(), newChangeMaterialTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeMaterialTestSpec(ChangeMaterialTestSpecType newChangeMaterialTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeMaterialTestSpec(), newChangeMaterialTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeOperationsCapabilityType getChangeOperationsCapability() {
		return (ChangeOperationsCapabilityType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsCapability(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeOperationsCapability(ChangeOperationsCapabilityType newChangeOperationsCapability, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsCapability(), newChangeOperationsCapability, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeOperationsCapability(ChangeOperationsCapabilityType newChangeOperationsCapability) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsCapability(), newChangeOperationsCapability);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeOperationsCapabilityInformationType getChangeOperationsCapabilityInformation() {
		return (ChangeOperationsCapabilityInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsCapabilityInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeOperationsCapabilityInformation(ChangeOperationsCapabilityInformationType newChangeOperationsCapabilityInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsCapabilityInformation(), newChangeOperationsCapabilityInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeOperationsCapabilityInformation(ChangeOperationsCapabilityInformationType newChangeOperationsCapabilityInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsCapabilityInformation(), newChangeOperationsCapabilityInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeOperationsDefinitionType getChangeOperationsDefinition() {
		return (ChangeOperationsDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeOperationsDefinition(ChangeOperationsDefinitionType newChangeOperationsDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsDefinition(), newChangeOperationsDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeOperationsDefinition(ChangeOperationsDefinitionType newChangeOperationsDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsDefinition(), newChangeOperationsDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeOperationsDefinitionInformationType getChangeOperationsDefinitionInformation() {
		return (ChangeOperationsDefinitionInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsDefinitionInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeOperationsDefinitionInformation(ChangeOperationsDefinitionInformationType newChangeOperationsDefinitionInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsDefinitionInformation(), newChangeOperationsDefinitionInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeOperationsDefinitionInformation(ChangeOperationsDefinitionInformationType newChangeOperationsDefinitionInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsDefinitionInformation(), newChangeOperationsDefinitionInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeOperationsPerformanceType getChangeOperationsPerformance() {
		return (ChangeOperationsPerformanceType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsPerformance(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeOperationsPerformance(ChangeOperationsPerformanceType newChangeOperationsPerformance, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsPerformance(), newChangeOperationsPerformance, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeOperationsPerformance(ChangeOperationsPerformanceType newChangeOperationsPerformance) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsPerformance(), newChangeOperationsPerformance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeOperationsScheduleType getChangeOperationsSchedule() {
		return (ChangeOperationsScheduleType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsSchedule(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeOperationsSchedule(ChangeOperationsScheduleType newChangeOperationsSchedule, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsSchedule(), newChangeOperationsSchedule, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeOperationsSchedule(ChangeOperationsScheduleType newChangeOperationsSchedule) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeOperationsSchedule(), newChangeOperationsSchedule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePersonType getChangePerson() {
		return (ChangePersonType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePerson(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangePerson(ChangePersonType newChangePerson, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePerson(), newChangePerson, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangePerson(ChangePersonType newChangePerson) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePerson(), newChangePerson);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePersonnelClassType getChangePersonnelClass() {
		return (ChangePersonnelClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePersonnelClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangePersonnelClass(ChangePersonnelClassType newChangePersonnelClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePersonnelClass(), newChangePersonnelClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangePersonnelClass(ChangePersonnelClassType newChangePersonnelClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePersonnelClass(), newChangePersonnelClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePersonnelInformationType getChangePersonnelInformation() {
		return (ChangePersonnelInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePersonnelInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangePersonnelInformation(ChangePersonnelInformationType newChangePersonnelInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePersonnelInformation(), newChangePersonnelInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangePersonnelInformation(ChangePersonnelInformationType newChangePersonnelInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePersonnelInformation(), newChangePersonnelInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePhysicalAssetType getChangePhysicalAsset() {
		return (ChangePhysicalAssetType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePhysicalAsset(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangePhysicalAsset(ChangePhysicalAssetType newChangePhysicalAsset, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePhysicalAsset(), newChangePhysicalAsset, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangePhysicalAsset(ChangePhysicalAssetType newChangePhysicalAsset) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePhysicalAsset(), newChangePhysicalAsset);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePhysicalAssetCapabilityTestSpecType getChangePhysicalAssetCapabilityTestSpec() {
		return (ChangePhysicalAssetCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePhysicalAssetCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangePhysicalAssetCapabilityTestSpec(ChangePhysicalAssetCapabilityTestSpecType newChangePhysicalAssetCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePhysicalAssetCapabilityTestSpec(), newChangePhysicalAssetCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangePhysicalAssetCapabilityTestSpec(ChangePhysicalAssetCapabilityTestSpecType newChangePhysicalAssetCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePhysicalAssetCapabilityTestSpec(), newChangePhysicalAssetCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePhysicalAssetClassType getChangePhysicalAssetClass() {
		return (ChangePhysicalAssetClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePhysicalAssetClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangePhysicalAssetClass(ChangePhysicalAssetClassType newChangePhysicalAssetClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePhysicalAssetClass(), newChangePhysicalAssetClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangePhysicalAssetClass(ChangePhysicalAssetClassType newChangePhysicalAssetClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePhysicalAssetClass(), newChangePhysicalAssetClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangePhysicalAssetInformationType getChangePhysicalAssetInformation() {
		return (ChangePhysicalAssetInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePhysicalAssetInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangePhysicalAssetInformation(ChangePhysicalAssetInformationType newChangePhysicalAssetInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePhysicalAssetInformation(), newChangePhysicalAssetInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangePhysicalAssetInformation(ChangePhysicalAssetInformationType newChangePhysicalAssetInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangePhysicalAssetInformation(), newChangePhysicalAssetInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeProcessSegmentType getChangeProcessSegment() {
		return (ChangeProcessSegmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeProcessSegment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeProcessSegment(ChangeProcessSegmentType newChangeProcessSegment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeProcessSegment(), newChangeProcessSegment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeProcessSegment(ChangeProcessSegmentType newChangeProcessSegment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeProcessSegment(), newChangeProcessSegment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeProcessSegmentInformationType getChangeProcessSegmentInformation() {
		return (ChangeProcessSegmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeProcessSegmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeProcessSegmentInformation(ChangeProcessSegmentInformationType newChangeProcessSegmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeProcessSegmentInformation(), newChangeProcessSegmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeProcessSegmentInformation(ChangeProcessSegmentInformationType newChangeProcessSegmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeProcessSegmentInformation(), newChangeProcessSegmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeQualificationTestSpecificationType getChangeQualificationTestSpecification() {
		return (ChangeQualificationTestSpecificationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeQualificationTestSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeQualificationTestSpecification(ChangeQualificationTestSpecificationType newChangeQualificationTestSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeQualificationTestSpecification(), newChangeQualificationTestSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeQualificationTestSpecification(ChangeQualificationTestSpecificationType newChangeQualificationTestSpecification) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ChangeQualificationTestSpecification(), newChangeQualificationTestSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfirmBODType getConfirmBOD() {
		return (ConfirmBODType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ConfirmBOD(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConfirmBOD(ConfirmBODType newConfirmBOD, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ConfirmBOD(), newConfirmBOD, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfirmBOD(ConfirmBODType newConfirmBOD) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ConfirmBOD(), newConfirmBOD);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentType getEquipment() {
		return (EquipmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_Equipment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipment(EquipmentType newEquipment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_Equipment(), newEquipment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipment(EquipmentType newEquipment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_Equipment(), newEquipment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentCapabilityTestSpecificationType getEquipmentCapabilityTestSpecification() {
		return (EquipmentCapabilityTestSpecificationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_EquipmentCapabilityTestSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentCapabilityTestSpecification(EquipmentCapabilityTestSpecificationType newEquipmentCapabilityTestSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_EquipmentCapabilityTestSpecification(), newEquipmentCapabilityTestSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentCapabilityTestSpecification(EquipmentCapabilityTestSpecificationType newEquipmentCapabilityTestSpecification) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_EquipmentCapabilityTestSpecification(), newEquipmentCapabilityTestSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentClassType getEquipmentClass() {
		return (EquipmentClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_EquipmentClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentClass(EquipmentClassType newEquipmentClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_EquipmentClass(), newEquipmentClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentClass(EquipmentClassType newEquipmentClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_EquipmentClass(), newEquipmentClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentInformationType getEquipmentInformation() {
		return (EquipmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_EquipmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentInformation(EquipmentInformationType newEquipmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_EquipmentInformation(), newEquipmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentInformation(EquipmentInformationType newEquipmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_EquipmentInformation(), newEquipmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetEquipmentType getGetEquipment() {
		return (GetEquipmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetEquipment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetEquipment(GetEquipmentType newGetEquipment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetEquipment(), newGetEquipment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetEquipment(GetEquipmentType newGetEquipment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetEquipment(), newGetEquipment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetEquipmentCapabilityTestSpecType getGetEquipmentCapabilityTestSpec() {
		return (GetEquipmentCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetEquipmentCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetEquipmentCapabilityTestSpec(GetEquipmentCapabilityTestSpecType newGetEquipmentCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetEquipmentCapabilityTestSpec(), newGetEquipmentCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetEquipmentCapabilityTestSpec(GetEquipmentCapabilityTestSpecType newGetEquipmentCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetEquipmentCapabilityTestSpec(), newGetEquipmentCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetEquipmentClassType getGetEquipmentClass() {
		return (GetEquipmentClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetEquipmentClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetEquipmentClass(GetEquipmentClassType newGetEquipmentClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetEquipmentClass(), newGetEquipmentClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetEquipmentClass(GetEquipmentClassType newGetEquipmentClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetEquipmentClass(), newGetEquipmentClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetEquipmentInformationType getGetEquipmentInformation() {
		return (GetEquipmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetEquipmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetEquipmentInformation(GetEquipmentInformationType newGetEquipmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetEquipmentInformation(), newGetEquipmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetEquipmentInformation(GetEquipmentInformationType newGetEquipmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetEquipmentInformation(), newGetEquipmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetMaterialClassType getGetMaterialClass() {
		return (GetMaterialClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetMaterialClass(GetMaterialClassType newGetMaterialClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialClass(), newGetMaterialClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetMaterialClass(GetMaterialClassType newGetMaterialClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialClass(), newGetMaterialClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetMaterialDefinitionType getGetMaterialDefinition() {
		return (GetMaterialDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetMaterialDefinition(GetMaterialDefinitionType newGetMaterialDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialDefinition(), newGetMaterialDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetMaterialDefinition(GetMaterialDefinitionType newGetMaterialDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialDefinition(), newGetMaterialDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetMaterialInformationType getGetMaterialInformation() {
		return (GetMaterialInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetMaterialInformation(GetMaterialInformationType newGetMaterialInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialInformation(), newGetMaterialInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetMaterialInformation(GetMaterialInformationType newGetMaterialInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialInformation(), newGetMaterialInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetMaterialLotType getGetMaterialLot() {
		return (GetMaterialLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetMaterialLot(GetMaterialLotType newGetMaterialLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialLot(), newGetMaterialLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetMaterialLot(GetMaterialLotType newGetMaterialLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialLot(), newGetMaterialLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetMaterialSubLotType getGetMaterialSubLot() {
		return (GetMaterialSubLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialSubLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetMaterialSubLot(GetMaterialSubLotType newGetMaterialSubLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialSubLot(), newGetMaterialSubLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetMaterialSubLot(GetMaterialSubLotType newGetMaterialSubLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialSubLot(), newGetMaterialSubLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetMaterialTestSpecType getGetMaterialTestSpec() {
		return (GetMaterialTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetMaterialTestSpec(GetMaterialTestSpecType newGetMaterialTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialTestSpec(), newGetMaterialTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetMaterialTestSpec(GetMaterialTestSpecType newGetMaterialTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetMaterialTestSpec(), newGetMaterialTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetOperationsCapabilityType getGetOperationsCapability() {
		return (GetOperationsCapabilityType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsCapability(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetOperationsCapability(GetOperationsCapabilityType newGetOperationsCapability, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsCapability(), newGetOperationsCapability, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetOperationsCapability(GetOperationsCapabilityType newGetOperationsCapability) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsCapability(), newGetOperationsCapability);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetOperationsCapabilityInformationType getGetOperationsCapabilityInformation() {
		return (GetOperationsCapabilityInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsCapabilityInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetOperationsCapabilityInformation(GetOperationsCapabilityInformationType newGetOperationsCapabilityInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsCapabilityInformation(), newGetOperationsCapabilityInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetOperationsCapabilityInformation(GetOperationsCapabilityInformationType newGetOperationsCapabilityInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsCapabilityInformation(), newGetOperationsCapabilityInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetOperationsDefinitionType getGetOperationsDefinition() {
		return (GetOperationsDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetOperationsDefinition(GetOperationsDefinitionType newGetOperationsDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsDefinition(), newGetOperationsDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetOperationsDefinition(GetOperationsDefinitionType newGetOperationsDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsDefinition(), newGetOperationsDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetOperationsDefinitionInformationType getGetOperationsDefinitionInformation() {
		return (GetOperationsDefinitionInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsDefinitionInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetOperationsDefinitionInformation(GetOperationsDefinitionInformationType newGetOperationsDefinitionInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsDefinitionInformation(), newGetOperationsDefinitionInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetOperationsDefinitionInformation(GetOperationsDefinitionInformationType newGetOperationsDefinitionInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsDefinitionInformation(), newGetOperationsDefinitionInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetOperationsPerformanceType getGetOperationsPerformance() {
		return (GetOperationsPerformanceType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsPerformance(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetOperationsPerformance(GetOperationsPerformanceType newGetOperationsPerformance, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsPerformance(), newGetOperationsPerformance, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetOperationsPerformance(GetOperationsPerformanceType newGetOperationsPerformance) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsPerformance(), newGetOperationsPerformance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetOperationsScheduleType getGetOperationsSchedule() {
		return (GetOperationsScheduleType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsSchedule(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetOperationsSchedule(GetOperationsScheduleType newGetOperationsSchedule, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsSchedule(), newGetOperationsSchedule, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetOperationsSchedule(GetOperationsScheduleType newGetOperationsSchedule) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetOperationsSchedule(), newGetOperationsSchedule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPersonType getGetPerson() {
		return (GetPersonType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPerson(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetPerson(GetPersonType newGetPerson, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPerson(), newGetPerson, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetPerson(GetPersonType newGetPerson) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPerson(), newGetPerson);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPersonnelClassType getGetPersonnelClass() {
		return (GetPersonnelClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPersonnelClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetPersonnelClass(GetPersonnelClassType newGetPersonnelClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPersonnelClass(), newGetPersonnelClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetPersonnelClass(GetPersonnelClassType newGetPersonnelClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPersonnelClass(), newGetPersonnelClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPersonnelInformationType getGetPersonnelInformation() {
		return (GetPersonnelInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPersonnelInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetPersonnelInformation(GetPersonnelInformationType newGetPersonnelInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPersonnelInformation(), newGetPersonnelInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetPersonnelInformation(GetPersonnelInformationType newGetPersonnelInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPersonnelInformation(), newGetPersonnelInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPhysicalAssetType getGetPhysicalAsset() {
		return (GetPhysicalAssetType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPhysicalAsset(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetPhysicalAsset(GetPhysicalAssetType newGetPhysicalAsset, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPhysicalAsset(), newGetPhysicalAsset, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetPhysicalAsset(GetPhysicalAssetType newGetPhysicalAsset) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPhysicalAsset(), newGetPhysicalAsset);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPhysicalAssetCapabilityTestSpecType getGetPhysicalAssetCapabilityTestSpec() {
		return (GetPhysicalAssetCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPhysicalAssetCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetPhysicalAssetCapabilityTestSpec(GetPhysicalAssetCapabilityTestSpecType newGetPhysicalAssetCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPhysicalAssetCapabilityTestSpec(), newGetPhysicalAssetCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetPhysicalAssetCapabilityTestSpec(GetPhysicalAssetCapabilityTestSpecType newGetPhysicalAssetCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPhysicalAssetCapabilityTestSpec(), newGetPhysicalAssetCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPhysicalAssetClassType getGetPhysicalAssetClass() {
		return (GetPhysicalAssetClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPhysicalAssetClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetPhysicalAssetClass(GetPhysicalAssetClassType newGetPhysicalAssetClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPhysicalAssetClass(), newGetPhysicalAssetClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetPhysicalAssetClass(GetPhysicalAssetClassType newGetPhysicalAssetClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPhysicalAssetClass(), newGetPhysicalAssetClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPhysicalAssetInformationType getGetPhysicalAssetInformation() {
		return (GetPhysicalAssetInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPhysicalAssetInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetPhysicalAssetInformation(GetPhysicalAssetInformationType newGetPhysicalAssetInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPhysicalAssetInformation(), newGetPhysicalAssetInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetPhysicalAssetInformation(GetPhysicalAssetInformationType newGetPhysicalAssetInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetPhysicalAssetInformation(), newGetPhysicalAssetInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetProcessSegmentType getGetProcessSegment() {
		return (GetProcessSegmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetProcessSegment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetProcessSegment(GetProcessSegmentType newGetProcessSegment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetProcessSegment(), newGetProcessSegment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetProcessSegment(GetProcessSegmentType newGetProcessSegment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetProcessSegment(), newGetProcessSegment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetProcessSegmentInformationType getGetProcessSegmentInformation() {
		return (GetProcessSegmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetProcessSegmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetProcessSegmentInformation(GetProcessSegmentInformationType newGetProcessSegmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetProcessSegmentInformation(), newGetProcessSegmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetProcessSegmentInformation(GetProcessSegmentInformationType newGetProcessSegmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetProcessSegmentInformation(), newGetProcessSegmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetQualificationTestSpecificationType getGetQualificationTestSpecification() {
		return (GetQualificationTestSpecificationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_GetQualificationTestSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGetQualificationTestSpecification(GetQualificationTestSpecificationType newGetQualificationTestSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_GetQualificationTestSpecification(), newGetQualificationTestSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetQualificationTestSpecification(GetQualificationTestSpecificationType newGetQualificationTestSpecification) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_GetQualificationTestSpecification(), newGetQualificationTestSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialClassType getMaterialClass() {
		return (MaterialClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialClass(MaterialClassType newMaterialClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialClass(), newMaterialClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialClass(MaterialClassType newMaterialClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialClass(), newMaterialClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialDefinitionType getMaterialDefinition() {
		return (MaterialDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialDefinition(MaterialDefinitionType newMaterialDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialDefinition(), newMaterialDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialDefinition(MaterialDefinitionType newMaterialDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialDefinition(), newMaterialDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialInformationType getMaterialInformation() {
		return (MaterialInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialInformation(MaterialInformationType newMaterialInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialInformation(), newMaterialInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialInformation(MaterialInformationType newMaterialInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialInformation(), newMaterialInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialLotType getMaterialLot() {
		return (MaterialLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialLot(MaterialLotType newMaterialLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialLot(), newMaterialLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialLot(MaterialLotType newMaterialLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialLot(), newMaterialLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialSubLotType getMaterialSubLot() {
		return (MaterialSubLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialSubLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialSubLot(MaterialSubLotType newMaterialSubLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialSubLot(), newMaterialSubLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialSubLot(MaterialSubLotType newMaterialSubLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialSubLot(), newMaterialSubLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialTestSpecificationType getMaterialTestSpecification() {
		return (MaterialTestSpecificationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialTestSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialTestSpecification(MaterialTestSpecificationType newMaterialTestSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialTestSpecification(), newMaterialTestSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialTestSpecification(MaterialTestSpecificationType newMaterialTestSpecification) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_MaterialTestSpecification(), newMaterialTestSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsCapabilityType getOperationsCapability() {
		return (OperationsCapabilityType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsCapability(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsCapability(OperationsCapabilityType newOperationsCapability, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsCapability(), newOperationsCapability, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsCapability(OperationsCapabilityType newOperationsCapability) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsCapability(), newOperationsCapability);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsCapabilityInformationType getOperationsCapabilityInformation() {
		return (OperationsCapabilityInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsCapabilityInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsCapabilityInformation(OperationsCapabilityInformationType newOperationsCapabilityInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsCapabilityInformation(), newOperationsCapabilityInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsCapabilityInformation(OperationsCapabilityInformationType newOperationsCapabilityInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsCapabilityInformation(), newOperationsCapabilityInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinitionType getOperationsDefinition() {
		return (OperationsDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsDefinition(OperationsDefinitionType newOperationsDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsDefinition(), newOperationsDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsDefinition(OperationsDefinitionType newOperationsDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsDefinition(), newOperationsDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsDefinitionInformationType getOperationsDefinitionInformation() {
		return (OperationsDefinitionInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsDefinitionInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsDefinitionInformation(OperationsDefinitionInformationType newOperationsDefinitionInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsDefinitionInformation(), newOperationsDefinitionInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsDefinitionInformation(OperationsDefinitionInformationType newOperationsDefinitionInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsDefinitionInformation(), newOperationsDefinitionInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsPerformanceType getOperationsPerformance() {
		return (OperationsPerformanceType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsPerformance(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsPerformance(OperationsPerformanceType newOperationsPerformance, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsPerformance(), newOperationsPerformance, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsPerformance(OperationsPerformanceType newOperationsPerformance) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsPerformance(), newOperationsPerformance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsRequestType getOperationsRequest() {
		return (OperationsRequestType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsRequest(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsRequest(OperationsRequestType newOperationsRequest, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsRequest(), newOperationsRequest, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsRequest(OperationsRequestType newOperationsRequest) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsRequest(), newOperationsRequest);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsResponseType getOperationsResponse() {
		return (OperationsResponseType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsResponse(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsResponse(OperationsResponseType newOperationsResponse, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsResponse(), newOperationsResponse, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsResponse(OperationsResponseType newOperationsResponse) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsResponse(), newOperationsResponse);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsScheduleType getOperationsSchedule() {
		return (OperationsScheduleType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsSchedule(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsSchedule(OperationsScheduleType newOperationsSchedule, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsSchedule(), newOperationsSchedule, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsSchedule(OperationsScheduleType newOperationsSchedule) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_OperationsSchedule(), newOperationsSchedule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonType getPerson() {
		return (PersonType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_Person(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPerson(PersonType newPerson, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_Person(), newPerson, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPerson(PersonType newPerson) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_Person(), newPerson);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelClassType getPersonnelClass() {
		return (PersonnelClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_PersonnelClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPersonnelClass(PersonnelClassType newPersonnelClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_PersonnelClass(), newPersonnelClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonnelClass(PersonnelClassType newPersonnelClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_PersonnelClass(), newPersonnelClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelInformationType getPersonnelInformation() {
		return (PersonnelInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_PersonnelInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPersonnelInformation(PersonnelInformationType newPersonnelInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_PersonnelInformation(), newPersonnelInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonnelInformation(PersonnelInformationType newPersonnelInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_PersonnelInformation(), newPersonnelInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetType getPhysicalAsset() {
		return (PhysicalAssetType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_PhysicalAsset(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPhysicalAsset(PhysicalAssetType newPhysicalAsset, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_PhysicalAsset(), newPhysicalAsset, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAsset(PhysicalAssetType newPhysicalAsset) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_PhysicalAsset(), newPhysicalAsset);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetCapabilityTestSpecificationType getPhysicalAssetCapabilityTestSpecification() {
		return (PhysicalAssetCapabilityTestSpecificationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_PhysicalAssetCapabilityTestSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPhysicalAssetCapabilityTestSpecification(PhysicalAssetCapabilityTestSpecificationType newPhysicalAssetCapabilityTestSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_PhysicalAssetCapabilityTestSpecification(), newPhysicalAssetCapabilityTestSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetCapabilityTestSpecification(PhysicalAssetCapabilityTestSpecificationType newPhysicalAssetCapabilityTestSpecification) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_PhysicalAssetCapabilityTestSpecification(), newPhysicalAssetCapabilityTestSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClassType getPhysicalAssetClass() {
		return (PhysicalAssetClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_PhysicalAssetClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPhysicalAssetClass(PhysicalAssetClassType newPhysicalAssetClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_PhysicalAssetClass(), newPhysicalAssetClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetClass(PhysicalAssetClassType newPhysicalAssetClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_PhysicalAssetClass(), newPhysicalAssetClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetInformationType getPhysicalAssetInformation() {
		return (PhysicalAssetInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_PhysicalAssetInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPhysicalAssetInformation(PhysicalAssetInformationType newPhysicalAssetInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_PhysicalAssetInformation(), newPhysicalAssetInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetInformation(PhysicalAssetInformationType newPhysicalAssetInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_PhysicalAssetInformation(), newPhysicalAssetInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessEquipmentType getProcessEquipment() {
		return (ProcessEquipmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessEquipment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessEquipment(ProcessEquipmentType newProcessEquipment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessEquipment(), newProcessEquipment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessEquipment(ProcessEquipmentType newProcessEquipment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessEquipment(), newProcessEquipment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessEquipmentCapabilityTestSpecType getProcessEquipmentCapabilityTestSpec() {
		return (ProcessEquipmentCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessEquipmentCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessEquipmentCapabilityTestSpec(ProcessEquipmentCapabilityTestSpecType newProcessEquipmentCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessEquipmentCapabilityTestSpec(), newProcessEquipmentCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessEquipmentCapabilityTestSpec(ProcessEquipmentCapabilityTestSpecType newProcessEquipmentCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessEquipmentCapabilityTestSpec(), newProcessEquipmentCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessEquipmentClassType getProcessEquipmentClass() {
		return (ProcessEquipmentClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessEquipmentClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessEquipmentClass(ProcessEquipmentClassType newProcessEquipmentClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessEquipmentClass(), newProcessEquipmentClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessEquipmentClass(ProcessEquipmentClassType newProcessEquipmentClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessEquipmentClass(), newProcessEquipmentClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessEquipmentInformationType getProcessEquipmentInformation() {
		return (ProcessEquipmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessEquipmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessEquipmentInformation(ProcessEquipmentInformationType newProcessEquipmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessEquipmentInformation(), newProcessEquipmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessEquipmentInformation(ProcessEquipmentInformationType newProcessEquipmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessEquipmentInformation(), newProcessEquipmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessMaterialClassType getProcessMaterialClass() {
		return (ProcessMaterialClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessMaterialClass(ProcessMaterialClassType newProcessMaterialClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialClass(), newProcessMaterialClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessMaterialClass(ProcessMaterialClassType newProcessMaterialClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialClass(), newProcessMaterialClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessMaterialDefinitionType getProcessMaterialDefinition() {
		return (ProcessMaterialDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessMaterialDefinition(ProcessMaterialDefinitionType newProcessMaterialDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialDefinition(), newProcessMaterialDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessMaterialDefinition(ProcessMaterialDefinitionType newProcessMaterialDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialDefinition(), newProcessMaterialDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessMaterialInformationType getProcessMaterialInformation() {
		return (ProcessMaterialInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessMaterialInformation(ProcessMaterialInformationType newProcessMaterialInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialInformation(), newProcessMaterialInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessMaterialInformation(ProcessMaterialInformationType newProcessMaterialInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialInformation(), newProcessMaterialInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessMaterialLotType getProcessMaterialLot() {
		return (ProcessMaterialLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessMaterialLot(ProcessMaterialLotType newProcessMaterialLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialLot(), newProcessMaterialLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessMaterialLot(ProcessMaterialLotType newProcessMaterialLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialLot(), newProcessMaterialLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessMaterialSubLotType getProcessMaterialSubLot() {
		return (ProcessMaterialSubLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialSubLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessMaterialSubLot(ProcessMaterialSubLotType newProcessMaterialSubLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialSubLot(), newProcessMaterialSubLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessMaterialSubLot(ProcessMaterialSubLotType newProcessMaterialSubLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialSubLot(), newProcessMaterialSubLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessMaterialTestSpecType getProcessMaterialTestSpec() {
		return (ProcessMaterialTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessMaterialTestSpec(ProcessMaterialTestSpecType newProcessMaterialTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialTestSpec(), newProcessMaterialTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessMaterialTestSpec(ProcessMaterialTestSpecType newProcessMaterialTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessMaterialTestSpec(), newProcessMaterialTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessOperationsCapabilityType getProcessOperationsCapability() {
		return (ProcessOperationsCapabilityType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsCapability(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessOperationsCapability(ProcessOperationsCapabilityType newProcessOperationsCapability, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsCapability(), newProcessOperationsCapability, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessOperationsCapability(ProcessOperationsCapabilityType newProcessOperationsCapability) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsCapability(), newProcessOperationsCapability);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessOperationsCapabilityInformationType getProcessOperationsCapabilityInformation() {
		return (ProcessOperationsCapabilityInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsCapabilityInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessOperationsCapabilityInformation(ProcessOperationsCapabilityInformationType newProcessOperationsCapabilityInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsCapabilityInformation(), newProcessOperationsCapabilityInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessOperationsCapabilityInformation(ProcessOperationsCapabilityInformationType newProcessOperationsCapabilityInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsCapabilityInformation(), newProcessOperationsCapabilityInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessOperationsDefinitionType getProcessOperationsDefinition() {
		return (ProcessOperationsDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessOperationsDefinition(ProcessOperationsDefinitionType newProcessOperationsDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsDefinition(), newProcessOperationsDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessOperationsDefinition(ProcessOperationsDefinitionType newProcessOperationsDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsDefinition(), newProcessOperationsDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessOperationsDefinitionInformationType getProcessOperationsDefinitionInformation() {
		return (ProcessOperationsDefinitionInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsDefinitionInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessOperationsDefinitionInformation(ProcessOperationsDefinitionInformationType newProcessOperationsDefinitionInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsDefinitionInformation(), newProcessOperationsDefinitionInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessOperationsDefinitionInformation(ProcessOperationsDefinitionInformationType newProcessOperationsDefinitionInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsDefinitionInformation(), newProcessOperationsDefinitionInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessOperationsPerformanceType getProcessOperationsPerformance() {
		return (ProcessOperationsPerformanceType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsPerformance(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessOperationsPerformance(ProcessOperationsPerformanceType newProcessOperationsPerformance, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsPerformance(), newProcessOperationsPerformance, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessOperationsPerformance(ProcessOperationsPerformanceType newProcessOperationsPerformance) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsPerformance(), newProcessOperationsPerformance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessOperationsScheduleType getProcessOperationsSchedule() {
		return (ProcessOperationsScheduleType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsSchedule(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessOperationsSchedule(ProcessOperationsScheduleType newProcessOperationsSchedule, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsSchedule(), newProcessOperationsSchedule, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessOperationsSchedule(ProcessOperationsScheduleType newProcessOperationsSchedule) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessOperationsSchedule(), newProcessOperationsSchedule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPersonType getProcessPerson() {
		return (ProcessPersonType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPerson(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessPerson(ProcessPersonType newProcessPerson, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPerson(), newProcessPerson, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessPerson(ProcessPersonType newProcessPerson) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPerson(), newProcessPerson);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPersonnelClassType getProcessPersonnelClass() {
		return (ProcessPersonnelClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPersonnelClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessPersonnelClass(ProcessPersonnelClassType newProcessPersonnelClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPersonnelClass(), newProcessPersonnelClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessPersonnelClass(ProcessPersonnelClassType newProcessPersonnelClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPersonnelClass(), newProcessPersonnelClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPersonnelInformationType getProcessPersonnelInformation() {
		return (ProcessPersonnelInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPersonnelInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessPersonnelInformation(ProcessPersonnelInformationType newProcessPersonnelInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPersonnelInformation(), newProcessPersonnelInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessPersonnelInformation(ProcessPersonnelInformationType newProcessPersonnelInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPersonnelInformation(), newProcessPersonnelInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPhysicalAssetType getProcessPhysicalAsset() {
		return (ProcessPhysicalAssetType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPhysicalAsset(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessPhysicalAsset(ProcessPhysicalAssetType newProcessPhysicalAsset, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPhysicalAsset(), newProcessPhysicalAsset, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessPhysicalAsset(ProcessPhysicalAssetType newProcessPhysicalAsset) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPhysicalAsset(), newProcessPhysicalAsset);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPhysicalAssetCapabilityTestSpecType getProcessPhysicalAssetCapabilityTestSpec() {
		return (ProcessPhysicalAssetCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPhysicalAssetCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessPhysicalAssetCapabilityTestSpec(ProcessPhysicalAssetCapabilityTestSpecType newProcessPhysicalAssetCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPhysicalAssetCapabilityTestSpec(), newProcessPhysicalAssetCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessPhysicalAssetCapabilityTestSpec(ProcessPhysicalAssetCapabilityTestSpecType newProcessPhysicalAssetCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPhysicalAssetCapabilityTestSpec(), newProcessPhysicalAssetCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPhysicalAssetClassType getProcessPhysicalAssetClass() {
		return (ProcessPhysicalAssetClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPhysicalAssetClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessPhysicalAssetClass(ProcessPhysicalAssetClassType newProcessPhysicalAssetClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPhysicalAssetClass(), newProcessPhysicalAssetClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessPhysicalAssetClass(ProcessPhysicalAssetClassType newProcessPhysicalAssetClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPhysicalAssetClass(), newProcessPhysicalAssetClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessPhysicalAssetInformationType getProcessPhysicalAssetInformation() {
		return (ProcessPhysicalAssetInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPhysicalAssetInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessPhysicalAssetInformation(ProcessPhysicalAssetInformationType newProcessPhysicalAssetInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPhysicalAssetInformation(), newProcessPhysicalAssetInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessPhysicalAssetInformation(ProcessPhysicalAssetInformationType newProcessPhysicalAssetInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessPhysicalAssetInformation(), newProcessPhysicalAssetInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessProcessSegmentType getProcessProcessSegment() {
		return (ProcessProcessSegmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessProcessSegment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessProcessSegment(ProcessProcessSegmentType newProcessProcessSegment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessProcessSegment(), newProcessProcessSegment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessProcessSegment(ProcessProcessSegmentType newProcessProcessSegment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessProcessSegment(), newProcessProcessSegment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessProcessSegmentInformationType getProcessProcessSegmentInformation() {
		return (ProcessProcessSegmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessProcessSegmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessProcessSegmentInformation(ProcessProcessSegmentInformationType newProcessProcessSegmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessProcessSegmentInformation(), newProcessProcessSegmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessProcessSegmentInformation(ProcessProcessSegmentInformationType newProcessProcessSegmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessProcessSegmentInformation(), newProcessProcessSegmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessQualificationTestSpecificationType getProcessQualificationTestSpecification() {
		return (ProcessQualificationTestSpecificationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessQualificationTestSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessQualificationTestSpecification(ProcessQualificationTestSpecificationType newProcessQualificationTestSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessQualificationTestSpecification(), newProcessQualificationTestSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessQualificationTestSpecification(ProcessQualificationTestSpecificationType newProcessQualificationTestSpecification) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessQualificationTestSpecification(), newProcessQualificationTestSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentType getProcessSegment() {
		return (ProcessSegmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessSegment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessSegment(ProcessSegmentType newProcessSegment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessSegment(), newProcessSegment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessSegment(ProcessSegmentType newProcessSegment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessSegment(), newProcessSegment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessSegmentInformationType getProcessSegmentInformation() {
		return (ProcessSegmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessSegmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessSegmentInformation(ProcessSegmentInformationType newProcessSegmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessSegmentInformation(), newProcessSegmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessSegmentInformation(ProcessSegmentInformationType newProcessSegmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ProcessSegmentInformation(), newProcessSegmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualificationTestSpecificationType getQualificationTestSpecification() {
		return (QualificationTestSpecificationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_QualificationTestSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetQualificationTestSpecification(QualificationTestSpecificationType newQualificationTestSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_QualificationTestSpecification(), newQualificationTestSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQualificationTestSpecification(QualificationTestSpecificationType newQualificationTestSpecification) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_QualificationTestSpecification(), newQualificationTestSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondEquipmentType getRespondEquipment() {
		return (RespondEquipmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondEquipment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondEquipment(RespondEquipmentType newRespondEquipment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondEquipment(), newRespondEquipment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondEquipment(RespondEquipmentType newRespondEquipment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondEquipment(), newRespondEquipment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondEquipmentCapabilityTestSpecType getRespondEquipmentCapabilityTestSpec() {
		return (RespondEquipmentCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondEquipmentCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondEquipmentCapabilityTestSpec(RespondEquipmentCapabilityTestSpecType newRespondEquipmentCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondEquipmentCapabilityTestSpec(), newRespondEquipmentCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondEquipmentCapabilityTestSpec(RespondEquipmentCapabilityTestSpecType newRespondEquipmentCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondEquipmentCapabilityTestSpec(), newRespondEquipmentCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondEquipmentClassType getRespondEquipmentClass() {
		return (RespondEquipmentClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondEquipmentClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondEquipmentClass(RespondEquipmentClassType newRespondEquipmentClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondEquipmentClass(), newRespondEquipmentClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondEquipmentClass(RespondEquipmentClassType newRespondEquipmentClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondEquipmentClass(), newRespondEquipmentClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondEquipmentInformationType getRespondEquipmentInformation() {
		return (RespondEquipmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondEquipmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondEquipmentInformation(RespondEquipmentInformationType newRespondEquipmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondEquipmentInformation(), newRespondEquipmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondEquipmentInformation(RespondEquipmentInformationType newRespondEquipmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondEquipmentInformation(), newRespondEquipmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondMaterialClassType getRespondMaterialClass() {
		return (RespondMaterialClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondMaterialClass(RespondMaterialClassType newRespondMaterialClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialClass(), newRespondMaterialClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondMaterialClass(RespondMaterialClassType newRespondMaterialClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialClass(), newRespondMaterialClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondMaterialDefinitionType getRespondMaterialDefinition() {
		return (RespondMaterialDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondMaterialDefinition(RespondMaterialDefinitionType newRespondMaterialDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialDefinition(), newRespondMaterialDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondMaterialDefinition(RespondMaterialDefinitionType newRespondMaterialDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialDefinition(), newRespondMaterialDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondMaterialInformationType getRespondMaterialInformation() {
		return (RespondMaterialInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondMaterialInformation(RespondMaterialInformationType newRespondMaterialInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialInformation(), newRespondMaterialInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondMaterialInformation(RespondMaterialInformationType newRespondMaterialInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialInformation(), newRespondMaterialInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondMaterialLotType getRespondMaterialLot() {
		return (RespondMaterialLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondMaterialLot(RespondMaterialLotType newRespondMaterialLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialLot(), newRespondMaterialLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondMaterialLot(RespondMaterialLotType newRespondMaterialLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialLot(), newRespondMaterialLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondMaterialSubLotType getRespondMaterialSubLot() {
		return (RespondMaterialSubLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialSubLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondMaterialSubLot(RespondMaterialSubLotType newRespondMaterialSubLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialSubLot(), newRespondMaterialSubLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondMaterialSubLot(RespondMaterialSubLotType newRespondMaterialSubLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialSubLot(), newRespondMaterialSubLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondMaterialTestSpecType getRespondMaterialTestSpec() {
		return (RespondMaterialTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondMaterialTestSpec(RespondMaterialTestSpecType newRespondMaterialTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialTestSpec(), newRespondMaterialTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondMaterialTestSpec(RespondMaterialTestSpecType newRespondMaterialTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondMaterialTestSpec(), newRespondMaterialTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondOperationsCapabilityType getRespondOperationsCapability() {
		return (RespondOperationsCapabilityType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsCapability(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondOperationsCapability(RespondOperationsCapabilityType newRespondOperationsCapability, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsCapability(), newRespondOperationsCapability, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondOperationsCapability(RespondOperationsCapabilityType newRespondOperationsCapability) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsCapability(), newRespondOperationsCapability);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondOperationsCapabilityInformationType getRespondOperationsCapabilityInformation() {
		return (RespondOperationsCapabilityInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsCapabilityInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondOperationsCapabilityInformation(RespondOperationsCapabilityInformationType newRespondOperationsCapabilityInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsCapabilityInformation(), newRespondOperationsCapabilityInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondOperationsCapabilityInformation(RespondOperationsCapabilityInformationType newRespondOperationsCapabilityInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsCapabilityInformation(), newRespondOperationsCapabilityInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondOperationsDefinitionType getRespondOperationsDefinition() {
		return (RespondOperationsDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondOperationsDefinition(RespondOperationsDefinitionType newRespondOperationsDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsDefinition(), newRespondOperationsDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondOperationsDefinition(RespondOperationsDefinitionType newRespondOperationsDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsDefinition(), newRespondOperationsDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondOperationsDefinitionInformationType getRespondOperationsDefinitionInformation() {
		return (RespondOperationsDefinitionInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsDefinitionInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondOperationsDefinitionInformation(RespondOperationsDefinitionInformationType newRespondOperationsDefinitionInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsDefinitionInformation(), newRespondOperationsDefinitionInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondOperationsDefinitionInformation(RespondOperationsDefinitionInformationType newRespondOperationsDefinitionInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsDefinitionInformation(), newRespondOperationsDefinitionInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondOperationsPerformanceType getRespondOperationsPerformance() {
		return (RespondOperationsPerformanceType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsPerformance(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondOperationsPerformance(RespondOperationsPerformanceType newRespondOperationsPerformance, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsPerformance(), newRespondOperationsPerformance, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondOperationsPerformance(RespondOperationsPerformanceType newRespondOperationsPerformance) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsPerformance(), newRespondOperationsPerformance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondOperationsScheduleType getRespondOperationsSchedule() {
		return (RespondOperationsScheduleType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsSchedule(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondOperationsSchedule(RespondOperationsScheduleType newRespondOperationsSchedule, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsSchedule(), newRespondOperationsSchedule, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondOperationsSchedule(RespondOperationsScheduleType newRespondOperationsSchedule) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondOperationsSchedule(), newRespondOperationsSchedule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPersonType getRespondPerson() {
		return (RespondPersonType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPerson(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondPerson(RespondPersonType newRespondPerson, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPerson(), newRespondPerson, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondPerson(RespondPersonType newRespondPerson) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPerson(), newRespondPerson);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPersonnelClassType getRespondPersonnelClass() {
		return (RespondPersonnelClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPersonnelClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondPersonnelClass(RespondPersonnelClassType newRespondPersonnelClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPersonnelClass(), newRespondPersonnelClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondPersonnelClass(RespondPersonnelClassType newRespondPersonnelClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPersonnelClass(), newRespondPersonnelClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPersonnelInformationType getRespondPersonnelInformation() {
		return (RespondPersonnelInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPersonnelInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondPersonnelInformation(RespondPersonnelInformationType newRespondPersonnelInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPersonnelInformation(), newRespondPersonnelInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondPersonnelInformation(RespondPersonnelInformationType newRespondPersonnelInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPersonnelInformation(), newRespondPersonnelInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPhysicalAssetType getRespondPhysicalAsset() {
		return (RespondPhysicalAssetType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPhysicalAsset(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondPhysicalAsset(RespondPhysicalAssetType newRespondPhysicalAsset, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPhysicalAsset(), newRespondPhysicalAsset, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondPhysicalAsset(RespondPhysicalAssetType newRespondPhysicalAsset) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPhysicalAsset(), newRespondPhysicalAsset);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPhysicalAssetCapabilityTestSpecType getRespondPhysicalAssetCapabilityTestSpec() {
		return (RespondPhysicalAssetCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPhysicalAssetCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondPhysicalAssetCapabilityTestSpec(RespondPhysicalAssetCapabilityTestSpecType newRespondPhysicalAssetCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPhysicalAssetCapabilityTestSpec(), newRespondPhysicalAssetCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondPhysicalAssetCapabilityTestSpec(RespondPhysicalAssetCapabilityTestSpecType newRespondPhysicalAssetCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPhysicalAssetCapabilityTestSpec(), newRespondPhysicalAssetCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPhysicalAssetClassType getRespondPhysicalAssetClass() {
		return (RespondPhysicalAssetClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPhysicalAssetClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondPhysicalAssetClass(RespondPhysicalAssetClassType newRespondPhysicalAssetClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPhysicalAssetClass(), newRespondPhysicalAssetClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondPhysicalAssetClass(RespondPhysicalAssetClassType newRespondPhysicalAssetClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPhysicalAssetClass(), newRespondPhysicalAssetClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondPhysicalAssetInformationType getRespondPhysicalAssetInformation() {
		return (RespondPhysicalAssetInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPhysicalAssetInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondPhysicalAssetInformation(RespondPhysicalAssetInformationType newRespondPhysicalAssetInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPhysicalAssetInformation(), newRespondPhysicalAssetInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondPhysicalAssetInformation(RespondPhysicalAssetInformationType newRespondPhysicalAssetInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondPhysicalAssetInformation(), newRespondPhysicalAssetInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondProcessSegmentType getRespondProcessSegment() {
		return (RespondProcessSegmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondProcessSegment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondProcessSegment(RespondProcessSegmentType newRespondProcessSegment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondProcessSegment(), newRespondProcessSegment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondProcessSegment(RespondProcessSegmentType newRespondProcessSegment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondProcessSegment(), newRespondProcessSegment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondProcessSegmentInformationType getRespondProcessSegmentInformation() {
		return (RespondProcessSegmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondProcessSegmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondProcessSegmentInformation(RespondProcessSegmentInformationType newRespondProcessSegmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondProcessSegmentInformation(), newRespondProcessSegmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondProcessSegmentInformation(RespondProcessSegmentInformationType newRespondProcessSegmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondProcessSegmentInformation(), newRespondProcessSegmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RespondQualificationTestSpecificationType getRespondQualificationTestSpecification() {
		return (RespondQualificationTestSpecificationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondQualificationTestSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRespondQualificationTestSpecification(RespondQualificationTestSpecificationType newRespondQualificationTestSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondQualificationTestSpecification(), newRespondQualificationTestSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRespondQualificationTestSpecification(RespondQualificationTestSpecificationType newRespondQualificationTestSpecification) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_RespondQualificationTestSpecification(), newRespondQualificationTestSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowEquipmentType getShowEquipment() {
		return (ShowEquipmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowEquipment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowEquipment(ShowEquipmentType newShowEquipment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowEquipment(), newShowEquipment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowEquipment(ShowEquipmentType newShowEquipment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowEquipment(), newShowEquipment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowEquipmentCapabilityTestSpecType getShowEquipmentCapabilityTestSpec() {
		return (ShowEquipmentCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowEquipmentCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowEquipmentCapabilityTestSpec(ShowEquipmentCapabilityTestSpecType newShowEquipmentCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowEquipmentCapabilityTestSpec(), newShowEquipmentCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowEquipmentCapabilityTestSpec(ShowEquipmentCapabilityTestSpecType newShowEquipmentCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowEquipmentCapabilityTestSpec(), newShowEquipmentCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowEquipmentClassType getShowEquipmentClass() {
		return (ShowEquipmentClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowEquipmentClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowEquipmentClass(ShowEquipmentClassType newShowEquipmentClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowEquipmentClass(), newShowEquipmentClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowEquipmentClass(ShowEquipmentClassType newShowEquipmentClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowEquipmentClass(), newShowEquipmentClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowEquipmentInformationType getShowEquipmentInformation() {
		return (ShowEquipmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowEquipmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowEquipmentInformation(ShowEquipmentInformationType newShowEquipmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowEquipmentInformation(), newShowEquipmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowEquipmentInformation(ShowEquipmentInformationType newShowEquipmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowEquipmentInformation(), newShowEquipmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowMaterialClassType getShowMaterialClass() {
		return (ShowMaterialClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowMaterialClass(ShowMaterialClassType newShowMaterialClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialClass(), newShowMaterialClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowMaterialClass(ShowMaterialClassType newShowMaterialClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialClass(), newShowMaterialClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowMaterialDefinitionType getShowMaterialDefinition() {
		return (ShowMaterialDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowMaterialDefinition(ShowMaterialDefinitionType newShowMaterialDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialDefinition(), newShowMaterialDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowMaterialDefinition(ShowMaterialDefinitionType newShowMaterialDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialDefinition(), newShowMaterialDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowMaterialInformationType getShowMaterialInformation() {
		return (ShowMaterialInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowMaterialInformation(ShowMaterialInformationType newShowMaterialInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialInformation(), newShowMaterialInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowMaterialInformation(ShowMaterialInformationType newShowMaterialInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialInformation(), newShowMaterialInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowMaterialLotType getShowMaterialLot() {
		return (ShowMaterialLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowMaterialLot(ShowMaterialLotType newShowMaterialLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialLot(), newShowMaterialLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowMaterialLot(ShowMaterialLotType newShowMaterialLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialLot(), newShowMaterialLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowMaterialSubLotType getShowMaterialSubLot() {
		return (ShowMaterialSubLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialSubLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowMaterialSubLot(ShowMaterialSubLotType newShowMaterialSubLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialSubLot(), newShowMaterialSubLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowMaterialSubLot(ShowMaterialSubLotType newShowMaterialSubLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialSubLot(), newShowMaterialSubLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowMaterialTestSpecType getShowMaterialTestSpec() {
		return (ShowMaterialTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowMaterialTestSpec(ShowMaterialTestSpecType newShowMaterialTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialTestSpec(), newShowMaterialTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowMaterialTestSpec(ShowMaterialTestSpecType newShowMaterialTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowMaterialTestSpec(), newShowMaterialTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowOperationsCapabilityType getShowOperationsCapability() {
		return (ShowOperationsCapabilityType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsCapability(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowOperationsCapability(ShowOperationsCapabilityType newShowOperationsCapability, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsCapability(), newShowOperationsCapability, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowOperationsCapability(ShowOperationsCapabilityType newShowOperationsCapability) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsCapability(), newShowOperationsCapability);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowOperationsCapabilityInformationType getShowOperationsCapabilityInformation() {
		return (ShowOperationsCapabilityInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsCapabilityInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowOperationsCapabilityInformation(ShowOperationsCapabilityInformationType newShowOperationsCapabilityInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsCapabilityInformation(), newShowOperationsCapabilityInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowOperationsCapabilityInformation(ShowOperationsCapabilityInformationType newShowOperationsCapabilityInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsCapabilityInformation(), newShowOperationsCapabilityInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowOperationsDefinitionType getShowOperationsDefinition() {
		return (ShowOperationsDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowOperationsDefinition(ShowOperationsDefinitionType newShowOperationsDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsDefinition(), newShowOperationsDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowOperationsDefinition(ShowOperationsDefinitionType newShowOperationsDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsDefinition(), newShowOperationsDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowOperationsDefinitionInformationType getShowOperationsDefinitionInformation() {
		return (ShowOperationsDefinitionInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsDefinitionInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowOperationsDefinitionInformation(ShowOperationsDefinitionInformationType newShowOperationsDefinitionInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsDefinitionInformation(), newShowOperationsDefinitionInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowOperationsDefinitionInformation(ShowOperationsDefinitionInformationType newShowOperationsDefinitionInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsDefinitionInformation(), newShowOperationsDefinitionInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowOperationsPerformanceType getShowOperationsPerformance() {
		return (ShowOperationsPerformanceType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsPerformance(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowOperationsPerformance(ShowOperationsPerformanceType newShowOperationsPerformance, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsPerformance(), newShowOperationsPerformance, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowOperationsPerformance(ShowOperationsPerformanceType newShowOperationsPerformance) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsPerformance(), newShowOperationsPerformance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowOperationsScheduleType getShowOperationsSchedule() {
		return (ShowOperationsScheduleType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsSchedule(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowOperationsSchedule(ShowOperationsScheduleType newShowOperationsSchedule, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsSchedule(), newShowOperationsSchedule, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowOperationsSchedule(ShowOperationsScheduleType newShowOperationsSchedule) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowOperationsSchedule(), newShowOperationsSchedule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPersonType getShowPerson() {
		return (ShowPersonType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPerson(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowPerson(ShowPersonType newShowPerson, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPerson(), newShowPerson, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowPerson(ShowPersonType newShowPerson) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPerson(), newShowPerson);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPersonnelClassType getShowPersonnelClass() {
		return (ShowPersonnelClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPersonnelClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowPersonnelClass(ShowPersonnelClassType newShowPersonnelClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPersonnelClass(), newShowPersonnelClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowPersonnelClass(ShowPersonnelClassType newShowPersonnelClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPersonnelClass(), newShowPersonnelClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPersonnelInformationType getShowPersonnelInformation() {
		return (ShowPersonnelInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPersonnelInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowPersonnelInformation(ShowPersonnelInformationType newShowPersonnelInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPersonnelInformation(), newShowPersonnelInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowPersonnelInformation(ShowPersonnelInformationType newShowPersonnelInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPersonnelInformation(), newShowPersonnelInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPhysicalAssetType getShowPhysicalAsset() {
		return (ShowPhysicalAssetType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPhysicalAsset(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowPhysicalAsset(ShowPhysicalAssetType newShowPhysicalAsset, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPhysicalAsset(), newShowPhysicalAsset, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowPhysicalAsset(ShowPhysicalAssetType newShowPhysicalAsset) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPhysicalAsset(), newShowPhysicalAsset);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPhysicalAssetCapabilityTestSpecType getShowPhysicalAssetCapabilityTestSpec() {
		return (ShowPhysicalAssetCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPhysicalAssetCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowPhysicalAssetCapabilityTestSpec(ShowPhysicalAssetCapabilityTestSpecType newShowPhysicalAssetCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPhysicalAssetCapabilityTestSpec(), newShowPhysicalAssetCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowPhysicalAssetCapabilityTestSpec(ShowPhysicalAssetCapabilityTestSpecType newShowPhysicalAssetCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPhysicalAssetCapabilityTestSpec(), newShowPhysicalAssetCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPhysicalAssetClassType getShowPhysicalAssetClass() {
		return (ShowPhysicalAssetClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPhysicalAssetClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowPhysicalAssetClass(ShowPhysicalAssetClassType newShowPhysicalAssetClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPhysicalAssetClass(), newShowPhysicalAssetClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowPhysicalAssetClass(ShowPhysicalAssetClassType newShowPhysicalAssetClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPhysicalAssetClass(), newShowPhysicalAssetClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowPhysicalAssetInformationType getShowPhysicalAssetInformation() {
		return (ShowPhysicalAssetInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPhysicalAssetInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowPhysicalAssetInformation(ShowPhysicalAssetInformationType newShowPhysicalAssetInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPhysicalAssetInformation(), newShowPhysicalAssetInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowPhysicalAssetInformation(ShowPhysicalAssetInformationType newShowPhysicalAssetInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowPhysicalAssetInformation(), newShowPhysicalAssetInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowProcessSegmentType getShowProcessSegment() {
		return (ShowProcessSegmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowProcessSegment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowProcessSegment(ShowProcessSegmentType newShowProcessSegment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowProcessSegment(), newShowProcessSegment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowProcessSegment(ShowProcessSegmentType newShowProcessSegment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowProcessSegment(), newShowProcessSegment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowProcessSegmentInformationType getShowProcessSegmentInformation() {
		return (ShowProcessSegmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowProcessSegmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowProcessSegmentInformation(ShowProcessSegmentInformationType newShowProcessSegmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowProcessSegmentInformation(), newShowProcessSegmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowProcessSegmentInformation(ShowProcessSegmentInformationType newShowProcessSegmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowProcessSegmentInformation(), newShowProcessSegmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowQualificationTestSpecificationType getShowQualificationTestSpecification() {
		return (ShowQualificationTestSpecificationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowQualificationTestSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShowQualificationTestSpecification(ShowQualificationTestSpecificationType newShowQualificationTestSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowQualificationTestSpecification(), newShowQualificationTestSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowQualificationTestSpecification(ShowQualificationTestSpecificationType newShowQualificationTestSpecification) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_ShowQualificationTestSpecification(), newShowQualificationTestSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncEquipmentType getSyncEquipment() {
		return (SyncEquipmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncEquipment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncEquipment(SyncEquipmentType newSyncEquipment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncEquipment(), newSyncEquipment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncEquipment(SyncEquipmentType newSyncEquipment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncEquipment(), newSyncEquipment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncEquipmentCapabilityTestSpecType getSyncEquipmentCapabilityTestSpec() {
		return (SyncEquipmentCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncEquipmentCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncEquipmentCapabilityTestSpec(SyncEquipmentCapabilityTestSpecType newSyncEquipmentCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncEquipmentCapabilityTestSpec(), newSyncEquipmentCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncEquipmentCapabilityTestSpec(SyncEquipmentCapabilityTestSpecType newSyncEquipmentCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncEquipmentCapabilityTestSpec(), newSyncEquipmentCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncEquipmentClassType getSyncEquipmentClass() {
		return (SyncEquipmentClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncEquipmentClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncEquipmentClass(SyncEquipmentClassType newSyncEquipmentClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncEquipmentClass(), newSyncEquipmentClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncEquipmentClass(SyncEquipmentClassType newSyncEquipmentClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncEquipmentClass(), newSyncEquipmentClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncEquipmentInformationType getSyncEquipmentInformation() {
		return (SyncEquipmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncEquipmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncEquipmentInformation(SyncEquipmentInformationType newSyncEquipmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncEquipmentInformation(), newSyncEquipmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncEquipmentInformation(SyncEquipmentInformationType newSyncEquipmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncEquipmentInformation(), newSyncEquipmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncMaterialClassType getSyncMaterialClass() {
		return (SyncMaterialClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncMaterialClass(SyncMaterialClassType newSyncMaterialClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialClass(), newSyncMaterialClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncMaterialClass(SyncMaterialClassType newSyncMaterialClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialClass(), newSyncMaterialClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncMaterialDefinitionType getSyncMaterialDefinition() {
		return (SyncMaterialDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncMaterialDefinition(SyncMaterialDefinitionType newSyncMaterialDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialDefinition(), newSyncMaterialDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncMaterialDefinition(SyncMaterialDefinitionType newSyncMaterialDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialDefinition(), newSyncMaterialDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncMaterialInformationType getSyncMaterialInformation() {
		return (SyncMaterialInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncMaterialInformation(SyncMaterialInformationType newSyncMaterialInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialInformation(), newSyncMaterialInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncMaterialInformation(SyncMaterialInformationType newSyncMaterialInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialInformation(), newSyncMaterialInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncMaterialLotType getSyncMaterialLot() {
		return (SyncMaterialLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncMaterialLot(SyncMaterialLotType newSyncMaterialLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialLot(), newSyncMaterialLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncMaterialLot(SyncMaterialLotType newSyncMaterialLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialLot(), newSyncMaterialLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncMaterialSubLotType getSyncMaterialSubLot() {
		return (SyncMaterialSubLotType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialSubLot(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncMaterialSubLot(SyncMaterialSubLotType newSyncMaterialSubLot, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialSubLot(), newSyncMaterialSubLot, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncMaterialSubLot(SyncMaterialSubLotType newSyncMaterialSubLot) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialSubLot(), newSyncMaterialSubLot);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncMaterialTestSpecType getSyncMaterialTestSpec() {
		return (SyncMaterialTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncMaterialTestSpec(SyncMaterialTestSpecType newSyncMaterialTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialTestSpec(), newSyncMaterialTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncMaterialTestSpec(SyncMaterialTestSpecType newSyncMaterialTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncMaterialTestSpec(), newSyncMaterialTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncOperationsCapabilityType getSyncOperationsCapability() {
		return (SyncOperationsCapabilityType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsCapability(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncOperationsCapability(SyncOperationsCapabilityType newSyncOperationsCapability, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsCapability(), newSyncOperationsCapability, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncOperationsCapability(SyncOperationsCapabilityType newSyncOperationsCapability) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsCapability(), newSyncOperationsCapability);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncOperationsCapabilityInformationType getSyncOperationsCapabilityInformation() {
		return (SyncOperationsCapabilityInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsCapabilityInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncOperationsCapabilityInformation(SyncOperationsCapabilityInformationType newSyncOperationsCapabilityInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsCapabilityInformation(), newSyncOperationsCapabilityInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncOperationsCapabilityInformation(SyncOperationsCapabilityInformationType newSyncOperationsCapabilityInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsCapabilityInformation(), newSyncOperationsCapabilityInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncOperationsDefinitionType getSyncOperationsDefinition() {
		return (SyncOperationsDefinitionType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncOperationsDefinition(SyncOperationsDefinitionType newSyncOperationsDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsDefinition(), newSyncOperationsDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncOperationsDefinition(SyncOperationsDefinitionType newSyncOperationsDefinition) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsDefinition(), newSyncOperationsDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncOperationsDefinitionInformationType getSyncOperationsDefinitionInformation() {
		return (SyncOperationsDefinitionInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsDefinitionInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncOperationsDefinitionInformation(SyncOperationsDefinitionInformationType newSyncOperationsDefinitionInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsDefinitionInformation(), newSyncOperationsDefinitionInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncOperationsDefinitionInformation(SyncOperationsDefinitionInformationType newSyncOperationsDefinitionInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsDefinitionInformation(), newSyncOperationsDefinitionInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncOperationsPerformanceType getSyncOperationsPerformance() {
		return (SyncOperationsPerformanceType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsPerformance(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncOperationsPerformance(SyncOperationsPerformanceType newSyncOperationsPerformance, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsPerformance(), newSyncOperationsPerformance, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncOperationsPerformance(SyncOperationsPerformanceType newSyncOperationsPerformance) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsPerformance(), newSyncOperationsPerformance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncOperationsScheduleType getSyncOperationsSchedule() {
		return (SyncOperationsScheduleType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsSchedule(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncOperationsSchedule(SyncOperationsScheduleType newSyncOperationsSchedule, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsSchedule(), newSyncOperationsSchedule, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncOperationsSchedule(SyncOperationsScheduleType newSyncOperationsSchedule) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncOperationsSchedule(), newSyncOperationsSchedule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPersonType getSyncPerson() {
		return (SyncPersonType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPerson(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncPerson(SyncPersonType newSyncPerson, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPerson(), newSyncPerson, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncPerson(SyncPersonType newSyncPerson) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPerson(), newSyncPerson);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPersonnelClassType getSyncPersonnelClass() {
		return (SyncPersonnelClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPersonnelClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncPersonnelClass(SyncPersonnelClassType newSyncPersonnelClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPersonnelClass(), newSyncPersonnelClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncPersonnelClass(SyncPersonnelClassType newSyncPersonnelClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPersonnelClass(), newSyncPersonnelClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPersonnelInformationType getSyncPersonnelInformation() {
		return (SyncPersonnelInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPersonnelInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncPersonnelInformation(SyncPersonnelInformationType newSyncPersonnelInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPersonnelInformation(), newSyncPersonnelInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncPersonnelInformation(SyncPersonnelInformationType newSyncPersonnelInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPersonnelInformation(), newSyncPersonnelInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPhysicalAssetType getSyncPhysicalAsset() {
		return (SyncPhysicalAssetType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPhysicalAsset(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncPhysicalAsset(SyncPhysicalAssetType newSyncPhysicalAsset, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPhysicalAsset(), newSyncPhysicalAsset, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncPhysicalAsset(SyncPhysicalAssetType newSyncPhysicalAsset) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPhysicalAsset(), newSyncPhysicalAsset);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPhysicalAssetCapabilityTestSpecType getSyncPhysicalAssetCapabilityTestSpec() {
		return (SyncPhysicalAssetCapabilityTestSpecType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPhysicalAssetCapabilityTestSpec(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncPhysicalAssetCapabilityTestSpec(SyncPhysicalAssetCapabilityTestSpecType newSyncPhysicalAssetCapabilityTestSpec, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPhysicalAssetCapabilityTestSpec(), newSyncPhysicalAssetCapabilityTestSpec, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncPhysicalAssetCapabilityTestSpec(SyncPhysicalAssetCapabilityTestSpecType newSyncPhysicalAssetCapabilityTestSpec) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPhysicalAssetCapabilityTestSpec(), newSyncPhysicalAssetCapabilityTestSpec);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPhysicalAssetClassType getSyncPhysicalAssetClass() {
		return (SyncPhysicalAssetClassType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPhysicalAssetClass(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncPhysicalAssetClass(SyncPhysicalAssetClassType newSyncPhysicalAssetClass, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPhysicalAssetClass(), newSyncPhysicalAssetClass, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncPhysicalAssetClass(SyncPhysicalAssetClassType newSyncPhysicalAssetClass) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPhysicalAssetClass(), newSyncPhysicalAssetClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncPhysicalAssetInformationType getSyncPhysicalAssetInformation() {
		return (SyncPhysicalAssetInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPhysicalAssetInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncPhysicalAssetInformation(SyncPhysicalAssetInformationType newSyncPhysicalAssetInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPhysicalAssetInformation(), newSyncPhysicalAssetInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncPhysicalAssetInformation(SyncPhysicalAssetInformationType newSyncPhysicalAssetInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncPhysicalAssetInformation(), newSyncPhysicalAssetInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncProcessSegmentType getSyncProcessSegment() {
		return (SyncProcessSegmentType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncProcessSegment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncProcessSegment(SyncProcessSegmentType newSyncProcessSegment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncProcessSegment(), newSyncProcessSegment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncProcessSegment(SyncProcessSegmentType newSyncProcessSegment) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncProcessSegment(), newSyncProcessSegment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncProcessSegmentInformationType getSyncProcessSegmentInformation() {
		return (SyncProcessSegmentInformationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncProcessSegmentInformation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncProcessSegmentInformation(SyncProcessSegmentInformationType newSyncProcessSegmentInformation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncProcessSegmentInformation(), newSyncProcessSegmentInformation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncProcessSegmentInformation(SyncProcessSegmentInformationType newSyncProcessSegmentInformation) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncProcessSegmentInformation(), newSyncProcessSegmentInformation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncQualificationTestSpecificationType getSyncQualificationTestSpecification() {
		return (SyncQualificationTestSpecificationType)getMixed().get(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncQualificationTestSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSyncQualificationTestSpecification(SyncQualificationTestSpecificationType newSyncQualificationTestSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncQualificationTestSpecification(), newSyncQualificationTestSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSyncQualificationTestSpecification(SyncQualificationTestSpecificationType newSyncQualificationTestSpecification) {
		((FeatureMap.Internal)getMixed()).set(B2MMLPackage.eINSTANCE.getDocumentRoot_SyncQualificationTestSpecification(), newSyncQualificationTestSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DOCUMENT_ROOT__MIXED:
				return ((InternalEList<?>)getMixed()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return ((InternalEList<?>)getXMLNSPrefixMap()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return ((InternalEList<?>)getXSISchemaLocation()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT:
				return basicSetAcknowledgeEquipment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return basicSetAcknowledgeEquipmentCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_CLASS:
				return basicSetAcknowledgeEquipmentClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_INFORMATION:
				return basicSetAcknowledgeEquipmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_CLASS:
				return basicSetAcknowledgeMaterialClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_DEFINITION:
				return basicSetAcknowledgeMaterialDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_INFORMATION:
				return basicSetAcknowledgeMaterialInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_LOT:
				return basicSetAcknowledgeMaterialLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_SUB_LOT:
				return basicSetAcknowledgeMaterialSubLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_TEST_SPEC:
				return basicSetAcknowledgeMaterialTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_CAPABILITY:
				return basicSetAcknowledgeOperationsCapability(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_CAPABILITY_INFORMATION:
				return basicSetAcknowledgeOperationsCapabilityInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_DEFINITION:
				return basicSetAcknowledgeOperationsDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_DEFINITION_INFORMATION:
				return basicSetAcknowledgeOperationsDefinitionInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_PERFORMANCE:
				return basicSetAcknowledgeOperationsPerformance(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_SCHEDULE:
				return basicSetAcknowledgeOperationsSchedule(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSON:
				return basicSetAcknowledgePerson(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSONNEL_CLASS:
				return basicSetAcknowledgePersonnelClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSONNEL_INFORMATION:
				return basicSetAcknowledgePersonnelInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET:
				return basicSetAcknowledgePhysicalAsset(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return basicSetAcknowledgePhysicalAssetCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_CLASS:
				return basicSetAcknowledgePhysicalAssetClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_INFORMATION:
				return basicSetAcknowledgePhysicalAssetInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PROCESS_SEGMENT:
				return basicSetAcknowledgeProcessSegment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PROCESS_SEGMENT_INFORMATION:
				return basicSetAcknowledgeProcessSegmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_QUALIFICATION_TEST_SPECIFICATION:
				return basicSetAcknowledgeQualificationTestSpecification(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT:
				return basicSetCancelEquipment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return basicSetCancelEquipmentCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_CLASS:
				return basicSetCancelEquipmentClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_INFORMATION:
				return basicSetCancelEquipmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_CLASS:
				return basicSetCancelMaterialClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_DEFINITION:
				return basicSetCancelMaterialDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_INFORMATION:
				return basicSetCancelMaterialInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_LOT:
				return basicSetCancelMaterialLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_SUB_LOT:
				return basicSetCancelMaterialSubLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_TEST_SPEC:
				return basicSetCancelMaterialTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_CAPABILITY:
				return basicSetCancelOperationsCapability(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_CAPABILITY_INFORMATION:
				return basicSetCancelOperationsCapabilityInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_DEFINITION:
				return basicSetCancelOperationsDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_DEFINITION_INFORMATION:
				return basicSetCancelOperationsDefinitionInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_PERFORMANCE:
				return basicSetCancelOperationsPerformance(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_SCHEDULE:
				return basicSetCancelOperationsSchedule(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSON:
				return basicSetCancelPerson(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSONNEL_CLASS:
				return basicSetCancelPersonnelClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSONNEL_INFORMATION:
				return basicSetCancelPersonnelInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET:
				return basicSetCancelPhysicalAsset(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return basicSetCancelPhysicalAssetCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_CLASS:
				return basicSetCancelPhysicalAssetClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_INFORMATION:
				return basicSetCancelPhysicalAssetInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PROCESS_SEGMENT:
				return basicSetCancelProcessSegment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PROCESS_SEGMENT_INFORMATION:
				return basicSetCancelProcessSegmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_QUALIFICATION_TEST_SPECIFICATION:
				return basicSetCancelQualificationTestSpecification(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT:
				return basicSetChangeEquipment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return basicSetChangeEquipmentCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_CLASS:
				return basicSetChangeEquipmentClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_INFORMATION:
				return basicSetChangeEquipmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_CLASS:
				return basicSetChangeMaterialClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_DEFINITION:
				return basicSetChangeMaterialDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_INFORMATION:
				return basicSetChangeMaterialInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_LOT:
				return basicSetChangeMaterialLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_SUB_LOT:
				return basicSetChangeMaterialSubLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_TEST_SPEC:
				return basicSetChangeMaterialTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_CAPABILITY:
				return basicSetChangeOperationsCapability(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_CAPABILITY_INFORMATION:
				return basicSetChangeOperationsCapabilityInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_DEFINITION:
				return basicSetChangeOperationsDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_DEFINITION_INFORMATION:
				return basicSetChangeOperationsDefinitionInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_PERFORMANCE:
				return basicSetChangeOperationsPerformance(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_SCHEDULE:
				return basicSetChangeOperationsSchedule(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSON:
				return basicSetChangePerson(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSONNEL_CLASS:
				return basicSetChangePersonnelClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSONNEL_INFORMATION:
				return basicSetChangePersonnelInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET:
				return basicSetChangePhysicalAsset(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return basicSetChangePhysicalAssetCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_CLASS:
				return basicSetChangePhysicalAssetClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_INFORMATION:
				return basicSetChangePhysicalAssetInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PROCESS_SEGMENT:
				return basicSetChangeProcessSegment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PROCESS_SEGMENT_INFORMATION:
				return basicSetChangeProcessSegmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_QUALIFICATION_TEST_SPECIFICATION:
				return basicSetChangeQualificationTestSpecification(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__CONFIRM_BOD:
				return basicSetConfirmBOD(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT:
				return basicSetEquipment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION:
				return basicSetEquipmentCapabilityTestSpecification(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_CLASS:
				return basicSetEquipmentClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_INFORMATION:
				return basicSetEquipmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT:
				return basicSetGetEquipment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return basicSetGetEquipmentCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_CLASS:
				return basicSetGetEquipmentClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_INFORMATION:
				return basicSetGetEquipmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_CLASS:
				return basicSetGetMaterialClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_DEFINITION:
				return basicSetGetMaterialDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_INFORMATION:
				return basicSetGetMaterialInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_LOT:
				return basicSetGetMaterialLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_SUB_LOT:
				return basicSetGetMaterialSubLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_TEST_SPEC:
				return basicSetGetMaterialTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_CAPABILITY:
				return basicSetGetOperationsCapability(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_CAPABILITY_INFORMATION:
				return basicSetGetOperationsCapabilityInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_DEFINITION:
				return basicSetGetOperationsDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_DEFINITION_INFORMATION:
				return basicSetGetOperationsDefinitionInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_PERFORMANCE:
				return basicSetGetOperationsPerformance(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_SCHEDULE:
				return basicSetGetOperationsSchedule(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSON:
				return basicSetGetPerson(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSONNEL_CLASS:
				return basicSetGetPersonnelClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSONNEL_INFORMATION:
				return basicSetGetPersonnelInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET:
				return basicSetGetPhysicalAsset(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return basicSetGetPhysicalAssetCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_CLASS:
				return basicSetGetPhysicalAssetClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_INFORMATION:
				return basicSetGetPhysicalAssetInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_PROCESS_SEGMENT:
				return basicSetGetProcessSegment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_PROCESS_SEGMENT_INFORMATION:
				return basicSetGetProcessSegmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__GET_QUALIFICATION_TEST_SPECIFICATION:
				return basicSetGetQualificationTestSpecification(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_CLASS:
				return basicSetMaterialClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_DEFINITION:
				return basicSetMaterialDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_INFORMATION:
				return basicSetMaterialInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_LOT:
				return basicSetMaterialLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_SUB_LOT:
				return basicSetMaterialSubLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_TEST_SPECIFICATION:
				return basicSetMaterialTestSpecification(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_CAPABILITY:
				return basicSetOperationsCapability(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_CAPABILITY_INFORMATION:
				return basicSetOperationsCapabilityInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_DEFINITION:
				return basicSetOperationsDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_DEFINITION_INFORMATION:
				return basicSetOperationsDefinitionInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_PERFORMANCE:
				return basicSetOperationsPerformance(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_REQUEST:
				return basicSetOperationsRequest(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_RESPONSE:
				return basicSetOperationsResponse(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_SCHEDULE:
				return basicSetOperationsSchedule(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PERSON:
				return basicSetPerson(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PERSONNEL_CLASS:
				return basicSetPersonnelClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PERSONNEL_INFORMATION:
				return basicSetPersonnelInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET:
				return basicSetPhysicalAsset(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION:
				return basicSetPhysicalAssetCapabilityTestSpecification(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_CLASS:
				return basicSetPhysicalAssetClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_INFORMATION:
				return basicSetPhysicalAssetInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT:
				return basicSetProcessEquipment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return basicSetProcessEquipmentCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_CLASS:
				return basicSetProcessEquipmentClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_INFORMATION:
				return basicSetProcessEquipmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_CLASS:
				return basicSetProcessMaterialClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_DEFINITION:
				return basicSetProcessMaterialDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_INFORMATION:
				return basicSetProcessMaterialInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_LOT:
				return basicSetProcessMaterialLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_SUB_LOT:
				return basicSetProcessMaterialSubLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_TEST_SPEC:
				return basicSetProcessMaterialTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_CAPABILITY:
				return basicSetProcessOperationsCapability(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_CAPABILITY_INFORMATION:
				return basicSetProcessOperationsCapabilityInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_DEFINITION:
				return basicSetProcessOperationsDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_DEFINITION_INFORMATION:
				return basicSetProcessOperationsDefinitionInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_PERFORMANCE:
				return basicSetProcessOperationsPerformance(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_SCHEDULE:
				return basicSetProcessOperationsSchedule(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSON:
				return basicSetProcessPerson(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSONNEL_CLASS:
				return basicSetProcessPersonnelClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSONNEL_INFORMATION:
				return basicSetProcessPersonnelInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET:
				return basicSetProcessPhysicalAsset(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return basicSetProcessPhysicalAssetCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_CLASS:
				return basicSetProcessPhysicalAssetClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_INFORMATION:
				return basicSetProcessPhysicalAssetInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PROCESS_SEGMENT:
				return basicSetProcessProcessSegment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PROCESS_SEGMENT_INFORMATION:
				return basicSetProcessProcessSegmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_QUALIFICATION_TEST_SPECIFICATION:
				return basicSetProcessQualificationTestSpecification(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_SEGMENT:
				return basicSetProcessSegment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_SEGMENT_INFORMATION:
				return basicSetProcessSegmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__QUALIFICATION_TEST_SPECIFICATION:
				return basicSetQualificationTestSpecification(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT:
				return basicSetRespondEquipment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return basicSetRespondEquipmentCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_CLASS:
				return basicSetRespondEquipmentClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_INFORMATION:
				return basicSetRespondEquipmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_CLASS:
				return basicSetRespondMaterialClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_DEFINITION:
				return basicSetRespondMaterialDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_INFORMATION:
				return basicSetRespondMaterialInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_LOT:
				return basicSetRespondMaterialLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_SUB_LOT:
				return basicSetRespondMaterialSubLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_TEST_SPEC:
				return basicSetRespondMaterialTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_CAPABILITY:
				return basicSetRespondOperationsCapability(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_CAPABILITY_INFORMATION:
				return basicSetRespondOperationsCapabilityInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_DEFINITION:
				return basicSetRespondOperationsDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_DEFINITION_INFORMATION:
				return basicSetRespondOperationsDefinitionInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_PERFORMANCE:
				return basicSetRespondOperationsPerformance(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_SCHEDULE:
				return basicSetRespondOperationsSchedule(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSON:
				return basicSetRespondPerson(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSONNEL_CLASS:
				return basicSetRespondPersonnelClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSONNEL_INFORMATION:
				return basicSetRespondPersonnelInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET:
				return basicSetRespondPhysicalAsset(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return basicSetRespondPhysicalAssetCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_CLASS:
				return basicSetRespondPhysicalAssetClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_INFORMATION:
				return basicSetRespondPhysicalAssetInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PROCESS_SEGMENT:
				return basicSetRespondProcessSegment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PROCESS_SEGMENT_INFORMATION:
				return basicSetRespondProcessSegmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_QUALIFICATION_TEST_SPECIFICATION:
				return basicSetRespondQualificationTestSpecification(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT:
				return basicSetShowEquipment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return basicSetShowEquipmentCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_CLASS:
				return basicSetShowEquipmentClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_INFORMATION:
				return basicSetShowEquipmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_CLASS:
				return basicSetShowMaterialClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_DEFINITION:
				return basicSetShowMaterialDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_INFORMATION:
				return basicSetShowMaterialInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_LOT:
				return basicSetShowMaterialLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_SUB_LOT:
				return basicSetShowMaterialSubLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_TEST_SPEC:
				return basicSetShowMaterialTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_CAPABILITY:
				return basicSetShowOperationsCapability(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_CAPABILITY_INFORMATION:
				return basicSetShowOperationsCapabilityInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_DEFINITION:
				return basicSetShowOperationsDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_DEFINITION_INFORMATION:
				return basicSetShowOperationsDefinitionInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_PERFORMANCE:
				return basicSetShowOperationsPerformance(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_SCHEDULE:
				return basicSetShowOperationsSchedule(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSON:
				return basicSetShowPerson(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSONNEL_CLASS:
				return basicSetShowPersonnelClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSONNEL_INFORMATION:
				return basicSetShowPersonnelInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET:
				return basicSetShowPhysicalAsset(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return basicSetShowPhysicalAssetCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_CLASS:
				return basicSetShowPhysicalAssetClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_INFORMATION:
				return basicSetShowPhysicalAssetInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PROCESS_SEGMENT:
				return basicSetShowProcessSegment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PROCESS_SEGMENT_INFORMATION:
				return basicSetShowProcessSegmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_QUALIFICATION_TEST_SPECIFICATION:
				return basicSetShowQualificationTestSpecification(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT:
				return basicSetSyncEquipment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return basicSetSyncEquipmentCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_CLASS:
				return basicSetSyncEquipmentClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_INFORMATION:
				return basicSetSyncEquipmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_CLASS:
				return basicSetSyncMaterialClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_DEFINITION:
				return basicSetSyncMaterialDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_INFORMATION:
				return basicSetSyncMaterialInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_LOT:
				return basicSetSyncMaterialLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_SUB_LOT:
				return basicSetSyncMaterialSubLot(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_TEST_SPEC:
				return basicSetSyncMaterialTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_CAPABILITY:
				return basicSetSyncOperationsCapability(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_CAPABILITY_INFORMATION:
				return basicSetSyncOperationsCapabilityInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_DEFINITION:
				return basicSetSyncOperationsDefinition(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_DEFINITION_INFORMATION:
				return basicSetSyncOperationsDefinitionInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_PERFORMANCE:
				return basicSetSyncOperationsPerformance(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_SCHEDULE:
				return basicSetSyncOperationsSchedule(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSON:
				return basicSetSyncPerson(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSONNEL_CLASS:
				return basicSetSyncPersonnelClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSONNEL_INFORMATION:
				return basicSetSyncPersonnelInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET:
				return basicSetSyncPhysicalAsset(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return basicSetSyncPhysicalAssetCapabilityTestSpec(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_CLASS:
				return basicSetSyncPhysicalAssetClass(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_INFORMATION:
				return basicSetSyncPhysicalAssetInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PROCESS_SEGMENT:
				return basicSetSyncProcessSegment(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PROCESS_SEGMENT_INFORMATION:
				return basicSetSyncProcessSegmentInformation(null, msgs);
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_QUALIFICATION_TEST_SPECIFICATION:
				return basicSetSyncQualificationTestSpecification(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DOCUMENT_ROOT__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case B2MMLPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				if (coreType) return getXMLNSPrefixMap();
				else return getXMLNSPrefixMap().map();
			case B2MMLPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				if (coreType) return getXSISchemaLocation();
				else return getXSISchemaLocation().map();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT:
				return getAcknowledgeEquipment();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getAcknowledgeEquipmentCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_CLASS:
				return getAcknowledgeEquipmentClass();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_INFORMATION:
				return getAcknowledgeEquipmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_CLASS:
				return getAcknowledgeMaterialClass();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_DEFINITION:
				return getAcknowledgeMaterialDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_INFORMATION:
				return getAcknowledgeMaterialInformation();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_LOT:
				return getAcknowledgeMaterialLot();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_SUB_LOT:
				return getAcknowledgeMaterialSubLot();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_TEST_SPEC:
				return getAcknowledgeMaterialTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_CAPABILITY:
				return getAcknowledgeOperationsCapability();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_CAPABILITY_INFORMATION:
				return getAcknowledgeOperationsCapabilityInformation();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_DEFINITION:
				return getAcknowledgeOperationsDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_DEFINITION_INFORMATION:
				return getAcknowledgeOperationsDefinitionInformation();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_PERFORMANCE:
				return getAcknowledgeOperationsPerformance();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_SCHEDULE:
				return getAcknowledgeOperationsSchedule();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSON:
				return getAcknowledgePerson();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSONNEL_CLASS:
				return getAcknowledgePersonnelClass();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSONNEL_INFORMATION:
				return getAcknowledgePersonnelInformation();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET:
				return getAcknowledgePhysicalAsset();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getAcknowledgePhysicalAssetCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_CLASS:
				return getAcknowledgePhysicalAssetClass();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_INFORMATION:
				return getAcknowledgePhysicalAssetInformation();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PROCESS_SEGMENT:
				return getAcknowledgeProcessSegment();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PROCESS_SEGMENT_INFORMATION:
				return getAcknowledgeProcessSegmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_QUALIFICATION_TEST_SPECIFICATION:
				return getAcknowledgeQualificationTestSpecification();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT:
				return getCancelEquipment();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getCancelEquipmentCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_CLASS:
				return getCancelEquipmentClass();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_INFORMATION:
				return getCancelEquipmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_CLASS:
				return getCancelMaterialClass();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_DEFINITION:
				return getCancelMaterialDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_INFORMATION:
				return getCancelMaterialInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_LOT:
				return getCancelMaterialLot();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_SUB_LOT:
				return getCancelMaterialSubLot();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_TEST_SPEC:
				return getCancelMaterialTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_CAPABILITY:
				return getCancelOperationsCapability();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_CAPABILITY_INFORMATION:
				return getCancelOperationsCapabilityInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_DEFINITION:
				return getCancelOperationsDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_DEFINITION_INFORMATION:
				return getCancelOperationsDefinitionInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_PERFORMANCE:
				return getCancelOperationsPerformance();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_SCHEDULE:
				return getCancelOperationsSchedule();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSON:
				return getCancelPerson();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSONNEL_CLASS:
				return getCancelPersonnelClass();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSONNEL_INFORMATION:
				return getCancelPersonnelInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET:
				return getCancelPhysicalAsset();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getCancelPhysicalAssetCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_CLASS:
				return getCancelPhysicalAssetClass();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_INFORMATION:
				return getCancelPhysicalAssetInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PROCESS_SEGMENT:
				return getCancelProcessSegment();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PROCESS_SEGMENT_INFORMATION:
				return getCancelProcessSegmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_QUALIFICATION_TEST_SPECIFICATION:
				return getCancelQualificationTestSpecification();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT:
				return getChangeEquipment();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getChangeEquipmentCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_CLASS:
				return getChangeEquipmentClass();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_INFORMATION:
				return getChangeEquipmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_CLASS:
				return getChangeMaterialClass();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_DEFINITION:
				return getChangeMaterialDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_INFORMATION:
				return getChangeMaterialInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_LOT:
				return getChangeMaterialLot();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_SUB_LOT:
				return getChangeMaterialSubLot();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_TEST_SPEC:
				return getChangeMaterialTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_CAPABILITY:
				return getChangeOperationsCapability();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_CAPABILITY_INFORMATION:
				return getChangeOperationsCapabilityInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_DEFINITION:
				return getChangeOperationsDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_DEFINITION_INFORMATION:
				return getChangeOperationsDefinitionInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_PERFORMANCE:
				return getChangeOperationsPerformance();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_SCHEDULE:
				return getChangeOperationsSchedule();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSON:
				return getChangePerson();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSONNEL_CLASS:
				return getChangePersonnelClass();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSONNEL_INFORMATION:
				return getChangePersonnelInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET:
				return getChangePhysicalAsset();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getChangePhysicalAssetCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_CLASS:
				return getChangePhysicalAssetClass();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_INFORMATION:
				return getChangePhysicalAssetInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PROCESS_SEGMENT:
				return getChangeProcessSegment();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PROCESS_SEGMENT_INFORMATION:
				return getChangeProcessSegmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_QUALIFICATION_TEST_SPECIFICATION:
				return getChangeQualificationTestSpecification();
			case B2MMLPackage.DOCUMENT_ROOT__CONFIRM_BOD:
				return getConfirmBOD();
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT:
				return getEquipment();
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION:
				return getEquipmentCapabilityTestSpecification();
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_CLASS:
				return getEquipmentClass();
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_INFORMATION:
				return getEquipmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT:
				return getGetEquipment();
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getGetEquipmentCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_CLASS:
				return getGetEquipmentClass();
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_INFORMATION:
				return getGetEquipmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_CLASS:
				return getGetMaterialClass();
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_DEFINITION:
				return getGetMaterialDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_INFORMATION:
				return getGetMaterialInformation();
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_LOT:
				return getGetMaterialLot();
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_SUB_LOT:
				return getGetMaterialSubLot();
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_TEST_SPEC:
				return getGetMaterialTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_CAPABILITY:
				return getGetOperationsCapability();
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_CAPABILITY_INFORMATION:
				return getGetOperationsCapabilityInformation();
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_DEFINITION:
				return getGetOperationsDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_DEFINITION_INFORMATION:
				return getGetOperationsDefinitionInformation();
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_PERFORMANCE:
				return getGetOperationsPerformance();
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_SCHEDULE:
				return getGetOperationsSchedule();
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSON:
				return getGetPerson();
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSONNEL_CLASS:
				return getGetPersonnelClass();
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSONNEL_INFORMATION:
				return getGetPersonnelInformation();
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET:
				return getGetPhysicalAsset();
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getGetPhysicalAssetCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_CLASS:
				return getGetPhysicalAssetClass();
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_INFORMATION:
				return getGetPhysicalAssetInformation();
			case B2MMLPackage.DOCUMENT_ROOT__GET_PROCESS_SEGMENT:
				return getGetProcessSegment();
			case B2MMLPackage.DOCUMENT_ROOT__GET_PROCESS_SEGMENT_INFORMATION:
				return getGetProcessSegmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__GET_QUALIFICATION_TEST_SPECIFICATION:
				return getGetQualificationTestSpecification();
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_CLASS:
				return getMaterialClass();
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_DEFINITION:
				return getMaterialDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_INFORMATION:
				return getMaterialInformation();
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_LOT:
				return getMaterialLot();
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_SUB_LOT:
				return getMaterialSubLot();
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_TEST_SPECIFICATION:
				return getMaterialTestSpecification();
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_CAPABILITY:
				return getOperationsCapability();
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_CAPABILITY_INFORMATION:
				return getOperationsCapabilityInformation();
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_DEFINITION:
				return getOperationsDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_DEFINITION_INFORMATION:
				return getOperationsDefinitionInformation();
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_PERFORMANCE:
				return getOperationsPerformance();
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_REQUEST:
				return getOperationsRequest();
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_RESPONSE:
				return getOperationsResponse();
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_SCHEDULE:
				return getOperationsSchedule();
			case B2MMLPackage.DOCUMENT_ROOT__PERSON:
				return getPerson();
			case B2MMLPackage.DOCUMENT_ROOT__PERSONNEL_CLASS:
				return getPersonnelClass();
			case B2MMLPackage.DOCUMENT_ROOT__PERSONNEL_INFORMATION:
				return getPersonnelInformation();
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET:
				return getPhysicalAsset();
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION:
				return getPhysicalAssetCapabilityTestSpecification();
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_CLASS:
				return getPhysicalAssetClass();
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_INFORMATION:
				return getPhysicalAssetInformation();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT:
				return getProcessEquipment();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getProcessEquipmentCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_CLASS:
				return getProcessEquipmentClass();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_INFORMATION:
				return getProcessEquipmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_CLASS:
				return getProcessMaterialClass();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_DEFINITION:
				return getProcessMaterialDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_INFORMATION:
				return getProcessMaterialInformation();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_LOT:
				return getProcessMaterialLot();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_SUB_LOT:
				return getProcessMaterialSubLot();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_TEST_SPEC:
				return getProcessMaterialTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_CAPABILITY:
				return getProcessOperationsCapability();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_CAPABILITY_INFORMATION:
				return getProcessOperationsCapabilityInformation();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_DEFINITION:
				return getProcessOperationsDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_DEFINITION_INFORMATION:
				return getProcessOperationsDefinitionInformation();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_PERFORMANCE:
				return getProcessOperationsPerformance();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_SCHEDULE:
				return getProcessOperationsSchedule();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSON:
				return getProcessPerson();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSONNEL_CLASS:
				return getProcessPersonnelClass();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSONNEL_INFORMATION:
				return getProcessPersonnelInformation();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET:
				return getProcessPhysicalAsset();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getProcessPhysicalAssetCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_CLASS:
				return getProcessPhysicalAssetClass();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_INFORMATION:
				return getProcessPhysicalAssetInformation();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PROCESS_SEGMENT:
				return getProcessProcessSegment();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PROCESS_SEGMENT_INFORMATION:
				return getProcessProcessSegmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_QUALIFICATION_TEST_SPECIFICATION:
				return getProcessQualificationTestSpecification();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_SEGMENT:
				return getProcessSegment();
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_SEGMENT_INFORMATION:
				return getProcessSegmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__QUALIFICATION_TEST_SPECIFICATION:
				return getQualificationTestSpecification();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT:
				return getRespondEquipment();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getRespondEquipmentCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_CLASS:
				return getRespondEquipmentClass();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_INFORMATION:
				return getRespondEquipmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_CLASS:
				return getRespondMaterialClass();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_DEFINITION:
				return getRespondMaterialDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_INFORMATION:
				return getRespondMaterialInformation();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_LOT:
				return getRespondMaterialLot();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_SUB_LOT:
				return getRespondMaterialSubLot();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_TEST_SPEC:
				return getRespondMaterialTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_CAPABILITY:
				return getRespondOperationsCapability();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_CAPABILITY_INFORMATION:
				return getRespondOperationsCapabilityInformation();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_DEFINITION:
				return getRespondOperationsDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_DEFINITION_INFORMATION:
				return getRespondOperationsDefinitionInformation();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_PERFORMANCE:
				return getRespondOperationsPerformance();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_SCHEDULE:
				return getRespondOperationsSchedule();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSON:
				return getRespondPerson();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSONNEL_CLASS:
				return getRespondPersonnelClass();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSONNEL_INFORMATION:
				return getRespondPersonnelInformation();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET:
				return getRespondPhysicalAsset();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getRespondPhysicalAssetCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_CLASS:
				return getRespondPhysicalAssetClass();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_INFORMATION:
				return getRespondPhysicalAssetInformation();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PROCESS_SEGMENT:
				return getRespondProcessSegment();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PROCESS_SEGMENT_INFORMATION:
				return getRespondProcessSegmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_QUALIFICATION_TEST_SPECIFICATION:
				return getRespondQualificationTestSpecification();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT:
				return getShowEquipment();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getShowEquipmentCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_CLASS:
				return getShowEquipmentClass();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_INFORMATION:
				return getShowEquipmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_CLASS:
				return getShowMaterialClass();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_DEFINITION:
				return getShowMaterialDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_INFORMATION:
				return getShowMaterialInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_LOT:
				return getShowMaterialLot();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_SUB_LOT:
				return getShowMaterialSubLot();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_TEST_SPEC:
				return getShowMaterialTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_CAPABILITY:
				return getShowOperationsCapability();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_CAPABILITY_INFORMATION:
				return getShowOperationsCapabilityInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_DEFINITION:
				return getShowOperationsDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_DEFINITION_INFORMATION:
				return getShowOperationsDefinitionInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_PERFORMANCE:
				return getShowOperationsPerformance();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_SCHEDULE:
				return getShowOperationsSchedule();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSON:
				return getShowPerson();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSONNEL_CLASS:
				return getShowPersonnelClass();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSONNEL_INFORMATION:
				return getShowPersonnelInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET:
				return getShowPhysicalAsset();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getShowPhysicalAssetCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_CLASS:
				return getShowPhysicalAssetClass();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_INFORMATION:
				return getShowPhysicalAssetInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PROCESS_SEGMENT:
				return getShowProcessSegment();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PROCESS_SEGMENT_INFORMATION:
				return getShowProcessSegmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_QUALIFICATION_TEST_SPECIFICATION:
				return getShowQualificationTestSpecification();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT:
				return getSyncEquipment();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getSyncEquipmentCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_CLASS:
				return getSyncEquipmentClass();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_INFORMATION:
				return getSyncEquipmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_CLASS:
				return getSyncMaterialClass();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_DEFINITION:
				return getSyncMaterialDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_INFORMATION:
				return getSyncMaterialInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_LOT:
				return getSyncMaterialLot();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_SUB_LOT:
				return getSyncMaterialSubLot();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_TEST_SPEC:
				return getSyncMaterialTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_CAPABILITY:
				return getSyncOperationsCapability();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_CAPABILITY_INFORMATION:
				return getSyncOperationsCapabilityInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_DEFINITION:
				return getSyncOperationsDefinition();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_DEFINITION_INFORMATION:
				return getSyncOperationsDefinitionInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_PERFORMANCE:
				return getSyncOperationsPerformance();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_SCHEDULE:
				return getSyncOperationsSchedule();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSON:
				return getSyncPerson();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSONNEL_CLASS:
				return getSyncPersonnelClass();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSONNEL_INFORMATION:
				return getSyncPersonnelInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET:
				return getSyncPhysicalAsset();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getSyncPhysicalAssetCapabilityTestSpec();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_CLASS:
				return getSyncPhysicalAssetClass();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_INFORMATION:
				return getSyncPhysicalAssetInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PROCESS_SEGMENT:
				return getSyncProcessSegment();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PROCESS_SEGMENT_INFORMATION:
				return getSyncProcessSegmentInformation();
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_QUALIFICATION_TEST_SPECIFICATION:
				return getSyncQualificationTestSpecification();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DOCUMENT_ROOT__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				((EStructuralFeature.Setting)getXMLNSPrefixMap()).set(newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				((EStructuralFeature.Setting)getXSISchemaLocation()).set(newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT:
				setAcknowledgeEquipment((AcknowledgeEquipmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setAcknowledgeEquipmentCapabilityTestSpec((AcknowledgeEquipmentCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_CLASS:
				setAcknowledgeEquipmentClass((AcknowledgeEquipmentClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_INFORMATION:
				setAcknowledgeEquipmentInformation((AcknowledgeEquipmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_CLASS:
				setAcknowledgeMaterialClass((AcknowledgeMaterialClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_DEFINITION:
				setAcknowledgeMaterialDefinition((AcknowledgeMaterialDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_INFORMATION:
				setAcknowledgeMaterialInformation((AcknowledgeMaterialInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_LOT:
				setAcknowledgeMaterialLot((AcknowledgeMaterialLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_SUB_LOT:
				setAcknowledgeMaterialSubLot((AcknowledgeMaterialSubLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_TEST_SPEC:
				setAcknowledgeMaterialTestSpec((AcknowledgeMaterialTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_CAPABILITY:
				setAcknowledgeOperationsCapability((AcknowledgeOperationsCapabilityType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_CAPABILITY_INFORMATION:
				setAcknowledgeOperationsCapabilityInformation((AcknowledgeOperationsCapabilityInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_DEFINITION:
				setAcknowledgeOperationsDefinition((AcknowledgeOperationsDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_DEFINITION_INFORMATION:
				setAcknowledgeOperationsDefinitionInformation((AcknowledgeOperationsDefinitionInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_PERFORMANCE:
				setAcknowledgeOperationsPerformance((AcknowledgeOperationsPerformanceType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_SCHEDULE:
				setAcknowledgeOperationsSchedule((AcknowledgeOperationsScheduleType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSON:
				setAcknowledgePerson((AcknowledgePersonType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSONNEL_CLASS:
				setAcknowledgePersonnelClass((AcknowledgePersonnelClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSONNEL_INFORMATION:
				setAcknowledgePersonnelInformation((AcknowledgePersonnelInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET:
				setAcknowledgePhysicalAsset((AcknowledgePhysicalAssetType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setAcknowledgePhysicalAssetCapabilityTestSpec((AcknowledgePhysicalAssetCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_CLASS:
				setAcknowledgePhysicalAssetClass((AcknowledgePhysicalAssetClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_INFORMATION:
				setAcknowledgePhysicalAssetInformation((AcknowledgePhysicalAssetInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PROCESS_SEGMENT:
				setAcknowledgeProcessSegment((AcknowledgeProcessSegmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PROCESS_SEGMENT_INFORMATION:
				setAcknowledgeProcessSegmentInformation((AcknowledgeProcessSegmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_QUALIFICATION_TEST_SPECIFICATION:
				setAcknowledgeQualificationTestSpecification((AcknowledgeQualificationTestSpecificationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT:
				setCancelEquipment((CancelEquipmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setCancelEquipmentCapabilityTestSpec((CancelEquipmentCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_CLASS:
				setCancelEquipmentClass((CancelEquipmentClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_INFORMATION:
				setCancelEquipmentInformation((CancelEquipmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_CLASS:
				setCancelMaterialClass((CancelMaterialClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_DEFINITION:
				setCancelMaterialDefinition((CancelMaterialDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_INFORMATION:
				setCancelMaterialInformation((CancelMaterialInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_LOT:
				setCancelMaterialLot((CancelMaterialLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_SUB_LOT:
				setCancelMaterialSubLot((CancelMaterialSubLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_TEST_SPEC:
				setCancelMaterialTestSpec((CancelMaterialTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_CAPABILITY:
				setCancelOperationsCapability((CancelOperationsCapabilityType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_CAPABILITY_INFORMATION:
				setCancelOperationsCapabilityInformation((CancelOperationsCapabilityInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_DEFINITION:
				setCancelOperationsDefinition((CancelOperationsDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_DEFINITION_INFORMATION:
				setCancelOperationsDefinitionInformation((CancelOperationsDefinitionInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_PERFORMANCE:
				setCancelOperationsPerformance((CancelOperationsPerformanceType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_SCHEDULE:
				setCancelOperationsSchedule((CancelOperationsScheduleType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSON:
				setCancelPerson((CancelPersonType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSONNEL_CLASS:
				setCancelPersonnelClass((CancelPersonnelClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSONNEL_INFORMATION:
				setCancelPersonnelInformation((CancelPersonnelInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET:
				setCancelPhysicalAsset((CancelPhysicalAssetType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setCancelPhysicalAssetCapabilityTestSpec((CancelPhysicalAssetCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_CLASS:
				setCancelPhysicalAssetClass((CancelPhysicalAssetClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_INFORMATION:
				setCancelPhysicalAssetInformation((CancelPhysicalAssetInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PROCESS_SEGMENT:
				setCancelProcessSegment((CancelProcessSegmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PROCESS_SEGMENT_INFORMATION:
				setCancelProcessSegmentInformation((CancelProcessSegmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_QUALIFICATION_TEST_SPECIFICATION:
				setCancelQualificationTestSpecification((CancelQualificationTestSpecificationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT:
				setChangeEquipment((ChangeEquipmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setChangeEquipmentCapabilityTestSpec((ChangeEquipmentCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_CLASS:
				setChangeEquipmentClass((ChangeEquipmentClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_INFORMATION:
				setChangeEquipmentInformation((ChangeEquipmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_CLASS:
				setChangeMaterialClass((ChangeMaterialClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_DEFINITION:
				setChangeMaterialDefinition((ChangeMaterialDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_INFORMATION:
				setChangeMaterialInformation((ChangeMaterialInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_LOT:
				setChangeMaterialLot((ChangeMaterialLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_SUB_LOT:
				setChangeMaterialSubLot((ChangeMaterialSubLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_TEST_SPEC:
				setChangeMaterialTestSpec((ChangeMaterialTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_CAPABILITY:
				setChangeOperationsCapability((ChangeOperationsCapabilityType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_CAPABILITY_INFORMATION:
				setChangeOperationsCapabilityInformation((ChangeOperationsCapabilityInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_DEFINITION:
				setChangeOperationsDefinition((ChangeOperationsDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_DEFINITION_INFORMATION:
				setChangeOperationsDefinitionInformation((ChangeOperationsDefinitionInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_PERFORMANCE:
				setChangeOperationsPerformance((ChangeOperationsPerformanceType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_SCHEDULE:
				setChangeOperationsSchedule((ChangeOperationsScheduleType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSON:
				setChangePerson((ChangePersonType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSONNEL_CLASS:
				setChangePersonnelClass((ChangePersonnelClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSONNEL_INFORMATION:
				setChangePersonnelInformation((ChangePersonnelInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET:
				setChangePhysicalAsset((ChangePhysicalAssetType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setChangePhysicalAssetCapabilityTestSpec((ChangePhysicalAssetCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_CLASS:
				setChangePhysicalAssetClass((ChangePhysicalAssetClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_INFORMATION:
				setChangePhysicalAssetInformation((ChangePhysicalAssetInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PROCESS_SEGMENT:
				setChangeProcessSegment((ChangeProcessSegmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PROCESS_SEGMENT_INFORMATION:
				setChangeProcessSegmentInformation((ChangeProcessSegmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_QUALIFICATION_TEST_SPECIFICATION:
				setChangeQualificationTestSpecification((ChangeQualificationTestSpecificationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CONFIRM_BOD:
				setConfirmBOD((ConfirmBODType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT:
				setEquipment((EquipmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION:
				setEquipmentCapabilityTestSpecification((EquipmentCapabilityTestSpecificationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_CLASS:
				setEquipmentClass((EquipmentClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_INFORMATION:
				setEquipmentInformation((EquipmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT:
				setGetEquipment((GetEquipmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setGetEquipmentCapabilityTestSpec((GetEquipmentCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_CLASS:
				setGetEquipmentClass((GetEquipmentClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_INFORMATION:
				setGetEquipmentInformation((GetEquipmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_CLASS:
				setGetMaterialClass((GetMaterialClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_DEFINITION:
				setGetMaterialDefinition((GetMaterialDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_INFORMATION:
				setGetMaterialInformation((GetMaterialInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_LOT:
				setGetMaterialLot((GetMaterialLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_SUB_LOT:
				setGetMaterialSubLot((GetMaterialSubLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_TEST_SPEC:
				setGetMaterialTestSpec((GetMaterialTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_CAPABILITY:
				setGetOperationsCapability((GetOperationsCapabilityType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_CAPABILITY_INFORMATION:
				setGetOperationsCapabilityInformation((GetOperationsCapabilityInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_DEFINITION:
				setGetOperationsDefinition((GetOperationsDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_DEFINITION_INFORMATION:
				setGetOperationsDefinitionInformation((GetOperationsDefinitionInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_PERFORMANCE:
				setGetOperationsPerformance((GetOperationsPerformanceType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_SCHEDULE:
				setGetOperationsSchedule((GetOperationsScheduleType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSON:
				setGetPerson((GetPersonType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSONNEL_CLASS:
				setGetPersonnelClass((GetPersonnelClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSONNEL_INFORMATION:
				setGetPersonnelInformation((GetPersonnelInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET:
				setGetPhysicalAsset((GetPhysicalAssetType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setGetPhysicalAssetCapabilityTestSpec((GetPhysicalAssetCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_CLASS:
				setGetPhysicalAssetClass((GetPhysicalAssetClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_INFORMATION:
				setGetPhysicalAssetInformation((GetPhysicalAssetInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PROCESS_SEGMENT:
				setGetProcessSegment((GetProcessSegmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PROCESS_SEGMENT_INFORMATION:
				setGetProcessSegmentInformation((GetProcessSegmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_QUALIFICATION_TEST_SPECIFICATION:
				setGetQualificationTestSpecification((GetQualificationTestSpecificationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_CLASS:
				setMaterialClass((MaterialClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_DEFINITION:
				setMaterialDefinition((MaterialDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_INFORMATION:
				setMaterialInformation((MaterialInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_LOT:
				setMaterialLot((MaterialLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_SUB_LOT:
				setMaterialSubLot((MaterialSubLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_TEST_SPECIFICATION:
				setMaterialTestSpecification((MaterialTestSpecificationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_CAPABILITY:
				setOperationsCapability((OperationsCapabilityType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_CAPABILITY_INFORMATION:
				setOperationsCapabilityInformation((OperationsCapabilityInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_DEFINITION:
				setOperationsDefinition((OperationsDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_DEFINITION_INFORMATION:
				setOperationsDefinitionInformation((OperationsDefinitionInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_PERFORMANCE:
				setOperationsPerformance((OperationsPerformanceType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_REQUEST:
				setOperationsRequest((OperationsRequestType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_RESPONSE:
				setOperationsResponse((OperationsResponseType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_SCHEDULE:
				setOperationsSchedule((OperationsScheduleType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PERSON:
				setPerson((PersonType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PERSONNEL_CLASS:
				setPersonnelClass((PersonnelClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PERSONNEL_INFORMATION:
				setPersonnelInformation((PersonnelInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET:
				setPhysicalAsset((PhysicalAssetType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION:
				setPhysicalAssetCapabilityTestSpecification((PhysicalAssetCapabilityTestSpecificationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_CLASS:
				setPhysicalAssetClass((PhysicalAssetClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_INFORMATION:
				setPhysicalAssetInformation((PhysicalAssetInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT:
				setProcessEquipment((ProcessEquipmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setProcessEquipmentCapabilityTestSpec((ProcessEquipmentCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_CLASS:
				setProcessEquipmentClass((ProcessEquipmentClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_INFORMATION:
				setProcessEquipmentInformation((ProcessEquipmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_CLASS:
				setProcessMaterialClass((ProcessMaterialClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_DEFINITION:
				setProcessMaterialDefinition((ProcessMaterialDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_INFORMATION:
				setProcessMaterialInformation((ProcessMaterialInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_LOT:
				setProcessMaterialLot((ProcessMaterialLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_SUB_LOT:
				setProcessMaterialSubLot((ProcessMaterialSubLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_TEST_SPEC:
				setProcessMaterialTestSpec((ProcessMaterialTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_CAPABILITY:
				setProcessOperationsCapability((ProcessOperationsCapabilityType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_CAPABILITY_INFORMATION:
				setProcessOperationsCapabilityInformation((ProcessOperationsCapabilityInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_DEFINITION:
				setProcessOperationsDefinition((ProcessOperationsDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_DEFINITION_INFORMATION:
				setProcessOperationsDefinitionInformation((ProcessOperationsDefinitionInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_PERFORMANCE:
				setProcessOperationsPerformance((ProcessOperationsPerformanceType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_SCHEDULE:
				setProcessOperationsSchedule((ProcessOperationsScheduleType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSON:
				setProcessPerson((ProcessPersonType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSONNEL_CLASS:
				setProcessPersonnelClass((ProcessPersonnelClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSONNEL_INFORMATION:
				setProcessPersonnelInformation((ProcessPersonnelInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET:
				setProcessPhysicalAsset((ProcessPhysicalAssetType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setProcessPhysicalAssetCapabilityTestSpec((ProcessPhysicalAssetCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_CLASS:
				setProcessPhysicalAssetClass((ProcessPhysicalAssetClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_INFORMATION:
				setProcessPhysicalAssetInformation((ProcessPhysicalAssetInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PROCESS_SEGMENT:
				setProcessProcessSegment((ProcessProcessSegmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PROCESS_SEGMENT_INFORMATION:
				setProcessProcessSegmentInformation((ProcessProcessSegmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_QUALIFICATION_TEST_SPECIFICATION:
				setProcessQualificationTestSpecification((ProcessQualificationTestSpecificationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_SEGMENT:
				setProcessSegment((ProcessSegmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_SEGMENT_INFORMATION:
				setProcessSegmentInformation((ProcessSegmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__QUALIFICATION_TEST_SPECIFICATION:
				setQualificationTestSpecification((QualificationTestSpecificationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT:
				setRespondEquipment((RespondEquipmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setRespondEquipmentCapabilityTestSpec((RespondEquipmentCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_CLASS:
				setRespondEquipmentClass((RespondEquipmentClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_INFORMATION:
				setRespondEquipmentInformation((RespondEquipmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_CLASS:
				setRespondMaterialClass((RespondMaterialClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_DEFINITION:
				setRespondMaterialDefinition((RespondMaterialDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_INFORMATION:
				setRespondMaterialInformation((RespondMaterialInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_LOT:
				setRespondMaterialLot((RespondMaterialLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_SUB_LOT:
				setRespondMaterialSubLot((RespondMaterialSubLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_TEST_SPEC:
				setRespondMaterialTestSpec((RespondMaterialTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_CAPABILITY:
				setRespondOperationsCapability((RespondOperationsCapabilityType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_CAPABILITY_INFORMATION:
				setRespondOperationsCapabilityInformation((RespondOperationsCapabilityInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_DEFINITION:
				setRespondOperationsDefinition((RespondOperationsDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_DEFINITION_INFORMATION:
				setRespondOperationsDefinitionInformation((RespondOperationsDefinitionInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_PERFORMANCE:
				setRespondOperationsPerformance((RespondOperationsPerformanceType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_SCHEDULE:
				setRespondOperationsSchedule((RespondOperationsScheduleType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSON:
				setRespondPerson((RespondPersonType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSONNEL_CLASS:
				setRespondPersonnelClass((RespondPersonnelClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSONNEL_INFORMATION:
				setRespondPersonnelInformation((RespondPersonnelInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET:
				setRespondPhysicalAsset((RespondPhysicalAssetType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setRespondPhysicalAssetCapabilityTestSpec((RespondPhysicalAssetCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_CLASS:
				setRespondPhysicalAssetClass((RespondPhysicalAssetClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_INFORMATION:
				setRespondPhysicalAssetInformation((RespondPhysicalAssetInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PROCESS_SEGMENT:
				setRespondProcessSegment((RespondProcessSegmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PROCESS_SEGMENT_INFORMATION:
				setRespondProcessSegmentInformation((RespondProcessSegmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_QUALIFICATION_TEST_SPECIFICATION:
				setRespondQualificationTestSpecification((RespondQualificationTestSpecificationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT:
				setShowEquipment((ShowEquipmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setShowEquipmentCapabilityTestSpec((ShowEquipmentCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_CLASS:
				setShowEquipmentClass((ShowEquipmentClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_INFORMATION:
				setShowEquipmentInformation((ShowEquipmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_CLASS:
				setShowMaterialClass((ShowMaterialClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_DEFINITION:
				setShowMaterialDefinition((ShowMaterialDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_INFORMATION:
				setShowMaterialInformation((ShowMaterialInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_LOT:
				setShowMaterialLot((ShowMaterialLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_SUB_LOT:
				setShowMaterialSubLot((ShowMaterialSubLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_TEST_SPEC:
				setShowMaterialTestSpec((ShowMaterialTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_CAPABILITY:
				setShowOperationsCapability((ShowOperationsCapabilityType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_CAPABILITY_INFORMATION:
				setShowOperationsCapabilityInformation((ShowOperationsCapabilityInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_DEFINITION:
				setShowOperationsDefinition((ShowOperationsDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_DEFINITION_INFORMATION:
				setShowOperationsDefinitionInformation((ShowOperationsDefinitionInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_PERFORMANCE:
				setShowOperationsPerformance((ShowOperationsPerformanceType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_SCHEDULE:
				setShowOperationsSchedule((ShowOperationsScheduleType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSON:
				setShowPerson((ShowPersonType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSONNEL_CLASS:
				setShowPersonnelClass((ShowPersonnelClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSONNEL_INFORMATION:
				setShowPersonnelInformation((ShowPersonnelInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET:
				setShowPhysicalAsset((ShowPhysicalAssetType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setShowPhysicalAssetCapabilityTestSpec((ShowPhysicalAssetCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_CLASS:
				setShowPhysicalAssetClass((ShowPhysicalAssetClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_INFORMATION:
				setShowPhysicalAssetInformation((ShowPhysicalAssetInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PROCESS_SEGMENT:
				setShowProcessSegment((ShowProcessSegmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PROCESS_SEGMENT_INFORMATION:
				setShowProcessSegmentInformation((ShowProcessSegmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_QUALIFICATION_TEST_SPECIFICATION:
				setShowQualificationTestSpecification((ShowQualificationTestSpecificationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT:
				setSyncEquipment((SyncEquipmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setSyncEquipmentCapabilityTestSpec((SyncEquipmentCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_CLASS:
				setSyncEquipmentClass((SyncEquipmentClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_INFORMATION:
				setSyncEquipmentInformation((SyncEquipmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_CLASS:
				setSyncMaterialClass((SyncMaterialClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_DEFINITION:
				setSyncMaterialDefinition((SyncMaterialDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_INFORMATION:
				setSyncMaterialInformation((SyncMaterialInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_LOT:
				setSyncMaterialLot((SyncMaterialLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_SUB_LOT:
				setSyncMaterialSubLot((SyncMaterialSubLotType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_TEST_SPEC:
				setSyncMaterialTestSpec((SyncMaterialTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_CAPABILITY:
				setSyncOperationsCapability((SyncOperationsCapabilityType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_CAPABILITY_INFORMATION:
				setSyncOperationsCapabilityInformation((SyncOperationsCapabilityInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_DEFINITION:
				setSyncOperationsDefinition((SyncOperationsDefinitionType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_DEFINITION_INFORMATION:
				setSyncOperationsDefinitionInformation((SyncOperationsDefinitionInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_PERFORMANCE:
				setSyncOperationsPerformance((SyncOperationsPerformanceType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_SCHEDULE:
				setSyncOperationsSchedule((SyncOperationsScheduleType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSON:
				setSyncPerson((SyncPersonType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSONNEL_CLASS:
				setSyncPersonnelClass((SyncPersonnelClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSONNEL_INFORMATION:
				setSyncPersonnelInformation((SyncPersonnelInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET:
				setSyncPhysicalAsset((SyncPhysicalAssetType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setSyncPhysicalAssetCapabilityTestSpec((SyncPhysicalAssetCapabilityTestSpecType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_CLASS:
				setSyncPhysicalAssetClass((SyncPhysicalAssetClassType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_INFORMATION:
				setSyncPhysicalAssetInformation((SyncPhysicalAssetInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PROCESS_SEGMENT:
				setSyncProcessSegment((SyncProcessSegmentType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PROCESS_SEGMENT_INFORMATION:
				setSyncProcessSegmentInformation((SyncProcessSegmentInformationType)newValue);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_QUALIFICATION_TEST_SPECIFICATION:
				setSyncQualificationTestSpecification((SyncQualificationTestSpecificationType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DOCUMENT_ROOT__MIXED:
				getMixed().clear();
				return;
			case B2MMLPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				getXMLNSPrefixMap().clear();
				return;
			case B2MMLPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				getXSISchemaLocation().clear();
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT:
				setAcknowledgeEquipment((AcknowledgeEquipmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setAcknowledgeEquipmentCapabilityTestSpec((AcknowledgeEquipmentCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_CLASS:
				setAcknowledgeEquipmentClass((AcknowledgeEquipmentClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_INFORMATION:
				setAcknowledgeEquipmentInformation((AcknowledgeEquipmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_CLASS:
				setAcknowledgeMaterialClass((AcknowledgeMaterialClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_DEFINITION:
				setAcknowledgeMaterialDefinition((AcknowledgeMaterialDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_INFORMATION:
				setAcknowledgeMaterialInformation((AcknowledgeMaterialInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_LOT:
				setAcknowledgeMaterialLot((AcknowledgeMaterialLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_SUB_LOT:
				setAcknowledgeMaterialSubLot((AcknowledgeMaterialSubLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_TEST_SPEC:
				setAcknowledgeMaterialTestSpec((AcknowledgeMaterialTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_CAPABILITY:
				setAcknowledgeOperationsCapability((AcknowledgeOperationsCapabilityType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_CAPABILITY_INFORMATION:
				setAcknowledgeOperationsCapabilityInformation((AcknowledgeOperationsCapabilityInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_DEFINITION:
				setAcknowledgeOperationsDefinition((AcknowledgeOperationsDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_DEFINITION_INFORMATION:
				setAcknowledgeOperationsDefinitionInformation((AcknowledgeOperationsDefinitionInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_PERFORMANCE:
				setAcknowledgeOperationsPerformance((AcknowledgeOperationsPerformanceType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_SCHEDULE:
				setAcknowledgeOperationsSchedule((AcknowledgeOperationsScheduleType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSON:
				setAcknowledgePerson((AcknowledgePersonType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSONNEL_CLASS:
				setAcknowledgePersonnelClass((AcknowledgePersonnelClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSONNEL_INFORMATION:
				setAcknowledgePersonnelInformation((AcknowledgePersonnelInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET:
				setAcknowledgePhysicalAsset((AcknowledgePhysicalAssetType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setAcknowledgePhysicalAssetCapabilityTestSpec((AcknowledgePhysicalAssetCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_CLASS:
				setAcknowledgePhysicalAssetClass((AcknowledgePhysicalAssetClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_INFORMATION:
				setAcknowledgePhysicalAssetInformation((AcknowledgePhysicalAssetInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PROCESS_SEGMENT:
				setAcknowledgeProcessSegment((AcknowledgeProcessSegmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PROCESS_SEGMENT_INFORMATION:
				setAcknowledgeProcessSegmentInformation((AcknowledgeProcessSegmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_QUALIFICATION_TEST_SPECIFICATION:
				setAcknowledgeQualificationTestSpecification((AcknowledgeQualificationTestSpecificationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT:
				setCancelEquipment((CancelEquipmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setCancelEquipmentCapabilityTestSpec((CancelEquipmentCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_CLASS:
				setCancelEquipmentClass((CancelEquipmentClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_INFORMATION:
				setCancelEquipmentInformation((CancelEquipmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_CLASS:
				setCancelMaterialClass((CancelMaterialClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_DEFINITION:
				setCancelMaterialDefinition((CancelMaterialDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_INFORMATION:
				setCancelMaterialInformation((CancelMaterialInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_LOT:
				setCancelMaterialLot((CancelMaterialLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_SUB_LOT:
				setCancelMaterialSubLot((CancelMaterialSubLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_TEST_SPEC:
				setCancelMaterialTestSpec((CancelMaterialTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_CAPABILITY:
				setCancelOperationsCapability((CancelOperationsCapabilityType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_CAPABILITY_INFORMATION:
				setCancelOperationsCapabilityInformation((CancelOperationsCapabilityInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_DEFINITION:
				setCancelOperationsDefinition((CancelOperationsDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_DEFINITION_INFORMATION:
				setCancelOperationsDefinitionInformation((CancelOperationsDefinitionInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_PERFORMANCE:
				setCancelOperationsPerformance((CancelOperationsPerformanceType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_SCHEDULE:
				setCancelOperationsSchedule((CancelOperationsScheduleType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSON:
				setCancelPerson((CancelPersonType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSONNEL_CLASS:
				setCancelPersonnelClass((CancelPersonnelClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSONNEL_INFORMATION:
				setCancelPersonnelInformation((CancelPersonnelInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET:
				setCancelPhysicalAsset((CancelPhysicalAssetType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setCancelPhysicalAssetCapabilityTestSpec((CancelPhysicalAssetCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_CLASS:
				setCancelPhysicalAssetClass((CancelPhysicalAssetClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_INFORMATION:
				setCancelPhysicalAssetInformation((CancelPhysicalAssetInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PROCESS_SEGMENT:
				setCancelProcessSegment((CancelProcessSegmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PROCESS_SEGMENT_INFORMATION:
				setCancelProcessSegmentInformation((CancelProcessSegmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_QUALIFICATION_TEST_SPECIFICATION:
				setCancelQualificationTestSpecification((CancelQualificationTestSpecificationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT:
				setChangeEquipment((ChangeEquipmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setChangeEquipmentCapabilityTestSpec((ChangeEquipmentCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_CLASS:
				setChangeEquipmentClass((ChangeEquipmentClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_INFORMATION:
				setChangeEquipmentInformation((ChangeEquipmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_CLASS:
				setChangeMaterialClass((ChangeMaterialClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_DEFINITION:
				setChangeMaterialDefinition((ChangeMaterialDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_INFORMATION:
				setChangeMaterialInformation((ChangeMaterialInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_LOT:
				setChangeMaterialLot((ChangeMaterialLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_SUB_LOT:
				setChangeMaterialSubLot((ChangeMaterialSubLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_TEST_SPEC:
				setChangeMaterialTestSpec((ChangeMaterialTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_CAPABILITY:
				setChangeOperationsCapability((ChangeOperationsCapabilityType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_CAPABILITY_INFORMATION:
				setChangeOperationsCapabilityInformation((ChangeOperationsCapabilityInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_DEFINITION:
				setChangeOperationsDefinition((ChangeOperationsDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_DEFINITION_INFORMATION:
				setChangeOperationsDefinitionInformation((ChangeOperationsDefinitionInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_PERFORMANCE:
				setChangeOperationsPerformance((ChangeOperationsPerformanceType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_SCHEDULE:
				setChangeOperationsSchedule((ChangeOperationsScheduleType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSON:
				setChangePerson((ChangePersonType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSONNEL_CLASS:
				setChangePersonnelClass((ChangePersonnelClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSONNEL_INFORMATION:
				setChangePersonnelInformation((ChangePersonnelInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET:
				setChangePhysicalAsset((ChangePhysicalAssetType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setChangePhysicalAssetCapabilityTestSpec((ChangePhysicalAssetCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_CLASS:
				setChangePhysicalAssetClass((ChangePhysicalAssetClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_INFORMATION:
				setChangePhysicalAssetInformation((ChangePhysicalAssetInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PROCESS_SEGMENT:
				setChangeProcessSegment((ChangeProcessSegmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PROCESS_SEGMENT_INFORMATION:
				setChangeProcessSegmentInformation((ChangeProcessSegmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_QUALIFICATION_TEST_SPECIFICATION:
				setChangeQualificationTestSpecification((ChangeQualificationTestSpecificationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__CONFIRM_BOD:
				setConfirmBOD((ConfirmBODType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT:
				setEquipment((EquipmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION:
				setEquipmentCapabilityTestSpecification((EquipmentCapabilityTestSpecificationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_CLASS:
				setEquipmentClass((EquipmentClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_INFORMATION:
				setEquipmentInformation((EquipmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT:
				setGetEquipment((GetEquipmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setGetEquipmentCapabilityTestSpec((GetEquipmentCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_CLASS:
				setGetEquipmentClass((GetEquipmentClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_INFORMATION:
				setGetEquipmentInformation((GetEquipmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_CLASS:
				setGetMaterialClass((GetMaterialClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_DEFINITION:
				setGetMaterialDefinition((GetMaterialDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_INFORMATION:
				setGetMaterialInformation((GetMaterialInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_LOT:
				setGetMaterialLot((GetMaterialLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_SUB_LOT:
				setGetMaterialSubLot((GetMaterialSubLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_TEST_SPEC:
				setGetMaterialTestSpec((GetMaterialTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_CAPABILITY:
				setGetOperationsCapability((GetOperationsCapabilityType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_CAPABILITY_INFORMATION:
				setGetOperationsCapabilityInformation((GetOperationsCapabilityInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_DEFINITION:
				setGetOperationsDefinition((GetOperationsDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_DEFINITION_INFORMATION:
				setGetOperationsDefinitionInformation((GetOperationsDefinitionInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_PERFORMANCE:
				setGetOperationsPerformance((GetOperationsPerformanceType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_SCHEDULE:
				setGetOperationsSchedule((GetOperationsScheduleType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSON:
				setGetPerson((GetPersonType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSONNEL_CLASS:
				setGetPersonnelClass((GetPersonnelClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSONNEL_INFORMATION:
				setGetPersonnelInformation((GetPersonnelInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET:
				setGetPhysicalAsset((GetPhysicalAssetType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setGetPhysicalAssetCapabilityTestSpec((GetPhysicalAssetCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_CLASS:
				setGetPhysicalAssetClass((GetPhysicalAssetClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_INFORMATION:
				setGetPhysicalAssetInformation((GetPhysicalAssetInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PROCESS_SEGMENT:
				setGetProcessSegment((GetProcessSegmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PROCESS_SEGMENT_INFORMATION:
				setGetProcessSegmentInformation((GetProcessSegmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__GET_QUALIFICATION_TEST_SPECIFICATION:
				setGetQualificationTestSpecification((GetQualificationTestSpecificationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_CLASS:
				setMaterialClass((MaterialClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_DEFINITION:
				setMaterialDefinition((MaterialDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_INFORMATION:
				setMaterialInformation((MaterialInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_LOT:
				setMaterialLot((MaterialLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_SUB_LOT:
				setMaterialSubLot((MaterialSubLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_TEST_SPECIFICATION:
				setMaterialTestSpecification((MaterialTestSpecificationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_CAPABILITY:
				setOperationsCapability((OperationsCapabilityType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_CAPABILITY_INFORMATION:
				setOperationsCapabilityInformation((OperationsCapabilityInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_DEFINITION:
				setOperationsDefinition((OperationsDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_DEFINITION_INFORMATION:
				setOperationsDefinitionInformation((OperationsDefinitionInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_PERFORMANCE:
				setOperationsPerformance((OperationsPerformanceType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_REQUEST:
				setOperationsRequest((OperationsRequestType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_RESPONSE:
				setOperationsResponse((OperationsResponseType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_SCHEDULE:
				setOperationsSchedule((OperationsScheduleType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PERSON:
				setPerson((PersonType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PERSONNEL_CLASS:
				setPersonnelClass((PersonnelClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PERSONNEL_INFORMATION:
				setPersonnelInformation((PersonnelInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET:
				setPhysicalAsset((PhysicalAssetType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION:
				setPhysicalAssetCapabilityTestSpecification((PhysicalAssetCapabilityTestSpecificationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_CLASS:
				setPhysicalAssetClass((PhysicalAssetClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_INFORMATION:
				setPhysicalAssetInformation((PhysicalAssetInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT:
				setProcessEquipment((ProcessEquipmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setProcessEquipmentCapabilityTestSpec((ProcessEquipmentCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_CLASS:
				setProcessEquipmentClass((ProcessEquipmentClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_INFORMATION:
				setProcessEquipmentInformation((ProcessEquipmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_CLASS:
				setProcessMaterialClass((ProcessMaterialClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_DEFINITION:
				setProcessMaterialDefinition((ProcessMaterialDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_INFORMATION:
				setProcessMaterialInformation((ProcessMaterialInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_LOT:
				setProcessMaterialLot((ProcessMaterialLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_SUB_LOT:
				setProcessMaterialSubLot((ProcessMaterialSubLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_TEST_SPEC:
				setProcessMaterialTestSpec((ProcessMaterialTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_CAPABILITY:
				setProcessOperationsCapability((ProcessOperationsCapabilityType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_CAPABILITY_INFORMATION:
				setProcessOperationsCapabilityInformation((ProcessOperationsCapabilityInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_DEFINITION:
				setProcessOperationsDefinition((ProcessOperationsDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_DEFINITION_INFORMATION:
				setProcessOperationsDefinitionInformation((ProcessOperationsDefinitionInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_PERFORMANCE:
				setProcessOperationsPerformance((ProcessOperationsPerformanceType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_SCHEDULE:
				setProcessOperationsSchedule((ProcessOperationsScheduleType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSON:
				setProcessPerson((ProcessPersonType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSONNEL_CLASS:
				setProcessPersonnelClass((ProcessPersonnelClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSONNEL_INFORMATION:
				setProcessPersonnelInformation((ProcessPersonnelInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET:
				setProcessPhysicalAsset((ProcessPhysicalAssetType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setProcessPhysicalAssetCapabilityTestSpec((ProcessPhysicalAssetCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_CLASS:
				setProcessPhysicalAssetClass((ProcessPhysicalAssetClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_INFORMATION:
				setProcessPhysicalAssetInformation((ProcessPhysicalAssetInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PROCESS_SEGMENT:
				setProcessProcessSegment((ProcessProcessSegmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PROCESS_SEGMENT_INFORMATION:
				setProcessProcessSegmentInformation((ProcessProcessSegmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_QUALIFICATION_TEST_SPECIFICATION:
				setProcessQualificationTestSpecification((ProcessQualificationTestSpecificationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_SEGMENT:
				setProcessSegment((ProcessSegmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_SEGMENT_INFORMATION:
				setProcessSegmentInformation((ProcessSegmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__QUALIFICATION_TEST_SPECIFICATION:
				setQualificationTestSpecification((QualificationTestSpecificationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT:
				setRespondEquipment((RespondEquipmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setRespondEquipmentCapabilityTestSpec((RespondEquipmentCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_CLASS:
				setRespondEquipmentClass((RespondEquipmentClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_INFORMATION:
				setRespondEquipmentInformation((RespondEquipmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_CLASS:
				setRespondMaterialClass((RespondMaterialClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_DEFINITION:
				setRespondMaterialDefinition((RespondMaterialDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_INFORMATION:
				setRespondMaterialInformation((RespondMaterialInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_LOT:
				setRespondMaterialLot((RespondMaterialLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_SUB_LOT:
				setRespondMaterialSubLot((RespondMaterialSubLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_TEST_SPEC:
				setRespondMaterialTestSpec((RespondMaterialTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_CAPABILITY:
				setRespondOperationsCapability((RespondOperationsCapabilityType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_CAPABILITY_INFORMATION:
				setRespondOperationsCapabilityInformation((RespondOperationsCapabilityInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_DEFINITION:
				setRespondOperationsDefinition((RespondOperationsDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_DEFINITION_INFORMATION:
				setRespondOperationsDefinitionInformation((RespondOperationsDefinitionInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_PERFORMANCE:
				setRespondOperationsPerformance((RespondOperationsPerformanceType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_SCHEDULE:
				setRespondOperationsSchedule((RespondOperationsScheduleType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSON:
				setRespondPerson((RespondPersonType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSONNEL_CLASS:
				setRespondPersonnelClass((RespondPersonnelClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSONNEL_INFORMATION:
				setRespondPersonnelInformation((RespondPersonnelInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET:
				setRespondPhysicalAsset((RespondPhysicalAssetType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setRespondPhysicalAssetCapabilityTestSpec((RespondPhysicalAssetCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_CLASS:
				setRespondPhysicalAssetClass((RespondPhysicalAssetClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_INFORMATION:
				setRespondPhysicalAssetInformation((RespondPhysicalAssetInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PROCESS_SEGMENT:
				setRespondProcessSegment((RespondProcessSegmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PROCESS_SEGMENT_INFORMATION:
				setRespondProcessSegmentInformation((RespondProcessSegmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_QUALIFICATION_TEST_SPECIFICATION:
				setRespondQualificationTestSpecification((RespondQualificationTestSpecificationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT:
				setShowEquipment((ShowEquipmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setShowEquipmentCapabilityTestSpec((ShowEquipmentCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_CLASS:
				setShowEquipmentClass((ShowEquipmentClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_INFORMATION:
				setShowEquipmentInformation((ShowEquipmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_CLASS:
				setShowMaterialClass((ShowMaterialClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_DEFINITION:
				setShowMaterialDefinition((ShowMaterialDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_INFORMATION:
				setShowMaterialInformation((ShowMaterialInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_LOT:
				setShowMaterialLot((ShowMaterialLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_SUB_LOT:
				setShowMaterialSubLot((ShowMaterialSubLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_TEST_SPEC:
				setShowMaterialTestSpec((ShowMaterialTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_CAPABILITY:
				setShowOperationsCapability((ShowOperationsCapabilityType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_CAPABILITY_INFORMATION:
				setShowOperationsCapabilityInformation((ShowOperationsCapabilityInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_DEFINITION:
				setShowOperationsDefinition((ShowOperationsDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_DEFINITION_INFORMATION:
				setShowOperationsDefinitionInformation((ShowOperationsDefinitionInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_PERFORMANCE:
				setShowOperationsPerformance((ShowOperationsPerformanceType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_SCHEDULE:
				setShowOperationsSchedule((ShowOperationsScheduleType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSON:
				setShowPerson((ShowPersonType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSONNEL_CLASS:
				setShowPersonnelClass((ShowPersonnelClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSONNEL_INFORMATION:
				setShowPersonnelInformation((ShowPersonnelInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET:
				setShowPhysicalAsset((ShowPhysicalAssetType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setShowPhysicalAssetCapabilityTestSpec((ShowPhysicalAssetCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_CLASS:
				setShowPhysicalAssetClass((ShowPhysicalAssetClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_INFORMATION:
				setShowPhysicalAssetInformation((ShowPhysicalAssetInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PROCESS_SEGMENT:
				setShowProcessSegment((ShowProcessSegmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PROCESS_SEGMENT_INFORMATION:
				setShowProcessSegmentInformation((ShowProcessSegmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_QUALIFICATION_TEST_SPECIFICATION:
				setShowQualificationTestSpecification((ShowQualificationTestSpecificationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT:
				setSyncEquipment((SyncEquipmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_CAPABILITY_TEST_SPEC:
				setSyncEquipmentCapabilityTestSpec((SyncEquipmentCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_CLASS:
				setSyncEquipmentClass((SyncEquipmentClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_INFORMATION:
				setSyncEquipmentInformation((SyncEquipmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_CLASS:
				setSyncMaterialClass((SyncMaterialClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_DEFINITION:
				setSyncMaterialDefinition((SyncMaterialDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_INFORMATION:
				setSyncMaterialInformation((SyncMaterialInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_LOT:
				setSyncMaterialLot((SyncMaterialLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_SUB_LOT:
				setSyncMaterialSubLot((SyncMaterialSubLotType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_TEST_SPEC:
				setSyncMaterialTestSpec((SyncMaterialTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_CAPABILITY:
				setSyncOperationsCapability((SyncOperationsCapabilityType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_CAPABILITY_INFORMATION:
				setSyncOperationsCapabilityInformation((SyncOperationsCapabilityInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_DEFINITION:
				setSyncOperationsDefinition((SyncOperationsDefinitionType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_DEFINITION_INFORMATION:
				setSyncOperationsDefinitionInformation((SyncOperationsDefinitionInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_PERFORMANCE:
				setSyncOperationsPerformance((SyncOperationsPerformanceType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_SCHEDULE:
				setSyncOperationsSchedule((SyncOperationsScheduleType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSON:
				setSyncPerson((SyncPersonType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSONNEL_CLASS:
				setSyncPersonnelClass((SyncPersonnelClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSONNEL_INFORMATION:
				setSyncPersonnelInformation((SyncPersonnelInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET:
				setSyncPhysicalAsset((SyncPhysicalAssetType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				setSyncPhysicalAssetCapabilityTestSpec((SyncPhysicalAssetCapabilityTestSpecType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_CLASS:
				setSyncPhysicalAssetClass((SyncPhysicalAssetClassType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_INFORMATION:
				setSyncPhysicalAssetInformation((SyncPhysicalAssetInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PROCESS_SEGMENT:
				setSyncProcessSegment((SyncProcessSegmentType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PROCESS_SEGMENT_INFORMATION:
				setSyncProcessSegmentInformation((SyncProcessSegmentInformationType)null);
				return;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_QUALIFICATION_TEST_SPECIFICATION:
				setSyncQualificationTestSpecification((SyncQualificationTestSpecificationType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DOCUMENT_ROOT__MIXED:
				return mixed != null && !mixed.isEmpty();
			case B2MMLPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return xMLNSPrefixMap != null && !xMLNSPrefixMap.isEmpty();
			case B2MMLPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return xSISchemaLocation != null && !xSISchemaLocation.isEmpty();
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT:
				return getAcknowledgeEquipment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getAcknowledgeEquipmentCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_CLASS:
				return getAcknowledgeEquipmentClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_EQUIPMENT_INFORMATION:
				return getAcknowledgeEquipmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_CLASS:
				return getAcknowledgeMaterialClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_DEFINITION:
				return getAcknowledgeMaterialDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_INFORMATION:
				return getAcknowledgeMaterialInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_LOT:
				return getAcknowledgeMaterialLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_SUB_LOT:
				return getAcknowledgeMaterialSubLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_MATERIAL_TEST_SPEC:
				return getAcknowledgeMaterialTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_CAPABILITY:
				return getAcknowledgeOperationsCapability() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_CAPABILITY_INFORMATION:
				return getAcknowledgeOperationsCapabilityInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_DEFINITION:
				return getAcknowledgeOperationsDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_DEFINITION_INFORMATION:
				return getAcknowledgeOperationsDefinitionInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_PERFORMANCE:
				return getAcknowledgeOperationsPerformance() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_OPERATIONS_SCHEDULE:
				return getAcknowledgeOperationsSchedule() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSON:
				return getAcknowledgePerson() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSONNEL_CLASS:
				return getAcknowledgePersonnelClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PERSONNEL_INFORMATION:
				return getAcknowledgePersonnelInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET:
				return getAcknowledgePhysicalAsset() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getAcknowledgePhysicalAssetCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_CLASS:
				return getAcknowledgePhysicalAssetClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PHYSICAL_ASSET_INFORMATION:
				return getAcknowledgePhysicalAssetInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PROCESS_SEGMENT:
				return getAcknowledgeProcessSegment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_PROCESS_SEGMENT_INFORMATION:
				return getAcknowledgeProcessSegmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__ACKNOWLEDGE_QUALIFICATION_TEST_SPECIFICATION:
				return getAcknowledgeQualificationTestSpecification() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT:
				return getCancelEquipment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getCancelEquipmentCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_CLASS:
				return getCancelEquipmentClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_EQUIPMENT_INFORMATION:
				return getCancelEquipmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_CLASS:
				return getCancelMaterialClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_DEFINITION:
				return getCancelMaterialDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_INFORMATION:
				return getCancelMaterialInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_LOT:
				return getCancelMaterialLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_SUB_LOT:
				return getCancelMaterialSubLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_MATERIAL_TEST_SPEC:
				return getCancelMaterialTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_CAPABILITY:
				return getCancelOperationsCapability() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_CAPABILITY_INFORMATION:
				return getCancelOperationsCapabilityInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_DEFINITION:
				return getCancelOperationsDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_DEFINITION_INFORMATION:
				return getCancelOperationsDefinitionInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_PERFORMANCE:
				return getCancelOperationsPerformance() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_OPERATIONS_SCHEDULE:
				return getCancelOperationsSchedule() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSON:
				return getCancelPerson() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSONNEL_CLASS:
				return getCancelPersonnelClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PERSONNEL_INFORMATION:
				return getCancelPersonnelInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET:
				return getCancelPhysicalAsset() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getCancelPhysicalAssetCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_CLASS:
				return getCancelPhysicalAssetClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PHYSICAL_ASSET_INFORMATION:
				return getCancelPhysicalAssetInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PROCESS_SEGMENT:
				return getCancelProcessSegment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_PROCESS_SEGMENT_INFORMATION:
				return getCancelProcessSegmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CANCEL_QUALIFICATION_TEST_SPECIFICATION:
				return getCancelQualificationTestSpecification() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT:
				return getChangeEquipment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getChangeEquipmentCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_CLASS:
				return getChangeEquipmentClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_EQUIPMENT_INFORMATION:
				return getChangeEquipmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_CLASS:
				return getChangeMaterialClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_DEFINITION:
				return getChangeMaterialDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_INFORMATION:
				return getChangeMaterialInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_LOT:
				return getChangeMaterialLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_SUB_LOT:
				return getChangeMaterialSubLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_MATERIAL_TEST_SPEC:
				return getChangeMaterialTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_CAPABILITY:
				return getChangeOperationsCapability() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_CAPABILITY_INFORMATION:
				return getChangeOperationsCapabilityInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_DEFINITION:
				return getChangeOperationsDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_DEFINITION_INFORMATION:
				return getChangeOperationsDefinitionInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_PERFORMANCE:
				return getChangeOperationsPerformance() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_OPERATIONS_SCHEDULE:
				return getChangeOperationsSchedule() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSON:
				return getChangePerson() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSONNEL_CLASS:
				return getChangePersonnelClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PERSONNEL_INFORMATION:
				return getChangePersonnelInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET:
				return getChangePhysicalAsset() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getChangePhysicalAssetCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_CLASS:
				return getChangePhysicalAssetClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PHYSICAL_ASSET_INFORMATION:
				return getChangePhysicalAssetInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PROCESS_SEGMENT:
				return getChangeProcessSegment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_PROCESS_SEGMENT_INFORMATION:
				return getChangeProcessSegmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CHANGE_QUALIFICATION_TEST_SPECIFICATION:
				return getChangeQualificationTestSpecification() != null;
			case B2MMLPackage.DOCUMENT_ROOT__CONFIRM_BOD:
				return getConfirmBOD() != null;
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT:
				return getEquipment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION:
				return getEquipmentCapabilityTestSpecification() != null;
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_CLASS:
				return getEquipmentClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__EQUIPMENT_INFORMATION:
				return getEquipmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT:
				return getGetEquipment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getGetEquipmentCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_CLASS:
				return getGetEquipmentClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_EQUIPMENT_INFORMATION:
				return getGetEquipmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_CLASS:
				return getGetMaterialClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_DEFINITION:
				return getGetMaterialDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_INFORMATION:
				return getGetMaterialInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_LOT:
				return getGetMaterialLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_SUB_LOT:
				return getGetMaterialSubLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_MATERIAL_TEST_SPEC:
				return getGetMaterialTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_CAPABILITY:
				return getGetOperationsCapability() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_CAPABILITY_INFORMATION:
				return getGetOperationsCapabilityInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_DEFINITION:
				return getGetOperationsDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_DEFINITION_INFORMATION:
				return getGetOperationsDefinitionInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_PERFORMANCE:
				return getGetOperationsPerformance() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_OPERATIONS_SCHEDULE:
				return getGetOperationsSchedule() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSON:
				return getGetPerson() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSONNEL_CLASS:
				return getGetPersonnelClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PERSONNEL_INFORMATION:
				return getGetPersonnelInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET:
				return getGetPhysicalAsset() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getGetPhysicalAssetCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_CLASS:
				return getGetPhysicalAssetClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PHYSICAL_ASSET_INFORMATION:
				return getGetPhysicalAssetInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PROCESS_SEGMENT:
				return getGetProcessSegment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_PROCESS_SEGMENT_INFORMATION:
				return getGetProcessSegmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__GET_QUALIFICATION_TEST_SPECIFICATION:
				return getGetQualificationTestSpecification() != null;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_CLASS:
				return getMaterialClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_DEFINITION:
				return getMaterialDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_INFORMATION:
				return getMaterialInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_LOT:
				return getMaterialLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_SUB_LOT:
				return getMaterialSubLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__MATERIAL_TEST_SPECIFICATION:
				return getMaterialTestSpecification() != null;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_CAPABILITY:
				return getOperationsCapability() != null;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_CAPABILITY_INFORMATION:
				return getOperationsCapabilityInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_DEFINITION:
				return getOperationsDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_DEFINITION_INFORMATION:
				return getOperationsDefinitionInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_PERFORMANCE:
				return getOperationsPerformance() != null;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_REQUEST:
				return getOperationsRequest() != null;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_RESPONSE:
				return getOperationsResponse() != null;
			case B2MMLPackage.DOCUMENT_ROOT__OPERATIONS_SCHEDULE:
				return getOperationsSchedule() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PERSON:
				return getPerson() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PERSONNEL_CLASS:
				return getPersonnelClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PERSONNEL_INFORMATION:
				return getPersonnelInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET:
				return getPhysicalAsset() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION:
				return getPhysicalAssetCapabilityTestSpecification() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_CLASS:
				return getPhysicalAssetClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PHYSICAL_ASSET_INFORMATION:
				return getPhysicalAssetInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT:
				return getProcessEquipment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getProcessEquipmentCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_CLASS:
				return getProcessEquipmentClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_EQUIPMENT_INFORMATION:
				return getProcessEquipmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_CLASS:
				return getProcessMaterialClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_DEFINITION:
				return getProcessMaterialDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_INFORMATION:
				return getProcessMaterialInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_LOT:
				return getProcessMaterialLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_SUB_LOT:
				return getProcessMaterialSubLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_MATERIAL_TEST_SPEC:
				return getProcessMaterialTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_CAPABILITY:
				return getProcessOperationsCapability() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_CAPABILITY_INFORMATION:
				return getProcessOperationsCapabilityInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_DEFINITION:
				return getProcessOperationsDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_DEFINITION_INFORMATION:
				return getProcessOperationsDefinitionInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_PERFORMANCE:
				return getProcessOperationsPerformance() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_OPERATIONS_SCHEDULE:
				return getProcessOperationsSchedule() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSON:
				return getProcessPerson() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSONNEL_CLASS:
				return getProcessPersonnelClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PERSONNEL_INFORMATION:
				return getProcessPersonnelInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET:
				return getProcessPhysicalAsset() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getProcessPhysicalAssetCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_CLASS:
				return getProcessPhysicalAssetClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PHYSICAL_ASSET_INFORMATION:
				return getProcessPhysicalAssetInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PROCESS_SEGMENT:
				return getProcessProcessSegment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_PROCESS_SEGMENT_INFORMATION:
				return getProcessProcessSegmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_QUALIFICATION_TEST_SPECIFICATION:
				return getProcessQualificationTestSpecification() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_SEGMENT:
				return getProcessSegment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__PROCESS_SEGMENT_INFORMATION:
				return getProcessSegmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__QUALIFICATION_TEST_SPECIFICATION:
				return getQualificationTestSpecification() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT:
				return getRespondEquipment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getRespondEquipmentCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_CLASS:
				return getRespondEquipmentClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_EQUIPMENT_INFORMATION:
				return getRespondEquipmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_CLASS:
				return getRespondMaterialClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_DEFINITION:
				return getRespondMaterialDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_INFORMATION:
				return getRespondMaterialInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_LOT:
				return getRespondMaterialLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_SUB_LOT:
				return getRespondMaterialSubLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_MATERIAL_TEST_SPEC:
				return getRespondMaterialTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_CAPABILITY:
				return getRespondOperationsCapability() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_CAPABILITY_INFORMATION:
				return getRespondOperationsCapabilityInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_DEFINITION:
				return getRespondOperationsDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_DEFINITION_INFORMATION:
				return getRespondOperationsDefinitionInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_PERFORMANCE:
				return getRespondOperationsPerformance() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_OPERATIONS_SCHEDULE:
				return getRespondOperationsSchedule() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSON:
				return getRespondPerson() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSONNEL_CLASS:
				return getRespondPersonnelClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PERSONNEL_INFORMATION:
				return getRespondPersonnelInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET:
				return getRespondPhysicalAsset() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getRespondPhysicalAssetCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_CLASS:
				return getRespondPhysicalAssetClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PHYSICAL_ASSET_INFORMATION:
				return getRespondPhysicalAssetInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PROCESS_SEGMENT:
				return getRespondProcessSegment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_PROCESS_SEGMENT_INFORMATION:
				return getRespondProcessSegmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__RESPOND_QUALIFICATION_TEST_SPECIFICATION:
				return getRespondQualificationTestSpecification() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT:
				return getShowEquipment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getShowEquipmentCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_CLASS:
				return getShowEquipmentClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_EQUIPMENT_INFORMATION:
				return getShowEquipmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_CLASS:
				return getShowMaterialClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_DEFINITION:
				return getShowMaterialDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_INFORMATION:
				return getShowMaterialInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_LOT:
				return getShowMaterialLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_SUB_LOT:
				return getShowMaterialSubLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_MATERIAL_TEST_SPEC:
				return getShowMaterialTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_CAPABILITY:
				return getShowOperationsCapability() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_CAPABILITY_INFORMATION:
				return getShowOperationsCapabilityInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_DEFINITION:
				return getShowOperationsDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_DEFINITION_INFORMATION:
				return getShowOperationsDefinitionInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_PERFORMANCE:
				return getShowOperationsPerformance() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_OPERATIONS_SCHEDULE:
				return getShowOperationsSchedule() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSON:
				return getShowPerson() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSONNEL_CLASS:
				return getShowPersonnelClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PERSONNEL_INFORMATION:
				return getShowPersonnelInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET:
				return getShowPhysicalAsset() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getShowPhysicalAssetCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_CLASS:
				return getShowPhysicalAssetClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PHYSICAL_ASSET_INFORMATION:
				return getShowPhysicalAssetInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PROCESS_SEGMENT:
				return getShowProcessSegment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_PROCESS_SEGMENT_INFORMATION:
				return getShowProcessSegmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SHOW_QUALIFICATION_TEST_SPECIFICATION:
				return getShowQualificationTestSpecification() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT:
				return getSyncEquipment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getSyncEquipmentCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_CLASS:
				return getSyncEquipmentClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_EQUIPMENT_INFORMATION:
				return getSyncEquipmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_CLASS:
				return getSyncMaterialClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_DEFINITION:
				return getSyncMaterialDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_INFORMATION:
				return getSyncMaterialInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_LOT:
				return getSyncMaterialLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_SUB_LOT:
				return getSyncMaterialSubLot() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_MATERIAL_TEST_SPEC:
				return getSyncMaterialTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_CAPABILITY:
				return getSyncOperationsCapability() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_CAPABILITY_INFORMATION:
				return getSyncOperationsCapabilityInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_DEFINITION:
				return getSyncOperationsDefinition() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_DEFINITION_INFORMATION:
				return getSyncOperationsDefinitionInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_PERFORMANCE:
				return getSyncOperationsPerformance() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_OPERATIONS_SCHEDULE:
				return getSyncOperationsSchedule() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSON:
				return getSyncPerson() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSONNEL_CLASS:
				return getSyncPersonnelClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PERSONNEL_INFORMATION:
				return getSyncPersonnelInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET:
				return getSyncPhysicalAsset() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getSyncPhysicalAssetCapabilityTestSpec() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_CLASS:
				return getSyncPhysicalAssetClass() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PHYSICAL_ASSET_INFORMATION:
				return getSyncPhysicalAssetInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PROCESS_SEGMENT:
				return getSyncProcessSegment() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_PROCESS_SEGMENT_INFORMATION:
				return getSyncProcessSegmentInformation() != null;
			case B2MMLPackage.DOCUMENT_ROOT__SYNC_QUALIFICATION_TEST_SPECIFICATION:
				return getSyncQualificationTestSpecification() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //DocumentRootImpl
