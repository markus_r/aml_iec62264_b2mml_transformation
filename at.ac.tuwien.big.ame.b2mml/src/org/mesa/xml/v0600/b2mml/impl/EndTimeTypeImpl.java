/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.EndTimeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>End Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EndTimeTypeImpl extends DateTimeTypeImpl implements EndTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EndTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getEndTimeType();
	}

} //EndTimeTypeImpl
