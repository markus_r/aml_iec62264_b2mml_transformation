/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.PersonnelClassIDType;
import org.mesa.xml.v0600.b2mml.PropertyIDType;
import org.mesa.xml.v0600.b2mml.TestedPersonnelClassPropertyType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tested Personnel Class Property Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TestedPersonnelClassPropertyTypeImpl#getPersonnelClassID <em>Personnel Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TestedPersonnelClassPropertyTypeImpl#getPropertyID <em>Property ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestedPersonnelClassPropertyTypeImpl extends MinimalEObjectImpl.Container implements TestedPersonnelClassPropertyType {
	/**
	 * The cached value of the '{@link #getPersonnelClassID() <em>Personnel Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelClassID()
	 * @generated
	 * @ordered
	 */
	protected PersonnelClassIDType personnelClassID;

	/**
	 * The cached value of the '{@link #getPropertyID() <em>Property ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyID()
	 * @generated
	 * @ordered
	 */
	protected PropertyIDType propertyID;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestedPersonnelClassPropertyTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTestedPersonnelClassPropertyType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonnelClassIDType getPersonnelClassID() {
		return personnelClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPersonnelClassID(PersonnelClassIDType newPersonnelClassID, NotificationChain msgs) {
		PersonnelClassIDType oldPersonnelClassID = personnelClassID;
		personnelClassID = newPersonnelClassID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PERSONNEL_CLASS_ID, oldPersonnelClassID, newPersonnelClassID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersonnelClassID(PersonnelClassIDType newPersonnelClassID) {
		if (newPersonnelClassID != personnelClassID) {
			NotificationChain msgs = null;
			if (personnelClassID != null)
				msgs = ((InternalEObject)personnelClassID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PERSONNEL_CLASS_ID, null, msgs);
			if (newPersonnelClassID != null)
				msgs = ((InternalEObject)newPersonnelClassID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PERSONNEL_CLASS_ID, null, msgs);
			msgs = basicSetPersonnelClassID(newPersonnelClassID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PERSONNEL_CLASS_ID, newPersonnelClassID, newPersonnelClassID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyIDType getPropertyID() {
		return propertyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPropertyID(PropertyIDType newPropertyID, NotificationChain msgs) {
		PropertyIDType oldPropertyID = propertyID;
		propertyID = newPropertyID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PROPERTY_ID, oldPropertyID, newPropertyID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyID(PropertyIDType newPropertyID) {
		if (newPropertyID != propertyID) {
			NotificationChain msgs = null;
			if (propertyID != null)
				msgs = ((InternalEObject)propertyID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PROPERTY_ID, null, msgs);
			if (newPropertyID != null)
				msgs = ((InternalEObject)newPropertyID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PROPERTY_ID, null, msgs);
			msgs = basicSetPropertyID(newPropertyID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PROPERTY_ID, newPropertyID, newPropertyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PERSONNEL_CLASS_ID:
				return basicSetPersonnelClassID(null, msgs);
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				return basicSetPropertyID(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PERSONNEL_CLASS_ID:
				return getPersonnelClassID();
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				return getPropertyID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PERSONNEL_CLASS_ID:
				setPersonnelClassID((PersonnelClassIDType)newValue);
				return;
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				setPropertyID((PropertyIDType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PERSONNEL_CLASS_ID:
				setPersonnelClassID((PersonnelClassIDType)null);
				return;
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				setPropertyID((PropertyIDType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PERSONNEL_CLASS_ID:
				return personnelClassID != null;
			case B2MMLPackage.TESTED_PERSONNEL_CLASS_PROPERTY_TYPE__PROPERTY_ID:
				return propertyID != null;
		}
		return super.eIsSet(featureID);
	}

} //TestedPersonnelClassPropertyTypeImpl
