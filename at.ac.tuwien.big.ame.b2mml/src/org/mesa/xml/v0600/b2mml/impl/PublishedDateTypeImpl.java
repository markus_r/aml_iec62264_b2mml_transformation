/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.PublishedDateType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Published Date Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PublishedDateTypeImpl extends DateTimeTypeImpl implements PublishedDateType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PublishedDateTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPublishedDateType();
	}

} //PublishedDateTypeImpl
