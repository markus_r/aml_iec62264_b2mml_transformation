/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.ActualStartTimeType;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actual Start Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ActualStartTimeTypeImpl extends DateTimeTypeImpl implements ActualStartTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActualStartTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getActualStartTimeType();
	}

} //ActualStartTimeTypeImpl
