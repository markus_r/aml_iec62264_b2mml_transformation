/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.RequestedPriorityType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requested Priority Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequestedPriorityTypeImpl extends NumericTypeImpl implements RequestedPriorityType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequestedPriorityTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getRequestedPriorityType();
	}

} //RequestedPriorityTypeImpl
