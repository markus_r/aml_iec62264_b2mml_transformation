/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.CapabilityTypeType;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.EndTimeType;
import org.mesa.xml.v0600.b2mml.EquipmentElementLevelType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.OpEquipmentCapabilityType;
import org.mesa.xml.v0600.b2mml.OpMaterialCapabilityType;
import org.mesa.xml.v0600.b2mml.OpPersonnelCapabilityType;
import org.mesa.xml.v0600.b2mml.OpPhysicalAssetCapabilityType;
import org.mesa.xml.v0600.b2mml.OpProcessSegmentCapabilityType;
import org.mesa.xml.v0600.b2mml.ProcessSegmentIDType;
import org.mesa.xml.v0600.b2mml.ReasonType;
import org.mesa.xml.v0600.b2mml.StartTimeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Process Segment Capability Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getProcessSegmentID <em>Process Segment ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getCapabilityType <em>Capability Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getReason <em>Reason</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getEquipmentElementLevel <em>Equipment Element Level</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getPersonnelCapability <em>Personnel Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getEquipmentCapability <em>Equipment Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getPhysicalAssetCapability <em>Physical Asset Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getMaterialCapability <em>Material Capability</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpProcessSegmentCapabilityTypeImpl#getProcessSegmentCapability <em>Process Segment Capability</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpProcessSegmentCapabilityTypeImpl extends MinimalEObjectImpl.Container implements OpProcessSegmentCapabilityType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getProcessSegmentID() <em>Process Segment ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegmentID()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessSegmentIDType> processSegmentID;

	/**
	 * The cached value of the '{@link #getCapabilityType() <em>Capability Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilityType()
	 * @generated
	 * @ordered
	 */
	protected CapabilityTypeType capabilityType;

	/**
	 * The cached value of the '{@link #getReason() <em>Reason</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReason()
	 * @generated
	 * @ordered
	 */
	protected EList<ReasonType> reason;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected EList<HierarchyScopeType> hierarchyScope;

	/**
	 * The cached value of the '{@link #getEquipmentElementLevel() <em>Equipment Element Level</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentElementLevel()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentElementLevelType> equipmentElementLevel;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected StartTimeType startTime;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected EndTimeType endTime;

	/**
	 * The cached value of the '{@link #getPersonnelCapability() <em>Personnel Capability</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelCapability()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPersonnelCapabilityType> personnelCapability;

	/**
	 * The cached value of the '{@link #getEquipmentCapability() <em>Equipment Capability</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentCapability()
	 * @generated
	 * @ordered
	 */
	protected EList<OpEquipmentCapabilityType> equipmentCapability;

	/**
	 * The cached value of the '{@link #getPhysicalAssetCapability() <em>Physical Asset Capability</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetCapability()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPhysicalAssetCapabilityType> physicalAssetCapability;

	/**
	 * The cached value of the '{@link #getMaterialCapability() <em>Material Capability</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialCapability()
	 * @generated
	 * @ordered
	 */
	protected EList<OpMaterialCapabilityType> materialCapability;

	/**
	 * The cached value of the '{@link #getProcessSegmentCapability() <em>Process Segment Capability</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegmentCapability()
	 * @generated
	 * @ordered
	 */
	protected EList<OpProcessSegmentCapabilityType> processSegmentCapability;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpProcessSegmentCapabilityTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpProcessSegmentCapabilityType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessSegmentIDType> getProcessSegmentID() {
		if (processSegmentID == null) {
			processSegmentID = new EObjectContainmentEList<ProcessSegmentIDType>(ProcessSegmentIDType.class, this, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PROCESS_SEGMENT_ID);
		}
		return processSegmentID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CapabilityTypeType getCapabilityType() {
		return capabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCapabilityType(CapabilityTypeType newCapabilityType, NotificationChain msgs) {
		CapabilityTypeType oldCapabilityType = capabilityType;
		capabilityType = newCapabilityType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__CAPABILITY_TYPE, oldCapabilityType, newCapabilityType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCapabilityType(CapabilityTypeType newCapabilityType) {
		if (newCapabilityType != capabilityType) {
			NotificationChain msgs = null;
			if (capabilityType != null)
				msgs = ((InternalEObject)capabilityType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__CAPABILITY_TYPE, null, msgs);
			if (newCapabilityType != null)
				msgs = ((InternalEObject)newCapabilityType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__CAPABILITY_TYPE, null, msgs);
			msgs = basicSetCapabilityType(newCapabilityType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__CAPABILITY_TYPE, newCapabilityType, newCapabilityType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReasonType> getReason() {
		if (reason == null) {
			reason = new EObjectContainmentEList<ReasonType>(ReasonType.class, this, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__REASON);
		}
		return reason;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HierarchyScopeType> getHierarchyScope() {
		if (hierarchyScope == null) {
			hierarchyScope = new EObjectContainmentEList<HierarchyScopeType>(HierarchyScopeType.class, this, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__HIERARCHY_SCOPE);
		}
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentElementLevelType> getEquipmentElementLevel() {
		if (equipmentElementLevel == null) {
			equipmentElementLevel = new EObjectContainmentEList<EquipmentElementLevelType>(EquipmentElementLevelType.class, this, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__EQUIPMENT_ELEMENT_LEVEL);
		}
		return equipmentElementLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StartTimeType getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStartTime(StartTimeType newStartTime, NotificationChain msgs) {
		StartTimeType oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__START_TIME, oldStartTime, newStartTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(StartTimeType newStartTime) {
		if (newStartTime != startTime) {
			NotificationChain msgs = null;
			if (startTime != null)
				msgs = ((InternalEObject)startTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__START_TIME, null, msgs);
			if (newStartTime != null)
				msgs = ((InternalEObject)newStartTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__START_TIME, null, msgs);
			msgs = basicSetStartTime(newStartTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__START_TIME, newStartTime, newStartTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EndTimeType getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEndTime(EndTimeType newEndTime, NotificationChain msgs) {
		EndTimeType oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__END_TIME, oldEndTime, newEndTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(EndTimeType newEndTime) {
		if (newEndTime != endTime) {
			NotificationChain msgs = null;
			if (endTime != null)
				msgs = ((InternalEObject)endTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__END_TIME, null, msgs);
			if (newEndTime != null)
				msgs = ((InternalEObject)newEndTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__END_TIME, null, msgs);
			msgs = basicSetEndTime(newEndTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__END_TIME, newEndTime, newEndTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPersonnelCapabilityType> getPersonnelCapability() {
		if (personnelCapability == null) {
			personnelCapability = new EObjectContainmentEList<OpPersonnelCapabilityType>(OpPersonnelCapabilityType.class, this, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PERSONNEL_CAPABILITY);
		}
		return personnelCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpEquipmentCapabilityType> getEquipmentCapability() {
		if (equipmentCapability == null) {
			equipmentCapability = new EObjectContainmentEList<OpEquipmentCapabilityType>(OpEquipmentCapabilityType.class, this, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__EQUIPMENT_CAPABILITY);
		}
		return equipmentCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPhysicalAssetCapabilityType> getPhysicalAssetCapability() {
		if (physicalAssetCapability == null) {
			physicalAssetCapability = new EObjectContainmentEList<OpPhysicalAssetCapabilityType>(OpPhysicalAssetCapabilityType.class, this, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PHYSICAL_ASSET_CAPABILITY);
		}
		return physicalAssetCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpMaterialCapabilityType> getMaterialCapability() {
		if (materialCapability == null) {
			materialCapability = new EObjectContainmentEList<OpMaterialCapabilityType>(OpMaterialCapabilityType.class, this, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__MATERIAL_CAPABILITY);
		}
		return materialCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpProcessSegmentCapabilityType> getProcessSegmentCapability() {
		if (processSegmentCapability == null) {
			processSegmentCapability = new EObjectContainmentEList<OpProcessSegmentCapabilityType>(OpProcessSegmentCapabilityType.class, this, B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PROCESS_SEGMENT_CAPABILITY);
		}
		return processSegmentCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PROCESS_SEGMENT_ID:
				return ((InternalEList<?>)getProcessSegmentID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__CAPABILITY_TYPE:
				return basicSetCapabilityType(null, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__REASON:
				return ((InternalEList<?>)getReason()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				return ((InternalEList<?>)getHierarchyScope()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				return ((InternalEList<?>)getEquipmentElementLevel()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__START_TIME:
				return basicSetStartTime(null, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__END_TIME:
				return basicSetEndTime(null, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PERSONNEL_CAPABILITY:
				return ((InternalEList<?>)getPersonnelCapability()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__EQUIPMENT_CAPABILITY:
				return ((InternalEList<?>)getEquipmentCapability()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PHYSICAL_ASSET_CAPABILITY:
				return ((InternalEList<?>)getPhysicalAssetCapability()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__MATERIAL_CAPABILITY:
				return ((InternalEList<?>)getMaterialCapability()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PROCESS_SEGMENT_CAPABILITY:
				return ((InternalEList<?>)getProcessSegmentCapability()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__ID:
				return getID();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PROCESS_SEGMENT_ID:
				return getProcessSegmentID();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__CAPABILITY_TYPE:
				return getCapabilityType();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__REASON:
				return getReason();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				return getEquipmentElementLevel();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__START_TIME:
				return getStartTime();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__END_TIME:
				return getEndTime();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PERSONNEL_CAPABILITY:
				return getPersonnelCapability();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__EQUIPMENT_CAPABILITY:
				return getEquipmentCapability();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PHYSICAL_ASSET_CAPABILITY:
				return getPhysicalAssetCapability();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__MATERIAL_CAPABILITY:
				return getMaterialCapability();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PROCESS_SEGMENT_CAPABILITY:
				return getProcessSegmentCapability();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PROCESS_SEGMENT_ID:
				getProcessSegmentID().clear();
				getProcessSegmentID().addAll((Collection<? extends ProcessSegmentIDType>)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__CAPABILITY_TYPE:
				setCapabilityType((CapabilityTypeType)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__REASON:
				getReason().clear();
				getReason().addAll((Collection<? extends ReasonType>)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				getHierarchyScope().clear();
				getHierarchyScope().addAll((Collection<? extends HierarchyScopeType>)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				getEquipmentElementLevel().clear();
				getEquipmentElementLevel().addAll((Collection<? extends EquipmentElementLevelType>)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__START_TIME:
				setStartTime((StartTimeType)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__END_TIME:
				setEndTime((EndTimeType)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PERSONNEL_CAPABILITY:
				getPersonnelCapability().clear();
				getPersonnelCapability().addAll((Collection<? extends OpPersonnelCapabilityType>)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__EQUIPMENT_CAPABILITY:
				getEquipmentCapability().clear();
				getEquipmentCapability().addAll((Collection<? extends OpEquipmentCapabilityType>)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PHYSICAL_ASSET_CAPABILITY:
				getPhysicalAssetCapability().clear();
				getPhysicalAssetCapability().addAll((Collection<? extends OpPhysicalAssetCapabilityType>)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__MATERIAL_CAPABILITY:
				getMaterialCapability().clear();
				getMaterialCapability().addAll((Collection<? extends OpMaterialCapabilityType>)newValue);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PROCESS_SEGMENT_CAPABILITY:
				getProcessSegmentCapability().clear();
				getProcessSegmentCapability().addAll((Collection<? extends OpProcessSegmentCapabilityType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PROCESS_SEGMENT_ID:
				getProcessSegmentID().clear();
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__CAPABILITY_TYPE:
				setCapabilityType((CapabilityTypeType)null);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__REASON:
				getReason().clear();
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				getHierarchyScope().clear();
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				getEquipmentElementLevel().clear();
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__START_TIME:
				setStartTime((StartTimeType)null);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__END_TIME:
				setEndTime((EndTimeType)null);
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PERSONNEL_CAPABILITY:
				getPersonnelCapability().clear();
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__EQUIPMENT_CAPABILITY:
				getEquipmentCapability().clear();
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PHYSICAL_ASSET_CAPABILITY:
				getPhysicalAssetCapability().clear();
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__MATERIAL_CAPABILITY:
				getMaterialCapability().clear();
				return;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PROCESS_SEGMENT_CAPABILITY:
				getProcessSegmentCapability().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__ID:
				return iD != null;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PROCESS_SEGMENT_ID:
				return processSegmentID != null && !processSegmentID.isEmpty();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__CAPABILITY_TYPE:
				return capabilityType != null;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__REASON:
				return reason != null && !reason.isEmpty();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null && !hierarchyScope.isEmpty();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__EQUIPMENT_ELEMENT_LEVEL:
				return equipmentElementLevel != null && !equipmentElementLevel.isEmpty();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__START_TIME:
				return startTime != null;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__END_TIME:
				return endTime != null;
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PERSONNEL_CAPABILITY:
				return personnelCapability != null && !personnelCapability.isEmpty();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__EQUIPMENT_CAPABILITY:
				return equipmentCapability != null && !equipmentCapability.isEmpty();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PHYSICAL_ASSET_CAPABILITY:
				return physicalAssetCapability != null && !physicalAssetCapability.isEmpty();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__MATERIAL_CAPABILITY:
				return materialCapability != null && !materialCapability.isEmpty();
			case B2MMLPackage.OP_PROCESS_SEGMENT_CAPABILITY_TYPE__PROCESS_SEGMENT_CAPABILITY:
				return processSegmentCapability != null && !processSegmentCapability.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OpProcessSegmentCapabilityTypeImpl
