/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.CodeType;
import org.mesa.xml.v0600.b2mml.DateTimeType;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.TextType;
import org.mesa.xml.v0600.b2mml.TransStateChangeType;
import org.mesa.xml.v0600.b2mml.TransUserAreaType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trans State Change Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransStateChangeTypeImpl#getFromStateCode <em>From State Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransStateChangeTypeImpl#getToStateCode <em>To State Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransStateChangeTypeImpl#getChangeDateTime <em>Change Date Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransStateChangeTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransStateChangeTypeImpl#getNote <em>Note</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransStateChangeTypeImpl#getUserArea <em>User Area</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransStateChangeTypeImpl extends MinimalEObjectImpl.Container implements TransStateChangeType {
	/**
	 * The cached value of the '{@link #getFromStateCode() <em>From State Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromStateCode()
	 * @generated
	 * @ordered
	 */
	protected CodeType fromStateCode;

	/**
	 * The cached value of the '{@link #getToStateCode() <em>To State Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToStateCode()
	 * @generated
	 * @ordered
	 */
	protected CodeType toStateCode;

	/**
	 * The cached value of the '{@link #getChangeDateTime() <em>Change Date Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChangeDateTime()
	 * @generated
	 * @ordered
	 */
	protected DateTimeType changeDateTime;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getNote() <em>Note</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNote()
	 * @generated
	 * @ordered
	 */
	protected EList<TextType> note;

	/**
	 * The cached value of the '{@link #getUserArea() <em>User Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserArea()
	 * @generated
	 * @ordered
	 */
	protected TransUserAreaType userArea;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransStateChangeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTransStateChangeType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeType getFromStateCode() {
		return fromStateCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFromStateCode(CodeType newFromStateCode, NotificationChain msgs) {
		CodeType oldFromStateCode = fromStateCode;
		fromStateCode = newFromStateCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_STATE_CHANGE_TYPE__FROM_STATE_CODE, oldFromStateCode, newFromStateCode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromStateCode(CodeType newFromStateCode) {
		if (newFromStateCode != fromStateCode) {
			NotificationChain msgs = null;
			if (fromStateCode != null)
				msgs = ((InternalEObject)fromStateCode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_STATE_CHANGE_TYPE__FROM_STATE_CODE, null, msgs);
			if (newFromStateCode != null)
				msgs = ((InternalEObject)newFromStateCode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_STATE_CHANGE_TYPE__FROM_STATE_CODE, null, msgs);
			msgs = basicSetFromStateCode(newFromStateCode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_STATE_CHANGE_TYPE__FROM_STATE_CODE, newFromStateCode, newFromStateCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeType getToStateCode() {
		return toStateCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetToStateCode(CodeType newToStateCode, NotificationChain msgs) {
		CodeType oldToStateCode = toStateCode;
		toStateCode = newToStateCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_STATE_CHANGE_TYPE__TO_STATE_CODE, oldToStateCode, newToStateCode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToStateCode(CodeType newToStateCode) {
		if (newToStateCode != toStateCode) {
			NotificationChain msgs = null;
			if (toStateCode != null)
				msgs = ((InternalEObject)toStateCode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_STATE_CHANGE_TYPE__TO_STATE_CODE, null, msgs);
			if (newToStateCode != null)
				msgs = ((InternalEObject)newToStateCode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_STATE_CHANGE_TYPE__TO_STATE_CODE, null, msgs);
			msgs = basicSetToStateCode(newToStateCode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_STATE_CHANGE_TYPE__TO_STATE_CODE, newToStateCode, newToStateCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTimeType getChangeDateTime() {
		return changeDateTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeDateTime(DateTimeType newChangeDateTime, NotificationChain msgs) {
		DateTimeType oldChangeDateTime = changeDateTime;
		changeDateTime = newChangeDateTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_STATE_CHANGE_TYPE__CHANGE_DATE_TIME, oldChangeDateTime, newChangeDateTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeDateTime(DateTimeType newChangeDateTime) {
		if (newChangeDateTime != changeDateTime) {
			NotificationChain msgs = null;
			if (changeDateTime != null)
				msgs = ((InternalEObject)changeDateTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_STATE_CHANGE_TYPE__CHANGE_DATE_TIME, null, msgs);
			if (newChangeDateTime != null)
				msgs = ((InternalEObject)newChangeDateTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_STATE_CHANGE_TYPE__CHANGE_DATE_TIME, null, msgs);
			msgs = basicSetChangeDateTime(newChangeDateTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_STATE_CHANGE_TYPE__CHANGE_DATE_TIME, newChangeDateTime, newChangeDateTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.TRANS_STATE_CHANGE_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TextType> getNote() {
		if (note == null) {
			note = new EObjectContainmentEList<TextType>(TextType.class, this, B2MMLPackage.TRANS_STATE_CHANGE_TYPE__NOTE);
		}
		return note;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransUserAreaType getUserArea() {
		return userArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUserArea(TransUserAreaType newUserArea, NotificationChain msgs) {
		TransUserAreaType oldUserArea = userArea;
		userArea = newUserArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_STATE_CHANGE_TYPE__USER_AREA, oldUserArea, newUserArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUserArea(TransUserAreaType newUserArea) {
		if (newUserArea != userArea) {
			NotificationChain msgs = null;
			if (userArea != null)
				msgs = ((InternalEObject)userArea).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_STATE_CHANGE_TYPE__USER_AREA, null, msgs);
			if (newUserArea != null)
				msgs = ((InternalEObject)newUserArea).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_STATE_CHANGE_TYPE__USER_AREA, null, msgs);
			msgs = basicSetUserArea(newUserArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_STATE_CHANGE_TYPE__USER_AREA, newUserArea, newUserArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__FROM_STATE_CODE:
				return basicSetFromStateCode(null, msgs);
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__TO_STATE_CODE:
				return basicSetToStateCode(null, msgs);
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__CHANGE_DATE_TIME:
				return basicSetChangeDateTime(null, msgs);
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__NOTE:
				return ((InternalEList<?>)getNote()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__USER_AREA:
				return basicSetUserArea(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__FROM_STATE_CODE:
				return getFromStateCode();
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__TO_STATE_CODE:
				return getToStateCode();
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__CHANGE_DATE_TIME:
				return getChangeDateTime();
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__NOTE:
				return getNote();
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__USER_AREA:
				return getUserArea();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__FROM_STATE_CODE:
				setFromStateCode((CodeType)newValue);
				return;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__TO_STATE_CODE:
				setToStateCode((CodeType)newValue);
				return;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__CHANGE_DATE_TIME:
				setChangeDateTime((DateTimeType)newValue);
				return;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__NOTE:
				getNote().clear();
				getNote().addAll((Collection<? extends TextType>)newValue);
				return;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__USER_AREA:
				setUserArea((TransUserAreaType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__FROM_STATE_CODE:
				setFromStateCode((CodeType)null);
				return;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__TO_STATE_CODE:
				setToStateCode((CodeType)null);
				return;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__CHANGE_DATE_TIME:
				setChangeDateTime((DateTimeType)null);
				return;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__NOTE:
				getNote().clear();
				return;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__USER_AREA:
				setUserArea((TransUserAreaType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__FROM_STATE_CODE:
				return fromStateCode != null;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__TO_STATE_CODE:
				return toStateCode != null;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__CHANGE_DATE_TIME:
				return changeDateTime != null;
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__NOTE:
				return note != null && !note.isEmpty();
			case B2MMLPackage.TRANS_STATE_CHANGE_TYPE__USER_AREA:
				return userArea != null;
		}
		return super.eIsSet(featureID);
	}

} //TransStateChangeTypeImpl
