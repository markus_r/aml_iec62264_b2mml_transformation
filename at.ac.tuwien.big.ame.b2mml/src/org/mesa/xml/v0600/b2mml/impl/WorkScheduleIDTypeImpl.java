/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.WorkScheduleIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Work Schedule ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class WorkScheduleIDTypeImpl extends IdentifierTypeImpl implements WorkScheduleIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WorkScheduleIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getWorkScheduleIDType();
	}

} //WorkScheduleIDTypeImpl
