/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataTypeType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.QuantityStringType;
import org.mesa.xml.v0600.b2mml.QuantityValueType;
import org.mesa.xml.v0600.b2mml.UnitOfMeasureType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Quantity Value Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.QuantityValueTypeImpl#getQuantityString <em>Quantity String</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.QuantityValueTypeImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.QuantityValueTypeImpl#getUnitOfMeasure <em>Unit Of Measure</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.QuantityValueTypeImpl#getKey <em>Key</em>}</li>
 * </ul>
 *
 * @generated
 */
public class QuantityValueTypeImpl extends MinimalEObjectImpl.Container implements QuantityValueType {
	/**
	 * The cached value of the '{@link #getQuantityString() <em>Quantity String</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantityString()
	 * @generated
	 * @ordered
	 */
	protected QuantityStringType quantityString;

	/**
	 * This is true if the Quantity String containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean quantityStringESet;

	/**
	 * The cached value of the '{@link #getDataType() <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected DataTypeType dataType;

	/**
	 * This is true if the Data Type containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dataTypeESet;

	/**
	 * The cached value of the '{@link #getUnitOfMeasure() <em>Unit Of Measure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitOfMeasure()
	 * @generated
	 * @ordered
	 */
	protected UnitOfMeasureType unitOfMeasure;

	/**
	 * This is true if the Unit Of Measure containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean unitOfMeasureESet;

	/**
	 * The cached value of the '{@link #getKey() <em>Key</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKey()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType key;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QuantityValueTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getQuantityValueType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuantityStringType getQuantityString() {
		return quantityString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetQuantityString(QuantityStringType newQuantityString, NotificationChain msgs) {
		QuantityStringType oldQuantityString = quantityString;
		quantityString = newQuantityString;
		boolean oldQuantityStringESet = quantityStringESet;
		quantityStringESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_VALUE_TYPE__QUANTITY_STRING, oldQuantityString, newQuantityString, !oldQuantityStringESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantityString(QuantityStringType newQuantityString) {
		if (newQuantityString != quantityString) {
			NotificationChain msgs = null;
			if (quantityString != null)
				msgs = ((InternalEObject)quantityString).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUANTITY_VALUE_TYPE__QUANTITY_STRING, null, msgs);
			if (newQuantityString != null)
				msgs = ((InternalEObject)newQuantityString).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUANTITY_VALUE_TYPE__QUANTITY_STRING, null, msgs);
			msgs = basicSetQuantityString(newQuantityString, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldQuantityStringESet = quantityStringESet;
			quantityStringESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_VALUE_TYPE__QUANTITY_STRING, newQuantityString, newQuantityString, !oldQuantityStringESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetQuantityString(NotificationChain msgs) {
		QuantityStringType oldQuantityString = quantityString;
		quantityString = null;
		boolean oldQuantityStringESet = quantityStringESet;
		quantityStringESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.QUANTITY_VALUE_TYPE__QUANTITY_STRING, oldQuantityString, null, oldQuantityStringESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetQuantityString() {
		if (quantityString != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)quantityString).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUANTITY_VALUE_TYPE__QUANTITY_STRING, null, msgs);
			msgs = basicUnsetQuantityString(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldQuantityStringESet = quantityStringESet;
			quantityStringESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.QUANTITY_VALUE_TYPE__QUANTITY_STRING, null, null, oldQuantityStringESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetQuantityString() {
		return quantityStringESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataTypeType getDataType() {
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataType(DataTypeType newDataType, NotificationChain msgs) {
		DataTypeType oldDataType = dataType;
		dataType = newDataType;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_VALUE_TYPE__DATA_TYPE, oldDataType, newDataType, !oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataType(DataTypeType newDataType) {
		if (newDataType != dataType) {
			NotificationChain msgs = null;
			if (dataType != null)
				msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUANTITY_VALUE_TYPE__DATA_TYPE, null, msgs);
			if (newDataType != null)
				msgs = ((InternalEObject)newDataType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUANTITY_VALUE_TYPE__DATA_TYPE, null, msgs);
			msgs = basicSetDataType(newDataType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_VALUE_TYPE__DATA_TYPE, newDataType, newDataType, !oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetDataType(NotificationChain msgs) {
		DataTypeType oldDataType = dataType;
		dataType = null;
		boolean oldDataTypeESet = dataTypeESet;
		dataTypeESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.QUANTITY_VALUE_TYPE__DATA_TYPE, oldDataType, null, oldDataTypeESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDataType() {
		if (dataType != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)dataType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUANTITY_VALUE_TYPE__DATA_TYPE, null, msgs);
			msgs = basicUnsetDataType(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldDataTypeESet = dataTypeESet;
			dataTypeESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.QUANTITY_VALUE_TYPE__DATA_TYPE, null, null, oldDataTypeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDataType() {
		return dataTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitOfMeasureType getUnitOfMeasure() {
		return unitOfMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnitOfMeasure(UnitOfMeasureType newUnitOfMeasure, NotificationChain msgs) {
		UnitOfMeasureType oldUnitOfMeasure = unitOfMeasure;
		unitOfMeasure = newUnitOfMeasure;
		boolean oldUnitOfMeasureESet = unitOfMeasureESet;
		unitOfMeasureESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_VALUE_TYPE__UNIT_OF_MEASURE, oldUnitOfMeasure, newUnitOfMeasure, !oldUnitOfMeasureESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitOfMeasure(UnitOfMeasureType newUnitOfMeasure) {
		if (newUnitOfMeasure != unitOfMeasure) {
			NotificationChain msgs = null;
			if (unitOfMeasure != null)
				msgs = ((InternalEObject)unitOfMeasure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUANTITY_VALUE_TYPE__UNIT_OF_MEASURE, null, msgs);
			if (newUnitOfMeasure != null)
				msgs = ((InternalEObject)newUnitOfMeasure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUANTITY_VALUE_TYPE__UNIT_OF_MEASURE, null, msgs);
			msgs = basicSetUnitOfMeasure(newUnitOfMeasure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldUnitOfMeasureESet = unitOfMeasureESet;
			unitOfMeasureESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_VALUE_TYPE__UNIT_OF_MEASURE, newUnitOfMeasure, newUnitOfMeasure, !oldUnitOfMeasureESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetUnitOfMeasure(NotificationChain msgs) {
		UnitOfMeasureType oldUnitOfMeasure = unitOfMeasure;
		unitOfMeasure = null;
		boolean oldUnitOfMeasureESet = unitOfMeasureESet;
		unitOfMeasureESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.QUANTITY_VALUE_TYPE__UNIT_OF_MEASURE, oldUnitOfMeasure, null, oldUnitOfMeasureESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUnitOfMeasure() {
		if (unitOfMeasure != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)unitOfMeasure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUANTITY_VALUE_TYPE__UNIT_OF_MEASURE, null, msgs);
			msgs = basicUnsetUnitOfMeasure(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldUnitOfMeasureESet = unitOfMeasureESet;
			unitOfMeasureESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, B2MMLPackage.QUANTITY_VALUE_TYPE__UNIT_OF_MEASURE, null, null, oldUnitOfMeasureESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUnitOfMeasure() {
		return unitOfMeasureESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getKey() {
		return key;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetKey(IdentifierType newKey, NotificationChain msgs) {
		IdentifierType oldKey = key;
		key = newKey;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_VALUE_TYPE__KEY, oldKey, newKey);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKey(IdentifierType newKey) {
		if (newKey != key) {
			NotificationChain msgs = null;
			if (key != null)
				msgs = ((InternalEObject)key).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUANTITY_VALUE_TYPE__KEY, null, msgs);
			if (newKey != null)
				msgs = ((InternalEObject)newKey).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.QUANTITY_VALUE_TYPE__KEY, null, msgs);
			msgs = basicSetKey(newKey, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_VALUE_TYPE__KEY, newKey, newKey));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.QUANTITY_VALUE_TYPE__QUANTITY_STRING:
				return basicUnsetQuantityString(msgs);
			case B2MMLPackage.QUANTITY_VALUE_TYPE__DATA_TYPE:
				return basicUnsetDataType(msgs);
			case B2MMLPackage.QUANTITY_VALUE_TYPE__UNIT_OF_MEASURE:
				return basicUnsetUnitOfMeasure(msgs);
			case B2MMLPackage.QUANTITY_VALUE_TYPE__KEY:
				return basicSetKey(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.QUANTITY_VALUE_TYPE__QUANTITY_STRING:
				return getQuantityString();
			case B2MMLPackage.QUANTITY_VALUE_TYPE__DATA_TYPE:
				return getDataType();
			case B2MMLPackage.QUANTITY_VALUE_TYPE__UNIT_OF_MEASURE:
				return getUnitOfMeasure();
			case B2MMLPackage.QUANTITY_VALUE_TYPE__KEY:
				return getKey();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.QUANTITY_VALUE_TYPE__QUANTITY_STRING:
				setQuantityString((QuantityStringType)newValue);
				return;
			case B2MMLPackage.QUANTITY_VALUE_TYPE__DATA_TYPE:
				setDataType((DataTypeType)newValue);
				return;
			case B2MMLPackage.QUANTITY_VALUE_TYPE__UNIT_OF_MEASURE:
				setUnitOfMeasure((UnitOfMeasureType)newValue);
				return;
			case B2MMLPackage.QUANTITY_VALUE_TYPE__KEY:
				setKey((IdentifierType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.QUANTITY_VALUE_TYPE__QUANTITY_STRING:
				unsetQuantityString();
				return;
			case B2MMLPackage.QUANTITY_VALUE_TYPE__DATA_TYPE:
				unsetDataType();
				return;
			case B2MMLPackage.QUANTITY_VALUE_TYPE__UNIT_OF_MEASURE:
				unsetUnitOfMeasure();
				return;
			case B2MMLPackage.QUANTITY_VALUE_TYPE__KEY:
				setKey((IdentifierType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.QUANTITY_VALUE_TYPE__QUANTITY_STRING:
				return isSetQuantityString();
			case B2MMLPackage.QUANTITY_VALUE_TYPE__DATA_TYPE:
				return isSetDataType();
			case B2MMLPackage.QUANTITY_VALUE_TYPE__UNIT_OF_MEASURE:
				return isSetUnitOfMeasure();
			case B2MMLPackage.QUANTITY_VALUE_TYPE__KEY:
				return key != null;
		}
		return super.eIsSet(featureID);
	}

} //QuantityValueTypeImpl
