/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.ActualFinishTimeType;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actual Finish Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ActualFinishTimeTypeImpl extends DateTimeTypeImpl implements ActualFinishTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActualFinishTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getActualFinishTimeType();
	}

} //ActualFinishTimeTypeImpl
