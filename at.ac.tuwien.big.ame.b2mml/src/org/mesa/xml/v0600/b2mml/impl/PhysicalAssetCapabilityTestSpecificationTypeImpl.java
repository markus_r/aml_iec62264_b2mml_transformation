/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.NameType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetCapabilityTestSpecificationType;
import org.mesa.xml.v0600.b2mml.TestedPhysicalAssetClassPropertyType;
import org.mesa.xml.v0600.b2mml.TestedPhysicalAssetPropertyType;
import org.mesa.xml.v0600.b2mml.VersionType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Capability Test Specification Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetCapabilityTestSpecificationTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetCapabilityTestSpecificationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetCapabilityTestSpecificationTypeImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetCapabilityTestSpecificationTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetCapabilityTestSpecificationTypeImpl#getTestedPhysicalAssetProperty <em>Tested Physical Asset Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetCapabilityTestSpecificationTypeImpl#getTestedPhysicalAssetClassProperty <em>Tested Physical Asset Class Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalAssetCapabilityTestSpecificationTypeImpl extends MinimalEObjectImpl.Container implements PhysicalAssetCapabilityTestSpecificationType {
	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected NameType name;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected VersionType version;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getTestedPhysicalAssetProperty() <em>Tested Physical Asset Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestedPhysicalAssetProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<TestedPhysicalAssetPropertyType> testedPhysicalAssetProperty;

	/**
	 * The cached value of the '{@link #getTestedPhysicalAssetClassProperty() <em>Tested Physical Asset Class Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestedPhysicalAssetClassProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<TestedPhysicalAssetClassPropertyType> testedPhysicalAssetClassProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetCapabilityTestSpecificationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPhysicalAssetCapabilityTestSpecificationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameType getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetName(NameType newName, NotificationChain msgs) {
		NameType oldName = name;
		name = newName;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME, oldName, newName);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(NameType newName) {
		if (newName != name) {
			NotificationChain msgs = null;
			if (name != null)
				msgs = ((InternalEObject)name).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME, null, msgs);
			if (newName != null)
				msgs = ((InternalEObject)newName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME, null, msgs);
			msgs = basicSetName(newName, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME, newName, newName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VersionType getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVersion(VersionType newVersion, NotificationChain msgs) {
		VersionType oldVersion = version;
		version = newVersion;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION, oldVersion, newVersion);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(VersionType newVersion) {
		if (newVersion != version) {
			NotificationChain msgs = null;
			if (version != null)
				msgs = ((InternalEObject)version).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION, null, msgs);
			if (newVersion != null)
				msgs = ((InternalEObject)newVersion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION, null, msgs);
			msgs = basicSetVersion(newVersion, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION, newVersion, newVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestedPhysicalAssetPropertyType> getTestedPhysicalAssetProperty() {
		if (testedPhysicalAssetProperty == null) {
			testedPhysicalAssetProperty = new EObjectContainmentEList<TestedPhysicalAssetPropertyType>(TestedPhysicalAssetPropertyType.class, this, B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_PHYSICAL_ASSET_PROPERTY);
		}
		return testedPhysicalAssetProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestedPhysicalAssetClassPropertyType> getTestedPhysicalAssetClassProperty() {
		if (testedPhysicalAssetClassProperty == null) {
			testedPhysicalAssetClassProperty = new EObjectContainmentEList<TestedPhysicalAssetClassPropertyType>(TestedPhysicalAssetClassPropertyType.class, this, B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_PHYSICAL_ASSET_CLASS_PROPERTY);
		}
		return testedPhysicalAssetClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME:
				return basicSetName(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION:
				return basicSetVersion(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_PHYSICAL_ASSET_PROPERTY:
				return ((InternalEList<?>)getTestedPhysicalAssetProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_PHYSICAL_ASSET_CLASS_PROPERTY:
				return ((InternalEList<?>)getTestedPhysicalAssetClassProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME:
				return getName();
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION:
				return getVersion();
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_PHYSICAL_ASSET_PROPERTY:
				return getTestedPhysicalAssetProperty();
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_PHYSICAL_ASSET_CLASS_PROPERTY:
				return getTestedPhysicalAssetClassProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME:
				setName((NameType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION:
				setVersion((VersionType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_PHYSICAL_ASSET_PROPERTY:
				getTestedPhysicalAssetProperty().clear();
				getTestedPhysicalAssetProperty().addAll((Collection<? extends TestedPhysicalAssetPropertyType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_PHYSICAL_ASSET_CLASS_PROPERTY:
				getTestedPhysicalAssetClassProperty().clear();
				getTestedPhysicalAssetClassProperty().addAll((Collection<? extends TestedPhysicalAssetClassPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME:
				setName((NameType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION:
				setVersion((VersionType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_PHYSICAL_ASSET_PROPERTY:
				getTestedPhysicalAssetProperty().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_PHYSICAL_ASSET_CLASS_PROPERTY:
				getTestedPhysicalAssetClassProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__NAME:
				return name != null;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__VERSION:
				return version != null;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_PHYSICAL_ASSET_PROPERTY:
				return testedPhysicalAssetProperty != null && !testedPhysicalAssetProperty.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_TYPE__TESTED_PHYSICAL_ASSET_CLASS_PROPERTY:
				return testedPhysicalAssetClassProperty != null && !testedPhysicalAssetClassProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PhysicalAssetCapabilityTestSpecificationTypeImpl
