/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType49;
import org.mesa.xml.v0600.b2mml.EquipmentCapabilityTestSpecificationType;
import org.mesa.xml.v0600.b2mml.TransSyncType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type49</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType49Impl#getSync <em>Sync</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType49Impl#getEquipmentCapabilityTestSpec <em>Equipment Capability Test Spec</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType49Impl extends MinimalEObjectImpl.Container implements DataAreaType49 {
	/**
	 * The cached value of the '{@link #getSync() <em>Sync</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSync()
	 * @generated
	 * @ordered
	 */
	protected TransSyncType sync;

	/**
	 * The cached value of the '{@link #getEquipmentCapabilityTestSpec() <em>Equipment Capability Test Spec</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentCapabilityTestSpec()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentCapabilityTestSpecificationType> equipmentCapabilityTestSpec;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType49Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType49();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransSyncType getSync() {
		return sync;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSync(TransSyncType newSync, NotificationChain msgs) {
		TransSyncType oldSync = sync;
		sync = newSync;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE49__SYNC, oldSync, newSync);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSync(TransSyncType newSync) {
		if (newSync != sync) {
			NotificationChain msgs = null;
			if (sync != null)
				msgs = ((InternalEObject)sync).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE49__SYNC, null, msgs);
			if (newSync != null)
				msgs = ((InternalEObject)newSync).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE49__SYNC, null, msgs);
			msgs = basicSetSync(newSync, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE49__SYNC, newSync, newSync));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentCapabilityTestSpecificationType> getEquipmentCapabilityTestSpec() {
		if (equipmentCapabilityTestSpec == null) {
			equipmentCapabilityTestSpec = new EObjectContainmentEList<EquipmentCapabilityTestSpecificationType>(EquipmentCapabilityTestSpecificationType.class, this, B2MMLPackage.DATA_AREA_TYPE49__EQUIPMENT_CAPABILITY_TEST_SPEC);
		}
		return equipmentCapabilityTestSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE49__SYNC:
				return basicSetSync(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE49__EQUIPMENT_CAPABILITY_TEST_SPEC:
				return ((InternalEList<?>)getEquipmentCapabilityTestSpec()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE49__SYNC:
				return getSync();
			case B2MMLPackage.DATA_AREA_TYPE49__EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getEquipmentCapabilityTestSpec();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE49__SYNC:
				setSync((TransSyncType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE49__EQUIPMENT_CAPABILITY_TEST_SPEC:
				getEquipmentCapabilityTestSpec().clear();
				getEquipmentCapabilityTestSpec().addAll((Collection<? extends EquipmentCapabilityTestSpecificationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE49__SYNC:
				setSync((TransSyncType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE49__EQUIPMENT_CAPABILITY_TEST_SPEC:
				getEquipmentCapabilityTestSpec().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE49__SYNC:
				return sync != null;
			case B2MMLPackage.DATA_AREA_TYPE49__EQUIPMENT_CAPABILITY_TEST_SPEC:
				return equipmentCapabilityTestSpec != null && !equipmentCapabilityTestSpec.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType49Impl
