/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType65;
import org.mesa.xml.v0600.b2mml.PersonnelInformationType;
import org.mesa.xml.v0600.b2mml.TransSyncType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type65</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType65Impl#getSync <em>Sync</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType65Impl#getPersonnelInformation <em>Personnel Information</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType65Impl extends MinimalEObjectImpl.Container implements DataAreaType65 {
	/**
	 * The cached value of the '{@link #getSync() <em>Sync</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSync()
	 * @generated
	 * @ordered
	 */
	protected TransSyncType sync;

	/**
	 * The cached value of the '{@link #getPersonnelInformation() <em>Personnel Information</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelInformation()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelInformationType> personnelInformation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType65Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType65();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransSyncType getSync() {
		return sync;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSync(TransSyncType newSync, NotificationChain msgs) {
		TransSyncType oldSync = sync;
		sync = newSync;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE65__SYNC, oldSync, newSync);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSync(TransSyncType newSync) {
		if (newSync != sync) {
			NotificationChain msgs = null;
			if (sync != null)
				msgs = ((InternalEObject)sync).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE65__SYNC, null, msgs);
			if (newSync != null)
				msgs = ((InternalEObject)newSync).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE65__SYNC, null, msgs);
			msgs = basicSetSync(newSync, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE65__SYNC, newSync, newSync));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelInformationType> getPersonnelInformation() {
		if (personnelInformation == null) {
			personnelInformation = new EObjectContainmentEList<PersonnelInformationType>(PersonnelInformationType.class, this, B2MMLPackage.DATA_AREA_TYPE65__PERSONNEL_INFORMATION);
		}
		return personnelInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE65__SYNC:
				return basicSetSync(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE65__PERSONNEL_INFORMATION:
				return ((InternalEList<?>)getPersonnelInformation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE65__SYNC:
				return getSync();
			case B2MMLPackage.DATA_AREA_TYPE65__PERSONNEL_INFORMATION:
				return getPersonnelInformation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE65__SYNC:
				setSync((TransSyncType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE65__PERSONNEL_INFORMATION:
				getPersonnelInformation().clear();
				getPersonnelInformation().addAll((Collection<? extends PersonnelInformationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE65__SYNC:
				setSync((TransSyncType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE65__PERSONNEL_INFORMATION:
				getPersonnelInformation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE65__SYNC:
				return sync != null;
			case B2MMLPackage.DATA_AREA_TYPE65__PERSONNEL_INFORMATION:
				return personnelInformation != null && !personnelInformation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType65Impl
