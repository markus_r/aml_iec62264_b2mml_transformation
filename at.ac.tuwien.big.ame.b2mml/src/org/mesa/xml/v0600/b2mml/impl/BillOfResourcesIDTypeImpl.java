/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.BillOfResourcesIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bill Of Resources ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BillOfResourcesIDTypeImpl extends IdentifierTypeImpl implements BillOfResourcesIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BillOfResourcesIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getBillOfResourcesIDType();
	}

} //BillOfResourcesIDTypeImpl
