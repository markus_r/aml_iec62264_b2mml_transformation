/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType86;
import org.mesa.xml.v0600.b2mml.EquipmentCapabilityTestSpecificationType;
import org.mesa.xml.v0600.b2mml.TransShowType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type86</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType86Impl#getShow <em>Show</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType86Impl#getEquipmentCapabilityTestSpec <em>Equipment Capability Test Spec</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType86Impl extends MinimalEObjectImpl.Container implements DataAreaType86 {
	/**
	 * The cached value of the '{@link #getShow() <em>Show</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShow()
	 * @generated
	 * @ordered
	 */
	protected TransShowType show;

	/**
	 * The cached value of the '{@link #getEquipmentCapabilityTestSpec() <em>Equipment Capability Test Spec</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentCapabilityTestSpec()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentCapabilityTestSpecificationType> equipmentCapabilityTestSpec;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType86Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType86();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransShowType getShow() {
		return show;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShow(TransShowType newShow, NotificationChain msgs) {
		TransShowType oldShow = show;
		show = newShow;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE86__SHOW, oldShow, newShow);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShow(TransShowType newShow) {
		if (newShow != show) {
			NotificationChain msgs = null;
			if (show != null)
				msgs = ((InternalEObject)show).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE86__SHOW, null, msgs);
			if (newShow != null)
				msgs = ((InternalEObject)newShow).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE86__SHOW, null, msgs);
			msgs = basicSetShow(newShow, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE86__SHOW, newShow, newShow));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentCapabilityTestSpecificationType> getEquipmentCapabilityTestSpec() {
		if (equipmentCapabilityTestSpec == null) {
			equipmentCapabilityTestSpec = new EObjectContainmentEList<EquipmentCapabilityTestSpecificationType>(EquipmentCapabilityTestSpecificationType.class, this, B2MMLPackage.DATA_AREA_TYPE86__EQUIPMENT_CAPABILITY_TEST_SPEC);
		}
		return equipmentCapabilityTestSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE86__SHOW:
				return basicSetShow(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE86__EQUIPMENT_CAPABILITY_TEST_SPEC:
				return ((InternalEList<?>)getEquipmentCapabilityTestSpec()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE86__SHOW:
				return getShow();
			case B2MMLPackage.DATA_AREA_TYPE86__EQUIPMENT_CAPABILITY_TEST_SPEC:
				return getEquipmentCapabilityTestSpec();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE86__SHOW:
				setShow((TransShowType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE86__EQUIPMENT_CAPABILITY_TEST_SPEC:
				getEquipmentCapabilityTestSpec().clear();
				getEquipmentCapabilityTestSpec().addAll((Collection<? extends EquipmentCapabilityTestSpecificationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE86__SHOW:
				setShow((TransShowType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE86__EQUIPMENT_CAPABILITY_TEST_SPEC:
				getEquipmentCapabilityTestSpec().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE86__SHOW:
				return show != null;
			case B2MMLPackage.DATA_AREA_TYPE86__EQUIPMENT_CAPABILITY_TEST_SPEC:
				return equipmentCapabilityTestSpec != null && !equipmentCapabilityTestSpec.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType86Impl
