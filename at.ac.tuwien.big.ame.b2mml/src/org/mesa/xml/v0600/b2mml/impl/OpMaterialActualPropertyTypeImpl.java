/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.OpMaterialActualPropertyType;
import org.mesa.xml.v0600.b2mml.QuantityValueType;
import org.mesa.xml.v0600.b2mml.RequiredByRequestedSegmentResponseType;
import org.mesa.xml.v0600.b2mml.ValueType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Material Actual Property Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualPropertyTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualPropertyTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualPropertyTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualPropertyTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualPropertyTypeImpl#getMaterialActualProperty <em>Material Actual Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualPropertyTypeImpl#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpMaterialActualPropertyTypeImpl extends MinimalEObjectImpl.Container implements OpMaterialActualPropertyType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected EList<ValueType> value;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getMaterialActualProperty() <em>Material Actual Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialActualProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<OpMaterialActualPropertyType> materialActualProperty;

	/**
	 * The cached value of the '{@link #getRequiredByRequestedSegmentResponse() <em>Required By Requested Segment Response</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredByRequestedSegmentResponse()
	 * @generated
	 * @ordered
	 */
	protected RequiredByRequestedSegmentResponseType requiredByRequestedSegmentResponse;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpMaterialActualPropertyTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpMaterialActualPropertyType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueType> getValue() {
		if (value == null) {
			value = new EObjectContainmentEList<ValueType>(ValueType.class, this, B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__VALUE);
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpMaterialActualPropertyType> getMaterialActualProperty() {
		if (materialActualProperty == null) {
			materialActualProperty = new EObjectContainmentEList<OpMaterialActualPropertyType>(OpMaterialActualPropertyType.class, this, B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__MATERIAL_ACTUAL_PROPERTY);
		}
		return materialActualProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredByRequestedSegmentResponseType getRequiredByRequestedSegmentResponse() {
		return requiredByRequestedSegmentResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType newRequiredByRequestedSegmentResponse, NotificationChain msgs) {
		RequiredByRequestedSegmentResponseType oldRequiredByRequestedSegmentResponse = requiredByRequestedSegmentResponse;
		requiredByRequestedSegmentResponse = newRequiredByRequestedSegmentResponse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, oldRequiredByRequestedSegmentResponse, newRequiredByRequestedSegmentResponse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType newRequiredByRequestedSegmentResponse) {
		if (newRequiredByRequestedSegmentResponse != requiredByRequestedSegmentResponse) {
			NotificationChain msgs = null;
			if (requiredByRequestedSegmentResponse != null)
				msgs = ((InternalEObject)requiredByRequestedSegmentResponse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, null, msgs);
			if (newRequiredByRequestedSegmentResponse != null)
				msgs = ((InternalEObject)newRequiredByRequestedSegmentResponse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, null, msgs);
			msgs = basicSetRequiredByRequestedSegmentResponse(newRequiredByRequestedSegmentResponse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, newRequiredByRequestedSegmentResponse, newRequiredByRequestedSegmentResponse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__VALUE:
				return ((InternalEList<?>)getValue()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__MATERIAL_ACTUAL_PROPERTY:
				return ((InternalEList<?>)getMaterialActualProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return basicSetRequiredByRequestedSegmentResponse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__ID:
				return getID();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__VALUE:
				return getValue();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__MATERIAL_ACTUAL_PROPERTY:
				return getMaterialActualProperty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return getRequiredByRequestedSegmentResponse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__VALUE:
				getValue().clear();
				getValue().addAll((Collection<? extends ValueType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__MATERIAL_ACTUAL_PROPERTY:
				getMaterialActualProperty().clear();
				getMaterialActualProperty().addAll((Collection<? extends OpMaterialActualPropertyType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				setRequiredByRequestedSegmentResponse((RequiredByRequestedSegmentResponseType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__VALUE:
				getValue().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__MATERIAL_ACTUAL_PROPERTY:
				getMaterialActualProperty().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				setRequiredByRequestedSegmentResponse((RequiredByRequestedSegmentResponseType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__ID:
				return iD != null;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__VALUE:
				return value != null && !value.isEmpty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__MATERIAL_ACTUAL_PROPERTY:
				return materialActualProperty != null && !materialActualProperty.isEmpty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_PROPERTY_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return requiredByRequestedSegmentResponse != null;
		}
		return super.eIsSet(featureID);
	}

} //OpMaterialActualPropertyTypeImpl
