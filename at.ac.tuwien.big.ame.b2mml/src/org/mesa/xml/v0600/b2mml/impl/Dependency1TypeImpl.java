/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.Dependency1Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dependency1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Dependency1TypeImpl extends CodeTypeImpl implements Dependency1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Dependency1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDependency1Type();
	}

} //Dependency1TypeImpl
