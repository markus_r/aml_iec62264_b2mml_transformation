/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.LatestEndTimeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Latest End Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LatestEndTimeTypeImpl extends DateTimeTypeImpl implements LatestEndTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LatestEndTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getLatestEndTimeType();
	}

} //LatestEndTimeTypeImpl
