/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType135;
import org.mesa.xml.v0600.b2mml.MaterialSubLotType;
import org.mesa.xml.v0600.b2mml.TransChangeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type135</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType135Impl#getChange <em>Change</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType135Impl#getMaterialSubLot <em>Material Sub Lot</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType135Impl extends MinimalEObjectImpl.Container implements DataAreaType135 {
	/**
	 * The cached value of the '{@link #getChange() <em>Change</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChange()
	 * @generated
	 * @ordered
	 */
	protected TransChangeType change;

	/**
	 * The cached value of the '{@link #getMaterialSubLot() <em>Material Sub Lot</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSubLot()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSubLotType> materialSubLot;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType135Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType135();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransChangeType getChange() {
		return change;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChange(TransChangeType newChange, NotificationChain msgs) {
		TransChangeType oldChange = change;
		change = newChange;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE135__CHANGE, oldChange, newChange);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChange(TransChangeType newChange) {
		if (newChange != change) {
			NotificationChain msgs = null;
			if (change != null)
				msgs = ((InternalEObject)change).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE135__CHANGE, null, msgs);
			if (newChange != null)
				msgs = ((InternalEObject)newChange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE135__CHANGE, null, msgs);
			msgs = basicSetChange(newChange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE135__CHANGE, newChange, newChange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSubLotType> getMaterialSubLot() {
		if (materialSubLot == null) {
			materialSubLot = new EObjectContainmentEList<MaterialSubLotType>(MaterialSubLotType.class, this, B2MMLPackage.DATA_AREA_TYPE135__MATERIAL_SUB_LOT);
		}
		return materialSubLot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE135__CHANGE:
				return basicSetChange(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE135__MATERIAL_SUB_LOT:
				return ((InternalEList<?>)getMaterialSubLot()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE135__CHANGE:
				return getChange();
			case B2MMLPackage.DATA_AREA_TYPE135__MATERIAL_SUB_LOT:
				return getMaterialSubLot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE135__CHANGE:
				setChange((TransChangeType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE135__MATERIAL_SUB_LOT:
				getMaterialSubLot().clear();
				getMaterialSubLot().addAll((Collection<? extends MaterialSubLotType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE135__CHANGE:
				setChange((TransChangeType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE135__MATERIAL_SUB_LOT:
				getMaterialSubLot().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE135__CHANGE:
				return change != null;
			case B2MMLPackage.DATA_AREA_TYPE135__MATERIAL_SUB_LOT:
				return materialSubLot != null && !materialSubLot.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType135Impl
