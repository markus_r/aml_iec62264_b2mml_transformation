/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.StatusTimeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Status Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StatusTimeTypeImpl extends DateTimeTypeImpl implements StatusTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StatusTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getStatusTimeType();
	}

} //StatusTimeTypeImpl
