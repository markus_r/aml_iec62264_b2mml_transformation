/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.ProcessSegmentIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process Segment ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ProcessSegmentIDTypeImpl extends IdentifierTypeImpl implements ProcessSegmentIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessSegmentIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getProcessSegmentIDType();
	}

} //ProcessSegmentIDTypeImpl
