/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.MaterialUse1Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Use1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MaterialUse1TypeImpl extends CodeTypeImpl implements MaterialUse1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialUse1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getMaterialUse1Type();
	}

} //MaterialUse1TypeImpl
