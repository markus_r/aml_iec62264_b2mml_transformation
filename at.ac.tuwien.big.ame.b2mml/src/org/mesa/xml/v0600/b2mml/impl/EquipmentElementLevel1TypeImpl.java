/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.EquipmentElementLevel1Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Element Level1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EquipmentElementLevel1TypeImpl extends CodeTypeImpl implements EquipmentElementLevel1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentElementLevel1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getEquipmentElementLevel1Type();
	}

} //EquipmentElementLevel1TypeImpl
