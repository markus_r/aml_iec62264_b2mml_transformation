/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.PersonnelUseType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Personnel Use Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PersonnelUseTypeImpl extends CodeTypeImpl implements PersonnelUseType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonnelUseTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPersonnelUseType();
	}

} //PersonnelUseTypeImpl
