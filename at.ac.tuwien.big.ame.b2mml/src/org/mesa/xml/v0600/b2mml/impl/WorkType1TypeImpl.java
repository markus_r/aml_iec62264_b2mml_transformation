/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.WorkType1Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Work Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class WorkType1TypeImpl extends CodeTypeImpl implements WorkType1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WorkType1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getWorkType1Type();
	}

} //WorkType1TypeImpl
