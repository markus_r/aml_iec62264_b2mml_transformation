/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.OpPhysicalAssetSpecificationPropertyType;
import org.mesa.xml.v0600.b2mml.OpPhysicalAssetSpecificationType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetClassIDType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetIDType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetUseType;
import org.mesa.xml.v0600.b2mml.QuantityValueType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Physical Asset Specification Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPhysicalAssetSpecificationTypeImpl#getPhysicalAssetClassID <em>Physical Asset Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPhysicalAssetSpecificationTypeImpl#getPhysicalAssetID <em>Physical Asset ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPhysicalAssetSpecificationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPhysicalAssetSpecificationTypeImpl#getPhysicalAssetUse <em>Physical Asset Use</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPhysicalAssetSpecificationTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpPhysicalAssetSpecificationTypeImpl#getPhysicalAssetSpecificationProperty <em>Physical Asset Specification Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpPhysicalAssetSpecificationTypeImpl extends MinimalEObjectImpl.Container implements OpPhysicalAssetSpecificationType {
	/**
	 * The cached value of the '{@link #getPhysicalAssetClassID() <em>Physical Asset Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetClassIDType> physicalAssetClassID;

	/**
	 * The cached value of the '{@link #getPhysicalAssetID() <em>Physical Asset ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetID()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetIDType> physicalAssetID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getPhysicalAssetUse() <em>Physical Asset Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetUse()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetUseType physicalAssetUse;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getPhysicalAssetSpecificationProperty() <em>Physical Asset Specification Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetSpecificationProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPhysicalAssetSpecificationPropertyType> physicalAssetSpecificationProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpPhysicalAssetSpecificationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpPhysicalAssetSpecificationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetClassIDType> getPhysicalAssetClassID() {
		if (physicalAssetClassID == null) {
			physicalAssetClassID = new EObjectContainmentEList<PhysicalAssetClassIDType>(PhysicalAssetClassIDType.class, this, B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID);
		}
		return physicalAssetClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetIDType> getPhysicalAssetID() {
		if (physicalAssetID == null) {
			physicalAssetID = new EObjectContainmentEList<PhysicalAssetIDType>(PhysicalAssetIDType.class, this, B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID);
		}
		return physicalAssetID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetUseType getPhysicalAssetUse() {
		return physicalAssetUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPhysicalAssetUse(PhysicalAssetUseType newPhysicalAssetUse, NotificationChain msgs) {
		PhysicalAssetUseType oldPhysicalAssetUse = physicalAssetUse;
		physicalAssetUse = newPhysicalAssetUse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE, oldPhysicalAssetUse, newPhysicalAssetUse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetUse(PhysicalAssetUseType newPhysicalAssetUse) {
		if (newPhysicalAssetUse != physicalAssetUse) {
			NotificationChain msgs = null;
			if (physicalAssetUse != null)
				msgs = ((InternalEObject)physicalAssetUse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE, null, msgs);
			if (newPhysicalAssetUse != null)
				msgs = ((InternalEObject)newPhysicalAssetUse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE, null, msgs);
			msgs = basicSetPhysicalAssetUse(newPhysicalAssetUse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE, newPhysicalAssetUse, newPhysicalAssetUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPhysicalAssetSpecificationPropertyType> getPhysicalAssetSpecificationProperty() {
		if (physicalAssetSpecificationProperty == null) {
			physicalAssetSpecificationProperty = new EObjectContainmentEList<OpPhysicalAssetSpecificationPropertyType>(OpPhysicalAssetSpecificationPropertyType.class, this, B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_SPECIFICATION_PROPERTY);
		}
		return physicalAssetSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return ((InternalEList<?>)getPhysicalAssetClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID:
				return ((InternalEList<?>)getPhysicalAssetID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE:
				return basicSetPhysicalAssetUse(null, msgs);
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_SPECIFICATION_PROPERTY:
				return ((InternalEList<?>)getPhysicalAssetSpecificationProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return getPhysicalAssetClassID();
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID:
				return getPhysicalAssetID();
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE:
				return getPhysicalAssetUse();
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_SPECIFICATION_PROPERTY:
				return getPhysicalAssetSpecificationProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID:
				getPhysicalAssetClassID().clear();
				getPhysicalAssetClassID().addAll((Collection<? extends PhysicalAssetClassIDType>)newValue);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID:
				getPhysicalAssetID().clear();
				getPhysicalAssetID().addAll((Collection<? extends PhysicalAssetIDType>)newValue);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE:
				setPhysicalAssetUse((PhysicalAssetUseType)newValue);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_SPECIFICATION_PROPERTY:
				getPhysicalAssetSpecificationProperty().clear();
				getPhysicalAssetSpecificationProperty().addAll((Collection<? extends OpPhysicalAssetSpecificationPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID:
				getPhysicalAssetClassID().clear();
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID:
				getPhysicalAssetID().clear();
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE:
				setPhysicalAssetUse((PhysicalAssetUseType)null);
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_SPECIFICATION_PROPERTY:
				getPhysicalAssetSpecificationProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return physicalAssetClassID != null && !physicalAssetClassID.isEmpty();
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID:
				return physicalAssetID != null && !physicalAssetID.isEmpty();
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE:
				return physicalAssetUse != null;
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.OP_PHYSICAL_ASSET_SPECIFICATION_TYPE__PHYSICAL_ASSET_SPECIFICATION_PROPERTY:
				return physicalAssetSpecificationProperty != null && !physicalAssetSpecificationProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OpPhysicalAssetSpecificationTypeImpl
