/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.CancelPersonnelInformationType;
import org.mesa.xml.v0600.b2mml.DataAreaType159;
import org.mesa.xml.v0600.b2mml.TransApplicationAreaType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cancel Personnel Information Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.CancelPersonnelInformationTypeImpl#getApplicationArea <em>Application Area</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.CancelPersonnelInformationTypeImpl#getDataArea <em>Data Area</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CancelPersonnelInformationTypeImpl extends MinimalEObjectImpl.Container implements CancelPersonnelInformationType {
	/**
	 * The cached value of the '{@link #getApplicationArea() <em>Application Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplicationArea()
	 * @generated
	 * @ordered
	 */
	protected TransApplicationAreaType applicationArea;

	/**
	 * The cached value of the '{@link #getDataArea() <em>Data Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataArea()
	 * @generated
	 * @ordered
	 */
	protected DataAreaType159 dataArea;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CancelPersonnelInformationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getCancelPersonnelInformationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransApplicationAreaType getApplicationArea() {
		return applicationArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetApplicationArea(TransApplicationAreaType newApplicationArea, NotificationChain msgs) {
		TransApplicationAreaType oldApplicationArea = applicationArea;
		applicationArea = newApplicationArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__APPLICATION_AREA, oldApplicationArea, newApplicationArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicationArea(TransApplicationAreaType newApplicationArea) {
		if (newApplicationArea != applicationArea) {
			NotificationChain msgs = null;
			if (applicationArea != null)
				msgs = ((InternalEObject)applicationArea).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__APPLICATION_AREA, null, msgs);
			if (newApplicationArea != null)
				msgs = ((InternalEObject)newApplicationArea).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__APPLICATION_AREA, null, msgs);
			msgs = basicSetApplicationArea(newApplicationArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__APPLICATION_AREA, newApplicationArea, newApplicationArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataAreaType159 getDataArea() {
		return dataArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataArea(DataAreaType159 newDataArea, NotificationChain msgs) {
		DataAreaType159 oldDataArea = dataArea;
		dataArea = newDataArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__DATA_AREA, oldDataArea, newDataArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataArea(DataAreaType159 newDataArea) {
		if (newDataArea != dataArea) {
			NotificationChain msgs = null;
			if (dataArea != null)
				msgs = ((InternalEObject)dataArea).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__DATA_AREA, null, msgs);
			if (newDataArea != null)
				msgs = ((InternalEObject)newDataArea).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__DATA_AREA, null, msgs);
			msgs = basicSetDataArea(newDataArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__DATA_AREA, newDataArea, newDataArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__APPLICATION_AREA:
				return basicSetApplicationArea(null, msgs);
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__DATA_AREA:
				return basicSetDataArea(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__APPLICATION_AREA:
				return getApplicationArea();
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__DATA_AREA:
				return getDataArea();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__APPLICATION_AREA:
				setApplicationArea((TransApplicationAreaType)newValue);
				return;
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__DATA_AREA:
				setDataArea((DataAreaType159)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__APPLICATION_AREA:
				setApplicationArea((TransApplicationAreaType)null);
				return;
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__DATA_AREA:
				setDataArea((DataAreaType159)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__APPLICATION_AREA:
				return applicationArea != null;
			case B2MMLPackage.CANCEL_PERSONNEL_INFORMATION_TYPE__DATA_AREA:
				return dataArea != null;
		}
		return super.eIsSet(featureID);
	}

} //CancelPersonnelInformationTypeImpl
