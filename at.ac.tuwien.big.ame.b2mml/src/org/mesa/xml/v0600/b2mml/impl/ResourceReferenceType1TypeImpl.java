/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.ResourceReferenceType1Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource Reference Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ResourceReferenceType1TypeImpl extends CodeTypeImpl implements ResourceReferenceType1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceReferenceType1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getResourceReferenceType1Type();
	}

} //ResourceReferenceType1TypeImpl
