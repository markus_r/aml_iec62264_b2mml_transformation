/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.BillOfMaterialsIDType;
import org.mesa.xml.v0600.b2mml.BillOfResourcesIDType;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.OperationsDefinitionType;
import org.mesa.xml.v0600.b2mml.OperationsMaterialBillType;
import org.mesa.xml.v0600.b2mml.OperationsSegmentType;
import org.mesa.xml.v0600.b2mml.OperationsTypeType;
import org.mesa.xml.v0600.b2mml.PublishedDateType;
import org.mesa.xml.v0600.b2mml.VersionType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Definition Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsDefinitionTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsDefinitionTypeImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsDefinitionTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsDefinitionTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsDefinitionTypeImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsDefinitionTypeImpl#getPublishedDate <em>Published Date</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsDefinitionTypeImpl#getBillOfMaterialsID <em>Bill Of Materials ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsDefinitionTypeImpl#getWorkDefinitionID <em>Work Definition ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsDefinitionTypeImpl#getBillOfResourcesID <em>Bill Of Resources ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsDefinitionTypeImpl#getOperationsMaterialBill <em>Operations Material Bill</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OperationsDefinitionTypeImpl#getOperationsSegment <em>Operations Segment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationsDefinitionTypeImpl extends MinimalEObjectImpl.Container implements OperationsDefinitionType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected VersionType version;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsTypeType operationsType;

	/**
	 * The cached value of the '{@link #getPublishedDate() <em>Published Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublishedDate()
	 * @generated
	 * @ordered
	 */
	protected PublishedDateType publishedDate;

	/**
	 * The cached value of the '{@link #getBillOfMaterialsID() <em>Bill Of Materials ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBillOfMaterialsID()
	 * @generated
	 * @ordered
	 */
	protected BillOfMaterialsIDType billOfMaterialsID;

	/**
	 * The cached value of the '{@link #getWorkDefinitionID() <em>Work Definition ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkDefinitionID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType workDefinitionID;

	/**
	 * The cached value of the '{@link #getBillOfResourcesID() <em>Bill Of Resources ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBillOfResourcesID()
	 * @generated
	 * @ordered
	 */
	protected BillOfResourcesIDType billOfResourcesID;

	/**
	 * The cached value of the '{@link #getOperationsMaterialBill() <em>Operations Material Bill</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsMaterialBill()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsMaterialBillType> operationsMaterialBill;

	/**
	 * The cached value of the '{@link #getOperationsSegment() <em>Operations Segment</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsSegment()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsSegmentType> operationsSegment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsDefinitionTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOperationsDefinitionType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VersionType getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVersion(VersionType newVersion, NotificationChain msgs) {
		VersionType oldVersion = version;
		version = newVersion;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__VERSION, oldVersion, newVersion);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(VersionType newVersion) {
		if (newVersion != version) {
			NotificationChain msgs = null;
			if (version != null)
				msgs = ((InternalEObject)version).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__VERSION, null, msgs);
			if (newVersion != null)
				msgs = ((InternalEObject)newVersion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__VERSION, null, msgs);
			msgs = basicSetVersion(newVersion, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__VERSION, newVersion, newVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsTypeType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsType(OperationsTypeType newOperationsType, NotificationChain msgs) {
		OperationsTypeType oldOperationsType = operationsType;
		operationsType = newOperationsType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_TYPE, oldOperationsType, newOperationsType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsTypeType newOperationsType) {
		if (newOperationsType != operationsType) {
			NotificationChain msgs = null;
			if (operationsType != null)
				msgs = ((InternalEObject)operationsType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_TYPE, null, msgs);
			if (newOperationsType != null)
				msgs = ((InternalEObject)newOperationsType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_TYPE, null, msgs);
			msgs = basicSetOperationsType(newOperationsType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_TYPE, newOperationsType, newOperationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PublishedDateType getPublishedDate() {
		return publishedDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPublishedDate(PublishedDateType newPublishedDate, NotificationChain msgs) {
		PublishedDateType oldPublishedDate = publishedDate;
		publishedDate = newPublishedDate;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__PUBLISHED_DATE, oldPublishedDate, newPublishedDate);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublishedDate(PublishedDateType newPublishedDate) {
		if (newPublishedDate != publishedDate) {
			NotificationChain msgs = null;
			if (publishedDate != null)
				msgs = ((InternalEObject)publishedDate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__PUBLISHED_DATE, null, msgs);
			if (newPublishedDate != null)
				msgs = ((InternalEObject)newPublishedDate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__PUBLISHED_DATE, null, msgs);
			msgs = basicSetPublishedDate(newPublishedDate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__PUBLISHED_DATE, newPublishedDate, newPublishedDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BillOfMaterialsIDType getBillOfMaterialsID() {
		return billOfMaterialsID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBillOfMaterialsID(BillOfMaterialsIDType newBillOfMaterialsID, NotificationChain msgs) {
		BillOfMaterialsIDType oldBillOfMaterialsID = billOfMaterialsID;
		billOfMaterialsID = newBillOfMaterialsID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_MATERIALS_ID, oldBillOfMaterialsID, newBillOfMaterialsID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBillOfMaterialsID(BillOfMaterialsIDType newBillOfMaterialsID) {
		if (newBillOfMaterialsID != billOfMaterialsID) {
			NotificationChain msgs = null;
			if (billOfMaterialsID != null)
				msgs = ((InternalEObject)billOfMaterialsID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_MATERIALS_ID, null, msgs);
			if (newBillOfMaterialsID != null)
				msgs = ((InternalEObject)newBillOfMaterialsID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_MATERIALS_ID, null, msgs);
			msgs = basicSetBillOfMaterialsID(newBillOfMaterialsID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_MATERIALS_ID, newBillOfMaterialsID, newBillOfMaterialsID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getWorkDefinitionID() {
		return workDefinitionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWorkDefinitionID(IdentifierType newWorkDefinitionID, NotificationChain msgs) {
		IdentifierType oldWorkDefinitionID = workDefinitionID;
		workDefinitionID = newWorkDefinitionID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__WORK_DEFINITION_ID, oldWorkDefinitionID, newWorkDefinitionID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorkDefinitionID(IdentifierType newWorkDefinitionID) {
		if (newWorkDefinitionID != workDefinitionID) {
			NotificationChain msgs = null;
			if (workDefinitionID != null)
				msgs = ((InternalEObject)workDefinitionID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__WORK_DEFINITION_ID, null, msgs);
			if (newWorkDefinitionID != null)
				msgs = ((InternalEObject)newWorkDefinitionID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__WORK_DEFINITION_ID, null, msgs);
			msgs = basicSetWorkDefinitionID(newWorkDefinitionID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__WORK_DEFINITION_ID, newWorkDefinitionID, newWorkDefinitionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BillOfResourcesIDType getBillOfResourcesID() {
		return billOfResourcesID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBillOfResourcesID(BillOfResourcesIDType newBillOfResourcesID, NotificationChain msgs) {
		BillOfResourcesIDType oldBillOfResourcesID = billOfResourcesID;
		billOfResourcesID = newBillOfResourcesID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_RESOURCES_ID, oldBillOfResourcesID, newBillOfResourcesID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBillOfResourcesID(BillOfResourcesIDType newBillOfResourcesID) {
		if (newBillOfResourcesID != billOfResourcesID) {
			NotificationChain msgs = null;
			if (billOfResourcesID != null)
				msgs = ((InternalEObject)billOfResourcesID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_RESOURCES_ID, null, msgs);
			if (newBillOfResourcesID != null)
				msgs = ((InternalEObject)newBillOfResourcesID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_RESOURCES_ID, null, msgs);
			msgs = basicSetBillOfResourcesID(newBillOfResourcesID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_RESOURCES_ID, newBillOfResourcesID, newBillOfResourcesID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsMaterialBillType> getOperationsMaterialBill() {
		if (operationsMaterialBill == null) {
			operationsMaterialBill = new EObjectContainmentEList<OperationsMaterialBillType>(OperationsMaterialBillType.class, this, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_MATERIAL_BILL);
		}
		return operationsMaterialBill;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsSegmentType> getOperationsSegment() {
		if (operationsSegment == null) {
			operationsSegment = new EObjectContainmentEList<OperationsSegmentType>(OperationsSegmentType.class, this, B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_SEGMENT);
		}
		return operationsSegment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__VERSION:
				return basicSetVersion(null, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_TYPE:
				return basicSetOperationsType(null, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__PUBLISHED_DATE:
				return basicSetPublishedDate(null, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_MATERIALS_ID:
				return basicSetBillOfMaterialsID(null, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__WORK_DEFINITION_ID:
				return basicSetWorkDefinitionID(null, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_RESOURCES_ID:
				return basicSetBillOfResourcesID(null, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_MATERIAL_BILL:
				return ((InternalEList<?>)getOperationsMaterialBill()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_SEGMENT:
				return ((InternalEList<?>)getOperationsSegment()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__ID:
				return getID();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__VERSION:
				return getVersion();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_TYPE:
				return getOperationsType();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__PUBLISHED_DATE:
				return getPublishedDate();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_MATERIALS_ID:
				return getBillOfMaterialsID();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__WORK_DEFINITION_ID:
				return getWorkDefinitionID();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_RESOURCES_ID:
				return getBillOfResourcesID();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_MATERIAL_BILL:
				return getOperationsMaterialBill();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_SEGMENT:
				return getOperationsSegment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__VERSION:
				setVersion((VersionType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__PUBLISHED_DATE:
				setPublishedDate((PublishedDateType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_MATERIALS_ID:
				setBillOfMaterialsID((BillOfMaterialsIDType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__WORK_DEFINITION_ID:
				setWorkDefinitionID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_RESOURCES_ID:
				setBillOfResourcesID((BillOfResourcesIDType)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_MATERIAL_BILL:
				getOperationsMaterialBill().clear();
				getOperationsMaterialBill().addAll((Collection<? extends OperationsMaterialBillType>)newValue);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_SEGMENT:
				getOperationsSegment().clear();
				getOperationsSegment().addAll((Collection<? extends OperationsSegmentType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__VERSION:
				setVersion((VersionType)null);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)null);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__PUBLISHED_DATE:
				setPublishedDate((PublishedDateType)null);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_MATERIALS_ID:
				setBillOfMaterialsID((BillOfMaterialsIDType)null);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__WORK_DEFINITION_ID:
				setWorkDefinitionID((IdentifierType)null);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_RESOURCES_ID:
				setBillOfResourcesID((BillOfResourcesIDType)null);
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_MATERIAL_BILL:
				getOperationsMaterialBill().clear();
				return;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_SEGMENT:
				getOperationsSegment().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__ID:
				return iD != null;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__VERSION:
				return version != null;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_TYPE:
				return operationsType != null;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__PUBLISHED_DATE:
				return publishedDate != null;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_MATERIALS_ID:
				return billOfMaterialsID != null;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__WORK_DEFINITION_ID:
				return workDefinitionID != null;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__BILL_OF_RESOURCES_ID:
				return billOfResourcesID != null;
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_MATERIAL_BILL:
				return operationsMaterialBill != null && !operationsMaterialBill.isEmpty();
			case B2MMLPackage.OPERATIONS_DEFINITION_TYPE__OPERATIONS_SEGMENT:
				return operationsSegment != null && !operationsSegment.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OperationsDefinitionTypeImpl
