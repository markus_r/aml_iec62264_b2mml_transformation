/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.PlannedFinishTimeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Planned Finish Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PlannedFinishTimeTypeImpl extends DateTimeTypeImpl implements PlannedFinishTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PlannedFinishTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPlannedFinishTimeType();
	}

} //PlannedFinishTimeTypeImpl
