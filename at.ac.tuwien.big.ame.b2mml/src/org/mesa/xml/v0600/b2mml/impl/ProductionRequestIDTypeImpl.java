/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.ProductionRequestIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Production Request ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ProductionRequestIDTypeImpl extends IdentifierTypeImpl implements ProductionRequestIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProductionRequestIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getProductionRequestIDType();
	}

} //ProductionRequestIDTypeImpl
