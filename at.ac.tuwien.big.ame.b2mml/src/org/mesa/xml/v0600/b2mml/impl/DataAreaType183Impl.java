/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType183;
import org.mesa.xml.v0600.b2mml.OperationsDefinitionInformationType;
import org.mesa.xml.v0600.b2mml.TransCancelType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type183</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType183Impl#getCancel <em>Cancel</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType183Impl#getOperationsDefinitionInformation <em>Operations Definition Information</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType183Impl extends MinimalEObjectImpl.Container implements DataAreaType183 {
	/**
	 * The cached value of the '{@link #getCancel() <em>Cancel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCancel()
	 * @generated
	 * @ordered
	 */
	protected TransCancelType cancel;

	/**
	 * The cached value of the '{@link #getOperationsDefinitionInformation() <em>Operations Definition Information</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsDefinitionInformation()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsDefinitionInformationType> operationsDefinitionInformation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType183Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType183();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransCancelType getCancel() {
		return cancel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancel(TransCancelType newCancel, NotificationChain msgs) {
		TransCancelType oldCancel = cancel;
		cancel = newCancel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE183__CANCEL, oldCancel, newCancel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancel(TransCancelType newCancel) {
		if (newCancel != cancel) {
			NotificationChain msgs = null;
			if (cancel != null)
				msgs = ((InternalEObject)cancel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE183__CANCEL, null, msgs);
			if (newCancel != null)
				msgs = ((InternalEObject)newCancel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE183__CANCEL, null, msgs);
			msgs = basicSetCancel(newCancel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE183__CANCEL, newCancel, newCancel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsDefinitionInformationType> getOperationsDefinitionInformation() {
		if (operationsDefinitionInformation == null) {
			operationsDefinitionInformation = new EObjectContainmentEList<OperationsDefinitionInformationType>(OperationsDefinitionInformationType.class, this, B2MMLPackage.DATA_AREA_TYPE183__OPERATIONS_DEFINITION_INFORMATION);
		}
		return operationsDefinitionInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE183__CANCEL:
				return basicSetCancel(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE183__OPERATIONS_DEFINITION_INFORMATION:
				return ((InternalEList<?>)getOperationsDefinitionInformation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE183__CANCEL:
				return getCancel();
			case B2MMLPackage.DATA_AREA_TYPE183__OPERATIONS_DEFINITION_INFORMATION:
				return getOperationsDefinitionInformation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE183__CANCEL:
				setCancel((TransCancelType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE183__OPERATIONS_DEFINITION_INFORMATION:
				getOperationsDefinitionInformation().clear();
				getOperationsDefinitionInformation().addAll((Collection<? extends OperationsDefinitionInformationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE183__CANCEL:
				setCancel((TransCancelType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE183__OPERATIONS_DEFINITION_INFORMATION:
				getOperationsDefinitionInformation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE183__CANCEL:
				return cancel != null;
			case B2MMLPackage.DATA_AREA_TYPE183__OPERATIONS_DEFINITION_INFORMATION:
				return operationsDefinitionInformation != null && !operationsDefinitionInformation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType183Impl
