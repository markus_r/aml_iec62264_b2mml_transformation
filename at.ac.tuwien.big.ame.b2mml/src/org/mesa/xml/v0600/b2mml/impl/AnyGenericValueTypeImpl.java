/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.mesa.xml.v0600.b2mml.AnyGenericValueType;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Any Generic Value Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getCharacterSetCode <em>Character Set Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getCurrencyCodeListVersionID <em>Currency Code List Version ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getCurrencyID <em>Currency ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getEncodingCode <em>Encoding Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getFilename <em>Filename</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getFormat <em>Format</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getLanguageID <em>Language ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getLanguageLocaleID <em>Language Locale ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getListAgencyID <em>List Agency ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getListAgencyName <em>List Agency Name</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getListID <em>List ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getListName <em>List Name</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getListSchemaURI <em>List Schema URI</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getListURI <em>List URI</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getListVersionID <em>List Version ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getMimeCode <em>Mime Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getSchemaAgencyID <em>Schema Agency ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getSchemaAgencyName <em>Schema Agency Name</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getSchemaDataURI <em>Schema Data URI</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getSchemaID <em>Schema ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getSchemaName <em>Schema Name</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getSchemaURI <em>Schema URI</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getSchemaVersionID <em>Schema Version ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getUnitCode <em>Unit Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getUnitCodeListAgencyID <em>Unit Code List Agency ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getUnitCodeListAgencyName <em>Unit Code List Agency Name</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getUnitCodeListID <em>Unit Code List ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getUnitCodeListVersionID <em>Unit Code List Version ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AnyGenericValueTypeImpl#getUri <em>Uri</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnyGenericValueTypeImpl extends MinimalEObjectImpl.Container implements AnyGenericValueType {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCharacterSetCode() <em>Character Set Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacterSetCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CHARACTER_SET_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCharacterSetCode() <em>Character Set Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacterSetCode()
	 * @generated
	 * @ordered
	 */
	protected String characterSetCode = CHARACTER_SET_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCurrencyCodeListVersionID() <em>Currency Code List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrencyCodeListVersionID()
	 * @generated
	 * @ordered
	 */
	protected static final String CURRENCY_CODE_LIST_VERSION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCurrencyCodeListVersionID() <em>Currency Code List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrencyCodeListVersionID()
	 * @generated
	 * @ordered
	 */
	protected String currencyCodeListVersionID = CURRENCY_CODE_LIST_VERSION_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getCurrencyID() <em>Currency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrencyID()
	 * @generated
	 * @ordered
	 */
	protected static final String CURRENCY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCurrencyID() <em>Currency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrencyID()
	 * @generated
	 * @ordered
	 */
	protected String currencyID = CURRENCY_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getEncodingCode() <em>Encoding Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEncodingCode()
	 * @generated
	 * @ordered
	 */
	protected static final String ENCODING_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEncodingCode() <em>Encoding Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEncodingCode()
	 * @generated
	 * @ordered
	 */
	protected String encodingCode = ENCODING_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFilename() <em>Filename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilename()
	 * @generated
	 * @ordered
	 */
	protected static final String FILENAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFilename() <em>Filename</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilename()
	 * @generated
	 * @ordered
	 */
	protected String filename = FILENAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getFormat() <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormat()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMAT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFormat() <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormat()
	 * @generated
	 * @ordered
	 */
	protected String format = FORMAT_EDEFAULT;

	/**
	 * The default value of the '{@link #getLanguageID() <em>Language ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguageID()
	 * @generated
	 * @ordered
	 */
	protected static final String LANGUAGE_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLanguageID() <em>Language ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguageID()
	 * @generated
	 * @ordered
	 */
	protected String languageID = LANGUAGE_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getLanguageLocaleID() <em>Language Locale ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguageLocaleID()
	 * @generated
	 * @ordered
	 */
	protected static final String LANGUAGE_LOCALE_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLanguageLocaleID() <em>Language Locale ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguageLocaleID()
	 * @generated
	 * @ordered
	 */
	protected String languageLocaleID = LANGUAGE_LOCALE_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getListAgencyID() <em>List Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListAgencyID()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_AGENCY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListAgencyID() <em>List Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListAgencyID()
	 * @generated
	 * @ordered
	 */
	protected String listAgencyID = LIST_AGENCY_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getListAgencyName() <em>List Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListAgencyName()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_AGENCY_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListAgencyName() <em>List Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListAgencyName()
	 * @generated
	 * @ordered
	 */
	protected String listAgencyName = LIST_AGENCY_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getListID() <em>List ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListID()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListID() <em>List ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListID()
	 * @generated
	 * @ordered
	 */
	protected String listID = LIST_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getListName() <em>List Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListName()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListName() <em>List Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListName()
	 * @generated
	 * @ordered
	 */
	protected String listName = LIST_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getListSchemaURI() <em>List Schema URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListSchemaURI()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_SCHEMA_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListSchemaURI() <em>List Schema URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListSchemaURI()
	 * @generated
	 * @ordered
	 */
	protected String listSchemaURI = LIST_SCHEMA_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getListURI() <em>List URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListURI()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListURI() <em>List URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListURI()
	 * @generated
	 * @ordered
	 */
	protected String listURI = LIST_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getListVersionID() <em>List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListVersionID()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_VERSION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListVersionID() <em>List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListVersionID()
	 * @generated
	 * @ordered
	 */
	protected String listVersionID = LIST_VERSION_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getMimeCode() <em>Mime Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMimeCode()
	 * @generated
	 * @ordered
	 */
	protected static final String MIME_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMimeCode() <em>Mime Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMimeCode()
	 * @generated
	 * @ordered
	 */
	protected String mimeCode = MIME_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemaAgencyID() <em>Schema Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaAgencyID()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEMA_AGENCY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemaAgencyID() <em>Schema Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaAgencyID()
	 * @generated
	 * @ordered
	 */
	protected String schemaAgencyID = SCHEMA_AGENCY_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemaAgencyName() <em>Schema Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaAgencyName()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEMA_AGENCY_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemaAgencyName() <em>Schema Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaAgencyName()
	 * @generated
	 * @ordered
	 */
	protected String schemaAgencyName = SCHEMA_AGENCY_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemaDataURI() <em>Schema Data URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaDataURI()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEMA_DATA_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemaDataURI() <em>Schema Data URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaDataURI()
	 * @generated
	 * @ordered
	 */
	protected String schemaDataURI = SCHEMA_DATA_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemaID() <em>Schema ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaID()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEMA_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemaID() <em>Schema ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaID()
	 * @generated
	 * @ordered
	 */
	protected String schemaID = SCHEMA_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemaName() <em>Schema Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaName()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEMA_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemaName() <em>Schema Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaName()
	 * @generated
	 * @ordered
	 */
	protected String schemaName = SCHEMA_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemaURI() <em>Schema URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaURI()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEMA_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemaURI() <em>Schema URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaURI()
	 * @generated
	 * @ordered
	 */
	protected String schemaURI = SCHEMA_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemaVersionID() <em>Schema Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaVersionID()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEMA_VERSION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchemaVersionID() <em>Schema Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaVersionID()
	 * @generated
	 * @ordered
	 */
	protected String schemaVersionID = SCHEMA_VERSION_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitCode() <em>Unit Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCode()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitCode() <em>Unit Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCode()
	 * @generated
	 * @ordered
	 */
	protected String unitCode = UNIT_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitCodeListAgencyID() <em>Unit Code List Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListAgencyID()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_CODE_LIST_AGENCY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitCodeListAgencyID() <em>Unit Code List Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListAgencyID()
	 * @generated
	 * @ordered
	 */
	protected String unitCodeListAgencyID = UNIT_CODE_LIST_AGENCY_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitCodeListAgencyName() <em>Unit Code List Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListAgencyName()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_CODE_LIST_AGENCY_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitCodeListAgencyName() <em>Unit Code List Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListAgencyName()
	 * @generated
	 * @ordered
	 */
	protected String unitCodeListAgencyName = UNIT_CODE_LIST_AGENCY_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitCodeListID() <em>Unit Code List ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListID()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_CODE_LIST_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitCodeListID() <em>Unit Code List ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListID()
	 * @generated
	 * @ordered
	 */
	protected String unitCodeListID = UNIT_CODE_LIST_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitCodeListVersionID() <em>Unit Code List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListVersionID()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_CODE_LIST_VERSION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitCodeListVersionID() <em>Unit Code List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListVersionID()
	 * @generated
	 * @ordered
	 */
	protected String unitCodeListVersionID = UNIT_CODE_LIST_VERSION_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getUri() <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUri()
	 * @generated
	 * @ordered
	 */
	protected static final String URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUri() <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUri()
	 * @generated
	 * @ordered
	 */
	protected String uri = URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnyGenericValueTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getAnyGenericValueType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCharacterSetCode() {
		return characterSetCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCharacterSetCode(String newCharacterSetCode) {
		String oldCharacterSetCode = characterSetCode;
		characterSetCode = newCharacterSetCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CHARACTER_SET_CODE, oldCharacterSetCode, characterSetCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCurrencyCodeListVersionID() {
		return currencyCodeListVersionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrencyCodeListVersionID(String newCurrencyCodeListVersionID) {
		String oldCurrencyCodeListVersionID = currencyCodeListVersionID;
		currencyCodeListVersionID = newCurrencyCodeListVersionID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CURRENCY_CODE_LIST_VERSION_ID, oldCurrencyCodeListVersionID, currencyCodeListVersionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCurrencyID() {
		return currencyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrencyID(String newCurrencyID) {
		String oldCurrencyID = currencyID;
		currencyID = newCurrencyID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CURRENCY_ID, oldCurrencyID, currencyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEncodingCode() {
		return encodingCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEncodingCode(String newEncodingCode) {
		String oldEncodingCode = encodingCode;
		encodingCode = newEncodingCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__ENCODING_CODE, oldEncodingCode, encodingCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilename(String newFilename) {
		String oldFilename = filename;
		filename = newFilename;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__FILENAME, oldFilename, filename));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormat(String newFormat) {
		String oldFormat = format;
		format = newFormat;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__FORMAT, oldFormat, format));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLanguageID() {
		return languageID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLanguageID(String newLanguageID) {
		String oldLanguageID = languageID;
		languageID = newLanguageID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LANGUAGE_ID, oldLanguageID, languageID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLanguageLocaleID() {
		return languageLocaleID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLanguageLocaleID(String newLanguageLocaleID) {
		String oldLanguageLocaleID = languageLocaleID;
		languageLocaleID = newLanguageLocaleID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LANGUAGE_LOCALE_ID, oldLanguageLocaleID, languageLocaleID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListAgencyID() {
		return listAgencyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListAgencyID(String newListAgencyID) {
		String oldListAgencyID = listAgencyID;
		listAgencyID = newListAgencyID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_AGENCY_ID, oldListAgencyID, listAgencyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListAgencyName() {
		return listAgencyName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListAgencyName(String newListAgencyName) {
		String oldListAgencyName = listAgencyName;
		listAgencyName = newListAgencyName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_AGENCY_NAME, oldListAgencyName, listAgencyName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListID() {
		return listID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListID(String newListID) {
		String oldListID = listID;
		listID = newListID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_ID, oldListID, listID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListName() {
		return listName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListName(String newListName) {
		String oldListName = listName;
		listName = newListName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_NAME, oldListName, listName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListSchemaURI() {
		return listSchemaURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListSchemaURI(String newListSchemaURI) {
		String oldListSchemaURI = listSchemaURI;
		listSchemaURI = newListSchemaURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_SCHEMA_URI, oldListSchemaURI, listSchemaURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListURI() {
		return listURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListURI(String newListURI) {
		String oldListURI = listURI;
		listURI = newListURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_URI, oldListURI, listURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListVersionID() {
		return listVersionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListVersionID(String newListVersionID) {
		String oldListVersionID = listVersionID;
		listVersionID = newListVersionID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_VERSION_ID, oldListVersionID, listVersionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMimeCode() {
		return mimeCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMimeCode(String newMimeCode) {
		String oldMimeCode = mimeCode;
		mimeCode = newMimeCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__MIME_CODE, oldMimeCode, mimeCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemaAgencyID() {
		return schemaAgencyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemaAgencyID(String newSchemaAgencyID) {
		String oldSchemaAgencyID = schemaAgencyID;
		schemaAgencyID = newSchemaAgencyID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_AGENCY_ID, oldSchemaAgencyID, schemaAgencyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemaAgencyName() {
		return schemaAgencyName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemaAgencyName(String newSchemaAgencyName) {
		String oldSchemaAgencyName = schemaAgencyName;
		schemaAgencyName = newSchemaAgencyName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_AGENCY_NAME, oldSchemaAgencyName, schemaAgencyName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemaDataURI() {
		return schemaDataURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemaDataURI(String newSchemaDataURI) {
		String oldSchemaDataURI = schemaDataURI;
		schemaDataURI = newSchemaDataURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_DATA_URI, oldSchemaDataURI, schemaDataURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemaID() {
		return schemaID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemaID(String newSchemaID) {
		String oldSchemaID = schemaID;
		schemaID = newSchemaID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_ID, oldSchemaID, schemaID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemaName() {
		return schemaName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemaName(String newSchemaName) {
		String oldSchemaName = schemaName;
		schemaName = newSchemaName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_NAME, oldSchemaName, schemaName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemaURI() {
		return schemaURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemaURI(String newSchemaURI) {
		String oldSchemaURI = schemaURI;
		schemaURI = newSchemaURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_URI, oldSchemaURI, schemaURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSchemaVersionID() {
		return schemaVersionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemaVersionID(String newSchemaVersionID) {
		String oldSchemaVersionID = schemaVersionID;
		schemaVersionID = newSchemaVersionID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_VERSION_ID, oldSchemaVersionID, schemaVersionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitCode() {
		return unitCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitCode(String newUnitCode) {
		String oldUnitCode = unitCode;
		unitCode = newUnitCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE, oldUnitCode, unitCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitCodeListAgencyID() {
		return unitCodeListAgencyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitCodeListAgencyID(String newUnitCodeListAgencyID) {
		String oldUnitCodeListAgencyID = unitCodeListAgencyID;
		unitCodeListAgencyID = newUnitCodeListAgencyID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_AGENCY_ID, oldUnitCodeListAgencyID, unitCodeListAgencyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitCodeListAgencyName() {
		return unitCodeListAgencyName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitCodeListAgencyName(String newUnitCodeListAgencyName) {
		String oldUnitCodeListAgencyName = unitCodeListAgencyName;
		unitCodeListAgencyName = newUnitCodeListAgencyName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_AGENCY_NAME, oldUnitCodeListAgencyName, unitCodeListAgencyName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitCodeListID() {
		return unitCodeListID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitCodeListID(String newUnitCodeListID) {
		String oldUnitCodeListID = unitCodeListID;
		unitCodeListID = newUnitCodeListID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_ID, oldUnitCodeListID, unitCodeListID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitCodeListVersionID() {
		return unitCodeListVersionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitCodeListVersionID(String newUnitCodeListVersionID) {
		String oldUnitCodeListVersionID = unitCodeListVersionID;
		unitCodeListVersionID = newUnitCodeListVersionID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_VERSION_ID, oldUnitCodeListVersionID, unitCodeListVersionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUri(String newUri) {
		String oldUri = uri;
		uri = newUri;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.ANY_GENERIC_VALUE_TYPE__URI, oldUri, uri));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__VALUE:
				return getValue();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CHARACTER_SET_CODE:
				return getCharacterSetCode();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CURRENCY_CODE_LIST_VERSION_ID:
				return getCurrencyCodeListVersionID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CURRENCY_ID:
				return getCurrencyID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__ENCODING_CODE:
				return getEncodingCode();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__FILENAME:
				return getFilename();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__FORMAT:
				return getFormat();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LANGUAGE_ID:
				return getLanguageID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LANGUAGE_LOCALE_ID:
				return getLanguageLocaleID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_AGENCY_ID:
				return getListAgencyID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_AGENCY_NAME:
				return getListAgencyName();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_ID:
				return getListID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_NAME:
				return getListName();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_SCHEMA_URI:
				return getListSchemaURI();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_URI:
				return getListURI();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_VERSION_ID:
				return getListVersionID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__MIME_CODE:
				return getMimeCode();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__NAME:
				return getName();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_AGENCY_ID:
				return getSchemaAgencyID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_AGENCY_NAME:
				return getSchemaAgencyName();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_DATA_URI:
				return getSchemaDataURI();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_ID:
				return getSchemaID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_NAME:
				return getSchemaName();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_URI:
				return getSchemaURI();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_VERSION_ID:
				return getSchemaVersionID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE:
				return getUnitCode();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_AGENCY_ID:
				return getUnitCodeListAgencyID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_AGENCY_NAME:
				return getUnitCodeListAgencyName();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_ID:
				return getUnitCodeListID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_VERSION_ID:
				return getUnitCodeListVersionID();
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__URI:
				return getUri();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__VALUE:
				setValue((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CHARACTER_SET_CODE:
				setCharacterSetCode((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CURRENCY_CODE_LIST_VERSION_ID:
				setCurrencyCodeListVersionID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CURRENCY_ID:
				setCurrencyID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__ENCODING_CODE:
				setEncodingCode((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__FILENAME:
				setFilename((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__FORMAT:
				setFormat((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LANGUAGE_ID:
				setLanguageID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LANGUAGE_LOCALE_ID:
				setLanguageLocaleID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_AGENCY_ID:
				setListAgencyID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_AGENCY_NAME:
				setListAgencyName((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_ID:
				setListID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_NAME:
				setListName((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_SCHEMA_URI:
				setListSchemaURI((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_URI:
				setListURI((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_VERSION_ID:
				setListVersionID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__MIME_CODE:
				setMimeCode((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__NAME:
				setName((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_AGENCY_ID:
				setSchemaAgencyID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_AGENCY_NAME:
				setSchemaAgencyName((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_DATA_URI:
				setSchemaDataURI((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_ID:
				setSchemaID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_NAME:
				setSchemaName((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_URI:
				setSchemaURI((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_VERSION_ID:
				setSchemaVersionID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE:
				setUnitCode((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_AGENCY_ID:
				setUnitCodeListAgencyID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_AGENCY_NAME:
				setUnitCodeListAgencyName((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_ID:
				setUnitCodeListID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_VERSION_ID:
				setUnitCodeListVersionID((String)newValue);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__URI:
				setUri((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CHARACTER_SET_CODE:
				setCharacterSetCode(CHARACTER_SET_CODE_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CURRENCY_CODE_LIST_VERSION_ID:
				setCurrencyCodeListVersionID(CURRENCY_CODE_LIST_VERSION_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CURRENCY_ID:
				setCurrencyID(CURRENCY_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__ENCODING_CODE:
				setEncodingCode(ENCODING_CODE_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__FILENAME:
				setFilename(FILENAME_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__FORMAT:
				setFormat(FORMAT_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LANGUAGE_ID:
				setLanguageID(LANGUAGE_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LANGUAGE_LOCALE_ID:
				setLanguageLocaleID(LANGUAGE_LOCALE_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_AGENCY_ID:
				setListAgencyID(LIST_AGENCY_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_AGENCY_NAME:
				setListAgencyName(LIST_AGENCY_NAME_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_ID:
				setListID(LIST_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_NAME:
				setListName(LIST_NAME_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_SCHEMA_URI:
				setListSchemaURI(LIST_SCHEMA_URI_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_URI:
				setListURI(LIST_URI_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_VERSION_ID:
				setListVersionID(LIST_VERSION_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__MIME_CODE:
				setMimeCode(MIME_CODE_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_AGENCY_ID:
				setSchemaAgencyID(SCHEMA_AGENCY_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_AGENCY_NAME:
				setSchemaAgencyName(SCHEMA_AGENCY_NAME_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_DATA_URI:
				setSchemaDataURI(SCHEMA_DATA_URI_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_ID:
				setSchemaID(SCHEMA_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_NAME:
				setSchemaName(SCHEMA_NAME_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_URI:
				setSchemaURI(SCHEMA_URI_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_VERSION_ID:
				setSchemaVersionID(SCHEMA_VERSION_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE:
				setUnitCode(UNIT_CODE_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_AGENCY_ID:
				setUnitCodeListAgencyID(UNIT_CODE_LIST_AGENCY_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_AGENCY_NAME:
				setUnitCodeListAgencyName(UNIT_CODE_LIST_AGENCY_NAME_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_ID:
				setUnitCodeListID(UNIT_CODE_LIST_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_VERSION_ID:
				setUnitCodeListVersionID(UNIT_CODE_LIST_VERSION_ID_EDEFAULT);
				return;
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__URI:
				setUri(URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CHARACTER_SET_CODE:
				return CHARACTER_SET_CODE_EDEFAULT == null ? characterSetCode != null : !CHARACTER_SET_CODE_EDEFAULT.equals(characterSetCode);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CURRENCY_CODE_LIST_VERSION_ID:
				return CURRENCY_CODE_LIST_VERSION_ID_EDEFAULT == null ? currencyCodeListVersionID != null : !CURRENCY_CODE_LIST_VERSION_ID_EDEFAULT.equals(currencyCodeListVersionID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__CURRENCY_ID:
				return CURRENCY_ID_EDEFAULT == null ? currencyID != null : !CURRENCY_ID_EDEFAULT.equals(currencyID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__ENCODING_CODE:
				return ENCODING_CODE_EDEFAULT == null ? encodingCode != null : !ENCODING_CODE_EDEFAULT.equals(encodingCode);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__FILENAME:
				return FILENAME_EDEFAULT == null ? filename != null : !FILENAME_EDEFAULT.equals(filename);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__FORMAT:
				return FORMAT_EDEFAULT == null ? format != null : !FORMAT_EDEFAULT.equals(format);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LANGUAGE_ID:
				return LANGUAGE_ID_EDEFAULT == null ? languageID != null : !LANGUAGE_ID_EDEFAULT.equals(languageID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LANGUAGE_LOCALE_ID:
				return LANGUAGE_LOCALE_ID_EDEFAULT == null ? languageLocaleID != null : !LANGUAGE_LOCALE_ID_EDEFAULT.equals(languageLocaleID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_AGENCY_ID:
				return LIST_AGENCY_ID_EDEFAULT == null ? listAgencyID != null : !LIST_AGENCY_ID_EDEFAULT.equals(listAgencyID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_AGENCY_NAME:
				return LIST_AGENCY_NAME_EDEFAULT == null ? listAgencyName != null : !LIST_AGENCY_NAME_EDEFAULT.equals(listAgencyName);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_ID:
				return LIST_ID_EDEFAULT == null ? listID != null : !LIST_ID_EDEFAULT.equals(listID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_NAME:
				return LIST_NAME_EDEFAULT == null ? listName != null : !LIST_NAME_EDEFAULT.equals(listName);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_SCHEMA_URI:
				return LIST_SCHEMA_URI_EDEFAULT == null ? listSchemaURI != null : !LIST_SCHEMA_URI_EDEFAULT.equals(listSchemaURI);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_URI:
				return LIST_URI_EDEFAULT == null ? listURI != null : !LIST_URI_EDEFAULT.equals(listURI);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__LIST_VERSION_ID:
				return LIST_VERSION_ID_EDEFAULT == null ? listVersionID != null : !LIST_VERSION_ID_EDEFAULT.equals(listVersionID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__MIME_CODE:
				return MIME_CODE_EDEFAULT == null ? mimeCode != null : !MIME_CODE_EDEFAULT.equals(mimeCode);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_AGENCY_ID:
				return SCHEMA_AGENCY_ID_EDEFAULT == null ? schemaAgencyID != null : !SCHEMA_AGENCY_ID_EDEFAULT.equals(schemaAgencyID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_AGENCY_NAME:
				return SCHEMA_AGENCY_NAME_EDEFAULT == null ? schemaAgencyName != null : !SCHEMA_AGENCY_NAME_EDEFAULT.equals(schemaAgencyName);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_DATA_URI:
				return SCHEMA_DATA_URI_EDEFAULT == null ? schemaDataURI != null : !SCHEMA_DATA_URI_EDEFAULT.equals(schemaDataURI);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_ID:
				return SCHEMA_ID_EDEFAULT == null ? schemaID != null : !SCHEMA_ID_EDEFAULT.equals(schemaID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_NAME:
				return SCHEMA_NAME_EDEFAULT == null ? schemaName != null : !SCHEMA_NAME_EDEFAULT.equals(schemaName);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_URI:
				return SCHEMA_URI_EDEFAULT == null ? schemaURI != null : !SCHEMA_URI_EDEFAULT.equals(schemaURI);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__SCHEMA_VERSION_ID:
				return SCHEMA_VERSION_ID_EDEFAULT == null ? schemaVersionID != null : !SCHEMA_VERSION_ID_EDEFAULT.equals(schemaVersionID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE:
				return UNIT_CODE_EDEFAULT == null ? unitCode != null : !UNIT_CODE_EDEFAULT.equals(unitCode);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_AGENCY_ID:
				return UNIT_CODE_LIST_AGENCY_ID_EDEFAULT == null ? unitCodeListAgencyID != null : !UNIT_CODE_LIST_AGENCY_ID_EDEFAULT.equals(unitCodeListAgencyID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_AGENCY_NAME:
				return UNIT_CODE_LIST_AGENCY_NAME_EDEFAULT == null ? unitCodeListAgencyName != null : !UNIT_CODE_LIST_AGENCY_NAME_EDEFAULT.equals(unitCodeListAgencyName);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_ID:
				return UNIT_CODE_LIST_ID_EDEFAULT == null ? unitCodeListID != null : !UNIT_CODE_LIST_ID_EDEFAULT.equals(unitCodeListID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__UNIT_CODE_LIST_VERSION_ID:
				return UNIT_CODE_LIST_VERSION_ID_EDEFAULT == null ? unitCodeListVersionID != null : !UNIT_CODE_LIST_VERSION_ID_EDEFAULT.equals(unitCodeListVersionID);
			case B2MMLPackage.ANY_GENERIC_VALUE_TYPE__URI:
				return URI_EDEFAULT == null ? uri != null : !URI_EDEFAULT.equals(uri);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", characterSetCode: ");
		result.append(characterSetCode);
		result.append(", currencyCodeListVersionID: ");
		result.append(currencyCodeListVersionID);
		result.append(", currencyID: ");
		result.append(currencyID);
		result.append(", encodingCode: ");
		result.append(encodingCode);
		result.append(", filename: ");
		result.append(filename);
		result.append(", format: ");
		result.append(format);
		result.append(", languageID: ");
		result.append(languageID);
		result.append(", languageLocaleID: ");
		result.append(languageLocaleID);
		result.append(", listAgencyID: ");
		result.append(listAgencyID);
		result.append(", listAgencyName: ");
		result.append(listAgencyName);
		result.append(", listID: ");
		result.append(listID);
		result.append(", listName: ");
		result.append(listName);
		result.append(", listSchemaURI: ");
		result.append(listSchemaURI);
		result.append(", listURI: ");
		result.append(listURI);
		result.append(", listVersionID: ");
		result.append(listVersionID);
		result.append(", mimeCode: ");
		result.append(mimeCode);
		result.append(", name: ");
		result.append(name);
		result.append(", schemaAgencyID: ");
		result.append(schemaAgencyID);
		result.append(", schemaAgencyName: ");
		result.append(schemaAgencyName);
		result.append(", schemaDataURI: ");
		result.append(schemaDataURI);
		result.append(", schemaID: ");
		result.append(schemaID);
		result.append(", schemaName: ");
		result.append(schemaName);
		result.append(", schemaURI: ");
		result.append(schemaURI);
		result.append(", schemaVersionID: ");
		result.append(schemaVersionID);
		result.append(", unitCode: ");
		result.append(unitCode);
		result.append(", unitCodeListAgencyID: ");
		result.append(unitCodeListAgencyID);
		result.append(", unitCodeListAgencyName: ");
		result.append(unitCodeListAgencyName);
		result.append(", unitCodeListID: ");
		result.append(unitCodeListID);
		result.append(", unitCodeListVersionID: ");
		result.append(unitCodeListVersionID);
		result.append(", uri: ");
		result.append(uri);
		result.append(')');
		return result.toString();
	}

} //AnyGenericValueTypeImpl
