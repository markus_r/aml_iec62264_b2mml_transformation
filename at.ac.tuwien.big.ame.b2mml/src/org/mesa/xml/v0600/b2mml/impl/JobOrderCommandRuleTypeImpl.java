/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.JobOrderCommandRuleType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Job Order Command Rule Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class JobOrderCommandRuleTypeImpl extends TextTypeImpl implements JobOrderCommandRuleType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JobOrderCommandRuleTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getJobOrderCommandRuleType();
	}

} //JobOrderCommandRuleTypeImpl
