/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.OperationsRequestIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Request ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OperationsRequestIDTypeImpl extends IdentifierTypeImpl implements OperationsRequestIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsRequestIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOperationsRequestIDType();
	}

} //OperationsRequestIDTypeImpl
