/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.NameType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetCapabilityTestSpecificationIDType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetClassPropertyType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetClassType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Class Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetClassTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetClassTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetClassTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetClassTypeImpl#getManufacturer <em>Manufacturer</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetClassTypeImpl#getPhysicalAssetClassProperty <em>Physical Asset Class Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetClassTypeImpl#getPhysicalAssetID <em>Physical Asset ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetClassTypeImpl#getPhysicalAssetCapabilityTestSpecificationID <em>Physical Asset Capability Test Specification ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalAssetClassTypeImpl extends MinimalEObjectImpl.Container implements PhysicalAssetClassType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getManufacturer() <em>Manufacturer</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManufacturer()
	 * @generated
	 * @ordered
	 */
	protected EList<NameType> manufacturer;

	/**
	 * The cached value of the '{@link #getPhysicalAssetClassProperty() <em>Physical Asset Class Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClassProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetClassPropertyType> physicalAssetClassProperty;

	/**
	 * The cached value of the '{@link #getPhysicalAssetID() <em>Physical Asset ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetID()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetIDType> physicalAssetID;

	/**
	 * The cached value of the '{@link #getPhysicalAssetCapabilityTestSpecificationID() <em>Physical Asset Capability Test Specification ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetCapabilityTestSpecificationID()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetCapabilityTestSpecificationIDType> physicalAssetCapabilityTestSpecificationID;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetClassTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPhysicalAssetClassType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NameType> getManufacturer() {
		if (manufacturer == null) {
			manufacturer = new EObjectContainmentEList<NameType>(NameType.class, this, B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__MANUFACTURER);
		}
		return manufacturer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetClassPropertyType> getPhysicalAssetClassProperty() {
		if (physicalAssetClassProperty == null) {
			physicalAssetClassProperty = new EObjectContainmentEList<PhysicalAssetClassPropertyType>(PhysicalAssetClassPropertyType.class, this, B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_CLASS_PROPERTY);
		}
		return physicalAssetClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetIDType> getPhysicalAssetID() {
		if (physicalAssetID == null) {
			physicalAssetID = new EObjectContainmentEList<PhysicalAssetIDType>(PhysicalAssetIDType.class, this, B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_ID);
		}
		return physicalAssetID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetCapabilityTestSpecificationIDType> getPhysicalAssetCapabilityTestSpecificationID() {
		if (physicalAssetCapabilityTestSpecificationID == null) {
			physicalAssetCapabilityTestSpecificationID = new EObjectContainmentEList<PhysicalAssetCapabilityTestSpecificationIDType>(PhysicalAssetCapabilityTestSpecificationIDType.class, this, B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID);
		}
		return physicalAssetCapabilityTestSpecificationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__MANUFACTURER:
				return ((InternalEList<?>)getManufacturer()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_CLASS_PROPERTY:
				return ((InternalEList<?>)getPhysicalAssetClassProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_ID:
				return ((InternalEList<?>)getPhysicalAssetID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				return ((InternalEList<?>)getPhysicalAssetCapabilityTestSpecificationID()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__ID:
				return getID();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__MANUFACTURER:
				return getManufacturer();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_CLASS_PROPERTY:
				return getPhysicalAssetClassProperty();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_ID:
				return getPhysicalAssetID();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				return getPhysicalAssetCapabilityTestSpecificationID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__MANUFACTURER:
				getManufacturer().clear();
				getManufacturer().addAll((Collection<? extends NameType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_CLASS_PROPERTY:
				getPhysicalAssetClassProperty().clear();
				getPhysicalAssetClassProperty().addAll((Collection<? extends PhysicalAssetClassPropertyType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_ID:
				getPhysicalAssetID().clear();
				getPhysicalAssetID().addAll((Collection<? extends PhysicalAssetIDType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				getPhysicalAssetCapabilityTestSpecificationID().clear();
				getPhysicalAssetCapabilityTestSpecificationID().addAll((Collection<? extends PhysicalAssetCapabilityTestSpecificationIDType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__MANUFACTURER:
				getManufacturer().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_CLASS_PROPERTY:
				getPhysicalAssetClassProperty().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_ID:
				getPhysicalAssetID().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				getPhysicalAssetCapabilityTestSpecificationID().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__ID:
				return iD != null;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__MANUFACTURER:
				return manufacturer != null && !manufacturer.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_CLASS_PROPERTY:
				return physicalAssetClassProperty != null && !physicalAssetClassProperty.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_ID:
				return physicalAssetID != null && !physicalAssetID.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_CLASS_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				return physicalAssetCapabilityTestSpecificationID != null && !physicalAssetCapabilityTestSpecificationID.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PhysicalAssetClassTypeImpl
