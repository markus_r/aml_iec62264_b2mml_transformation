/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.QuantityType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Quantity Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.QuantityTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.QuantityTypeImpl#getUnitCode <em>Unit Code</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.QuantityTypeImpl#getUnitCodeListAgencyID <em>Unit Code List Agency ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.QuantityTypeImpl#getUnitCodeListAgencyName <em>Unit Code List Agency Name</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.QuantityTypeImpl#getUnitCodeListID <em>Unit Code List ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class QuantityTypeImpl extends MinimalEObjectImpl.Container implements QuantityType {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitCode() <em>Unit Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCode()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitCode() <em>Unit Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCode()
	 * @generated
	 * @ordered
	 */
	protected String unitCode = UNIT_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitCodeListAgencyID() <em>Unit Code List Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListAgencyID()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_CODE_LIST_AGENCY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitCodeListAgencyID() <em>Unit Code List Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListAgencyID()
	 * @generated
	 * @ordered
	 */
	protected String unitCodeListAgencyID = UNIT_CODE_LIST_AGENCY_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitCodeListAgencyName() <em>Unit Code List Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListAgencyName()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_CODE_LIST_AGENCY_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitCodeListAgencyName() <em>Unit Code List Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListAgencyName()
	 * @generated
	 * @ordered
	 */
	protected String unitCodeListAgencyName = UNIT_CODE_LIST_AGENCY_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitCodeListID() <em>Unit Code List ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListID()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_CODE_LIST_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitCodeListID() <em>Unit Code List ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitCodeListID()
	 * @generated
	 * @ordered
	 */
	protected String unitCodeListID = UNIT_CODE_LIST_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QuantityTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getQuantityType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(BigDecimal newValue) {
		BigDecimal oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_TYPE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitCode() {
		return unitCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitCode(String newUnitCode) {
		String oldUnitCode = unitCode;
		unitCode = newUnitCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_TYPE__UNIT_CODE, oldUnitCode, unitCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitCodeListAgencyID() {
		return unitCodeListAgencyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitCodeListAgencyID(String newUnitCodeListAgencyID) {
		String oldUnitCodeListAgencyID = unitCodeListAgencyID;
		unitCodeListAgencyID = newUnitCodeListAgencyID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_AGENCY_ID, oldUnitCodeListAgencyID, unitCodeListAgencyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitCodeListAgencyName() {
		return unitCodeListAgencyName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitCodeListAgencyName(String newUnitCodeListAgencyName) {
		String oldUnitCodeListAgencyName = unitCodeListAgencyName;
		unitCodeListAgencyName = newUnitCodeListAgencyName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_AGENCY_NAME, oldUnitCodeListAgencyName, unitCodeListAgencyName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitCodeListID() {
		return unitCodeListID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitCodeListID(String newUnitCodeListID) {
		String oldUnitCodeListID = unitCodeListID;
		unitCodeListID = newUnitCodeListID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_ID, oldUnitCodeListID, unitCodeListID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.QUANTITY_TYPE__VALUE:
				return getValue();
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE:
				return getUnitCode();
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_AGENCY_ID:
				return getUnitCodeListAgencyID();
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_AGENCY_NAME:
				return getUnitCodeListAgencyName();
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_ID:
				return getUnitCodeListID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.QUANTITY_TYPE__VALUE:
				setValue((BigDecimal)newValue);
				return;
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE:
				setUnitCode((String)newValue);
				return;
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_AGENCY_ID:
				setUnitCodeListAgencyID((String)newValue);
				return;
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_AGENCY_NAME:
				setUnitCodeListAgencyName((String)newValue);
				return;
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_ID:
				setUnitCodeListID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.QUANTITY_TYPE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE:
				setUnitCode(UNIT_CODE_EDEFAULT);
				return;
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_AGENCY_ID:
				setUnitCodeListAgencyID(UNIT_CODE_LIST_AGENCY_ID_EDEFAULT);
				return;
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_AGENCY_NAME:
				setUnitCodeListAgencyName(UNIT_CODE_LIST_AGENCY_NAME_EDEFAULT);
				return;
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_ID:
				setUnitCodeListID(UNIT_CODE_LIST_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.QUANTITY_TYPE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE:
				return UNIT_CODE_EDEFAULT == null ? unitCode != null : !UNIT_CODE_EDEFAULT.equals(unitCode);
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_AGENCY_ID:
				return UNIT_CODE_LIST_AGENCY_ID_EDEFAULT == null ? unitCodeListAgencyID != null : !UNIT_CODE_LIST_AGENCY_ID_EDEFAULT.equals(unitCodeListAgencyID);
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_AGENCY_NAME:
				return UNIT_CODE_LIST_AGENCY_NAME_EDEFAULT == null ? unitCodeListAgencyName != null : !UNIT_CODE_LIST_AGENCY_NAME_EDEFAULT.equals(unitCodeListAgencyName);
			case B2MMLPackage.QUANTITY_TYPE__UNIT_CODE_LIST_ID:
				return UNIT_CODE_LIST_ID_EDEFAULT == null ? unitCodeListID != null : !UNIT_CODE_LIST_ID_EDEFAULT.equals(unitCodeListID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", unitCode: ");
		result.append(unitCode);
		result.append(", unitCodeListAgencyID: ");
		result.append(unitCodeListAgencyID);
		result.append(", unitCodeListAgencyName: ");
		result.append(unitCodeListAgencyName);
		result.append(", unitCodeListID: ");
		result.append(unitCodeListID);
		result.append(')');
		return result.toString();
	}

} //QuantityTypeImpl
