/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.EquipmentAssetMappingType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetCapabilityTestSpecificationIDType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetClassIDType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetPropertyType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetTypeImpl#getPhysicalLocation <em>Physical Location</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetTypeImpl#getFixedAssetID <em>Fixed Asset ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetTypeImpl#getVendorID <em>Vendor ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetTypeImpl#getEquipmentLevel <em>Equipment Level</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetTypeImpl#getEquipmentAssetMapping <em>Equipment Asset Mapping</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetTypeImpl#getPhysicalAssetProperty <em>Physical Asset Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetTypeImpl#getPhysicalAsset <em>Physical Asset</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetTypeImpl#getPhysicalAssetClassID <em>Physical Asset Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetTypeImpl#getPhysicalAssetCapabilityTestSpecificationID <em>Physical Asset Capability Test Specification ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalAssetTypeImpl extends MinimalEObjectImpl.Container implements PhysicalAssetType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getPhysicalLocation() <em>Physical Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalLocation()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType physicalLocation;

	/**
	 * The cached value of the '{@link #getFixedAssetID() <em>Fixed Asset ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFixedAssetID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType fixedAssetID;

	/**
	 * The cached value of the '{@link #getVendorID() <em>Vendor ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVendorID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType vendorID;

	/**
	 * The cached value of the '{@link #getEquipmentLevel() <em>Equipment Level</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentLevel()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType equipmentLevel;

	/**
	 * The cached value of the '{@link #getEquipmentAssetMapping() <em>Equipment Asset Mapping</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentAssetMapping()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentAssetMappingType> equipmentAssetMapping;

	/**
	 * The cached value of the '{@link #getPhysicalAssetProperty() <em>Physical Asset Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetPropertyType> physicalAssetProperty;

	/**
	 * The cached value of the '{@link #getPhysicalAsset() <em>Physical Asset</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAsset()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetType> physicalAsset;

	/**
	 * The cached value of the '{@link #getPhysicalAssetClassID() <em>Physical Asset Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetClassIDType> physicalAssetClassID;

	/**
	 * The cached value of the '{@link #getPhysicalAssetCapabilityTestSpecificationID() <em>Physical Asset Capability Test Specification ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetCapabilityTestSpecificationID()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetCapabilityTestSpecificationIDType> physicalAssetCapabilityTestSpecificationID;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPhysicalAssetType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.PHYSICAL_ASSET_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getPhysicalLocation() {
		return physicalLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPhysicalLocation(IdentifierType newPhysicalLocation, NotificationChain msgs) {
		IdentifierType oldPhysicalLocation = physicalLocation;
		physicalLocation = newPhysicalLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_LOCATION, oldPhysicalLocation, newPhysicalLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalLocation(IdentifierType newPhysicalLocation) {
		if (newPhysicalLocation != physicalLocation) {
			NotificationChain msgs = null;
			if (physicalLocation != null)
				msgs = ((InternalEObject)physicalLocation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_LOCATION, null, msgs);
			if (newPhysicalLocation != null)
				msgs = ((InternalEObject)newPhysicalLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_LOCATION, null, msgs);
			msgs = basicSetPhysicalLocation(newPhysicalLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_LOCATION, newPhysicalLocation, newPhysicalLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getFixedAssetID() {
		return fixedAssetID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFixedAssetID(IdentifierType newFixedAssetID, NotificationChain msgs) {
		IdentifierType oldFixedAssetID = fixedAssetID;
		fixedAssetID = newFixedAssetID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_TYPE__FIXED_ASSET_ID, oldFixedAssetID, newFixedAssetID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFixedAssetID(IdentifierType newFixedAssetID) {
		if (newFixedAssetID != fixedAssetID) {
			NotificationChain msgs = null;
			if (fixedAssetID != null)
				msgs = ((InternalEObject)fixedAssetID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_TYPE__FIXED_ASSET_ID, null, msgs);
			if (newFixedAssetID != null)
				msgs = ((InternalEObject)newFixedAssetID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_TYPE__FIXED_ASSET_ID, null, msgs);
			msgs = basicSetFixedAssetID(newFixedAssetID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_TYPE__FIXED_ASSET_ID, newFixedAssetID, newFixedAssetID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getVendorID() {
		return vendorID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVendorID(IdentifierType newVendorID, NotificationChain msgs) {
		IdentifierType oldVendorID = vendorID;
		vendorID = newVendorID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_TYPE__VENDOR_ID, oldVendorID, newVendorID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVendorID(IdentifierType newVendorID) {
		if (newVendorID != vendorID) {
			NotificationChain msgs = null;
			if (vendorID != null)
				msgs = ((InternalEObject)vendorID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_TYPE__VENDOR_ID, null, msgs);
			if (newVendorID != null)
				msgs = ((InternalEObject)newVendorID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_TYPE__VENDOR_ID, null, msgs);
			msgs = basicSetVendorID(newVendorID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_TYPE__VENDOR_ID, newVendorID, newVendorID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getEquipmentLevel() {
		return equipmentLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentLevel(HierarchyScopeType newEquipmentLevel, NotificationChain msgs) {
		HierarchyScopeType oldEquipmentLevel = equipmentLevel;
		equipmentLevel = newEquipmentLevel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_LEVEL, oldEquipmentLevel, newEquipmentLevel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentLevel(HierarchyScopeType newEquipmentLevel) {
		if (newEquipmentLevel != equipmentLevel) {
			NotificationChain msgs = null;
			if (equipmentLevel != null)
				msgs = ((InternalEObject)equipmentLevel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_LEVEL, null, msgs);
			if (newEquipmentLevel != null)
				msgs = ((InternalEObject)newEquipmentLevel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_LEVEL, null, msgs);
			msgs = basicSetEquipmentLevel(newEquipmentLevel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_LEVEL, newEquipmentLevel, newEquipmentLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentAssetMappingType> getEquipmentAssetMapping() {
		if (equipmentAssetMapping == null) {
			equipmentAssetMapping = new EObjectContainmentEList<EquipmentAssetMappingType>(EquipmentAssetMappingType.class, this, B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_ASSET_MAPPING);
		}
		return equipmentAssetMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetPropertyType> getPhysicalAssetProperty() {
		if (physicalAssetProperty == null) {
			physicalAssetProperty = new EObjectContainmentEList<PhysicalAssetPropertyType>(PhysicalAssetPropertyType.class, this, B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_PROPERTY);
		}
		return physicalAssetProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetType> getPhysicalAsset() {
		if (physicalAsset == null) {
			physicalAsset = new EObjectContainmentEList<PhysicalAssetType>(PhysicalAssetType.class, this, B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET);
		}
		return physicalAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetClassIDType> getPhysicalAssetClassID() {
		if (physicalAssetClassID == null) {
			physicalAssetClassID = new EObjectContainmentEList<PhysicalAssetClassIDType>(PhysicalAssetClassIDType.class, this, B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_CLASS_ID);
		}
		return physicalAssetClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetCapabilityTestSpecificationIDType> getPhysicalAssetCapabilityTestSpecificationID() {
		if (physicalAssetCapabilityTestSpecificationID == null) {
			physicalAssetCapabilityTestSpecificationID = new EObjectContainmentEList<PhysicalAssetCapabilityTestSpecificationIDType>(PhysicalAssetCapabilityTestSpecificationIDType.class, this, B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID);
		}
		return physicalAssetCapabilityTestSpecificationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_LOCATION:
				return basicSetPhysicalLocation(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__FIXED_ASSET_ID:
				return basicSetFixedAssetID(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__VENDOR_ID:
				return basicSetVendorID(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_LEVEL:
				return basicSetEquipmentLevel(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_ASSET_MAPPING:
				return ((InternalEList<?>)getEquipmentAssetMapping()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_PROPERTY:
				return ((InternalEList<?>)getPhysicalAssetProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET:
				return ((InternalEList<?>)getPhysicalAsset()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return ((InternalEList<?>)getPhysicalAssetClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				return ((InternalEList<?>)getPhysicalAssetCapabilityTestSpecificationID()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__ID:
				return getID();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_LOCATION:
				return getPhysicalLocation();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__FIXED_ASSET_ID:
				return getFixedAssetID();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__VENDOR_ID:
				return getVendorID();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_LEVEL:
				return getEquipmentLevel();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_ASSET_MAPPING:
				return getEquipmentAssetMapping();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_PROPERTY:
				return getPhysicalAssetProperty();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET:
				return getPhysicalAsset();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return getPhysicalAssetClassID();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				return getPhysicalAssetCapabilityTestSpecificationID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_LOCATION:
				setPhysicalLocation((IdentifierType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__FIXED_ASSET_ID:
				setFixedAssetID((IdentifierType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__VENDOR_ID:
				setVendorID((IdentifierType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_LEVEL:
				setEquipmentLevel((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_ASSET_MAPPING:
				getEquipmentAssetMapping().clear();
				getEquipmentAssetMapping().addAll((Collection<? extends EquipmentAssetMappingType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_PROPERTY:
				getPhysicalAssetProperty().clear();
				getPhysicalAssetProperty().addAll((Collection<? extends PhysicalAssetPropertyType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET:
				getPhysicalAsset().clear();
				getPhysicalAsset().addAll((Collection<? extends PhysicalAssetType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_CLASS_ID:
				getPhysicalAssetClassID().clear();
				getPhysicalAssetClassID().addAll((Collection<? extends PhysicalAssetClassIDType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				getPhysicalAssetCapabilityTestSpecificationID().clear();
				getPhysicalAssetCapabilityTestSpecificationID().addAll((Collection<? extends PhysicalAssetCapabilityTestSpecificationIDType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_LOCATION:
				setPhysicalLocation((IdentifierType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__FIXED_ASSET_ID:
				setFixedAssetID((IdentifierType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__VENDOR_ID:
				setVendorID((IdentifierType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_LEVEL:
				setEquipmentLevel((HierarchyScopeType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_ASSET_MAPPING:
				getEquipmentAssetMapping().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_PROPERTY:
				getPhysicalAssetProperty().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET:
				getPhysicalAsset().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_CLASS_ID:
				getPhysicalAssetClassID().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				getPhysicalAssetCapabilityTestSpecificationID().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__ID:
				return iD != null;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_LOCATION:
				return physicalLocation != null;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__FIXED_ASSET_ID:
				return fixedAssetID != null;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__VENDOR_ID:
				return vendorID != null;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_LEVEL:
				return equipmentLevel != null;
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__EQUIPMENT_ASSET_MAPPING:
				return equipmentAssetMapping != null && !equipmentAssetMapping.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_PROPERTY:
				return physicalAssetProperty != null && !physicalAssetProperty.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET:
				return physicalAsset != null && !physicalAsset.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return physicalAssetClassID != null && !physicalAssetClassID.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_TYPE__PHYSICAL_ASSET_CAPABILITY_TEST_SPECIFICATION_ID:
				return physicalAssetCapabilityTestSpecificationID != null && !physicalAssetCapabilityTestSpecificationID.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PhysicalAssetTypeImpl
