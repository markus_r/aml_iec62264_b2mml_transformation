/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.RequestedCompletionDateType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requested Completion Date Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequestedCompletionDateTypeImpl extends DateTimeTypeImpl implements RequestedCompletionDateType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequestedCompletionDateTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getRequestedCompletionDateType();
	}

} //RequestedCompletionDateTypeImpl
