/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.JobOrderCommand1Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Job Order Command1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class JobOrderCommand1TypeImpl extends CodeTypeImpl implements JobOrderCommand1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JobOrderCommand1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getJobOrderCommand1Type();
	}

} //JobOrderCommand1TypeImpl
