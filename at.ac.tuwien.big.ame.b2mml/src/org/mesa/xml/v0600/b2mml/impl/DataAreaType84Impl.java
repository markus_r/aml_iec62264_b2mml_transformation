/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType84;
import org.mesa.xml.v0600.b2mml.MaterialDefinitionType;
import org.mesa.xml.v0600.b2mml.TransShowType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type84</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType84Impl#getShow <em>Show</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType84Impl#getMaterialDefinition <em>Material Definition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType84Impl extends MinimalEObjectImpl.Container implements DataAreaType84 {
	/**
	 * The cached value of the '{@link #getShow() <em>Show</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShow()
	 * @generated
	 * @ordered
	 */
	protected TransShowType show;

	/**
	 * The cached value of the '{@link #getMaterialDefinition() <em>Material Definition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialDefinitionType> materialDefinition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType84Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType84();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransShowType getShow() {
		return show;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShow(TransShowType newShow, NotificationChain msgs) {
		TransShowType oldShow = show;
		show = newShow;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE84__SHOW, oldShow, newShow);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShow(TransShowType newShow) {
		if (newShow != show) {
			NotificationChain msgs = null;
			if (show != null)
				msgs = ((InternalEObject)show).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE84__SHOW, null, msgs);
			if (newShow != null)
				msgs = ((InternalEObject)newShow).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE84__SHOW, null, msgs);
			msgs = basicSetShow(newShow, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE84__SHOW, newShow, newShow));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialDefinitionType> getMaterialDefinition() {
		if (materialDefinition == null) {
			materialDefinition = new EObjectContainmentEList<MaterialDefinitionType>(MaterialDefinitionType.class, this, B2MMLPackage.DATA_AREA_TYPE84__MATERIAL_DEFINITION);
		}
		return materialDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE84__SHOW:
				return basicSetShow(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE84__MATERIAL_DEFINITION:
				return ((InternalEList<?>)getMaterialDefinition()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE84__SHOW:
				return getShow();
			case B2MMLPackage.DATA_AREA_TYPE84__MATERIAL_DEFINITION:
				return getMaterialDefinition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE84__SHOW:
				setShow((TransShowType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE84__MATERIAL_DEFINITION:
				getMaterialDefinition().clear();
				getMaterialDefinition().addAll((Collection<? extends MaterialDefinitionType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE84__SHOW:
				setShow((TransShowType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE84__MATERIAL_DEFINITION:
				getMaterialDefinition().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE84__SHOW:
				return show != null;
			case B2MMLPackage.DATA_AREA_TYPE84__MATERIAL_DEFINITION:
				return materialDefinition != null && !materialDefinition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType84Impl
