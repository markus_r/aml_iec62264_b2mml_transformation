/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.ResourceIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ResourceIDTypeImpl extends IdentifierTypeImpl implements ResourceIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getResourceIDType();
	}

} //ResourceIDTypeImpl
