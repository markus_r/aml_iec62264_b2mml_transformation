/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.TransAcknowledgeType;
import org.mesa.xml.v0600.b2mml.TransApplicationAreaType;
import org.mesa.xml.v0600.b2mml.TransResponseCriteriaType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trans Acknowledge Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransAcknowledgeTypeImpl#getOriginalApplicationArea <em>Original Application Area</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransAcknowledgeTypeImpl#getResponseCriteria <em>Response Criteria</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransAcknowledgeTypeImpl extends MinimalEObjectImpl.Container implements TransAcknowledgeType {
	/**
	 * The cached value of the '{@link #getOriginalApplicationArea() <em>Original Application Area</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginalApplicationArea()
	 * @generated
	 * @ordered
	 */
	protected TransApplicationAreaType originalApplicationArea;

	/**
	 * The cached value of the '{@link #getResponseCriteria() <em>Response Criteria</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseCriteria()
	 * @generated
	 * @ordered
	 */
	protected EList<TransResponseCriteriaType> responseCriteria;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransAcknowledgeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTransAcknowledgeType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransApplicationAreaType getOriginalApplicationArea() {
		return originalApplicationArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOriginalApplicationArea(TransApplicationAreaType newOriginalApplicationArea, NotificationChain msgs) {
		TransApplicationAreaType oldOriginalApplicationArea = originalApplicationArea;
		originalApplicationArea = newOriginalApplicationArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__ORIGINAL_APPLICATION_AREA, oldOriginalApplicationArea, newOriginalApplicationArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOriginalApplicationArea(TransApplicationAreaType newOriginalApplicationArea) {
		if (newOriginalApplicationArea != originalApplicationArea) {
			NotificationChain msgs = null;
			if (originalApplicationArea != null)
				msgs = ((InternalEObject)originalApplicationArea).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__ORIGINAL_APPLICATION_AREA, null, msgs);
			if (newOriginalApplicationArea != null)
				msgs = ((InternalEObject)newOriginalApplicationArea).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__ORIGINAL_APPLICATION_AREA, null, msgs);
			msgs = basicSetOriginalApplicationArea(newOriginalApplicationArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__ORIGINAL_APPLICATION_AREA, newOriginalApplicationArea, newOriginalApplicationArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TransResponseCriteriaType> getResponseCriteria() {
		if (responseCriteria == null) {
			responseCriteria = new EObjectContainmentEList<TransResponseCriteriaType>(TransResponseCriteriaType.class, this, B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__RESPONSE_CRITERIA);
		}
		return responseCriteria;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__ORIGINAL_APPLICATION_AREA:
				return basicSetOriginalApplicationArea(null, msgs);
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__RESPONSE_CRITERIA:
				return ((InternalEList<?>)getResponseCriteria()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__ORIGINAL_APPLICATION_AREA:
				return getOriginalApplicationArea();
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__RESPONSE_CRITERIA:
				return getResponseCriteria();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__ORIGINAL_APPLICATION_AREA:
				setOriginalApplicationArea((TransApplicationAreaType)newValue);
				return;
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__RESPONSE_CRITERIA:
				getResponseCriteria().clear();
				getResponseCriteria().addAll((Collection<? extends TransResponseCriteriaType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__ORIGINAL_APPLICATION_AREA:
				setOriginalApplicationArea((TransApplicationAreaType)null);
				return;
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__RESPONSE_CRITERIA:
				getResponseCriteria().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__ORIGINAL_APPLICATION_AREA:
				return originalApplicationArea != null;
			case B2MMLPackage.TRANS_ACKNOWLEDGE_TYPE__RESPONSE_CRITERIA:
				return responseCriteria != null && !responseCriteria.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TransAcknowledgeTypeImpl
