/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.ExpirationTimeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Expiration Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ExpirationTimeTypeImpl extends DateTimeTypeImpl implements ExpirationTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpirationTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getExpirationTimeType();
	}

} //ExpirationTimeTypeImpl
