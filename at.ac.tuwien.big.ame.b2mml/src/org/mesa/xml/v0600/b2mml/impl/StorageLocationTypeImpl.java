/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.StorageLocationType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Storage Location Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StorageLocationTypeImpl extends IdentifierTypeImpl implements StorageLocationType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StorageLocationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getStorageLocationType();
	}

} //StorageLocationTypeImpl
