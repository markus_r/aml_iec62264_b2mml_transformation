/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.PriorityType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Priority Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PriorityTypeImpl extends NumericTypeImpl implements PriorityType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PriorityTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPriorityType();
	}

} //PriorityTypeImpl
