/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.OperationsType1Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operations Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OperationsType1TypeImpl extends CodeTypeImpl implements OperationsType1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationsType1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOperationsType1Type();
	}

} //OperationsType1TypeImpl
