/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.LocationType;
import org.mesa.xml.v0600.b2mml.PersonIDType;
import org.mesa.xml.v0600.b2mml.PersonnelClassPropertyType;
import org.mesa.xml.v0600.b2mml.PersonnelClassType;
import org.mesa.xml.v0600.b2mml.QualificationTestSpecificationIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Personnel Class Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PersonnelClassTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PersonnelClassTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PersonnelClassTypeImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PersonnelClassTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PersonnelClassTypeImpl#getPersonnelClassProperty <em>Personnel Class Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PersonnelClassTypeImpl#getPersonID <em>Person ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PersonnelClassTypeImpl#getQualificationTestSpecificationID <em>Qualification Test Specification ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonnelClassTypeImpl extends MinimalEObjectImpl.Container implements PersonnelClassType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected LocationType location;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getPersonnelClassProperty() <em>Personnel Class Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelClassProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonnelClassPropertyType> personnelClassProperty;

	/**
	 * The cached value of the '{@link #getPersonID() <em>Person ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonID()
	 * @generated
	 * @ordered
	 */
	protected EList<PersonIDType> personID;

	/**
	 * The cached value of the '{@link #getQualificationTestSpecificationID() <em>Qualification Test Specification ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualificationTestSpecificationID()
	 * @generated
	 * @ordered
	 */
	protected EList<QualificationTestSpecificationIDType> qualificationTestSpecificationID;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonnelClassTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPersonnelClassType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSONNEL_CLASS_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSONNEL_CLASS_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSONNEL_CLASS_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSONNEL_CLASS_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.PERSONNEL_CLASS_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationType getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocation(LocationType newLocation, NotificationChain msgs) {
		LocationType oldLocation = location;
		location = newLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSONNEL_CLASS_TYPE__LOCATION, oldLocation, newLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(LocationType newLocation) {
		if (newLocation != location) {
			NotificationChain msgs = null;
			if (location != null)
				msgs = ((InternalEObject)location).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSONNEL_CLASS_TYPE__LOCATION, null, msgs);
			if (newLocation != null)
				msgs = ((InternalEObject)newLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSONNEL_CLASS_TYPE__LOCATION, null, msgs);
			msgs = basicSetLocation(newLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSONNEL_CLASS_TYPE__LOCATION, newLocation, newLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSONNEL_CLASS_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSONNEL_CLASS_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PERSONNEL_CLASS_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PERSONNEL_CLASS_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonnelClassPropertyType> getPersonnelClassProperty() {
		if (personnelClassProperty == null) {
			personnelClassProperty = new EObjectContainmentEList<PersonnelClassPropertyType>(PersonnelClassPropertyType.class, this, B2MMLPackage.PERSONNEL_CLASS_TYPE__PERSONNEL_CLASS_PROPERTY);
		}
		return personnelClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PersonIDType> getPersonID() {
		if (personID == null) {
			personID = new EObjectContainmentEList<PersonIDType>(PersonIDType.class, this, B2MMLPackage.PERSONNEL_CLASS_TYPE__PERSON_ID);
		}
		return personID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QualificationTestSpecificationIDType> getQualificationTestSpecificationID() {
		if (qualificationTestSpecificationID == null) {
			qualificationTestSpecificationID = new EObjectContainmentEList<QualificationTestSpecificationIDType>(QualificationTestSpecificationIDType.class, this, B2MMLPackage.PERSONNEL_CLASS_TYPE__QUALIFICATION_TEST_SPECIFICATION_ID);
		}
		return qualificationTestSpecificationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__LOCATION:
				return basicSetLocation(null, msgs);
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__PERSONNEL_CLASS_PROPERTY:
				return ((InternalEList<?>)getPersonnelClassProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__PERSON_ID:
				return ((InternalEList<?>)getPersonID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__QUALIFICATION_TEST_SPECIFICATION_ID:
				return ((InternalEList<?>)getQualificationTestSpecificationID()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__ID:
				return getID();
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__LOCATION:
				return getLocation();
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__PERSONNEL_CLASS_PROPERTY:
				return getPersonnelClassProperty();
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__PERSON_ID:
				return getPersonID();
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__QUALIFICATION_TEST_SPECIFICATION_ID:
				return getQualificationTestSpecificationID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__LOCATION:
				setLocation((LocationType)newValue);
				return;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__PERSONNEL_CLASS_PROPERTY:
				getPersonnelClassProperty().clear();
				getPersonnelClassProperty().addAll((Collection<? extends PersonnelClassPropertyType>)newValue);
				return;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__PERSON_ID:
				getPersonID().clear();
				getPersonID().addAll((Collection<? extends PersonIDType>)newValue);
				return;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__QUALIFICATION_TEST_SPECIFICATION_ID:
				getQualificationTestSpecificationID().clear();
				getQualificationTestSpecificationID().addAll((Collection<? extends QualificationTestSpecificationIDType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__LOCATION:
				setLocation((LocationType)null);
				return;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__PERSONNEL_CLASS_PROPERTY:
				getPersonnelClassProperty().clear();
				return;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__PERSON_ID:
				getPersonID().clear();
				return;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__QUALIFICATION_TEST_SPECIFICATION_ID:
				getQualificationTestSpecificationID().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__ID:
				return iD != null;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__LOCATION:
				return location != null;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__PERSONNEL_CLASS_PROPERTY:
				return personnelClassProperty != null && !personnelClassProperty.isEmpty();
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__PERSON_ID:
				return personID != null && !personID.isEmpty();
			case B2MMLPackage.PERSONNEL_CLASS_TYPE__QUALIFICATION_TEST_SPECIFICATION_ID:
				return qualificationTestSpecificationID != null && !qualificationTestSpecificationID.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PersonnelClassTypeImpl
