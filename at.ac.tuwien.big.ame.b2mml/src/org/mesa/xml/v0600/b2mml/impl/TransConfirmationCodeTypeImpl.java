/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.TransConfirmationCodeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trans Confirmation Code Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TransConfirmationCodeTypeImpl extends CodeTypeImpl implements TransConfirmationCodeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransConfirmationCodeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTransConfirmationCodeType();
	}

} //TransConfirmationCodeTypeImpl
