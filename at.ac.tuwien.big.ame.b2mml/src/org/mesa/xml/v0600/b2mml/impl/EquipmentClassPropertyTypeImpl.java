/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.EquipmentCapabilityTestSpecificationIDType;
import org.mesa.xml.v0600.b2mml.EquipmentClassPropertyType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.ValueType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Class Property Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentClassPropertyTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentClassPropertyTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentClassPropertyTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentClassPropertyTypeImpl#getEquipmentClassProperty <em>Equipment Class Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentClassPropertyTypeImpl#getEquipmentCapabilityTestSpecificationID <em>Equipment Capability Test Specification ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EquipmentClassPropertyTypeImpl extends MinimalEObjectImpl.Container implements EquipmentClassPropertyType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected EList<ValueType> value;

	/**
	 * The cached value of the '{@link #getEquipmentClassProperty() <em>Equipment Class Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentClassProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentClassPropertyType> equipmentClassProperty;

	/**
	 * The cached value of the '{@link #getEquipmentCapabilityTestSpecificationID() <em>Equipment Capability Test Specification ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentCapabilityTestSpecificationID()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentCapabilityTestSpecificationIDType> equipmentCapabilityTestSpecificationID;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentClassPropertyTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getEquipmentClassPropertyType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueType> getValue() {
		if (value == null) {
			value = new EObjectContainmentEList<ValueType>(ValueType.class, this, B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__VALUE);
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentClassPropertyType> getEquipmentClassProperty() {
		if (equipmentClassProperty == null) {
			equipmentClassProperty = new EObjectContainmentEList<EquipmentClassPropertyType>(EquipmentClassPropertyType.class, this, B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__EQUIPMENT_CLASS_PROPERTY);
		}
		return equipmentClassProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentCapabilityTestSpecificationIDType> getEquipmentCapabilityTestSpecificationID() {
		if (equipmentCapabilityTestSpecificationID == null) {
			equipmentCapabilityTestSpecificationID = new EObjectContainmentEList<EquipmentCapabilityTestSpecificationIDType>(EquipmentCapabilityTestSpecificationIDType.class, this, B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID);
		}
		return equipmentCapabilityTestSpecificationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__VALUE:
				return ((InternalEList<?>)getValue()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__EQUIPMENT_CLASS_PROPERTY:
				return ((InternalEList<?>)getEquipmentClassProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID:
				return ((InternalEList<?>)getEquipmentCapabilityTestSpecificationID()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__ID:
				return getID();
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__VALUE:
				return getValue();
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__EQUIPMENT_CLASS_PROPERTY:
				return getEquipmentClassProperty();
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID:
				return getEquipmentCapabilityTestSpecificationID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__VALUE:
				getValue().clear();
				getValue().addAll((Collection<? extends ValueType>)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__EQUIPMENT_CLASS_PROPERTY:
				getEquipmentClassProperty().clear();
				getEquipmentClassProperty().addAll((Collection<? extends EquipmentClassPropertyType>)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID:
				getEquipmentCapabilityTestSpecificationID().clear();
				getEquipmentCapabilityTestSpecificationID().addAll((Collection<? extends EquipmentCapabilityTestSpecificationIDType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__VALUE:
				getValue().clear();
				return;
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__EQUIPMENT_CLASS_PROPERTY:
				getEquipmentClassProperty().clear();
				return;
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID:
				getEquipmentCapabilityTestSpecificationID().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__ID:
				return iD != null;
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__VALUE:
				return value != null && !value.isEmpty();
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__EQUIPMENT_CLASS_PROPERTY:
				return equipmentClassProperty != null && !equipmentClassProperty.isEmpty();
			case B2MMLPackage.EQUIPMENT_CLASS_PROPERTY_TYPE__EQUIPMENT_CAPABILITY_TEST_SPECIFICATION_ID:
				return equipmentCapabilityTestSpecificationID != null && !equipmentCapabilityTestSpecificationID.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EquipmentClassPropertyTypeImpl
