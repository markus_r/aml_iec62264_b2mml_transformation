/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.ReasonType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reason Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ReasonTypeImpl extends CodeTypeImpl implements ReasonType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReasonTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getReasonType();
	}

} //ReasonTypeImpl
