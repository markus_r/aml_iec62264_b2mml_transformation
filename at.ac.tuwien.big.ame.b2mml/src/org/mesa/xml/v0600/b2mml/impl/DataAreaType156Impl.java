/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType156;
import org.mesa.xml.v0600.b2mml.PhysicalAssetCapabilityTestSpecificationType;
import org.mesa.xml.v0600.b2mml.TransCancelType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type156</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType156Impl#getCancel <em>Cancel</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType156Impl#getPhysicalAssetCapabilityTestSpec <em>Physical Asset Capability Test Spec</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType156Impl extends MinimalEObjectImpl.Container implements DataAreaType156 {
	/**
	 * The cached value of the '{@link #getCancel() <em>Cancel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCancel()
	 * @generated
	 * @ordered
	 */
	protected TransCancelType cancel;

	/**
	 * The cached value of the '{@link #getPhysicalAssetCapabilityTestSpec() <em>Physical Asset Capability Test Spec</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetCapabilityTestSpec()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetCapabilityTestSpecificationType> physicalAssetCapabilityTestSpec;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType156Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType156();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransCancelType getCancel() {
		return cancel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCancel(TransCancelType newCancel, NotificationChain msgs) {
		TransCancelType oldCancel = cancel;
		cancel = newCancel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE156__CANCEL, oldCancel, newCancel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCancel(TransCancelType newCancel) {
		if (newCancel != cancel) {
			NotificationChain msgs = null;
			if (cancel != null)
				msgs = ((InternalEObject)cancel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE156__CANCEL, null, msgs);
			if (newCancel != null)
				msgs = ((InternalEObject)newCancel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE156__CANCEL, null, msgs);
			msgs = basicSetCancel(newCancel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE156__CANCEL, newCancel, newCancel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetCapabilityTestSpecificationType> getPhysicalAssetCapabilityTestSpec() {
		if (physicalAssetCapabilityTestSpec == null) {
			physicalAssetCapabilityTestSpec = new EObjectContainmentEList<PhysicalAssetCapabilityTestSpecificationType>(PhysicalAssetCapabilityTestSpecificationType.class, this, B2MMLPackage.DATA_AREA_TYPE156__PHYSICAL_ASSET_CAPABILITY_TEST_SPEC);
		}
		return physicalAssetCapabilityTestSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE156__CANCEL:
				return basicSetCancel(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE156__PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return ((InternalEList<?>)getPhysicalAssetCapabilityTestSpec()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE156__CANCEL:
				return getCancel();
			case B2MMLPackage.DATA_AREA_TYPE156__PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return getPhysicalAssetCapabilityTestSpec();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE156__CANCEL:
				setCancel((TransCancelType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE156__PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				getPhysicalAssetCapabilityTestSpec().clear();
				getPhysicalAssetCapabilityTestSpec().addAll((Collection<? extends PhysicalAssetCapabilityTestSpecificationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE156__CANCEL:
				setCancel((TransCancelType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE156__PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				getPhysicalAssetCapabilityTestSpec().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE156__CANCEL:
				return cancel != null;
			case B2MMLPackage.DATA_AREA_TYPE156__PHYSICAL_ASSET_CAPABILITY_TEST_SPEC:
				return physicalAssetCapabilityTestSpec != null && !physicalAssetCapabilityTestSpec.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType156Impl
