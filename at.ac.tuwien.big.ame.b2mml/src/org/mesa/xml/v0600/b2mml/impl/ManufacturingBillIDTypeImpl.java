/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.ManufacturingBillIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Manufacturing Bill ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ManufacturingBillIDTypeImpl extends IdentifierTypeImpl implements ManufacturingBillIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ManufacturingBillIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getManufacturingBillIDType();
	}

} //ManufacturingBillIDTypeImpl
