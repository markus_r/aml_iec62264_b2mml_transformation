/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.EquipmentClassIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Class ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EquipmentClassIDTypeImpl extends IdentifierTypeImpl implements EquipmentClassIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentClassIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getEquipmentClassIDType();
	}

} //EquipmentClassIDTypeImpl
