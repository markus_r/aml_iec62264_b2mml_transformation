/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.ProductSegmentIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Product Segment ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ProductSegmentIDTypeImpl extends IdentifierTypeImpl implements ProductSegmentIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProductSegmentIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getProductSegmentIDType();
	}

} //ProductSegmentIDTypeImpl
