/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.TransActionCriteriaType;
import org.mesa.xml.v0600.b2mml.TransChangeStatusType;
import org.mesa.xml.v0600.b2mml.TransExpressionType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trans Action Criteria Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransActionCriteriaTypeImpl#getActionExpression <em>Action Expression</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TransActionCriteriaTypeImpl#getChangeStatus <em>Change Status</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransActionCriteriaTypeImpl extends MinimalEObjectImpl.Container implements TransActionCriteriaType {
	/**
	 * The cached value of the '{@link #getActionExpression() <em>Action Expression</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionExpression()
	 * @generated
	 * @ordered
	 */
	protected EList<TransExpressionType> actionExpression;

	/**
	 * The cached value of the '{@link #getChangeStatus() <em>Change Status</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChangeStatus()
	 * @generated
	 * @ordered
	 */
	protected TransChangeStatusType changeStatus;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransActionCriteriaTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTransActionCriteriaType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TransExpressionType> getActionExpression() {
		if (actionExpression == null) {
			actionExpression = new EObjectContainmentEList<TransExpressionType>(TransExpressionType.class, this, B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__ACTION_EXPRESSION);
		}
		return actionExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransChangeStatusType getChangeStatus() {
		return changeStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChangeStatus(TransChangeStatusType newChangeStatus, NotificationChain msgs) {
		TransChangeStatusType oldChangeStatus = changeStatus;
		changeStatus = newChangeStatus;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__CHANGE_STATUS, oldChangeStatus, newChangeStatus);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeStatus(TransChangeStatusType newChangeStatus) {
		if (newChangeStatus != changeStatus) {
			NotificationChain msgs = null;
			if (changeStatus != null)
				msgs = ((InternalEObject)changeStatus).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__CHANGE_STATUS, null, msgs);
			if (newChangeStatus != null)
				msgs = ((InternalEObject)newChangeStatus).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__CHANGE_STATUS, null, msgs);
			msgs = basicSetChangeStatus(newChangeStatus, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__CHANGE_STATUS, newChangeStatus, newChangeStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__ACTION_EXPRESSION:
				return ((InternalEList<?>)getActionExpression()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__CHANGE_STATUS:
				return basicSetChangeStatus(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__ACTION_EXPRESSION:
				return getActionExpression();
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__CHANGE_STATUS:
				return getChangeStatus();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__ACTION_EXPRESSION:
				getActionExpression().clear();
				getActionExpression().addAll((Collection<? extends TransExpressionType>)newValue);
				return;
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__CHANGE_STATUS:
				setChangeStatus((TransChangeStatusType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__ACTION_EXPRESSION:
				getActionExpression().clear();
				return;
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__CHANGE_STATUS:
				setChangeStatus((TransChangeStatusType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__ACTION_EXPRESSION:
				return actionExpression != null && !actionExpression.isEmpty();
			case B2MMLPackage.TRANS_ACTION_CRITERIA_TYPE__CHANGE_STATUS:
				return changeStatus != null;
		}
		return super.eIsSet(featureID);
	}

} //TransActionCriteriaTypeImpl
