/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.RequestState1Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Request State1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequestState1TypeImpl extends CodeTypeImpl implements RequestState1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequestState1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getRequestState1Type();
	}

} //RequestState1TypeImpl
