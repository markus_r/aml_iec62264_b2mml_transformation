/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType184;
import org.mesa.xml.v0600.b2mml.OperationsPerformanceType;
import org.mesa.xml.v0600.b2mml.TransAcknowledgeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type184</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType184Impl#getAcknowledge <em>Acknowledge</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType184Impl#getOperationsPerformance <em>Operations Performance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType184Impl extends MinimalEObjectImpl.Container implements DataAreaType184 {
	/**
	 * The cached value of the '{@link #getAcknowledge() <em>Acknowledge</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcknowledge()
	 * @generated
	 * @ordered
	 */
	protected TransAcknowledgeType acknowledge;

	/**
	 * The cached value of the '{@link #getOperationsPerformance() <em>Operations Performance</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsPerformance()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsPerformanceType> operationsPerformance;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType184Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType184();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransAcknowledgeType getAcknowledge() {
		return acknowledge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcknowledge(TransAcknowledgeType newAcknowledge, NotificationChain msgs) {
		TransAcknowledgeType oldAcknowledge = acknowledge;
		acknowledge = newAcknowledge;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE184__ACKNOWLEDGE, oldAcknowledge, newAcknowledge);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcknowledge(TransAcknowledgeType newAcknowledge) {
		if (newAcknowledge != acknowledge) {
			NotificationChain msgs = null;
			if (acknowledge != null)
				msgs = ((InternalEObject)acknowledge).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE184__ACKNOWLEDGE, null, msgs);
			if (newAcknowledge != null)
				msgs = ((InternalEObject)newAcknowledge).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE184__ACKNOWLEDGE, null, msgs);
			msgs = basicSetAcknowledge(newAcknowledge, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE184__ACKNOWLEDGE, newAcknowledge, newAcknowledge));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsPerformanceType> getOperationsPerformance() {
		if (operationsPerformance == null) {
			operationsPerformance = new EObjectContainmentEList<OperationsPerformanceType>(OperationsPerformanceType.class, this, B2MMLPackage.DATA_AREA_TYPE184__OPERATIONS_PERFORMANCE);
		}
		return operationsPerformance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE184__ACKNOWLEDGE:
				return basicSetAcknowledge(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE184__OPERATIONS_PERFORMANCE:
				return ((InternalEList<?>)getOperationsPerformance()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE184__ACKNOWLEDGE:
				return getAcknowledge();
			case B2MMLPackage.DATA_AREA_TYPE184__OPERATIONS_PERFORMANCE:
				return getOperationsPerformance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE184__ACKNOWLEDGE:
				setAcknowledge((TransAcknowledgeType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE184__OPERATIONS_PERFORMANCE:
				getOperationsPerformance().clear();
				getOperationsPerformance().addAll((Collection<? extends OperationsPerformanceType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE184__ACKNOWLEDGE:
				setAcknowledge((TransAcknowledgeType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE184__OPERATIONS_PERFORMANCE:
				getOperationsPerformance().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE184__ACKNOWLEDGE:
				return acknowledge != null;
			case B2MMLPackage.DATA_AREA_TYPE184__OPERATIONS_PERFORMANCE:
				return operationsPerformance != null && !operationsPerformance.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType184Impl
