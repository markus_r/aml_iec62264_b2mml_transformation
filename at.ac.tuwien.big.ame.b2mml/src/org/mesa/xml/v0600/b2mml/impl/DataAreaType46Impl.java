/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType46;
import org.mesa.xml.v0600.b2mml.PhysicalAssetInformationType;
import org.mesa.xml.v0600.b2mml.TransShowType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type46</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType46Impl#getShow <em>Show</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType46Impl#getPhysicalAssetInformation <em>Physical Asset Information</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType46Impl extends MinimalEObjectImpl.Container implements DataAreaType46 {
	/**
	 * The cached value of the '{@link #getShow() <em>Show</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShow()
	 * @generated
	 * @ordered
	 */
	protected TransShowType show;

	/**
	 * The cached value of the '{@link #getPhysicalAssetInformation() <em>Physical Asset Information</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetInformation()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetInformationType> physicalAssetInformation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType46Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType46();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransShowType getShow() {
		return show;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShow(TransShowType newShow, NotificationChain msgs) {
		TransShowType oldShow = show;
		show = newShow;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE46__SHOW, oldShow, newShow);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShow(TransShowType newShow) {
		if (newShow != show) {
			NotificationChain msgs = null;
			if (show != null)
				msgs = ((InternalEObject)show).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE46__SHOW, null, msgs);
			if (newShow != null)
				msgs = ((InternalEObject)newShow).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE46__SHOW, null, msgs);
			msgs = basicSetShow(newShow, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE46__SHOW, newShow, newShow));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetInformationType> getPhysicalAssetInformation() {
		if (physicalAssetInformation == null) {
			physicalAssetInformation = new EObjectContainmentEList<PhysicalAssetInformationType>(PhysicalAssetInformationType.class, this, B2MMLPackage.DATA_AREA_TYPE46__PHYSICAL_ASSET_INFORMATION);
		}
		return physicalAssetInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE46__SHOW:
				return basicSetShow(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE46__PHYSICAL_ASSET_INFORMATION:
				return ((InternalEList<?>)getPhysicalAssetInformation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE46__SHOW:
				return getShow();
			case B2MMLPackage.DATA_AREA_TYPE46__PHYSICAL_ASSET_INFORMATION:
				return getPhysicalAssetInformation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE46__SHOW:
				setShow((TransShowType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE46__PHYSICAL_ASSET_INFORMATION:
				getPhysicalAssetInformation().clear();
				getPhysicalAssetInformation().addAll((Collection<? extends PhysicalAssetInformationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE46__SHOW:
				setShow((TransShowType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE46__PHYSICAL_ASSET_INFORMATION:
				getPhysicalAssetInformation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE46__SHOW:
				return show != null;
			case B2MMLPackage.DATA_AREA_TYPE46__PHYSICAL_ASSET_INFORMATION:
				return physicalAssetInformation != null && !physicalAssetInformation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType46Impl
