/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.MaterialActualIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Actual ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MaterialActualIDTypeImpl extends IdentifierTypeImpl implements MaterialActualIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialActualIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getMaterialActualIDType();
	}

} //MaterialActualIDTypeImpl
