/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.EquipmentClassIDType;
import org.mesa.xml.v0600.b2mml.EquipmentIDType;
import org.mesa.xml.v0600.b2mml.EquipmentUseType;
import org.mesa.xml.v0600.b2mml.OpEquipmentSpecificationPropertyType;
import org.mesa.xml.v0600.b2mml.OpEquipmentSpecificationType;
import org.mesa.xml.v0600.b2mml.QuantityValueType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Equipment Specification Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpEquipmentSpecificationTypeImpl#getEquipmentClassID <em>Equipment Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpEquipmentSpecificationTypeImpl#getEquipmentID <em>Equipment ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpEquipmentSpecificationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpEquipmentSpecificationTypeImpl#getEquipmentUse <em>Equipment Use</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpEquipmentSpecificationTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpEquipmentSpecificationTypeImpl#getEquipmentSpecificationProperty <em>Equipment Specification Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpEquipmentSpecificationTypeImpl extends MinimalEObjectImpl.Container implements OpEquipmentSpecificationType {
	/**
	 * The cached value of the '{@link #getEquipmentClassID() <em>Equipment Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentClassIDType> equipmentClassID;

	/**
	 * The cached value of the '{@link #getEquipmentID() <em>Equipment ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentID()
	 * @generated
	 * @ordered
	 */
	protected EList<EquipmentIDType> equipmentID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getEquipmentUse() <em>Equipment Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentUse()
	 * @generated
	 * @ordered
	 */
	protected EquipmentUseType equipmentUse;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getEquipmentSpecificationProperty() <em>Equipment Specification Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentSpecificationProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<OpEquipmentSpecificationPropertyType> equipmentSpecificationProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpEquipmentSpecificationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpEquipmentSpecificationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentClassIDType> getEquipmentClassID() {
		if (equipmentClassID == null) {
			equipmentClassID = new EObjectContainmentEList<EquipmentClassIDType>(EquipmentClassIDType.class, this, B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID);
		}
		return equipmentClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EquipmentIDType> getEquipmentID() {
		if (equipmentID == null) {
			equipmentID = new EObjectContainmentEList<EquipmentIDType>(EquipmentIDType.class, this, B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_ID);
		}
		return equipmentID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentUseType getEquipmentUse() {
		return equipmentUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentUse(EquipmentUseType newEquipmentUse, NotificationChain msgs) {
		EquipmentUseType oldEquipmentUse = equipmentUse;
		equipmentUse = newEquipmentUse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_USE, oldEquipmentUse, newEquipmentUse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentUse(EquipmentUseType newEquipmentUse) {
		if (newEquipmentUse != equipmentUse) {
			NotificationChain msgs = null;
			if (equipmentUse != null)
				msgs = ((InternalEObject)equipmentUse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_USE, null, msgs);
			if (newEquipmentUse != null)
				msgs = ((InternalEObject)newEquipmentUse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_USE, null, msgs);
			msgs = basicSetEquipmentUse(newEquipmentUse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_USE, newEquipmentUse, newEquipmentUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpEquipmentSpecificationPropertyType> getEquipmentSpecificationProperty() {
		if (equipmentSpecificationProperty == null) {
			equipmentSpecificationProperty = new EObjectContainmentEList<OpEquipmentSpecificationPropertyType>(OpEquipmentSpecificationPropertyType.class, this, B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_SPECIFICATION_PROPERTY);
		}
		return equipmentSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID:
				return ((InternalEList<?>)getEquipmentClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_ID:
				return ((InternalEList<?>)getEquipmentID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_USE:
				return basicSetEquipmentUse(null, msgs);
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_SPECIFICATION_PROPERTY:
				return ((InternalEList<?>)getEquipmentSpecificationProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID:
				return getEquipmentClassID();
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_ID:
				return getEquipmentID();
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_USE:
				return getEquipmentUse();
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_SPECIFICATION_PROPERTY:
				return getEquipmentSpecificationProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID:
				getEquipmentClassID().clear();
				getEquipmentClassID().addAll((Collection<? extends EquipmentClassIDType>)newValue);
				return;
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_ID:
				getEquipmentID().clear();
				getEquipmentID().addAll((Collection<? extends EquipmentIDType>)newValue);
				return;
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_USE:
				setEquipmentUse((EquipmentUseType)newValue);
				return;
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_SPECIFICATION_PROPERTY:
				getEquipmentSpecificationProperty().clear();
				getEquipmentSpecificationProperty().addAll((Collection<? extends OpEquipmentSpecificationPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID:
				getEquipmentClassID().clear();
				return;
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_ID:
				getEquipmentID().clear();
				return;
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_USE:
				setEquipmentUse((EquipmentUseType)null);
				return;
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_SPECIFICATION_PROPERTY:
				getEquipmentSpecificationProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_CLASS_ID:
				return equipmentClassID != null && !equipmentClassID.isEmpty();
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_ID:
				return equipmentID != null && !equipmentID.isEmpty();
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_USE:
				return equipmentUse != null;
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.OP_EQUIPMENT_SPECIFICATION_TYPE__EQUIPMENT_SPECIFICATION_PROPERTY:
				return equipmentSpecificationProperty != null && !equipmentSpecificationProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OpEquipmentSpecificationTypeImpl
