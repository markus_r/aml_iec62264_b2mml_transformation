/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.PhysicalAssetUseType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Use Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PhysicalAssetUseTypeImpl extends CodeTypeImpl implements PhysicalAssetUseType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetUseTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPhysicalAssetUseType();
	}

} //PhysicalAssetUseTypeImpl
