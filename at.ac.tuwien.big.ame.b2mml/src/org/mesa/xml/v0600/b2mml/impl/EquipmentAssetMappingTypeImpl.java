/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DateTimeType;
import org.mesa.xml.v0600.b2mml.EquipmentAssetMappingType;
import org.mesa.xml.v0600.b2mml.EquipmentIDType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Asset Mapping Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentAssetMappingTypeImpl#getEquipmentID <em>Equipment ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentAssetMappingTypeImpl#getPhysicalAssetID <em>Physical Asset ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentAssetMappingTypeImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.EquipmentAssetMappingTypeImpl#getEndTime <em>End Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EquipmentAssetMappingTypeImpl extends MinimalEObjectImpl.Container implements EquipmentAssetMappingType {
	/**
	 * The cached value of the '{@link #getEquipmentID() <em>Equipment ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentID()
	 * @generated
	 * @ordered
	 */
	protected EquipmentIDType equipmentID;

	/**
	 * The cached value of the '{@link #getPhysicalAssetID() <em>Physical Asset ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetID()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetIDType physicalAssetID;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected DateTimeType startTime;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected DateTimeType endTime;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentAssetMappingTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getEquipmentAssetMappingType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquipmentIDType getEquipmentID() {
		return equipmentID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquipmentID(EquipmentIDType newEquipmentID, NotificationChain msgs) {
		EquipmentIDType oldEquipmentID = equipmentID;
		equipmentID = newEquipmentID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__EQUIPMENT_ID, oldEquipmentID, newEquipmentID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquipmentID(EquipmentIDType newEquipmentID) {
		if (newEquipmentID != equipmentID) {
			NotificationChain msgs = null;
			if (equipmentID != null)
				msgs = ((InternalEObject)equipmentID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__EQUIPMENT_ID, null, msgs);
			if (newEquipmentID != null)
				msgs = ((InternalEObject)newEquipmentID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__EQUIPMENT_ID, null, msgs);
			msgs = basicSetEquipmentID(newEquipmentID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__EQUIPMENT_ID, newEquipmentID, newEquipmentID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetIDType getPhysicalAssetID() {
		return physicalAssetID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPhysicalAssetID(PhysicalAssetIDType newPhysicalAssetID, NotificationChain msgs) {
		PhysicalAssetIDType oldPhysicalAssetID = physicalAssetID;
		physicalAssetID = newPhysicalAssetID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__PHYSICAL_ASSET_ID, oldPhysicalAssetID, newPhysicalAssetID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetID(PhysicalAssetIDType newPhysicalAssetID) {
		if (newPhysicalAssetID != physicalAssetID) {
			NotificationChain msgs = null;
			if (physicalAssetID != null)
				msgs = ((InternalEObject)physicalAssetID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__PHYSICAL_ASSET_ID, null, msgs);
			if (newPhysicalAssetID != null)
				msgs = ((InternalEObject)newPhysicalAssetID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__PHYSICAL_ASSET_ID, null, msgs);
			msgs = basicSetPhysicalAssetID(newPhysicalAssetID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__PHYSICAL_ASSET_ID, newPhysicalAssetID, newPhysicalAssetID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTimeType getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStartTime(DateTimeType newStartTime, NotificationChain msgs) {
		DateTimeType oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__START_TIME, oldStartTime, newStartTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(DateTimeType newStartTime) {
		if (newStartTime != startTime) {
			NotificationChain msgs = null;
			if (startTime != null)
				msgs = ((InternalEObject)startTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__START_TIME, null, msgs);
			if (newStartTime != null)
				msgs = ((InternalEObject)newStartTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__START_TIME, null, msgs);
			msgs = basicSetStartTime(newStartTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__START_TIME, newStartTime, newStartTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTimeType getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEndTime(DateTimeType newEndTime, NotificationChain msgs) {
		DateTimeType oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__END_TIME, oldEndTime, newEndTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(DateTimeType newEndTime) {
		if (newEndTime != endTime) {
			NotificationChain msgs = null;
			if (endTime != null)
				msgs = ((InternalEObject)endTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__END_TIME, null, msgs);
			if (newEndTime != null)
				msgs = ((InternalEObject)newEndTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__END_TIME, null, msgs);
			msgs = basicSetEndTime(newEndTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__END_TIME, newEndTime, newEndTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__EQUIPMENT_ID:
				return basicSetEquipmentID(null, msgs);
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__PHYSICAL_ASSET_ID:
				return basicSetPhysicalAssetID(null, msgs);
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__START_TIME:
				return basicSetStartTime(null, msgs);
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__END_TIME:
				return basicSetEndTime(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__EQUIPMENT_ID:
				return getEquipmentID();
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__PHYSICAL_ASSET_ID:
				return getPhysicalAssetID();
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__START_TIME:
				return getStartTime();
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__END_TIME:
				return getEndTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__EQUIPMENT_ID:
				setEquipmentID((EquipmentIDType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__PHYSICAL_ASSET_ID:
				setPhysicalAssetID((PhysicalAssetIDType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__START_TIME:
				setStartTime((DateTimeType)newValue);
				return;
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__END_TIME:
				setEndTime((DateTimeType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__EQUIPMENT_ID:
				setEquipmentID((EquipmentIDType)null);
				return;
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__PHYSICAL_ASSET_ID:
				setPhysicalAssetID((PhysicalAssetIDType)null);
				return;
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__START_TIME:
				setStartTime((DateTimeType)null);
				return;
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__END_TIME:
				setEndTime((DateTimeType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__EQUIPMENT_ID:
				return equipmentID != null;
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__PHYSICAL_ASSET_ID:
				return physicalAssetID != null;
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__START_TIME:
				return startTime != null;
			case B2MMLPackage.EQUIPMENT_ASSET_MAPPING_TYPE__END_TIME:
				return endTime != null;
		}
		return super.eIsSet(featureID);
	}

} //EquipmentAssetMappingTypeImpl
