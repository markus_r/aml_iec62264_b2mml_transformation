/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.AssemblyRelationshipType;
import org.mesa.xml.v0600.b2mml.AssemblyTypeType;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.MaterialClassIDType;
import org.mesa.xml.v0600.b2mml.MaterialDefinitionIDType;
import org.mesa.xml.v0600.b2mml.MaterialLotIDType;
import org.mesa.xml.v0600.b2mml.MaterialSubLotIDType;
import org.mesa.xml.v0600.b2mml.MaterialUseType;
import org.mesa.xml.v0600.b2mml.OpMaterialActualPropertyType;
import org.mesa.xml.v0600.b2mml.OpMaterialActualType;
import org.mesa.xml.v0600.b2mml.QuantityValueType;
import org.mesa.xml.v0600.b2mml.RequiredByRequestedSegmentResponseType;
import org.mesa.xml.v0600.b2mml.StorageLocationType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Material Actual Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getMaterialClassID <em>Material Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getMaterialDefinitionID <em>Material Definition ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getMaterialLotID <em>Material Lot ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getMaterialSubLotID <em>Material Sub Lot ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getMaterialUse <em>Material Use</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getStorageLocation <em>Storage Location</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getAssemblyActual <em>Assembly Actual</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getAssemblyType <em>Assembly Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getAssemblyRelationship <em>Assembly Relationship</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getMaterialActualProperty <em>Material Actual Property</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpMaterialActualTypeImpl#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpMaterialActualTypeImpl extends MinimalEObjectImpl.Container implements OpMaterialActualType {
	/**
	 * The cached value of the '{@link #getMaterialClassID() <em>Material Class ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialClassID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialClassIDType> materialClassID;

	/**
	 * The cached value of the '{@link #getMaterialDefinitionID() <em>Material Definition ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialDefinitionID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialDefinitionIDType> materialDefinitionID;

	/**
	 * The cached value of the '{@link #getMaterialLotID() <em>Material Lot ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialLotID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialLotIDType> materialLotID;

	/**
	 * The cached value of the '{@link #getMaterialSubLotID() <em>Material Sub Lot ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialSubLotID()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialSubLotIDType> materialSubLotID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getMaterialUse() <em>Material Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialUse()
	 * @generated
	 * @ordered
	 */
	protected MaterialUseType materialUse;

	/**
	 * The cached value of the '{@link #getStorageLocation() <em>Storage Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageLocation()
	 * @generated
	 * @ordered
	 */
	protected StorageLocationType storageLocation;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getAssemblyActual() <em>Assembly Actual</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyActual()
	 * @generated
	 * @ordered
	 */
	protected EList<OpMaterialActualType> assemblyActual;

	/**
	 * The cached value of the '{@link #getAssemblyType() <em>Assembly Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyType()
	 * @generated
	 * @ordered
	 */
	protected AssemblyTypeType assemblyType;

	/**
	 * The cached value of the '{@link #getAssemblyRelationship() <em>Assembly Relationship</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssemblyRelationship()
	 * @generated
	 * @ordered
	 */
	protected AssemblyRelationshipType assemblyRelationship;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getMaterialActualProperty() <em>Material Actual Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialActualProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<OpMaterialActualPropertyType> materialActualProperty;

	/**
	 * The cached value of the '{@link #getRequiredByRequestedSegmentResponse() <em>Required By Requested Segment Response</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredByRequestedSegmentResponse()
	 * @generated
	 * @ordered
	 */
	protected RequiredByRequestedSegmentResponseType requiredByRequestedSegmentResponse;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpMaterialActualTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpMaterialActualType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialClassIDType> getMaterialClassID() {
		if (materialClassID == null) {
			materialClassID = new EObjectContainmentEList<MaterialClassIDType>(MaterialClassIDType.class, this, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_CLASS_ID);
		}
		return materialClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialDefinitionIDType> getMaterialDefinitionID() {
		if (materialDefinitionID == null) {
			materialDefinitionID = new EObjectContainmentEList<MaterialDefinitionIDType>(MaterialDefinitionIDType.class, this, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_DEFINITION_ID);
		}
		return materialDefinitionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialLotIDType> getMaterialLotID() {
		if (materialLotID == null) {
			materialLotID = new EObjectContainmentEList<MaterialLotIDType>(MaterialLotIDType.class, this, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_LOT_ID);
		}
		return materialLotID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaterialSubLotIDType> getMaterialSubLotID() {
		if (materialSubLotID == null) {
			materialSubLotID = new EObjectContainmentEList<MaterialSubLotIDType>(MaterialSubLotIDType.class, this, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_SUB_LOT_ID);
		}
		return materialSubLotID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialUseType getMaterialUse() {
		return materialUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaterialUse(MaterialUseType newMaterialUse, NotificationChain msgs) {
		MaterialUseType oldMaterialUse = materialUse;
		materialUse = newMaterialUse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_USE, oldMaterialUse, newMaterialUse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaterialUse(MaterialUseType newMaterialUse) {
		if (newMaterialUse != materialUse) {
			NotificationChain msgs = null;
			if (materialUse != null)
				msgs = ((InternalEObject)materialUse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_USE, null, msgs);
			if (newMaterialUse != null)
				msgs = ((InternalEObject)newMaterialUse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_USE, null, msgs);
			msgs = basicSetMaterialUse(newMaterialUse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_USE, newMaterialUse, newMaterialUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageLocationType getStorageLocation() {
		return storageLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStorageLocation(StorageLocationType newStorageLocation, NotificationChain msgs) {
		StorageLocationType oldStorageLocation = storageLocation;
		storageLocation = newStorageLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__STORAGE_LOCATION, oldStorageLocation, newStorageLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageLocation(StorageLocationType newStorageLocation) {
		if (newStorageLocation != storageLocation) {
			NotificationChain msgs = null;
			if (storageLocation != null)
				msgs = ((InternalEObject)storageLocation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__STORAGE_LOCATION, null, msgs);
			if (newStorageLocation != null)
				msgs = ((InternalEObject)newStorageLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__STORAGE_LOCATION, null, msgs);
			msgs = basicSetStorageLocation(newStorageLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__STORAGE_LOCATION, newStorageLocation, newStorageLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpMaterialActualType> getAssemblyActual() {
		if (assemblyActual == null) {
			assemblyActual = new EObjectContainmentEList<OpMaterialActualType>(OpMaterialActualType.class, this, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_ACTUAL);
		}
		return assemblyActual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyTypeType getAssemblyType() {
		return assemblyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyType(AssemblyTypeType newAssemblyType, NotificationChain msgs) {
		AssemblyTypeType oldAssemblyType = assemblyType;
		assemblyType = newAssemblyType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_TYPE, oldAssemblyType, newAssemblyType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyType(AssemblyTypeType newAssemblyType) {
		if (newAssemblyType != assemblyType) {
			NotificationChain msgs = null;
			if (assemblyType != null)
				msgs = ((InternalEObject)assemblyType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_TYPE, null, msgs);
			if (newAssemblyType != null)
				msgs = ((InternalEObject)newAssemblyType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_TYPE, null, msgs);
			msgs = basicSetAssemblyType(newAssemblyType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_TYPE, newAssemblyType, newAssemblyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssemblyRelationshipType getAssemblyRelationship() {
		return assemblyRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship, NotificationChain msgs) {
		AssemblyRelationshipType oldAssemblyRelationship = assemblyRelationship;
		assemblyRelationship = newAssemblyRelationship;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_RELATIONSHIP, oldAssemblyRelationship, newAssemblyRelationship);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssemblyRelationship(AssemblyRelationshipType newAssemblyRelationship) {
		if (newAssemblyRelationship != assemblyRelationship) {
			NotificationChain msgs = null;
			if (assemblyRelationship != null)
				msgs = ((InternalEObject)assemblyRelationship).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			if (newAssemblyRelationship != null)
				msgs = ((InternalEObject)newAssemblyRelationship).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_RELATIONSHIP, null, msgs);
			msgs = basicSetAssemblyRelationship(newAssemblyRelationship, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_RELATIONSHIP, newAssemblyRelationship, newAssemblyRelationship));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpMaterialActualPropertyType> getMaterialActualProperty() {
		if (materialActualProperty == null) {
			materialActualProperty = new EObjectContainmentEList<OpMaterialActualPropertyType>(OpMaterialActualPropertyType.class, this, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_ACTUAL_PROPERTY);
		}
		return materialActualProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredByRequestedSegmentResponseType getRequiredByRequestedSegmentResponse() {
		return requiredByRequestedSegmentResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType newRequiredByRequestedSegmentResponse, NotificationChain msgs) {
		RequiredByRequestedSegmentResponseType oldRequiredByRequestedSegmentResponse = requiredByRequestedSegmentResponse;
		requiredByRequestedSegmentResponse = newRequiredByRequestedSegmentResponse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, oldRequiredByRequestedSegmentResponse, newRequiredByRequestedSegmentResponse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType newRequiredByRequestedSegmentResponse) {
		if (newRequiredByRequestedSegmentResponse != requiredByRequestedSegmentResponse) {
			NotificationChain msgs = null;
			if (requiredByRequestedSegmentResponse != null)
				msgs = ((InternalEObject)requiredByRequestedSegmentResponse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, null, msgs);
			if (newRequiredByRequestedSegmentResponse != null)
				msgs = ((InternalEObject)newRequiredByRequestedSegmentResponse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, null, msgs);
			msgs = basicSetRequiredByRequestedSegmentResponse(newRequiredByRequestedSegmentResponse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, newRequiredByRequestedSegmentResponse, newRequiredByRequestedSegmentResponse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_CLASS_ID:
				return ((InternalEList<?>)getMaterialClassID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_DEFINITION_ID:
				return ((InternalEList<?>)getMaterialDefinitionID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_LOT_ID:
				return ((InternalEList<?>)getMaterialLotID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_SUB_LOT_ID:
				return ((InternalEList<?>)getMaterialSubLotID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_USE:
				return basicSetMaterialUse(null, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__STORAGE_LOCATION:
				return basicSetStorageLocation(null, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_ACTUAL:
				return ((InternalEList<?>)getAssemblyActual()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_TYPE:
				return basicSetAssemblyType(null, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_RELATIONSHIP:
				return basicSetAssemblyRelationship(null, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_ACTUAL_PROPERTY:
				return ((InternalEList<?>)getMaterialActualProperty()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return basicSetRequiredByRequestedSegmentResponse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_CLASS_ID:
				return getMaterialClassID();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_DEFINITION_ID:
				return getMaterialDefinitionID();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_LOT_ID:
				return getMaterialLotID();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_SUB_LOT_ID:
				return getMaterialSubLotID();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_USE:
				return getMaterialUse();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__STORAGE_LOCATION:
				return getStorageLocation();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_ACTUAL:
				return getAssemblyActual();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_TYPE:
				return getAssemblyType();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_RELATIONSHIP:
				return getAssemblyRelationship();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_ACTUAL_PROPERTY:
				return getMaterialActualProperty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return getRequiredByRequestedSegmentResponse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_CLASS_ID:
				getMaterialClassID().clear();
				getMaterialClassID().addAll((Collection<? extends MaterialClassIDType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_DEFINITION_ID:
				getMaterialDefinitionID().clear();
				getMaterialDefinitionID().addAll((Collection<? extends MaterialDefinitionIDType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_LOT_ID:
				getMaterialLotID().clear();
				getMaterialLotID().addAll((Collection<? extends MaterialLotIDType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_SUB_LOT_ID:
				getMaterialSubLotID().clear();
				getMaterialSubLotID().addAll((Collection<? extends MaterialSubLotIDType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_USE:
				setMaterialUse((MaterialUseType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__STORAGE_LOCATION:
				setStorageLocation((StorageLocationType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_ACTUAL:
				getAssemblyActual().clear();
				getAssemblyActual().addAll((Collection<? extends OpMaterialActualType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_ACTUAL_PROPERTY:
				getMaterialActualProperty().clear();
				getMaterialActualProperty().addAll((Collection<? extends OpMaterialActualPropertyType>)newValue);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				setRequiredByRequestedSegmentResponse((RequiredByRequestedSegmentResponseType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_CLASS_ID:
				getMaterialClassID().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_DEFINITION_ID:
				getMaterialDefinitionID().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_LOT_ID:
				getMaterialLotID().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_SUB_LOT_ID:
				getMaterialSubLotID().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_USE:
				setMaterialUse((MaterialUseType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__STORAGE_LOCATION:
				setStorageLocation((StorageLocationType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_ACTUAL:
				getAssemblyActual().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_TYPE:
				setAssemblyType((AssemblyTypeType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_RELATIONSHIP:
				setAssemblyRelationship((AssemblyRelationshipType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_ACTUAL_PROPERTY:
				getMaterialActualProperty().clear();
				return;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				setRequiredByRequestedSegmentResponse((RequiredByRequestedSegmentResponseType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_CLASS_ID:
				return materialClassID != null && !materialClassID.isEmpty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_DEFINITION_ID:
				return materialDefinitionID != null && !materialDefinitionID.isEmpty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_LOT_ID:
				return materialLotID != null && !materialLotID.isEmpty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_SUB_LOT_ID:
				return materialSubLotID != null && !materialSubLotID.isEmpty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_USE:
				return materialUse != null;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__STORAGE_LOCATION:
				return storageLocation != null;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_ACTUAL:
				return assemblyActual != null && !assemblyActual.isEmpty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_TYPE:
				return assemblyType != null;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__ASSEMBLY_RELATIONSHIP:
				return assemblyRelationship != null;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__MATERIAL_ACTUAL_PROPERTY:
				return materialActualProperty != null && !materialActualProperty.isEmpty();
			case B2MMLPackage.OP_MATERIAL_ACTUAL_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return requiredByRequestedSegmentResponse != null;
		}
		return super.eIsSet(featureID);
	}

} //OpMaterialActualTypeImpl
