/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.PlannedStartTimeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Planned Start Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PlannedStartTimeTypeImpl extends DateTimeTypeImpl implements PlannedStartTimeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PlannedStartTimeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPlannedStartTimeType();
	}

} //PlannedStartTimeTypeImpl
