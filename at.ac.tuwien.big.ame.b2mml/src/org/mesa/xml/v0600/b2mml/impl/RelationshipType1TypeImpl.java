/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.RelationshipType1Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Relationship Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RelationshipType1TypeImpl extends CodeTypeImpl implements RelationshipType1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RelationshipType1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getRelationshipType1Type();
	}

} //RelationshipType1TypeImpl
