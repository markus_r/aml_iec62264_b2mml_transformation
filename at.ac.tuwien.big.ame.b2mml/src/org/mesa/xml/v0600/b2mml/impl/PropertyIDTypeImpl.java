/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.PropertyIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PropertyIDTypeImpl extends IdentifierTypeImpl implements PropertyIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropertyIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPropertyIDType();
	}

} //PropertyIDTypeImpl
