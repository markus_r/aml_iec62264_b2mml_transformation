/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.CodeType;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetClassIDType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetIDType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetSegmentSpecificationPropertyType;
import org.mesa.xml.v0600.b2mml.PhysicalAssetSegmentSpecificationType;
import org.mesa.xml.v0600.b2mml.QuantityValueType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Asset Segment Specification Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetSegmentSpecificationTypeImpl#getPhysicalAssetClassID <em>Physical Asset Class ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetSegmentSpecificationTypeImpl#getPhysicalAssetID <em>Physical Asset ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetSegmentSpecificationTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetSegmentSpecificationTypeImpl#getPhysicalAssetUse <em>Physical Asset Use</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetSegmentSpecificationTypeImpl#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.PhysicalAssetSegmentSpecificationTypeImpl#getPhysicalAssetSegmentSpecificationProperty <em>Physical Asset Segment Specification Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalAssetSegmentSpecificationTypeImpl extends MinimalEObjectImpl.Container implements PhysicalAssetSegmentSpecificationType {
	/**
	 * The cached value of the '{@link #getPhysicalAssetClassID() <em>Physical Asset Class ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetClassID()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetClassIDType physicalAssetClassID;

	/**
	 * The cached value of the '{@link #getPhysicalAssetID() <em>Physical Asset ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetID()
	 * @generated
	 * @ordered
	 */
	protected PhysicalAssetIDType physicalAssetID;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getPhysicalAssetUse() <em>Physical Asset Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetUse()
	 * @generated
	 * @ordered
	 */
	protected CodeType physicalAssetUse;

	/**
	 * The cached value of the '{@link #getQuantity() <em>Quantity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantityValueType> quantity;

	/**
	 * The cached value of the '{@link #getPhysicalAssetSegmentSpecificationProperty() <em>Physical Asset Segment Specification Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetSegmentSpecificationProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalAssetSegmentSpecificationPropertyType> physicalAssetSegmentSpecificationProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalAssetSegmentSpecificationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getPhysicalAssetSegmentSpecificationType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetClassIDType getPhysicalAssetClassID() {
		return physicalAssetClassID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPhysicalAssetClassID(PhysicalAssetClassIDType newPhysicalAssetClassID, NotificationChain msgs) {
		PhysicalAssetClassIDType oldPhysicalAssetClassID = physicalAssetClassID;
		physicalAssetClassID = newPhysicalAssetClassID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID, oldPhysicalAssetClassID, newPhysicalAssetClassID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetClassID(PhysicalAssetClassIDType newPhysicalAssetClassID) {
		if (newPhysicalAssetClassID != physicalAssetClassID) {
			NotificationChain msgs = null;
			if (physicalAssetClassID != null)
				msgs = ((InternalEObject)physicalAssetClassID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID, null, msgs);
			if (newPhysicalAssetClassID != null)
				msgs = ((InternalEObject)newPhysicalAssetClassID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID, null, msgs);
			msgs = basicSetPhysicalAssetClassID(newPhysicalAssetClassID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID, newPhysicalAssetClassID, newPhysicalAssetClassID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhysicalAssetIDType getPhysicalAssetID() {
		return physicalAssetID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPhysicalAssetID(PhysicalAssetIDType newPhysicalAssetID, NotificationChain msgs) {
		PhysicalAssetIDType oldPhysicalAssetID = physicalAssetID;
		physicalAssetID = newPhysicalAssetID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID, oldPhysicalAssetID, newPhysicalAssetID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetID(PhysicalAssetIDType newPhysicalAssetID) {
		if (newPhysicalAssetID != physicalAssetID) {
			NotificationChain msgs = null;
			if (physicalAssetID != null)
				msgs = ((InternalEObject)physicalAssetID).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID, null, msgs);
			if (newPhysicalAssetID != null)
				msgs = ((InternalEObject)newPhysicalAssetID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID, null, msgs);
			msgs = basicSetPhysicalAssetID(newPhysicalAssetID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID, newPhysicalAssetID, newPhysicalAssetID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeType getPhysicalAssetUse() {
		return physicalAssetUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPhysicalAssetUse(CodeType newPhysicalAssetUse, NotificationChain msgs) {
		CodeType oldPhysicalAssetUse = physicalAssetUse;
		physicalAssetUse = newPhysicalAssetUse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE, oldPhysicalAssetUse, newPhysicalAssetUse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalAssetUse(CodeType newPhysicalAssetUse) {
		if (newPhysicalAssetUse != physicalAssetUse) {
			NotificationChain msgs = null;
			if (physicalAssetUse != null)
				msgs = ((InternalEObject)physicalAssetUse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE, null, msgs);
			if (newPhysicalAssetUse != null)
				msgs = ((InternalEObject)newPhysicalAssetUse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE, null, msgs);
			msgs = basicSetPhysicalAssetUse(newPhysicalAssetUse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE, newPhysicalAssetUse, newPhysicalAssetUse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantityValueType> getQuantity() {
		if (quantity == null) {
			quantity = new EObjectContainmentEList<QuantityValueType>(QuantityValueType.class, this, B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__QUANTITY);
		}
		return quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalAssetSegmentSpecificationPropertyType> getPhysicalAssetSegmentSpecificationProperty() {
		if (physicalAssetSegmentSpecificationProperty == null) {
			physicalAssetSegmentSpecificationProperty = new EObjectContainmentEList<PhysicalAssetSegmentSpecificationPropertyType>(PhysicalAssetSegmentSpecificationPropertyType.class, this, B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY);
		}
		return physicalAssetSegmentSpecificationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return basicSetPhysicalAssetClassID(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID:
				return basicSetPhysicalAssetID(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE:
				return basicSetPhysicalAssetUse(null, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				return ((InternalEList<?>)getQuantity()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY:
				return ((InternalEList<?>)getPhysicalAssetSegmentSpecificationProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return getPhysicalAssetClassID();
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID:
				return getPhysicalAssetID();
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE:
				return getPhysicalAssetUse();
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				return getQuantity();
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY:
				return getPhysicalAssetSegmentSpecificationProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID:
				setPhysicalAssetClassID((PhysicalAssetClassIDType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID:
				setPhysicalAssetID((PhysicalAssetIDType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE:
				setPhysicalAssetUse((CodeType)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				getQuantity().addAll((Collection<? extends QuantityValueType>)newValue);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY:
				getPhysicalAssetSegmentSpecificationProperty().clear();
				getPhysicalAssetSegmentSpecificationProperty().addAll((Collection<? extends PhysicalAssetSegmentSpecificationPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID:
				setPhysicalAssetClassID((PhysicalAssetClassIDType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID:
				setPhysicalAssetID((PhysicalAssetIDType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE:
				setPhysicalAssetUse((CodeType)null);
				return;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				getQuantity().clear();
				return;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY:
				getPhysicalAssetSegmentSpecificationProperty().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_CLASS_ID:
				return physicalAssetClassID != null;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_ID:
				return physicalAssetID != null;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_USE:
				return physicalAssetUse != null;
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__QUANTITY:
				return quantity != null && !quantity.isEmpty();
			case B2MMLPackage.PHYSICAL_ASSET_SEGMENT_SPECIFICATION_TYPE__PHYSICAL_ASSET_SEGMENT_SPECIFICATION_PROPERTY:
				return physicalAssetSegmentSpecificationProperty != null && !physicalAssetSegmentSpecificationProperty.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PhysicalAssetSegmentSpecificationTypeImpl
