/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.mesa.xml.v0600.b2mml.AmountType;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Amount Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AmountTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AmountTypeImpl#getCurrencyCodeListVersionID <em>Currency Code List Version ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.AmountTypeImpl#getCurrencyID <em>Currency ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AmountTypeImpl extends MinimalEObjectImpl.Container implements AmountType {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCurrencyCodeListVersionID() <em>Currency Code List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrencyCodeListVersionID()
	 * @generated
	 * @ordered
	 */
	protected static final String CURRENCY_CODE_LIST_VERSION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCurrencyCodeListVersionID() <em>Currency Code List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrencyCodeListVersionID()
	 * @generated
	 * @ordered
	 */
	protected String currencyCodeListVersionID = CURRENCY_CODE_LIST_VERSION_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getCurrencyID() <em>Currency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrencyID()
	 * @generated
	 * @ordered
	 */
	protected static final String CURRENCY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCurrencyID() <em>Currency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrencyID()
	 * @generated
	 * @ordered
	 */
	protected String currencyID = CURRENCY_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AmountTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getAmountType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(BigDecimal newValue) {
		BigDecimal oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.AMOUNT_TYPE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCurrencyCodeListVersionID() {
		return currencyCodeListVersionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrencyCodeListVersionID(String newCurrencyCodeListVersionID) {
		String oldCurrencyCodeListVersionID = currencyCodeListVersionID;
		currencyCodeListVersionID = newCurrencyCodeListVersionID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.AMOUNT_TYPE__CURRENCY_CODE_LIST_VERSION_ID, oldCurrencyCodeListVersionID, currencyCodeListVersionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCurrencyID() {
		return currencyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrencyID(String newCurrencyID) {
		String oldCurrencyID = currencyID;
		currencyID = newCurrencyID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.AMOUNT_TYPE__CURRENCY_ID, oldCurrencyID, currencyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.AMOUNT_TYPE__VALUE:
				return getValue();
			case B2MMLPackage.AMOUNT_TYPE__CURRENCY_CODE_LIST_VERSION_ID:
				return getCurrencyCodeListVersionID();
			case B2MMLPackage.AMOUNT_TYPE__CURRENCY_ID:
				return getCurrencyID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.AMOUNT_TYPE__VALUE:
				setValue((BigDecimal)newValue);
				return;
			case B2MMLPackage.AMOUNT_TYPE__CURRENCY_CODE_LIST_VERSION_ID:
				setCurrencyCodeListVersionID((String)newValue);
				return;
			case B2MMLPackage.AMOUNT_TYPE__CURRENCY_ID:
				setCurrencyID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.AMOUNT_TYPE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case B2MMLPackage.AMOUNT_TYPE__CURRENCY_CODE_LIST_VERSION_ID:
				setCurrencyCodeListVersionID(CURRENCY_CODE_LIST_VERSION_ID_EDEFAULT);
				return;
			case B2MMLPackage.AMOUNT_TYPE__CURRENCY_ID:
				setCurrencyID(CURRENCY_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.AMOUNT_TYPE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case B2MMLPackage.AMOUNT_TYPE__CURRENCY_CODE_LIST_VERSION_ID:
				return CURRENCY_CODE_LIST_VERSION_ID_EDEFAULT == null ? currencyCodeListVersionID != null : !CURRENCY_CODE_LIST_VERSION_ID_EDEFAULT.equals(currencyCodeListVersionID);
			case B2MMLPackage.AMOUNT_TYPE__CURRENCY_ID:
				return CURRENCY_ID_EDEFAULT == null ? currencyID != null : !CURRENCY_ID_EDEFAULT.equals(currencyID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", currencyCodeListVersionID: ");
		result.append(currencyCodeListVersionID);
		result.append(", currencyID: ");
		result.append(currencyID);
		result.append(')');
		return result.toString();
	}

} //AmountTypeImpl
