/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataType1Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Type1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DataType1TypeImpl extends CodeTypeImpl implements DataType1Type {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataType1TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataType1Type();
	}

} //DataType1TypeImpl
