/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.EquipmentCapabilityTestSpecificationIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Capability Test Specification ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EquipmentCapabilityTestSpecificationIDTypeImpl extends IdentifierTypeImpl implements EquipmentCapabilityTestSpecificationIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentCapabilityTestSpecificationIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getEquipmentCapabilityTestSpecificationIDType();
	}

} //EquipmentCapabilityTestSpecificationIDTypeImpl
