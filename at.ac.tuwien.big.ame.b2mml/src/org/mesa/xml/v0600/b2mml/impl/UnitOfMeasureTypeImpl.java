/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.UnitOfMeasureType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unit Of Measure Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UnitOfMeasureTypeImpl extends CodeTypeImpl implements UnitOfMeasureType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnitOfMeasureTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getUnitOfMeasureType();
	}

} //UnitOfMeasureTypeImpl
