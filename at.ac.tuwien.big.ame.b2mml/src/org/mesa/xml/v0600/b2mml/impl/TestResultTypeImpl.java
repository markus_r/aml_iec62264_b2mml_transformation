/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.ExpirationTimeType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.ResultType;
import org.mesa.xml.v0600.b2mml.TestDateTimeType;
import org.mesa.xml.v0600.b2mml.TestResultType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Result Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TestResultTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TestResultTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TestResultTypeImpl#getTestDateTime <em>Test Date Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TestResultTypeImpl#getResult <em>Result</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.TestResultTypeImpl#getExpirationTime <em>Expiration Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestResultTypeImpl extends MinimalEObjectImpl.Container implements TestResultType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getTestDateTime() <em>Test Date Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestDateTime()
	 * @generated
	 * @ordered
	 */
	protected TestDateTimeType testDateTime;

	/**
	 * The cached value of the '{@link #getResult() <em>Result</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResult()
	 * @generated
	 * @ordered
	 */
	protected EList<ResultType> result;

	/**
	 * The cached value of the '{@link #getExpirationTime() <em>Expiration Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpirationTime()
	 * @generated
	 * @ordered
	 */
	protected ExpirationTimeType expirationTime;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestResultTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getTestResultType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TEST_RESULT_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TEST_RESULT_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TEST_RESULT_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TEST_RESULT_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.TEST_RESULT_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestDateTimeType getTestDateTime() {
		return testDateTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTestDateTime(TestDateTimeType newTestDateTime, NotificationChain msgs) {
		TestDateTimeType oldTestDateTime = testDateTime;
		testDateTime = newTestDateTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TEST_RESULT_TYPE__TEST_DATE_TIME, oldTestDateTime, newTestDateTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestDateTime(TestDateTimeType newTestDateTime) {
		if (newTestDateTime != testDateTime) {
			NotificationChain msgs = null;
			if (testDateTime != null)
				msgs = ((InternalEObject)testDateTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TEST_RESULT_TYPE__TEST_DATE_TIME, null, msgs);
			if (newTestDateTime != null)
				msgs = ((InternalEObject)newTestDateTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TEST_RESULT_TYPE__TEST_DATE_TIME, null, msgs);
			msgs = basicSetTestDateTime(newTestDateTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TEST_RESULT_TYPE__TEST_DATE_TIME, newTestDateTime, newTestDateTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ResultType> getResult() {
		if (result == null) {
			result = new EObjectContainmentEList<ResultType>(ResultType.class, this, B2MMLPackage.TEST_RESULT_TYPE__RESULT);
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpirationTimeType getExpirationTime() {
		return expirationTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpirationTime(ExpirationTimeType newExpirationTime, NotificationChain msgs) {
		ExpirationTimeType oldExpirationTime = expirationTime;
		expirationTime = newExpirationTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.TEST_RESULT_TYPE__EXPIRATION_TIME, oldExpirationTime, newExpirationTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpirationTime(ExpirationTimeType newExpirationTime) {
		if (newExpirationTime != expirationTime) {
			NotificationChain msgs = null;
			if (expirationTime != null)
				msgs = ((InternalEObject)expirationTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TEST_RESULT_TYPE__EXPIRATION_TIME, null, msgs);
			if (newExpirationTime != null)
				msgs = ((InternalEObject)newExpirationTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.TEST_RESULT_TYPE__EXPIRATION_TIME, null, msgs);
			msgs = basicSetExpirationTime(newExpirationTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.TEST_RESULT_TYPE__EXPIRATION_TIME, newExpirationTime, newExpirationTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.TEST_RESULT_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.TEST_RESULT_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.TEST_RESULT_TYPE__TEST_DATE_TIME:
				return basicSetTestDateTime(null, msgs);
			case B2MMLPackage.TEST_RESULT_TYPE__RESULT:
				return ((InternalEList<?>)getResult()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.TEST_RESULT_TYPE__EXPIRATION_TIME:
				return basicSetExpirationTime(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.TEST_RESULT_TYPE__ID:
				return getID();
			case B2MMLPackage.TEST_RESULT_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.TEST_RESULT_TYPE__TEST_DATE_TIME:
				return getTestDateTime();
			case B2MMLPackage.TEST_RESULT_TYPE__RESULT:
				return getResult();
			case B2MMLPackage.TEST_RESULT_TYPE__EXPIRATION_TIME:
				return getExpirationTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.TEST_RESULT_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.TEST_RESULT_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.TEST_RESULT_TYPE__TEST_DATE_TIME:
				setTestDateTime((TestDateTimeType)newValue);
				return;
			case B2MMLPackage.TEST_RESULT_TYPE__RESULT:
				getResult().clear();
				getResult().addAll((Collection<? extends ResultType>)newValue);
				return;
			case B2MMLPackage.TEST_RESULT_TYPE__EXPIRATION_TIME:
				setExpirationTime((ExpirationTimeType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TEST_RESULT_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.TEST_RESULT_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.TEST_RESULT_TYPE__TEST_DATE_TIME:
				setTestDateTime((TestDateTimeType)null);
				return;
			case B2MMLPackage.TEST_RESULT_TYPE__RESULT:
				getResult().clear();
				return;
			case B2MMLPackage.TEST_RESULT_TYPE__EXPIRATION_TIME:
				setExpirationTime((ExpirationTimeType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.TEST_RESULT_TYPE__ID:
				return iD != null;
			case B2MMLPackage.TEST_RESULT_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.TEST_RESULT_TYPE__TEST_DATE_TIME:
				return testDateTime != null;
			case B2MMLPackage.TEST_RESULT_TYPE__RESULT:
				return result != null && !result.isEmpty();
			case B2MMLPackage.TEST_RESULT_TYPE__EXPIRATION_TIME:
				return expirationTime != null;
		}
		return super.eIsSet(featureID);
	}

} //TestResultTypeImpl
