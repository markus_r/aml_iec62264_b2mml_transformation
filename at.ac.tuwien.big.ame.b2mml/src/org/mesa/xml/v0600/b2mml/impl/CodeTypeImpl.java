/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.CodeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Code Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.CodeTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.CodeTypeImpl#getLanguageID <em>Language ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.CodeTypeImpl#getListAgencyID <em>List Agency ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.CodeTypeImpl#getListAgencyName <em>List Agency Name</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.CodeTypeImpl#getListID <em>List ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.CodeTypeImpl#getListName <em>List Name</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.CodeTypeImpl#getListSchemeURI <em>List Scheme URI</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.CodeTypeImpl#getListURI <em>List URI</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.CodeTypeImpl#getListVersionID <em>List Version ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.CodeTypeImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CodeTypeImpl extends MinimalEObjectImpl.Container implements CodeType {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLanguageID() <em>Language ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguageID()
	 * @generated
	 * @ordered
	 */
	protected static final String LANGUAGE_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLanguageID() <em>Language ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguageID()
	 * @generated
	 * @ordered
	 */
	protected String languageID = LANGUAGE_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getListAgencyID() <em>List Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListAgencyID()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_AGENCY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListAgencyID() <em>List Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListAgencyID()
	 * @generated
	 * @ordered
	 */
	protected String listAgencyID = LIST_AGENCY_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getListAgencyName() <em>List Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListAgencyName()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_AGENCY_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListAgencyName() <em>List Agency Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListAgencyName()
	 * @generated
	 * @ordered
	 */
	protected String listAgencyName = LIST_AGENCY_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getListID() <em>List ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListID()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListID() <em>List ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListID()
	 * @generated
	 * @ordered
	 */
	protected String listID = LIST_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getListName() <em>List Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListName()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListName() <em>List Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListName()
	 * @generated
	 * @ordered
	 */
	protected String listName = LIST_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getListSchemeURI() <em>List Scheme URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListSchemeURI()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_SCHEME_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListSchemeURI() <em>List Scheme URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListSchemeURI()
	 * @generated
	 * @ordered
	 */
	protected String listSchemeURI = LIST_SCHEME_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getListURI() <em>List URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListURI()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListURI() <em>List URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListURI()
	 * @generated
	 * @ordered
	 */
	protected String listURI = LIST_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getListVersionID() <em>List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListVersionID()
	 * @generated
	 * @ordered
	 */
	protected static final String LIST_VERSION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getListVersionID() <em>List Version ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListVersionID()
	 * @generated
	 * @ordered
	 */
	protected String listVersionID = LIST_VERSION_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getCodeType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CODE_TYPE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLanguageID() {
		return languageID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLanguageID(String newLanguageID) {
		String oldLanguageID = languageID;
		languageID = newLanguageID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CODE_TYPE__LANGUAGE_ID, oldLanguageID, languageID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListAgencyID() {
		return listAgencyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListAgencyID(String newListAgencyID) {
		String oldListAgencyID = listAgencyID;
		listAgencyID = newListAgencyID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CODE_TYPE__LIST_AGENCY_ID, oldListAgencyID, listAgencyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListAgencyName() {
		return listAgencyName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListAgencyName(String newListAgencyName) {
		String oldListAgencyName = listAgencyName;
		listAgencyName = newListAgencyName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CODE_TYPE__LIST_AGENCY_NAME, oldListAgencyName, listAgencyName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListID() {
		return listID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListID(String newListID) {
		String oldListID = listID;
		listID = newListID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CODE_TYPE__LIST_ID, oldListID, listID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListName() {
		return listName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListName(String newListName) {
		String oldListName = listName;
		listName = newListName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CODE_TYPE__LIST_NAME, oldListName, listName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListSchemeURI() {
		return listSchemeURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListSchemeURI(String newListSchemeURI) {
		String oldListSchemeURI = listSchemeURI;
		listSchemeURI = newListSchemeURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CODE_TYPE__LIST_SCHEME_URI, oldListSchemeURI, listSchemeURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListURI() {
		return listURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListURI(String newListURI) {
		String oldListURI = listURI;
		listURI = newListURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CODE_TYPE__LIST_URI, oldListURI, listURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getListVersionID() {
		return listVersionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListVersionID(String newListVersionID) {
		String oldListVersionID = listVersionID;
		listVersionID = newListVersionID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CODE_TYPE__LIST_VERSION_ID, oldListVersionID, listVersionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.CODE_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.CODE_TYPE__VALUE:
				return getValue();
			case B2MMLPackage.CODE_TYPE__LANGUAGE_ID:
				return getLanguageID();
			case B2MMLPackage.CODE_TYPE__LIST_AGENCY_ID:
				return getListAgencyID();
			case B2MMLPackage.CODE_TYPE__LIST_AGENCY_NAME:
				return getListAgencyName();
			case B2MMLPackage.CODE_TYPE__LIST_ID:
				return getListID();
			case B2MMLPackage.CODE_TYPE__LIST_NAME:
				return getListName();
			case B2MMLPackage.CODE_TYPE__LIST_SCHEME_URI:
				return getListSchemeURI();
			case B2MMLPackage.CODE_TYPE__LIST_URI:
				return getListURI();
			case B2MMLPackage.CODE_TYPE__LIST_VERSION_ID:
				return getListVersionID();
			case B2MMLPackage.CODE_TYPE__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.CODE_TYPE__VALUE:
				setValue((String)newValue);
				return;
			case B2MMLPackage.CODE_TYPE__LANGUAGE_ID:
				setLanguageID((String)newValue);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_AGENCY_ID:
				setListAgencyID((String)newValue);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_AGENCY_NAME:
				setListAgencyName((String)newValue);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_ID:
				setListID((String)newValue);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_NAME:
				setListName((String)newValue);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_SCHEME_URI:
				setListSchemeURI((String)newValue);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_URI:
				setListURI((String)newValue);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_VERSION_ID:
				setListVersionID((String)newValue);
				return;
			case B2MMLPackage.CODE_TYPE__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.CODE_TYPE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case B2MMLPackage.CODE_TYPE__LANGUAGE_ID:
				setLanguageID(LANGUAGE_ID_EDEFAULT);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_AGENCY_ID:
				setListAgencyID(LIST_AGENCY_ID_EDEFAULT);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_AGENCY_NAME:
				setListAgencyName(LIST_AGENCY_NAME_EDEFAULT);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_ID:
				setListID(LIST_ID_EDEFAULT);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_NAME:
				setListName(LIST_NAME_EDEFAULT);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_SCHEME_URI:
				setListSchemeURI(LIST_SCHEME_URI_EDEFAULT);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_URI:
				setListURI(LIST_URI_EDEFAULT);
				return;
			case B2MMLPackage.CODE_TYPE__LIST_VERSION_ID:
				setListVersionID(LIST_VERSION_ID_EDEFAULT);
				return;
			case B2MMLPackage.CODE_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.CODE_TYPE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case B2MMLPackage.CODE_TYPE__LANGUAGE_ID:
				return LANGUAGE_ID_EDEFAULT == null ? languageID != null : !LANGUAGE_ID_EDEFAULT.equals(languageID);
			case B2MMLPackage.CODE_TYPE__LIST_AGENCY_ID:
				return LIST_AGENCY_ID_EDEFAULT == null ? listAgencyID != null : !LIST_AGENCY_ID_EDEFAULT.equals(listAgencyID);
			case B2MMLPackage.CODE_TYPE__LIST_AGENCY_NAME:
				return LIST_AGENCY_NAME_EDEFAULT == null ? listAgencyName != null : !LIST_AGENCY_NAME_EDEFAULT.equals(listAgencyName);
			case B2MMLPackage.CODE_TYPE__LIST_ID:
				return LIST_ID_EDEFAULT == null ? listID != null : !LIST_ID_EDEFAULT.equals(listID);
			case B2MMLPackage.CODE_TYPE__LIST_NAME:
				return LIST_NAME_EDEFAULT == null ? listName != null : !LIST_NAME_EDEFAULT.equals(listName);
			case B2MMLPackage.CODE_TYPE__LIST_SCHEME_URI:
				return LIST_SCHEME_URI_EDEFAULT == null ? listSchemeURI != null : !LIST_SCHEME_URI_EDEFAULT.equals(listSchemeURI);
			case B2MMLPackage.CODE_TYPE__LIST_URI:
				return LIST_URI_EDEFAULT == null ? listURI != null : !LIST_URI_EDEFAULT.equals(listURI);
			case B2MMLPackage.CODE_TYPE__LIST_VERSION_ID:
				return LIST_VERSION_ID_EDEFAULT == null ? listVersionID != null : !LIST_VERSION_ID_EDEFAULT.equals(listVersionID);
			case B2MMLPackage.CODE_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", languageID: ");
		result.append(languageID);
		result.append(", listAgencyID: ");
		result.append(listAgencyID);
		result.append(", listAgencyName: ");
		result.append(listAgencyName);
		result.append(", listID: ");
		result.append(listID);
		result.append(", listName: ");
		result.append(listName);
		result.append(", listSchemeURI: ");
		result.append(listSchemeURI);
		result.append(", listURI: ");
		result.append(listURI);
		result.append(", listVersionID: ");
		result.append(listVersionID);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //CodeTypeImpl
