/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DataAreaType14;
import org.mesa.xml.v0600.b2mml.OperationsCapabilityInformationType;
import org.mesa.xml.v0600.b2mml.TransProcessType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Area Type14</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType14Impl#getProcess <em>Process</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.DataAreaType14Impl#getOperationsCapabilityInformation <em>Operations Capability Information</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataAreaType14Impl extends MinimalEObjectImpl.Container implements DataAreaType14 {
	/**
	 * The cached value of the '{@link #getProcess() <em>Process</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcess()
	 * @generated
	 * @ordered
	 */
	protected TransProcessType process;

	/**
	 * The cached value of the '{@link #getOperationsCapabilityInformation() <em>Operations Capability Information</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsCapabilityInformation()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsCapabilityInformationType> operationsCapabilityInformation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataAreaType14Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getDataAreaType14();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransProcessType getProcess() {
		return process;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcess(TransProcessType newProcess, NotificationChain msgs) {
		TransProcessType oldProcess = process;
		process = newProcess;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE14__PROCESS, oldProcess, newProcess);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcess(TransProcessType newProcess) {
		if (newProcess != process) {
			NotificationChain msgs = null;
			if (process != null)
				msgs = ((InternalEObject)process).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE14__PROCESS, null, msgs);
			if (newProcess != null)
				msgs = ((InternalEObject)newProcess).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.DATA_AREA_TYPE14__PROCESS, null, msgs);
			msgs = basicSetProcess(newProcess, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.DATA_AREA_TYPE14__PROCESS, newProcess, newProcess));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsCapabilityInformationType> getOperationsCapabilityInformation() {
		if (operationsCapabilityInformation == null) {
			operationsCapabilityInformation = new EObjectContainmentEList<OperationsCapabilityInformationType>(OperationsCapabilityInformationType.class, this, B2MMLPackage.DATA_AREA_TYPE14__OPERATIONS_CAPABILITY_INFORMATION);
		}
		return operationsCapabilityInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE14__PROCESS:
				return basicSetProcess(null, msgs);
			case B2MMLPackage.DATA_AREA_TYPE14__OPERATIONS_CAPABILITY_INFORMATION:
				return ((InternalEList<?>)getOperationsCapabilityInformation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE14__PROCESS:
				return getProcess();
			case B2MMLPackage.DATA_AREA_TYPE14__OPERATIONS_CAPABILITY_INFORMATION:
				return getOperationsCapabilityInformation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE14__PROCESS:
				setProcess((TransProcessType)newValue);
				return;
			case B2MMLPackage.DATA_AREA_TYPE14__OPERATIONS_CAPABILITY_INFORMATION:
				getOperationsCapabilityInformation().clear();
				getOperationsCapabilityInformation().addAll((Collection<? extends OperationsCapabilityInformationType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE14__PROCESS:
				setProcess((TransProcessType)null);
				return;
			case B2MMLPackage.DATA_AREA_TYPE14__OPERATIONS_CAPABILITY_INFORMATION:
				getOperationsCapabilityInformation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.DATA_AREA_TYPE14__PROCESS:
				return process != null;
			case B2MMLPackage.DATA_AREA_TYPE14__OPERATIONS_CAPABILITY_INFORMATION:
				return operationsCapabilityInformation != null && !operationsCapabilityInformation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataAreaType14Impl
