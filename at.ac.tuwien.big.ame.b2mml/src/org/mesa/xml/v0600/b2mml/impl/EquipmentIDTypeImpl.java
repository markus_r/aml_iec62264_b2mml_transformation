/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import org.eclipse.emf.ecore.EClass;

import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.EquipmentIDType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EquipmentIDTypeImpl extends IdentifierTypeImpl implements EquipmentIDType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EquipmentIDTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getEquipmentIDType();
	}

} //EquipmentIDTypeImpl
