/**
 */
package org.mesa.xml.v0600.b2mml.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.mesa.xml.v0600.b2mml.ActualEndTimeType;
import org.mesa.xml.v0600.b2mml.ActualStartTimeType;
import org.mesa.xml.v0600.b2mml.B2MMLPackage;
import org.mesa.xml.v0600.b2mml.DescriptionType;
import org.mesa.xml.v0600.b2mml.HierarchyScopeType;
import org.mesa.xml.v0600.b2mml.IdentifierType;
import org.mesa.xml.v0600.b2mml.OpEquipmentActualType;
import org.mesa.xml.v0600.b2mml.OpMaterialActualType;
import org.mesa.xml.v0600.b2mml.OpPersonnelActualType;
import org.mesa.xml.v0600.b2mml.OpPhysicalAssetActualType;
import org.mesa.xml.v0600.b2mml.OpSegmentDataType;
import org.mesa.xml.v0600.b2mml.OpSegmentResponseType;
import org.mesa.xml.v0600.b2mml.OperationsDefinitionIDType;
import org.mesa.xml.v0600.b2mml.OperationsTypeType;
import org.mesa.xml.v0600.b2mml.ProcessSegmentIDType;
import org.mesa.xml.v0600.b2mml.RequiredByRequestedSegmentResponseType;
import org.mesa.xml.v0600.b2mml.ResponseStateType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Segment Response Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getHierarchyScope <em>Hierarchy Scope</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getOperationsType <em>Operations Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getProcessSegmentID <em>Process Segment ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getActualStartTime <em>Actual Start Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getActualEndTime <em>Actual End Time</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getOperationsDefinitionID <em>Operations Definition ID</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getSegmentState <em>Segment State</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getSegmentData <em>Segment Data</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getPersonnelActual <em>Personnel Actual</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getEquipmentActual <em>Equipment Actual</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getPhysicalAssetActual <em>Physical Asset Actual</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getMaterialActual <em>Material Actual</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getSegmentResponse <em>Segment Response</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.impl.OpSegmentResponseTypeImpl#getRequiredByRequestedSegmentResponse <em>Required By Requested Segment Response</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpSegmentResponseTypeImpl extends MinimalEObjectImpl.Container implements OpSegmentResponseType {
	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected IdentifierType iD;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected EList<DescriptionType> description;

	/**
	 * The cached value of the '{@link #getHierarchyScope() <em>Hierarchy Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchyScope()
	 * @generated
	 * @ordered
	 */
	protected HierarchyScopeType hierarchyScope;

	/**
	 * The cached value of the '{@link #getOperationsType() <em>Operations Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsType()
	 * @generated
	 * @ordered
	 */
	protected OperationsTypeType operationsType;

	/**
	 * The cached value of the '{@link #getProcessSegmentID() <em>Process Segment ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessSegmentID()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcessSegmentIDType> processSegmentID;

	/**
	 * The cached value of the '{@link #getActualStartTime() <em>Actual Start Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualStartTime()
	 * @generated
	 * @ordered
	 */
	protected ActualStartTimeType actualStartTime;

	/**
	 * The cached value of the '{@link #getActualEndTime() <em>Actual End Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualEndTime()
	 * @generated
	 * @ordered
	 */
	protected ActualEndTimeType actualEndTime;

	/**
	 * The cached value of the '{@link #getOperationsDefinitionID() <em>Operations Definition ID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationsDefinitionID()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationsDefinitionIDType> operationsDefinitionID;

	/**
	 * The cached value of the '{@link #getSegmentState() <em>Segment State</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentState()
	 * @generated
	 * @ordered
	 */
	protected ResponseStateType segmentState;

	/**
	 * The cached value of the '{@link #getSegmentData() <em>Segment Data</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentData()
	 * @generated
	 * @ordered
	 */
	protected EList<OpSegmentDataType> segmentData;

	/**
	 * The cached value of the '{@link #getPersonnelActual() <em>Personnel Actual</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonnelActual()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPersonnelActualType> personnelActual;

	/**
	 * The cached value of the '{@link #getEquipmentActual() <em>Equipment Actual</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEquipmentActual()
	 * @generated
	 * @ordered
	 */
	protected EList<OpEquipmentActualType> equipmentActual;

	/**
	 * The cached value of the '{@link #getPhysicalAssetActual() <em>Physical Asset Actual</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalAssetActual()
	 * @generated
	 * @ordered
	 */
	protected EList<OpPhysicalAssetActualType> physicalAssetActual;

	/**
	 * The cached value of the '{@link #getMaterialActual() <em>Material Actual</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterialActual()
	 * @generated
	 * @ordered
	 */
	protected EList<OpMaterialActualType> materialActual;

	/**
	 * The cached value of the '{@link #getSegmentResponse() <em>Segment Response</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSegmentResponse()
	 * @generated
	 * @ordered
	 */
	protected EList<OpSegmentResponseType> segmentResponse;

	/**
	 * The cached value of the '{@link #getRequiredByRequestedSegmentResponse() <em>Required By Requested Segment Response</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredByRequestedSegmentResponse()
	 * @generated
	 * @ordered
	 */
	protected RequiredByRequestedSegmentResponseType requiredByRequestedSegmentResponse;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OpSegmentResponseTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return B2MMLPackage.eINSTANCE.getOpSegmentResponseType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierType getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetID(IdentifierType newID, NotificationChain msgs) {
		IdentifierType oldID = iD;
		iD = newID;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ID, oldID, newID);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(IdentifierType newID) {
		if (newID != iD) {
			NotificationChain msgs = null;
			if (iD != null)
				msgs = ((InternalEObject)iD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ID, null, msgs);
			if (newID != null)
				msgs = ((InternalEObject)newID).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ID, null, msgs);
			msgs = basicSetID(newID, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ID, newID, newID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DescriptionType> getDescription() {
		if (description == null) {
			description = new EObjectContainmentEList<DescriptionType>(DescriptionType.class, this, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchyScopeType getHierarchyScope() {
		return hierarchyScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHierarchyScope(HierarchyScopeType newHierarchyScope, NotificationChain msgs) {
		HierarchyScopeType oldHierarchyScope = hierarchyScope;
		hierarchyScope = newHierarchyScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__HIERARCHY_SCOPE, oldHierarchyScope, newHierarchyScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHierarchyScope(HierarchyScopeType newHierarchyScope) {
		if (newHierarchyScope != hierarchyScope) {
			NotificationChain msgs = null;
			if (hierarchyScope != null)
				msgs = ((InternalEObject)hierarchyScope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__HIERARCHY_SCOPE, null, msgs);
			if (newHierarchyScope != null)
				msgs = ((InternalEObject)newHierarchyScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__HIERARCHY_SCOPE, null, msgs);
			msgs = basicSetHierarchyScope(newHierarchyScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__HIERARCHY_SCOPE, newHierarchyScope, newHierarchyScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsTypeType getOperationsType() {
		return operationsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationsType(OperationsTypeType newOperationsType, NotificationChain msgs) {
		OperationsTypeType oldOperationsType = operationsType;
		operationsType = newOperationsType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_TYPE, oldOperationsType, newOperationsType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationsType(OperationsTypeType newOperationsType) {
		if (newOperationsType != operationsType) {
			NotificationChain msgs = null;
			if (operationsType != null)
				msgs = ((InternalEObject)operationsType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_TYPE, null, msgs);
			if (newOperationsType != null)
				msgs = ((InternalEObject)newOperationsType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_TYPE, null, msgs);
			msgs = basicSetOperationsType(newOperationsType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_TYPE, newOperationsType, newOperationsType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcessSegmentIDType> getProcessSegmentID() {
		if (processSegmentID == null) {
			processSegmentID = new EObjectContainmentEList<ProcessSegmentIDType>(ProcessSegmentIDType.class, this, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PROCESS_SEGMENT_ID);
		}
		return processSegmentID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActualStartTimeType getActualStartTime() {
		return actualStartTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActualStartTime(ActualStartTimeType newActualStartTime, NotificationChain msgs) {
		ActualStartTimeType oldActualStartTime = actualStartTime;
		actualStartTime = newActualStartTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_START_TIME, oldActualStartTime, newActualStartTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActualStartTime(ActualStartTimeType newActualStartTime) {
		if (newActualStartTime != actualStartTime) {
			NotificationChain msgs = null;
			if (actualStartTime != null)
				msgs = ((InternalEObject)actualStartTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_START_TIME, null, msgs);
			if (newActualStartTime != null)
				msgs = ((InternalEObject)newActualStartTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_START_TIME, null, msgs);
			msgs = basicSetActualStartTime(newActualStartTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_START_TIME, newActualStartTime, newActualStartTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActualEndTimeType getActualEndTime() {
		return actualEndTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActualEndTime(ActualEndTimeType newActualEndTime, NotificationChain msgs) {
		ActualEndTimeType oldActualEndTime = actualEndTime;
		actualEndTime = newActualEndTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_END_TIME, oldActualEndTime, newActualEndTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActualEndTime(ActualEndTimeType newActualEndTime) {
		if (newActualEndTime != actualEndTime) {
			NotificationChain msgs = null;
			if (actualEndTime != null)
				msgs = ((InternalEObject)actualEndTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_END_TIME, null, msgs);
			if (newActualEndTime != null)
				msgs = ((InternalEObject)newActualEndTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_END_TIME, null, msgs);
			msgs = basicSetActualEndTime(newActualEndTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_END_TIME, newActualEndTime, newActualEndTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationsDefinitionIDType> getOperationsDefinitionID() {
		if (operationsDefinitionID == null) {
			operationsDefinitionID = new EObjectContainmentEList<OperationsDefinitionIDType>(OperationsDefinitionIDType.class, this, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_DEFINITION_ID);
		}
		return operationsDefinitionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseStateType getSegmentState() {
		return segmentState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSegmentState(ResponseStateType newSegmentState, NotificationChain msgs) {
		ResponseStateType oldSegmentState = segmentState;
		segmentState = newSegmentState;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_STATE, oldSegmentState, newSegmentState);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSegmentState(ResponseStateType newSegmentState) {
		if (newSegmentState != segmentState) {
			NotificationChain msgs = null;
			if (segmentState != null)
				msgs = ((InternalEObject)segmentState).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_STATE, null, msgs);
			if (newSegmentState != null)
				msgs = ((InternalEObject)newSegmentState).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_STATE, null, msgs);
			msgs = basicSetSegmentState(newSegmentState, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_STATE, newSegmentState, newSegmentState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpSegmentDataType> getSegmentData() {
		if (segmentData == null) {
			segmentData = new EObjectContainmentEList<OpSegmentDataType>(OpSegmentDataType.class, this, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_DATA);
		}
		return segmentData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPersonnelActualType> getPersonnelActual() {
		if (personnelActual == null) {
			personnelActual = new EObjectContainmentEList<OpPersonnelActualType>(OpPersonnelActualType.class, this, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PERSONNEL_ACTUAL);
		}
		return personnelActual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpEquipmentActualType> getEquipmentActual() {
		if (equipmentActual == null) {
			equipmentActual = new EObjectContainmentEList<OpEquipmentActualType>(OpEquipmentActualType.class, this, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__EQUIPMENT_ACTUAL);
		}
		return equipmentActual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpPhysicalAssetActualType> getPhysicalAssetActual() {
		if (physicalAssetActual == null) {
			physicalAssetActual = new EObjectContainmentEList<OpPhysicalAssetActualType>(OpPhysicalAssetActualType.class, this, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PHYSICAL_ASSET_ACTUAL);
		}
		return physicalAssetActual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpMaterialActualType> getMaterialActual() {
		if (materialActual == null) {
			materialActual = new EObjectContainmentEList<OpMaterialActualType>(OpMaterialActualType.class, this, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__MATERIAL_ACTUAL);
		}
		return materialActual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OpSegmentResponseType> getSegmentResponse() {
		if (segmentResponse == null) {
			segmentResponse = new EObjectContainmentEList<OpSegmentResponseType>(OpSegmentResponseType.class, this, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_RESPONSE);
		}
		return segmentResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredByRequestedSegmentResponseType getRequiredByRequestedSegmentResponse() {
		return requiredByRequestedSegmentResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType newRequiredByRequestedSegmentResponse, NotificationChain msgs) {
		RequiredByRequestedSegmentResponseType oldRequiredByRequestedSegmentResponse = requiredByRequestedSegmentResponse;
		requiredByRequestedSegmentResponse = newRequiredByRequestedSegmentResponse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, oldRequiredByRequestedSegmentResponse, newRequiredByRequestedSegmentResponse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredByRequestedSegmentResponse(RequiredByRequestedSegmentResponseType newRequiredByRequestedSegmentResponse) {
		if (newRequiredByRequestedSegmentResponse != requiredByRequestedSegmentResponse) {
			NotificationChain msgs = null;
			if (requiredByRequestedSegmentResponse != null)
				msgs = ((InternalEObject)requiredByRequestedSegmentResponse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, null, msgs);
			if (newRequiredByRequestedSegmentResponse != null)
				msgs = ((InternalEObject)newRequiredByRequestedSegmentResponse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, null, msgs);
			msgs = basicSetRequiredByRequestedSegmentResponse(newRequiredByRequestedSegmentResponse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE, newRequiredByRequestedSegmentResponse, newRequiredByRequestedSegmentResponse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ID:
				return basicSetID(null, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__DESCRIPTION:
				return ((InternalEList<?>)getDescription()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__HIERARCHY_SCOPE:
				return basicSetHierarchyScope(null, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_TYPE:
				return basicSetOperationsType(null, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PROCESS_SEGMENT_ID:
				return ((InternalEList<?>)getProcessSegmentID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_START_TIME:
				return basicSetActualStartTime(null, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_END_TIME:
				return basicSetActualEndTime(null, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_DEFINITION_ID:
				return ((InternalEList<?>)getOperationsDefinitionID()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_STATE:
				return basicSetSegmentState(null, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_DATA:
				return ((InternalEList<?>)getSegmentData()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PERSONNEL_ACTUAL:
				return ((InternalEList<?>)getPersonnelActual()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__EQUIPMENT_ACTUAL:
				return ((InternalEList<?>)getEquipmentActual()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PHYSICAL_ASSET_ACTUAL:
				return ((InternalEList<?>)getPhysicalAssetActual()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__MATERIAL_ACTUAL:
				return ((InternalEList<?>)getMaterialActual()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_RESPONSE:
				return ((InternalEList<?>)getSegmentResponse()).basicRemove(otherEnd, msgs);
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return basicSetRequiredByRequestedSegmentResponse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ID:
				return getID();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__DESCRIPTION:
				return getDescription();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__HIERARCHY_SCOPE:
				return getHierarchyScope();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_TYPE:
				return getOperationsType();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PROCESS_SEGMENT_ID:
				return getProcessSegmentID();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_START_TIME:
				return getActualStartTime();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_END_TIME:
				return getActualEndTime();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_DEFINITION_ID:
				return getOperationsDefinitionID();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_STATE:
				return getSegmentState();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_DATA:
				return getSegmentData();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PERSONNEL_ACTUAL:
				return getPersonnelActual();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__EQUIPMENT_ACTUAL:
				return getEquipmentActual();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PHYSICAL_ASSET_ACTUAL:
				return getPhysicalAssetActual();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__MATERIAL_ACTUAL:
				return getMaterialActual();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_RESPONSE:
				return getSegmentResponse();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return getRequiredByRequestedSegmentResponse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ID:
				setID((IdentifierType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__DESCRIPTION:
				getDescription().clear();
				getDescription().addAll((Collection<? extends DescriptionType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PROCESS_SEGMENT_ID:
				getProcessSegmentID().clear();
				getProcessSegmentID().addAll((Collection<? extends ProcessSegmentIDType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_START_TIME:
				setActualStartTime((ActualStartTimeType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_END_TIME:
				setActualEndTime((ActualEndTimeType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_DEFINITION_ID:
				getOperationsDefinitionID().clear();
				getOperationsDefinitionID().addAll((Collection<? extends OperationsDefinitionIDType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_STATE:
				setSegmentState((ResponseStateType)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_DATA:
				getSegmentData().clear();
				getSegmentData().addAll((Collection<? extends OpSegmentDataType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PERSONNEL_ACTUAL:
				getPersonnelActual().clear();
				getPersonnelActual().addAll((Collection<? extends OpPersonnelActualType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__EQUIPMENT_ACTUAL:
				getEquipmentActual().clear();
				getEquipmentActual().addAll((Collection<? extends OpEquipmentActualType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PHYSICAL_ASSET_ACTUAL:
				getPhysicalAssetActual().clear();
				getPhysicalAssetActual().addAll((Collection<? extends OpPhysicalAssetActualType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__MATERIAL_ACTUAL:
				getMaterialActual().clear();
				getMaterialActual().addAll((Collection<? extends OpMaterialActualType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_RESPONSE:
				getSegmentResponse().clear();
				getSegmentResponse().addAll((Collection<? extends OpSegmentResponseType>)newValue);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				setRequiredByRequestedSegmentResponse((RequiredByRequestedSegmentResponseType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ID:
				setID((IdentifierType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__DESCRIPTION:
				getDescription().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__HIERARCHY_SCOPE:
				setHierarchyScope((HierarchyScopeType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_TYPE:
				setOperationsType((OperationsTypeType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PROCESS_SEGMENT_ID:
				getProcessSegmentID().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_START_TIME:
				setActualStartTime((ActualStartTimeType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_END_TIME:
				setActualEndTime((ActualEndTimeType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_DEFINITION_ID:
				getOperationsDefinitionID().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_STATE:
				setSegmentState((ResponseStateType)null);
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_DATA:
				getSegmentData().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PERSONNEL_ACTUAL:
				getPersonnelActual().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__EQUIPMENT_ACTUAL:
				getEquipmentActual().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PHYSICAL_ASSET_ACTUAL:
				getPhysicalAssetActual().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__MATERIAL_ACTUAL:
				getMaterialActual().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_RESPONSE:
				getSegmentResponse().clear();
				return;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				setRequiredByRequestedSegmentResponse((RequiredByRequestedSegmentResponseType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ID:
				return iD != null;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__DESCRIPTION:
				return description != null && !description.isEmpty();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__HIERARCHY_SCOPE:
				return hierarchyScope != null;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_TYPE:
				return operationsType != null;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PROCESS_SEGMENT_ID:
				return processSegmentID != null && !processSegmentID.isEmpty();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_START_TIME:
				return actualStartTime != null;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__ACTUAL_END_TIME:
				return actualEndTime != null;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__OPERATIONS_DEFINITION_ID:
				return operationsDefinitionID != null && !operationsDefinitionID.isEmpty();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_STATE:
				return segmentState != null;
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_DATA:
				return segmentData != null && !segmentData.isEmpty();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PERSONNEL_ACTUAL:
				return personnelActual != null && !personnelActual.isEmpty();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__EQUIPMENT_ACTUAL:
				return equipmentActual != null && !equipmentActual.isEmpty();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__PHYSICAL_ASSET_ACTUAL:
				return physicalAssetActual != null && !physicalAssetActual.isEmpty();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__MATERIAL_ACTUAL:
				return materialActual != null && !materialActual.isEmpty();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__SEGMENT_RESPONSE:
				return segmentResponse != null && !segmentResponse.isEmpty();
			case B2MMLPackage.OP_SEGMENT_RESPONSE_TYPE__REQUIRED_BY_REQUESTED_SEGMENT_RESPONSE:
				return requiredByRequestedSegmentResponse != null;
		}
		return super.eIsSet(featureID);
	}

} //OpSegmentResponseTypeImpl
