/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trans Signature Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransSignatureType#getAny <em>Any</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.TransSignatureType#getQualifyingAgencyID <em>Qualifying Agency ID</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransSignatureType()
 * @model extendedMetaData="name='TransSignatureType' kind='elementOnly'"
 * @generated
 */
public interface TransSignatureType extends EObject {
	/**
	 * Returns the value of the '<em><b>Any</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any</em>' attribute list.
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransSignatureType_Any()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' wildcards='##any' name=':0' processing='strict'"
	 * @generated
	 */
	FeatureMap getAny();

	/**
	 * Returns the value of the '<em><b>Qualifying Agency ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualifying Agency ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualifying Agency ID</em>' attribute.
	 * @see #setQualifyingAgencyID(String)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getTransSignatureType_QualifyingAgencyID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NormalizedString"
	 *        extendedMetaData="kind='attribute' name='qualifyingAgencyID'"
	 * @generated
	 */
	String getQualifyingAgencyID();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.TransSignatureType#getQualifyingAgencyID <em>Qualifying Agency ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qualifying Agency ID</em>' attribute.
	 * @see #getQualifyingAgencyID()
	 * @generated
	 */
	void setQualifyingAgencyID(String value);

} // TransSignatureType
