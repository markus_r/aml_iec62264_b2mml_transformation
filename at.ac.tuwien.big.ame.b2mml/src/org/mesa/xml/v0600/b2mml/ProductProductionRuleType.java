/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Product Production Rule Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getProductProductionRuleType()
 * @model extendedMetaData="name='ProductProductionRuleType' kind='simple'"
 * @generated
 */
public interface ProductProductionRuleType extends IdentifierType {
} // ProductProductionRuleType
