/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Product Production Rule ID Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getProductProductionRuleIDType()
 * @model extendedMetaData="name='ProductProductionRuleIDType' kind='simple'"
 * @generated
 */
public interface ProductProductionRuleIDType extends IdentifierType {
} // ProductProductionRuleIDType
