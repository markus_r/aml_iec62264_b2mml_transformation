/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage
 * @generated
 */
public interface B2MMLFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	B2MMLFactory eINSTANCE = org.mesa.xml.v0600.b2mml.impl.B2MMLFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Acknowledge Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Equipment Capability Test Spec Type</em>'.
	 * @generated
	 */
	AcknowledgeEquipmentCapabilityTestSpecType createAcknowledgeEquipmentCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Acknowledge Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Equipment Class Type</em>'.
	 * @generated
	 */
	AcknowledgeEquipmentClassType createAcknowledgeEquipmentClassType();

	/**
	 * Returns a new object of class '<em>Acknowledge Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Equipment Information Type</em>'.
	 * @generated
	 */
	AcknowledgeEquipmentInformationType createAcknowledgeEquipmentInformationType();

	/**
	 * Returns a new object of class '<em>Acknowledge Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Equipment Type</em>'.
	 * @generated
	 */
	AcknowledgeEquipmentType createAcknowledgeEquipmentType();

	/**
	 * Returns a new object of class '<em>Acknowledge Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Material Class Type</em>'.
	 * @generated
	 */
	AcknowledgeMaterialClassType createAcknowledgeMaterialClassType();

	/**
	 * Returns a new object of class '<em>Acknowledge Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Material Definition Type</em>'.
	 * @generated
	 */
	AcknowledgeMaterialDefinitionType createAcknowledgeMaterialDefinitionType();

	/**
	 * Returns a new object of class '<em>Acknowledge Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Material Information Type</em>'.
	 * @generated
	 */
	AcknowledgeMaterialInformationType createAcknowledgeMaterialInformationType();

	/**
	 * Returns a new object of class '<em>Acknowledge Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Material Lot Type</em>'.
	 * @generated
	 */
	AcknowledgeMaterialLotType createAcknowledgeMaterialLotType();

	/**
	 * Returns a new object of class '<em>Acknowledge Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Material Sub Lot Type</em>'.
	 * @generated
	 */
	AcknowledgeMaterialSubLotType createAcknowledgeMaterialSubLotType();

	/**
	 * Returns a new object of class '<em>Acknowledge Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Material Test Spec Type</em>'.
	 * @generated
	 */
	AcknowledgeMaterialTestSpecType createAcknowledgeMaterialTestSpecType();

	/**
	 * Returns a new object of class '<em>Acknowledge Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Operations Capability Information Type</em>'.
	 * @generated
	 */
	AcknowledgeOperationsCapabilityInformationType createAcknowledgeOperationsCapabilityInformationType();

	/**
	 * Returns a new object of class '<em>Acknowledge Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Operations Capability Type</em>'.
	 * @generated
	 */
	AcknowledgeOperationsCapabilityType createAcknowledgeOperationsCapabilityType();

	/**
	 * Returns a new object of class '<em>Acknowledge Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Operations Definition Information Type</em>'.
	 * @generated
	 */
	AcknowledgeOperationsDefinitionInformationType createAcknowledgeOperationsDefinitionInformationType();

	/**
	 * Returns a new object of class '<em>Acknowledge Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Operations Definition Type</em>'.
	 * @generated
	 */
	AcknowledgeOperationsDefinitionType createAcknowledgeOperationsDefinitionType();

	/**
	 * Returns a new object of class '<em>Acknowledge Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Operations Performance Type</em>'.
	 * @generated
	 */
	AcknowledgeOperationsPerformanceType createAcknowledgeOperationsPerformanceType();

	/**
	 * Returns a new object of class '<em>Acknowledge Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Operations Schedule Type</em>'.
	 * @generated
	 */
	AcknowledgeOperationsScheduleType createAcknowledgeOperationsScheduleType();

	/**
	 * Returns a new object of class '<em>Acknowledge Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Personnel Class Type</em>'.
	 * @generated
	 */
	AcknowledgePersonnelClassType createAcknowledgePersonnelClassType();

	/**
	 * Returns a new object of class '<em>Acknowledge Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Personnel Information Type</em>'.
	 * @generated
	 */
	AcknowledgePersonnelInformationType createAcknowledgePersonnelInformationType();

	/**
	 * Returns a new object of class '<em>Acknowledge Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Person Type</em>'.
	 * @generated
	 */
	AcknowledgePersonType createAcknowledgePersonType();

	/**
	 * Returns a new object of class '<em>Acknowledge Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Physical Asset Capability Test Spec Type</em>'.
	 * @generated
	 */
	AcknowledgePhysicalAssetCapabilityTestSpecType createAcknowledgePhysicalAssetCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Acknowledge Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Physical Asset Class Type</em>'.
	 * @generated
	 */
	AcknowledgePhysicalAssetClassType createAcknowledgePhysicalAssetClassType();

	/**
	 * Returns a new object of class '<em>Acknowledge Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Physical Asset Information Type</em>'.
	 * @generated
	 */
	AcknowledgePhysicalAssetInformationType createAcknowledgePhysicalAssetInformationType();

	/**
	 * Returns a new object of class '<em>Acknowledge Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Physical Asset Type</em>'.
	 * @generated
	 */
	AcknowledgePhysicalAssetType createAcknowledgePhysicalAssetType();

	/**
	 * Returns a new object of class '<em>Acknowledge Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Process Segment Information Type</em>'.
	 * @generated
	 */
	AcknowledgeProcessSegmentInformationType createAcknowledgeProcessSegmentInformationType();

	/**
	 * Returns a new object of class '<em>Acknowledge Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Process Segment Type</em>'.
	 * @generated
	 */
	AcknowledgeProcessSegmentType createAcknowledgeProcessSegmentType();

	/**
	 * Returns a new object of class '<em>Acknowledge Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Acknowledge Qualification Test Specification Type</em>'.
	 * @generated
	 */
	AcknowledgeQualificationTestSpecificationType createAcknowledgeQualificationTestSpecificationType();

	/**
	 * Returns a new object of class '<em>Actual End Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Actual End Time Type</em>'.
	 * @generated
	 */
	ActualEndTimeType createActualEndTimeType();

	/**
	 * Returns a new object of class '<em>Actual Finish Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Actual Finish Time Type</em>'.
	 * @generated
	 */
	ActualFinishTimeType createActualFinishTimeType();

	/**
	 * Returns a new object of class '<em>Actual Start Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Actual Start Time Type</em>'.
	 * @generated
	 */
	ActualStartTimeType createActualStartTimeType();

	/**
	 * Returns a new object of class '<em>Amount Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Amount Type</em>'.
	 * @generated
	 */
	AmountType createAmountType();

	/**
	 * Returns a new object of class '<em>Any Generic Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Any Generic Value Type</em>'.
	 * @generated
	 */
	AnyGenericValueType createAnyGenericValueType();

	/**
	 * Returns a new object of class '<em>Assembly Relationship1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assembly Relationship1 Type</em>'.
	 * @generated
	 */
	AssemblyRelationship1Type createAssemblyRelationship1Type();

	/**
	 * Returns a new object of class '<em>Assembly Relationship Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assembly Relationship Type</em>'.
	 * @generated
	 */
	AssemblyRelationshipType createAssemblyRelationshipType();

	/**
	 * Returns a new object of class '<em>Assembly Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assembly Type1 Type</em>'.
	 * @generated
	 */
	AssemblyType1Type createAssemblyType1Type();

	/**
	 * Returns a new object of class '<em>Assembly Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assembly Type Type</em>'.
	 * @generated
	 */
	AssemblyTypeType createAssemblyTypeType();

	/**
	 * Returns a new object of class '<em>Bill Of Material ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bill Of Material ID Type</em>'.
	 * @generated
	 */
	BillOfMaterialIDType createBillOfMaterialIDType();

	/**
	 * Returns a new object of class '<em>Bill Of Materials ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bill Of Materials ID Type</em>'.
	 * @generated
	 */
	BillOfMaterialsIDType createBillOfMaterialsIDType();

	/**
	 * Returns a new object of class '<em>Bill Of Resources ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bill Of Resources ID Type</em>'.
	 * @generated
	 */
	BillOfResourcesIDType createBillOfResourcesIDType();

	/**
	 * Returns a new object of class '<em>Binary Object Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Binary Object Type</em>'.
	 * @generated
	 */
	BinaryObjectType createBinaryObjectType();

	/**
	 * Returns a new object of class '<em>BOD Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BOD Type</em>'.
	 * @generated
	 */
	BODType createBODType();

	/**
	 * Returns a new object of class '<em>Cancel Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Equipment Capability Test Spec Type</em>'.
	 * @generated
	 */
	CancelEquipmentCapabilityTestSpecType createCancelEquipmentCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Cancel Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Equipment Class Type</em>'.
	 * @generated
	 */
	CancelEquipmentClassType createCancelEquipmentClassType();

	/**
	 * Returns a new object of class '<em>Cancel Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Equipment Information Type</em>'.
	 * @generated
	 */
	CancelEquipmentInformationType createCancelEquipmentInformationType();

	/**
	 * Returns a new object of class '<em>Cancel Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Equipment Type</em>'.
	 * @generated
	 */
	CancelEquipmentType createCancelEquipmentType();

	/**
	 * Returns a new object of class '<em>Cancel Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Material Class Type</em>'.
	 * @generated
	 */
	CancelMaterialClassType createCancelMaterialClassType();

	/**
	 * Returns a new object of class '<em>Cancel Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Material Definition Type</em>'.
	 * @generated
	 */
	CancelMaterialDefinitionType createCancelMaterialDefinitionType();

	/**
	 * Returns a new object of class '<em>Cancel Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Material Information Type</em>'.
	 * @generated
	 */
	CancelMaterialInformationType createCancelMaterialInformationType();

	/**
	 * Returns a new object of class '<em>Cancel Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Material Lot Type</em>'.
	 * @generated
	 */
	CancelMaterialLotType createCancelMaterialLotType();

	/**
	 * Returns a new object of class '<em>Cancel Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Material Sub Lot Type</em>'.
	 * @generated
	 */
	CancelMaterialSubLotType createCancelMaterialSubLotType();

	/**
	 * Returns a new object of class '<em>Cancel Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Material Test Spec Type</em>'.
	 * @generated
	 */
	CancelMaterialTestSpecType createCancelMaterialTestSpecType();

	/**
	 * Returns a new object of class '<em>Cancel Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Operations Capability Information Type</em>'.
	 * @generated
	 */
	CancelOperationsCapabilityInformationType createCancelOperationsCapabilityInformationType();

	/**
	 * Returns a new object of class '<em>Cancel Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Operations Capability Type</em>'.
	 * @generated
	 */
	CancelOperationsCapabilityType createCancelOperationsCapabilityType();

	/**
	 * Returns a new object of class '<em>Cancel Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Operations Definition Information Type</em>'.
	 * @generated
	 */
	CancelOperationsDefinitionInformationType createCancelOperationsDefinitionInformationType();

	/**
	 * Returns a new object of class '<em>Cancel Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Operations Definition Type</em>'.
	 * @generated
	 */
	CancelOperationsDefinitionType createCancelOperationsDefinitionType();

	/**
	 * Returns a new object of class '<em>Cancel Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Operations Performance Type</em>'.
	 * @generated
	 */
	CancelOperationsPerformanceType createCancelOperationsPerformanceType();

	/**
	 * Returns a new object of class '<em>Cancel Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Operations Schedule Type</em>'.
	 * @generated
	 */
	CancelOperationsScheduleType createCancelOperationsScheduleType();

	/**
	 * Returns a new object of class '<em>Cancel Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Personnel Class Type</em>'.
	 * @generated
	 */
	CancelPersonnelClassType createCancelPersonnelClassType();

	/**
	 * Returns a new object of class '<em>Cancel Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Personnel Information Type</em>'.
	 * @generated
	 */
	CancelPersonnelInformationType createCancelPersonnelInformationType();

	/**
	 * Returns a new object of class '<em>Cancel Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Person Type</em>'.
	 * @generated
	 */
	CancelPersonType createCancelPersonType();

	/**
	 * Returns a new object of class '<em>Cancel Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Physical Asset Capability Test Spec Type</em>'.
	 * @generated
	 */
	CancelPhysicalAssetCapabilityTestSpecType createCancelPhysicalAssetCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Cancel Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Physical Asset Class Type</em>'.
	 * @generated
	 */
	CancelPhysicalAssetClassType createCancelPhysicalAssetClassType();

	/**
	 * Returns a new object of class '<em>Cancel Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Physical Asset Information Type</em>'.
	 * @generated
	 */
	CancelPhysicalAssetInformationType createCancelPhysicalAssetInformationType();

	/**
	 * Returns a new object of class '<em>Cancel Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Physical Asset Type</em>'.
	 * @generated
	 */
	CancelPhysicalAssetType createCancelPhysicalAssetType();

	/**
	 * Returns a new object of class '<em>Cancel Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Process Segment Information Type</em>'.
	 * @generated
	 */
	CancelProcessSegmentInformationType createCancelProcessSegmentInformationType();

	/**
	 * Returns a new object of class '<em>Cancel Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Process Segment Type</em>'.
	 * @generated
	 */
	CancelProcessSegmentType createCancelProcessSegmentType();

	/**
	 * Returns a new object of class '<em>Cancel Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cancel Qualification Test Specification Type</em>'.
	 * @generated
	 */
	CancelQualificationTestSpecificationType createCancelQualificationTestSpecificationType();

	/**
	 * Returns a new object of class '<em>Capability Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Capability Type1 Type</em>'.
	 * @generated
	 */
	CapabilityType1Type createCapabilityType1Type();

	/**
	 * Returns a new object of class '<em>Capability Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Capability Type Type</em>'.
	 * @generated
	 */
	CapabilityTypeType createCapabilityTypeType();

	/**
	 * Returns a new object of class '<em>Cause Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cause Type</em>'.
	 * @generated
	 */
	CauseType createCauseType();

	/**
	 * Returns a new object of class '<em>Certificate Of Analysis Reference Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Certificate Of Analysis Reference Type</em>'.
	 * @generated
	 */
	CertificateOfAnalysisReferenceType createCertificateOfAnalysisReferenceType();

	/**
	 * Returns a new object of class '<em>Change Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Equipment Capability Test Spec Type</em>'.
	 * @generated
	 */
	ChangeEquipmentCapabilityTestSpecType createChangeEquipmentCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Change Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Equipment Class Type</em>'.
	 * @generated
	 */
	ChangeEquipmentClassType createChangeEquipmentClassType();

	/**
	 * Returns a new object of class '<em>Change Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Equipment Information Type</em>'.
	 * @generated
	 */
	ChangeEquipmentInformationType createChangeEquipmentInformationType();

	/**
	 * Returns a new object of class '<em>Change Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Equipment Type</em>'.
	 * @generated
	 */
	ChangeEquipmentType createChangeEquipmentType();

	/**
	 * Returns a new object of class '<em>Change Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Material Class Type</em>'.
	 * @generated
	 */
	ChangeMaterialClassType createChangeMaterialClassType();

	/**
	 * Returns a new object of class '<em>Change Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Material Definition Type</em>'.
	 * @generated
	 */
	ChangeMaterialDefinitionType createChangeMaterialDefinitionType();

	/**
	 * Returns a new object of class '<em>Change Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Material Information Type</em>'.
	 * @generated
	 */
	ChangeMaterialInformationType createChangeMaterialInformationType();

	/**
	 * Returns a new object of class '<em>Change Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Material Lot Type</em>'.
	 * @generated
	 */
	ChangeMaterialLotType createChangeMaterialLotType();

	/**
	 * Returns a new object of class '<em>Change Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Material Sub Lot Type</em>'.
	 * @generated
	 */
	ChangeMaterialSubLotType createChangeMaterialSubLotType();

	/**
	 * Returns a new object of class '<em>Change Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Material Test Spec Type</em>'.
	 * @generated
	 */
	ChangeMaterialTestSpecType createChangeMaterialTestSpecType();

	/**
	 * Returns a new object of class '<em>Change Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Operations Capability Information Type</em>'.
	 * @generated
	 */
	ChangeOperationsCapabilityInformationType createChangeOperationsCapabilityInformationType();

	/**
	 * Returns a new object of class '<em>Change Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Operations Capability Type</em>'.
	 * @generated
	 */
	ChangeOperationsCapabilityType createChangeOperationsCapabilityType();

	/**
	 * Returns a new object of class '<em>Change Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Operations Definition Information Type</em>'.
	 * @generated
	 */
	ChangeOperationsDefinitionInformationType createChangeOperationsDefinitionInformationType();

	/**
	 * Returns a new object of class '<em>Change Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Operations Definition Type</em>'.
	 * @generated
	 */
	ChangeOperationsDefinitionType createChangeOperationsDefinitionType();

	/**
	 * Returns a new object of class '<em>Change Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Operations Performance Type</em>'.
	 * @generated
	 */
	ChangeOperationsPerformanceType createChangeOperationsPerformanceType();

	/**
	 * Returns a new object of class '<em>Change Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Operations Schedule Type</em>'.
	 * @generated
	 */
	ChangeOperationsScheduleType createChangeOperationsScheduleType();

	/**
	 * Returns a new object of class '<em>Change Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Personnel Class Type</em>'.
	 * @generated
	 */
	ChangePersonnelClassType createChangePersonnelClassType();

	/**
	 * Returns a new object of class '<em>Change Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Personnel Information Type</em>'.
	 * @generated
	 */
	ChangePersonnelInformationType createChangePersonnelInformationType();

	/**
	 * Returns a new object of class '<em>Change Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Person Type</em>'.
	 * @generated
	 */
	ChangePersonType createChangePersonType();

	/**
	 * Returns a new object of class '<em>Change Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Physical Asset Capability Test Spec Type</em>'.
	 * @generated
	 */
	ChangePhysicalAssetCapabilityTestSpecType createChangePhysicalAssetCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Change Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Physical Asset Class Type</em>'.
	 * @generated
	 */
	ChangePhysicalAssetClassType createChangePhysicalAssetClassType();

	/**
	 * Returns a new object of class '<em>Change Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Physical Asset Information Type</em>'.
	 * @generated
	 */
	ChangePhysicalAssetInformationType createChangePhysicalAssetInformationType();

	/**
	 * Returns a new object of class '<em>Change Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Physical Asset Type</em>'.
	 * @generated
	 */
	ChangePhysicalAssetType createChangePhysicalAssetType();

	/**
	 * Returns a new object of class '<em>Change Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Process Segment Information Type</em>'.
	 * @generated
	 */
	ChangeProcessSegmentInformationType createChangeProcessSegmentInformationType();

	/**
	 * Returns a new object of class '<em>Change Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Process Segment Type</em>'.
	 * @generated
	 */
	ChangeProcessSegmentType createChangeProcessSegmentType();

	/**
	 * Returns a new object of class '<em>Change Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Qualification Test Specification Type</em>'.
	 * @generated
	 */
	ChangeQualificationTestSpecificationType createChangeQualificationTestSpecificationType();

	/**
	 * Returns a new object of class '<em>Code Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Code Type</em>'.
	 * @generated
	 */
	CodeType createCodeType();

	/**
	 * Returns a new object of class '<em>Confidence Factor Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Confidence Factor Type</em>'.
	 * @generated
	 */
	ConfidenceFactorType createConfidenceFactorType();

	/**
	 * Returns a new object of class '<em>Confirm BOD Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Confirm BOD Type</em>'.
	 * @generated
	 */
	ConfirmBODType createConfirmBODType();

	/**
	 * Returns a new object of class '<em>Correction Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Correction Type</em>'.
	 * @generated
	 */
	CorrectionType createCorrectionType();

	/**
	 * Returns a new object of class '<em>Data Area Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type</em>'.
	 * @generated
	 */
	DataAreaType createDataAreaType();

	/**
	 * Returns a new object of class '<em>Data Area Type1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type1</em>'.
	 * @generated
	 */
	DataAreaType1 createDataAreaType1();

	/**
	 * Returns a new object of class '<em>Data Area Type2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type2</em>'.
	 * @generated
	 */
	DataAreaType2 createDataAreaType2();

	/**
	 * Returns a new object of class '<em>Data Area Type3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type3</em>'.
	 * @generated
	 */
	DataAreaType3 createDataAreaType3();

	/**
	 * Returns a new object of class '<em>Data Area Type4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type4</em>'.
	 * @generated
	 */
	DataAreaType4 createDataAreaType4();

	/**
	 * Returns a new object of class '<em>Data Area Type5</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type5</em>'.
	 * @generated
	 */
	DataAreaType5 createDataAreaType5();

	/**
	 * Returns a new object of class '<em>Data Area Type6</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type6</em>'.
	 * @generated
	 */
	DataAreaType6 createDataAreaType6();

	/**
	 * Returns a new object of class '<em>Data Area Type7</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type7</em>'.
	 * @generated
	 */
	DataAreaType7 createDataAreaType7();

	/**
	 * Returns a new object of class '<em>Data Area Type8</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type8</em>'.
	 * @generated
	 */
	DataAreaType8 createDataAreaType8();

	/**
	 * Returns a new object of class '<em>Data Area Type9</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type9</em>'.
	 * @generated
	 */
	DataAreaType9 createDataAreaType9();

	/**
	 * Returns a new object of class '<em>Data Area Type10</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type10</em>'.
	 * @generated
	 */
	DataAreaType10 createDataAreaType10();

	/**
	 * Returns a new object of class '<em>Data Area Type11</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type11</em>'.
	 * @generated
	 */
	DataAreaType11 createDataAreaType11();

	/**
	 * Returns a new object of class '<em>Data Area Type12</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type12</em>'.
	 * @generated
	 */
	DataAreaType12 createDataAreaType12();

	/**
	 * Returns a new object of class '<em>Data Area Type13</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type13</em>'.
	 * @generated
	 */
	DataAreaType13 createDataAreaType13();

	/**
	 * Returns a new object of class '<em>Data Area Type14</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type14</em>'.
	 * @generated
	 */
	DataAreaType14 createDataAreaType14();

	/**
	 * Returns a new object of class '<em>Data Area Type15</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type15</em>'.
	 * @generated
	 */
	DataAreaType15 createDataAreaType15();

	/**
	 * Returns a new object of class '<em>Data Area Type16</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type16</em>'.
	 * @generated
	 */
	DataAreaType16 createDataAreaType16();

	/**
	 * Returns a new object of class '<em>Data Area Type17</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type17</em>'.
	 * @generated
	 */
	DataAreaType17 createDataAreaType17();

	/**
	 * Returns a new object of class '<em>Data Area Type18</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type18</em>'.
	 * @generated
	 */
	DataAreaType18 createDataAreaType18();

	/**
	 * Returns a new object of class '<em>Data Area Type19</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type19</em>'.
	 * @generated
	 */
	DataAreaType19 createDataAreaType19();

	/**
	 * Returns a new object of class '<em>Data Area Type20</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type20</em>'.
	 * @generated
	 */
	DataAreaType20 createDataAreaType20();

	/**
	 * Returns a new object of class '<em>Data Area Type21</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type21</em>'.
	 * @generated
	 */
	DataAreaType21 createDataAreaType21();

	/**
	 * Returns a new object of class '<em>Data Area Type22</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type22</em>'.
	 * @generated
	 */
	DataAreaType22 createDataAreaType22();

	/**
	 * Returns a new object of class '<em>Data Area Type23</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type23</em>'.
	 * @generated
	 */
	DataAreaType23 createDataAreaType23();

	/**
	 * Returns a new object of class '<em>Data Area Type24</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type24</em>'.
	 * @generated
	 */
	DataAreaType24 createDataAreaType24();

	/**
	 * Returns a new object of class '<em>Data Area Type25</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type25</em>'.
	 * @generated
	 */
	DataAreaType25 createDataAreaType25();

	/**
	 * Returns a new object of class '<em>Data Area Type26</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type26</em>'.
	 * @generated
	 */
	DataAreaType26 createDataAreaType26();

	/**
	 * Returns a new object of class '<em>Data Area Type27</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type27</em>'.
	 * @generated
	 */
	DataAreaType27 createDataAreaType27();

	/**
	 * Returns a new object of class '<em>Data Area Type28</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type28</em>'.
	 * @generated
	 */
	DataAreaType28 createDataAreaType28();

	/**
	 * Returns a new object of class '<em>Data Area Type29</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type29</em>'.
	 * @generated
	 */
	DataAreaType29 createDataAreaType29();

	/**
	 * Returns a new object of class '<em>Data Area Type30</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type30</em>'.
	 * @generated
	 */
	DataAreaType30 createDataAreaType30();

	/**
	 * Returns a new object of class '<em>Data Area Type31</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type31</em>'.
	 * @generated
	 */
	DataAreaType31 createDataAreaType31();

	/**
	 * Returns a new object of class '<em>Data Area Type32</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type32</em>'.
	 * @generated
	 */
	DataAreaType32 createDataAreaType32();

	/**
	 * Returns a new object of class '<em>Data Area Type33</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type33</em>'.
	 * @generated
	 */
	DataAreaType33 createDataAreaType33();

	/**
	 * Returns a new object of class '<em>Data Area Type34</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type34</em>'.
	 * @generated
	 */
	DataAreaType34 createDataAreaType34();

	/**
	 * Returns a new object of class '<em>Data Area Type35</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type35</em>'.
	 * @generated
	 */
	DataAreaType35 createDataAreaType35();

	/**
	 * Returns a new object of class '<em>Data Area Type36</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type36</em>'.
	 * @generated
	 */
	DataAreaType36 createDataAreaType36();

	/**
	 * Returns a new object of class '<em>Data Area Type37</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type37</em>'.
	 * @generated
	 */
	DataAreaType37 createDataAreaType37();

	/**
	 * Returns a new object of class '<em>Data Area Type38</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type38</em>'.
	 * @generated
	 */
	DataAreaType38 createDataAreaType38();

	/**
	 * Returns a new object of class '<em>Data Area Type39</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type39</em>'.
	 * @generated
	 */
	DataAreaType39 createDataAreaType39();

	/**
	 * Returns a new object of class '<em>Data Area Type40</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type40</em>'.
	 * @generated
	 */
	DataAreaType40 createDataAreaType40();

	/**
	 * Returns a new object of class '<em>Data Area Type41</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type41</em>'.
	 * @generated
	 */
	DataAreaType41 createDataAreaType41();

	/**
	 * Returns a new object of class '<em>Data Area Type42</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type42</em>'.
	 * @generated
	 */
	DataAreaType42 createDataAreaType42();

	/**
	 * Returns a new object of class '<em>Data Area Type43</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type43</em>'.
	 * @generated
	 */
	DataAreaType43 createDataAreaType43();

	/**
	 * Returns a new object of class '<em>Data Area Type44</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type44</em>'.
	 * @generated
	 */
	DataAreaType44 createDataAreaType44();

	/**
	 * Returns a new object of class '<em>Data Area Type45</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type45</em>'.
	 * @generated
	 */
	DataAreaType45 createDataAreaType45();

	/**
	 * Returns a new object of class '<em>Data Area Type46</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type46</em>'.
	 * @generated
	 */
	DataAreaType46 createDataAreaType46();

	/**
	 * Returns a new object of class '<em>Data Area Type47</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type47</em>'.
	 * @generated
	 */
	DataAreaType47 createDataAreaType47();

	/**
	 * Returns a new object of class '<em>Data Area Type48</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type48</em>'.
	 * @generated
	 */
	DataAreaType48 createDataAreaType48();

	/**
	 * Returns a new object of class '<em>Data Area Type49</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type49</em>'.
	 * @generated
	 */
	DataAreaType49 createDataAreaType49();

	/**
	 * Returns a new object of class '<em>Data Area Type50</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type50</em>'.
	 * @generated
	 */
	DataAreaType50 createDataAreaType50();

	/**
	 * Returns a new object of class '<em>Data Area Type51</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type51</em>'.
	 * @generated
	 */
	DataAreaType51 createDataAreaType51();

	/**
	 * Returns a new object of class '<em>Data Area Type52</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type52</em>'.
	 * @generated
	 */
	DataAreaType52 createDataAreaType52();

	/**
	 * Returns a new object of class '<em>Data Area Type53</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type53</em>'.
	 * @generated
	 */
	DataAreaType53 createDataAreaType53();

	/**
	 * Returns a new object of class '<em>Data Area Type54</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type54</em>'.
	 * @generated
	 */
	DataAreaType54 createDataAreaType54();

	/**
	 * Returns a new object of class '<em>Data Area Type55</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type55</em>'.
	 * @generated
	 */
	DataAreaType55 createDataAreaType55();

	/**
	 * Returns a new object of class '<em>Data Area Type56</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type56</em>'.
	 * @generated
	 */
	DataAreaType56 createDataAreaType56();

	/**
	 * Returns a new object of class '<em>Data Area Type57</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type57</em>'.
	 * @generated
	 */
	DataAreaType57 createDataAreaType57();

	/**
	 * Returns a new object of class '<em>Data Area Type58</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type58</em>'.
	 * @generated
	 */
	DataAreaType58 createDataAreaType58();

	/**
	 * Returns a new object of class '<em>Data Area Type59</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type59</em>'.
	 * @generated
	 */
	DataAreaType59 createDataAreaType59();

	/**
	 * Returns a new object of class '<em>Data Area Type60</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type60</em>'.
	 * @generated
	 */
	DataAreaType60 createDataAreaType60();

	/**
	 * Returns a new object of class '<em>Data Area Type61</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type61</em>'.
	 * @generated
	 */
	DataAreaType61 createDataAreaType61();

	/**
	 * Returns a new object of class '<em>Data Area Type62</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type62</em>'.
	 * @generated
	 */
	DataAreaType62 createDataAreaType62();

	/**
	 * Returns a new object of class '<em>Data Area Type63</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type63</em>'.
	 * @generated
	 */
	DataAreaType63 createDataAreaType63();

	/**
	 * Returns a new object of class '<em>Data Area Type64</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type64</em>'.
	 * @generated
	 */
	DataAreaType64 createDataAreaType64();

	/**
	 * Returns a new object of class '<em>Data Area Type65</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type65</em>'.
	 * @generated
	 */
	DataAreaType65 createDataAreaType65();

	/**
	 * Returns a new object of class '<em>Data Area Type66</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type66</em>'.
	 * @generated
	 */
	DataAreaType66 createDataAreaType66();

	/**
	 * Returns a new object of class '<em>Data Area Type67</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type67</em>'.
	 * @generated
	 */
	DataAreaType67 createDataAreaType67();

	/**
	 * Returns a new object of class '<em>Data Area Type68</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type68</em>'.
	 * @generated
	 */
	DataAreaType68 createDataAreaType68();

	/**
	 * Returns a new object of class '<em>Data Area Type69</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type69</em>'.
	 * @generated
	 */
	DataAreaType69 createDataAreaType69();

	/**
	 * Returns a new object of class '<em>Data Area Type70</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type70</em>'.
	 * @generated
	 */
	DataAreaType70 createDataAreaType70();

	/**
	 * Returns a new object of class '<em>Data Area Type71</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type71</em>'.
	 * @generated
	 */
	DataAreaType71 createDataAreaType71();

	/**
	 * Returns a new object of class '<em>Data Area Type72</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type72</em>'.
	 * @generated
	 */
	DataAreaType72 createDataAreaType72();

	/**
	 * Returns a new object of class '<em>Data Area Type73</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type73</em>'.
	 * @generated
	 */
	DataAreaType73 createDataAreaType73();

	/**
	 * Returns a new object of class '<em>Data Area Type74</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type74</em>'.
	 * @generated
	 */
	DataAreaType74 createDataAreaType74();

	/**
	 * Returns a new object of class '<em>Data Area Type75</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type75</em>'.
	 * @generated
	 */
	DataAreaType75 createDataAreaType75();

	/**
	 * Returns a new object of class '<em>Data Area Type76</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type76</em>'.
	 * @generated
	 */
	DataAreaType76 createDataAreaType76();

	/**
	 * Returns a new object of class '<em>Data Area Type77</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type77</em>'.
	 * @generated
	 */
	DataAreaType77 createDataAreaType77();

	/**
	 * Returns a new object of class '<em>Data Area Type78</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type78</em>'.
	 * @generated
	 */
	DataAreaType78 createDataAreaType78();

	/**
	 * Returns a new object of class '<em>Data Area Type79</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type79</em>'.
	 * @generated
	 */
	DataAreaType79 createDataAreaType79();

	/**
	 * Returns a new object of class '<em>Data Area Type80</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type80</em>'.
	 * @generated
	 */
	DataAreaType80 createDataAreaType80();

	/**
	 * Returns a new object of class '<em>Data Area Type81</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type81</em>'.
	 * @generated
	 */
	DataAreaType81 createDataAreaType81();

	/**
	 * Returns a new object of class '<em>Data Area Type82</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type82</em>'.
	 * @generated
	 */
	DataAreaType82 createDataAreaType82();

	/**
	 * Returns a new object of class '<em>Data Area Type83</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type83</em>'.
	 * @generated
	 */
	DataAreaType83 createDataAreaType83();

	/**
	 * Returns a new object of class '<em>Data Area Type84</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type84</em>'.
	 * @generated
	 */
	DataAreaType84 createDataAreaType84();

	/**
	 * Returns a new object of class '<em>Data Area Type85</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type85</em>'.
	 * @generated
	 */
	DataAreaType85 createDataAreaType85();

	/**
	 * Returns a new object of class '<em>Data Area Type86</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type86</em>'.
	 * @generated
	 */
	DataAreaType86 createDataAreaType86();

	/**
	 * Returns a new object of class '<em>Data Area Type87</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type87</em>'.
	 * @generated
	 */
	DataAreaType87 createDataAreaType87();

	/**
	 * Returns a new object of class '<em>Data Area Type88</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type88</em>'.
	 * @generated
	 */
	DataAreaType88 createDataAreaType88();

	/**
	 * Returns a new object of class '<em>Data Area Type89</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type89</em>'.
	 * @generated
	 */
	DataAreaType89 createDataAreaType89();

	/**
	 * Returns a new object of class '<em>Data Area Type90</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type90</em>'.
	 * @generated
	 */
	DataAreaType90 createDataAreaType90();

	/**
	 * Returns a new object of class '<em>Data Area Type91</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type91</em>'.
	 * @generated
	 */
	DataAreaType91 createDataAreaType91();

	/**
	 * Returns a new object of class '<em>Data Area Type92</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type92</em>'.
	 * @generated
	 */
	DataAreaType92 createDataAreaType92();

	/**
	 * Returns a new object of class '<em>Data Area Type93</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type93</em>'.
	 * @generated
	 */
	DataAreaType93 createDataAreaType93();

	/**
	 * Returns a new object of class '<em>Data Area Type94</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type94</em>'.
	 * @generated
	 */
	DataAreaType94 createDataAreaType94();

	/**
	 * Returns a new object of class '<em>Data Area Type95</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type95</em>'.
	 * @generated
	 */
	DataAreaType95 createDataAreaType95();

	/**
	 * Returns a new object of class '<em>Data Area Type96</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type96</em>'.
	 * @generated
	 */
	DataAreaType96 createDataAreaType96();

	/**
	 * Returns a new object of class '<em>Data Area Type97</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type97</em>'.
	 * @generated
	 */
	DataAreaType97 createDataAreaType97();

	/**
	 * Returns a new object of class '<em>Data Area Type98</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type98</em>'.
	 * @generated
	 */
	DataAreaType98 createDataAreaType98();

	/**
	 * Returns a new object of class '<em>Data Area Type99</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type99</em>'.
	 * @generated
	 */
	DataAreaType99 createDataAreaType99();

	/**
	 * Returns a new object of class '<em>Data Area Type100</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type100</em>'.
	 * @generated
	 */
	DataAreaType100 createDataAreaType100();

	/**
	 * Returns a new object of class '<em>Data Area Type101</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type101</em>'.
	 * @generated
	 */
	DataAreaType101 createDataAreaType101();

	/**
	 * Returns a new object of class '<em>Data Area Type102</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type102</em>'.
	 * @generated
	 */
	DataAreaType102 createDataAreaType102();

	/**
	 * Returns a new object of class '<em>Data Area Type103</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type103</em>'.
	 * @generated
	 */
	DataAreaType103 createDataAreaType103();

	/**
	 * Returns a new object of class '<em>Data Area Type104</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type104</em>'.
	 * @generated
	 */
	DataAreaType104 createDataAreaType104();

	/**
	 * Returns a new object of class '<em>Data Area Type105</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type105</em>'.
	 * @generated
	 */
	DataAreaType105 createDataAreaType105();

	/**
	 * Returns a new object of class '<em>Data Area Type106</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type106</em>'.
	 * @generated
	 */
	DataAreaType106 createDataAreaType106();

	/**
	 * Returns a new object of class '<em>Data Area Type107</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type107</em>'.
	 * @generated
	 */
	DataAreaType107 createDataAreaType107();

	/**
	 * Returns a new object of class '<em>Data Area Type108</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type108</em>'.
	 * @generated
	 */
	DataAreaType108 createDataAreaType108();

	/**
	 * Returns a new object of class '<em>Data Area Type109</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type109</em>'.
	 * @generated
	 */
	DataAreaType109 createDataAreaType109();

	/**
	 * Returns a new object of class '<em>Data Area Type110</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type110</em>'.
	 * @generated
	 */
	DataAreaType110 createDataAreaType110();

	/**
	 * Returns a new object of class '<em>Data Area Type111</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type111</em>'.
	 * @generated
	 */
	DataAreaType111 createDataAreaType111();

	/**
	 * Returns a new object of class '<em>Data Area Type112</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type112</em>'.
	 * @generated
	 */
	DataAreaType112 createDataAreaType112();

	/**
	 * Returns a new object of class '<em>Data Area Type113</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type113</em>'.
	 * @generated
	 */
	DataAreaType113 createDataAreaType113();

	/**
	 * Returns a new object of class '<em>Data Area Type114</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type114</em>'.
	 * @generated
	 */
	DataAreaType114 createDataAreaType114();

	/**
	 * Returns a new object of class '<em>Data Area Type115</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type115</em>'.
	 * @generated
	 */
	DataAreaType115 createDataAreaType115();

	/**
	 * Returns a new object of class '<em>Data Area Type116</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type116</em>'.
	 * @generated
	 */
	DataAreaType116 createDataAreaType116();

	/**
	 * Returns a new object of class '<em>Data Area Type117</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type117</em>'.
	 * @generated
	 */
	DataAreaType117 createDataAreaType117();

	/**
	 * Returns a new object of class '<em>Data Area Type118</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type118</em>'.
	 * @generated
	 */
	DataAreaType118 createDataAreaType118();

	/**
	 * Returns a new object of class '<em>Data Area Type119</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type119</em>'.
	 * @generated
	 */
	DataAreaType119 createDataAreaType119();

	/**
	 * Returns a new object of class '<em>Data Area Type120</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type120</em>'.
	 * @generated
	 */
	DataAreaType120 createDataAreaType120();

	/**
	 * Returns a new object of class '<em>Data Area Type121</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type121</em>'.
	 * @generated
	 */
	DataAreaType121 createDataAreaType121();

	/**
	 * Returns a new object of class '<em>Data Area Type122</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type122</em>'.
	 * @generated
	 */
	DataAreaType122 createDataAreaType122();

	/**
	 * Returns a new object of class '<em>Data Area Type123</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type123</em>'.
	 * @generated
	 */
	DataAreaType123 createDataAreaType123();

	/**
	 * Returns a new object of class '<em>Data Area Type124</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type124</em>'.
	 * @generated
	 */
	DataAreaType124 createDataAreaType124();

	/**
	 * Returns a new object of class '<em>Data Area Type125</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type125</em>'.
	 * @generated
	 */
	DataAreaType125 createDataAreaType125();

	/**
	 * Returns a new object of class '<em>Data Area Type126</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type126</em>'.
	 * @generated
	 */
	DataAreaType126 createDataAreaType126();

	/**
	 * Returns a new object of class '<em>Data Area Type127</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type127</em>'.
	 * @generated
	 */
	DataAreaType127 createDataAreaType127();

	/**
	 * Returns a new object of class '<em>Data Area Type128</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type128</em>'.
	 * @generated
	 */
	DataAreaType128 createDataAreaType128();

	/**
	 * Returns a new object of class '<em>Data Area Type129</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type129</em>'.
	 * @generated
	 */
	DataAreaType129 createDataAreaType129();

	/**
	 * Returns a new object of class '<em>Data Area Type130</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type130</em>'.
	 * @generated
	 */
	DataAreaType130 createDataAreaType130();

	/**
	 * Returns a new object of class '<em>Data Area Type131</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type131</em>'.
	 * @generated
	 */
	DataAreaType131 createDataAreaType131();

	/**
	 * Returns a new object of class '<em>Data Area Type132</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type132</em>'.
	 * @generated
	 */
	DataAreaType132 createDataAreaType132();

	/**
	 * Returns a new object of class '<em>Data Area Type133</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type133</em>'.
	 * @generated
	 */
	DataAreaType133 createDataAreaType133();

	/**
	 * Returns a new object of class '<em>Data Area Type134</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type134</em>'.
	 * @generated
	 */
	DataAreaType134 createDataAreaType134();

	/**
	 * Returns a new object of class '<em>Data Area Type135</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type135</em>'.
	 * @generated
	 */
	DataAreaType135 createDataAreaType135();

	/**
	 * Returns a new object of class '<em>Data Area Type136</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type136</em>'.
	 * @generated
	 */
	DataAreaType136 createDataAreaType136();

	/**
	 * Returns a new object of class '<em>Data Area Type137</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type137</em>'.
	 * @generated
	 */
	DataAreaType137 createDataAreaType137();

	/**
	 * Returns a new object of class '<em>Data Area Type138</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type138</em>'.
	 * @generated
	 */
	DataAreaType138 createDataAreaType138();

	/**
	 * Returns a new object of class '<em>Data Area Type139</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type139</em>'.
	 * @generated
	 */
	DataAreaType139 createDataAreaType139();

	/**
	 * Returns a new object of class '<em>Data Area Type140</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type140</em>'.
	 * @generated
	 */
	DataAreaType140 createDataAreaType140();

	/**
	 * Returns a new object of class '<em>Data Area Type141</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type141</em>'.
	 * @generated
	 */
	DataAreaType141 createDataAreaType141();

	/**
	 * Returns a new object of class '<em>Data Area Type142</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type142</em>'.
	 * @generated
	 */
	DataAreaType142 createDataAreaType142();

	/**
	 * Returns a new object of class '<em>Data Area Type143</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type143</em>'.
	 * @generated
	 */
	DataAreaType143 createDataAreaType143();

	/**
	 * Returns a new object of class '<em>Data Area Type144</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type144</em>'.
	 * @generated
	 */
	DataAreaType144 createDataAreaType144();

	/**
	 * Returns a new object of class '<em>Data Area Type145</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type145</em>'.
	 * @generated
	 */
	DataAreaType145 createDataAreaType145();

	/**
	 * Returns a new object of class '<em>Data Area Type146</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type146</em>'.
	 * @generated
	 */
	DataAreaType146 createDataAreaType146();

	/**
	 * Returns a new object of class '<em>Data Area Type147</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type147</em>'.
	 * @generated
	 */
	DataAreaType147 createDataAreaType147();

	/**
	 * Returns a new object of class '<em>Data Area Type148</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type148</em>'.
	 * @generated
	 */
	DataAreaType148 createDataAreaType148();

	/**
	 * Returns a new object of class '<em>Data Area Type149</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type149</em>'.
	 * @generated
	 */
	DataAreaType149 createDataAreaType149();

	/**
	 * Returns a new object of class '<em>Data Area Type150</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type150</em>'.
	 * @generated
	 */
	DataAreaType150 createDataAreaType150();

	/**
	 * Returns a new object of class '<em>Data Area Type151</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type151</em>'.
	 * @generated
	 */
	DataAreaType151 createDataAreaType151();

	/**
	 * Returns a new object of class '<em>Data Area Type152</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type152</em>'.
	 * @generated
	 */
	DataAreaType152 createDataAreaType152();

	/**
	 * Returns a new object of class '<em>Data Area Type153</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type153</em>'.
	 * @generated
	 */
	DataAreaType153 createDataAreaType153();

	/**
	 * Returns a new object of class '<em>Data Area Type154</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type154</em>'.
	 * @generated
	 */
	DataAreaType154 createDataAreaType154();

	/**
	 * Returns a new object of class '<em>Data Area Type155</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type155</em>'.
	 * @generated
	 */
	DataAreaType155 createDataAreaType155();

	/**
	 * Returns a new object of class '<em>Data Area Type156</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type156</em>'.
	 * @generated
	 */
	DataAreaType156 createDataAreaType156();

	/**
	 * Returns a new object of class '<em>Data Area Type157</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type157</em>'.
	 * @generated
	 */
	DataAreaType157 createDataAreaType157();

	/**
	 * Returns a new object of class '<em>Data Area Type158</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type158</em>'.
	 * @generated
	 */
	DataAreaType158 createDataAreaType158();

	/**
	 * Returns a new object of class '<em>Data Area Type159</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type159</em>'.
	 * @generated
	 */
	DataAreaType159 createDataAreaType159();

	/**
	 * Returns a new object of class '<em>Data Area Type160</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type160</em>'.
	 * @generated
	 */
	DataAreaType160 createDataAreaType160();

	/**
	 * Returns a new object of class '<em>Data Area Type161</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type161</em>'.
	 * @generated
	 */
	DataAreaType161 createDataAreaType161();

	/**
	 * Returns a new object of class '<em>Data Area Type162</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type162</em>'.
	 * @generated
	 */
	DataAreaType162 createDataAreaType162();

	/**
	 * Returns a new object of class '<em>Data Area Type163</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type163</em>'.
	 * @generated
	 */
	DataAreaType163 createDataAreaType163();

	/**
	 * Returns a new object of class '<em>Data Area Type164</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type164</em>'.
	 * @generated
	 */
	DataAreaType164 createDataAreaType164();

	/**
	 * Returns a new object of class '<em>Data Area Type165</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type165</em>'.
	 * @generated
	 */
	DataAreaType165 createDataAreaType165();

	/**
	 * Returns a new object of class '<em>Data Area Type166</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type166</em>'.
	 * @generated
	 */
	DataAreaType166 createDataAreaType166();

	/**
	 * Returns a new object of class '<em>Data Area Type167</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type167</em>'.
	 * @generated
	 */
	DataAreaType167 createDataAreaType167();

	/**
	 * Returns a new object of class '<em>Data Area Type168</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type168</em>'.
	 * @generated
	 */
	DataAreaType168 createDataAreaType168();

	/**
	 * Returns a new object of class '<em>Data Area Type169</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type169</em>'.
	 * @generated
	 */
	DataAreaType169 createDataAreaType169();

	/**
	 * Returns a new object of class '<em>Data Area Type170</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type170</em>'.
	 * @generated
	 */
	DataAreaType170 createDataAreaType170();

	/**
	 * Returns a new object of class '<em>Data Area Type171</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type171</em>'.
	 * @generated
	 */
	DataAreaType171 createDataAreaType171();

	/**
	 * Returns a new object of class '<em>Data Area Type172</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type172</em>'.
	 * @generated
	 */
	DataAreaType172 createDataAreaType172();

	/**
	 * Returns a new object of class '<em>Data Area Type173</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type173</em>'.
	 * @generated
	 */
	DataAreaType173 createDataAreaType173();

	/**
	 * Returns a new object of class '<em>Data Area Type174</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type174</em>'.
	 * @generated
	 */
	DataAreaType174 createDataAreaType174();

	/**
	 * Returns a new object of class '<em>Data Area Type175</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type175</em>'.
	 * @generated
	 */
	DataAreaType175 createDataAreaType175();

	/**
	 * Returns a new object of class '<em>Data Area Type176</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type176</em>'.
	 * @generated
	 */
	DataAreaType176 createDataAreaType176();

	/**
	 * Returns a new object of class '<em>Data Area Type177</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type177</em>'.
	 * @generated
	 */
	DataAreaType177 createDataAreaType177();

	/**
	 * Returns a new object of class '<em>Data Area Type178</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type178</em>'.
	 * @generated
	 */
	DataAreaType178 createDataAreaType178();

	/**
	 * Returns a new object of class '<em>Data Area Type179</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type179</em>'.
	 * @generated
	 */
	DataAreaType179 createDataAreaType179();

	/**
	 * Returns a new object of class '<em>Data Area Type180</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type180</em>'.
	 * @generated
	 */
	DataAreaType180 createDataAreaType180();

	/**
	 * Returns a new object of class '<em>Data Area Type181</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type181</em>'.
	 * @generated
	 */
	DataAreaType181 createDataAreaType181();

	/**
	 * Returns a new object of class '<em>Data Area Type182</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type182</em>'.
	 * @generated
	 */
	DataAreaType182 createDataAreaType182();

	/**
	 * Returns a new object of class '<em>Data Area Type183</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type183</em>'.
	 * @generated
	 */
	DataAreaType183 createDataAreaType183();

	/**
	 * Returns a new object of class '<em>Data Area Type184</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type184</em>'.
	 * @generated
	 */
	DataAreaType184 createDataAreaType184();

	/**
	 * Returns a new object of class '<em>Data Area Type185</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type185</em>'.
	 * @generated
	 */
	DataAreaType185 createDataAreaType185();

	/**
	 * Returns a new object of class '<em>Data Area Type186</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type186</em>'.
	 * @generated
	 */
	DataAreaType186 createDataAreaType186();

	/**
	 * Returns a new object of class '<em>Data Area Type187</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type187</em>'.
	 * @generated
	 */
	DataAreaType187 createDataAreaType187();

	/**
	 * Returns a new object of class '<em>Data Area Type188</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type188</em>'.
	 * @generated
	 */
	DataAreaType188 createDataAreaType188();

	/**
	 * Returns a new object of class '<em>Data Area Type189</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type189</em>'.
	 * @generated
	 */
	DataAreaType189 createDataAreaType189();

	/**
	 * Returns a new object of class '<em>Data Area Type190</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type190</em>'.
	 * @generated
	 */
	DataAreaType190 createDataAreaType190();

	/**
	 * Returns a new object of class '<em>Data Area Type191</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type191</em>'.
	 * @generated
	 */
	DataAreaType191 createDataAreaType191();

	/**
	 * Returns a new object of class '<em>Data Area Type192</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type192</em>'.
	 * @generated
	 */
	DataAreaType192 createDataAreaType192();

	/**
	 * Returns a new object of class '<em>Data Area Type193</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type193</em>'.
	 * @generated
	 */
	DataAreaType193 createDataAreaType193();

	/**
	 * Returns a new object of class '<em>Data Area Type194</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type194</em>'.
	 * @generated
	 */
	DataAreaType194 createDataAreaType194();

	/**
	 * Returns a new object of class '<em>Data Area Type195</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type195</em>'.
	 * @generated
	 */
	DataAreaType195 createDataAreaType195();

	/**
	 * Returns a new object of class '<em>Data Area Type196</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type196</em>'.
	 * @generated
	 */
	DataAreaType196 createDataAreaType196();

	/**
	 * Returns a new object of class '<em>Data Area Type197</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type197</em>'.
	 * @generated
	 */
	DataAreaType197 createDataAreaType197();

	/**
	 * Returns a new object of class '<em>Data Area Type198</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type198</em>'.
	 * @generated
	 */
	DataAreaType198 createDataAreaType198();

	/**
	 * Returns a new object of class '<em>Data Area Type199</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type199</em>'.
	 * @generated
	 */
	DataAreaType199 createDataAreaType199();

	/**
	 * Returns a new object of class '<em>Data Area Type200</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type200</em>'.
	 * @generated
	 */
	DataAreaType200 createDataAreaType200();

	/**
	 * Returns a new object of class '<em>Data Area Type201</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type201</em>'.
	 * @generated
	 */
	DataAreaType201 createDataAreaType201();

	/**
	 * Returns a new object of class '<em>Data Area Type202</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type202</em>'.
	 * @generated
	 */
	DataAreaType202 createDataAreaType202();

	/**
	 * Returns a new object of class '<em>Data Area Type203</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type203</em>'.
	 * @generated
	 */
	DataAreaType203 createDataAreaType203();

	/**
	 * Returns a new object of class '<em>Data Area Type204</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type204</em>'.
	 * @generated
	 */
	DataAreaType204 createDataAreaType204();

	/**
	 * Returns a new object of class '<em>Data Area Type205</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type205</em>'.
	 * @generated
	 */
	DataAreaType205 createDataAreaType205();

	/**
	 * Returns a new object of class '<em>Data Area Type206</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type206</em>'.
	 * @generated
	 */
	DataAreaType206 createDataAreaType206();

	/**
	 * Returns a new object of class '<em>Data Area Type207</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type207</em>'.
	 * @generated
	 */
	DataAreaType207 createDataAreaType207();

	/**
	 * Returns a new object of class '<em>Data Area Type208</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Area Type208</em>'.
	 * @generated
	 */
	DataAreaType208 createDataAreaType208();

	/**
	 * Returns a new object of class '<em>Data Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Type1 Type</em>'.
	 * @generated
	 */
	DataType1Type createDataType1Type();

	/**
	 * Returns a new object of class '<em>Data Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Type Type</em>'.
	 * @generated
	 */
	DataTypeType createDataTypeType();

	/**
	 * Returns a new object of class '<em>Date Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Date Time Type</em>'.
	 * @generated
	 */
	DateTimeType createDateTimeType();

	/**
	 * Returns a new object of class '<em>Dependency1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dependency1 Type</em>'.
	 * @generated
	 */
	Dependency1Type createDependency1Type();

	/**
	 * Returns a new object of class '<em>Dependency Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dependency Type</em>'.
	 * @generated
	 */
	DependencyType createDependencyType();

	/**
	 * Returns a new object of class '<em>Description Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Description Type</em>'.
	 * @generated
	 */
	DescriptionType createDescriptionType();

	/**
	 * Returns a new object of class '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Document Root</em>'.
	 * @generated
	 */
	DocumentRoot createDocumentRoot();

	/**
	 * Returns a new object of class '<em>Earliest Start Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Earliest Start Time Type</em>'.
	 * @generated
	 */
	EarliestStartTimeType createEarliestStartTimeType();

	/**
	 * Returns a new object of class '<em>End Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>End Time Type</em>'.
	 * @generated
	 */
	EndTimeType createEndTimeType();

	/**
	 * Returns a new object of class '<em>Equipment Asset Mapping Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Asset Mapping Type</em>'.
	 * @generated
	 */
	EquipmentAssetMappingType createEquipmentAssetMappingType();

	/**
	 * Returns a new object of class '<em>Equipment Capability Test Specification ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Capability Test Specification ID Type</em>'.
	 * @generated
	 */
	EquipmentCapabilityTestSpecificationIDType createEquipmentCapabilityTestSpecificationIDType();

	/**
	 * Returns a new object of class '<em>Equipment Capability Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Capability Test Specification Type</em>'.
	 * @generated
	 */
	EquipmentCapabilityTestSpecificationType createEquipmentCapabilityTestSpecificationType();

	/**
	 * Returns a new object of class '<em>Equipment Class ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Class ID Type</em>'.
	 * @generated
	 */
	EquipmentClassIDType createEquipmentClassIDType();

	/**
	 * Returns a new object of class '<em>Equipment Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Class Property Type</em>'.
	 * @generated
	 */
	EquipmentClassPropertyType createEquipmentClassPropertyType();

	/**
	 * Returns a new object of class '<em>Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Class Type</em>'.
	 * @generated
	 */
	EquipmentClassType createEquipmentClassType();

	/**
	 * Returns a new object of class '<em>Equipment Element Level1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Element Level1 Type</em>'.
	 * @generated
	 */
	EquipmentElementLevel1Type createEquipmentElementLevel1Type();

	/**
	 * Returns a new object of class '<em>Equipment Element Level Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Element Level Type</em>'.
	 * @generated
	 */
	EquipmentElementLevelType createEquipmentElementLevelType();

	/**
	 * Returns a new object of class '<em>Equipment ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment ID Type</em>'.
	 * @generated
	 */
	EquipmentIDType createEquipmentIDType();

	/**
	 * Returns a new object of class '<em>Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Information Type</em>'.
	 * @generated
	 */
	EquipmentInformationType createEquipmentInformationType();

	/**
	 * Returns a new object of class '<em>Equipment Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Property Type</em>'.
	 * @generated
	 */
	EquipmentPropertyType createEquipmentPropertyType();

	/**
	 * Returns a new object of class '<em>Equipment Segment Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Segment Specification Property Type</em>'.
	 * @generated
	 */
	EquipmentSegmentSpecificationPropertyType createEquipmentSegmentSpecificationPropertyType();

	/**
	 * Returns a new object of class '<em>Equipment Segment Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Segment Specification Type</em>'.
	 * @generated
	 */
	EquipmentSegmentSpecificationType createEquipmentSegmentSpecificationType();

	/**
	 * Returns a new object of class '<em>Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Type</em>'.
	 * @generated
	 */
	EquipmentType createEquipmentType();

	/**
	 * Returns a new object of class '<em>Equipment Use Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equipment Use Type</em>'.
	 * @generated
	 */
	EquipmentUseType createEquipmentUseType();

	/**
	 * Returns a new object of class '<em>Expiration Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expiration Time Type</em>'.
	 * @generated
	 */
	ExpirationTimeType createExpirationTimeType();

	/**
	 * Returns a new object of class '<em>Get Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Equipment Capability Test Spec Type</em>'.
	 * @generated
	 */
	GetEquipmentCapabilityTestSpecType createGetEquipmentCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Get Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Equipment Class Type</em>'.
	 * @generated
	 */
	GetEquipmentClassType createGetEquipmentClassType();

	/**
	 * Returns a new object of class '<em>Get Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Equipment Information Type</em>'.
	 * @generated
	 */
	GetEquipmentInformationType createGetEquipmentInformationType();

	/**
	 * Returns a new object of class '<em>Get Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Equipment Type</em>'.
	 * @generated
	 */
	GetEquipmentType createGetEquipmentType();

	/**
	 * Returns a new object of class '<em>Get Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Material Class Type</em>'.
	 * @generated
	 */
	GetMaterialClassType createGetMaterialClassType();

	/**
	 * Returns a new object of class '<em>Get Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Material Definition Type</em>'.
	 * @generated
	 */
	GetMaterialDefinitionType createGetMaterialDefinitionType();

	/**
	 * Returns a new object of class '<em>Get Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Material Information Type</em>'.
	 * @generated
	 */
	GetMaterialInformationType createGetMaterialInformationType();

	/**
	 * Returns a new object of class '<em>Get Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Material Lot Type</em>'.
	 * @generated
	 */
	GetMaterialLotType createGetMaterialLotType();

	/**
	 * Returns a new object of class '<em>Get Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Material Sub Lot Type</em>'.
	 * @generated
	 */
	GetMaterialSubLotType createGetMaterialSubLotType();

	/**
	 * Returns a new object of class '<em>Get Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Material Test Spec Type</em>'.
	 * @generated
	 */
	GetMaterialTestSpecType createGetMaterialTestSpecType();

	/**
	 * Returns a new object of class '<em>Get Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Operations Capability Information Type</em>'.
	 * @generated
	 */
	GetOperationsCapabilityInformationType createGetOperationsCapabilityInformationType();

	/**
	 * Returns a new object of class '<em>Get Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Operations Capability Type</em>'.
	 * @generated
	 */
	GetOperationsCapabilityType createGetOperationsCapabilityType();

	/**
	 * Returns a new object of class '<em>Get Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Operations Definition Information Type</em>'.
	 * @generated
	 */
	GetOperationsDefinitionInformationType createGetOperationsDefinitionInformationType();

	/**
	 * Returns a new object of class '<em>Get Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Operations Definition Type</em>'.
	 * @generated
	 */
	GetOperationsDefinitionType createGetOperationsDefinitionType();

	/**
	 * Returns a new object of class '<em>Get Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Operations Performance Type</em>'.
	 * @generated
	 */
	GetOperationsPerformanceType createGetOperationsPerformanceType();

	/**
	 * Returns a new object of class '<em>Get Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Operations Schedule Type</em>'.
	 * @generated
	 */
	GetOperationsScheduleType createGetOperationsScheduleType();

	/**
	 * Returns a new object of class '<em>Get Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Personnel Class Type</em>'.
	 * @generated
	 */
	GetPersonnelClassType createGetPersonnelClassType();

	/**
	 * Returns a new object of class '<em>Get Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Personnel Information Type</em>'.
	 * @generated
	 */
	GetPersonnelInformationType createGetPersonnelInformationType();

	/**
	 * Returns a new object of class '<em>Get Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Person Type</em>'.
	 * @generated
	 */
	GetPersonType createGetPersonType();

	/**
	 * Returns a new object of class '<em>Get Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Physical Asset Capability Test Spec Type</em>'.
	 * @generated
	 */
	GetPhysicalAssetCapabilityTestSpecType createGetPhysicalAssetCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Get Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Physical Asset Class Type</em>'.
	 * @generated
	 */
	GetPhysicalAssetClassType createGetPhysicalAssetClassType();

	/**
	 * Returns a new object of class '<em>Get Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Physical Asset Information Type</em>'.
	 * @generated
	 */
	GetPhysicalAssetInformationType createGetPhysicalAssetInformationType();

	/**
	 * Returns a new object of class '<em>Get Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Physical Asset Type</em>'.
	 * @generated
	 */
	GetPhysicalAssetType createGetPhysicalAssetType();

	/**
	 * Returns a new object of class '<em>Get Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Process Segment Information Type</em>'.
	 * @generated
	 */
	GetProcessSegmentInformationType createGetProcessSegmentInformationType();

	/**
	 * Returns a new object of class '<em>Get Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Process Segment Type</em>'.
	 * @generated
	 */
	GetProcessSegmentType createGetProcessSegmentType();

	/**
	 * Returns a new object of class '<em>Get Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Qualification Test Specification Type</em>'.
	 * @generated
	 */
	GetQualificationTestSpecificationType createGetQualificationTestSpecificationType();

	/**
	 * Returns a new object of class '<em>Hierarchy Scope Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hierarchy Scope Type</em>'.
	 * @generated
	 */
	HierarchyScopeType createHierarchyScopeType();

	/**
	 * Returns a new object of class '<em>Identifier Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Identifier Type</em>'.
	 * @generated
	 */
	IdentifierType createIdentifierType();

	/**
	 * Returns a new object of class '<em>Job Order Command1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Job Order Command1 Type</em>'.
	 * @generated
	 */
	JobOrderCommand1Type createJobOrderCommand1Type();

	/**
	 * Returns a new object of class '<em>Job Order Command Rule Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Job Order Command Rule Type</em>'.
	 * @generated
	 */
	JobOrderCommandRuleType createJobOrderCommandRuleType();

	/**
	 * Returns a new object of class '<em>Job Order Command Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Job Order Command Type</em>'.
	 * @generated
	 */
	JobOrderCommandType createJobOrderCommandType();

	/**
	 * Returns a new object of class '<em>Job Order Dispatch Status Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Job Order Dispatch Status Type</em>'.
	 * @generated
	 */
	JobOrderDispatchStatusType createJobOrderDispatchStatusType();

	/**
	 * Returns a new object of class '<em>Latest End Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Latest End Time Type</em>'.
	 * @generated
	 */
	LatestEndTimeType createLatestEndTimeType();

	/**
	 * Returns a new object of class '<em>Location Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Location Type</em>'.
	 * @generated
	 */
	LocationType createLocationType();

	/**
	 * Returns a new object of class '<em>Manufacturing Bill ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Manufacturing Bill ID Type</em>'.
	 * @generated
	 */
	ManufacturingBillIDType createManufacturingBillIDType();

	/**
	 * Returns a new object of class '<em>Material Actual ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Actual ID Type</em>'.
	 * @generated
	 */
	MaterialActualIDType createMaterialActualIDType();

	/**
	 * Returns a new object of class '<em>Material Capability ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Capability ID Type</em>'.
	 * @generated
	 */
	MaterialCapabilityIDType createMaterialCapabilityIDType();

	/**
	 * Returns a new object of class '<em>Material Class ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Class ID Type</em>'.
	 * @generated
	 */
	MaterialClassIDType createMaterialClassIDType();

	/**
	 * Returns a new object of class '<em>Material Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Class Property Type</em>'.
	 * @generated
	 */
	MaterialClassPropertyType createMaterialClassPropertyType();

	/**
	 * Returns a new object of class '<em>Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Class Type</em>'.
	 * @generated
	 */
	MaterialClassType createMaterialClassType();

	/**
	 * Returns a new object of class '<em>Material Definition ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Definition ID Type</em>'.
	 * @generated
	 */
	MaterialDefinitionIDType createMaterialDefinitionIDType();

	/**
	 * Returns a new object of class '<em>Material Definition Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Definition Property Type</em>'.
	 * @generated
	 */
	MaterialDefinitionPropertyType createMaterialDefinitionPropertyType();

	/**
	 * Returns a new object of class '<em>Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Definition Type</em>'.
	 * @generated
	 */
	MaterialDefinitionType createMaterialDefinitionType();

	/**
	 * Returns a new object of class '<em>Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Information Type</em>'.
	 * @generated
	 */
	MaterialInformationType createMaterialInformationType();

	/**
	 * Returns a new object of class '<em>Material Lot ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Lot ID Type</em>'.
	 * @generated
	 */
	MaterialLotIDType createMaterialLotIDType();

	/**
	 * Returns a new object of class '<em>Material Lot Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Lot Property Type</em>'.
	 * @generated
	 */
	MaterialLotPropertyType createMaterialLotPropertyType();

	/**
	 * Returns a new object of class '<em>Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Lot Type</em>'.
	 * @generated
	 */
	MaterialLotType createMaterialLotType();

	/**
	 * Returns a new object of class '<em>Material Requirement ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Requirement ID Type</em>'.
	 * @generated
	 */
	MaterialRequirementIDType createMaterialRequirementIDType();

	/**
	 * Returns a new object of class '<em>Material Segment Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Segment Specification Property Type</em>'.
	 * @generated
	 */
	MaterialSegmentSpecificationPropertyType createMaterialSegmentSpecificationPropertyType();

	/**
	 * Returns a new object of class '<em>Material Segment Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Segment Specification Type</em>'.
	 * @generated
	 */
	MaterialSegmentSpecificationType createMaterialSegmentSpecificationType();

	/**
	 * Returns a new object of class '<em>Material Specification ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Specification ID Type</em>'.
	 * @generated
	 */
	MaterialSpecificationIDType createMaterialSpecificationIDType();

	/**
	 * Returns a new object of class '<em>Material Sub Lot ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Sub Lot ID Type</em>'.
	 * @generated
	 */
	MaterialSubLotIDType createMaterialSubLotIDType();

	/**
	 * Returns a new object of class '<em>Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Sub Lot Type</em>'.
	 * @generated
	 */
	MaterialSubLotType createMaterialSubLotType();

	/**
	 * Returns a new object of class '<em>Material Test Specification ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Test Specification ID Type</em>'.
	 * @generated
	 */
	MaterialTestSpecificationIDType createMaterialTestSpecificationIDType();

	/**
	 * Returns a new object of class '<em>Material Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Test Specification Type</em>'.
	 * @generated
	 */
	MaterialTestSpecificationType createMaterialTestSpecificationType();

	/**
	 * Returns a new object of class '<em>Material Use1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Use1 Type</em>'.
	 * @generated
	 */
	MaterialUse1Type createMaterialUse1Type();

	/**
	 * Returns a new object of class '<em>Material Use Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Material Use Type</em>'.
	 * @generated
	 */
	MaterialUseType createMaterialUseType();

	/**
	 * Returns a new object of class '<em>Measure Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Measure Type</em>'.
	 * @generated
	 */
	MeasureType createMeasureType();

	/**
	 * Returns a new object of class '<em>Name Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Name Type</em>'.
	 * @generated
	 */
	NameType createNameType();

	/**
	 * Returns a new object of class '<em>Numeric Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Numeric Type</em>'.
	 * @generated
	 */
	NumericType createNumericType();

	/**
	 * Returns a new object of class '<em>Op Equipment Actual Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Equipment Actual Property Type</em>'.
	 * @generated
	 */
	OpEquipmentActualPropertyType createOpEquipmentActualPropertyType();

	/**
	 * Returns a new object of class '<em>Op Equipment Actual Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Equipment Actual Type</em>'.
	 * @generated
	 */
	OpEquipmentActualType createOpEquipmentActualType();

	/**
	 * Returns a new object of class '<em>Op Equipment Capability Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Equipment Capability Property Type</em>'.
	 * @generated
	 */
	OpEquipmentCapabilityPropertyType createOpEquipmentCapabilityPropertyType();

	/**
	 * Returns a new object of class '<em>Op Equipment Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Equipment Capability Type</em>'.
	 * @generated
	 */
	OpEquipmentCapabilityType createOpEquipmentCapabilityType();

	/**
	 * Returns a new object of class '<em>Op Equipment Requirement Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Equipment Requirement Property Type</em>'.
	 * @generated
	 */
	OpEquipmentRequirementPropertyType createOpEquipmentRequirementPropertyType();

	/**
	 * Returns a new object of class '<em>Op Equipment Requirement Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Equipment Requirement Type</em>'.
	 * @generated
	 */
	OpEquipmentRequirementType createOpEquipmentRequirementType();

	/**
	 * Returns a new object of class '<em>Op Equipment Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Equipment Specification Property Type</em>'.
	 * @generated
	 */
	OpEquipmentSpecificationPropertyType createOpEquipmentSpecificationPropertyType();

	/**
	 * Returns a new object of class '<em>Op Equipment Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Equipment Specification Type</em>'.
	 * @generated
	 */
	OpEquipmentSpecificationType createOpEquipmentSpecificationType();

	/**
	 * Returns a new object of class '<em>Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Capability Information Type</em>'.
	 * @generated
	 */
	OperationsCapabilityInformationType createOperationsCapabilityInformationType();

	/**
	 * Returns a new object of class '<em>Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Capability Type</em>'.
	 * @generated
	 */
	OperationsCapabilityType createOperationsCapabilityType();

	/**
	 * Returns a new object of class '<em>Operations Definition ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Definition ID Type</em>'.
	 * @generated
	 */
	OperationsDefinitionIDType createOperationsDefinitionIDType();

	/**
	 * Returns a new object of class '<em>Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Definition Information Type</em>'.
	 * @generated
	 */
	OperationsDefinitionInformationType createOperationsDefinitionInformationType();

	/**
	 * Returns a new object of class '<em>Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Definition Type</em>'.
	 * @generated
	 */
	OperationsDefinitionType createOperationsDefinitionType();

	/**
	 * Returns a new object of class '<em>Operations Material Bill Item Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Material Bill Item Type</em>'.
	 * @generated
	 */
	OperationsMaterialBillItemType createOperationsMaterialBillItemType();

	/**
	 * Returns a new object of class '<em>Operations Material Bill Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Material Bill Type</em>'.
	 * @generated
	 */
	OperationsMaterialBillType createOperationsMaterialBillType();

	/**
	 * Returns a new object of class '<em>Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Performance Type</em>'.
	 * @generated
	 */
	OperationsPerformanceType createOperationsPerformanceType();

	/**
	 * Returns a new object of class '<em>Operations Request ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Request ID Type</em>'.
	 * @generated
	 */
	OperationsRequestIDType createOperationsRequestIDType();

	/**
	 * Returns a new object of class '<em>Operations Request Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Request Type</em>'.
	 * @generated
	 */
	OperationsRequestType createOperationsRequestType();

	/**
	 * Returns a new object of class '<em>Operations Response Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Response Type</em>'.
	 * @generated
	 */
	OperationsResponseType createOperationsResponseType();

	/**
	 * Returns a new object of class '<em>Operations Schedule ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Schedule ID Type</em>'.
	 * @generated
	 */
	OperationsScheduleIDType createOperationsScheduleIDType();

	/**
	 * Returns a new object of class '<em>Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Schedule Type</em>'.
	 * @generated
	 */
	OperationsScheduleType createOperationsScheduleType();

	/**
	 * Returns a new object of class '<em>Operations Segment ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Segment ID Type</em>'.
	 * @generated
	 */
	OperationsSegmentIDType createOperationsSegmentIDType();

	/**
	 * Returns a new object of class '<em>Operations Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Segment Type</em>'.
	 * @generated
	 */
	OperationsSegmentType createOperationsSegmentType();

	/**
	 * Returns a new object of class '<em>Operations Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Type1 Type</em>'.
	 * @generated
	 */
	OperationsType1Type createOperationsType1Type();

	/**
	 * Returns a new object of class '<em>Operations Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operations Type Type</em>'.
	 * @generated
	 */
	OperationsTypeType createOperationsTypeType();

	/**
	 * Returns a new object of class '<em>Op Material Actual Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Material Actual Property Type</em>'.
	 * @generated
	 */
	OpMaterialActualPropertyType createOpMaterialActualPropertyType();

	/**
	 * Returns a new object of class '<em>Op Material Actual Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Material Actual Type</em>'.
	 * @generated
	 */
	OpMaterialActualType createOpMaterialActualType();

	/**
	 * Returns a new object of class '<em>Op Material Capability Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Material Capability Property Type</em>'.
	 * @generated
	 */
	OpMaterialCapabilityPropertyType createOpMaterialCapabilityPropertyType();

	/**
	 * Returns a new object of class '<em>Op Material Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Material Capability Type</em>'.
	 * @generated
	 */
	OpMaterialCapabilityType createOpMaterialCapabilityType();

	/**
	 * Returns a new object of class '<em>Op Material Requirement Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Material Requirement Property Type</em>'.
	 * @generated
	 */
	OpMaterialRequirementPropertyType createOpMaterialRequirementPropertyType();

	/**
	 * Returns a new object of class '<em>Op Material Requirement Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Material Requirement Type</em>'.
	 * @generated
	 */
	OpMaterialRequirementType createOpMaterialRequirementType();

	/**
	 * Returns a new object of class '<em>Op Material Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Material Specification Property Type</em>'.
	 * @generated
	 */
	OpMaterialSpecificationPropertyType createOpMaterialSpecificationPropertyType();

	/**
	 * Returns a new object of class '<em>Op Material Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Material Specification Type</em>'.
	 * @generated
	 */
	OpMaterialSpecificationType createOpMaterialSpecificationType();

	/**
	 * Returns a new object of class '<em>Op Personnel Actual Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Personnel Actual Property Type</em>'.
	 * @generated
	 */
	OpPersonnelActualPropertyType createOpPersonnelActualPropertyType();

	/**
	 * Returns a new object of class '<em>Op Personnel Actual Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Personnel Actual Type</em>'.
	 * @generated
	 */
	OpPersonnelActualType createOpPersonnelActualType();

	/**
	 * Returns a new object of class '<em>Op Personnel Capability Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Personnel Capability Property Type</em>'.
	 * @generated
	 */
	OpPersonnelCapabilityPropertyType createOpPersonnelCapabilityPropertyType();

	/**
	 * Returns a new object of class '<em>Op Personnel Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Personnel Capability Type</em>'.
	 * @generated
	 */
	OpPersonnelCapabilityType createOpPersonnelCapabilityType();

	/**
	 * Returns a new object of class '<em>Op Personnel Requirement Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Personnel Requirement Property Type</em>'.
	 * @generated
	 */
	OpPersonnelRequirementPropertyType createOpPersonnelRequirementPropertyType();

	/**
	 * Returns a new object of class '<em>Op Personnel Requirement Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Personnel Requirement Type</em>'.
	 * @generated
	 */
	OpPersonnelRequirementType createOpPersonnelRequirementType();

	/**
	 * Returns a new object of class '<em>Op Personnel Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Personnel Specification Property Type</em>'.
	 * @generated
	 */
	OpPersonnelSpecificationPropertyType createOpPersonnelSpecificationPropertyType();

	/**
	 * Returns a new object of class '<em>Op Personnel Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Personnel Specification Type</em>'.
	 * @generated
	 */
	OpPersonnelSpecificationType createOpPersonnelSpecificationType();

	/**
	 * Returns a new object of class '<em>Op Physical Asset Actual Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Physical Asset Actual Property Type</em>'.
	 * @generated
	 */
	OpPhysicalAssetActualPropertyType createOpPhysicalAssetActualPropertyType();

	/**
	 * Returns a new object of class '<em>Op Physical Asset Actual Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Physical Asset Actual Type</em>'.
	 * @generated
	 */
	OpPhysicalAssetActualType createOpPhysicalAssetActualType();

	/**
	 * Returns a new object of class '<em>Op Physical Asset Capability Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Physical Asset Capability Property Type</em>'.
	 * @generated
	 */
	OpPhysicalAssetCapabilityPropertyType createOpPhysicalAssetCapabilityPropertyType();

	/**
	 * Returns a new object of class '<em>Op Physical Asset Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Physical Asset Capability Type</em>'.
	 * @generated
	 */
	OpPhysicalAssetCapabilityType createOpPhysicalAssetCapabilityType();

	/**
	 * Returns a new object of class '<em>Op Physical Asset Requirement Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Physical Asset Requirement Property Type</em>'.
	 * @generated
	 */
	OpPhysicalAssetRequirementPropertyType createOpPhysicalAssetRequirementPropertyType();

	/**
	 * Returns a new object of class '<em>Op Physical Asset Requirement Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Physical Asset Requirement Type</em>'.
	 * @generated
	 */
	OpPhysicalAssetRequirementType createOpPhysicalAssetRequirementType();

	/**
	 * Returns a new object of class '<em>Op Physical Asset Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Physical Asset Specification Property Type</em>'.
	 * @generated
	 */
	OpPhysicalAssetSpecificationPropertyType createOpPhysicalAssetSpecificationPropertyType();

	/**
	 * Returns a new object of class '<em>Op Physical Asset Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Physical Asset Specification Type</em>'.
	 * @generated
	 */
	OpPhysicalAssetSpecificationType createOpPhysicalAssetSpecificationType();

	/**
	 * Returns a new object of class '<em>Op Process Segment Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Process Segment Capability Type</em>'.
	 * @generated
	 */
	OpProcessSegmentCapabilityType createOpProcessSegmentCapabilityType();

	/**
	 * Returns a new object of class '<em>Op Segment Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Segment Data Type</em>'.
	 * @generated
	 */
	OpSegmentDataType createOpSegmentDataType();

	/**
	 * Returns a new object of class '<em>Op Segment Requirement Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Segment Requirement Type</em>'.
	 * @generated
	 */
	OpSegmentRequirementType createOpSegmentRequirementType();

	/**
	 * Returns a new object of class '<em>Op Segment Response Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Op Segment Response Type</em>'.
	 * @generated
	 */
	OpSegmentResponseType createOpSegmentResponseType();

	/**
	 * Returns a new object of class '<em>Other Dependency Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Other Dependency Type</em>'.
	 * @generated
	 */
	OtherDependencyType createOtherDependencyType();

	/**
	 * Returns a new object of class '<em>Parameter ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter ID Type</em>'.
	 * @generated
	 */
	ParameterIDType createParameterIDType();

	/**
	 * Returns a new object of class '<em>Parameter Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Type</em>'.
	 * @generated
	 */
	ParameterType createParameterType();

	/**
	 * Returns a new object of class '<em>Person ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Person ID Type</em>'.
	 * @generated
	 */
	PersonIDType createPersonIDType();

	/**
	 * Returns a new object of class '<em>Person Name Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Person Name Type</em>'.
	 * @generated
	 */
	PersonNameType createPersonNameType();

	/**
	 * Returns a new object of class '<em>Personnel Class ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Class ID Type</em>'.
	 * @generated
	 */
	PersonnelClassIDType createPersonnelClassIDType();

	/**
	 * Returns a new object of class '<em>Personnel Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Class Property Type</em>'.
	 * @generated
	 */
	PersonnelClassPropertyType createPersonnelClassPropertyType();

	/**
	 * Returns a new object of class '<em>Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Class Type</em>'.
	 * @generated
	 */
	PersonnelClassType createPersonnelClassType();

	/**
	 * Returns a new object of class '<em>Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Information Type</em>'.
	 * @generated
	 */
	PersonnelInformationType createPersonnelInformationType();

	/**
	 * Returns a new object of class '<em>Personnel Segment Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Segment Specification Property Type</em>'.
	 * @generated
	 */
	PersonnelSegmentSpecificationPropertyType createPersonnelSegmentSpecificationPropertyType();

	/**
	 * Returns a new object of class '<em>Personnel Segment Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Segment Specification Type</em>'.
	 * @generated
	 */
	PersonnelSegmentSpecificationType createPersonnelSegmentSpecificationType();

	/**
	 * Returns a new object of class '<em>Personnel Use Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Personnel Use Type</em>'.
	 * @generated
	 */
	PersonnelUseType createPersonnelUseType();

	/**
	 * Returns a new object of class '<em>Person Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Person Property Type</em>'.
	 * @generated
	 */
	PersonPropertyType createPersonPropertyType();

	/**
	 * Returns a new object of class '<em>Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Person Type</em>'.
	 * @generated
	 */
	PersonType createPersonType();

	/**
	 * Returns a new object of class '<em>Physical Asset Actual ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Actual ID Type</em>'.
	 * @generated
	 */
	PhysicalAssetActualIDType createPhysicalAssetActualIDType();

	/**
	 * Returns a new object of class '<em>Physical Asset Capability Test Specification ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Capability Test Specification ID Type</em>'.
	 * @generated
	 */
	PhysicalAssetCapabilityTestSpecificationIDType createPhysicalAssetCapabilityTestSpecificationIDType();

	/**
	 * Returns a new object of class '<em>Physical Asset Capability Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Capability Test Specification Type</em>'.
	 * @generated
	 */
	PhysicalAssetCapabilityTestSpecificationType createPhysicalAssetCapabilityTestSpecificationType();

	/**
	 * Returns a new object of class '<em>Physical Asset Class ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Class ID Type</em>'.
	 * @generated
	 */
	PhysicalAssetClassIDType createPhysicalAssetClassIDType();

	/**
	 * Returns a new object of class '<em>Physical Asset Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Class Property Type</em>'.
	 * @generated
	 */
	PhysicalAssetClassPropertyType createPhysicalAssetClassPropertyType();

	/**
	 * Returns a new object of class '<em>Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Class Type</em>'.
	 * @generated
	 */
	PhysicalAssetClassType createPhysicalAssetClassType();

	/**
	 * Returns a new object of class '<em>Physical Asset ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset ID Type</em>'.
	 * @generated
	 */
	PhysicalAssetIDType createPhysicalAssetIDType();

	/**
	 * Returns a new object of class '<em>Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Information Type</em>'.
	 * @generated
	 */
	PhysicalAssetInformationType createPhysicalAssetInformationType();

	/**
	 * Returns a new object of class '<em>Physical Asset Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Property Type</em>'.
	 * @generated
	 */
	PhysicalAssetPropertyType createPhysicalAssetPropertyType();

	/**
	 * Returns a new object of class '<em>Physical Asset Segment Specification Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Segment Specification Property Type</em>'.
	 * @generated
	 */
	PhysicalAssetSegmentSpecificationPropertyType createPhysicalAssetSegmentSpecificationPropertyType();

	/**
	 * Returns a new object of class '<em>Physical Asset Segment Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Segment Specification Type</em>'.
	 * @generated
	 */
	PhysicalAssetSegmentSpecificationType createPhysicalAssetSegmentSpecificationType();

	/**
	 * Returns a new object of class '<em>Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Type</em>'.
	 * @generated
	 */
	PhysicalAssetType createPhysicalAssetType();

	/**
	 * Returns a new object of class '<em>Physical Asset Use Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Asset Use Type</em>'.
	 * @generated
	 */
	PhysicalAssetUseType createPhysicalAssetUseType();

	/**
	 * Returns a new object of class '<em>Planned Finish Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Planned Finish Time Type</em>'.
	 * @generated
	 */
	PlannedFinishTimeType createPlannedFinishTimeType();

	/**
	 * Returns a new object of class '<em>Planned Start Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Planned Start Time Type</em>'.
	 * @generated
	 */
	PlannedStartTimeType createPlannedStartTimeType();

	/**
	 * Returns a new object of class '<em>Priority Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Priority Type</em>'.
	 * @generated
	 */
	PriorityType createPriorityType();

	/**
	 * Returns a new object of class '<em>Problem Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Problem Type</em>'.
	 * @generated
	 */
	ProblemType createProblemType();

	/**
	 * Returns a new object of class '<em>Process Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Equipment Capability Test Spec Type</em>'.
	 * @generated
	 */
	ProcessEquipmentCapabilityTestSpecType createProcessEquipmentCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Process Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Equipment Class Type</em>'.
	 * @generated
	 */
	ProcessEquipmentClassType createProcessEquipmentClassType();

	/**
	 * Returns a new object of class '<em>Process Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Equipment Information Type</em>'.
	 * @generated
	 */
	ProcessEquipmentInformationType createProcessEquipmentInformationType();

	/**
	 * Returns a new object of class '<em>Process Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Equipment Type</em>'.
	 * @generated
	 */
	ProcessEquipmentType createProcessEquipmentType();

	/**
	 * Returns a new object of class '<em>Process Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Material Class Type</em>'.
	 * @generated
	 */
	ProcessMaterialClassType createProcessMaterialClassType();

	/**
	 * Returns a new object of class '<em>Process Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Material Definition Type</em>'.
	 * @generated
	 */
	ProcessMaterialDefinitionType createProcessMaterialDefinitionType();

	/**
	 * Returns a new object of class '<em>Process Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Material Information Type</em>'.
	 * @generated
	 */
	ProcessMaterialInformationType createProcessMaterialInformationType();

	/**
	 * Returns a new object of class '<em>Process Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Material Lot Type</em>'.
	 * @generated
	 */
	ProcessMaterialLotType createProcessMaterialLotType();

	/**
	 * Returns a new object of class '<em>Process Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Material Sub Lot Type</em>'.
	 * @generated
	 */
	ProcessMaterialSubLotType createProcessMaterialSubLotType();

	/**
	 * Returns a new object of class '<em>Process Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Material Test Spec Type</em>'.
	 * @generated
	 */
	ProcessMaterialTestSpecType createProcessMaterialTestSpecType();

	/**
	 * Returns a new object of class '<em>Process Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Operations Capability Information Type</em>'.
	 * @generated
	 */
	ProcessOperationsCapabilityInformationType createProcessOperationsCapabilityInformationType();

	/**
	 * Returns a new object of class '<em>Process Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Operations Capability Type</em>'.
	 * @generated
	 */
	ProcessOperationsCapabilityType createProcessOperationsCapabilityType();

	/**
	 * Returns a new object of class '<em>Process Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Operations Definition Information Type</em>'.
	 * @generated
	 */
	ProcessOperationsDefinitionInformationType createProcessOperationsDefinitionInformationType();

	/**
	 * Returns a new object of class '<em>Process Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Operations Definition Type</em>'.
	 * @generated
	 */
	ProcessOperationsDefinitionType createProcessOperationsDefinitionType();

	/**
	 * Returns a new object of class '<em>Process Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Operations Performance Type</em>'.
	 * @generated
	 */
	ProcessOperationsPerformanceType createProcessOperationsPerformanceType();

	/**
	 * Returns a new object of class '<em>Process Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Operations Schedule Type</em>'.
	 * @generated
	 */
	ProcessOperationsScheduleType createProcessOperationsScheduleType();

	/**
	 * Returns a new object of class '<em>Process Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Personnel Class Type</em>'.
	 * @generated
	 */
	ProcessPersonnelClassType createProcessPersonnelClassType();

	/**
	 * Returns a new object of class '<em>Process Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Personnel Information Type</em>'.
	 * @generated
	 */
	ProcessPersonnelInformationType createProcessPersonnelInformationType();

	/**
	 * Returns a new object of class '<em>Process Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Person Type</em>'.
	 * @generated
	 */
	ProcessPersonType createProcessPersonType();

	/**
	 * Returns a new object of class '<em>Process Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Physical Asset Capability Test Spec Type</em>'.
	 * @generated
	 */
	ProcessPhysicalAssetCapabilityTestSpecType createProcessPhysicalAssetCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Process Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Physical Asset Class Type</em>'.
	 * @generated
	 */
	ProcessPhysicalAssetClassType createProcessPhysicalAssetClassType();

	/**
	 * Returns a new object of class '<em>Process Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Physical Asset Information Type</em>'.
	 * @generated
	 */
	ProcessPhysicalAssetInformationType createProcessPhysicalAssetInformationType();

	/**
	 * Returns a new object of class '<em>Process Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Physical Asset Type</em>'.
	 * @generated
	 */
	ProcessPhysicalAssetType createProcessPhysicalAssetType();

	/**
	 * Returns a new object of class '<em>Process Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Process Segment Information Type</em>'.
	 * @generated
	 */
	ProcessProcessSegmentInformationType createProcessProcessSegmentInformationType();

	/**
	 * Returns a new object of class '<em>Process Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Process Segment Type</em>'.
	 * @generated
	 */
	ProcessProcessSegmentType createProcessProcessSegmentType();

	/**
	 * Returns a new object of class '<em>Process Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Qualification Test Specification Type</em>'.
	 * @generated
	 */
	ProcessQualificationTestSpecificationType createProcessQualificationTestSpecificationType();

	/**
	 * Returns a new object of class '<em>Process Segment ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Segment ID Type</em>'.
	 * @generated
	 */
	ProcessSegmentIDType createProcessSegmentIDType();

	/**
	 * Returns a new object of class '<em>Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Segment Information Type</em>'.
	 * @generated
	 */
	ProcessSegmentInformationType createProcessSegmentInformationType();

	/**
	 * Returns a new object of class '<em>Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process Segment Type</em>'.
	 * @generated
	 */
	ProcessSegmentType createProcessSegmentType();

	/**
	 * Returns a new object of class '<em>Production Request ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Production Request ID Type</em>'.
	 * @generated
	 */
	ProductionRequestIDType createProductionRequestIDType();

	/**
	 * Returns a new object of class '<em>Production Schedule ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Production Schedule ID Type</em>'.
	 * @generated
	 */
	ProductionScheduleIDType createProductionScheduleIDType();

	/**
	 * Returns a new object of class '<em>Product Production Rule ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Product Production Rule ID Type</em>'.
	 * @generated
	 */
	ProductProductionRuleIDType createProductProductionRuleIDType();

	/**
	 * Returns a new object of class '<em>Product Production Rule Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Product Production Rule Type</em>'.
	 * @generated
	 */
	ProductProductionRuleType createProductProductionRuleType();

	/**
	 * Returns a new object of class '<em>Product Segment ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Product Segment ID Type</em>'.
	 * @generated
	 */
	ProductSegmentIDType createProductSegmentIDType();

	/**
	 * Returns a new object of class '<em>Property ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Property ID Type</em>'.
	 * @generated
	 */
	PropertyIDType createPropertyIDType();

	/**
	 * Returns a new object of class '<em>Published Date Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Published Date Type</em>'.
	 * @generated
	 */
	PublishedDateType createPublishedDateType();

	/**
	 * Returns a new object of class '<em>Qualification Test Specification ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Qualification Test Specification ID Type</em>'.
	 * @generated
	 */
	QualificationTestSpecificationIDType createQualificationTestSpecificationIDType();

	/**
	 * Returns a new object of class '<em>Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Qualification Test Specification Type</em>'.
	 * @generated
	 */
	QualificationTestSpecificationType createQualificationTestSpecificationType();

	/**
	 * Returns a new object of class '<em>Quantity String Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Quantity String Type</em>'.
	 * @generated
	 */
	QuantityStringType createQuantityStringType();

	/**
	 * Returns a new object of class '<em>Quantity Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Quantity Type</em>'.
	 * @generated
	 */
	QuantityType createQuantityType();

	/**
	 * Returns a new object of class '<em>Quantity Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Quantity Value Type</em>'.
	 * @generated
	 */
	QuantityValueType createQuantityValueType();

	/**
	 * Returns a new object of class '<em>Reason Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reason Type</em>'.
	 * @generated
	 */
	ReasonType createReasonType();

	/**
	 * Returns a new object of class '<em>Relationship Form1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Relationship Form1 Type</em>'.
	 * @generated
	 */
	RelationshipForm1Type createRelationshipForm1Type();

	/**
	 * Returns a new object of class '<em>Relationship Form Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Relationship Form Type</em>'.
	 * @generated
	 */
	RelationshipFormType createRelationshipFormType();

	/**
	 * Returns a new object of class '<em>Relationship Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Relationship Type1 Type</em>'.
	 * @generated
	 */
	RelationshipType1Type createRelationshipType1Type();

	/**
	 * Returns a new object of class '<em>Relationship Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Relationship Type Type</em>'.
	 * @generated
	 */
	RelationshipTypeType createRelationshipTypeType();

	/**
	 * Returns a new object of class '<em>Requested Completion Date Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Requested Completion Date Type</em>'.
	 * @generated
	 */
	RequestedCompletionDateType createRequestedCompletionDateType();

	/**
	 * Returns a new object of class '<em>Requested Priority Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Requested Priority Type</em>'.
	 * @generated
	 */
	RequestedPriorityType createRequestedPriorityType();

	/**
	 * Returns a new object of class '<em>Request State1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Request State1 Type</em>'.
	 * @generated
	 */
	RequestState1Type createRequestState1Type();

	/**
	 * Returns a new object of class '<em>Request State Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Request State Type</em>'.
	 * @generated
	 */
	RequestStateType createRequestStateType();

	/**
	 * Returns a new object of class '<em>Required By Requested Segment Response1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Required By Requested Segment Response1 Type</em>'.
	 * @generated
	 */
	RequiredByRequestedSegmentResponse1Type createRequiredByRequestedSegmentResponse1Type();

	/**
	 * Returns a new object of class '<em>Required By Requested Segment Response Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Required By Requested Segment Response Type</em>'.
	 * @generated
	 */
	RequiredByRequestedSegmentResponseType createRequiredByRequestedSegmentResponseType();

	/**
	 * Returns a new object of class '<em>Resource ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resource ID Type</em>'.
	 * @generated
	 */
	ResourceIDType createResourceIDType();

	/**
	 * Returns a new object of class '<em>Resource Network Connection ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resource Network Connection ID Type</em>'.
	 * @generated
	 */
	ResourceNetworkConnectionIDType createResourceNetworkConnectionIDType();

	/**
	 * Returns a new object of class '<em>Resource Reference Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resource Reference Type1 Type</em>'.
	 * @generated
	 */
	ResourceReferenceType1Type createResourceReferenceType1Type();

	/**
	 * Returns a new object of class '<em>Resource Reference Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resource Reference Type Type</em>'.
	 * @generated
	 */
	ResourceReferenceTypeType createResourceReferenceTypeType();

	/**
	 * Returns a new object of class '<em>Resources Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Resources Type</em>'.
	 * @generated
	 */
	ResourcesType createResourcesType();

	/**
	 * Returns a new object of class '<em>Respond Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Equipment Capability Test Spec Type</em>'.
	 * @generated
	 */
	RespondEquipmentCapabilityTestSpecType createRespondEquipmentCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Respond Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Equipment Class Type</em>'.
	 * @generated
	 */
	RespondEquipmentClassType createRespondEquipmentClassType();

	/**
	 * Returns a new object of class '<em>Respond Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Equipment Information Type</em>'.
	 * @generated
	 */
	RespondEquipmentInformationType createRespondEquipmentInformationType();

	/**
	 * Returns a new object of class '<em>Respond Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Equipment Type</em>'.
	 * @generated
	 */
	RespondEquipmentType createRespondEquipmentType();

	/**
	 * Returns a new object of class '<em>Respond Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Material Class Type</em>'.
	 * @generated
	 */
	RespondMaterialClassType createRespondMaterialClassType();

	/**
	 * Returns a new object of class '<em>Respond Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Material Definition Type</em>'.
	 * @generated
	 */
	RespondMaterialDefinitionType createRespondMaterialDefinitionType();

	/**
	 * Returns a new object of class '<em>Respond Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Material Information Type</em>'.
	 * @generated
	 */
	RespondMaterialInformationType createRespondMaterialInformationType();

	/**
	 * Returns a new object of class '<em>Respond Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Material Lot Type</em>'.
	 * @generated
	 */
	RespondMaterialLotType createRespondMaterialLotType();

	/**
	 * Returns a new object of class '<em>Respond Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Material Sub Lot Type</em>'.
	 * @generated
	 */
	RespondMaterialSubLotType createRespondMaterialSubLotType();

	/**
	 * Returns a new object of class '<em>Respond Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Material Test Spec Type</em>'.
	 * @generated
	 */
	RespondMaterialTestSpecType createRespondMaterialTestSpecType();

	/**
	 * Returns a new object of class '<em>Respond Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Operations Capability Information Type</em>'.
	 * @generated
	 */
	RespondOperationsCapabilityInformationType createRespondOperationsCapabilityInformationType();

	/**
	 * Returns a new object of class '<em>Respond Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Operations Capability Type</em>'.
	 * @generated
	 */
	RespondOperationsCapabilityType createRespondOperationsCapabilityType();

	/**
	 * Returns a new object of class '<em>Respond Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Operations Definition Information Type</em>'.
	 * @generated
	 */
	RespondOperationsDefinitionInformationType createRespondOperationsDefinitionInformationType();

	/**
	 * Returns a new object of class '<em>Respond Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Operations Definition Type</em>'.
	 * @generated
	 */
	RespondOperationsDefinitionType createRespondOperationsDefinitionType();

	/**
	 * Returns a new object of class '<em>Respond Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Operations Performance Type</em>'.
	 * @generated
	 */
	RespondOperationsPerformanceType createRespondOperationsPerformanceType();

	/**
	 * Returns a new object of class '<em>Respond Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Operations Schedule Type</em>'.
	 * @generated
	 */
	RespondOperationsScheduleType createRespondOperationsScheduleType();

	/**
	 * Returns a new object of class '<em>Respond Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Personnel Class Type</em>'.
	 * @generated
	 */
	RespondPersonnelClassType createRespondPersonnelClassType();

	/**
	 * Returns a new object of class '<em>Respond Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Personnel Information Type</em>'.
	 * @generated
	 */
	RespondPersonnelInformationType createRespondPersonnelInformationType();

	/**
	 * Returns a new object of class '<em>Respond Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Person Type</em>'.
	 * @generated
	 */
	RespondPersonType createRespondPersonType();

	/**
	 * Returns a new object of class '<em>Respond Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Physical Asset Capability Test Spec Type</em>'.
	 * @generated
	 */
	RespondPhysicalAssetCapabilityTestSpecType createRespondPhysicalAssetCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Respond Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Physical Asset Class Type</em>'.
	 * @generated
	 */
	RespondPhysicalAssetClassType createRespondPhysicalAssetClassType();

	/**
	 * Returns a new object of class '<em>Respond Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Physical Asset Information Type</em>'.
	 * @generated
	 */
	RespondPhysicalAssetInformationType createRespondPhysicalAssetInformationType();

	/**
	 * Returns a new object of class '<em>Respond Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Physical Asset Type</em>'.
	 * @generated
	 */
	RespondPhysicalAssetType createRespondPhysicalAssetType();

	/**
	 * Returns a new object of class '<em>Respond Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Process Segment Information Type</em>'.
	 * @generated
	 */
	RespondProcessSegmentInformationType createRespondProcessSegmentInformationType();

	/**
	 * Returns a new object of class '<em>Respond Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Process Segment Type</em>'.
	 * @generated
	 */
	RespondProcessSegmentType createRespondProcessSegmentType();

	/**
	 * Returns a new object of class '<em>Respond Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Respond Qualification Test Specification Type</em>'.
	 * @generated
	 */
	RespondQualificationTestSpecificationType createRespondQualificationTestSpecificationType();

	/**
	 * Returns a new object of class '<em>Response State1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Response State1 Type</em>'.
	 * @generated
	 */
	ResponseState1Type createResponseState1Type();

	/**
	 * Returns a new object of class '<em>Response State Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Response State Type</em>'.
	 * @generated
	 */
	ResponseStateType createResponseStateType();

	/**
	 * Returns a new object of class '<em>Result Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Result Type</em>'.
	 * @generated
	 */
	ResultType createResultType();

	/**
	 * Returns a new object of class '<em>Segment Dependency Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Segment Dependency Type</em>'.
	 * @generated
	 */
	SegmentDependencyType createSegmentDependencyType();

	/**
	 * Returns a new object of class '<em>Show Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Equipment Capability Test Spec Type</em>'.
	 * @generated
	 */
	ShowEquipmentCapabilityTestSpecType createShowEquipmentCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Show Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Equipment Class Type</em>'.
	 * @generated
	 */
	ShowEquipmentClassType createShowEquipmentClassType();

	/**
	 * Returns a new object of class '<em>Show Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Equipment Information Type</em>'.
	 * @generated
	 */
	ShowEquipmentInformationType createShowEquipmentInformationType();

	/**
	 * Returns a new object of class '<em>Show Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Equipment Type</em>'.
	 * @generated
	 */
	ShowEquipmentType createShowEquipmentType();

	/**
	 * Returns a new object of class '<em>Show Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Material Class Type</em>'.
	 * @generated
	 */
	ShowMaterialClassType createShowMaterialClassType();

	/**
	 * Returns a new object of class '<em>Show Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Material Definition Type</em>'.
	 * @generated
	 */
	ShowMaterialDefinitionType createShowMaterialDefinitionType();

	/**
	 * Returns a new object of class '<em>Show Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Material Information Type</em>'.
	 * @generated
	 */
	ShowMaterialInformationType createShowMaterialInformationType();

	/**
	 * Returns a new object of class '<em>Show Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Material Lot Type</em>'.
	 * @generated
	 */
	ShowMaterialLotType createShowMaterialLotType();

	/**
	 * Returns a new object of class '<em>Show Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Material Sub Lot Type</em>'.
	 * @generated
	 */
	ShowMaterialSubLotType createShowMaterialSubLotType();

	/**
	 * Returns a new object of class '<em>Show Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Material Test Spec Type</em>'.
	 * @generated
	 */
	ShowMaterialTestSpecType createShowMaterialTestSpecType();

	/**
	 * Returns a new object of class '<em>Show Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Operations Capability Information Type</em>'.
	 * @generated
	 */
	ShowOperationsCapabilityInformationType createShowOperationsCapabilityInformationType();

	/**
	 * Returns a new object of class '<em>Show Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Operations Capability Type</em>'.
	 * @generated
	 */
	ShowOperationsCapabilityType createShowOperationsCapabilityType();

	/**
	 * Returns a new object of class '<em>Show Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Operations Definition Information Type</em>'.
	 * @generated
	 */
	ShowOperationsDefinitionInformationType createShowOperationsDefinitionInformationType();

	/**
	 * Returns a new object of class '<em>Show Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Operations Definition Type</em>'.
	 * @generated
	 */
	ShowOperationsDefinitionType createShowOperationsDefinitionType();

	/**
	 * Returns a new object of class '<em>Show Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Operations Performance Type</em>'.
	 * @generated
	 */
	ShowOperationsPerformanceType createShowOperationsPerformanceType();

	/**
	 * Returns a new object of class '<em>Show Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Operations Schedule Type</em>'.
	 * @generated
	 */
	ShowOperationsScheduleType createShowOperationsScheduleType();

	/**
	 * Returns a new object of class '<em>Show Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Personnel Class Type</em>'.
	 * @generated
	 */
	ShowPersonnelClassType createShowPersonnelClassType();

	/**
	 * Returns a new object of class '<em>Show Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Personnel Information Type</em>'.
	 * @generated
	 */
	ShowPersonnelInformationType createShowPersonnelInformationType();

	/**
	 * Returns a new object of class '<em>Show Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Person Type</em>'.
	 * @generated
	 */
	ShowPersonType createShowPersonType();

	/**
	 * Returns a new object of class '<em>Show Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Physical Asset Capability Test Spec Type</em>'.
	 * @generated
	 */
	ShowPhysicalAssetCapabilityTestSpecType createShowPhysicalAssetCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Show Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Physical Asset Class Type</em>'.
	 * @generated
	 */
	ShowPhysicalAssetClassType createShowPhysicalAssetClassType();

	/**
	 * Returns a new object of class '<em>Show Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Physical Asset Information Type</em>'.
	 * @generated
	 */
	ShowPhysicalAssetInformationType createShowPhysicalAssetInformationType();

	/**
	 * Returns a new object of class '<em>Show Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Physical Asset Type</em>'.
	 * @generated
	 */
	ShowPhysicalAssetType createShowPhysicalAssetType();

	/**
	 * Returns a new object of class '<em>Show Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Process Segment Information Type</em>'.
	 * @generated
	 */
	ShowProcessSegmentInformationType createShowProcessSegmentInformationType();

	/**
	 * Returns a new object of class '<em>Show Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Process Segment Type</em>'.
	 * @generated
	 */
	ShowProcessSegmentType createShowProcessSegmentType();

	/**
	 * Returns a new object of class '<em>Show Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Qualification Test Specification Type</em>'.
	 * @generated
	 */
	ShowQualificationTestSpecificationType createShowQualificationTestSpecificationType();

	/**
	 * Returns a new object of class '<em>Start Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Start Time Type</em>'.
	 * @generated
	 */
	StartTimeType createStartTimeType();

	/**
	 * Returns a new object of class '<em>Status Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Status Time Type</em>'.
	 * @generated
	 */
	StatusTimeType createStatusTimeType();

	/**
	 * Returns a new object of class '<em>Status Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Status Type</em>'.
	 * @generated
	 */
	StatusType createStatusType();

	/**
	 * Returns a new object of class '<em>Storage Hierarchy Scope Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Storage Hierarchy Scope Type</em>'.
	 * @generated
	 */
	StorageHierarchyScopeType createStorageHierarchyScopeType();

	/**
	 * Returns a new object of class '<em>Storage Location Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Storage Location Type</em>'.
	 * @generated
	 */
	StorageLocationType createStorageLocationType();

	/**
	 * Returns a new object of class '<em>Sync Equipment Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Equipment Capability Test Spec Type</em>'.
	 * @generated
	 */
	SyncEquipmentCapabilityTestSpecType createSyncEquipmentCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Sync Equipment Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Equipment Class Type</em>'.
	 * @generated
	 */
	SyncEquipmentClassType createSyncEquipmentClassType();

	/**
	 * Returns a new object of class '<em>Sync Equipment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Equipment Information Type</em>'.
	 * @generated
	 */
	SyncEquipmentInformationType createSyncEquipmentInformationType();

	/**
	 * Returns a new object of class '<em>Sync Equipment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Equipment Type</em>'.
	 * @generated
	 */
	SyncEquipmentType createSyncEquipmentType();

	/**
	 * Returns a new object of class '<em>Sync Material Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Material Class Type</em>'.
	 * @generated
	 */
	SyncMaterialClassType createSyncMaterialClassType();

	/**
	 * Returns a new object of class '<em>Sync Material Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Material Definition Type</em>'.
	 * @generated
	 */
	SyncMaterialDefinitionType createSyncMaterialDefinitionType();

	/**
	 * Returns a new object of class '<em>Sync Material Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Material Information Type</em>'.
	 * @generated
	 */
	SyncMaterialInformationType createSyncMaterialInformationType();

	/**
	 * Returns a new object of class '<em>Sync Material Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Material Lot Type</em>'.
	 * @generated
	 */
	SyncMaterialLotType createSyncMaterialLotType();

	/**
	 * Returns a new object of class '<em>Sync Material Sub Lot Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Material Sub Lot Type</em>'.
	 * @generated
	 */
	SyncMaterialSubLotType createSyncMaterialSubLotType();

	/**
	 * Returns a new object of class '<em>Sync Material Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Material Test Spec Type</em>'.
	 * @generated
	 */
	SyncMaterialTestSpecType createSyncMaterialTestSpecType();

	/**
	 * Returns a new object of class '<em>Sync Operations Capability Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Operations Capability Information Type</em>'.
	 * @generated
	 */
	SyncOperationsCapabilityInformationType createSyncOperationsCapabilityInformationType();

	/**
	 * Returns a new object of class '<em>Sync Operations Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Operations Capability Type</em>'.
	 * @generated
	 */
	SyncOperationsCapabilityType createSyncOperationsCapabilityType();

	/**
	 * Returns a new object of class '<em>Sync Operations Definition Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Operations Definition Information Type</em>'.
	 * @generated
	 */
	SyncOperationsDefinitionInformationType createSyncOperationsDefinitionInformationType();

	/**
	 * Returns a new object of class '<em>Sync Operations Definition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Operations Definition Type</em>'.
	 * @generated
	 */
	SyncOperationsDefinitionType createSyncOperationsDefinitionType();

	/**
	 * Returns a new object of class '<em>Sync Operations Performance Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Operations Performance Type</em>'.
	 * @generated
	 */
	SyncOperationsPerformanceType createSyncOperationsPerformanceType();

	/**
	 * Returns a new object of class '<em>Sync Operations Schedule Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Operations Schedule Type</em>'.
	 * @generated
	 */
	SyncOperationsScheduleType createSyncOperationsScheduleType();

	/**
	 * Returns a new object of class '<em>Sync Personnel Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Personnel Class Type</em>'.
	 * @generated
	 */
	SyncPersonnelClassType createSyncPersonnelClassType();

	/**
	 * Returns a new object of class '<em>Sync Personnel Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Personnel Information Type</em>'.
	 * @generated
	 */
	SyncPersonnelInformationType createSyncPersonnelInformationType();

	/**
	 * Returns a new object of class '<em>Sync Person Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Person Type</em>'.
	 * @generated
	 */
	SyncPersonType createSyncPersonType();

	/**
	 * Returns a new object of class '<em>Sync Physical Asset Capability Test Spec Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Physical Asset Capability Test Spec Type</em>'.
	 * @generated
	 */
	SyncPhysicalAssetCapabilityTestSpecType createSyncPhysicalAssetCapabilityTestSpecType();

	/**
	 * Returns a new object of class '<em>Sync Physical Asset Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Physical Asset Class Type</em>'.
	 * @generated
	 */
	SyncPhysicalAssetClassType createSyncPhysicalAssetClassType();

	/**
	 * Returns a new object of class '<em>Sync Physical Asset Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Physical Asset Information Type</em>'.
	 * @generated
	 */
	SyncPhysicalAssetInformationType createSyncPhysicalAssetInformationType();

	/**
	 * Returns a new object of class '<em>Sync Physical Asset Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Physical Asset Type</em>'.
	 * @generated
	 */
	SyncPhysicalAssetType createSyncPhysicalAssetType();

	/**
	 * Returns a new object of class '<em>Sync Process Segment Information Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Process Segment Information Type</em>'.
	 * @generated
	 */
	SyncProcessSegmentInformationType createSyncProcessSegmentInformationType();

	/**
	 * Returns a new object of class '<em>Sync Process Segment Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Process Segment Type</em>'.
	 * @generated
	 */
	SyncProcessSegmentType createSyncProcessSegmentType();

	/**
	 * Returns a new object of class '<em>Sync Qualification Test Specification Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Qualification Test Specification Type</em>'.
	 * @generated
	 */
	SyncQualificationTestSpecificationType createSyncQualificationTestSpecificationType();

	/**
	 * Returns a new object of class '<em>Test Date Time Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Date Time Type</em>'.
	 * @generated
	 */
	TestDateTimeType createTestDateTimeType();

	/**
	 * Returns a new object of class '<em>Tested Equipment Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tested Equipment Class Property Type</em>'.
	 * @generated
	 */
	TestedEquipmentClassPropertyType createTestedEquipmentClassPropertyType();

	/**
	 * Returns a new object of class '<em>Tested Equipment Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tested Equipment Property Type</em>'.
	 * @generated
	 */
	TestedEquipmentPropertyType createTestedEquipmentPropertyType();

	/**
	 * Returns a new object of class '<em>Tested Material Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tested Material Class Property Type</em>'.
	 * @generated
	 */
	TestedMaterialClassPropertyType createTestedMaterialClassPropertyType();

	/**
	 * Returns a new object of class '<em>Tested Material Definition Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tested Material Definition Property Type</em>'.
	 * @generated
	 */
	TestedMaterialDefinitionPropertyType createTestedMaterialDefinitionPropertyType();

	/**
	 * Returns a new object of class '<em>Tested Material Lot Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tested Material Lot Property Type</em>'.
	 * @generated
	 */
	TestedMaterialLotPropertyType createTestedMaterialLotPropertyType();

	/**
	 * Returns a new object of class '<em>Tested Personnel Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tested Personnel Class Property Type</em>'.
	 * @generated
	 */
	TestedPersonnelClassPropertyType createTestedPersonnelClassPropertyType();

	/**
	 * Returns a new object of class '<em>Tested Person Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tested Person Property Type</em>'.
	 * @generated
	 */
	TestedPersonPropertyType createTestedPersonPropertyType();

	/**
	 * Returns a new object of class '<em>Tested Physical Asset Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tested Physical Asset Class Property Type</em>'.
	 * @generated
	 */
	TestedPhysicalAssetClassPropertyType createTestedPhysicalAssetClassPropertyType();

	/**
	 * Returns a new object of class '<em>Tested Physical Asset Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tested Physical Asset Property Type</em>'.
	 * @generated
	 */
	TestedPhysicalAssetPropertyType createTestedPhysicalAssetPropertyType();

	/**
	 * Returns a new object of class '<em>Test Result Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Result Type</em>'.
	 * @generated
	 */
	TestResultType createTestResultType();

	/**
	 * Returns a new object of class '<em>Text Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Text Type</em>'.
	 * @generated
	 */
	TextType createTextType();

	/**
	 * Returns a new object of class '<em>Trans Acknowledge Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Acknowledge Type</em>'.
	 * @generated
	 */
	TransAcknowledgeType createTransAcknowledgeType();

	/**
	 * Returns a new object of class '<em>Trans Action Criteria Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Action Criteria Type</em>'.
	 * @generated
	 */
	TransActionCriteriaType createTransActionCriteriaType();

	/**
	 * Returns a new object of class '<em>Trans Application Area Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Application Area Type</em>'.
	 * @generated
	 */
	TransApplicationAreaType createTransApplicationAreaType();

	/**
	 * Returns a new object of class '<em>Trans Cancel Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Cancel Type</em>'.
	 * @generated
	 */
	TransCancelType createTransCancelType();

	/**
	 * Returns a new object of class '<em>Trans Change Status Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Change Status Type</em>'.
	 * @generated
	 */
	TransChangeStatusType createTransChangeStatusType();

	/**
	 * Returns a new object of class '<em>Trans Change Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Change Type</em>'.
	 * @generated
	 */
	TransChangeType createTransChangeType();

	/**
	 * Returns a new object of class '<em>Trans Confirmation Code Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Confirmation Code Type</em>'.
	 * @generated
	 */
	TransConfirmationCodeType createTransConfirmationCodeType();

	/**
	 * Returns a new object of class '<em>Trans Confirm Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Confirm Type</em>'.
	 * @generated
	 */
	TransConfirmType createTransConfirmType();

	/**
	 * Returns a new object of class '<em>Trans Expression1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Expression1 Type</em>'.
	 * @generated
	 */
	TransExpression1Type createTransExpression1Type();

	/**
	 * Returns a new object of class '<em>Trans Expression Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Expression Type</em>'.
	 * @generated
	 */
	TransExpressionType createTransExpressionType();

	/**
	 * Returns a new object of class '<em>Trans Get Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Get Type</em>'.
	 * @generated
	 */
	TransGetType createTransGetType();

	/**
	 * Returns a new object of class '<em>Trans Process Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Process Type</em>'.
	 * @generated
	 */
	TransProcessType createTransProcessType();

	/**
	 * Returns a new object of class '<em>Trans Receiver Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Receiver Type</em>'.
	 * @generated
	 */
	TransReceiverType createTransReceiverType();

	/**
	 * Returns a new object of class '<em>Trans Respond Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Respond Type</em>'.
	 * @generated
	 */
	TransRespondType createTransRespondType();

	/**
	 * Returns a new object of class '<em>Trans Response Criteria Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Response Criteria Type</em>'.
	 * @generated
	 */
	TransResponseCriteriaType createTransResponseCriteriaType();

	/**
	 * Returns a new object of class '<em>Trans Sender Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Sender Type</em>'.
	 * @generated
	 */
	TransSenderType createTransSenderType();

	/**
	 * Returns a new object of class '<em>Trans Show Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Show Type</em>'.
	 * @generated
	 */
	TransShowType createTransShowType();

	/**
	 * Returns a new object of class '<em>Trans Signature Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Signature Type</em>'.
	 * @generated
	 */
	TransSignatureType createTransSignatureType();

	/**
	 * Returns a new object of class '<em>Trans State Change Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans State Change Type</em>'.
	 * @generated
	 */
	TransStateChangeType createTransStateChangeType();

	/**
	 * Returns a new object of class '<em>Trans Sync Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans Sync Type</em>'.
	 * @generated
	 */
	TransSyncType createTransSyncType();

	/**
	 * Returns a new object of class '<em>Trans User Area Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trans User Area Type</em>'.
	 * @generated
	 */
	TransUserAreaType createTransUserAreaType();

	/**
	 * Returns a new object of class '<em>Unit Of Measure Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unit Of Measure Type</em>'.
	 * @generated
	 */
	UnitOfMeasureType createUnitOfMeasureType();

	/**
	 * Returns a new object of class '<em>Value String Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value String Type</em>'.
	 * @generated
	 */
	ValueStringType createValueStringType();

	/**
	 * Returns a new object of class '<em>Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Type</em>'.
	 * @generated
	 */
	ValueType createValueType();

	/**
	 * Returns a new object of class '<em>Version Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Version Type</em>'.
	 * @generated
	 */
	VersionType createVersionType();

	/**
	 * Returns a new object of class '<em>Work Request ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Work Request ID Type</em>'.
	 * @generated
	 */
	WorkRequestIDType createWorkRequestIDType();

	/**
	 * Returns a new object of class '<em>Work Schedule ID Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Work Schedule ID Type</em>'.
	 * @generated
	 */
	WorkScheduleIDType createWorkScheduleIDType();

	/**
	 * Returns a new object of class '<em>Work Type1 Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Work Type1 Type</em>'.
	 * @generated
	 */
	WorkType1Type createWorkType1Type();

	/**
	 * Returns a new object of class '<em>Work Type Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Work Type Type</em>'.
	 * @generated
	 */
	WorkTypeType createWorkTypeType();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	B2MMLPackage getB2MMLPackage();

} //B2MMLFactory
