/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Response State1 Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getResponseState1Type()
 * @model extendedMetaData="name='ResponseState1Type' kind='simple'"
 * @generated
 */
public interface ResponseState1Type extends CodeType {
} // ResponseState1Type
