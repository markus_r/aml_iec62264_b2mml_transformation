/**
 */
package org.mesa.xml.v0600.b2mml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Latest End Time Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getLatestEndTimeType()
 * @model extendedMetaData="name='LatestEndTimeType' kind='simple'"
 * @generated
 */
public interface LatestEndTimeType extends DateTimeType {
} // LatestEndTimeType
