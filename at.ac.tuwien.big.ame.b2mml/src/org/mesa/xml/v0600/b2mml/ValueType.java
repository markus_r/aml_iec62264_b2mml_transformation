/**
 */
package org.mesa.xml.v0600.b2mml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.mesa.xml.v0600.b2mml.ValueType#getValueString <em>Value String</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.ValueType#getDataType <em>Data Type</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.ValueType#getUnitOfMeasure <em>Unit Of Measure</em>}</li>
 *   <li>{@link org.mesa.xml.v0600.b2mml.ValueType#getKey <em>Key</em>}</li>
 * </ul>
 *
 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getValueType()
 * @model extendedMetaData="name='ValueType' kind='elementOnly'"
 * @generated
 */
public interface ValueType extends EObject {
	/**
	 * Returns the value of the '<em><b>Value String</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value String</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value String</em>' containment reference.
	 * @see #isSetValueString()
	 * @see #unsetValueString()
	 * @see #setValueString(ValueStringType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getValueType_ValueString()
	 * @model containment="true" unsettable="true" required="true"
	 *        extendedMetaData="kind='element' name='ValueString' namespace='##targetNamespace'"
	 * @generated
	 */
	ValueStringType getValueString();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.ValueType#getValueString <em>Value String</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value String</em>' containment reference.
	 * @see #isSetValueString()
	 * @see #unsetValueString()
	 * @see #getValueString()
	 * @generated
	 */
	void setValueString(ValueStringType value);

	/**
	 * Unsets the value of the '{@link org.mesa.xml.v0600.b2mml.ValueType#getValueString <em>Value String</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetValueString()
	 * @see #getValueString()
	 * @see #setValueString(ValueStringType)
	 * @generated
	 */
	void unsetValueString();

	/**
	 * Returns whether the value of the '{@link org.mesa.xml.v0600.b2mml.ValueType#getValueString <em>Value String</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Value String</em>' containment reference is set.
	 * @see #unsetValueString()
	 * @see #getValueString()
	 * @see #setValueString(ValueStringType)
	 * @generated
	 */
	boolean isSetValueString();

	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' containment reference.
	 * @see #isSetDataType()
	 * @see #unsetDataType()
	 * @see #setDataType(DataTypeType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getValueType_DataType()
	 * @model containment="true" unsettable="true"
	 *        extendedMetaData="kind='element' name='DataType' namespace='##targetNamespace'"
	 * @generated
	 */
	DataTypeType getDataType();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.ValueType#getDataType <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' containment reference.
	 * @see #isSetDataType()
	 * @see #unsetDataType()
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(DataTypeType value);

	/**
	 * Unsets the value of the '{@link org.mesa.xml.v0600.b2mml.ValueType#getDataType <em>Data Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDataType()
	 * @see #getDataType()
	 * @see #setDataType(DataTypeType)
	 * @generated
	 */
	void unsetDataType();

	/**
	 * Returns whether the value of the '{@link org.mesa.xml.v0600.b2mml.ValueType#getDataType <em>Data Type</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Data Type</em>' containment reference is set.
	 * @see #unsetDataType()
	 * @see #getDataType()
	 * @see #setDataType(DataTypeType)
	 * @generated
	 */
	boolean isSetDataType();

	/**
	 * Returns the value of the '<em><b>Unit Of Measure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Of Measure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Of Measure</em>' containment reference.
	 * @see #isSetUnitOfMeasure()
	 * @see #unsetUnitOfMeasure()
	 * @see #setUnitOfMeasure(UnitOfMeasureType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getValueType_UnitOfMeasure()
	 * @model containment="true" unsettable="true"
	 *        extendedMetaData="kind='element' name='UnitOfMeasure' namespace='##targetNamespace'"
	 * @generated
	 */
	UnitOfMeasureType getUnitOfMeasure();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.ValueType#getUnitOfMeasure <em>Unit Of Measure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Of Measure</em>' containment reference.
	 * @see #isSetUnitOfMeasure()
	 * @see #unsetUnitOfMeasure()
	 * @see #getUnitOfMeasure()
	 * @generated
	 */
	void setUnitOfMeasure(UnitOfMeasureType value);

	/**
	 * Unsets the value of the '{@link org.mesa.xml.v0600.b2mml.ValueType#getUnitOfMeasure <em>Unit Of Measure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUnitOfMeasure()
	 * @see #getUnitOfMeasure()
	 * @see #setUnitOfMeasure(UnitOfMeasureType)
	 * @generated
	 */
	void unsetUnitOfMeasure();

	/**
	 * Returns whether the value of the '{@link org.mesa.xml.v0600.b2mml.ValueType#getUnitOfMeasure <em>Unit Of Measure</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Unit Of Measure</em>' containment reference is set.
	 * @see #unsetUnitOfMeasure()
	 * @see #getUnitOfMeasure()
	 * @see #setUnitOfMeasure(UnitOfMeasureType)
	 * @generated
	 */
	boolean isSetUnitOfMeasure();

	/**
	 * Returns the value of the '<em><b>Key</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' containment reference.
	 * @see #setKey(IdentifierType)
	 * @see org.mesa.xml.v0600.b2mml.B2MMLPackage#getValueType_Key()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Key' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierType getKey();

	/**
	 * Sets the value of the '{@link org.mesa.xml.v0600.b2mml.ValueType#getKey <em>Key</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' containment reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(IdentifierType value);

} // ValueType
