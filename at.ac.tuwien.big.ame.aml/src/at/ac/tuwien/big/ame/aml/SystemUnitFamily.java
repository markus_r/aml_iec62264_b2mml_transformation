/**
 */
package at.ac.tuwien.big.ame.aml;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System Unit Family</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Defines base structures for a hierarchical SystemUnitClass tree. The hierarchical structure of a SystemUnit library has organizational character only.  
 * <!-- end-model-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.aml.AmlPackage#getSystemUnitFamily()
 * @model extendedMetaData="name='SystemUnitFamilyType' kind='elementOnly'"
 * @generated
 */
public interface SystemUnitFamily extends SystemUnitClass {
} // SystemUnitFamily
