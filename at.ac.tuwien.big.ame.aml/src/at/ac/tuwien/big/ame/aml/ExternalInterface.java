/**
 */
package at.ac.tuwien.big.ame.aml;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see at.ac.tuwien.big.ame.aml.AmlPackage#getExternalInterface()
 * @model extendedMetaData="name='ExternalInterface' kind='elementOnly'"
 * @generated
 */
public interface ExternalInterface extends InterfaceClass {
} // ExternalInterface
