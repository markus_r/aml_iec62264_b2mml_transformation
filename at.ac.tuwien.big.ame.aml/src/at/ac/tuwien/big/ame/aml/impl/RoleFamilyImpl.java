/**
 */
package at.ac.tuwien.big.ame.aml.impl;

import at.ac.tuwien.big.ame.aml.AmlPackage;
import at.ac.tuwien.big.ame.aml.RoleFamily;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Role Family</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RoleFamilyImpl extends RoleClassImpl implements RoleFamily {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleFamilyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmlPackage.Literals.ROLE_FAMILY;
	}

} //RoleFamilyImpl
