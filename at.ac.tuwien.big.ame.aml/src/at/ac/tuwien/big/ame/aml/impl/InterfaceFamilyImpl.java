/**
 */
package at.ac.tuwien.big.ame.aml.impl;

import at.ac.tuwien.big.ame.aml.AmlPackage;
import at.ac.tuwien.big.ame.aml.InterfaceFamily;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interface Family</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InterfaceFamilyImpl extends InterfaceClassImpl implements InterfaceFamily {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterfaceFamilyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmlPackage.Literals.INTERFACE_FAMILY;
	}

} //InterfaceFamilyImpl
