/**
 */
package at.ac.tuwien.big.ame.aml.impl;

import at.ac.tuwien.big.ame.aml.AmlPackage;
import at.ac.tuwien.big.ame.aml.SystemUnitFamily;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>System Unit Family</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SystemUnitFamilyImpl extends SystemUnitClassImpl implements SystemUnitFamily {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemUnitFamilyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmlPackage.Literals.SYSTEM_UNIT_FAMILY;
	}

} //SystemUnitFamilyImpl
