package at.ac.tuwien.big.ame.b2mml.codegen;

import org.eclipse.xtext.resource.generic.AbstractGenericResourceSupport;

import com.google.inject.Module;

public class B2MMLGeneratorSupport extends AbstractGenericResourceSupport {

	@Override
	protected Module createGuiceModule() {
		return new B2MMLGeneratorModule();
	}

}
