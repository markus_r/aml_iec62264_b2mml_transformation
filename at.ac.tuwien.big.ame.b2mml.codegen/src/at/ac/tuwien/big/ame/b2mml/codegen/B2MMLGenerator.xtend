package at.ac.tuwien.big.ame.b2mml.codegen

import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.ac.tuwien.big.ame.b2mml.PersonnelInformationType
import at.ac.tuwien.big.ame.b2mml.EquipmentInformationType
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetInformationType
import at.ac.tuwien.big.ame.b2mml.MaterialInformationType
import at.ac.tuwien.big.ame.b2mml.ProcessSegmentInformationType
import at.ac.tuwien.big.ame.b2mml.OperationsDefinitionInformationType
import at.ac.tuwien.big.ame.b2mml.OperationsScheduleType
import at.ac.tuwien.big.ame.b2mml.OperationsPerformanceType
import at.ac.tuwien.big.ame.b2mml.EquipmentType
import at.ac.tuwien.big.ame.b2mml.MaterialClassType
import at.ac.tuwien.big.ame.b2mml.MaterialDefinitionIDType
import at.ac.tuwien.big.ame.b2mml.MaterialDefinitionType
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetType
import at.ac.tuwien.big.ame.b2mml.PersonnelClassType
import at.ac.tuwien.big.ame.b2mml.ProcessSegmentType
import at.ac.tuwien.big.ame.b2mml.PersonType

class B2MMLGenerator implements IGenerator {

	override doGenerate(Resource input, IFileSystemAccess fsa) {
		println("Generating B2MML-PIT files...")
		for (element : input.contents) {
			val EObject root = element
			generateB2mmlFile(root, fsa)
		}
		println("B2MML files completed!")
	}
	
	def dispatch generateB2mmlFile(PersonnelInformationType pit, IFileSystemAccess fsa) {
		fsa.generateFile("personnelInformationType.b2mml", generatePitContent(pit))
	}
	
	private def generatePitContent(PersonnelInformationType pit){
		'''
		<?xml version="1.0" encoding="UTF-8"?>
		<b2mml:PersonnelInformation xmlns:b2mml="http://www.mesa.org/xml/B2MML-V0600">
			�FOR pc : pit.personnelClass�
				�generatePersonnelClass(pc)�
			�ENDFOR�
			�FOR p : pit.person�
				�generatePerson(p)�
			�ENDFOR�
		</b2mml:PersonnelInformation>
		'''
	}
	
	private def generatePersonnelClass(PersonnelClassType pct){
		'''
		<?xml version="1.0" encoding="UTF-8"?>
		<b2mml:PersonnelClass xmlns:b2mml="http://www.mesa.org/xml/B2MML-V0600">
			<b2mml:ID>�pct.ID?.value�</b2mml:ID>
			�FOR desc : pct.description�
			<b2mml:Description>�desc.value�</b2mml:Description>
			�ENDFOR�
		</b2mml:PersonnelClass>
		'''
	}
	
	private def generatePerson(PersonType p){
		'''
		<?xml version="1.0" encoding="UTF-8"?>
		<b2mml:Person xmlns:b2mml="http://www.mesa.org/xml/B2MML-V0600">
			<b2mml:ID>�p.ID?.value�</b2mml:ID>
			�FOR desc : p.description�
			<b2mml:Description>�desc.value�</b2mml:Description>
			�ENDFOR�
			�FOR pp : p.personProperty�
			<b2mml:PersonProperty>
				<b2mml:ID>�pp.ID�</b2mml:ID>
				<b2mml:Value>�pp.value�</b2mml:Value>
			</b2mml:PersonProperty>
			�ENDFOR�
			�FOR pc : p.personnelClassID�
			<b2mml:PersonnelClassID>�pc.value�</b2mml:PersonnelClassID>
			�ENDFOR�
		</b2mml:Person>
		'''
	}
	
	def dispatch generateB2mmlFile(EquipmentInformationType eit, IFileSystemAccess fsa) {
		fsa.generateFile("equipmentInformationType.b2mml", generateEitContent(eit))
	}
	
	private def generateEitContent(EquipmentInformationType eit){
		'''
		<?xml version="1.0" encoding="UTF-8"?>
		<b2mml:EquipmentInformation xmlns:b2mml="http://www.mesa.org/xml/B2MML-V0600">
			�FOR eq : eit.equipment�
			�generateEquipment(eq)�
			�ENDFOR�
		</b2mml:EquipmentInformation>
		'''
	}
	
	private def generateEquipment(EquipmentType et){
		'''
			<b2mml:Equipment>
				<b2mml:ID>�et.ID?.value�</b2mml:ID>
				�FOR desc : et.description�
				<b2mml:Description>�desc.value�</b2mml:Description>
				�ENDFOR�
				�IF et.equipmentLevel !== null�
				<b2mml:EquipmentLevel>
					<b2mml:EquipmentElementLevel>�et.equipmentLevel.hierarchyScope?.equipmentID?.value�</b2mml:EquipmentElementLevel>
				</b2mml:EquipmentLevel>
				�ENDIF�
			</b2mml:Equipment>
		'''
	}	
	
	def dispatch generateB2mmlFile(PhysicalAssetInformationType pait, IFileSystemAccess fsa) {
		fsa.generateFile("pyhsicalAssetInformationType.b2mml", generatePaitContent(pait))
	}
	
	private def generatePaitContent(PhysicalAssetInformationType pait){
		'''
		<PhysicalAssetInformation xmlns="http://www.mesa.org/xml/B2MML-V0600">
		  �FOR pa : pait.physicalAsset�
		  �generatePhysicalAsset(pa)�
		  �ENDFOR�
		  <PhysicalAssetClass>
		    <ID>Short-Conveyer-PA-Class</ID>
		    <Description>The short conveyer of Conveyer Manufacturer.</Description>
		    <Manufacturer>Conveyer Manufacturer</Manufacturer>
		    <PhysicalAssetID>Conveyer-1-PA</PhysicalAssetID>
		  </PhysicalAssetClass>
		</PhysicalAssetInformation>
		'''
	}
	
	private def generatePhysicalAsset(PhysicalAssetType pat){
		'''
		<PhysicalAsset>
			<ID>�pat.ID.value�</ID>
			�FOR desc : pat.description�
			<Description>�desc.value�</b2mml:Description>
			�ENDFOR�
			�IF pat.physicalLocation !== null�
				<PhysicalLocation>�pat.physicalLocation.value�</PhysicalLocation>
			�ENDIF�
			�IF pat.fixedAssetID !== null�
				<FixedAssetID>�pat.fixedAssetID.value�</FixedAssetID>
			�ENDIF�	
			�IF pat.vendorID !== null�
						<VendorID>�pat.vendorID.value�</VendorID>
			�ENDIF�
			�IF pat.equipmentAssetMapping.size != 0�
				�FOR eam : pat.equipmentAssetMapping�
					<EquipmentAssetMapping>
						<EquipmentID>�eam.equipmentID.value�</EquipmentID>
						<PhysicalAssetID>�eam.physicalAssetID.value�</PhysicalAssetID>
						�IF eam.startTime !== null�
							<StartTime>�eam.startTime.value�</StartTime>
						�ENDIF�
						�IF eam.endTime !== null�
							<EndTime>�eam.endTime.value�</EndTime>
						�ENDIF�
					</EquipmentAssetMapping>
				�ENDFOR�
			�ENDIF�
			<PhysicalAssetClassID>Robot-PA-Class</PhysicalAssetClassID>
		</PhysicalAsset>
		'''
	}
	
	def dispatch generateB2mmlFile(MaterialInformationType mit, IFileSystemAccess fsa) {
		fsa.generateFile("materialInformationType.b2mml", generateMitContent(mit))
	}
	
	private def generateMitContent(MaterialInformationType mit){
		'''
		<?xml version="1.0" encoding="UTF-8"?>
		<b2mml:MaterialInformation xmlns:b2mml="http://www.mesa.org/xml/B2MML-V0600">
			�FOR mc : mit.materialClass�
				�generateMaterialClass(mc)�
			�ENDFOR�
			�FOR md : mit.materialDefinition�
				�generateMaterialDefinition(md)�
			�ENDFOR�
		</b2mml:MaterialInformation>
		'''
	}
	
	private def generateMaterialClass(MaterialClassType mct){
		'''
		<b2mml:MaterialClass>
			<b2mml:ID>�mct.ID?.value�</b2mml:ID>
			�FOR desc : mct.description�
				<b2mml:Description>�desc.value�</b2mml:Description>
			�ENDFOR�
			�FOR mdId: mct.materialDefinitionID�
				<b2mml:MaterialDefinitionID>�mdId�</b2mml:MaterialDefinitionID>
			�ENDFOR�
			�FOR acId : mct.assemblyClassID�
				<b2mml:AssemblyClassID>�acId�</b2mml:AssemblyClassID>
			�ENDFOR�
			�IF mct.assemblyType !== null�
				<b2mml:AssemblyType>�mct.assemblyType.value�</b2mml:AssemblyType>		
			�ENDIF�
			�IF mct.assemblyRelationship !== null�
				<b2mml:AssemblyRelationship>�mct.assemblyRelationship.value�</b2mml:AssemblyRelationship>
			�ENDIF�
		</b2mml:MaterialClass>
		'''
	}
	
	private def generateMaterialDefinition(MaterialDefinitionType mdt){
		'''
		<b2mml:MaterialDefinition>
			<b2mml:ID>�mdt.ID?.value�</b2mml:ID>
			�FOR desc : mdt.description�
				<b2mml:Description>�desc.value�</b2mml:Description>
			�ENDFOR�
			�FOR mdId: mdt.materialDefinitionProperty�
				�FOR mdp: mdId.value�
				<b2mml:MaterialDefinitionID>�mdp.valueString?.value�</b2mml:MaterialDefinitionID>
				�ENDFOR�
			�ENDFOR�
			�IF mdt.assemblyType !== null�
				<b2mml:AssemblyType>�mdt.assemblyType.value�</b2mml:AssemblyType>		
			�ENDIF�
			�IF mdt.assemblyRelationship !== null�
				<b2mml:AssemblyRelationship>�mdt.assemblyRelationship.value�</b2mml:AssemblyRelationship>
			�ENDIF�
		</b2mml:MaterialDefinition>
		'''
	}
	
	def dispatch generateB2mmlFile(ProcessSegmentInformationType psit, IFileSystemAccess fsa) {
		fsa.generateFile("processSegmentInformationType.b2mml", generatePsitContent(psit))
	}
	
	private def generatePsitContent(ProcessSegmentInformationType psit){
		'''
		<?xml version="1.0" encoding="UTF-8"?>
		<b2mml:ProcessSegmentInformation xmlns:b2mml="http://www.mesa.org/xml/B2MML-V0600">
		�FOR ps : psit.processSegment�
			�generateProcessSegment(ps)�
		�ENDFOR�
		</b2mml:ProcessSegmentInformation>
		'''
	}
	
	private def generateProcessSegment(ProcessSegmentType pst){
		'''
		<b2mml:ProcessSegment>
			<b2mml:ID>�pst.ID?.value�</b2mml:ID>
		    �FOR desc : pst.description�
		    <b2mml:Description>�desc.value�</b2mml:Description>
		    �ENDFOR�
		    �IF pst.operationsType !== null�
		    <b2mml:OperationsType>�pst.operationsType.value�</b2mml:OperationsType>
			�ENDIF�
			�IF pst.duration !== null�
			<b2mml:Duration>�pst.duration.toString�</b2mml:Duration>
			�ENDIF�
		    �FOR es : pst.equipmentSegmentSpecification�
			<b2mml:EquipmentSegmentSpecification>
				�FOR desc : es.description�
				<b2mml:Description>�desc.value�</b2mml:Description>
				�ENDFOR�
				�IF es.equipmentClassID !== null�
					<b2mml:EquipmentClassID>�es.equipmentClassID.value�</b2mml:EquipmentClassID>
				�ENDIF�
				�IF es.equipmentID !== null�
					<b2mml:EquipmentID>�es.equipmentID.value�</b2mml:EquipmentID>
				�ENDIF�
				�IF es.equipmentUse !== null�
				<b2mml:EquipmentUse>�es.equipmentUse.value�</b2mml:EquipmentUse>
				�ENDIF�
				�FOR q : es.quantity�
					<b2mml:Quantity>
						<b2mml:QuantityString>�q.quantityString?.value�</b2mml:QuantityString>
						<b2mml:UnitOfMeasure>�q.unitOfMeasure?.value�</b2mml:UnitOfMeasure>
					</b2mml:Quantity>
				�ENDFOR�
			</b2mml:EquipmentSegmentSpecification>
		    �ENDFOR�
			�FOR ms : pst.materialSegmentSpecification�
		    <b2mml:MaterialSegmentSpecification>
				�FOR desc : ms.description�
				<b2mml:Description>�desc.value�</b2mml:Description>
				�ENDFOR�
				<b2mml:MaterialDefinitionID>�ms.materialDefinitionID?.value�</b2mml:MaterialDefinitionID>
				<b2mml:MaterialClassID>�ms.materialClassID?.value�</b2mml:MaterialClassID>
				<b2mml:MaterialUse>�ms.materialUse?.value�</b2mml:MaterialUse>
				�FOR q : ms.quantity�
					<b2mml:Quantity>
						<b2mml:QuantityString>�q.quantityString?.value�</b2mml:QuantityString>
						<b2mml:UnitOfMeasure>�q.unitOfMeasure?.value�</b2mml:UnitOfMeasure>
					</b2mml:Quantity>
				�ENDFOR�
			</b2mml:MaterialSegmentSpecification>
		    �ENDFOR�
		    �FOR pas : pst.physicalAssetSegmentSpecification�
			<b2mml:PhysicalAssetSegmentSpecification>
				�FOR desc : pas.description�
				<b2mml:Description>�desc.value�</b2mml:Description>
				�ENDFOR�
				<b2mml:PhysicalAssetClassID>�pas.physicalAssetClassID?.value�</b2mml:PhysicalAssetClassID>
				<b2mml:PhysicalAssetID>�pas.physicalAssetID?.value�</b2mml:PhysicalAssetID>
				�FOR q : pas.quantity�
					<b2mml:Quantity>
						<b2mml:QuantityString>�q.quantityString?.value�</b2mml:QuantityString>
						<b2mml:UnitOfMeasure>�q.unitOfMeasure?.value�</b2mml:UnitOfMeasure>
					</b2mml:Quantity>
				�ENDFOR�
			</b2mml:PhysicalAssetSegmentSpecification>
		    �ENDFOR�
		    �FOR sd : pst.segmentDependency�
			<b2mml:SegmentDependency>
				<b2mml:ID>�sd.ID?.value�</b2mml:ID>
		      	�FOR desc : sd.description�
			  	<b2mml:Description>�desc.value�</b2mml:Description>
				�ENDFOR�
				<b2mml:Dependency>�sd.dependency?.value�</b2mml:Dependency>
		      	�FOR ps : sd.processSegmentID�
				<b2mml:ProcessSegmentID>�ps.value�</b2mml:ProcessSegmentID>
		      	�ENDFOR�
		    </b2mml:SegmentDependency>
		    �ENDFOR�
		</b2mml:ProcessSegment>
		'''
	}	
	
	def dispatch generateB2mmlFile(OperationsDefinitionInformationType odit, IFileSystemAccess fsa) {
		fsa.generateFile("operationsDefinitionInformationType.b2mml", generateOditContent(odit))
	}
	
	private def generateOditContent(OperationsDefinitionInformationType odit){
		'''
		'''
	}
	
	def dispatch generateB2mmlFile(OperationsScheduleType osit, IFileSystemAccess fsa) {
		fsa.generateFile("operationsScheduleType.b2mml", generateOsitContent(osit))
	}
	
	private def generateOsitContent(OperationsScheduleType osit){
		'''
		'''
	}
	
	def dispatch generateB2mmlFile(OperationsPerformanceType opit, IFileSystemAccess fsa) {
		fsa.generateFile("operationsPerformanceType.b2mml", generateOpitContent(opit))
	}
	
	private def generateOpitContent(OperationsPerformanceType opit){
		'''
		'''
	}
	
	def dispatch generateB2mmlFile(EObject object, IFileSystemAccess access) {
		println("AMLCodeGenerator: Ignoring model element: " + object)
	}

	/**
	 * Formats an Integer by introducing thousands separator.
	 * 
	 * @param i Integer that should be formatted
	 * @return formatted Integer value as String
	 */
	private def formatInteger(int i) {
		var ic = i
		var ret = ""
		while (ic >= 1000) {
			var part = "" + (ic % 1000)
			while (part.length < 3) {
				part = "0" + part
			}
			ret = "." + part + ret
			ic = ic / 1000
		}
		ret = ic + ret
		ret
	}

	/**
	 * Formats a double value by introducing floating point.
	 * 
	 * @param d double value that should be formatted
	 * @return formatted double value as String
	 */
	private def formatDouble(double d) {
		if (d == Math.round(d)) {
			return String.valueOf(d as long)
		}
		val ret = Math.round(d * 10);
		ret / 10 + "," + (ret % 10)
	}

	/**
	 * Returns a String which is stripped of all non alpha-numerical values. 
	 * This is needed for computing the identifiers (id) of HTML elements as well as 
	 * the file names for details pages.
	 * 
	 * @param string String for which non alpha-numerical values should be removed
	 * @return String with removed non alpha-numerical values
	 */
	private def toAlphaNumerical(String string) {
		string.replaceAll("[^A-Za-z0-9]", "")
	}
	
	/**
	 * Computes the output directory in which the generated HTML files should be places.
	 * 
	 * @param system STL system for which HTML code is generated
	 * @return name of the output directory in which the generated HTML files should be placed
	 *
	private def getOutputDir(CAEXFile caexFile) {
		val systemName = caexFile?.fileName
		val systemNameInLowerCase = systemName.toLowerCase
		val systemNameWithoutSpaces = systemNameInLowerCase.replace(" ", "")
		systemNameWithoutSpaces
	}*/
}
