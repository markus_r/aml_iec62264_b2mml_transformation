package at.ac.tuwien.big.ame.b2mml.codegen;

import at.ac.tuwien.big.ame.b2mml.AssemblyRelationshipType;
import at.ac.tuwien.big.ame.b2mml.AssemblyTypeType;
import at.ac.tuwien.big.ame.b2mml.CodeType;
import at.ac.tuwien.big.ame.b2mml.DateTimeType;
import at.ac.tuwien.big.ame.b2mml.DependencyType;
import at.ac.tuwien.big.ame.b2mml.DescriptionType;
import at.ac.tuwien.big.ame.b2mml.EquipmentAssetMappingType;
import at.ac.tuwien.big.ame.b2mml.EquipmentClassIDType;
import at.ac.tuwien.big.ame.b2mml.EquipmentIDType;
import at.ac.tuwien.big.ame.b2mml.EquipmentInformationType;
import at.ac.tuwien.big.ame.b2mml.EquipmentSegmentSpecificationType;
import at.ac.tuwien.big.ame.b2mml.EquipmentType;
import at.ac.tuwien.big.ame.b2mml.HierarchyScopeType;
import at.ac.tuwien.big.ame.b2mml.IdentifierType;
import at.ac.tuwien.big.ame.b2mml.MaterialClassIDType;
import at.ac.tuwien.big.ame.b2mml.MaterialClassType;
import at.ac.tuwien.big.ame.b2mml.MaterialDefinitionIDType;
import at.ac.tuwien.big.ame.b2mml.MaterialDefinitionPropertyType;
import at.ac.tuwien.big.ame.b2mml.MaterialDefinitionType;
import at.ac.tuwien.big.ame.b2mml.MaterialInformationType;
import at.ac.tuwien.big.ame.b2mml.MaterialSegmentSpecificationType;
import at.ac.tuwien.big.ame.b2mml.MaterialUseType;
import at.ac.tuwien.big.ame.b2mml.OperationsDefinitionInformationType;
import at.ac.tuwien.big.ame.b2mml.OperationsPerformanceType;
import at.ac.tuwien.big.ame.b2mml.OperationsScheduleType;
import at.ac.tuwien.big.ame.b2mml.OperationsTypeType;
import at.ac.tuwien.big.ame.b2mml.PersonPropertyType;
import at.ac.tuwien.big.ame.b2mml.PersonType;
import at.ac.tuwien.big.ame.b2mml.PersonnelClassIDType;
import at.ac.tuwien.big.ame.b2mml.PersonnelClassType;
import at.ac.tuwien.big.ame.b2mml.PersonnelInformationType;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetClassIDType;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetIDType;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetInformationType;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetSegmentSpecificationType;
import at.ac.tuwien.big.ame.b2mml.PhysicalAssetType;
import at.ac.tuwien.big.ame.b2mml.ProcessSegmentIDType;
import at.ac.tuwien.big.ame.b2mml.ProcessSegmentInformationType;
import at.ac.tuwien.big.ame.b2mml.ProcessSegmentType;
import at.ac.tuwien.big.ame.b2mml.QuantityStringType;
import at.ac.tuwien.big.ame.b2mml.QuantityValueType;
import at.ac.tuwien.big.ame.b2mml.SegmentDependencyType;
import at.ac.tuwien.big.ame.b2mml.UnitOfMeasureType;
import at.ac.tuwien.big.ame.b2mml.ValueStringType;
import at.ac.tuwien.big.ame.b2mml.ValueType;
import java.util.Arrays;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.xbase.lib.InputOutput;

@SuppressWarnings("all")
public class B2MMLGenerator implements IGenerator {
  @Override
  public void doGenerate(final Resource input, final IFileSystemAccess fsa) {
    InputOutput.<String>println("Generating B2MML-PIT files...");
    EList<EObject> _contents = input.getContents();
    for (final EObject element : _contents) {
      {
        final EObject root = element;
        this.generateB2mmlFile(root, fsa);
      }
    }
    InputOutput.<String>println("B2MML files completed!");
  }
  
  protected String _generateB2mmlFile(final PersonnelInformationType pit, final IFileSystemAccess fsa) {
    fsa.generateFile("personnelInformationType.b2mml", this.generatePitContent(pit));
    return null;
  }
  
  private CharSequence generatePitContent(final PersonnelInformationType pit) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<b2mml:PersonnelInformation xmlns:b2mml=\"http://www.mesa.org/xml/B2MML-V0600\">");
    _builder.newLine();
    {
      EList<PersonnelClassType> _personnelClass = pit.getPersonnelClass();
      for(final PersonnelClassType pc : _personnelClass) {
        _builder.append("\t");
        CharSequence _generatePersonnelClass = this.generatePersonnelClass(pc);
        _builder.append(_generatePersonnelClass, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<PersonType> _person = pit.getPerson();
      for(final PersonType p : _person) {
        _builder.append("\t");
        CharSequence _generatePerson = this.generatePerson(p);
        _builder.append(_generatePerson, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</b2mml:PersonnelInformation>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generatePersonnelClass(final PersonnelClassType pct) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<b2mml:PersonnelClass xmlns:b2mml=\"http://www.mesa.org/xml/B2MML-V0600\">");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<b2mml:ID>");
    IdentifierType _iD = pct.getID();
    String _value = null;
    if (_iD!=null) {
      _value=_iD.getValue();
    }
    _builder.append(_value, "\t");
    _builder.append("</b2mml:ID>");
    _builder.newLineIfNotEmpty();
    {
      EList<DescriptionType> _description = pct.getDescription();
      for(final DescriptionType desc : _description) {
        _builder.append("\t");
        _builder.append("<b2mml:Description>");
        String _value_1 = desc.getValue();
        _builder.append(_value_1, "\t");
        _builder.append("</b2mml:Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</b2mml:PersonnelClass>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generatePerson(final PersonType p) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<b2mml:Person xmlns:b2mml=\"http://www.mesa.org/xml/B2MML-V0600\">");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<b2mml:ID>");
    IdentifierType _iD = p.getID();
    String _value = null;
    if (_iD!=null) {
      _value=_iD.getValue();
    }
    _builder.append(_value, "\t");
    _builder.append("</b2mml:ID>");
    _builder.newLineIfNotEmpty();
    {
      EList<DescriptionType> _description = p.getDescription();
      for(final DescriptionType desc : _description) {
        _builder.append("\t");
        _builder.append("<b2mml:Description>");
        String _value_1 = desc.getValue();
        _builder.append(_value_1, "\t");
        _builder.append("</b2mml:Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<PersonPropertyType> _personProperty = p.getPersonProperty();
      for(final PersonPropertyType pp : _personProperty) {
        _builder.append("\t");
        _builder.append("<b2mml:PersonProperty>");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("<b2mml:ID>");
        IdentifierType _iD_1 = pp.getID();
        _builder.append(_iD_1, "\t\t");
        _builder.append("</b2mml:ID>");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("<b2mml:Value>");
        EList<ValueType> _value_2 = pp.getValue();
        _builder.append(_value_2, "\t\t");
        _builder.append("</b2mml:Value>");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("</b2mml:PersonProperty>");
        _builder.newLine();
      }
    }
    {
      EList<PersonnelClassIDType> _personnelClassID = p.getPersonnelClassID();
      for(final PersonnelClassIDType pc : _personnelClassID) {
        _builder.append("\t");
        _builder.append("<b2mml:PersonnelClassID>");
        String _value_3 = pc.getValue();
        _builder.append(_value_3, "\t");
        _builder.append("</b2mml:PersonnelClassID>");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</b2mml:Person>");
    _builder.newLine();
    return _builder;
  }
  
  protected String _generateB2mmlFile(final EquipmentInformationType eit, final IFileSystemAccess fsa) {
    fsa.generateFile("equipmentInformationType.b2mml", this.generateEitContent(eit));
    return null;
  }
  
  private CharSequence generateEitContent(final EquipmentInformationType eit) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<b2mml:EquipmentInformation xmlns:b2mml=\"http://www.mesa.org/xml/B2MML-V0600\">");
    _builder.newLine();
    {
      EList<EquipmentType> _equipment = eit.getEquipment();
      for(final EquipmentType eq : _equipment) {
        _builder.append("\t");
        CharSequence _generateEquipment = this.generateEquipment(eq);
        _builder.append(_generateEquipment, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</b2mml:EquipmentInformation>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateEquipment(final EquipmentType et) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<b2mml:Equipment>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<b2mml:ID>");
    IdentifierType _iD = et.getID();
    String _value = null;
    if (_iD!=null) {
      _value=_iD.getValue();
    }
    _builder.append(_value, "\t");
    _builder.append("</b2mml:ID>");
    _builder.newLineIfNotEmpty();
    {
      EList<DescriptionType> _description = et.getDescription();
      for(final DescriptionType desc : _description) {
        _builder.append("\t");
        _builder.append("<b2mml:Description>");
        String _value_1 = desc.getValue();
        _builder.append(_value_1, "\t");
        _builder.append("</b2mml:Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      HierarchyScopeType _equipmentLevel = et.getEquipmentLevel();
      boolean _tripleNotEquals = (_equipmentLevel != null);
      if (_tripleNotEquals) {
        _builder.append("\t");
        _builder.append("<b2mml:EquipmentLevel>");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("<b2mml:EquipmentElementLevel>");
        HierarchyScopeType _hierarchyScope = et.getEquipmentLevel().getHierarchyScope();
        EquipmentIDType _equipmentID = null;
        if (_hierarchyScope!=null) {
          _equipmentID=_hierarchyScope.getEquipmentID();
        }
        String _value_2 = null;
        if (_equipmentID!=null) {
          _value_2=_equipmentID.getValue();
        }
        _builder.append(_value_2, "\t\t");
        _builder.append("</b2mml:EquipmentElementLevel>");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("</b2mml:EquipmentLevel>");
        _builder.newLine();
      }
    }
    _builder.append("</b2mml:Equipment>");
    _builder.newLine();
    return _builder;
  }
  
  protected String _generateB2mmlFile(final PhysicalAssetInformationType pait, final IFileSystemAccess fsa) {
    fsa.generateFile("pyhsicalAssetInformationType.b2mml", this.generatePaitContent(pait));
    return null;
  }
  
  private CharSequence generatePaitContent(final PhysicalAssetInformationType pait) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<PhysicalAssetInformation xmlns=\"http://www.mesa.org/xml/B2MML-V0600\">");
    _builder.newLine();
    {
      EList<PhysicalAssetType> _physicalAsset = pait.getPhysicalAsset();
      for(final PhysicalAssetType pa : _physicalAsset) {
        _builder.append("  ");
        CharSequence _generatePhysicalAsset = this.generatePhysicalAsset(pa);
        _builder.append(_generatePhysicalAsset, "  ");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("  ");
    _builder.append("<PhysicalAssetClass>");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<ID>Short-Conveyer-PA-Class</ID>");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<Description>The short conveyer of Conveyer Manufacturer.</Description>");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<Manufacturer>Conveyer Manufacturer</Manufacturer>");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<PhysicalAssetID>Conveyer-1-PA</PhysicalAssetID>");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("</PhysicalAssetClass>");
    _builder.newLine();
    _builder.append("</PhysicalAssetInformation>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generatePhysicalAsset(final PhysicalAssetType pat) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<PhysicalAsset>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<ID>");
    String _value = pat.getID().getValue();
    _builder.append(_value, "\t");
    _builder.append("</ID>");
    _builder.newLineIfNotEmpty();
    {
      EList<DescriptionType> _description = pat.getDescription();
      for(final DescriptionType desc : _description) {
        _builder.append("\t");
        _builder.append("<Description>");
        String _value_1 = desc.getValue();
        _builder.append(_value_1, "\t");
        _builder.append("</b2mml:Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      IdentifierType _physicalLocation = pat.getPhysicalLocation();
      boolean _tripleNotEquals = (_physicalLocation != null);
      if (_tripleNotEquals) {
        _builder.append("\t");
        _builder.append("<PhysicalLocation>");
        String _value_2 = pat.getPhysicalLocation().getValue();
        _builder.append(_value_2, "\t");
        _builder.append("</PhysicalLocation>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      IdentifierType _fixedAssetID = pat.getFixedAssetID();
      boolean _tripleNotEquals_1 = (_fixedAssetID != null);
      if (_tripleNotEquals_1) {
        _builder.append("\t");
        _builder.append("<FixedAssetID>");
        String _value_3 = pat.getFixedAssetID().getValue();
        _builder.append(_value_3, "\t");
        _builder.append("</FixedAssetID>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      IdentifierType _vendorID = pat.getVendorID();
      boolean _tripleNotEquals_2 = (_vendorID != null);
      if (_tripleNotEquals_2) {
        _builder.append("\t");
        _builder.append("<VendorID>");
        String _value_4 = pat.getVendorID().getValue();
        _builder.append(_value_4, "\t");
        _builder.append("</VendorID>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      int _size = pat.getEquipmentAssetMapping().size();
      boolean _notEquals = (_size != 0);
      if (_notEquals) {
        {
          EList<EquipmentAssetMappingType> _equipmentAssetMapping = pat.getEquipmentAssetMapping();
          for(final EquipmentAssetMappingType eam : _equipmentAssetMapping) {
            _builder.append("\t");
            _builder.append("<EquipmentAssetMapping>");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("<EquipmentID>");
            String _value_5 = eam.getEquipmentID().getValue();
            _builder.append(_value_5, "\t\t");
            _builder.append("</EquipmentID>");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("<PhysicalAssetID>");
            String _value_6 = eam.getPhysicalAssetID().getValue();
            _builder.append(_value_6, "\t\t");
            _builder.append("</PhysicalAssetID>");
            _builder.newLineIfNotEmpty();
            {
              DateTimeType _startTime = eam.getStartTime();
              boolean _tripleNotEquals_3 = (_startTime != null);
              if (_tripleNotEquals_3) {
                _builder.append("\t");
                _builder.append("\t");
                _builder.append("<StartTime>");
                XMLGregorianCalendar _value_7 = eam.getStartTime().getValue();
                _builder.append(_value_7, "\t\t");
                _builder.append("</StartTime>");
                _builder.newLineIfNotEmpty();
              }
            }
            {
              DateTimeType _endTime = eam.getEndTime();
              boolean _tripleNotEquals_4 = (_endTime != null);
              if (_tripleNotEquals_4) {
                _builder.append("\t");
                _builder.append("\t");
                _builder.append("<EndTime>");
                XMLGregorianCalendar _value_8 = eam.getEndTime().getValue();
                _builder.append(_value_8, "\t\t");
                _builder.append("</EndTime>");
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("\t");
            _builder.append("</EquipmentAssetMapping>");
            _builder.newLine();
          }
        }
      }
    }
    _builder.append("\t");
    _builder.append("<PhysicalAssetClassID>Robot-PA-Class</PhysicalAssetClassID>");
    _builder.newLine();
    _builder.append("</PhysicalAsset>");
    _builder.newLine();
    return _builder;
  }
  
  protected String _generateB2mmlFile(final MaterialInformationType mit, final IFileSystemAccess fsa) {
    fsa.generateFile("materialInformationType.b2mml", this.generateMitContent(mit));
    return null;
  }
  
  private CharSequence generateMitContent(final MaterialInformationType mit) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<b2mml:MaterialInformation xmlns:b2mml=\"http://www.mesa.org/xml/B2MML-V0600\">");
    _builder.newLine();
    {
      EList<MaterialClassType> _materialClass = mit.getMaterialClass();
      for(final MaterialClassType mc : _materialClass) {
        _builder.append("\t");
        CharSequence _generateMaterialClass = this.generateMaterialClass(mc);
        _builder.append(_generateMaterialClass, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<MaterialDefinitionType> _materialDefinition = mit.getMaterialDefinition();
      for(final MaterialDefinitionType md : _materialDefinition) {
        _builder.append("\t");
        CharSequence _generateMaterialDefinition = this.generateMaterialDefinition(md);
        _builder.append(_generateMaterialDefinition, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</b2mml:MaterialInformation>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateMaterialClass(final MaterialClassType mct) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<b2mml:MaterialClass>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<b2mml:ID>");
    IdentifierType _iD = mct.getID();
    String _value = null;
    if (_iD!=null) {
      _value=_iD.getValue();
    }
    _builder.append(_value, "\t");
    _builder.append("</b2mml:ID>");
    _builder.newLineIfNotEmpty();
    {
      EList<DescriptionType> _description = mct.getDescription();
      for(final DescriptionType desc : _description) {
        _builder.append("\t");
        _builder.append("<b2mml:Description>");
        String _value_1 = desc.getValue();
        _builder.append(_value_1, "\t");
        _builder.append("</b2mml:Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<MaterialDefinitionIDType> _materialDefinitionID = mct.getMaterialDefinitionID();
      for(final MaterialDefinitionIDType mdId : _materialDefinitionID) {
        _builder.append("\t");
        _builder.append("<b2mml:MaterialDefinitionID>");
        _builder.append(mdId, "\t");
        _builder.append("</b2mml:MaterialDefinitionID>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<MaterialClassIDType> _assemblyClassID = mct.getAssemblyClassID();
      for(final MaterialClassIDType acId : _assemblyClassID) {
        _builder.append("\t");
        _builder.append("<b2mml:AssemblyClassID>");
        _builder.append(acId, "\t");
        _builder.append("</b2mml:AssemblyClassID>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      AssemblyTypeType _assemblyType = mct.getAssemblyType();
      boolean _tripleNotEquals = (_assemblyType != null);
      if (_tripleNotEquals) {
        _builder.append("\t");
        _builder.append("<b2mml:AssemblyType>");
        String _value_2 = mct.getAssemblyType().getValue();
        _builder.append(_value_2, "\t");
        _builder.append("</b2mml:AssemblyType>\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      AssemblyRelationshipType _assemblyRelationship = mct.getAssemblyRelationship();
      boolean _tripleNotEquals_1 = (_assemblyRelationship != null);
      if (_tripleNotEquals_1) {
        _builder.append("\t");
        _builder.append("<b2mml:AssemblyRelationship>");
        String _value_3 = mct.getAssemblyRelationship().getValue();
        _builder.append(_value_3, "\t");
        _builder.append("</b2mml:AssemblyRelationship>");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</b2mml:MaterialClass>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateMaterialDefinition(final MaterialDefinitionType mdt) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<b2mml:MaterialDefinition>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<b2mml:ID>");
    IdentifierType _iD = mdt.getID();
    String _value = null;
    if (_iD!=null) {
      _value=_iD.getValue();
    }
    _builder.append(_value, "\t");
    _builder.append("</b2mml:ID>");
    _builder.newLineIfNotEmpty();
    {
      EList<DescriptionType> _description = mdt.getDescription();
      for(final DescriptionType desc : _description) {
        _builder.append("\t");
        _builder.append("<b2mml:Description>");
        String _value_1 = desc.getValue();
        _builder.append(_value_1, "\t");
        _builder.append("</b2mml:Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<MaterialDefinitionPropertyType> _materialDefinitionProperty = mdt.getMaterialDefinitionProperty();
      for(final MaterialDefinitionPropertyType mdId : _materialDefinitionProperty) {
        {
          EList<ValueType> _value_2 = mdId.getValue();
          for(final ValueType mdp : _value_2) {
            _builder.append("\t");
            _builder.append("<b2mml:MaterialDefinitionID>");
            ValueStringType _valueString = mdp.getValueString();
            String _value_3 = null;
            if (_valueString!=null) {
              _value_3=_valueString.getValue();
            }
            _builder.append(_value_3, "\t");
            _builder.append("</b2mml:MaterialDefinitionID>");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    {
      AssemblyTypeType _assemblyType = mdt.getAssemblyType();
      boolean _tripleNotEquals = (_assemblyType != null);
      if (_tripleNotEquals) {
        _builder.append("\t");
        _builder.append("<b2mml:AssemblyType>");
        String _value_4 = mdt.getAssemblyType().getValue();
        _builder.append(_value_4, "\t");
        _builder.append("</b2mml:AssemblyType>\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      AssemblyRelationshipType _assemblyRelationship = mdt.getAssemblyRelationship();
      boolean _tripleNotEquals_1 = (_assemblyRelationship != null);
      if (_tripleNotEquals_1) {
        _builder.append("\t");
        _builder.append("<b2mml:AssemblyRelationship>");
        String _value_5 = mdt.getAssemblyRelationship().getValue();
        _builder.append(_value_5, "\t");
        _builder.append("</b2mml:AssemblyRelationship>");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</b2mml:MaterialDefinition>");
    _builder.newLine();
    return _builder;
  }
  
  protected String _generateB2mmlFile(final ProcessSegmentInformationType psit, final IFileSystemAccess fsa) {
    fsa.generateFile("processSegmentInformationType.b2mml", this.generatePsitContent(psit));
    return null;
  }
  
  private CharSequence generatePsitContent(final ProcessSegmentInformationType psit) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<b2mml:ProcessSegmentInformation xmlns:b2mml=\"http://www.mesa.org/xml/B2MML-V0600\">");
    _builder.newLine();
    {
      EList<ProcessSegmentType> _processSegment = psit.getProcessSegment();
      for(final ProcessSegmentType ps : _processSegment) {
        CharSequence _generateProcessSegment = this.generateProcessSegment(ps);
        _builder.append(_generateProcessSegment);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("</b2mml:ProcessSegmentInformation>");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence generateProcessSegment(final ProcessSegmentType pst) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<b2mml:ProcessSegment>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<b2mml:ID>");
    IdentifierType _iD = pst.getID();
    String _value = null;
    if (_iD!=null) {
      _value=_iD.getValue();
    }
    _builder.append(_value, "\t");
    _builder.append("</b2mml:ID>");
    _builder.newLineIfNotEmpty();
    {
      EList<DescriptionType> _description = pst.getDescription();
      for(final DescriptionType desc : _description) {
        _builder.append("    ");
        _builder.append("<b2mml:Description>");
        String _value_1 = desc.getValue();
        _builder.append(_value_1, "    ");
        _builder.append("</b2mml:Description>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      OperationsTypeType _operationsType = pst.getOperationsType();
      boolean _tripleNotEquals = (_operationsType != null);
      if (_tripleNotEquals) {
        _builder.append("    ");
        _builder.append("<b2mml:OperationsType>");
        String _value_2 = pst.getOperationsType().getValue();
        _builder.append(_value_2, "    ");
        _builder.append("</b2mml:OperationsType>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      Duration _duration = pst.getDuration();
      boolean _tripleNotEquals_1 = (_duration != null);
      if (_tripleNotEquals_1) {
        _builder.append("\t");
        _builder.append("<b2mml:Duration>");
        String _string = pst.getDuration().toString();
        _builder.append(_string, "\t");
        _builder.append("</b2mml:Duration>");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<EquipmentSegmentSpecificationType> _equipmentSegmentSpecification = pst.getEquipmentSegmentSpecification();
      for(final EquipmentSegmentSpecificationType es : _equipmentSegmentSpecification) {
        _builder.append("<b2mml:EquipmentSegmentSpecification>");
        _builder.newLine();
        {
          EList<DescriptionType> _description_1 = es.getDescription();
          for(final DescriptionType desc_1 : _description_1) {
            _builder.append("\t");
            _builder.append("<b2mml:Description>");
            String _value_3 = desc_1.getValue();
            _builder.append(_value_3, "\t");
            _builder.append("</b2mml:Description>");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          EquipmentClassIDType _equipmentClassID = es.getEquipmentClassID();
          boolean _tripleNotEquals_2 = (_equipmentClassID != null);
          if (_tripleNotEquals_2) {
            _builder.append("\t");
            _builder.append("<b2mml:EquipmentClassID>");
            String _value_4 = es.getEquipmentClassID().getValue();
            _builder.append(_value_4, "\t");
            _builder.append("</b2mml:EquipmentClassID>");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          EquipmentIDType _equipmentID = es.getEquipmentID();
          boolean _tripleNotEquals_3 = (_equipmentID != null);
          if (_tripleNotEquals_3) {
            _builder.append("\t");
            _builder.append("<b2mml:EquipmentID>");
            String _value_5 = es.getEquipmentID().getValue();
            _builder.append(_value_5, "\t");
            _builder.append("</b2mml:EquipmentID>");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          CodeType _equipmentUse = es.getEquipmentUse();
          boolean _tripleNotEquals_4 = (_equipmentUse != null);
          if (_tripleNotEquals_4) {
            _builder.append("\t");
            _builder.append("<b2mml:EquipmentUse>");
            String _value_6 = es.getEquipmentUse().getValue();
            _builder.append(_value_6, "\t");
            _builder.append("</b2mml:EquipmentUse>");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          EList<QuantityValueType> _quantity = es.getQuantity();
          for(final QuantityValueType q : _quantity) {
            _builder.append("\t");
            _builder.append("<b2mml:Quantity>");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("<b2mml:QuantityString>");
            QuantityStringType _quantityString = q.getQuantityString();
            String _value_7 = null;
            if (_quantityString!=null) {
              _value_7=_quantityString.getValue();
            }
            _builder.append(_value_7, "\t\t");
            _builder.append("</b2mml:QuantityString>");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("<b2mml:UnitOfMeasure>");
            UnitOfMeasureType _unitOfMeasure = q.getUnitOfMeasure();
            String _value_8 = null;
            if (_unitOfMeasure!=null) {
              _value_8=_unitOfMeasure.getValue();
            }
            _builder.append(_value_8, "\t\t");
            _builder.append("</b2mml:UnitOfMeasure>");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("</b2mml:Quantity>");
            _builder.newLine();
          }
        }
        _builder.append("</b2mml:EquipmentSegmentSpecification>");
        _builder.newLine();
      }
    }
    {
      EList<MaterialSegmentSpecificationType> _materialSegmentSpecification = pst.getMaterialSegmentSpecification();
      for(final MaterialSegmentSpecificationType ms : _materialSegmentSpecification) {
        _builder.append("\t\t    ");
        _builder.append("<b2mml:MaterialSegmentSpecification>");
        _builder.newLine();
        {
          EList<DescriptionType> _description_2 = ms.getDescription();
          for(final DescriptionType desc_2 : _description_2) {
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("<b2mml:Description>");
            String _value_9 = desc_2.getValue();
            _builder.append(_value_9, "\t\t");
            _builder.append("</b2mml:Description>");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("<b2mml:MaterialDefinitionID>");
        MaterialDefinitionIDType _materialDefinitionID = ms.getMaterialDefinitionID();
        String _value_10 = null;
        if (_materialDefinitionID!=null) {
          _value_10=_materialDefinitionID.getValue();
        }
        _builder.append(_value_10, "\t\t");
        _builder.append("</b2mml:MaterialDefinitionID>");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("<b2mml:MaterialClassID>");
        MaterialClassIDType _materialClassID = ms.getMaterialClassID();
        String _value_11 = null;
        if (_materialClassID!=null) {
          _value_11=_materialClassID.getValue();
        }
        _builder.append(_value_11, "\t\t");
        _builder.append("</b2mml:MaterialClassID>");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("<b2mml:MaterialUse>");
        MaterialUseType _materialUse = ms.getMaterialUse();
        String _value_12 = null;
        if (_materialUse!=null) {
          _value_12=_materialUse.getValue();
        }
        _builder.append(_value_12, "\t\t");
        _builder.append("</b2mml:MaterialUse>");
        _builder.newLineIfNotEmpty();
        {
          EList<QuantityValueType> _quantity_1 = ms.getQuantity();
          for(final QuantityValueType q_1 : _quantity_1) {
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("<b2mml:Quantity>");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("<b2mml:QuantityString>");
            QuantityStringType _quantityString_1 = q_1.getQuantityString();
            String _value_13 = null;
            if (_quantityString_1!=null) {
              _value_13=_quantityString_1.getValue();
            }
            _builder.append(_value_13, "\t\t\t");
            _builder.append("</b2mml:QuantityString>");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("<b2mml:UnitOfMeasure>");
            UnitOfMeasureType _unitOfMeasure_1 = q_1.getUnitOfMeasure();
            String _value_14 = null;
            if (_unitOfMeasure_1!=null) {
              _value_14=_unitOfMeasure_1.getValue();
            }
            _builder.append(_value_14, "\t\t\t");
            _builder.append("</b2mml:UnitOfMeasure>");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("</b2mml:Quantity>");
            _builder.newLine();
          }
        }
        _builder.append("\t");
        _builder.append("</b2mml:MaterialSegmentSpecification>");
        _builder.newLine();
      }
    }
    {
      EList<PhysicalAssetSegmentSpecificationType> _physicalAssetSegmentSpecification = pst.getPhysicalAssetSegmentSpecification();
      for(final PhysicalAssetSegmentSpecificationType pas : _physicalAssetSegmentSpecification) {
        _builder.append("<b2mml:PhysicalAssetSegmentSpecification>");
        _builder.newLine();
        {
          EList<DescriptionType> _description_3 = pas.getDescription();
          for(final DescriptionType desc_3 : _description_3) {
            _builder.append("\t");
            _builder.append("<b2mml:Description>");
            String _value_15 = desc_3.getValue();
            _builder.append(_value_15, "\t");
            _builder.append("</b2mml:Description>");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t");
        _builder.append("<b2mml:PhysicalAssetClassID>");
        PhysicalAssetClassIDType _physicalAssetClassID = pas.getPhysicalAssetClassID();
        String _value_16 = null;
        if (_physicalAssetClassID!=null) {
          _value_16=_physicalAssetClassID.getValue();
        }
        _builder.append(_value_16, "\t");
        _builder.append("</b2mml:PhysicalAssetClassID>");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("<b2mml:PhysicalAssetID>");
        PhysicalAssetIDType _physicalAssetID = pas.getPhysicalAssetID();
        String _value_17 = null;
        if (_physicalAssetID!=null) {
          _value_17=_physicalAssetID.getValue();
        }
        _builder.append(_value_17, "\t");
        _builder.append("</b2mml:PhysicalAssetID>");
        _builder.newLineIfNotEmpty();
        {
          EList<QuantityValueType> _quantity_2 = pas.getQuantity();
          for(final QuantityValueType q_2 : _quantity_2) {
            _builder.append("\t");
            _builder.append("<b2mml:Quantity>");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("<b2mml:QuantityString>");
            QuantityStringType _quantityString_2 = q_2.getQuantityString();
            String _value_18 = null;
            if (_quantityString_2!=null) {
              _value_18=_quantityString_2.getValue();
            }
            _builder.append(_value_18, "\t\t");
            _builder.append("</b2mml:QuantityString>");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("<b2mml:UnitOfMeasure>");
            UnitOfMeasureType _unitOfMeasure_2 = q_2.getUnitOfMeasure();
            String _value_19 = null;
            if (_unitOfMeasure_2!=null) {
              _value_19=_unitOfMeasure_2.getValue();
            }
            _builder.append(_value_19, "\t\t");
            _builder.append("</b2mml:UnitOfMeasure>");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("</b2mml:Quantity>");
            _builder.newLine();
          }
        }
        _builder.append("</b2mml:PhysicalAssetSegmentSpecification>");
        _builder.newLine();
      }
    }
    {
      EList<SegmentDependencyType> _segmentDependency = pst.getSegmentDependency();
      for(final SegmentDependencyType sd : _segmentDependency) {
        _builder.append("<b2mml:SegmentDependency>");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("<b2mml:ID>");
        IdentifierType _iD_1 = sd.getID();
        String _value_20 = null;
        if (_iD_1!=null) {
          _value_20=_iD_1.getValue();
        }
        _builder.append(_value_20, "\t");
        _builder.append("</b2mml:ID>");
        _builder.newLineIfNotEmpty();
        {
          EList<DescriptionType> _description_4 = sd.getDescription();
          for(final DescriptionType desc_4 : _description_4) {
            _builder.append("<b2mml:Description>");
            String _value_21 = desc_4.getValue();
            _builder.append(_value_21);
            _builder.append("</b2mml:Description>");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t");
        _builder.append("<b2mml:Dependency>");
        DependencyType _dependency = sd.getDependency();
        String _value_22 = null;
        if (_dependency!=null) {
          _value_22=_dependency.getValue();
        }
        _builder.append(_value_22, "\t");
        _builder.append("</b2mml:Dependency>");
        _builder.newLineIfNotEmpty();
        {
          EList<ProcessSegmentIDType> _processSegmentID = sd.getProcessSegmentID();
          for(final ProcessSegmentIDType ps : _processSegmentID) {
            _builder.append("<b2mml:ProcessSegmentID>");
            String _value_23 = ps.getValue();
            _builder.append(_value_23);
            _builder.append("</b2mml:ProcessSegmentID>");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t\t    ");
        _builder.append("</b2mml:SegmentDependency>");
        _builder.newLine();
      }
    }
    _builder.append("</b2mml:ProcessSegment>");
    _builder.newLine();
    return _builder;
  }
  
  protected String _generateB2mmlFile(final OperationsDefinitionInformationType odit, final IFileSystemAccess fsa) {
    fsa.generateFile("operationsDefinitionInformationType.b2mml", this.generateOditContent(odit));
    return null;
  }
  
  private CharSequence generateOditContent(final OperationsDefinitionInformationType odit) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected String _generateB2mmlFile(final OperationsScheduleType osit, final IFileSystemAccess fsa) {
    fsa.generateFile("operationsScheduleType.b2mml", this.generateOsitContent(osit));
    return null;
  }
  
  private CharSequence generateOsitContent(final OperationsScheduleType osit) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected String _generateB2mmlFile(final OperationsPerformanceType opit, final IFileSystemAccess fsa) {
    fsa.generateFile("operationsPerformanceType.b2mml", this.generateOpitContent(opit));
    return null;
  }
  
  private CharSequence generateOpitContent(final OperationsPerformanceType opit) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  protected String _generateB2mmlFile(final EObject object, final IFileSystemAccess access) {
    return InputOutput.<String>println(("AMLCodeGenerator: Ignoring model element: " + object));
  }
  
  /**
   * Formats an Integer by introducing thousands separator.
   * 
   * @param i Integer that should be formatted
   * @return formatted Integer value as String
   */
  private String formatInteger(final int i) {
    String _xblockexpression = null;
    {
      int ic = i;
      String ret = "";
      while ((ic >= 1000)) {
        {
          String part = ("" + Integer.valueOf((ic % 1000)));
          while ((part.length() < 3)) {
            part = ("0" + part);
          }
          ret = (("." + part) + ret);
          ic = (ic / 1000);
        }
      }
      String _plus = (Integer.valueOf(ic) + ret);
      ret = _plus;
      _xblockexpression = ret;
    }
    return _xblockexpression;
  }
  
  /**
   * Formats a double value by introducing floating point.
   * 
   * @param d double value that should be formatted
   * @return formatted double value as String
   */
  private String formatDouble(final double d) {
    String _xblockexpression = null;
    {
      long _round = Math.round(d);
      boolean _equals = (d == _round);
      if (_equals) {
        return String.valueOf(((long) d));
      }
      final long ret = Math.round((d * 10));
      String _plus = (Long.valueOf((ret / 10)) + ",");
      _xblockexpression = (_plus + Long.valueOf((ret % 10)));
    }
    return _xblockexpression;
  }
  
  /**
   * Returns a String which is stripped of all non alpha-numerical values.
   * This is needed for computing the identifiers (id) of HTML elements as well as
   * the file names for details pages.
   * 
   * @param string String for which non alpha-numerical values should be removed
   * @return String with removed non alpha-numerical values
   */
  private String toAlphaNumerical(final String string) {
    return string.replaceAll("[^A-Za-z0-9]", "");
  }
  
  public String generateB2mmlFile(final EObject eit, final IFileSystemAccess fsa) {
    if (eit instanceof EquipmentInformationType) {
      return _generateB2mmlFile((EquipmentInformationType)eit, fsa);
    } else if (eit instanceof MaterialInformationType) {
      return _generateB2mmlFile((MaterialInformationType)eit, fsa);
    } else if (eit instanceof OperationsDefinitionInformationType) {
      return _generateB2mmlFile((OperationsDefinitionInformationType)eit, fsa);
    } else if (eit instanceof OperationsPerformanceType) {
      return _generateB2mmlFile((OperationsPerformanceType)eit, fsa);
    } else if (eit instanceof OperationsScheduleType) {
      return _generateB2mmlFile((OperationsScheduleType)eit, fsa);
    } else if (eit instanceof PersonnelInformationType) {
      return _generateB2mmlFile((PersonnelInformationType)eit, fsa);
    } else if (eit instanceof PhysicalAssetInformationType) {
      return _generateB2mmlFile((PhysicalAssetInformationType)eit, fsa);
    } else if (eit instanceof ProcessSegmentInformationType) {
      return _generateB2mmlFile((ProcessSegmentInformationType)eit, fsa);
    } else if (eit != null) {
      return _generateB2mmlFile(eit, fsa);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(eit, fsa).toString());
    }
  }
}
