package at.ac.tuwien.big.ame.aml.libraryAdder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import at.ac.tuwien.big.ame.aml.AmlFactory;
import at.ac.tuwien.big.ame.aml.Description;
import at.ac.tuwien.big.ame.aml.RoleClass;
import at.ac.tuwien.big.ame.aml.RoleClassLib;
import at.ac.tuwien.big.ame.aml.Version;

public class RoleClassParser extends XmlParser {
	
	public RoleClassParser(String name, String filePath) {
		super(name, filePath);
	}
	
	public RoleClassLib generate() {
		return readRoleClassLibXmlFile(getName(), getFilePath());
	}
	
	private RoleClassLib readRoleClassLibXmlFile(String name, String filePath) {
		File file = new File(filePath);
		if (!file.exists() || file.isDirectory()) {
			System.err.println("File is either a directory or does not exist!");
			return null;
		}
		RoleClassLib roleClasses = null;
		try (FileInputStream fis = new FileInputStream(file)) {
			XMLInputFactory xmlInFact = XMLInputFactory.newInstance();
			XMLStreamReader reader = xmlInFact.createXMLStreamReader(fis);
			while (reader.hasNext()) {
				int eventType = reader.next();
				switch (eventType) {
				case XMLStreamReader.START_ELEMENT:
					String elementName = reader.getLocalName();
					if (elementName.equals("RoleClassLib"))
						roleClasses = readRoleClassLib(reader, name);
					break;
				case XMLStreamReader.END_ELEMENT:
					return roleClasses;
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return roleClasses;
	}
	
	private RoleClassLib readRoleClassLib(XMLStreamReader reader, String name) throws XMLStreamException {
		RoleClassLib roleClassLib = AmlFactory.eINSTANCE.createRoleClassLib();
		roleClassLib.setName(name);
		
		while (reader.hasNext()) {
			int eventType = reader.next();
			switch (eventType) {
			case XMLStreamReader.START_ELEMENT:
				String elementName = reader.getLocalName();
				if (elementName.equals("RoleClass"))
					roleClassLib.getRoleClass().add(readRoleClass(reader));
				else if (elementName.equals("Description")) {
					Description description = AmlFactory.eINSTANCE.createDescription();
					description.setValue(readText(reader));
				} else if (elementName.equals("Version")) {
					Version version = AmlFactory.eINSTANCE.createVersion();
					version.setValue(readText(reader));
					roleClassLib.setVersion(version);
				}
				break;
			case XMLStreamReader.END_ELEMENT:
				return roleClassLib;
			}
		}
		throw new XMLStreamException("Premature end of file");
	}

	private RoleClass readRoleClass(XMLStreamReader reader) throws XMLStreamException {
		RoleClass roleClass = AmlFactory.eINSTANCE.createRoleClass();
		roleClass.setName(reader.getAttributeValue(null, "Name"));
		roleClass.setRefBaseClassPath(reader.getAttributeValue(null, "RefBaseClassPath"));

		while (reader.hasNext()) {
			int eventType = reader.next();
			switch (eventType) {
			case XMLStreamReader.START_ELEMENT:
				String elementName = reader.getLocalName();
				if (elementName.equals("Description")) {
					Description description = AmlFactory.eINSTANCE.createDescription();
					description.setValue(readText(reader));
					roleClass.setDescription(description);
				} else if (elementName.equals("RoleClass"))
					roleClass.getRoleClass().add(readRoleClass(reader));
				else if (elementName.equals("Attribute"))
					roleClass.getAttribute().add(readAttribute(reader));
				break;
			case XMLStreamReader.END_ELEMENT:
				return roleClass;
			}
		}
		throw new XMLStreamException("Premature end of file");
	}

}
