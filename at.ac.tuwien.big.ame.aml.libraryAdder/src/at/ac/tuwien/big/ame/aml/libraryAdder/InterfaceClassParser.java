package at.ac.tuwien.big.ame.aml.libraryAdder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import at.ac.tuwien.big.ame.aml.AmlFactory;
import at.ac.tuwien.big.ame.aml.Description;
import at.ac.tuwien.big.ame.aml.InterfaceClass;
import at.ac.tuwien.big.ame.aml.InterfaceClassLib;
import at.ac.tuwien.big.ame.aml.Version;

public class InterfaceClassParser extends XmlParser {
	
	public InterfaceClassParser(String name, String filePath) {
		super(name, filePath);
	}
	
	public InterfaceClassLib generate() {
		return readInterfaceClassLibXmlFile(getName(), getFilePath());
	}
	
	private InterfaceClassLib readInterfaceClassLibXmlFile(String name, String filePath) {
		File file = new File(filePath);
		if (!file.exists() || file.isDirectory()) {
			System.err.println("File is either a directory or does not exist!");
			return null;
		}
		InterfaceClassLib interfaceClassLib = null;
		try (FileInputStream fis = new FileInputStream(file)) {
			XMLInputFactory xmlInFact = XMLInputFactory.newInstance();
			XMLStreamReader reader = xmlInFact.createXMLStreamReader(fis);
			while (reader.hasNext()) {
				int eventType = reader.next();
				switch (eventType) {
				case XMLStreamReader.START_ELEMENT:
					String elementName = reader.getLocalName();
					if (elementName.equals("InterfaceClassLib"))
						interfaceClassLib = readInterfaceClassLib(reader, name);
					break;
				case XMLStreamReader.END_ELEMENT:
					return interfaceClassLib;
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return interfaceClassLib;
	}
	
	private InterfaceClassLib readInterfaceClassLib(XMLStreamReader reader, String name) throws XMLStreamException {
		InterfaceClassLib interfaceClassLib = AmlFactory.eINSTANCE.createInterfaceClassLib();
		interfaceClassLib.setName(name);
		
		while (reader.hasNext()) {
			int eventType = reader.next();
			switch (eventType) {
			case XMLStreamReader.START_ELEMENT:
				String elementName = reader.getLocalName();
				if (elementName.equals("InterfaceClass"))
					interfaceClassLib.getInterfaceClass().add(readInterfaceClass(reader));
				else if (elementName.equals("Description")) {
					Description description = AmlFactory.eINSTANCE.createDescription();
					description.setValue(readText(reader));
				} else if (elementName.equals("Version")) {
					Version version = AmlFactory.eINSTANCE.createVersion();
					version.setValue(readText(reader));
					interfaceClassLib.setVersion(version);
				}
				break;
			case XMLStreamReader.END_ELEMENT:
				return interfaceClassLib;
			}
		}
		throw new XMLStreamException("Premature end of file");
	}

	private InterfaceClass readInterfaceClass(XMLStreamReader reader) throws XMLStreamException {
		InterfaceClass interfaceClass = AmlFactory.eINSTANCE.createInterfaceClass();
		interfaceClass.setName(reader.getAttributeValue(null, "Name"));
		interfaceClass.setRefBaseClassPath(reader.getAttributeValue(null, "RefBaseClassPath"));

		while (reader.hasNext()) {
			int eventType = reader.next();
			switch (eventType) {
			case XMLStreamReader.START_ELEMENT:
				String elementName = reader.getLocalName();
				if (elementName.equals("Description")) {
					Description description = AmlFactory.eINSTANCE.createDescription();
					description.setValue(readText(reader));
					interfaceClass.setDescription(description);
				} else if (elementName.equals("InterfaceClass"))
					interfaceClass.getInterfaceClass().add(readInterfaceClass(reader));
				else if (elementName.equals("Attribute"))
					interfaceClass.getAttribute().add(readAttribute(reader));
				break;
			case XMLStreamReader.END_ELEMENT:
				return interfaceClass;
			}
		}
		throw new XMLStreamException("Premature end of file");
	}

}
