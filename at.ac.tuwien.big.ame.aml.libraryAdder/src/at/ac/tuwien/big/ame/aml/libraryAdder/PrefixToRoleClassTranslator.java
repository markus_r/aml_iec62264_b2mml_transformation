package at.ac.tuwien.big.ame.aml.libraryAdder;

import at.ac.tuwien.big.ame.aml.CAEXFile;
import at.ac.tuwien.big.ame.aml.RoleClass;
import at.ac.tuwien.big.ame.aml.RoleClassLib;

public class PrefixToRoleClassTranslator {

	private CAEXFile model = null;

	public PrefixToRoleClassTranslator(CAEXFile model) {
		this.model = model;
	}

	public boolean translate(String roleClassLibName) {
		if (model == null) {
			System.err.println("Model is not set!");
			return false;
		}
		RoleClassLib roleClassLib = null;
		for (RoleClassLib rcl : model.getRoleClassLib()) {
			if (rcl.getName().equalsIgnoreCase(roleClassLibName)) {
				roleClassLib = rcl;
			}
		}
		if (roleClassLib == null) {
			System.err.println("RoleClassLib was not found in model!");
			return false;
		}
		RoleClassLib amlIecRoleClassLib = null;
		for (RoleClassLib rcl : model.getRoleClassLib()) {
			if (rcl.getName().equalsIgnoreCase("AutomationMLIEC622642RoleClassLib")) {
				amlIecRoleClassLib = rcl;
			}
		}
		if (amlIecRoleClassLib == null) {
			System.err.println("AMLIEC622642RoleClassLib not found in model!");
			return false;
		}
		for (RoleClass rc : roleClassLib.getRoleClass()) {
			String prefix = rc.getName().split("###")[0];
			String name = rc.getName().split("###")[1];
			if (prefix == null || prefix.equals("")) {
				System.err.println("Cannot find prefix for role class " + rc.getName() + "!");
				continue;
			}
			switch (prefix) {
			case "PC":
				rc.setRefBaseClassPath(amlIecRoleClassLib.getName() + "/PersonnelModel/PersonnelClass");
				rc.setName(name);
				break;
			case "EC":
				rc.setRefBaseClassPath(amlIecRoleClassLib.getName() + "/EquipmentModel/EquipmentClass");
				rc.setName(name);
				break;
			case "AC":
				rc.setRefBaseClassPath(amlIecRoleClassLib.getName() + "/PhysicalAssetModel/PhysicalAssetClass");
				rc.setName(name);
				break;
			case "MC":
				rc.setRefBaseClassPath(amlIecRoleClassLib.getName() + "/MaterialModel/MaterialClass");
				rc.setName(name);
				break;
			default:
				System.err.println("Cannot find class to prefix " + prefix + "!");
				break;
			}
		}
		return true;
	}
}
