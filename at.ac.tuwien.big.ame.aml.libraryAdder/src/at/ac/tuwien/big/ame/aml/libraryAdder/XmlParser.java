package at.ac.tuwien.big.ame.aml.libraryAdder;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import at.ac.tuwien.big.ame.aml.AmlFactory;
import at.ac.tuwien.big.ame.aml.Attribute;

public abstract class XmlParser {

	private String filePath;
	private String name;

	public XmlParser(String name, String filePath) {
		this.setFilePath(filePath);
		this.setName(name);
	}

	public abstract Object generate();

	protected String readText(XMLStreamReader reader) throws XMLStreamException {
		StringBuilder result = new StringBuilder();
		while (reader.hasNext()) {
			int eventType = reader.next();
			switch (eventType) {
			case XMLStreamReader.CHARACTERS:
			case XMLStreamReader.CDATA:
				result.append(reader.getText());
				break;
			case XMLStreamReader.END_ELEMENT:
				return result.toString();
			}
		}
		throw new XMLStreamException("Premature end of file");
	}

	protected Attribute readAttribute(XMLStreamReader reader) throws XMLStreamException {
		Attribute attribute = AmlFactory.eINSTANCE.createAttribute();
		attribute.setName(reader.getAttributeValue(null, "Name"));
		attribute.setAttributeDataType("xs:string");

		while (reader.hasNext()) {
			int eventType = reader.next();
			switch (eventType) {
			case XMLStreamReader.START_ELEMENT:
				attribute.setValue(readText(reader));
				break;
			case XMLStreamReader.END_ELEMENT:
				return attribute;
			}
		}
		throw new XMLStreamException("Premature end of file");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}
