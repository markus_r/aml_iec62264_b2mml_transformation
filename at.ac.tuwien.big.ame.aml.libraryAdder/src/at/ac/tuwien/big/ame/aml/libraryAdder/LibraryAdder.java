package at.ac.tuwien.big.ame.aml.libraryAdder;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import at.ac.tuwien.big.ame.aml.AmlPackage;
import at.ac.tuwien.big.ame.aml.CAEXFile;
import at.ac.tuwien.big.ame.aml.InterfaceClassLib;
import at.ac.tuwien.big.ame.aml.RoleClassLib;

// add libraries to existing AML models
public class LibraryAdder {

	public static void main(String[] args) {
		try {
			CAEXFile caexFile = (CAEXFile) loadModel(
					"E:\\OneDrive\\SS 2018\\AME - Advanced Model Engineering\\workspace\\at.ac.tuwien.big.ame.aml.libraryAdder\\resources\\result.xmi");
			addInterfaceClassLibraryToModel(caexFile, "AutomationMLIEC622642InterfaceClassLib",
					"E:\\OneDrive\\SS 2018\\AME - Advanced Model Engineering\\workspace\\at.ac.tuwien.big.ame.aml.libraryAdder\\resources\\AutomationMLIEC622642InterfaceClassLib.xml");
			addInterfaceClassLibraryToModel(caexFile, "AutomationMLInterfaceClassLib",
					"E:\\OneDrive\\SS 2018\\AME - Advanced Model Engineering\\workspace\\at.ac.tuwien.big.ame.aml.libraryAdder\\resources\\AutomationMLInterfaceClassLib.xml");
			addInterfaceClassLibraryToModel(caexFile, "AutomationMLB2MMLInterfaceClassLib",
					"E:\\OneDrive\\SS 2018\\AME - Advanced Model Engineering\\workspace\\at.ac.tuwien.big.ame.aml.libraryAdder\\resources\\AutomationMLB2MMLInterfaceClassLib.xml");
			addRoleClassLibraryToModel(caexFile, "AutomationMLIEC622642RoleClassLib",
					"E:\\OneDrive\\SS 2018\\AME - Advanced Model Engineering\\workspace\\at.ac.tuwien.big.ame.aml.libraryAdder\\resources\\AutomationMLIEC622642RoleClassLib.xml");
			addRoleClassLibraryToModel(caexFile, "AutomationMLBaseRoleClassLib",
					"E:\\OneDrive\\SS 2018\\AME - Advanced Model Engineering\\workspace\\at.ac.tuwien.big.ame.aml.libraryAdder\\resources\\AutomationMLBaseRoleClassLib.xml");
			addRoleClassLibraryToModel(caexFile, "AutomationMLB2MMLRoleClassLib",
					"E:\\OneDrive\\SS 2018\\AME - Advanced Model Engineering\\workspace\\at.ac.tuwien.big.ame.aml.libraryAdder\\resources\\AutomationMLB2MMLRoleClassLib.xml");
			translatePrefixes(caexFile, "RoleClassLibrary Furniture, Inc");
			saveModel(
					"E:\\OneDrive\\SS 2018\\AME - Advanced Model Engineering\\workspace\\at.ac.tuwien.big.ame.aml.libraryAdder\\resources\\resultWithLibraries.xmi",
					caexFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void translatePrefixes(CAEXFile caexFile, String rclName) {
		PrefixToRoleClassTranslator translator = new PrefixToRoleClassTranslator(caexFile);
		System.out.println(translator.translate(rclName));
	}

	public static EObject loadModel(String uri) {
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("xmi", new XMIResourceFactoryImpl());

		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getPackageRegistry().put(AmlPackage.eINSTANCE.getNsURI(), AmlPackage.eINSTANCE);

		URI fileUri = URI.createFileURI(new File(uri).getAbsolutePath());

		Resource resource = resourceSet.getResource(fileUri, true);

		return resource.getContents().get(0);
	}

	public static void addRoleClassLibraryToModel(CAEXFile caexFile, String libraryName, String libraryPath) {
		RoleClassParser roleClassParser = new RoleClassParser(libraryName, libraryPath);
		RoleClassLib newRoleClassLib = roleClassParser.generate();
		caexFile.getRoleClassLib().add(newRoleClassLib);
	}

	public static void addInterfaceClassLibraryToModel(CAEXFile caexFile, String libraryName, String libraryPath) {
		InterfaceClassParser interfaceClassParser = new InterfaceClassParser(libraryName, libraryPath);
		InterfaceClassLib newInterfaceClassLib = interfaceClassParser.generate();
		caexFile.getInterfaceClassLib().add(newInterfaceClassLib);
	}

	public static void saveModel(String uri, EObject content) throws IOException {
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("xmi", new XMIResourceFactoryImpl());

		ResourceSet resourceSet = new ResourceSetImpl();

		URI fileUri = URI.createFileURI(new File(uri).getAbsolutePath());

		Resource resource = resourceSet.createResource(fileUri);

		resource.getContents().add(content);

		resource.save(Collections.EMPTY_MAP);
	}
}
